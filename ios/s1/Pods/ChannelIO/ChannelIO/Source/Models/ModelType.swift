//
//  ModelType.swift
//  CHPlugin
//
//  Created by 이수완 on 2017. 1. 14..
//  Copyright © 2017년 ZOYI. All rights reserved.
//

import Then

protocol ModelType: Then {
  var id: String { get }
}
