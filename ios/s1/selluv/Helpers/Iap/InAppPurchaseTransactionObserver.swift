//
//  InAppPurchaseTransactionObserver.swift
//
//
//  Created by Dragon C. on 8/20/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import UIKit
import StoreKit

class InAppPurchaseTransactionObserver: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
	var productsRequest: SKProductsRequest!
	
	let ProductArray = [
		"me.kkebi.ios.coin7",
		"me.kkebi.ios.coin150",
		"me.kkebi.ios.coin300",
		"me.kkebi.ios.coin500",
		"me.kkebi.ios.coin1000",
		"me.kkebi.ios.coin3000",
		]
	
	let CoinArray = [ 70, 150, 300, 500, 1000, 3000 ]
	let MoneyArray = [ 2200, 4500, 9000, 15000, 30000, 90000 ]
	
	
	static var iap: InAppPurchaseTransactionObserver!
	
    class func getInstance() -> InAppPurchaseTransactionObserver {
        if InAppPurchaseTransactionObserver.iap == nil {
            InAppPurchaseTransactionObserver.iap = InAppPurchaseTransactionObserver()
        }
        return InAppPurchaseTransactionObserver.iap
    }
    
    //////////////////////////////////////////////////////////////////////
    // MARK: - SKProductsRequestDelegate
    //////////////////////////////////////////////////////////////////////
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let products = response.products
        for product in products {
            print(product.productIdentifier)
        }
    }
    
    //////////////////////////////////////////////////////////////////////
    // MARK: - SKPaymentTransactionObserver
    //////////////////////////////////////////////////////////////////////
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                submitIAP(transaction)
                break
                
            case SKPaymentTransactionState.failed:
				if let transactionError = transaction.error as NSError?, transactionError.code != SKError.paymentCancelled.rawValue {
					//transaction.error!.localizedDescription
//                    AlertVC.showAlert(CommonUtil.topViewController()!, "구매처리 중 오류가 발생하였습니다.\n잠시후 다시 시도해 주세요.", title: Menu, ok: Confirm)
				}
				SKPaymentQueue.default().finishTransaction(transaction)
                break
                
            case SKPaymentTransactionState.restored:
                SKPaymentQueue.default().finishTransaction(transaction)
                break
                
            default:
                break
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////
    // MARK: Helpers
    //////////////////////////////////////////////////////////////////////
    
    func initIAP() {
        productsRequest = SKProductsRequest(productIdentifiers: Set(ProductArray))
        productsRequest.delegate = self
        productsRequest.start()
        SKPaymentQueue.default().add(self)
    }
    
    func finalizeIAP() {
        SKPaymentQueue.default().remove(self)
    }
    
    func purchase(_ idx: Int) {
        let payment = SKMutablePayment()
        payment.productIdentifier = ProductArray[idx]
        SKPaymentQueue.default().add(payment)
    }
	
	
	//////////////////////////////////////////////////////////////////////
	// MARK: - Net Module
	//////////////////////////////////////////////////////////////////////
	
	func submitIAP(_ transaction: SKPaymentTransaction) {
        var receipt = ""
        
		if let url = Bundle.main.appStoreReceiptURL, let data = NSData(contentsOf: url) {
			let base64 = data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
			receipt = base64
		}
		
//        if !receipt.isEmpty && gMeInfo.isValid {
//            let idx = ProductArray.index(of: transaction.payment.productIdentifier)!
//            let product = transaction.payment.productIdentifier
//            let transactionID = transaction.transactionIdentifier!
//            let point = CoinArray[idx]
//            let money = MoneyArray[idx]
			
			gProgress.show()
//            Net.payment(
//                user_sno: gMeInfo.user_sno,
//                user_uuid: gMeInfo.user_uuid,
//                productID: product,
//                transactionID: transactionID,
//                receiptData: receipt,
//                point: point,
//                money: money,
//                success: { result -> Void in
//                    gProgress.hide()
//                    SKPaymentQueue.default().finishTransaction(transaction)
//                    let res = result as! Net.PaymentResult
//                    self.submitResult(res)
//            }, failure: { (code, err) -> Void in
//                gProgress.hide()
//                print("payment Error: #\(code) \(err)")
//                AlertVC.showAlert(CommonUtil.topViewController()!, err, title: Menu, ok: Confirm)
//            })
//        }
    }
	
}
