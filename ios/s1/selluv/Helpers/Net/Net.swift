    //
    //  Net.swift
    //
    //
    //  Created by Dragon C. on 8/8/16.
    //  Copyright © 2016 Dragon C. All rights reserved.
    //
    
    import UIKit
    import Alamofire
    import SwiftyJSON
    
    public class Net {
        
        private static func convVal(_ bVal: Bool) -> String {
            return bVal ? "Y" : "N"
        }
        
        //
        // MARK: API response structure
        //
        public typealias SuccessBlock = (ResponseResult?) -> Void
        public typealias FailureBlock = (_ code: Int, _ err: String) -> Void
        
        open class ResponseResult {
            
        }
        
        open class StatusResult: ResponseResult {
            var msg: String!
        }
        
        //회원가입 결과
        open class SingupResult: ResponseResult {
            var accessToken  : String!  //액세스 토큰 ,
            var addressList  = [UsrAddressDao!]()  //주소정보
            var usr          : JSON!  //회원정보
        }
        
        //관심 브렌드 목록 얻기 결과
        open class AllFramBrandResult: ResponseResult {
            var list = [Brand!]()
        }

        //모든 브렌드 디테일 목록 얻기
        open class AllBrandDetailListResult: ResponseResult {
            var list = [Brand!]()
            var list_en = [[Brand!]](repeating: [Brand](), count: index_eng.count)
            var list_ko = [[Brand!]](repeating: [Brand](), count: index_kor.count)

        }
        
        //로그인 결과
        open class UserLoginResult: ResponseResult {
            var accessToken  : String!  //액세스 토큰 ,
            var addressList  = [UsrAddressDao!]()  //주소정보
            var usr          : JSON!  //회원정보
        }
        
        //약관 얻기 결과
        open class OtherLineseResult: ResponseResult {
            var content         : String!
            var edtTime         : Int!
            var licenseUid      : Int!
            var regTime         : Int!
            var status          : Int!
        }
        
        //1:1문의 목록 얻기
        open class getQnaListResult: ResponseResult {
            var list = [Qna!]()
        }
        
        //faq 목록 얻기
        open class getFaqListResult: ResponseResult {
            var list = [Faq!]()
        }
        
        //프로모션 정보 얻기 결과
        open class getPromotionInfoResult: ResponseResult {
            var dicInfo         : PromotionCodeDao!
        }
      
        //공지사항 목록 얻기
        open class getNoticeListResult: ResponseResult {
            var list = [Notice!]()
        }
        
        //이벤트 목록 얻기
        open class getEventListResult: ResponseResult {
            var list = [Event!]()
        }
        
        //배너 목록 얻기
        open class getBannerListResult: ResponseResult {
            var list = [BannerDto!]()
        }
        
        //회원 상세정보 얻기
        open class MyDetailInfoResult: ResponseResult {
            var addressList         = [UsrAddressDao!]()  //주소정보
            var countBuy            : Int!  // 구매수 ,
            var countSell           : Int!  //판매수 ,
            var countPdt            : Int!   //상품수
            var countUnreadNotice   : Int!  //읽지 않는 공지수 ,
            var myPageEventList     = [Event!]()  //마이페이지 노출이벤트 ,
            var usr                 : JSON!  //회원정보 
            var usrFollowerCount    : Int!   //팔로워수
            var usrFollowingCount   : Int!  // 팔로잉수
        }
        
        //기타 회원 상세정보 얻기
        open class OtherUsrDetailInfoResult: ResponseResult {
            var usrDto              : UsrInfoDto! //
        }
        
        //회원 상세정보 얻기
        open class AlarmCountResult: ResponseResult {
            var dealCount          : Int! //거래 알림갯수 , ,
            var newsCount          : Int! //소식 알림갯수 , ,
            var replyCount         : Int! //댓글 알림갯수 , ,
            var totalCount         : Int! //전체 알림갯수 ,
        }
        
        //좋아요 업데이트뱃지 표시여부
        open class PdtLikeUpdateStatusResult: ResponseResult {
            var status          : Bool!
            
        }

        //알림목록 얻기 결과
        open class AlarmResult: ResponseResult {
            var list                = [Alarm!]()
            var last                : Bool!
        }
        
        //인기, 피드, 신상, 인기스타일, 피드스타일, 신상스타일 목록 결과
        open class FeedPdtResult: ResponseResult {
            var list                = [FeedPdtDto!]()
            var last                : Bool!
        }
        
        //알림설정 얻기 결과
        open class UserAlarmResult: ResponseResult {
            var dealSetting          : String! //거래알림 - 10자리 숫자배열(1-사용 0-사용안함) ,
            var dealYn               : Int! //  거래알림 사용여부 1-사용, 0-사용안함 ,
            var newsSetting          : String! // 소식알림 - 12자리 숫자배열(1-사용 0-사용안함) ,
            var newsYn               : Int! // // 소식알림 사용여부 1-사용, 0-사용안함 ,
            var replySetting         : String! //댓글알림 - 2자리 숫자배열(1-사용 0-사용안함) ,
            var replyYn              : Int! //댓글알림 사용여부 1-사용, 0-사용안함 ,
            var usrUid               : Int!

        }
        
        //찾는 상품 목록 얻기 결과
        open class FindItemResult: ResponseResult {
            var list                = [FindItemDto!]()
            var last                : Bool!
        }
        
        //카드 목록 얻기 결과
        open class UsrCardResult: ResponseResult {
            var list = [UsrCardDto!]()
        }
        
        //한개 파일 업로드 결과
        open class UploadFileResult: ResponseResult {
            var fileName               : String!
            var fileURL                : String!
        }

        //여러개 파일 업로드 결과
        open class UploadFilesResult: ResponseResult {
            var fileUrls            : Array<String> = []
            var fileNames           : Array<String> = []
        }

        //차단된 유저 목록 얻기 결과
        open class BlockUsrResult: ResponseResult {
            var list                = [BlockUsrDto!]()
            var last                : Bool!
        }
        
        //SNS 연동정보 얻기
        open class UserSNSStatusResult: ResponseResult {
            var facekbook           : Bool!
            var instagram           : Bool!
            var kakaoTalk           : Bool!
            var naver               : Bool!
            var twitter             : Bool!
            
        }
        
        //스타일 팝업 상세
        open class DetailPdtStyleResult: ResponseResult {
            var list                = [PdtStyleDao!]()
        }
        
        //상품 스타일 목록 얻기
        open class GetPdtStyleListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
        }
        
        //전체 상품 스타일 목록 얻기
        open class GetAllPdtStyleListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
        }
        
        //좋아요 목록 얻기
        open class GetLikeListResult: ResponseResult {
            var list                = [FeedPdtDto!]()
            var last                : Bool!
            var totalElements       : Int!
        }
        
        //좋아요 스타일 목록 얻기
        open class GetLikeStyleResult: ResponseResult {
            var list                = [FeedPdtDto!]()
            var last                : Bool!
            var totalElements       : Int!
        }
        
        //랄로우 브렌드 목록 얻기 결과
        open class BrandLikeResult: ResponseResult {
            var list = [Brand!]()
            var list_en = [[Brand!]](repeating: [Brand](), count: index_eng.count)
            var list_ko = [[Brand!]](repeating: [Brand](), count: index_kor.count)
        }
        
        //모든 브렌드 목록 얻기 결과
        open class AllBrandResult: ResponseResult {
            var list = [BrandMiniDto!]()
            var list_en = [[BrandMiniDto!]](repeating: [BrandMiniDto](), count: index_eng.count)
            var list_ko = [[BrandMiniDto!]](repeating: [BrandMiniDto](), count: index_kor.count)
        }

        //인기 브렌드 목록 얻기 결과(10개)
        open class FameBrandResult: ResponseResult {
            var list = [Brand!]()
        }
        
        //브렌드 상세 보기
        open class BrandDetailResult: ResponseResult {
            var backImg                 : String! //  백그라운드 이미지 ,
            var brandLikeCount          : Int!  // 팔로우 유저수 ,
            var brandLikeStatus         : Bool!  //유저 팔로우 상태 ,
            var brandUid                : Int!   //브랜드UID
            var femalePdtCount          : Int!  //여성상품군 상품갯수 ,
            var firstEn                 : String!  //영문첫글자
            var firstKo                 : String!  //한글초성
            var kidsPdtCount            : Int!   //키즈상품군 상품갯수 ,
            var malePdtCount            : Int!  // 남성상품군 상품갯수 ,
            var licenseUrl              : String!  //라이센스 ,
            var logoImg                 : String!  //로고이미지 ,
            var nameEn                  : String!  //영문이름 ,
            var nameKo                  : String!  //한글이름 ,
            var profileImg              : String!  //썸네일이미지 ,
            var stylePdtCount           : Int!   //스타일 상품갯수
            
        }
        
        //브렌드 상품군에 따르는 리스트 정보 얻기
        open class AllBrandPdtListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
            var totalElements       : Int!
        }
        
        //테미 목록 얻기 결과
        open class ThemeListResult: ResponseResult {
            var list                = [ThemeListDto!]()
        }

        //테미 상세 결과
        open class ThemeDetailResult: ResponseResult {
            var themeDeto           : ThemeListDto!
        }
        
        //추천 부렌드 목록 엳기 결과
        open class RecommendBrandResult: ResponseResult {
            var list               = [BrandMiniDto!]()
        }
        
        //상품 상세 정보 얻기 결과
        open class getPdtDetailInfoResult: ResponseResult {
            var dicInfo           : PdtDetailDto!
        }

        //상품상세 추천상품 목록 얻기 결과
        open class getRecommendPdtListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
        }
        
        //상품 가품신고정보 얻기
        open class getPdtFakeInfoResult: ResponseResult {
            var reward              : Int!  //가품신고 리워드 금액 ,
            var totalCount          : Int!  //가품신고 누적수
        }
        
        //상품상세 연관스타일 목록 얻기 결과
        open class getRelationStyleListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
            var totalElements       : Int!
        }
        
        //잇템 유저 목록 얻기 결과
        open class getPdtItemListResult: ResponseResult {
            var list                = [UsrFollowListDto!]()
            var last                : Bool!
        }
        
        //좋아요 유저목록 얻기 결과
        open class getLikeUsrListResult: ResponseResult {
            var list                = [UsrFollowListDto!]()
            var last                : Bool!
        }

        //상품 댓글 목록 얻기 결과
        open class getPdtReplyListResult: ResponseResult {
            var list                = [ReplyDto!]()
            var last                : Bool!
        }

        //상품 댓글 작성 결과
        open class setPdtReplyResult: ResponseResult {
            var replyDto            : ReplyDto!
        }

        //상품 댓글가능 회원목록얻기 결과
        open class getPdtReplyUsrListResult: ResponseResult {
            var list                = [UsrMiniDto!]()
        }
        
        //네고 신청 결과
        open class reqDealNegoResult: ResponseResult {
            var dealDao             : DealDao!
        }

        //상품 구매 결과
        open class reqDealBuyResult: ResponseResult {
            var dealDao             : DealDao!  //거래정보
            var paymentUrl          : String!  //카드간편결제인 경우 빈문자열, 기타 결제URL 웹뷰에서 호출필요함
        }
        
        //상품 정보 수정 결과
        open class reqpdtModifyResult: ResponseResult {
            var dicInfo             : PdtMiniDto!
        }

        //이벤트 상세 얻기 결과
        open class getEventDetailInfoResult: ResponseResult {
            var dicInfo             : Event!
        }
        
        //공지사항 상세 얻기 결과
        open class getNoticeDetailInfoResult: ResponseResult {
            var dicInfo             : Notice!
        }

        //시스템 설정정보 얻기 결과
        open class getSystemSettingInfoResult: ResponseResult {
            var dicInfo             : SystemSettingDto!
        }

        //사이즈 목록 얻기 결과
        open class getSizeListResult: ResponseResult {
            var dicInfo             : SizeRefDto!
        }

        //브랜드에 따르는 추천모델명 리스트 얻기 결과
        open class getBrandModelNmsListResult: ResponseResult {
            var list                : Array<String> = []
        }
        
        //하위 카테고리 목록 얻기 결과
        open class getSubCategoryListResult: ResponseResult {
            var list                = [CategoryDao!]()
        }

        //카테고리별 인기브랜드 및 하위카테고리 얻기 결과
        open class getCategoryDetailListResult: ResponseResult {
            var arrBrand            = [BrandRecommendedListDto!]()
            var arrCategory         = [CategoryDao!]()
            var dicCategory         : CategoryDao!
        }
        
        //해당 카테고리에 속하는 상품목록얻기(브랜드기준)
        open class getCategoryPdtListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
            var totalElements       : Int!
        }
        
        //상품 등록 결과
        open class regPdtInfoResult: ResponseResult {
            var dicPdtInfo         : PdtDetailDto!
        }
        
        //회원의 아이템 얻기 결과
        open class getUsrPdtListResult: ResponseResult {
            var list                = [PdtListDto!]()
            var last                : Bool!
            var totalElements       : Int!
        }
        
        
        //매너포인트 변경이력(거래후기) 얻기 결과
        open class getUsrReviewResult: ResponseResult {
            var list                = [ReviewListDto!]()
            var last                : Bool!
            var totalElements       : Int!
            
            var countOnePoint       : Int!  //보통갯수
            var countTwoPoint       : Int!  //만족갯수
            var countZeroPoint      : Int!  //불만족갯수
        }
        
        //브렌드 등록
        open class regBrandResult: ResponseResult {
            var dicInfo             : BrandMiniDto!
        }
        
        //초대정보 얻기
        open class InviteInfoResult: ResponseResult {
            var boughtCount          : Int! //첫 구매한 친구수
            var inviteCode           : String! //추천인코드
            var invitedCount         : Int! //가입한 친구수
            var invitedMonthCount    : Int! //이달 가입한 친구수
        }

        //연락처 친구찾기
        open class getFriendAddressResult: ResponseResult {
            var joinedList           = [UsrFollowListDto!]() //가입한 지인
            var unJoinedPhoneList    : Array<String> = [] //가입하지 않은 전화번호 목록
        }

        //facebook 친구찾기
        open class getFriendFacebookResult: ResponseResult {
            var list                 = [UsrFollowListDto!]()
        }
        
        //셀럽머니 변경이력 얻기
        open class getMoneyHistoryResult: ResponseResult {
            var list                 = [UsrMoneyHisDto!]()
            var last                 : Bool!
            var totalElements        : Int!
            var cashbackMoney        : Int! //판매정산
            var money                : Int! // 셀럽머니
        }

        //주문상세
        open class getDealDetailResult: ResponseResult {
            var dealDetail           : DealDetailDto!
        }
        
        //회원 구매내역 얻기
        open class getBuyHistoryResult: ResponseResult {
            var list                 = [DealListDto!]()
            var last                 : Bool!
            var totalElements        : Int!
            
            var completedCount       : Int! //거래완료수
            var negoCount            : Int! // 네고제안수
            var preparedCount        : Int! // 배송준비수
            var progressCount        : Int! // 배송진행수
            var sentCount            : Int! // 배송완료수
            var verifiedCount        : Int! // 정품인증수
        }

        //회원 판매내역 얻기
        open class getSellHistoryResult: ResponseResult {
            var list                 = [DealListDto!]()
            var last                 : Bool!
            var totalElements        : Int!
            
            var cachedCount          : Int! // 정산완료수
            var completedCount       : Int! // 거래완료수
            var preparedCount        : Int! // 발송대기수
            var progressCount        : Int! // 배송진행수
            var sentCount            : Int! // 배송완료수
            var negoCount            : Int! // 네고제안수
        }
        
        //판매자 반품 거절 결과
        open class reqRefundDisallowResult: ResponseResult {
            var deal                 : DealDao!
        }

        //판매자 반품 확인 결과
        open class reqRefundCompleteResult: ResponseResult {
            var deal                 : DealDao!
        }

        //구매자 반품 신청 결과
        open class reqDealRefundResult: ResponseResult {
            var deal                 : DealDao!
        }

        //구매자 카운터 신청 결과
        open class reqDealNegoAllowCounterResult: ResponseResult {
            var deal                 : DealDao!
        }

        //판매자네고 승인 결과
        open class reqDealNegoAllowResult: ResponseResult {
            var deal                 : DealDao!
        }

        //판매자 운송장 번호등록 결과
        open class reqDealDeliveryNumResult: ResponseResult {
            var deal                 : DealDao!
        }

        //택배 예약하기
        open class reqDealDeliveryResult: ResponseResult {
            var deal                 : DeliveryHistoryDao!
        }

        //구매자 거래 확정 결과
        open class reqDealCompleteResult: ResponseResult {
            var deal                 : DealDao!
        }

        //판매자 판매 거절
        open class reqDealCancelSellerResult: ResponseResult {
            var deal                 : DealDao!
        }

        //구매자 주문취소
        open class reqDealCancelResult: ResponseResult {
            var deal                 : DealDao!
        }

        //검색 결과
        open class SearchResult: ResponseResult {
            var list                 = [PdtListDto!]()
            var last                 : Bool!
            var totalElements        : Int!
        }

        //검색 결과 갯수
        open class SearchCountResult: ResponseResult {
            var count                : Int!
        }

        //상세검색
        open class getSearchDetailResult: ResponseResult {
            var list                 = [PdtListDto!]()
            var last                 : Bool!
            var totalElements        : Int!
        }

        //인기검색 키워드 리스트 얻기
        open class getSearchFameResult: ResponseResult {
            var list                 = [SearchWordDto!]()
        }

        //상품 최대 최소가격
        open class getSearchPriceRangeResult: ResponseResult {
            var max                  : Int!
            var min                  : Int!
        }

        //태그검색 결과
        open class getSearchTagResult: ResponseResult {
            var list                 : Array<String> = []
        }

        //추천검색 키워드 리스트 얻기 결과
        open class getSearchRecommendResult: ResponseResult {
            var searchDto            : RecommendSearchDto!
        }

        //추천 유저목록 얻기 결과
        open class getRecommendUsrListResult: ResponseResult {
            var list                 = [UsrRecommendDto!]()
            var last                 : Bool!
        }

        //영업일기준 날짜 얻기
        open class ggetWorkingDayResult: ResponseResult {
            var targetDate           : String! //타겟날짜
        }
        
        //택배사 목록 얻기
        open class ggetDeliveryCompaniesResult: ResponseResult {
            var companies            : Array<String> = [] //택배사 목록
        }
        
        //택배예약 정보 얻기
        open class getDealDeliveryInfoResult: ResponseResult {
            var dealHisDao           : DeliveryHistoryDao!
        }
        
        
        
        //
        // MARK: Helper functions
        //

        /**
           *  HTTP request.
           */
        private static func doRequest(
            method         : Alamofire.HTTPMethod,
            api            : String,
            api_type       : F_API,
            params         : [String: AnyObject],
            header         : [String: String] ,
            success        : SuccessBlock?,
            failure        : FailureBlock?
            )
        {

            let url = Server_Url + api//.rawValue //+ "?nocache=\(arc4random())"
            print("\n\(url)")
            print(params)
            
            Alamofire.request(url, method: method, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { response in //.URLEncoding JSONEncoding
                switch response.result {
                case .failure(let error):
                    if let failure = failure {
                        print("\nAPI Call Failed!\nURL : \(url)\nError : \(error.localizedDescription)")
                        if let data = response.data {
                            let res = String(describing: data)
                            print("Response : \(res)")
                        }
                        failure(-999, "네트워크 장애로 인해 서비스 이용이 제한됩니다\n\(url)")
                    }
                    return
                case .success(let json):
                    var res = JSON(json)
                    print(res)
                    
                    let code = res[kMeta][kCode].intValue
                    let msg = res[kMeta][kMsg].stringValue
                    
                    if code == 0 {
                        do {
                            let data = try Net.parseResponse(api: api_type, data: res[kData], res: res)
                            if let success = success {
                                success(data)
                            }
                        } catch _ {
                            if let failure = failure {
                                failure(-900, "Failed to parse server response(invalid object)")
                            }
                        }
                    }  else {
                        if let failure = failure {
                            failure(code, msg)
                        }
                    }
                }
            }
        }
        
        /**
         *  HTTP request.  (json을 이용하여 body 요청)
         // parmas 가 Array<String> 일때
         */
        private static func doRequest2(
            method         : Alamofire.HTTPMethod,
            api            : String,
            api_type       : F_API,
            params         : Array<String>,
            header         : [String: String] = [:],
            success        : SuccessBlock?,
            failure        : FailureBlock?
            )
        {
            
            let urlString = Server_Url + api//.rawValue //+ "?nocache=\(arc4random())"
            print("\n\(urlString)")
            print(params)
       
            
            let url = URL(string: urlString)!
            let jsonData = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            
            Alamofire.request(request).responseJSON { response in //.URLEncoding JSONEncoding
                switch response.result {
                case .failure(let error):
                    if let failure = failure {
                        print("\nAPI Call Failed!\nURL : \(url)\nError : \(error.localizedDescription)")
                        if let data = response.data {
                            let res = String(describing: data)
                            print("Response : \(res)")
                        }
                        failure(-999, "네트워크 장애로 인해 서비스 이용이 제한됩니다\n\(url)")
                    }
                    return
                case .success(let json):
                    var res = JSON(json)
                    print(res)
                    
                    let code = res[kMeta][kCode].intValue
                    let msg = res[kMeta][kMsg].stringValue
                    
                    if code == 0 {
                        do {
                            let data = try Net.parseResponse(api: api_type, data: res[kData], res: res)
                            if let success = success {
                                success(data)
                            }
                        } catch _ {
                            if let failure = failure {
                                failure(-900, "Failed to parse server response(invalid object)")
                            }
                        }
                    } else {
                        if let failure = failure {
                            failure(code, msg)
                        }
                    }
                }
            }
        }
        
        
        /**
         *  HTTP request.  (json을 이용하여 body 요청)
         // parmas 가 Dicionary 일때
         */
        private static func doRequest3(
            method         : Alamofire.HTTPMethod,
            api            : String,
            api_type       : F_API,
            params         : NSDictionary,
            header         : [String: String] = [:],
            success        : SuccessBlock?,
            failure        : FailureBlock?
            )
        {
            
            let urlString = Server_Url + api//.rawValue //+ "?nocache=\(arc4random())"
            print("\n\(urlString)")
            
            print(params)
    
            let url = URL(string: urlString)!
            let jsonData = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.allHTTPHeaderFields = header
            request.httpBody = jsonData
            
            Alamofire.request(request).responseJSON { response in //.URLEncoding JSONEncoding
                switch response.result {
                case .failure(let error):
                    if let failure = failure {
                        print("\nAPI Call Failed!\nURL : \(url)\nError : \(error.localizedDescription)")
                        if let data = response.data {
                            let res = String(describing: data)
                            print("Response : \(res)")
                        }
                        failure(-999, "네트워크 장애로 인해 서비스 이용이 제한됩니다\n\(url)")
                    }
                    return
                case .success(let json):
                    var res = JSON(json)
                    print(res)
                    
                    let code = res[kMeta][kCode].intValue
                    let msg = res[kMeta][kMsg].stringValue
                    
                    if code == 0 {
                        do {
                            let data = try Net.parseResponse(api: api_type, data: res[kData], res: res)
                            if let success = success {
                                success(data)
                            }
                        } catch _ {
                            if let failure = failure {
                                failure(-900, "Failed to parse server response(invalid object)")
                            }
                        }
                    } else {
                        if let failure = failure {
                            failure(code, msg)
                        }
                    }
                }
            }
        }
        
        /**
         *  File request.
         */
        private static func doRequestForFile (
            method            : Alamofire.HTTPMethod,
            api               : F_API,
            imgArray          : [Data]! = [],
            imgMarkArray      : [String]! = [],
            imgMark           : String = "file",
            imgIndexable      : Bool = false,
            vidArray          : [Data]! = [],
            vidMarkArray      : [String]! = [],
            vidMark           : String = "video",
            vidIndexable      : Bool = false,
            params            : [String: AnyObject]?,
            success           : SuccessBlock?,
            failure           : FailureBlock?
            )
        {

            let url = Server_Url + api.rawValue//.rawValue + "?nocache=\(arc4random())"
            print(url)
            print(params ?? "params:")
            
            Alamofire.upload(multipartFormData: { (MultipartFormData) in
                if imgArray.count > 0 {
                    for i in 0...imgArray.count - 1 {
                        var strName = imgMark
                        if imgIndexable {
                            strName += "\(i + 1)"
                        }
                        if i < imgMarkArray.count {
                            strName = imgMarkArray[i]
                        }
                        MultipartFormData.append(imgArray[i], withName: strName, fileName: "\(strName).jpg", mimeType: "image/jpg")
                    }
                } else if vidArray.count > 0 {
                    for i in 0...vidArray.count - 1 {
                        var strName = vidMark
                        if vidIndexable {
                            strName += "\(i + 1)"
                        }
                        if i < vidMarkArray.count {
                            strName = vidMarkArray[i]
                        }
                        MultipartFormData.append(vidArray[i], withName: strName, fileName: "\(strName).mov", mimeType: "video/mov")
                    }
                }
                
                for (key, value) in params! {
                    let data = JSON(value).stringValue
                    MultipartFormData.append(data.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to: url, method: method, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .failure(let error):
                    if let failure = failure {
                        print("\nAPI Call Failed!\nURL : \(url)\nError : \(error.localizedDescription)")
                        failure(999, "서버와의 연결에 실패했습니다.")
                    }
                    
                case .success(let upload, _, _):
                    upload.responseString { response in
                        print(response.result.value ?? "")
                    }
                    
                    upload.responseJSON { response in
                        let json = JSON(response.result.value ?? "")
                        
                        let code = json[kMeta][kCode].intValue
                        let msg = json[kMeta][kMsg].stringValue
                        
                        if code == 0 {
                            do {
                                let data = try Net.parseResponse(api: api, data: json[kData], res: json)
                                if let success = success {
                                    success(data)
                                }
                            } catch _ {
                                if let failure = failure {
                                    failure(-900, "Failed to parse server response(invalid object)")
                                }
                            }
                        } else {
                            if let failure = failure {
                                failure(code, msg)
                            }
                        }
                    }
                }
                
            })
        }
        
        
        
        /**
         *  Parse API response data.
         */
        private static func parseResponse(api: F_API, data: JSON, res: JSON) throws -> ResponseResult? {
            var result: ResponseResult? = nil
            
            switch api {
            case .USER:
                let res = SingupResult()

                res.accessToken = data["accessToken"].stringValue
                res.usr = data["usr"]
                
                let list = data["addressList"]
                if list != .null {
                    for (_, json) in list {
                        res.addressList.append(UsrAddressDao(json))
                    }
                }
                
                result = res
                
            case .USER_LOGIN:  //회원로그인
                let res = UserLoginResult()
                
                res.accessToken = data["accessToken"].stringValue
                res.usr = data["usr"]
                let list = data["addressList"]
                if list != .null {
                    for (_, json) in list {
                        res.addressList.append(UsrAddressDao(json))
                    }
                }
                
                result = res
                
            case .BRAND_FAME_ALL: //관심데이터 얻기
                let res = AllFramBrandResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Brand(json))
                    }
                }
                result = res
                
            case .BRAND_LIST: //모든 브렌드 디테일 목록 얻기
                let res = AllBrandDetailListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        let info = Brand(json)
                        res.list.append(info)
                        var idx = index_eng.index(of: info.firstEn) ?? 0
                        res.list_en[idx].append(info)
                        idx = index_kor.index(of: info.firstKo) ?? 0
                        res.list_ko[idx].append(info)
                    }
                }
                result = res
                
            case .OTHER_LICENSE:  //약관 얻기
                let res = OtherLineseResult()
                
                res.content = data["content"].stringValue
                res.edtTime = data["edtTime"].intValue
                res.licenseUid = data["licenseUid"].intValue
                res.regTime = data["regTime"].intValue
                res.status = data["status"].intValue
                
                result = res

            case .OTHER_QNA: //1 : 1문의 목록 얻기
                let res = getQnaListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Qna(json))
                    }
                }
                result = res
                
            case .OTHER_FAQ: // Faq목록 얻기
                let res = getFaqListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Faq(json))
                    }
                }
                result = res
                
            case .OTHER_PROMOTION: // 프로모션 정보 얻기
                let res = getPromotionInfoResult()
                
                res.dicInfo = PromotionCodeDao(data)
                result = res
                
            case .OTHER_NOTICE: // 공지사항
                let res = getNoticeListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Notice(json))
                    }
                }
                result = res
            case .OTHER_EVENT: // 이벤트
                let res = getEventListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Event(json))
                    }
                }
                result = res
                
            case .OTHER_BANNER: // 배너 목록 얻기
                let res = getBannerListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(BannerDto(json))
                    }
                }
                result = res
               
            case .USER_MYINFO: // 회원 상세정보 얻기
                let res = MyDetailInfoResult()
                
                let address = data["addressList"]
                if address != .null {
                    for (_, json) in address {
                        res.addressList.append(UsrAddressDao(json))
                    }
                }

                res.countBuy = data["countBuy"].intValue
                res.countSell = data["countSell"].intValue
                res.countPdt = data["countPdt"].intValue
                res.countUnreadNotice = data["countUnreadNotice"].intValue
                
                let list = data["myPageEventList"]
                if list != .null {
                    for (_, json) in list {
                        res.myPageEventList.append(Event(json))
                    }
                }
                res.usrFollowerCount = data["usrFollowerCount"].intValue
                res.usrFollowingCount = data["usrFollowingCount"].intValue
                res.usr = data["usr"]
                
                result = res
                
            case .USER_DETAILINFO: // 기타 회원 상세정보 얻기
                let res = OtherUsrDetailInfoResult()
                res.usrDto = UsrInfoDto(data)
                result = res
                
            case .ALARM_COUNT: // 알림갯수 얻기
                let res = AlarmCountResult()
                
                res.dealCount  = data["dealCount"].intValue
                res.newsCount  = data["newsCount"].intValue
                res.replyCount = data["replyCount"].intValue
                res.totalCount = data["totalCount"].intValue
                
                result = res
                
            case .PDT_LIKE_UPDATESTATUS: // 좋아요 업데이트뱃지 표시여부
                let res = PdtLikeUpdateStatusResult()
                
                res.status  = data.boolValue
                
                result = res

            case .ALARM: // 알림목록 얻기
                let res = AlarmResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Alarm(json))
                    }
                }
                
                result = res
                
            case .HOME_FEED_STYLE: // 피드 스타일 얻기
                let res = FeedPdtResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .HOME_FAME: // 인기목록 얻기
                let res = FeedPdtResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .HOME_NEW: // 신상목록 얻기
                let res = FeedPdtResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .HOME_FEED: // 피드목록 얻기
                let res = FeedPdtResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .HOME_FAME_STYLE: // 인기 스타일목록 얻기
                let res = FeedPdtResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .HOME_NEW_STYLE: // 신상품 스타일목록 얻기
                let res = FeedPdtResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .USER_ALARMSETTING: // 알림 설정 얻기
                let res = UserAlarmResult()
                
                res.dealSetting = data["dealSetting"].stringValue
                res.dealYn = data["dealYn"].intValue
                res.newsSetting = data["newsSetting"].stringValue
                res.newsYn = data["newsYn"].intValue
                res.replySetting = data["replySetting"].stringValue
                res.replyYn = data["replyYn"].intValue
                res.usrUid = data["usrUid"].intValue
                
                result = res
                
            case .OTHER_USRWISH: // 찾는 아이템 목록 얻기
                let res = FindItemResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FindItemDto(json))
                    }
                }
                
                result = res
                
            case .USER_CARD: // 카드 목록 얻기
                let res = UsrCardResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrCardDto(json))
                    }
                }
                
                result = res
                
            case .COMMON_UPLOADS: // 여러개 파일 업로드 결과
                let res = UploadFilesResult()
                
                for i in 0..<data.count {
                    let img = data[i]["fileURL"].stringValue
                    res.fileUrls.append(img)
                    let imgnames = data[i]["fileName"].stringValue
                    res.fileNames.append(imgnames)
                }

                result = res

            case .COMMON_UPLOAD: // 한개 파일 업로드 결과
                let res = UploadFileResult()
                
                res.fileName  = data["fileName"].stringValue
                res.fileURL  = data["fileURL"].stringValue
                
                result = res

            case .USER_BLOCK: // 차단된 유저 목록 얻기 결과
                let res = BlockUsrResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(BlockUsrDto(json))
                    }
                }
                
                result = res

            case .USER_FOLLOWER: // 팔로워목록 얻기
                let res = BlockUsrResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(BlockUsrDto(json))
                    }
                }
                
                result = res

            case .USER_FOLLOWING: // 팔로잉목록 얻기
                let res = BlockUsrResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(BlockUsrDto(json))
                    }
                }
                
                result = res
                
            case .USER_SNSSTATUS: // sns 연동정보 얻기
                let res = UserSNSStatusResult()
                
                res.facekbook  = data["facekbook"].boolValue
                res.instagram  = data["instagram"].boolValue
                res.kakaoTalk  = data["kakaoTalk"].boolValue
                res.naver  = data["naver"].boolValue
                res.twitter  = data["twitter"].boolValue
                
                result = res
                
            case .DETAIL_PDTSTYLE: // 스타일 팝업 상셍
                let res = DetailPdtStyleResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtStyleDao(json))
                    }
                }
                
                result = res
                
            case .GET_PDTSTYLE: // 상품 스타일 목록 얻기
                let res = GetPdtStyleListResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .GET_PDTSTYLE_ALL: // 전체 상품 스타일 목록 얻기
                let res = GetAllPdtStyleListResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .PDT_LIKE: // 좋아요 목록 얻기
                let res = GetLikeListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .PDT_LIKE_STYLE: // 좋아요 스타일 목록 얻기
                let res = GetLikeStyleResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(FeedPdtDto(json))
                    }
                }
                
                result = res
                
            case .BRAND_LIKE: // 팔로우 브렌드 목록 얻기
                let res = BrandLikeResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        let info = Brand(json)
                        res.list.append(info)
                        var idx = index_eng.index(of: info.firstEn) ?? 0
                        res.list_en[idx].append(info)
                        idx = index_kor.index(of: info.firstKo) ?? 0
                        res.list_ko[idx].append(info)
                    }
                }
                
                result = res
                
            case .BRAND: // 모든 부렌드 목록 얻기
                let res = AllBrandResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        let info = BrandMiniDto(json)
                        res.list.append(info)
                        var idx = index_eng.index(of: info.firstEn) ?? 0
                        res.list_en[idx].append(info)
                        idx = index_kor.index(of: info.firstKo) ?? 0
                        res.list_ko[idx].append(info)
                    }
                }
                
                result = res
                
            case .BRAND_FAME: // 인기 브렌드 목록 얻기(10개)
                let res = FameBrandResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(Brand(json))
                    }
                }
                
                result = res

            case .BRAND_DETAIL: // 브렌드 상세보기
                let res = BrandDetailResult()
                
                res.backImg  = data["backImg"].stringValue
                res.brandLikeCount  = data["brandLikeCount"].intValue
                res.brandLikeStatus  = data["brandLikeStatus"].boolValue
                res.brandUid  = data["brandUid"].intValue
                res.femalePdtCount  = data["femalePdtCount"].intValue
                res.firstEn  = data["firstEn"].stringValue
                res.firstKo  = data["firstKo"].stringValue
                res.kidsPdtCount  = data["kidsPdtCount"].intValue
                res.malePdtCount  = data["malePdtCount"].intValue
                res.licenseUrl  = data["licenseUrl"].stringValue
                res.logoImg  = data["logoImg"].stringValue
                res.nameEn  = data["nameEn"].stringValue
                res.nameKo  = data["nameKo"].stringValue
                res.profileImg  = data["profileImg"].stringValue
                res.stylePdtCount  = data["stylePdtCount"].intValue
                
                result = res

            case .BRAND_DETAIL_LIST: // 브렌드 상품군에 따르는 상품목록 얻기
                let res = AllBrandPdtListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .THEME_RECOMMENDED: // 테마 리스트 얻기
                let res = ThemeListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(ThemeListDto(json))
                    }
                }
                
                result = res
                
            case .THEME_DETAIL: // 테마 상세
                let res = ThemeDetailResult()
                res.themeDeto =  ThemeListDto(data)
                
                result = res
                
            case .BRAND_RECOMMENDED: // 추천 브렌드 목록 얻기
                let res = RecommendBrandResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(BrandMiniDto(json))
                    }
                }
                
                result = res
                
            case .PDT_DETAIL: // 상품 상세
                let res = getPdtDetailInfoResult()
                
                res.dicInfo = PdtDetailDto(data)
                
                result = res
                
                
            case .PDT_RECOMMENDED: // 상품상세 - 추천상품 목록 얻기
                let res = getRecommendPdtListResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .PDT_RELATIONSTYLE: // 상품상세 - 연관스타일 목록 얻기
                let res = getRelationStyleListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res

            case .PDT_FAKE: // 상품 가품신고정보 얻기
                let res = getPdtFakeInfoResult()
                
                res.reward  = data["reward"].intValue
                res.totalCount  = data["totalCount"].intValue
                
                result = res
                
            case .PDT_WISH_LIST: // 상품상세 - 잇템 유저 목록 얻기
                let res = getPdtItemListResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrFollowListDto(json))
                    }
                }
                
                result = res
                
            case .PDT_LIKE_LIST: // 좋아요 유저목록 얻기
                let res = getLikeUsrListResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrFollowListDto(json))
                    }
                }
                
                result = res

            case .PDT_REPLY_LIST: // 댓글 목록 얻기
                let res = getPdtReplyListResult()
                
                res.last  = data["last"].boolValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(ReplyDto(json))
                    }
                }
                
                result = res
                
            case .PDT_REPLY_WRITER: // 댓글 작성
                let res = setPdtReplyResult()
                res.replyDto  = ReplyDto(data)
                result = res

            case .PDT_REPLY_USR_LIST: // 상품 댓글가능 회원목록얻기
                let res = getPdtReplyUsrListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrMiniDto(json))
                    }
                }
                
                result = res
                
            case .DEAL_NEGO: // 네고 신청 결과
                let res = reqDealNegoResult()
                res.dealDao  = DealDao(data)
                result = res
                
            case .DEAL_BUY: // 상품 구매 결과
                let res = reqDealBuyResult()
                res.dealDao  = DealDao(data["deal"])
                res.paymentUrl  = data["paymentUrl"].stringValue
                result = res

            case .PDT_MODIFIY: // 상품 정보 수정 결과
                let res = reqpdtModifyResult()
                res.dicInfo  = PdtMiniDto(data)
                
                result = res

            case .OTHER_DETAIL_NOTICE: // 공지사항 상세 얻기 결과
                let res = getNoticeDetailInfoResult()
                res.dicInfo  = Notice(data)
                
                result = res

            case .OTHER_DETAIL_EVENT: // 이벤트 상세 얻기 결과
                let res = getEventDetailInfoResult()
                res.dicInfo  = Event(data)
                
                result = res

            case .OTHER_SETTING: // 시스템 설정 정보 얻기
                let res = getSystemSettingInfoResult()
                res.dicInfo  = SystemSettingDto(data)
                
                result = res

            case .CATEGORY_SIZE: // 사이즈 목록 얻기
                let res = getSizeListResult()
                res.dicInfo  = SizeRefDto(data)
                
                result = res
                
            case .BRAND_MODELNAMES: // 브랜드에 따르는 추천모델명 리스트 얻기
                let res = getBrandModelNmsListResult()
                
                for i in 0..<data.count {
                    let name = data[i].stringValue
                    res.list.append(name)
                }
                result = res

            case .CATEGORY_SUB: // 하위 카테고리 목록 얻기 결과
                let res = getSubCategoryListResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(CategoryDao(json))
                    }
                }
                
                result = res
                
            case .CATEGORY_DETAIL: // 카테고리별 인기브랜드 및 하위카테고리 얻기 결과
                let res = getCategoryDetailListResult()
                
                let list1 = data["childrenCategories"]
                if list1 != .null {
                    for (_, json) in list1 {
                        res.arrCategory.append(CategoryDao(json))
                    }
                }
                
                let list2 = data["recommendedBrand"]
                if list2 != .null {
                    for (_, json) in list2 {
                        res.arrBrand.append(BrandRecommendedListDto(json))
                    }
                }
                res.dicCategory = CategoryDao(data["category"])
                
                result = res
                
            case .CATEGORY_PDT: // 해당 카테고리에 속하는 상품목록얻기(브랜드기준)
                let res = getCategoryPdtListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                result = res
                
            case .PDT: // 상품 등록 결과
                let res = regPdtInfoResult()
                res.dicPdtInfo = PdtDetailDto(data)
                result = res
                
            case .USER_PDT: // 회원의 아이템목록 얻기 결과
                let res = getUsrPdtListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .USER_STYLE: // 회원의 스타일 목록 얻기 결과
                let res = getUsrPdtListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .USER_WISH: // 회원의 잇템 목록 얻기 결과
                let res = getUsrPdtListResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                
                result = res
                
            case .USER_REVIEW: // 매너포인트 변경이력(거래후기) 얻기 결과
                let res = getUsrReviewResult()
                
                res.countOnePoint  = data["countOnePoint"].intValue
                res.countTwoPoint  = data["countTwoPoint"].intValue
                res.countZeroPoint  = data["countZeroPoint"].intValue
                
                res.last  = data["reviews"]["last"].boolValue
                res.totalElements  = data["reviews"]["totalElements"].intValue
                let list = data["reviews"]["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(ReviewListDto(json))
                    }
                }
                
                result = res
                
            case .BRAND_REG: // 브렌드 등록
                let res = regBrandResult()
                
                res.dicInfo  = BrandMiniDto(data)
               
                result = res
                
            case .USER_INVITEINFO: // 초대정보 얻기
                let res = InviteInfoResult()
                
                res.boughtCount  = data["boughtCount"].intValue
                res.inviteCode  = data["inviteCode"].stringValue
                res.invitedCount  = data["invitedCount"].intValue
                res.invitedMonthCount  = data["invitedMonthCount"].intValue
                result = res

            case .USER_FRIEND_ADDRESSBOOK: // 연락처 친구찾기
                let res = getFriendAddressResult()
               
                let list = data["joinedList"]
                if list != .null {
                    for (_, json) in list {
                        res.joinedList.append(UsrFollowListDto(json))
                    }
                }
                
                let list1 = data["unJoinedPhoneList"]
                if list1 != .null {
                    for (_, json) in list1 {
                        res.unJoinedPhoneList.append(json.stringValue)
                    }
                }
                
                result = res
                
            case .USER_FRIEND_FACEBOOK: // facebook 친구찾기
                let res = getFriendFacebookResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrFollowListDto(json))
                    }
                }
              
                result = res

            case .USER_HISTORY_MONEY: // 셀럽머니 변경이력 얻기
                let res = getMoneyHistoryResult()
                
                res.cashbackMoney = data["cashbackMoney"].intValue
                res.money = data["money"].intValue
                
                res.last  = data["history"]["last"].boolValue
                res.totalElements  = data["history"]["totalElements"].intValue

                let list = data["history"]["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrMoneyHisDto(json))
                    }
                }
                
                result = res
                
            case .DEAL_DETAIL: // 주문상세 결과
                let res = getDealDetailResult()
                res.dealDetail = DealDetailDto(data)
                result = res

            case .USER_HISTORY_BUY: // 회원구매내역 얻기 결과
                let res = getBuyHistoryResult()
                
                res.completedCount = data["completedCount"].intValue
                res.negoCount = data["negoCount"].intValue
                res.preparedCount = data["preparedCount"].intValue
                res.progressCount = data["progressCount"].intValue
                res.sentCount = data["sentCount"].intValue
                res.verifiedCount = data["verifiedCount"].intValue
                
                res.last  = data["history"]["last"].boolValue
                res.totalElements  = data["history"]["totalElements"].intValue
                
                let list = data["history"]["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(DealListDto(json))
                    }
                }
                result = res
                
            case .USER_HISTORY_SELL: // 회원판매내역 얻기 결과
                let res = getSellHistoryResult()
                
                res.cachedCount = data["cachedCount"].intValue
                res.completedCount = data["completedCount"].intValue
                res.negoCount = data["negoCount"].intValue
                res.preparedCount = data["preparedCount"].intValue
                res.progressCount = data["progressCount"].intValue
                res.sentCount = data["sentCount"].intValue
                
                res.last  = data["history"]["last"].boolValue
                res.totalElements  = data["history"]["totalElements"].intValue
                
                let list = data["history"]["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(DealListDto(json))
                    }
                }
                result = res
                
            case .DEAL_REFUND_DISALLOW: // 판매자 반품 거절 결과
                let res = reqRefundDisallowResult()
                res.deal  = DealDao(data)
                result = res

            case .DEAL_REFUND_COMPLETE: // 판매자 반품 확인 결과
                let res = reqRefundCompleteResult()
                res.deal  = DealDao(data)
                result = res

            case .DEAL_REFUND: // 구매자 반품 신청 결과
                let res = reqDealRefundResult()
                res.deal  = DealDao(data)
                result = res

            case .DEAL_NEGO_ALLOW_COUNTER: // 구매자 카운터 신청 결과
                let res = reqDealNegoAllowCounterResult()
                res.deal  = DealDao(data)
                result = res

            case .DEAL_NEGO_ALLOW: // 판매자 네고 승인 결과
                let res = reqDealNegoAllowResult()
                res.deal  = DealDao(data)
                result = res
                
            case .DEAL_DELIVERY_NUMBER: // 판매자 운송장 번호등록
                let res = reqDealDeliveryNumResult()
                res.deal  = DealDao(data)
                result = res

            case .DEAL_DELIVERY: // 택베예약하기
                let res = reqDealDeliveryResult()
                res.deal  = DeliveryHistoryDao(data)
                result = res
                
            case .DEAL_COMPLETE: // 구매자 거래 확정
                let res = reqDealCompleteResult()
                res.deal  = DealDao(data)
                result = res

            case .DEAL_CANCEL_SELLER: // 판매자 판매가능
                let res = reqDealCancelSellerResult()
                res.deal  = DealDao(data)
                result = res
                
            case .DEAL_CANCEL: // 구매자 주문취소
                let res = reqDealCancelResult()
                res.deal  = DealDao(data)
                result = res

            case .SEARCH: // 검색결과
                let res = SearchResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                result = res
                
            case .SEARCH_COUNT: // 검색결과 갯수
                let res = SearchCountResult()
                
                res.count  = data.intValue
                result = res
                
            case .SEARCH_DETAIL: // 상세검색
                let res = getSearchDetailResult()
                
                res.last  = data["last"].boolValue
                res.totalElements  = data["totalElements"].intValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(PdtListDto(json))
                    }
                }
                result = res
                
            case .SEARCH_FAME: // 인기검색 키워드 리스트 얻기
                let res = getSearchFameResult()
                
                let list = data
                if list != .null {
                    for (_, json) in list {
                        res.list.append(SearchWordDto(json))
                    }
                }
                result = res
                
            case .SEARCH_PRICERANGE: // 인기검색 키워드 리스트 얻기
                let res = getSearchPriceRangeResult()
                res.min = data["min"].intValue
                res.max = data["max"].intValue
                result = res
                
            case .SEARCH_TAG: // 태그검색 결과
                let res = getSearchTagResult()
                
                for i in 0..<data.count {
                    let str = data[i].stringValue
                    res.list.append(str)
                }
                result = res
                
            case .SEARCH_RECOMMEND: // 추천검색 키워드 리스트 얻기
                let res = getSearchRecommendResult()
                res.searchDto = RecommendSearchDto(data)

                result = res
                
            case .USER_RECOMMEND: // 추천유저목록
                let res = getRecommendUsrListResult()
                
                res.last  = data["last"].boolValue
                
                let list = data["content"]
                if list != .null {
                    for (_, json) in list {
                        res.list.append(UsrRecommendDto(json))
                    }
                }
                result = res
                
            case .OTHER_DELAYWORKINGDAY: //
                let res = ggetWorkingDayResult()
                res.targetDate = data["targetDate"].stringValue
                
                result = res
                
            case .OTHER_DELIVERY_COMPANIES: //
                let res = ggetDeliveryCompaniesResult()
                
                let list = data["companies"]
                for i in 0..<list.count {
                    let str = list[i].stringValue
                    res.companies.append(str)
                }
                
                result = res
                
            case .DEAL_DELIVERY_INFO: //
                let res = getDealDeliveryInfoResult()
                res.dealHisDao = DeliveryHistoryDao(data)
                
                result = res
                
            default:
                break
            }
            
            return result
        }
        
        //
        // MARK: API call functions
        //
        
      /**
         *    회원가입
         */
        open static func UsrSingup(
            birthday            : String,
            gender              : Int,
            inviteCode          : String,
            likeBrands          : Array<String>,
            loginType           : String,
            password            : String,
            snsId               : String,
            snsProfileImg       : String,
            usrId               : String,
            usrMail             : String,
            usrNckNm            : String,
            usrNm               : String,
            usrPhone            : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let login_url  = F_API.USER.rawValue
            let params = [
                "birthday"              : birthday,
                "gender"                : gender,
                "inviteCode"            : inviteCode,
                "likeBrands"            : likeBrands,
                "loginType"             : loginType,
                "password"              : password,
                "snsId"                 : snsId,
                "snsProfileImg"         : snsProfileImg,
                "usrId"                 : usrId,
                "usrMail"               : usrMail,
                "usrNckNm"              : usrNckNm,
                "usrNm"                 : usrNm,
                "usrPhone"              : usrPhone,
                
                ] as NSDictionary
            
            doRequest3(method: .put, api: login_url, api_type: .USER, params: params,header:[:], success: success, failure: failure)
        }
        
        /**
         *    회원로그인
         */
        open static func UserLogin(
            loginType           : String,
            osTp                : String,
            password            : String,
            usrId               : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_LOGIN.rawValue
            let params = [
                "loginType"             : loginType,
                "osTp"                  : osTp,
                "password"              : password,
                "usrId"                 : usrId,
                ] as NSDictionary
            
            doRequest3(method: .post, api: _url, api_type: .USER_LOGIN, params: params,header:[:], success: success, failure: failure)
        }
  
       /**
         *    아이디 중복검사
         */
        open static func CheckUsrid(
            userid              : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CHECK_USRID.rawValue + "?usrId=" + userid

            doRequest(method: .get, api: _url, api_type: .USER_CHECK_USRID, params: [:],header:[:], success: success, failure: failure)
        }
        
      /**
         *    이메일 중복검사
         */
        open static func CheckUsrMail(
            usrMail             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CHECK_USRMAIL.rawValue + "?usrMail=" + usrMail
            
            doRequest(method: .get, api: _url, api_type: .USER_CHECK_USRMAIL, params: [:],header:[:], success: success, failure: failure)
        }
        
        /**
         *   인기 브렌드 목록 얻기(전체)
         */
        open static func getBrandFramAll(
            pdtGroupType        : String,
            token               : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_FAME_ALL.rawValue + "?pdtGroupType=" + pdtGroupType
            
            let header = [
                "accessToken"   : token,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_FAME_ALL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    회훤탈퇴
         */
        open static func userExit(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .USER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    비밀번호 찾기
         */
        open static func UserPassFind(
            email               : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_PASSWORD_FIND.rawValue
            let params = [
                "content"             : email,
                ] as NSDictionary
            
            doRequest3(method: .post, api: _url, api_type: .USER_PASSWORD_FIND, params: params,header:[:], success: success, failure: failure)
        }
        
       /**
         *    약관 얻기
         */
        open static func OtherLincese(
            accessToken         : String,
            licenseType         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_LICENSE.rawValue + "?licenseType=" + licenseType
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_LICENSE, params: [:],header:header, success: success, failure: failure)
        }
        
       /**
         *  1:1  문의 목록 얻기
         */
        open static func getQnaList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_QNA.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_QNA, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    1:1문의목록 작성
         */
        open static func reqQna(
            title               : String,
            content             : String,
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_QNA.rawValue
            let params = [
                "title"             : title,
                "content"           : content,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .OTHER_QNA, params: params,header:header, success: success, failure: failure)
        }
        
       /**
         *  Faq 목록 얻기
         */
        open static func getFaqList(
            accessToken         : String,
            faqType             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_FAQ.rawValue + "?faqType=" + faqType
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_FAQ, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  공지사항  목록 얻기
         */
        open static func getNoticeList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_NOTICE.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_NOTICE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  프로모션 정보 얻기
         */
        open static func getPromotionInfo(
            accessToken         : String,
            brandUid            : Int,
            categoryUid         : Int,
            dealType            : String,
            pdtUid              : Int,
            promotionCode       : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_PROMOTION.rawValue
            let params = [
                "brandUid"      : String(brandUid),
                "categoryUid"   : String(categoryUid),
                "dealType"      : dealType,
                "pdtUid"        : String(pdtUid),
                "promotionCode" : promotionCode,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .OTHER_PROMOTION, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  이벤트 목록 얻기
         */
        open static func getEventList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_EVENT.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_EVENT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
           *  배너목록 얻기
          */
        open static func getBannerList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_BANNER.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_BANNER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  회원 상세정보 얻기
         */
        open static func getMyDetailInfo(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_MYINFO.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_MYINFO, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  기타 회원 상세정보 얻기
         */
        open static func getOtherUsrDetailInfo(
            accessToken         : String,
            usrUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_DETAILINFO, params: [:],header:header, success: success, failure: failure)
        }
        
       /**
         *  알림 갯수 얻기
         */
        open static func getAlarmCnt(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.ALARM_COUNT.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .ALARM_COUNT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 알림 목록 얻기
         */
        open static func getAlarmList(
            kind                : String,
            page                : Int,
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.ALARM.rawValue + "?kind=" + kind + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .ALARM, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  좋아요 업데이트뱃지 표시여부
         */
        open static func getLikeUpdateStatus(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT_LIKE_UPDATESTATUS.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_LIKE_UPDATESTATUS, params: [:],header:header, success: success, failure: failure)
        }
        
       /**
         *  피드목록 얻기
         */
        open static func getHomeFeed(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.HOME_FEED.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .HOME_FEED, params: [:],header:header, success: success, failure: failure)
        }
        /**
         * 피드스타일 목록얻기
         */
        open static func getHomeFeedStyle(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.HOME_FEED_STYLE.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .HOME_FEED_STYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  인기목록 얻기
         */
        open static func getHomeFame(
            accessToken         : String,
            page                : Int,
            seed                : CLong,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.HOME_FAME.rawValue + "?page=" + String(page) + "&seed=" + String(seed)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .HOME_FAME, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 인기스타일 목록얻기
         */
        open static func getHomeFameStyle(
            accessToken         : String,
            page                : Int,
            seed                : CLong,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.HOME_FAME_STYLE.rawValue + "?page=" + String(page) + "&seed=" + String(seed)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .HOME_FAME_STYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  신상품목록 얻기
         */
        open static func getHomeNew(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.HOME_NEW.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .HOME_FEED, params: [:],header:header, success: success, failure: failure)
        }
        /**
         * 신상품 스타일 목록얻기
         */
        open static func getHomeNewStyle(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.HOME_NEW_STYLE.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .HOME_NEW_STYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 알림설정 얻기
         */
        open static func getAlarmSettingInfo(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_ALARMSETTING.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_ALARMSETTING, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 알림설정 업데이트
         */
        open static func reqAlarmSetting(
            accessToken         : String,
            dealSetting         : String,
            dealYn              : String,
            newsSetting         : String,
            newsYn              : String,
            replySetting        : String,
            replyYn             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_ALARMSETTING.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "dealSetting"           : dealSetting,
                "dealYn"                : dealYn,
                "newsSetting"           : newsSetting,
                "newsYn"                : newsYn,
                "replySetting"          : replySetting,
                "replyYn"               : replyYn,
                
                ] as NSDictionary
            
            doRequest3(method: .post, api: _url, api_type: .USER_ALARMSETTING, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 찾는 아이팀 목록 얻기
         */
        open static func getUsrWishList(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_USRWISH.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_USRWISH, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 찾는 상품 알림 설정
         */
        open static func updatePdtAlarm(
            accessToken         : String,
            brandUid            : Int,
            categoryUid         : Int,
            colorName           : String,
            pdtGroupType        : String,
            pdtModel            : String,
            pdtSize             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_USRWISH.rawValue
            
            let params = [
                "brandUid"             : String(brandUid),
                "categoryUid"          : String(categoryUid),
                "colorName"            : colorName,
                "pdtGroupType"         : pdtGroupType,
                "pdtModel"             : pdtModel,
                "pdtSize"              : pdtSize,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .OTHER_USRWISH1, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *   찾는 아이템 삭제
         */
        open static func delFindItem(
            uids                : Array<String>,
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_USRWISHS.rawValue
            let params = [
                "uids"             : uids,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .delete, api: _url, api_type: .OTHER_USRWISHS, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 판매자 설정
         */
        open static func reqSellerSetting(
            accessToken         : String,
            freeSendPrice       : String,
            isFreeSend          : Bool,
            timeOutDate         : String,
            isSleep             : Bool,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_SELLERSETTING.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "freeSendPrice"         : freeSendPrice,
                "isFreeSend"            : isFreeSend,
                "timeOutDate"           : timeOutDate,
                "isSleep"               : isSleep,
                ] as NSDictionary
            
            doRequest3(method: .post, api: _url, api_type: .USER_SELLERSETTING, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 계좌정보 업데이트
         */
        open static func reqAccountUpdate(
            accessToken         : String,
            accountNum          : String,
            bankNm              : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_ACCOUNTINFO.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "accountNum"         : accountNum,
                "bankNm"             : bankNm,
                ] as NSDictionary
            
            doRequest3(method: .post, api: _url, api_type: .USER_ACCOUNTINFO, params: params,header:header, success: success, failure: failure)
        }
        
        /**
          * 카드 목록 얻기
          */
        open static func getCardList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CARD.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_CARD, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 카드 등록
         */
        open static func regCard(
            accessToken         : String,
            cardNumber          : String,
            kind                : Int,
            password            : String,
            validDate           : String,
            validNum            : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CARD.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "cardNumber"         : cardNumber,
                "kind"               : String(kind),
                "password"           : password,
                "validDate"          : validDate,
                "validNum"           : validNum,
                ] as NSDictionary
            
            doRequest3(method: .put, api: _url, api_type: .USER_CARD, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 카드 선택
         */
        open static func selCard(
            accessToken         : String,
            usrCardUid          : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CARD.rawValue + "/" + String(usrCardUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .USER_CARD, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 카드 삭제
         */
        open static func delCard(
            accessToken         : String,
            usrCardUid          : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CARD.rawValue + "/" + String(usrCardUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .USER_CARD, params: [:],header:header, success: success, failure: failure)
        }
        
        
        /**
         * 비밀번호 변경
         */
        open static func changePass(
            accessToken         : String,
            oldPassword         : String,
            newPassword         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_PASSWORD.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "newPassword"         : newPassword,
                "oldPassword"         : oldPassword,
                ] as NSDictionary

            doRequest3(method: .post, api: _url, api_type: .USER_PASSWORD, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 프로필 이미지 업데이트
         */
        open static func updateProfile(
            accessToken         : String,
            filename            : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_PROFILEIMG.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "content"         : filename,
                ] as NSDictionary
            
            doRequest3(method: .post, api: _url, api_type: .USER_PROFILEIMG, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 차단된 유저 목록 얻기
         */
        open static func getBlockUsrList(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_BLOCK.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_BLOCK, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 팔로워 목록 얻기
         */
        open static func getFollowerList(
            accessToken         : String,
            usrUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/follower?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_FOLLOWER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 팔로잉 목록 얻기
         */
        open static func getFollowingList(
            accessToken         : String,
            usrUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/following?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_FOLLOWING, params: [:],header:header, success: success, failure: failure)
        }
        
         /**
            *    한개 파일 업로드
            */
            open static func uploadFile(

                file               : Data,
                success            : SuccessBlock?,
                failure            : FailureBlock?
                )
            {
                doRequestForFile(method: .post, api: .COMMON_UPLOAD, imgArray: [file], imgMark: "uploadfile", params: [:], success: success, failure: failure)
            }
        
        /**
          *    여러개 파일 업로드
          */
         open static func uploadFiles(
            file               : [Data],
            success            : SuccessBlock?,
            failure            : FailureBlock?
            )
         {
            doRequestForFile(method: .post, api: .COMMON_UPLOADS, imgArray: file, imgMark: "uploadfiles", params: [:], success: success, failure: failure)
        }

        /**
         * 유저 팔로우 하기
         */
        open static func reqFollow(
            accessToken         : String,
            usrUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/follow"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .USER_FOLLOW, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 유저 팔로우 해제하기
         */
        open static func delFollow(
            accessToken         : String,
            usrUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/follow"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .USER_FOLLOW, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    프로필 업데이트
         */
        open static func updateUserInfo(
            accessToken              : String,
            description              : String,
            isChildren               : Bool,
            isFemale                 : Bool,
            isMale                   : Bool,
            profileBackImg           : String,
            profileImg               : String,
            usrNckNm                 : String,
            success                  : SuccessBlock?,
            failure                  : FailureBlock?
            )
        {
            let _url  = F_API.USER_PROFILE.rawValue
            let params = [
                "description"           : description,
                "isChildren"            : isChildren,
                "isFemale"              : isFemale,
                "isMale"                : isMale,
                "profileBackImg"         : profileBackImg,
                "profileImg"            : profileImg,
                "usrNckNm"              : usrNckNm,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            
            doRequest3(method: .post, api: _url, api_type: .USER_PROFILE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    회원기본 정보 수정
         */
        open static func updateMainUserInfo(
            accessToken              : String,
            birthday                 : String,
            gender                   : Int,
            usrNm                    : String,
            usrPhone                 : String,
            success                  : SuccessBlock?,
            failure                  : FailureBlock?
            )
        {
            let _url  = F_API.USER_PROFILE.rawValue
            let params = [
                "birthday"           : birthday,
                "gender"             : String(gender),
                "usrNm"              : usrNm,
                "usrPhone"           : usrPhone,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            
            doRequest3(method: .post, api: _url, api_type: .USER_PROFILE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * SNS 연동정보 얻기
         */
        open static func getSNSStatusInfo(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_SNSSTATUS.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_SNSSTATUS, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 좋아요 하기
         */
        open static func setPdtLike(
            accessToken         : String,
            pdtUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .PDT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 좋아요 취소하기
         */
        open static func delPdtLike(
            accessToken         : String,
            pdtUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .PDT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 스타일 팝업 상세
         */
        open static func getPdtStyleDetailInfo(
            accessToken         : String,
            pdtStyleUid         : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.GET_PDTSTYLE.rawValue + "/" + String(pdtStyleUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .DETAIL_PDTSTYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 스타일 삭제
         */
        open static func delPdtStyle(
            accessToken         : String,
            pdtStyleUid         : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.GET_PDTSTYLE.rawValue + "/" + String(pdtStyleUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .DEL_PDTSTYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 스타일 목록 얻기
         */
        open static func getPdtStyleList(
            accessToken         : String,
            pdtGroup            : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.GET_PDTSTYLE.rawValue + "?pdtGroup=" + pdtGroup + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .GET_PDTSTYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 전체 상품 스타일 목록 얻기
         */
        open static func getAllPdtStyleList(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.GET_PDTSTYLE_ALL.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .GET_PDTSTYLE_ALL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 좋아요 목록 얻기
         */
        open static func getLikeList(
            accessToken         : String,
            sale                : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT_LIKE.rawValue + "?sale=" + String(sale) + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_LIKE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 좋아요 스타일 목록 얻기
         */
        open static func getLikeStyleList(
            accessToken         : String,
            sale                : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT_LIKE_STYLE.rawValue + "?sale=" + String(sale) + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_LIKE_STYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 모든 브렌드 목록 얻기
         */
        open static func getAllBrandList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 등록
         */
        open static func regBrand(
            accessToken         : String,
            brandName           : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "?brandName=" + brandName
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .BRAND_REG, params: [:],header:header, success: success, failure: failure)
        }
        
        
        /**
         * 팔로우 브렌드 목록 얻기
         */
        open static func getFollowBrandList(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_LIKE.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_LIKE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 멀티 브렌드  팔로우 하기
         */
        open static func reqFollowBrand(
            accessToken         : String,
            uids                : Array<String>,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_LIKE.rawValue
            
            let params = [
                "uids"              : uids,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .BRAND_LIKE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 멀티 브렌드 취소하기
         */
        open static func delFollowBrand(
            accessToken         : String,
            uids                : Array<String>,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_LIKE.rawValue
            
            let params = [
                "uids"              : uids,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .delete, api: _url, api_type: .BRAND_LIKE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  브렌드 한개 팔로우 하기
         */
        open static func reqOneBrandFollow(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .BRAND_FAME, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  브렌드 한개 팔로우 해제하기
         */
        open static func delOneBrandFollow(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .BRAND_FAME, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 인기 브렌드 목록 얻기(10개)
         */
        open static func getFameBrandList(
            accessToken         : String,
            pdtGroupType        : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_FAME.rawValue + "?pdtGroupType=" + pdtGroupType
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_FAME, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 모든 브랜드 디테일 목록얻기
         */
        open static func getAllBrandDetailList(
            accessToken         : String,
            column              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_LIST.rawValue + "?column=" + String(column)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_LIST, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 팔로우 취소하기
         */
        open static func delBrandFollow(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .BRAND, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 팔로우 하기
         */
        open static func setBrandFollow(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .BRAND, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 상세 보기
         */
        open static func getBrandDetail(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_DETAIL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 상품군에 따르는 목록 얻기
         */
        open static func getBrandDetailPdtList(
            accessToken         : String,
            brandUid            : Int,
            tab                 : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/list?tab=" + tab + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_DETAIL_LIST, params: [:],header:header, success: success, failure: failure)
        }
        /**
         * 테마 목록 얻기
         */
        open static func getThemeList(
            accessToken         : String,
            pdtGroupType        : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.THEME_RECOMMENDED.rawValue + "?pdtGroup=" + pdtGroupType
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .THEME_RECOMMENDED, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 테마 상세
         */
        open static func getThemeDetail(
            accessToken         : String,
            themeUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.THEME_DETAIL.rawValue + "/" + String(themeUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .THEME_DETAIL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 추천 브렌드 목록 얻기
         */
        open static func getRecommendBrandList(
            accessToken         : String,
            pdtGroupType        : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND_RECOMMENDED.rawValue + "?pdtGroup=" + pdtGroupType
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_RECOMMENDED, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 상세
         */
        open static func getPdtDetailInfo(
            accessToken         : String,
            pdtUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_DETAIL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 삭제
         */
        open static func delPdtInfo(
            accessToken         : String,
            pdtUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .PDT_DETAIL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품상세  추천상품 목록 얻기
         */
        open static func getRecommendPdtList(
            accessToken         : String,
            pdtUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/recommended?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_RECOMMENDED, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품잇템 추가하기
         */
        open static func addPdtItem(
            accessToken         : String,
            pdtUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/wish"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .PDT_WISH, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품잇템 목록 얻기
         */
        open static func getPdtItemList(
            accessToken         : String,
            pdtUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/wish?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_WISH_LIST, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 가품신고 정보 얻기
         */
        open static func getPdtFakeInfo(
            accessToken         : String,
            pdtUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/fake"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_FAKE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    상품 가품신고 하기
         */
        open static func reqPdtFakeInfo(
            accessToken         : String,
            pdtUid              : Int,
            content             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/fake"
            let params = [
                "content"              : content,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]

            doRequest3(method: .put, api: _url, api_type: .PDT_FAKE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    신고 하기
         */
        open static func reqReport(
            accessToken         : String,
            targetUid           : Int,
            kind                : Int,
            content             : String,
            reportType          : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_REPORT.rawValue
            let params = [
                "content"              : content,
                "kind"                 : String(kind),
                "reportType"           : reportType,
                "targetUid"            : String(targetUid),
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .OTHER_REPORT, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 상품상세  연관스타일 목록 얻기
         */
        open static func getRelationStyleList(
            accessToken         : String,
            pdtUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/relationStyle?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_RELATIONSTYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 좋아요 유저목록 얻기
         */
        open static func getLikeUsrList(
            accessToken         : String,
            pdtUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/like?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_LIKE_LIST, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 댓글목록 얻기
         */
        open static func getPdtReplyList(
            accessToken         : String,
            pdtUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/reply?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_REPLY_LIST, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 상품 댓글가능 회원목록얻기
         */
        open static func getPdtReplyUsrList(
            accessToken         : String,
            pdtUid              : Int,
            keyword             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/reply/search?keyword=" + keyword
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .PDT_REPLY_USR_LIST, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 댓글목록 작성
         */
        open static func reqPdtReplyInfo(
            accessToken         : String,
            pdtUid              : Int,
            content             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid) + "/reply"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            let params = [
                "content"              : content,
                ] as NSDictionary
            
            
            doRequest3(method: .put, api: _url, api_type: .PDT_REPLY_WRITER, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 댓글 삭제
         */
        open static func delPdtReplyInfo(
            accessToken         : String,
            replyUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.PDT.rawValue + "/reply/" + String(replyUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
      
            doRequest(method: .delete, api: _url, api_type: .PDT_REPLY_DELETE, params: [:],header:header, success: success, failure: failure)
        }

        /**
         *    스타일 등록
         */
        open static func regStyleInfo(
            accessToken         : String,
            pdtUid              : Int,
            styleImg            : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.GET_PDTSTYLE.rawValue
            let params = [
                "pdtUid"              : pdtUid,
                "styleImg"            : styleImg,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .REG_PDTSTYLE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    상품 스타일 다수 등록
         */
        open static func regMultiStyleInfo(
            accessToken         : String,
            pdtUid              : Int,
            styleImgList        : Array<String>,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.GET_PDTSTYLE.rawValue + "/" + String(pdtUid)
            let params = [
                "styleImgList"         : styleImgList,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .MULTI_REG_PDTSTYLE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    네고 신청
         */
        open static func reqDealNego(
            accessToken         : String,
            islandSendPrice     : Int,
            pdtUid              : Int,
            recipientAddress    : String,
            recipientNm         : String,
            recipientPhone      : String,
            reqPrice            : Int,
            usrCardUid          : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL_NEGO.rawValue
            let params = [
                "islandSendPrice"    : String(islandSendPrice),
                "pdtUid"            : String(pdtUid),
                "recipientAddress"   : recipientAddress,
                "recipientNm"        : recipientNm,
                "recipientPhone"     : recipientPhone,
                "reqPrice"           : String(reqPrice),
                "usrCardUid"         : String(usrCardUid),
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_NEGO, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    상품 구매
         */
        open static func reqpdtBuy(
            accessToken         : String,
            islandSendPrice     : Int,
            payPromotion        : String,
            payType             : String,
            pdtUid              : Int,
            pdtVerifyEnabled    : Int,
            recipientAddress    : String,
            recipientNm         : String,
            recipientPhone      : String,
            reqPrice            : Int,
            rewardPrice         : Int,
            usrCardUid          : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL_BUY.rawValue
            let params = [
                "islandSendPrice"   : String(islandSendPrice),
                "payPromotion"      : payPromotion,
                "payType"           : payType,
                "pdtUid"            : String(pdtUid),
                "pdtVerifyEnabled"  : String(pdtVerifyEnabled),
                "recipientAddress"  : recipientAddress,
                "recipientNm"       : recipientNm,
                "recipientPhone"    : recipientPhone,
                "reqPrice"          : String(reqPrice),
                "rewardPrice"       : String(rewardPrice),
                "usrCardUid"        : String(usrCardUid)
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_BUY, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    상품 정보 수정
         */
        open static func reqpdtModify(
            accessToken         : String,
            pdtUid              : Int,
            component           : String,
            content             : String,
            etc                 : String,
            negoYn              : Int,
            pdtColor            : String,
            pdtCondition        : String,
            pdtModel            : String,
            pdtSize             : String,
            photos              : Array<String>,
            price               : Int,
            promotionCode       : String,
            sell                : Bool,
            sendPrice           : Int,
            tag                 : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue + "/" + String(pdtUid)
            let params = [
                "component"      : component,
                "content"        : content,
                "etc"            : etc,
                "negoYn"         : String(negoYn),
                "pdtColor"       : pdtColor,
                "pdtCondition"   : pdtCondition,
                "pdtModel"       : pdtModel,
                "pdtSize"        : pdtSize,
                "photos"         : photos,
                "price"          : String(price),
                "promotionCode"  : promotionCode,
                "sell"           : sell,
                "sendPrice"      : String(sendPrice),
                "tag"            : tag
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .PDT_MODIFIY, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    시스템 설정정보 얻기
         */
        open static func getSystemSettingInfo(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_SETTING.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_SETTING, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    사이즈 목록 얻기
         */
        open static func getSizeList(
            accessToken         : String,
            categoryUid         : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.CATEGORY.rawValue + "/" + String(categoryUid) + "/size"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .CATEGORY_SIZE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브랜드에 따르는 추천모델명 리스트 얻기
         */
        open static func getBrandModelNmsList(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/modelNames"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .BRAND_MODELNAMES, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 하위 카테고리 목록 얻기
         */
        open static func getSubCategoryList(
            accessToken         : String,
            categoryUid         : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.CATEGORY.rawValue + "/" + String(categoryUid) + "/sub"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .CATEGORY_SUB, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 카테고리별 인기브랜드 및 하위카테고리 얻기
         */
        open static func getCategoryDetailList(
            accessToken         : String,
            categoryUid         : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.CATEGORY.rawValue + "/" + String(categoryUid) + "/detail"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .CATEGORY_DETAIL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 해당 카테고리에 속하는 상품목록얻기(브랜드기준)
         */
        open static func getCategoryPdtList(
            accessToken         : String,
            categoryUid         : Int,
            page                : Int,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.CATEGORY.rawValue + "/" + String(categoryUid) + "/pdt?page=" + String(page) + "&brandUid=" + String(brandUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .CATEGORY_PDT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    상품 추가
         */
        open static func reqPdtInfo(
            accessToken         : String,
            brandUid            : Int,
            categoryUid         : Int,
            component           : String,
            content             : String,
            etc                 : String,
            negoYn              : Int,
            pdtColor            : String,
            pdtCondition        : String,
            pdtGroup            : String,
            pdtModel            : String,
            pdtSize             : String,
            photos              : Array<String>,
            price               : Int,
            promotionCode       : String,
            sendPrice           : Int,
            signImg             : String,
            tag                 : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.PDT.rawValue
            let params = [
                "brandUid"            : String(brandUid),
                "categoryUid"         : String(categoryUid),
                "component"           : component,
                "content"             : content,
                "etc"                 : etc,
                "negoYn"              : String(negoYn),
                "pdtColor"            : pdtColor,
                "pdtCondition"        : pdtCondition,
                "pdtGroup"            : pdtGroup,
                "pdtModel"            : pdtModel,
                "pdtSize"             : pdtSize,
                "photos"              : photos,
                "price"               : String(price),
                "promotionCode"       : promotionCode,
                "sendPrice"           : String(sendPrice),
                "signImg"             : signImg,
                "tag"                 : tag,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .PDT, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 회원의 아이템 목록 얻기
         */
        open static func getUsrPdtList(
            accessToken         : String,
            usrUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/pdt?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_PDT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 회원의 스타일 목록 얻기
         */
        open static func getUsrStyleList(
            accessToken         : String,
            usrUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/style?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_STYLE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 회원의 잇템 목록 얻기
         */
        open static func getUsrWishList(
            accessToken         : String,
            usrUid              : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/wish?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_WISH, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    유저 차단 해제하기
         */
        open static func reqUsrUnblock(
            accessToken         : String,
            usrUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/block"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .USER_UNBLOCK, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    유저 차단 하기
         */
        open static func reqUsrBlock(
            accessToken         : String,
            usrUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/block"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .USER_BLOCKING, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
          *    추천인 코드 중복 검사
         */
        open static func checkReconCode(
            inviteCode          : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CHECK_INVITECODE.rawValue + "?inviteCode=" + inviteCode
            
            doRequest(method: .get, api: _url, api_type: .USER_CHECK_INVITECODE, params: [:],header:[:], success: success, failure: failure)
        }
        
        /**
         *    sns 아이디 중복 검사
         */
        open static func checkSnsId(
            snsId               : String,
            snsType             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_CHECK_SNSID.rawValue + "?snsId?snsId" + snsId + "&snsType=" + snsType
             
            doRequest(method: .get, api: _url, api_type: .USER_CHECK_SNSID, params: [:],header:[:], success: success, failure: failure)
        }
        
        /**
         * 매너포인트 변경이력(거래후기) 얻기
         */
        open static func getusrRevieList(
            accessToken         : String,
            usrUid              : Int,
            point               : Int,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/review?point=" + String(point) + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_REVIEW, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    관심상품군 업데이트
         */
        open static func updateUsrLikeGroup(
            accessToken         : String,
            isChildren          : Bool,
            isFemale            : Bool,
            isMale              : Bool,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_LIKEGROUP.rawValue
            
            let params = [
                "isChildren"              : isChildren,
                "isFemale"                : isFemale,
                "isMale"                  : isMale,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .USER_LIKEGROUP, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 초대정보 얻기
         */
        open static func getInviteInfo(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_INVITEINFO.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_INVITEINFO, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 푸시토큰 업데이트
         */
        open static func updatePushToken(
            accessToken         : String,
            deviceToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_DEVICETOKEN.rawValue + "?deviceToken=" + deviceToken
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .post, api: _url, api_type: .USER_DEVICETOKEN, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
          *    연락처 친구찾기
         */
        open static func getFriendAddressList(
            accessToken         : String,
            list                : Array<String>,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_FRIEND_ADDRESSBOOK.rawValue
            
            let params = [
                "list"              : list,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .USER_FRIEND_ADDRESSBOOK, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    facebook 친구찾기
         */
        open static func getFriendFacebookList(
            accessToken         : String,
            list                : Array<String>,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_FRIEND_FACEBOOK.rawValue
            
            let params = [
                "list"              : list,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .USER_FRIEND_FACEBOOK, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    발렛신청
         */
        open static func reqValet(
            accessToken         : String,
            brandUid            : Int,
            categoryUid         : Int,
            reqAddress          : String,
            reqName             : String,
            reqPhone            : String,
            reqPrice            : Int,
            sendType            : Int,
            signImg             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_VALET.rawValue
            let params = [
                "brandUid"            : String(brandUid),
                "categoryUid"         : String(categoryUid),
                "reqAddress"          : reqAddress,
                "reqName"             : reqName,
                "reqPhone"            : reqPhone,
                "reqPrice"            : String(reqPrice),
                "sendType"            : String(sendType),
                "signImg"             : signImg,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .OTHER_VALET	, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         * 셀럽머니 변경이력 얻기
         */
        open static func getMoneyHistory(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_HISTORY_MONEY.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_HISTORY_MONEY, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 주문상세
         */
        open static func getDealDetail(
            accessToken         : String,
            dealUid             : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .DEAL_DETAIL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 구매내역
         */
        open static func getBuyHistory(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_HISTORY_BUY.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_HISTORY_BUY, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 판매내역
         */
        open static func getSellHistory(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_HISTORY_SELL.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_HISTORY_SELL, params: [:],header:header, success: success, failure: failure)
        }
        

        /**
         *  판매자 공유리워드 신청
         */
        open static func reqDealShare(
            accessToken         : String,
            dealUid             : Int,
            facebookPhotos      : Array<String>,
            instagramPhotos     : Array<String>,
            kakaostoryPhotos    : Array<String>,
            naverblogPhotos     : Array<String>,
            twitterPhotos       : Array<String>,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/share"
            let params = [
                "facebookPhotos"        : facebookPhotos,
                "instagramPhotos"       : instagramPhotos,
                "kakaostoryPhotos"      : kakaostoryPhotos,
                "naverblogPhotos"       : naverblogPhotos,
                "twitterPhotos"         : twitterPhotos,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_SHARE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  후기작성
         */
        open static func reqDealReview(
            accessToken         : String,
            dealUid             : Int,
            content             : String,
            point               : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/review"
            let params = [
                "point"             : String(point),
                "content"           : content,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_REVIEW, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 반품 거절
         */
        open static func reqRefundDisallow(
            accessToken         : String,
            dealUid             : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/refund/disallow"
           
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_REFUND_DISALLOW, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 반품 확인
         */
        open static func reqRefundComplete(
            accessToken         : String,
            dealUid             : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/refund/complete"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_REFUND_COMPLETE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 반품 승인
         */
        open static func reqRefundAllow(
            accessToken             : String,
            dealUid                 : Int,
            addressDetail           : String,
            addressDetailSub        : String,
            addressName             : String,
            addressPhone            : String,
            addressSelectionIndex   : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/refund/allow"
            let params = [
                "addressSelectionIndex"     : String(addressSelectionIndex),
                "addressDetail"            : addressDetail,
                "addressDetailSub"         : addressDetailSub,
                "addressName"              : addressName,
                "addressPhone"             : addressPhone,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_REFUND_ALLOW, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  구매자 반품 신청
         */
        open static func reqDealRefund(
            accessToken         : String,
            dealUid             : Int,
            photos              : Array<String>,
            refundReason        : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/refund"
            let params = [
                "photos"             : photos,
                "refundReason"       : refundReason,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_REFUND, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  구매자 카운터네고 거절
         */
        open static func reqDealNegoRefuseCounter(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/nego/refuse/counter"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_NEGO_REFUSE_COUNTER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 네고거절
         */
        open static func reqDealNegoRefuse(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/nego/refuse"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_NEGO_REFUSE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 카운터 네고 신청
         */
        open static func reqDealNegoCounter(
            accessToken         : String,
            dealUid             : Int,
            reqPrice            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/nego/counter?reqPrice=" + String(reqPrice)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_NEGO_COUNTER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 네고 승인
         */
        open static func reqDealNegoAllow(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/nego/allow"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_NEGO_ALLOW, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  구매자 카운터네고 승인
         */
        open static func reqDealNegoAllowCounter(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/nego/allow/counter"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_NEGO_ALLOW_COUNTER, params: [:],header:header, success: success, failure: failure)
        }

        /**
         *  판매자 운송장 번호등록
         */
        open static func reqDealDeliveryNum(
            accessToken         : String,
            dealUid             : Int,
            deliveryCompany     : String,
            deliveryNumber      : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/deliveryNumber" 
            
            let params = [
                "deliveryCompany"      : deliveryCompany,
                "deliveryNumber"       : deliveryNumber,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_DELIVERY_NUMBER, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  택배 예약하기
         */
        open static func reqDealDelivery(
            accessToken         : String,
            dealUid             : Int,
            recipientAddress    : String,
            recipientNm         : String,
            recipientPhone      : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/delivery"
            
            let params = [
                "recipientAddress"      : recipientAddress,
                "recipientNm"           : recipientNm,
                "recipientPhone"        : recipientPhone,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .DEAL_DELIVERY, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  구매자 거래 확정
         */
        open static func reqDealComplete(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/complete"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_COMPLETE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  판매자 판매 거절
         */
        open static func reqDealCancelSeller(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/cancel/seller"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_CANCEL_SELLER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  구매자 주문취소
         */
        open static func reqDealCancel(
            accessToken             : String,
            dealUid                 : Int,
            success                 : SuccessBlock?,
            failure                 : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/cancel"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .DEAL_CANCEL, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    검색
         */
        open static func Search(
            accessToken         : String,
            keyword             : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH.rawValue + "?keyword=" + keyword + "&page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .SEARCH, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  검색결과 갯수
         */
        open static func SearchCount(
            accessToken         : String,
            brandUidList        : Array<Int>,
            categoryUid         : Int,
            keyword             : String,
            pdtColorList        : Array<String>,
            pdtConditionList    : Array<String>,
            pdtPriceMax         : Int,
            pdtPriceMin         : Int,
            pdtSizeList         : Array<String>,
            searchOrderType     : String,
            soldOutExpect       : Bool,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH_COUNT.rawValue
            let params = [
                "brandUidList"      : brandUidList,
                "categoryUid"       : String(categoryUid),
                "keyword"           : keyword,
                "pdtColorList"      : pdtColorList,
                "pdtConditionList"  : pdtConditionList,
                "pdtPriceMax"       : String(pdtPriceMax),
                "pdtPriceMin"       : String(pdtPriceMin),
                "pdtSizeList"       : pdtSizeList,
                "searchOrderType"   : searchOrderType,
                "soldOutExpect"     : soldOutExpect,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .SEARCH_COUNT, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  상세검색
         */
        open static func getSearchDetail(
            accessToken         : String,
            brandUidList        : Array<Int>,
            categoryUid         : Int,
            keyword             : String,
            pdtColorList        : Array<String>,
            pdtConditionList    : Array<String>,
            pdtPriceMax         : Int,
            pdtPriceMin         : Int,
            pdtSizeList         : Array<String>,
            searchOrderType     : String,
            soldOutExpect       : Bool,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH_DETAIL.rawValue
            let params = [
                "brandUidList"      : brandUidList,
                "categoryUid"       : String(categoryUid),
                "keyword"           : keyword,
                "pdtColorList"      : pdtColorList,
                "pdtConditionList"  : pdtConditionList,
                "pdtPriceMax"       : String(pdtPriceMax),
                "pdtPriceMin"       : String(pdtPriceMin),
                "pdtSizeList"       : pdtSizeList,
                "searchOrderType"   : searchOrderType,
                "soldOutExpect"     : soldOutExpect,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .SEARCH_DETAIL, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    인기검색 키워드 리스트 얻기
         */
        open static func getSearchFame(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH_FAME.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .SEARCH_FAME, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  상품 최대 최소가격
         */
        open static func getSearchPriceRange(
            accessToken         : String,
            brandUidList        : Array<Int>,
            categoryUid         : Int,
            keyword             : String,
            pdtColorList        : Array<String>,
            pdtConditionList    : Array<String>,
            pdtPriceMax         : Int,
            pdtPriceMin         : Int,
            pdtSizeList         : Array<String>,
            searchOrderType     : String,
            soldOutExpect       : Bool,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH_PRICERANGE.rawValue
            let params = [
                "brandUidList"      : brandUidList,
                "categoryUid"       : String(categoryUid),
                "keyword"           : keyword,
                "pdtColorList"      : pdtColorList,
                "pdtConditionList"  : pdtConditionList,
                "pdtPriceMax"       : String(pdtPriceMax),
                "pdtPriceMin"       : String(pdtPriceMin),
                "pdtSizeList"       : pdtSizeList,
                "searchOrderType"   : searchOrderType,
                "soldOutExpect"     : soldOutExpect,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .SEARCH_PRICERANGE, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    태그검색
         */
        open static func getSearchTag(
            accessToken         : String,
            keyword             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH_TAG.rawValue + "?keyword=" + keyword
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .SEARCH_TAG, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    추천검색 키워드 리스트 얻기
         */
        open static func getSearchRecommend(
            accessToken         : String,
            keyword             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.SEARCH_RECOMMEND.rawValue + "?keyword=" + keyword
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .SEARCH_RECOMMEND, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    추천 유저 목록
         */
        open static func getRecommendUsrList(
            accessToken         : String,
            page                : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_RECOMMEND.rawValue + "?page=" + String(page)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .USER_RECOMMEND, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    유저 숨기기
         */
        open static func hideRecommendUsr(
            accessToken         : String,
            usrUid              : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER.rawValue + "/" + String(usrUid) + "/hide"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .USER_HIDE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  영업일 기준 날짜 얻기
         */
        open static func getWorkingDay(
            accessToken         : String,
            basisDate           : String,
            deltaDays           : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_DELAYWORKINGDAY.rawValue
            
            let params = [
                "basisDate"      : basisDate,
                "deltaDays"      : String(deltaDays),
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .OTHER_DELAYWORKINGDAY, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    택배사 목록 얻기
         */
        open static func getDeliveryCompanies(
            accessToken         : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_DELIVERY_COMPANIES.rawValue
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_DELIVERY_COMPANIES, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    택베 예약 취소
         */
        open static func delDealDelivery(
            accessToken         : String,
            dealUid             : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/delivery"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .USER, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 택배 예약 정보 얻기
         */
        open static func getDealDeliveryInfo(
            accessToken         : String,
            dealUid             : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.DEAL.rawValue + "/" + String(dealUid) + "/delivery"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .DEAL_DELIVERY_INFO, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *  회원기타정보 수정
         */
        open static func updateOtherUsrInfo(
            accessToken                 : String,
            addrDetail1              : String,
            addrDetail2              : String,
            addrDetail3              : String,
            addrDetailSub1           : String,
            addrDetailSub2           : String,
            addrDetailSub3           : String,
            addrName1                : String,
            addrName2                : String,
            addrName3                : String,
            addrPhone1               : String,
            addrPhone2               : String,
            addrPhone3               : String,
            addrSelectionIndex       : Int,
            usermail                 : String,
            success                     : SuccessBlock?,
            failure                     : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_OTHER.rawValue
            let params = [
                "addressDetail1"              : addrDetail1,
                "addressDetail2"              : addrDetail2,
                "addressDetail3"              : addrDetail3,
                "addressDetailSub1"           : addrDetailSub1,
                "addressDetailSub2"           : addrDetailSub2,
                "addressDetailSub3"           : addrDetailSub3,
                "addressName1"                : addrName1,
                "addressName2"                : addrName2,
                "addressName3"                : addrName3,
                "addressPhone1"               : addrPhone1,
                "addressPhone2"               : addrPhone2,
                "addressPhone3"               : addrPhone3,
                "addressSelectionIndex"       : String(addrSelectionIndex),
                "usrMail"                     : usermail,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .USER_OTHER, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *  회원기본정보 수정
         */
        open static func updateBasisUsrInfo(
            accessToken        : String,
            birthday           : String,
            gender             : String,
            usrNm              : String,
            usrPhone           : String,
            success            : SuccessBlock?,
            failure            : FailureBlock?
            )
        {
            
            let _url  = F_API.USER_BASIS.rawValue
            let params = [
                "birthday"           : birthday,
                "gender"             : gender,
                "usrNm"              : usrNm,
                "usrPhone"           : usrPhone,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .post, api: _url, api_type: .USER_BASIS, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *   sns연동 삭제
         */
        open static func delUsrSns(
            accessToken         : String,
            snsType             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_SNS.rawValue + "?snsType=" + snsType
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .DEL_USER_SNS, params: [:],header:header, success: success, failure: failure)
        }
     
        /**
         *    sns 연동 하기
         */
        open static func setUsrSns(
            accessToken         : String,
            snsEmail            : String,
            snsId               : String,
            snsNckNm            : String,
            snsProfileImg       : String,
            snsToken            : String,
            snsType             : String,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.USER_SNS.rawValue
            let params = [
                "snsEmail"        : snsEmail,
                "snsId"           : snsId,
                "snsNckNm"        : snsNckNm,
                "snsProfileImg"   : snsProfileImg,
                "snsToken"        : snsToken,
                "snsType"         : snsType,
                ] as NSDictionary
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest3(method: .put, api: _url, api_type: .SET_USER_SNS, params: params,header:header, success: success, failure: failure)
        }
        
        /**
         *    이벤트 상세 얻기
         */
        open static func getEventDetailInfo(
            accessToken         : String,
            eventUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_EVENT.rawValue + "/" + String(eventUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_DETAIL_EVENT, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         *    공지사항 상세 얻기
         */
        open static func getNoticeDetailInfo(
            accessToken         : String,
            noticeUid           : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.OTHER_NOTICE.rawValue + "/" + String(noticeUid)
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .get, api: _url, api_type: .OTHER_DETAIL_NOTICE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 팔로우 하기
         */
        open static func setbrandFollow(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .put, api: _url, api_type: .BRAND_ONE_LIKE, params: [:],header:header, success: success, failure: failure)
        }
        
        /**
         * 브렌드 팔로우 취소하기
         */
        open static func delbrandFollow(
            accessToken         : String,
            brandUid            : Int,
            success             : SuccessBlock?,
            failure             : FailureBlock?
            )
        {
            let _url  = F_API.BRAND.rawValue + "/" + String(brandUid) + "/like"
            
            let header = [
                "accessToken"   : accessToken,
                ] as [String : String]
            
            doRequest(method: .delete, api: _url, api_type: .BRAND_ONE_LIKE, params: [:],header:header, success: success, failure: failure)
        }
    }
