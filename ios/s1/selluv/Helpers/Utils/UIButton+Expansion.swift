//
//  UIButton+Expansion.swift
//  Utility
//
//  Created by Dragon C. on 10/15/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func centerLabelVerticallyWithPadding(spacing: CGFloat = 0) {
        self.imageView!.contentMode = .scaleAspectFit
        
        // lower the text and push it left so it appears centered
        //  below the image
        let imageSize: CGSize = self.imageView!.frame.size
        self.titleEdgeInsets = UIEdgeInsetsMake(0, -imageSize.width, -(imageSize.height + spacing), 0)
        
        // raise the image and push it right so it appears centered
        //  above the text
        let titleSize: CGSize = self.titleLabel!.frame.size
        self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0, 0, -titleSize.width)
    }
    
     private struct AssociatedKeys {
        static var WithValue : [String: Any]? = [:]
    }
    
    @IBInspectable var withValue: [String: Any]? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.WithValue) as? [String: Any]
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.WithValue, newValue as [String: Any]?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN )
                
            }
        }
        
    }
}

