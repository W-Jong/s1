//
//  UILabel+Expansion.swift
//  Utility
//
//  Created by Dragon C. on 8/31/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class UIExtLabel: UILabel {
    
    @IBInspectable var topInset = CGFloat(0)
    @IBInspectable var bottomInset = CGFloat(0)
    @IBInspectable var leftInset = CGFloat(0)
    @IBInspectable var rightInset = CGFloat(0)
    
	override func drawText(in rect: CGRect) {
		let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
		super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
	}
	
	override var intrinsicContentSize: CGSize {
		var intrinsicSuperViewContentSize = super.intrinsicContentSize
		
		if intrinsicSuperViewContentSize != .zero {
			let textWidth = frame.size.width - (leftInset + rightInset)
			let newSize = text!.boundingRect(with: CGSize(width: textWidth, height: .greatestFiniteMagnitude),
											 options: .usesLineFragmentOrigin,
											 attributes: [NSAttributedStringKey.font: font],
											 context: nil)
			intrinsicSuperViewContentSize.height = ceil(newSize.height) + topInset + bottomInset
		}
		
		return intrinsicSuperViewContentSize
	}
	
}
