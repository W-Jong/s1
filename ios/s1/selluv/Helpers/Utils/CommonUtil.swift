//
//  CommonUtil.swift
//  Utility
//
//  Created by Dragon C. on 8/7/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper
import SwiftyJSON

class CommonUtil {
    
    static func bundleID() -> String {
        return Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
    }
    
    static func bundleVer() -> String {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
    
    static func systemVer() -> String {
        return UIDevice.current.systemVersion
    }
    
    static func buildNum() -> String {
        return Bundle.main.infoDictionary!["CFBundleVersion"] as! String
    }
    
    static func modelName() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    static func deviceName() -> String {
        return UIDevice.current.name
    }
    
    static func deviceUUID() -> String! {
        let id_key = App_Name + "_UUID"
        var stored = KeychainWrapper.standard.string(forKey: id_key)
        
        if stored == nil {
            let theUUID = CFUUIDCreate(nil)
            let cfstr = CFUUIDCreateString(nil, theUUID)
            let nsTypeString = cfstr!
            let swiftString = nsTypeString as String
            
            _ = KeychainWrapper.standard.set(swiftString, forKey: id_key)
            stored = KeychainWrapper.standard.string(forKey: id_key)
        }
        
        if stored == nil {
            stored = "undefined"
        }
        
        // print("DeviceID:\(stored)")
        return stored
    }
    
    static func topViewController() -> UIViewController? {
        var topVC: UIViewController? = UIApplication.shared.keyWindow!.rootViewController
        while (topVC != nil && topVC?.presentedViewController != nil) {
            topVC = topVC?.presentedViewController
        }
        
        return topVC
    }
    
    static func validEmail(_ exp: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: exp)
    }
    
	static func validUrl(_ string: String?) -> Bool {
		if string != nil, let url = URL(string: string!) {
			return UIApplication.shared.canOpenURL(url as URL)
		}
		return false
	}
	
    static func loadUrl(_ wvView: UIWebView, _ str: String) {
        return wvView.loadRequest(URLRequest(url: URL(string: str)!))
    }
    
    static func formatNum(_ val: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        return formatter.string(from: NSNumber(value: val))!
    }
    
    static func formatNum(_ val: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        return formatter.string(from: NSNumber(value: val))!
    }
    
    static func formatNum(_ val: Float) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        return formatter.string(from: NSNumber(value: val))!
    }
    
    static func phaseFullName(fullName: String!) -> [String] {
        var arrRet = ["", ""]
        
        if fullName != nil && fullName != "" {
            arrRet = fullName.characters.split{ $0 == " " }.map(String.init)
        }
        
        return arrRet
    }
    
    static func containsOnlyLettersAndNumbers(_ input: String) -> Bool {
        for chr in input.characters {
            if !(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") && !(chr >= "0" && chr <= "9") {
                return false
            }
        }
        return true
    }
    
    static func addDoneButton(_ textField: UITextField) {
        let keypadToolbar: UIToolbar = UIToolbar()
        
        keypadToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: textField, action: #selector(UITextField.resignFirstResponder)),
        ]
        keypadToolbar.sizeToFit()
        textField.inputAccessoryView = keypadToolbar
    }
    
    static func addNextButton(_ textField: UITextField, to nextTextField: UITextField) {
        let keypadToolbar: UIToolbar = UIToolbar()
        keypadToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: nextTextField, action: #selector(UITextField.becomeFirstResponder)),
        ]
        keypadToolbar.sizeToFit()
        textField.inputAccessoryView = keypadToolbar
    }
    
    static func addDoneButton(_ textView: UITextView) {
        let keypadToolbar: UIToolbar = UIToolbar()
        
        keypadToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: textView, action: #selector(UITextView.resignFirstResponder)),
        ]
        keypadToolbar.sizeToFit()
        textView.inputAccessoryView = keypadToolbar
    }
    
    static func addNextButton(_ textView: UITextView, to nextTextView: UITextView) {
        let keypadToolbar: UIToolbar = UIToolbar()
        keypadToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: nextTextView, action: #selector(UITextView.becomeFirstResponder)),
        ]
        keypadToolbar.sizeToFit()
        textView.inputAccessoryView = keypadToolbar
    }
    
    static func addNextButton(_ textView: UITextView, to nextTextField: UITextField) {
        let keypadToolbar: UIToolbar = UIToolbar()
        keypadToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: nextTextField, action: #selector(UITextField.becomeFirstResponder)),
        ]
        keypadToolbar.sizeToFit()
        textView.inputAccessoryView = keypadToolbar
    }
    
    static func callPhone(_ strPhone: String) {
        let formatedNumber = strPhone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        if let url = URL(string: "tel://\(formatedNumber)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static func removeAllSubviews(_ view: UIView) {
        for item in view.subviews {
            item.removeFromSuperview()
        }
    }
    
    //////////////////////////////////////////////////////////////////////
    // MARK: - load & store value
    //////////////////////////////////////////////////////////////////////
    
    static func bool(_ key: String) -> Bool {
        return UserDefaults.standard.bool(forKey: key)
    }
    
    static func setBool(_ value: Bool, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func int(_ key: String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }
    
    static func setInt(_ value: Int, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func string(_ key: String) -> String! {
        return UserDefaults.standard.string(forKey: key) as String!
    }
    
    static func setString(_ value: String!, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func data(_ key: String) -> AnyObject! {
        return UserDefaults.standard.object(forKey: key) as AnyObject!
    }
    
    static func setData(_ value: AnyObject!, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func archivedData(_ key: String) -> AnyObject! {
        if let data = UserDefaults.standard.object(forKey: key) as? NSData {
            return NSKeyedUnarchiver.unarchiveObject(with: data as Data) as AnyObject!
        }
        return nil
    }
    
    static func setArchivedData(_ value: AnyObject!, forKey: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(data, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func showToast(_ msg: String) {
        let mdtoast : MDToast = MDToast()
        mdtoast.text = msg
        mdtoast.show()
    }
    
    public static func getDateFromStamp(_ time : Int, type : String) -> String! {
        
        let date = NSDate(timeIntervalSince1970: TimeInterval(time) / 1000)
        let dateFormatter = DateFormatter()
        //        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
//        dateFormatter.timeZone = TimeZone(abbreviation: "KST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = type //Specify your format that you want
        let strDate = dateFormatter.string(from: date as Date)
        return strDate
    }
    
    /**
     * Get the string of diff time
     */
    static func diffTime(_ p_sec: Int) -> String {
//        var w_nDiffMillis: Int64 = Int64(p_sec) * 1000
        
        let curTimestamp = NSDate().timeIntervalSince1970
        let time: Int64 = Int64(curTimestamp - ( TimeInterval(p_sec) / 1000 ))
        var w_nDiffMillis = time  * 1000
        
        let w_nWeeks = w_nDiffMillis / (7 * 24 * 3600 * 1000)
        w_nDiffMillis -= w_nWeeks * (7 * 24 * 3600 * 1000)
        let w_nDiffDays: Int64 = w_nDiffMillis / (24 * 3600 * 1000)
        w_nDiffMillis -= w_nDiffDays * (24 * 3600 * 1000)
        let w_nDiffHours: Int64 = w_nDiffMillis / (3600 * 1000)
        w_nDiffMillis -= w_nDiffHours * (3600 * 1000)
        let w_nDiffMins: Int64 = w_nDiffMillis / (60 * 1000)
        w_nDiffMillis -= w_nDiffMins * (60 * 1000)
        let w_nDiffSeconds: Int64 = w_nDiffMillis / 1000
        
        var w_strExpression = ""
        
        let date = Date(timeIntervalSince1970: (TimeInterval(p_sec)/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy" //Specify your format that you want
        let strYear = dateFormatter.string(from: date)
        let strCurrentYear = dateFormatter.string(from: Date())
        
        if(strYear != strCurrentYear) {
             dateFormatter.dateFormat = "yyyy년 MM월 dd일"
             w_strExpression = dateFormatter.string(from: date)
        }
        else if ( w_nWeeks > 0 ){
            dateFormatter.dateFormat = "MM월 dd일"
            w_strExpression = dateFormatter.string(from: date)
            //w_strExpression = String(format:"%d주 전", w_nWeeks)
        } else if w_nDiffDays > 0 {
            w_strExpression = String(format:"%d일 전", w_nDiffDays)
        } else if w_nDiffHours > 0 {
            w_strExpression = String(format:"%d시간 전", w_nDiffHours)
        } else if w_nDiffMins > 0 {
            w_strExpression = String(format:"%d분 전", w_nDiffMins)
        } else if w_nDiffSeconds >= 0 {
            //w_strExpression = String(format:"%dsec", w_nDiffSeconds)
            w_strExpression = "방금 전"
        }
        
        return w_strExpression
    }

    /**
     * Get the string of diff time
     */
    static func diffTime1(_ p_sec: Int) -> String {
        //        var w_nDiffMillis: Int64 = Int64(p_sec) * 1000
        
        let curTimestamp = NSDate().timeIntervalSince1970
        let time: Int64 = Int64(curTimestamp - ( TimeInterval(p_sec) / 1000 ))
        var w_nDiffMillis = time  * 1000
        
        let w_nWeeks = w_nDiffMillis / (7 * 24 * 3600 * 1000)
        w_nDiffMillis -= w_nWeeks * (7 * 24 * 3600 * 1000)
        let w_nDiffDays: Int64 = w_nDiffMillis / (24 * 3600 * 1000)
        w_nDiffMillis -= w_nDiffDays * (24 * 3600 * 1000)
        let w_nDiffHours: Int64 = w_nDiffMillis / (3600 * 1000)
        w_nDiffMillis -= w_nDiffHours * (3600 * 1000)
        let w_nDiffMins: Int64 = w_nDiffMillis / (60 * 1000)
        w_nDiffMillis -= w_nDiffMins * (60 * 1000)
        let w_nDiffSeconds: Int64 = w_nDiffMillis / 1000
        
        var w_strExpression = ""
        
        let date = Date(timeIntervalSince1970: (TimeInterval(p_sec)/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy" //Specify your format that you want
        let strYear = dateFormatter.string(from: date)
        let strCurrentYear = dateFormatter.string(from: Date())
        
        if(strYear != strCurrentYear) {
            dateFormatter.dateFormat = "yyyy년 MM월 dd일"
            w_strExpression = dateFormatter.string(from: date)
        }
        else if ( w_nWeeks > 0 ){
            dateFormatter.dateFormat = "MM월 dd일"
            w_strExpression = dateFormatter.string(from: date)
            //w_strExpression = String(format:"%d주 전", w_nWeeks)
        }  else if w_nDiffDays > 0 {
            w_strExpression = String(format:"%d일", w_nDiffDays)
        } else if w_nDiffHours > 0 {
            w_strExpression = String(format:"%d시간", w_nDiffHours)
        } else if w_nDiffMins > 0 {
            w_strExpression = String(format:"%d분", w_nDiffMins)
        } else if w_nDiffSeconds >= 0 {
            //w_strExpression = String(format:"%dsec", w_nDiffSeconds)
            w_strExpression = "방금"
        }
        
        return w_strExpression
    }
    
    /**
     * 날짜차 계산
     */
    static func diffDay(_ p_sec: Int) -> Int {
        
        let curTimestamp = NSDate().timeIntervalSince1970
        let time: Int64 = Int64(curTimestamp - ( TimeInterval(p_sec) / 1000 ))
        
        let day : Int = Int(time/60/60/24)
        
        return day
    }
    
    /**
     * 시간차 계산
     */
    static func diffHour(_ p_sec: Int) -> Int {
        
        let curTimestamp = NSDate().timeIntervalSince1970
        let time: Int64 = Int64(curTimestamp - ( TimeInterval(p_sec) / 1000 ))
        
        let hour : Int = Int(time/60/60)
        
        return hour
    }
    
    static func convertNum(data : String) -> String! {
        var result : String = ""

//        let first : String = String(data[data.index(data.startIndex, offsetBy: 0)..<data.index(data.startIndex, offsetBy: 4)])
//        let second : String = String(data[data.index(data.startIndex, offsetBy: 8)..<data.index(data.startIndex, offsetBy: 12)])
//
//        result = String.init(format: "%@-****-%@-****", first , second)
        
        let arr = data.components(separatedBy: "-")
        result = String.init(format: "%@-%@-%@-%@", arr[0], "****", arr[2], "****")
        
        return result
    }
    
    //phone num format  3-4-4
    static func getDashPhoneNum(num : String) -> String! {
        var result : String = ""
        if num.count == 11 {
            
            let first : String = String(num[num.index(num.startIndex, offsetBy: 0)..<num.index(num.startIndex, offsetBy: 3)])
            let second : String = String(num[num.index(num.startIndex, offsetBy: 3)..<num.index(num.startIndex, offsetBy: 7)])
            let third : String = String(num[num.index(num.startIndex, offsetBy: 7)..<num.index(num.startIndex, offsetBy: 11)])
            
            result = String.init(format: "%@-%@-%@", first , second, third)
        } else {
            result = num
        }
        
        return result
    }
    
    static func getNameFromStortType(data : String) -> String! {
        var result : String = ""
        
        //['RECOMMEND', 'NEW', 'FAME', 'PRICELOW', 'PRICEHIGH'],
        if data == "RECOMMEND" {
            result = SortType.recommendOrder.rawValue
        } else if data == "NEW" {
            result = SortType.newOrder.rawValue
        } else if data == "FAME" {
            result = SortType.popularOrder.rawValue
        } else if data == "PRICELOW" {
            result = SortType.cheapOrder.rawValue
        } else {
            result = SortType.expensiveOrder.rawValue
        }
        return result
        
    }
}


extension Double {
	
	func round(_ digits: Int = 2) -> Double {
		let multiplier = pow(10, Double(digits))
		return Darwin.round(self * multiplier) / multiplier
	}
	
}
