//
//  ScreenUtil.swift
//  selluv
//
//  Created by Dev on 12/5/17.
//

import UIKit

class ScreenUtil {
    
    static func isPhone() -> Bool {
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
    }
    
    static func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    static func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    static func getScreenMaxLength() -> CGFloat {
        return max(getScreenWidth(), getScreenHeight())
    }
    
    static func getScreenMinLength() -> CGFloat {
        return min(getScreenWidth(), getScreenHeight())
    }
    
    static func isDevice_35Inch() -> Bool {
        return (UIScreen.main.bounds.size.equalTo(CGSize(width: 320.0, height: 480.0)) ||
            UIScreen.main.bounds.size.equalTo(CGSize(width: 480.0, height: 320.0)))
    }
    
    static func isDevice_4Inch() -> Bool {
        return (UIScreen.main.bounds.size.equalTo(CGSize(width: 320.0, height: 568.0)) ||
            UIScreen.main.bounds.size.equalTo(CGSize(width: 568.0, height: 320.0)))
    }
    
    static func isDevice_47Inch() -> Bool {
        return (UIScreen.main.bounds.size.equalTo(CGSize(width: 375.0, height: 667.0)) ||
            UIScreen.main.bounds.size.equalTo(CGSize(width: 667.0, height: 375.0)))
    }
    
    static func isDevice_55Inch() -> Bool {
        return (UIScreen.main.bounds.size.equalTo(CGSize(width: 414.0, height: 736.0)) ||
            UIScreen.main.bounds.size.equalTo(CGSize(width: 736.0, height: 414.0)))
    }
    
    static func isPhone6OrOver() -> Bool {
        return isPhone() && getScreenMaxLength() >= 667.0
    }
    
}
