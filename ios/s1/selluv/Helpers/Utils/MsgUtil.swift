//
//  MsgUtil.swift
//  Utility
//
//  Created by Dragon C. on 8/7/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import Material

class MsgUtil {

    /**
     *  Show the standard message.
     */
    static func showUIAlert(_ msg: String, _ title: String? = nil) {
        let acAlert = UIAlertController(title: title,
                                        message: msg,
                                        preferredStyle: .alert)
        acAlert.addAction(UIAlertAction(title: "확인", style: .cancel))
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController!.present(acAlert, animated: true, completion: nil)
    }
    
}
