//
//  ProgressUtil.swift
//  Utility
//
//  Created by Dragon C. on 8/7/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class ProgressUtil {
    
    var mShow = false
    var mIndicator : NVActivityIndicatorView?
        
    func show() {
        if mShow {
            return
        }
        
        let pMainWindow = UIApplication.shared.windows[0]
        let view = UIView(frame: pMainWindow.frame)
        mIndicator = NVActivityIndicatorView(frame: CGRect(x: pMainWindow.center.x - 20.0, y: pMainWindow.center.y - 20.0, width: 40.0, height: 40.0),
                                            type: .ballClipRotate, color: UIColor(hex: 0x0090FF), padding: 0)
        //mIndicator!.color = UIColor.grayColor()
        
        view.addSubview(mIndicator!)
        pMainWindow.addSubview(view)
        mIndicator!.startAnimating()
        
        mShow = true
    }
    
    func hide() {
        if !mShow {
            return
        }
        
        mIndicator!.stopAnimating()
        mIndicator!.superview?.removeFromSuperview()
        mIndicator!.removeFromSuperview()
        mIndicator = nil
        
        mShow = false
    }
    
}
