//
//  UIView+Expansion.swift
//  Utility
//
//  Created by Dragon C. on 8/8/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func setRoundRect(radius p_radius: CGFloat = -1) {
        self.layer.cornerRadius = p_radius != -1 ? p_radius : self.frame.size.height / 2
        self.layer.masksToBounds = true
    }
    
    func setBorder(color p_color: UIColor, width p_width: CGFloat = 1.0) {
        self.layer.borderColor = p_color.cgColor
        self.layer.borderWidth = p_width
    }
    
    func setShadow(radius p_radius: CGFloat = 3.0) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = p_radius
        self.clipsToBounds = false
    }
	
	//. https://stackoverflow.com/questions/26815263/setting-a-rotation-point-for-cgaffinetransformmakerotation-swift
	//. CGPoint(x: 0.5, y: 1) - bottom center
	func setAnchorPoint(anchorPoint: CGPoint) {
		var newPoint = CGPoint(x: self.bounds.size.width * anchorPoint.x, y: self.bounds.size.height * anchorPoint.y)
		var oldPoint = CGPoint(x: self.bounds.size.width * self.layer.anchorPoint.x, y: self.bounds.size.height * self.layer.anchorPoint.y)
		
		newPoint = newPoint.applying(self.transform)
		oldPoint = oldPoint.applying(self.transform)
		
		var position: CGPoint = self.layer.position
		
		position.x -= oldPoint.x
		position.x += newPoint.x
		
		position.y -= oldPoint.y
		position.y += newPoint.y
		
		self.layer.position = position
		self.layer.anchorPoint = anchorPoint
	}

}
