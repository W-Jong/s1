//
//  TimeUtil.swift
//  Utility
//
//  Created by Dragon C. on 8/7/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import UIKit

class TimeUtil {
    
    /**
     * Get the string of diff time.
     */
    static func diffTime(_ p_start: Date, p_end: Date = Date()) -> String {
        return diffTime(Int(p_end.timeIntervalSince(p_start)))
    }
    
    /**
     * Get the string of second time
     */
    static func diffTime(_ p_sec: Int) -> String {
        var w_nMillis: Int64 = Int64(p_sec) * 1000
        
        let w_nDays: Int64 = w_nMillis / (24 * 3600 * 1000)
        w_nMillis -= w_nDays * (24 * 3600 * 1000)
        let w_nHours: Int64 = w_nMillis / (3600 * 1000)
        w_nMillis -= w_nHours * (3600 * 1000)
        let w_nMins: Int64 = w_nMillis / (60 * 1000)
        w_nMillis -= w_nMins * (60 * 1000)
        let w_nSecs: Int64 = w_nMillis / 1000
        
        var w_strRet = ""
        
        if w_nDays > 0 {
            w_strRet = String(format:"%dday ago", w_nDays)
        } else if w_nHours > 0 {
            w_strRet = String(format:"%dhr ago", w_nHours)
        } else if w_nMins > 0 {
            w_strRet = String(format:"%dmin ago", w_nMins)
        } else if w_nSecs >= 0 {
            w_strRet = String(format:"%dsec ago", w_nSecs)
        }
        
        return w_strRet
    }
    
    /**
     * Get the string of diff time
     */
    static func diffTime2(_ p_sec: Int) -> String {
        var w_nDiffMillis: Int64 = Int64(p_sec) * 1000
        
        let w_nDiffDays: Int64 = w_nDiffMillis / (24 * 3600 * 1000)
        w_nDiffMillis -= w_nDiffDays * (24 * 3600 * 1000)
        let w_nDiffHours: Int64 = w_nDiffMillis / (3600 * 1000)
        w_nDiffMillis -= w_nDiffHours * (3600 * 1000)
        let w_nDiffMins: Int64 = w_nDiffMillis / (60 * 1000)
        w_nDiffMillis -= w_nDiffMins * (60 * 1000)
        let w_nDiffSeconds: Int64 = w_nDiffMillis / 1000
        
        var w_strExpression = ""
        
        if w_nDiffDays > 0 {
            w_strExpression = String(format:"%dday %dhr %dmin %dsec", w_nDiffDays, w_nDiffHours, w_nDiffMins, w_nDiffSeconds)
        } else if w_nDiffHours > 0 {
            w_strExpression = String(format:"%dhr %dmin %dsec", w_nDiffHours, w_nDiffMins, w_nDiffSeconds)
        } else if w_nDiffMins > 0 {
            w_strExpression = String(format:"%dmin %dsec", w_nDiffMins, w_nDiffSeconds)
        } else if w_nDiffSeconds >= 0 {
            w_strExpression = String(format:"%dsec", w_nDiffSeconds)
        }
        
        return w_strExpression
    }
    
    // "EEE, dd MMM yyyy HH:mm:ss a ZZZ"
    static func str2Date(_ p_date: String?, _ p_format: String = "yyyy-MM-dd HH:mm:ss") -> Date? {
		guard p_date != nil else {
			return nil
		}
		
		let dtFmt = DateFormatter()
        dtFmt.dateFormat = p_format
        dtFmt.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")
        return dtFmt.date(from: p_date!)
    }
    
    static func date2Str(_ p_date: Date?, _ p_format: String = "yyyy-MM-dd HH:mm:ss", _ p_week: Bool = false) -> String? {
		guard p_date != nil else {
			return nil
		}
		
        let dtFmt = DateFormatter()
        dtFmt.dateFormat = p_format
		dtFmt.amSymbol = "오전"
		dtFmt.pmSymbol = "오후"
        dtFmt.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")
        let strRet = dtFmt.string(from: p_date!)
        return p_week ? strRet + " (\(week(p_date!)!))" : strRet
    }
    
    static func week(_ p_date: Date?) -> String? {
		guard p_date != nil else {
			return nil
		}
		
		let cal = Calendar.current
        let weekday = (cal as NSCalendar).component(.weekday, from: p_date!)
        let weekdays = ["일", "월", "화", "수", "목", "금", "토"]
        return weekdays[weekday - 1]
    }
    
    static func year() -> Int {
        return Calendar.current.component(.year, from: Date())
    }
    
    static func month() -> Int {
        return Calendar.current.component(.month, from: Date())
    }
    
    static func monthName(_ p_month: Int = 0) -> String {
        var w_month = p_month
        
        if w_month == 0 {
            w_month = Calendar.current.component(.month, from: Date())
        }
        
        let format = DateFormatter()
        return format.monthSymbols[w_month - 1]
    }
    
    static func component(_ p_date: Date) -> DateComponents {
        let calendar = Calendar.current
        let units: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        return calendar.dateComponents(units, from: Date())
    }

    static func from(year: Int, month: Int, day: Int) -> Date {
        var comp = DateComponents()
        comp.year = year
        comp.month = month
        comp.day = day
        return Calendar.current.date(from: comp)!
    }
    
}
