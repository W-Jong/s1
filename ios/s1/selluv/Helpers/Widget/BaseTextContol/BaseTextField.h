//
//  BaseTextField.h
//  Colorencar
//
//  Created by Rabbit on 2014. 3. 25..
//  Copyright (c) 2014년 WG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTextField : UITextField {
    UIImage *m_imgNormal;
    UIImage *m_imgFocus;
    UIControlState m_state;
    UIColor *m_highlightColor;
    float m_padding;
}

@property (nonatomic, strong) IBInspectable UIColor *placeholderColor;
@property (nonatomic, strong) UIColor *highlightColor;
@property (nonatomic, strong) UIImage *highlightImage;
@property (nonatomic, strong) UIImage *defaultImage;

- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state;
- (void)setState:(UIControlState)state;
- (void)setPadding:(float)padding;

@end
