//
//  BaseTextView.m
//  PriceTalk
//
//  Created by Tiger on 2014. 3. 25..
//  Copyright (c) 2014년 WG. All rights reserved.
//

#import "BaseTextView.h"

@interface BaseTextView() {
}

@end

@implementation BaseTextView
@synthesize backgroundImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundImage = nil;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundImage = nil;
        [self removePadding];
    }
    return self;
}

- (void)removePadding {
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    
    if ([[vComp objectAtIndex:0] intValue] >= 7) {
        [self setTextContainerInset:UIEdgeInsetsZero];
        self.textContainer.lineFragmentPadding = 0;
    } else {
        self.contentInset = UIEdgeInsetsMake(-8, -8, 0, 0);
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (self.backgroundImage != nil)
        [self.backgroundImage drawInRect:self.bounds];
}

@end
