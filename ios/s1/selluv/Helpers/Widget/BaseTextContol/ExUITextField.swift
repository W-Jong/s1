//
//  ExUITextField.swift
//  y
//
//  Created by dev1 on 9/17/17.
//  Copyright © 2017 kty. All rights reserved.
//

import UIKit

class ExUITextField: UITextField {

    func setPadding(leftPadding : Int, rightPadding : Int) {
        let leftPaddingView : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: leftPadding, height: leftPadding))
        self.leftViewMode = UITextFieldViewMode.always
        self.leftView = leftPaddingView
        
        let rightPaddingView : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: rightPadding, height: rightPadding))
        self.rightViewMode = UITextFieldViewMode.always
        self.rightView = rightPaddingView
    }
    
    func setPadding(padding : Int, placeholder : String) {
        let leftPaddingView : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: padding, height: padding))
        self.leftViewMode = UITextFieldViewMode.always
        self.leftView = leftPaddingView
        
        let rightPaddingView : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: padding, height: padding))
        self.rightViewMode = UITextFieldViewMode.always
        self.rightView = rightPaddingView
    }
    
    func setBorder(borderWidth : CGFloat, borderColor : UIColor) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setFont(fontName : String, fontSize : CGFloat) {
        self.font = UIFont.init(name: fontName, size: fontSize)
    }
}
