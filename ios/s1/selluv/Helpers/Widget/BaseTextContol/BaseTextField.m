//
//  BaseTextField.m
//  Colorencar
//
//  Created by Rabbit on 2014. 3. 25..
//  Copyright (c) 2014년 WG. All rights reserved.
//

#import "BaseTextField.h"

@implementation BaseTextField

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.highlightColor = [UIColor clearColor];
        self.highlightImage = nil;
        self.defaultImage = nil;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// 배경이미지를 설정한다.
- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state {
    if (state == UIControlStateNormal) {
        m_imgNormal = image;
    } else {
        m_imgFocus = [[UIImage alloc] initWithCGImage:image.CGImage];
    }
    [self setNeedsDisplay];
}

- (void)setHighlightColor:(UIColor *)highlightColor {
    [self setBackgroundColor:highlightColor];
    _highlightColor = highlightColor;
}

// 현재의 컨트롤의 상태를 설정한다.
- (void)setState:(UIControlState)state {
    m_state = state;
    [self setNeedsDisplay];
}

// 패딩을 설정한다.
- (void)setPadding:(float)padding {
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, padding, padding)];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    [self setLeftView:spaceView];
//    [spaceView setBackgroundColor:[UIColor blueColor]];
    
    spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, padding, padding)];
    [self setRightViewMode:UITextFieldViewModeAlways];
    [self setRightView:spaceView];
//    [spaceView setBackgroundColor:[UIColor greenColor]];
    
    m_padding = padding;
}

- (void)drawRect:(CGRect)rect
{
    if (m_state == UIControlStateNormal && m_imgNormal != nil) {
        [m_imgNormal drawInRect:rect];
    } else if (m_state == UIControlStateHighlighted && m_imgFocus != nil) {
        [m_imgFocus drawInRect:rect];
    } else {
        [super drawRect:rect];
    }
    
    // Drawing code
}

@end
