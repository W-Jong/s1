//
//  BaseTextView.h
//  PriceTalk
//
//  Created by Tiger on 2014. 3. 25..
//  Copyright (c) 2014년 WG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTextView : UITextView

@property (nonatomic, strong) UIImage *backgroundImage;

- (void)removePadding;

@end
