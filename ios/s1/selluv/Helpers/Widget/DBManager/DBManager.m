//
//  DBManager.m
//
//  Created by PJH on 09s/03/18.
//  Copyright © 2018 PJH. All rights reserved.
//

#import "DBManager.h"
#import "UserModel.h"
#import "KeywordModel.h"

static DBManager * sharedInstance = nil;
static sqlite3 *database = nil;

@interface DBManager()

- (BOOL)createDB;
- (void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;
- (NSArray *)loadDataFromDB:(NSString *)query;
- (void)executeQuery:(NSString *)query;

@end

@implementation DBManager

#define DB_FILE_NAME            @"SELLUV.db"
#define USER_TABLE_NAME         @"USERTABLE"
#define KEYWORD_TABLE_NAME      @"KEYWORDTABLE"

+ (DBManager *) getSharedInstance {
    
    if(!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance createDB];
    }
    
    return sharedInstance;
}

- (BOOL) createDB {
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent: DB_FILE_NAME]];
    
    BOOL isSuccess = YES;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = [[NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (USER_UID TEXT, NICKNAME TEXT, LOGINTYPE, USERID, USERPASS , PROFILE TEXT)", USER_TABLE_NAME] UTF8String];
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create music table %s", errMsg);
            }
            
            const char *sql_stmt1 = [[NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (KEYWORD TEXT, COUNT TEXT, PROFILE TEXT)", KEYWORD_TABLE_NAME] UTF8String];
            if (sqlite3_exec(database, sql_stmt1, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create music table %s", errMsg);
            }

            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    
    return isSuccess;
}

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
//    // Set the database file path.
//    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        if(sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Check if the query is non-executable.
            if (!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                        }
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                }
            }
            else {
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
                int executeQueryResults = sqlite3_step(compiledStatement);
                if (executeQueryResults == SQLITE_DONE) {
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            }
        }
        else {
            // In the database cannot be opened then show the error message on the debugger.
            NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
        
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
}

- (NSArray *) loadDataFromDB:(NSString *) query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

//In this one, there is not a return value.
//However, the affectedRows property can be used to verify whether there were any changes or not after having executed a query.
- (void) executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

//////////////////////////////////////////////////
// Save & Load Room List from(to) DB
//////////////////////////////////////////////////

- (BOOL)isExistUser:(UserModel*)user {
    
    NSArray * exist = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM %@ WHERE USER_UID = '%@'", USER_TABLE_NAME, user.usrUid]];
    
    if (exist.count > 0)
        return true;
    
    return false;
}

- (void)createUser:(UserModel*)user {
    
    if(![self isExistUser:user]) {

        NSString * saveQuery = [NSString stringWithFormat:@"INSERT INTO %@(USER_UID, NICKNAME, LOGINTYPE, USERID, USERPASS, PROFILE) VALUES(\"%@\", \"%@\", \"%@\", \"%@\" , \"%@\" , \"%@\")",
                                USER_TABLE_NAME, user.usrUid, user.usrNcknm, user.loginType, user.usrId, user.usrPass, user.profileImg];
        
        // Execute the query.
        [self executeQuery:saveQuery];
        
    } else {
        //이미 usruid가 존재하면 업데이트
        NSString * saveQuery =[NSString stringWithFormat:@"UPDATE FROM %@ SET NICKNAME = '%@', LOGINTYPE = '%@', USERID = '%@', USERPASS = '%@', PROFILE = '%@' WHERE USER_UID = '%@'", USER_TABLE_NAME, user.usrNcknm, user.loginType, user.usrId, user.usrPass, user.profileImg, user.usrUid];
        [self executeQuery:saveQuery];
    }
}

- (void)deleteUser:(NSString*)_uid {
    
    NSString* delQuery = [NSString stringWithFormat:@"DELETE FROM %@ WHERE USER_UID = '%@'", USER_TABLE_NAME, _uid];
    
    // Execute the query.
    [self executeQuery:delQuery];
}

- (NSMutableArray *)getUsers {
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM %@", USER_TABLE_NAME];
    
    NSArray * results = [self loadDataFromDB:query];
    
    NSMutableArray* usrs = [NSMutableArray array];
    
    for (int i = 0; i < results.count; i++) {
        
        NSArray* result = [results objectAtIndex:i];
        
        NSString *uid = [result objectAtIndex:0];
        NSString *nickname = [result objectAtIndex:1];
        NSString *type = [result objectAtIndex:2];
        NSString *_id = [result objectAtIndex:3];
        NSString *pass = [result objectAtIndex:4];
        NSString *profile = [result objectAtIndex:5];
        
        UserModel* usr = [UserModel alloc];
        usr.usrUid = uid;
        usr.usrNcknm = nickname;
        usr.loginType = type;
        usr.usrId = _id;
        usr.usrPass = pass;
        usr.profileImg = profile;
        [usrs addObject:usr];
    }
    
    return usrs;
}

- (void)clearDB {
    
    NSString * delQuery = [NSString stringWithFormat:@"DELETE FROM %@", USER_TABLE_NAME];
    [self executeQuery:delQuery];

    NSString * delQuery1 = [NSString stringWithFormat:@"DELETE FROM %@", KEYWORD_TABLE_NAME];
    [self executeQuery:delQuery1];
}

- (BOOL)isExistKeyword:(KeywordModel*)key {
    
    NSArray * exist = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM %@ WHERE KEYWORD = '%@'", KEYWORD_TABLE_NAME, key.keyword]];
    
    if (exist.count > 0)
        return true;
    
    return false;
}

- (void)createKeyword:(KeywordModel*)key {
    
    if(![self isExistKeyword:key]) {
        //KEYWORD TEXT, COUNT TEXT, PROFILE TEXT
        NSString * saveQuery = [NSString stringWithFormat:@"INSERT INTO %@(KEYWORD, COUNT, PROFILE) VALUES(\"%@\", \"%@\",  \"%@\")",
                                KEYWORD_TABLE_NAME, key.keyword, key.count, key.profileImg];
        
        // Execute the query.
        [self executeQuery:saveQuery];
        
    } else {
        //이미 usruid가 존재하면 업데이트
        NSString * saveQuery =[NSString stringWithFormat:@"UPDATE FROM %@ SET COUNT = '%@', PROFILE = '%@' WHERE KEYWORD = '%@'", KEYWORD_TABLE_NAME, key.count, key.profileImg, key.keyword];
        [self executeQuery:saveQuery];
    }
}

- (void)deleteKeyword{
    
    NSString* delQuery = [NSString stringWithFormat:@"DELETE FROM %@", KEYWORD_TABLE_NAME];
    
    // Execute the query.
    [self executeQuery:delQuery];
}

- (NSMutableArray *)getKeywords {
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM %@", KEYWORD_TABLE_NAME];
    
    NSArray * results = [self loadDataFromDB:query];
    
    NSMutableArray* usrs = [NSMutableArray array];
    
    for (int i = 0; i < results.count; i++) {
        
        NSArray* result = [results objectAtIndex:i];
        
        NSString *keyword = [result objectAtIndex:0];
        NSString *count = [result objectAtIndex:1];
        NSString *profile = [result objectAtIndex:2];
        
        KeywordModel* dicKey = [KeywordModel alloc];
        dicKey.keyword = keyword;
        dicKey.count = count;
        dicKey.profileImg = profile;
        [usrs addObject:dicKey];
    }
    
    return usrs;
}

@end
















