//
//  UserModel.h
//  SELLUV
//
//  Created by PJH. on 09/03/18.
//  Copyright © 2018 PHH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property(nonatomic, retain)NSString *usrUid;
@property(nonatomic, retain)NSString *usrNcknm;
@property(nonatomic, retain)NSString *usrId;
@property(nonatomic, retain)NSString *usrPass;
@property(nonatomic, retain)NSString *profileImg;
@property(nonatomic, retain)NSString *loginType;


@end
