//
//  DBManager.h
//
//  Created by PJH on 09s/03/18.
//  Copyright © 2018 PJH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@class UserModel;
@class KeywordModel;
@interface DBManager : NSObject {

    NSString * databasePath;
}

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

+ (DBManager *)getSharedInstance;

- (void)createUser:(UserModel*) user;
- (NSMutableArray *)getUsers;
- (void)deleteUser:(NSString*) _uid;
- (BOOL)isExistUser:(UserModel*)user;
- (void)clearDB;
- (NSMutableArray *)getKeywords;
- (void)deleteKeyword;
- (void)createKeyword:(KeywordModel*)key;
- (BOOL)isExistKeyword:(KeywordModel*)key;
@end
