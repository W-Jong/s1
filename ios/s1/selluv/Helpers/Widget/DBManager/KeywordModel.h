//
//  KeywordModel.h
//  SELLUV
//
//  Created by PJH. on 11/04/18.
//  Copyright © 2018 PHH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeywordModel : NSObject

@property(nonatomic, retain)NSString *keyword;
@property(nonatomic, retain)NSString *count;
@property(nonatomic, retain)NSString *profileImg;

@end
