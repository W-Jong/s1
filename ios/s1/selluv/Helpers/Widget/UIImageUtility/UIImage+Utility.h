//
//  UIImage+Utility.h
//
//  Created by sho yakushiji on 2013/05/17.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utility)

+ (UIImage*)fastImageWithData:(NSData*)data;
+ (UIImage*)fastImageWithContentsOfFile:(NSString*)path;

- (UIImage*)deepCopy;

- (UIImage*)grayScaleImage;

- (UIImage*)resize:(CGSize)size;
- (UIImage*)aspectFit:(CGSize)size;
- (UIImage*)aspectFill:(CGSize)size;
- (UIImage*)aspectFill:(CGSize)size offset:(CGFloat)offset;

- (UIImage*)crop:(CGRect)rect;

- (UIImage*)cropCircle;

- (UIImage*)maskedImage:(UIImage*)maskImage;
- (UIImage*)maskedImage:(UIImage*)maskImage atPoint:(CGPoint)maskPoint;

- (UIImage*)mergeImage:(UIImage *)mergeImage withRect:(CGRect)mergeRect;

- (UIImage*)gaussBlur:(CGFloat)blurLevel;       //  {blurLevel | 0 ≤ t ≤ 1}

- (UIImage*)applyPixellateEffect:(CGFloat)value;   // level | 0 ≤ v ≤ 1

+ (UIImage*)createRotatedImage:(UIImage *)originalImage degrees:(float)degrees;

@end
