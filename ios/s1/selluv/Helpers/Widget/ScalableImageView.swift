//
//  ScalableImageView.swift
//  selluv
//
//  Created by Gambler Pan on 8/1/18.
//  Copyright © 2018 K. All rights reserved.
//

import UIKit
import Kingfisher

class ScalableImageView: UIScrollView, UIScrollViewDelegate {

    var m_strFileURL : String!
    var m_imageView : UIImageView!
    
    func setFileURL(_ strFileURL : String) {
        m_strFileURL = strFileURL
        m_imageView.kf.setImage(with: URL(string: m_strFileURL))
        resetLayout()
    }
    
    func setImage(_ image : UIImage) {
        m_imageView.image = image
        
        // 2018-01-15 added by Gambler
        // 해당 이미지뷰를 콘텐트의 비율에 맞춰 현시하도록 수정
        resetLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initUI()
    }
    
    override func awakeFromNib() {
        self.initUI()
    }
    
    func initUI() {
        m_imageView = UIImageView()
        m_imageView.contentMode = UIViewContentMode.scaleAspectFill
        self.addSubview(m_imageView)
        
        self.delaysContentTouches = false
        self.delegate = self
        self.maximumZoomScale = 5.0
        self.minimumZoomScale = 1.0
//        self.bounces = false
        self.contentSize = CGSize(width: self.frame.width, height: self.frame.height)
    }
    
    func resetLayout() {
        if let image = m_imageView.image {
//            if (self.frame.size.width / self.frame.size.height) <= (image.width / image.height) {
//                m_imageView.frame = CGRect(x: 0, y: (self.frame.size.height - image.height / image.width * self.frame.size.width) / 2, width: self.frame.size.width, height: image.height / image.width * self.frame.size.width)
//            } else {
//                m_imageView.frame = CGRect(x: (self.frame.size.width - image.width / image.height * self.frame.size.height) / 2, y: 0, width: image.width / image.height * self.frame.size.height, height: self.frame.size.height)
//                print(m_imageView.frame)
//            }
        
            m_imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            let frame = AVMakeRect(aspectRatio: m_imageView.frame.size, insideRect: CGRect(origin: .zero, size: CGSize(width: self.frame.size.width, height: self.frame.size.height)))
            m_imageView.frame = frame
        }
    
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return m_imageView
    }
    
    override func touchesShouldBegin(_ touches: Set<UITouch>, with event: UIEvent?, in view: UIView) -> Bool {
        return false
    }

}
