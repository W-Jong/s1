//
//  Global.swift
//  
//
//  Created by PJh. on 01/03/18.
//  Copyright © 2016 PJh C. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

//////////////////////////////////////////////////////////////////////
// MARK: - Public const
//////////////////////////////////////////////////////////////////////

//------------------------------
// Url
//------------------------------

let Server_Url =        "http://13.125.35.211/api"

enum F_API: String {
    
    //alarm controller
    case ALARM                                =   "/alarm"                  // 알림 목록 얻기
    case ALARM_COUNT                          =   "/alarm/count"            // 알림 갯수 얻기

    //brand controller
    case BRAND                                =   "/brand"                  //모든 부렌드 목록 얻기
    case BRAND_REG                            =   "/brand_reg"              //등록
    case BRAND_FAME                           =   "/brand/fame"            //인기 브랜드 목록 얻기(10개)
    case BRAND_FAME_ALL                       =   "/brand/fame/all"         //인기 브랜드 목록 얻기(전체)
    case BRAND_LIKE                           =   "/brand/like"             //팔로우 브렌드 목록 얻기, 말티 브렌드 팔로우 취소하기, 팔로우하기
    case BRAND_LIST                           =   "/brand/list"            //모든 브랜드 디테일 목록얻기
    case BRAND_RECOMMENDED                    =   "/brand/recommended"     //추천 브렌드 목록 얻기
    case BRAND_DETAIL                         =   "/brand/{brandUid"       //브렌드 상세보기
    case BRAND_ONE_LIKE                       =   "/brand/{brandUid/like"       //브렌드 팔로우하기, 취소하기
    case BRAND_DETAIL_LIST                    =   "/brand/{brandUid}/list"  //브렌드 상품군에 따르는 상품목록 얻기
    case BRAND_MODELNAMES                     =   "/brand/{brandUid}/modelNames"  //브랜드에 따르는 추천모델명 리스트 얻기
    
    //category controller
    case CATEGORY                             =   "/category"  //카테고리 상세
    case CATEGORY_DETAIL                      =   "/category/{categoryUid}/detail" //카테고리별 인기브랜드 및 하위카테고리 얻기
    case CATEGORY_PDT                         =   "/category/{categoryUid}/pdt" //해당 카테고리에 속하는 상품목록얻기(브랜드기준)
    case CATEGORY_SIZE                        =   "/category/{categoryUid}/size" //사이즈 목록 얻기
    case CATEGORY_SUB                         =   "/category/{categoryUid}/sub" //하위 카테고리 목록

    //check-app controller
    
    //common controller
    case COMMON_UPLOAD                        =   "/common/upload"          // 한개 파일 업로드
    case COMMON_UPLOADS                       =   "/common/uploads"         // 여러개 파일 업로드
    
    //deal controller  거래관리
    case DEAL                                 =   "/deal"                 // 
    case DEAL_BUY                             =   "/deal/buy"             // 상품구매
    case DEAL_NEGO                            =   "/deal/nego"            // 네고신청
    case DEAL_DETAIL                          =   "/deal/{dealUid}"       // 주문상세
    case DEAL_CANCEL                          =   "/deal/{dealUid}/cancel"  // 구매자 주문취소
    case DEAL_CANCEL_SELLER                   =   "/deal/{dealUid}/cancel/seller"  // 판매자 판매거절
    case DEAL_COMPLETE                        =   "/deal/{dealUid}/complete"  // 구매자 거래확정
    case DEAL_DELIVERY                        =   "/deal/{dealUid}/delivery"  // 택배예약하기
    case DEAL_DELIVERY_CANCEL                 =   "/deal/{dealUid}/delivery/cancel"  // 택배예약 취소
    case DEAL_DELIVERY_INFO                   =   "/deal/{dealUid}/delivery/info"  // 택배예약 정보얻기
    case DEAL_DELIVERY_NUMBER                 =   "/deal/{dealUid}/deliveryNumber"  // 판매자 운송장 번호등록
    case DEAL_NEGO_ALLOW                      =   "/deal/{dealUid}/nego/allow"  // 판매자네고승인
    case DEAL_NEGO_ALLOW_COUNTER              =   "/deal/{dealUid}/nego/allow/counter"  // 구매자카운터네고승인
    case DEAL_NEGO_COUNTER                    =   "/deal/{dealUid}/nego/counter"  // 판매자 카운터 네고 요청
    case DEAL_NEGO_REFUSE                     =   "/deal/{dealUid}/nego/refuse"  // 판매자네고거절
    case DEAL_NEGO_REFUSE_COUNTER             =   "/deal/{dealUid}/nego/refuse/counter"  // 구매자카운터네고거절
    case DEAL_REFUND                          =   "/deal/{dealUid}/refund"  // 구매자 반품 신청
    case DEAL_REFUND_ALLOW                    =   "/deal/{dealUid}/refund/allow" // 판매자 반품 승인
    case DEAL_REFUND_COMPLETE                 =   "/deal/{dealUid}/refund/complete" // 판매자 반품확인
    case DEAL_REFUND_DISALLOW                 =   "/deal/{dealUid}/refund/disallow" // 판매자 반품 거절
    case DEAL_REVIEW                          =   "/deal/{dealUid}/review" // 후기작성
    case DEAL_SHARE                           =   "/deal/{dealUid}/share"  // 판매자 공유리워드 신청
    
    //home controller
    case HOME_FEED                            =   "/home/feed"              // 피드목록얻기
    case HOME_FEED_STYLE                      =   "/home/feed/style"        // 피드스타일 목록얻기
    case HOME_FAME                            =   "/home/fame"              // 인기목록얻기
    case HOME_FAME_STYLE                      =   "/home/fame/style"        // 인기스타일 목록얻기
    case HOME_NEW                             =   "/home/new"               // 신상품목록얻기
    case HOME_NEW_STYLE                       =   "/home/new/style"         // 신상품스타일 목록얻기

    //other controller
    case OTHER_BANNER                         =   "/other/banner"           // 배너얻기
    case OTHER_DELAYWORKINGDAY                =   "/other/delayWorkingDay"  // 영업일기준 날짜얻기
    case OTHER_EVENT                          =   "/other/event"            // 이벤트 목록 얻기
    case OTHER_DETAIL_EVENT                   =   "/other/event/{eventUid}"   // 이벤트 상세 얻기
    case OTHER_DELIVERY_COMPANIES             =   "/other/deliveryCompanies" // 택배사목록얻기
    case OTHER_FAQ                            =   "/other/faq"              // FAQ목록 얻기
    case OTHER_LICENSE                        =   "/other/license"          // 약관 얻기
    case OTHER_NOTICE                         =   "/other/notice"           // 공지사항 얻기
    case OTHER_DETAIL_NOTICE                  =   "/other/notice/[noticeUid]" // 공지사항 상세 얻기
    case OTHER_PROMOTION                      =   "/other/promotion"           // 프로모션 정보 얻기
    case OTHER_QNA                            =   "/other/qna"              //1:1문의목록 얻기 , 작성
    case OTHER_REPORT                         =   "/other/report"           //신고하기
    case OTHER_SETTING                        =   "/other/setting"           //시스템 설정정보 얻기
    case OTHER_USRWISH                        =   "/other/usrWish"          // 찾는 아이템 목록 얻기
    case OTHER_USRWISH1                       =   "/other/usrWish1"          // 찾는 상품 알림 설정
    case OTHER_USRWISHS                       =   "/other/usrWishs"          // 찾고있는 아이템 멀티 삭제
    case OTHER_VALET                          =   "/other/valet"           // 발렛신청
    
    //pdt controller
    case PDT                                  =   "/pdt"                    // 상품추가 , 상품 좋아요 취소하기, 상품 좋아요 하기
    case PDT_LIKE                             =   "/pdt/like"               // 좋아요 목록
    case PDT_LIKE_STYLE                       =   "/pdt/like/style"         // 좋아요 스타일 목록
    case PDT_LIKE_UPDATESTATUS                =   "/pdt/like/updateStatus"   // 좋아요 업데이트뱃지 표시여부
    case PDT_DELETE                           =   "/pdt/delete"             // 상품 삭제
    case PDT_DETAIL                           =   "/pdt/{pdtUid}"           // 상품 상세
    case PDT_MODIFIY                          =   "/pdt/{pdtUid}1"           // 상품 정보 수정
    case PDT_FAKE                             =   "/pdt/{pdtUid}/fake"      // 상품 가품신고 정보 얻기, 가품 신고하기
    case PDT_LIKE_LIST                        =   "/pdt/{pdtUid}/like"      // 좋아요 유저 목록얻기
    case PDT_RECOMMENDED                      =   "/pdt/{pdtUid}/recommended" // 상품상세 - 추천상품  목록 얻기
    case PDT_RELATIONSTYLE                    =   "/pdt/{pdtUid}/relationStyle" // 상품상세 - 연관스타일 목록얻기
    case PDT_REPLY_LIST                       =   "/pdt/{pdtUid}/reply_list"     // 상품 댓글 목록얻기
    case PDT_REPLY_WRITER                     =   "/pdt/{pdtUid}/reply_write" // 상품 댓글 추가허가
    case PDT_REPLY_DELETE                     =   "/pdt/reply/{replyUid}" // 상품 댓글 삭제
    case PDT_REPLY_USR_LIST                   =   "/pdt/{pdtUid}/reply/search"  // 상품 댓글기능 회원목록 얻기
    case PDT_WISH_LIST                        =   "/pdt/{pdtUid}/wish_list"  // 잇템 유저 목록 얻기
    case PDT_WISH                             =   "/pdt/{pdtUid}/wish"       // 상품잇템 추가하기

    
    //pdt-style controller
    case GET_PDTSTYLE                         =   "/pdtStyle"               // 상품 스타일 목록 얻기,
    case REG_PDTSTYLE                         =   "/pdtStyle1"              // 상품 스타일 등록
    case GET_PDTSTYLE_ALL                     =   "/pdtStyle/all"           // 전체 상품 스타일 목록 얻기
    case DEL_PDTSTYLE                         =   "/pdtStyle/{pdtStyleUid}1" // 상품 스타일 삭제
    case DETAIL_PDTSTYLE                      =   "/pdtStyle/{pdtStyleUid}2" // 스타일 팝업 상세
    case MULTI_REG_PDTSTYLE                   =   "/pdtStyle/{pdtUid}"      // 상품 스타일  다수등록
    
    //search controller
    case SEARCH                               =   "/search"                // 검색
    case SEARCH_COUNT                         =   "/search/count"          // 검색 결과 갯수
    case SEARCH_DETAIL                        =   "/search/detail"          // 상세검색
    case SEARCH_FAME                          =   "/search/fame"          // 인기검색 키워드 리스트 얻기
    case SEARCH_PRICERANGE                    =   "/search/priceRange"     // 상품 최대, 최소가격
    case SEARCH_RECOMMEND                     =   "/search/recommend"     // 추천검색 키워드 리스트 얻기
    case SEARCH_TAG                           =   "/search/tag"          // 개그검색
    
    //theme controller
    case THEME_RECOMMENDED                    =   "/theme/recommended"      // 테마록록 얻기
    case THEME_DETAIL                         =   "/theme"                // 테마상세
    
    //user controller
    case USER                                 =   "/user"                   //회원가입, 회원탈퇴
    case USER_ACCOUNTINFO                     =   "/user/accountInfo"       //계좌정보 업데이트
    case USER_ALARMSETTING                    =   "/user/alarmSetting"      // 알림설정 얻기, 업데이트
    case USER_BASIS                           =   "/user/basis"             // 회원기본정보 수정
    case USER_BLOCK                           =   "/user/block"             // 차단된 유저 목록 얻기
    case USER_CARD                            =   "/user/card"             // 카드목록 얻기,  카드 등록, 카드 선택,  카드 삭제
    case USER_CHECK_INVITECODE                =   "/user/check/inviteCode"  // 추천인 코드 유효성검사
    case USER_CHECK_SNSID                     =   "/user/check/snsId"       // sns아이디 중복검사
    case USER_CHECK_USRID                     =   "/user/check/usrId"        //아이디 중복검사
    case USER_CHECK_USRMAIL                   =   "/user/check/usrMail"      //이메일 증복검사
    case USER_DEVICETOKEN                     =   "/user/deviceToken"      //푸시토큰 업데이트
    case USER_FRIEND_ADDRESSBOOK              =   "/user/friend/addressBook"  //연락처 친구찾기
    case USER_FRIEND_FACEBOOK                 =   "/user/friend/facebook"     //페이스북 친구찾기
    case USER_HISTORY_BUY                     =   "/user/history/buy"       //회원구매내역
    case USER_HISTORY_MONEY                   =   "/user/history/money"     //셀럽머니 변경이력 얻기
    case USER_HISTORY_SELL                    =   "/user/history/sell"       //회원판매내역
    case USER_INVITEINFO                      =   "/user/inviteInfo"         //초대정보 얻기
    case USER_LIKEGROUP                       =   "/user/likeGroup"         //관심상품군 업데이트
    case USER_LOGIN                           =   "/user/login"             //회원로그인
    case USER_MYINFO                          =   "/user/myInfo"            // 회원 상세정보 얻기
    case USER_OTHER                           =   "/user/other"            // 회원기타정보 수정
    case USER_PASSWORD                        =   "/user/password"          //비밀번호 변경
    case USER_PASSWORD_FIND                   =   "/user/password/find"     //비밀번호 찾기
    case USER_PROFILE                         =   "/user/profile"           //프로필 업데이트
    case USER_PROFILEIMG                      =   "/user/profileImg"         //프로필 이미지 업데이트
    case USER_RECOMMEND                       =   "/user/recommend"         //추천유저목록
    case USER_SELLERSETTING                   =   "/user/sellerSetting"     //판매자 설정 업데이트
    case USER_SNS                             =   "/user/sns"               //SNS
    case SET_USER_SNS                         =   "/user/sns/set"           //SNS연동하기
    case DEL_USER_SNS                         =   "/user/sns/del"            //SNS연동삭제
    case USER_SNSSTATUS                       =   "/user/snsStatus"         //SNS연동정보 얻기
    case USER_DETAILINFO                      =   "/user/{usrUid}"         //회원 상세정보 얻기
    case USER_UNBLOCK                         =   "/user/{usrUid}/block1"    //유저 차단 해제하기
    case USER_BLOCKING                        =   "/user/{usrUid}/block2"    //유저 차단 하기
    case USER_FOLLOW                          =   "/user/{usrUid}/follow"   //유저 팔로우 해제하기,  팔로우 하기
    case USER_FOLLOWER                        =   "/user/{usrUid}/follower"  //팔로워 유저목록 얻기
    case USER_FOLLOWING                       =   "/user/{usrUid}/following" //팔로잉 유저목록 얻기
    case USER_HIDE                            =   "/user/{usrUid}/hide"     //유저 숨기기
    case USER_PDT                             =   "/user/{usrUid}/pdt"      //회원의 아이템 목록 얻기
    case USER_REVIEW                          =   "/user/{usrUid}/review"      //매너포인트 변경이력(거래후기) 얻기
    case USER_STYLE                           =   "/user/{usrUid}/style"    //회원의 스타일 목록 얻기
    case USER_WISH                            =   "/user/{usrUid}/wish"     //회원의 잇템 목록 얻기

    //webview cotnroller
    case WEB_ADDRESS                          =   "/web/address"     //주소검색
    case WEB_DANALSMS                         =   "/web/danalsms"     //다날본인인증
    case WEB_PAY                              =   "/web/payment/"     //결제진행
}


//------------------------------
// Parameter
//------------------------------

let kCode =             "errCode"
let kMsg =              "errMsg"
let kData =             "data"
let kMeta =             "meta"

//------------------------------
// Message
//------------------------------

let App_Name =          "셀럽"
let Error =             "오류"
let Menu =				"안내"


//------------------------------
// 셀럽 주소
//------------------------------
let SELLUV_NAME     =          "셀럽"
let SELLUV_CONTACT  =          "01049991004"
let SELLUV_ADDRESS  =          "서울틀별시 성동구 해탄로 15"

//------------------------------
// 배송조회 url
//------------------------------
let DELIVERY_URL    =          "https://m.search.daum.net/search?w=tot&q="

//------------------------------
// Color
//------------------------------

let Color_BG =          UIColor(hex: 0x426994)

//------------------------------
// Typedef
//------------------------------

enum ENotificationType: String {
    case updatePhoto =	"updatePhoto"
}

//------------------------------
// Push Typedef
//------------------------------
enum EPushType: String {
    case PUSH01_S_PAY_COMPLETED     =    "PUSH01_S_PAY_COMPLETED"
    case PUSH02_B_DELIVERY_SENT     =    "PUSH02_B_DELIVERY_SENT"
    case PUSH03_D_DEAL_COMPLETED    =    "PUSH03_D_DEAL_COMPLETED"
    case PUSH04_B_DEAL_CANCELED     =    "PUSH04_B_DEAL_CANCELED"
    case PUSH05_S_DEAL_CANCELED     =    "PUSH05_S_DEAL_CANCELED"
    case PUSH06_B_PDT_VERIFICATION  =    "PUSH06_B_PDT_VERIFICATION"
    case PUSH07_S_NEGO_SUGGEST      =    "PUSH07_S_NEGO_SUGGEST"
    case PUSH08_B_NEGO_UPDATED      =    "PUSH08_B_NEGO_UPDATED"
    case PUSH09_S_REFUND_REQUEST    =    "PUSH09_S_REFUND_REQUEST"
    case PUSH10_B_REFUND_UPDATED    =    "PUSH10_B_REFUND_UPDATED"
    case PUSH11_REWARD_INVITE       =    "PUSH11_REWARD_INVITE"
    case PUSH12_REWARD_REPORT       =    "PUSH12_REWARD_REPORT"
    case PUSH13                     =    "PUSH13"
    case PUSH14_FOLLOW_ME           =    "PUSH14_FOLLOW_ME"
    case PUSH15_LIKE_MY_PDT         =    "PUSH15_LIKE_MY_PDT"
    case PUSH16_ITEM_MY_PDT         =    "PUSH16_ITEM_MY_PDT"
    case PUSH17_LIKE_PDT_SALE       =    "PUSH17_LIKE_PDT_SALE"
    case PUSH18_FOLLOWING_USER_PDT  =    "PUSH18_FOLLOWING_USER_PDT"
    case PUSH19_FOLLOWING_USER_STYLE =    "PUSH19_FOLLOWING_USER_STYLE"
    case PUSH20_FOLLOWING_BRAND_PDT =    "PUSH20_FOLLOWING_BRAND_PDT"
    case PUSH21_FINDING_PDT         =    "PUSH21_FINDING_PDT"
    case PUSH22_EVENT               =    "PUSH22_EVENT"
    case PUSH23_NOTICE              =    "PUSH23_NOTICE"
    case PUSH24_PDT_REPLY           =    "PUSH24_PDT_REPLY"
    case PUSH25_REPLY_REPLY         =    "PUSH25_REPLY_REPLY"
    case PUSH26_MENTIONED_ME        =    "PUSH26_MENTIONED_ME"
    
}

enum ELoginStep: Int {
    case intro = 0
    case sns
    case photo
}

enum EScrollDirection {
    case none
    case up
    case down
}

enum EKind : Int {
    case man = 0
    case woman
    case kids
}

enum SELL {
    case sell
    case valet
}
var actionType = SELL.sell

enum ETab : Int {
    case all = 0
    case men
    case women
    case kids
    case style
}

//search result page
enum WherePage : Int {
    case shop = 0
    case search
}

//seller page or buy page
enum DealPage : Int {
    case BUY = 0
    case SELL
}

//webview pages
enum Webview : Int {
    case DANAL_SMS = 0
    case PAY
}

//login type
let SIGNUP     = 1001
let SKIP       = 1002

let MenCategory1 = [
    (name: "모두 보기", val: 1, img: "ic_shop_menlist"),
    (name: "의류", val: 5, img: "ic_shop_menlist_clothing"),
    (name: "가방", val: 6, img: "ic_shop_menlist_bag"),
    (name: "슈즈", val: 7, img: "ic_shop_menlist_shoes"),
    (name: "패션소품", val: 8, img: "ic_shop_menlist_accessories"),
    (name: "시계/쥬얼리", val: 9, img: "ic_shop_menlist_watch")
]

let WomenCategory1 = [
    (name: "모두 보기", val: 2, img: "ic_shop_womenlist"),
    (name: "의류", val: 10, img: "ic_shop_womenlist_clothing"),
    (name: "핸드백", val: 11, img: "ic_shop_womenlist_handbag"),
    (name: "슈즈", val: 12, img: "ic_shop_womenlist_shoes"),
    (name: "패션소품", val: 13, img: "ic_shop_womenlist_accessories"),
    (name: "시계/쥬얼리", val: 14, img: "ic_shop_womenlist_jewelry")
]

let KidsCategory1 = [
    (name: "모두 보기", val: 4, img: "ic_shop_kids_list"),
    (name: "베이비", val: 15, img: "ic_shop_kidslist_baby"),
    (name: "키즈보이", val: 16, img: "ic_shop_kidslist_kidsboy"),
    (name: "키즈걸", val: 17, img: "ic_shop_kidslist_kidsgirl"),
    (name: "틴보이", val: 18, img: "ic_shop_kidslist_teenboy"),
    (name: "틴걸", val: 19, img: "ic_shop_kidslist_teengirl")
]

let GitaAreas = ["가거도", "가덕도", "가사도", "가사혈도", "가왕도", "가의도", "가조도", "가파도", "각이도", "각흘도", "간월도", "갈도", "갈도"
    , "갈목도", "강화도" , "개도", "개도", "개야도", "거륜도", "거문도", "거제도", "거금도", "거사도", "고개도", "고금도", "고대도", "고마도", "고사도", "고이도", "고파도", "고하도", "곤리도", "곽도", "관리도", "관매도", "관사도", "광도", "교동도", "구도", "구룡도", "국도", "국화도", "굴업도", "금당도", "금오도", "금일도", "금죽도", "금호도", "금호도", "금호도", "기린도", "기도", "나발도", "나배도", "나치도", "남가력도", "남해도", "납도", "낭도", "내나로도", "내도", "내장도", "내병도", "내죽도", "내초도", "내파수도", "넙도", "넙도", "노도", "노력도", "노록도", "노화도", "녹도", "눌도", "눌옥도", "눌차도", "늑도", "능산도", "다랑도", "다물도", "다사도", "달도", "달리도", "당사도", "대각씨도", "대각이도", "대경도",
      "대기점도", "대도", "대길산도", "대난지도", "대노록도", "대두라도", "대둔도", "대륵도", "대마도", "대모도", "대무의도", "대부도", "대석만도",
      "대수압도", "대야도", "대연평도", "대이작도","대장구도", "대장도", "대청도", "대초도", "대태이도", "대포작도", "대화도", "대횡간도", "대흑산도", "덕적도", "도초도", "독거도", "독도", "돌산도", "동거차도", "동검도","동도", "동서우이도", "동화도", "두리도", "두미도", "두지도", "둔병도", "득량도", "마도", "마라도", "마삭도", "마진도", "막금도", "만재도", "만지도", "말도", "매도", "매물도", "매화도", "맹골도", "명도", "모도", "모항도", "목도", "묘도", "무녀도", "무위도", "문갑도", "문병도", "미덕도", "미륵도", "미법도", "박지도", "반월도", "방축도", "별학도", "백아도", "백야도", "백야도", "백일도", "백령도", "병풍도", "보길도", "볼음도", "봉도", "부남도", "부도", "부도", "부소도", "분점도", "불도", "불탄도", "비견도", "비금도", "비산도", "비안도", "비양도", "비진도", "비토도", "비응도", "빙도", "사도", "사량도", "사양도", "사옥도", "사치도", "사후도", "산달도", "삼간도" , "삽시도", "상낙월도", "상구자도", "상노대도", "상도", "상마도", "삽시도", "상수치도", "상왕등도", "상조도", "상추자도", "상하태도", "상하죽도", "상태도", "서만도", "상화도", "생일도", "서거차도", "서검도", "서넙도", "서도", "서소우이도", "서화도", "석도", "석만도", "석모도", "선감도", "선도", "선유도", "선재도", "선유도", "선미도", "성남도", "섭도", "섬돌모루도", "섬달천도", "세어도", "소각씨도", "소거문도", "소경도", "소기점도", "소난지도", "소도", "소두라도", "소랑도", "소록도", "소륵도", "소마도", "소무의도", "소매물도", "소모도", "소수압도", "소석만도", "소성만도", "소악도", "소야도", "소야미도", "소연평도", "소이작도", "소지도", "소청도", "소화도", "소해물도", "소횡간도", "손죽도", "송도", "송여자도", "송이도", "수도", "수락도", "수우도", "수치도", "수항도" , "슬도", "승봉도", "시도", "시산도", "시하도", "식도", "신도", "신미도", "신수도", "신지도", "신진도", "실리도", "아차도", "안도", "안마도", "안면도", "안좌도", "암태도", "압해도", "애도", "야미도", "약산도", "양덕도", "양도", "어도", "어룡도", "어불도", "어의도", "어청도", "여자도", "연대도", "연도", "연태도", "연흥도", "연화도", "영도","영산도","영종도", "영흥도", "예작도", "오곡도", "오동도", "오동도", "오비도", "옥도", "옥도", "와도", "완도", "외나로도", "외병도", "외연도","외죽도", "외파수도", "요력도", "욕지도", "용유도", "용호도", "우곶도", "우도", "우음도", "우리도", "우이도", "운두도", "운렴도", "울도", "울릉도", "웅도", "원도", "원산도", "원주도", "월도", "월등도", "월호도", "위도", "유뷰도", "육도", "율도", "읍도", "이수도", "임자도", "임하도", "입도", "자라도", "자란도", "자봉도", "자월도" , "자은도", "잠도", "장고도", "장구도", "장도", "장병도", "장봉도", "장사도", "장사도", "장산도", "장자도", "장재도", "장좌도", "재원도", "저도", "적금도", "재원도", "접도", "정금도", "제도", "제부도", "제주도"
        , "주문도", "조발도", "좌도", "주란도", "주문도", "주지도", "죽굴도", "죽도", "죽도", "죽도", "죽항도", "중마도", "중태도", "증도", "지심도", "지주도", "지죽도", "진도", "진도", "진목도", "진지도", "창선도", "첨도", "청등도", "청산도", "초도草島", "초란도", "초양도", "초완도", "추도", "추봉도", "추포도", "충도", "취도", "측도", "치도", "칠발도", "칠천도", "탄도", "탄항도", "태인도", "토도", "팔금도", "평도", "평사도", "풍도", "하구자도", "하낙월도", "하노대도", "하도", "하도", "하마도", "하백도", "하왕등도", "하의도", "하조도", "하추자도", "하태도", "하화도", "하화도", "학림도", "학림도", "해간도", "해도", "한산도", "행담도", "허륙도", "허사도", "허우도", "혈도", "형도", "호도", "홍도", "화도", "화도", "화도", "화태도", "황덕도", "황도", "황도", "황마도", "황산도", "황제도", "회도", "횡간도", "횡간도", "횡도", "효자도", "후장구도", "흑일도"]

let bigCategory = ["의류", "가방", "슈즈", "패션소품", "시계/쥬얼리", "의류", "핸드백", "슈즈", "패션소품", "시계/쥬얼리", "베이비", "키즈보이","키즈걸", "틴보이", "틴걸"]

let index_eng = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"]
let index_kor = ["ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ", "ㅂ", "ㅅ", "ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"]

let CatrgoryTypeName = ["RECOMMEND", "NEW", "FAME", "PRICELOW", "PRICEHIGH"]


//////////////////////////////////////////////////////////////////////
// MARK: - Animation Time Constants
//////////////////////////////////////////////////////////////////////

let LoginViewAnimationTime = 1.0
let TabAnimationTime = 0.3
let MenuAnimationTime = 0.5
let SellAnimationTime = 0.3
let Scroll2TopAnimationTime = 0.8


//////////////////////////////////////////////////////////////////////
// MARK: - Color vars
//////////////////////////////////////////////////////////////////////

let ColorWhite = UIColor.init(red: 0xFF / 255.0, green: 0xFF / 255.0, blue: 0xFF / 255.0, alpha: 1.0)
let ColorBtnInactive = UIColor.init(red: 0x99 / 255.0, green: 0x99 / 255.0, blue: 0x99 / 255.0, alpha: 1.0)
let ColorBtnActive = UIColor.init(red: 0x42 / 255.0, green: 0xC2 / 255.0, blue: 0xFF / 255.0, alpha: 1.0)


//////////////////////////////////////////////////////////////////////
// MARK: - Define variable`
//////////////////////////////////////////////////////////////////////
//login Type
let NORMAL          =    0
let FACEBOOK        =    1
let NAVER           =    2
let KAKAOTALK       =    3

//////////////////////////////////////////////////////////////////////
// MARK: - Public vars
//////////////////////////////////////////////////////////////////////

var gProgress               =			ProgressUtil()
var gMeInfo                 =           UserInfo.getMe()
var gPdtRegInfo             =           PdtRegInfo()
var gSearchInfo             =           SearchInfo()
var gUsrAddress             =           [UsrAddressDao!]()
var gPushToken              =           ""
var gPushUid                =           ""
var gPushType               =           ""
var gCategoryUid            =           0

// 마지막으로 본 페지탭을 임시로 저장하는 변수
var gMainPageIndex          =           1
var gShopPageIndex          =           0
var gBrandPageIndex         =           0

// Notification
let NOTI_SORTTYPE_CHANGE            = "noti_sorttype_change"
let NOTI_CATEGORY_CHANGE            = "noti_category_change"
let NOTI_BRAND_CHANGE               = "noti_brand_change"
let NOTI_SIZE_CHANGE                = "noti_size_change"
let NOTI_CONDITION_CHANGE           = "noti_condition_change"
let NOTI_COLOR_CHANGE               = "noti_color_change"
let NOTI_MYPAGE_CHANGE              = "noti_mypage_change"
let NOTI_PROFILE_CHANGE             = "noti_profile_change"
let NOTI_BG_CHANGE                  = "noti_bg_change"
let NOTI_LOGOUT                     = "noti_logout"
let NOTI_PDT_DEL                    = "noti_pdt_del"
let NOTI_RE_VISIT                   = "noti_re_visit"
let NOTI_SEARCH                     = "noti_search"

//Server Error Code

let OK                                          = 0
let USER_NOT_EXISTS                             = 100 // "이미 탈퇴하였거나 존재하지 않는 회원입니다.");
let USER_ERROR_ACCESS_TOKEN                     = 101 // "인증시간이 만료되었거나 다른 기기에서 로그인 되었습니다.");
let USER_EMAIL_DUPLICATED                       = 102 // "이미 사용중인 이메일입니다.");
let USER_ID_DUPLICATED                          = 103 // "이미 사용중인 아이디입니다.");
let USER_SNSID_DUPLICATED                       = 104 // "이미 연결된 SNS 아이디입니다.");
let USER_PERSON_DUPLICATED                      = 105 // "이미 가입되어 있는 회원입니다.");
let USER_INVITE_CODE_ERROR                      = 106 // "잘못된 추천인 코드입니다.");
let USER_AGE_NOT_ALLOWED                        = 107 // "미성년자는 회원가입할 수 없습니다.");
let USER_ERROR_OLD_PASSWORD                     = 108 // "현재 비밀번호가 일치하지 않습니다.");
let USER_ID_NICKNAME_SPACE_NOT_ALLOWED          = 109 // "아이디 및 닉네임에는 공백을 사용할 수 없습니다.");
let USER_TEMP_NOT_PERMISSION                    = 150 // "임시회원은 요청에 대한 권한이 없습니다.");
let PDT_NOT_EXIST                               = 200 // "이미 삭제되었거나 존재하지 않는 상품입니다.");
let BRAND_NOT_EXIST                             = 201 // "이미 삭제되었거나 존재하지 않는 브랜드입니다.");
let PDT_STYLE_NOT_EXIST                         = 202 // "이미 삭제되었거나 존재하지 않는 상품스타일입니다.");
let CATEGORY_NOT_EXIST                          = 203 // "이미 삭제되었거나 존재하지 않는 카테고리입니다.");
let THEME_NOT_EXIST                             = 204 // "이미 삭제되었거나 존재하지 않는 테마입니다.");
let PROMOTION_NOT_EXIST                         = 205 // "이미 삭제되었거나 존재하지 않는 프로모션코드입니다.");
let CARD_NOT_EXIST                              = 206  // "이미 삭제되었거나 존재하지 않는 카드입니다.");
let DEAL_NOT_FOUND                              = 300 // "이미 취소되었거나 존재하지 않는 거래입니다.");
let PAY_PRICE_OVER_ZERO                         = 301 // "결제금액은 0원 이상이여야 합니다.");
let PAY_PRICE_RANGE_ERROR                       = 302 // "허용범위안의 금액을 입력해주세요.");
let PAY_PDT_ALREADY_SOLD                        = 303 // "이미 판매완료된 상품입니다.");
let PAY_PDT_NOT_NORMAL                          = 303 // "현재 판매중인 상품이 아닙니다.");
let PAY_TIME_RANGE_ERROR                        = 304 // "해당 조작의 허용시간을 초과했습니다.");
let DELIVERY_NUMBER_FAILED                      = 320 // "운송장번호조회에 실패했습니다.");
let DELIVERY_HISTORY_NOT_EXIST                  = 321 // "예약한 택배정보가 없습니다.");
let DELIVERY_HISTORY_ALREADY_EXIST              = 322 // "이미 예약한 택배정보가 있습니다.");
let PERMITTION_NOT_ALLOWED                      = 500 // "요청에 대한 권한이 없습니다.");
let ALREADY_ADDED                               = 501 // "이미 추가되어 있습니다.");
let ALREADY_DELETED                             = 502 // "이미 삭제되었습니다.");
let PAYMENT_NOT_SUPPORT                         = 503 // "지원되지 않는 결제형식입니다.");
let ALREADY_LIKE_ADDED                          = 504 // "이미 좋아요 했습니다.");
let ALREADY_LIKE_DELETED                        = 505 // "이미 좋아요를 취소했습니다.");
let ERROR_LIKE_GROUP_REQUIRED                   = 506 // "관심상품군을 하나이상 선택해주세요.");
let PDT_ALREADY_SOLD                            = 507 // "이미 판매완료된 상품은 수정할 수 없습니다.");
let PAGE_NOT_FOUND                              = 900 // "잘못된 요청입니다.");
let INTENAL_SERVER_ERROR                        = 910 // "서버오류입니다.");
let DATABASE_ERROR                              = 920 // "데이터베이스 오류입니다.");
let FILE_UPLOAD_ERROR                           = 930 // "파일 업로드에 실패했습니다.");

let kServiceAppUrlScheme                        = "com.selluv.ios"

let kConsumerKey                                = "Khgw7OI4mtNP47Hv9Jwq"
let kConsumerSecret                             = "K94EMljc5Q"

//instagram      
let kClientId = "fea36ebbfb67488f9d715473d5fa6835"
let kRedirectUri = "http://selluv.com"

//channel
let kPluginKey = "a364fb24-bbaf-4c0b-8a48-646d8596d787"
