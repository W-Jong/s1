//
//  AppDelegate.swift
//  selluv
//
//  Created by kim on 02/12/2017.
//  modified by PJH on 08/03/18.

import UIKit
import FBSDKCoreKit
import TwitterKit
import Firebase
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var isForeground = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        EasyAnimation.enable()
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        let thirdConn : NaverThirdPartyLoginConnection = NaverThirdPartyLoginConnection.getSharedInstance()
        thirdConn.serviceUrlScheme = kServiceAppUrlScheme
        thirdConn.consumerKey = kConsumerKey
        thirdConn.consumerSecret = kConsumerSecret
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:"kQv6NICUfwsx8Nh0QfOFWnDsK", consumerSecret:"Zt2yyba9phC9o4GMLepMhujLpCJIaYMc8DBXqcpUd6jZLJK6iu")
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        //kakao
        if KOSession.isKakaoAccountLoginCallback(url) {
            return KOSession.handleOpen(url)
        }
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        return handled
        
//         return [[NaverThirdPartyLoginConnection getSharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        //kakao
        if KOSession.isKakaoAccountLoginCallback(url as URL!) {
            return KOSession.handleOpen(url as URL!)
        }
        return false
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        isForeground = false
        disconnectFcm()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        isForeground = true
        connectFcm()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        isForeground = true
        KOSession.handleDidBecomeActive()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let refreshedToken = Messaging.messaging().fcmToken {
            gPushToken = refreshedToken
            updatePushToken()
        }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        //kakao
        if KOSession.isKakaoAccountLoginCallback(url) {
            return KOSession.handleOpen(url)
        }
        let scheme = url.scheme
        if (scheme?.contains("fb"))! {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else if (scheme?.contains(kServiceAppUrlScheme))! { //naver
            //return [[NaverThirdPartyLoginConnection getSharedInstance] application:app openURL:url options:options];
            return NaverThirdPartyLoginConnection.getSharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation] as Any)
        }
        return true
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        phasePush(userInfo)
        completionHandler([.alert, .badge, .sound])
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        let userInfo = response.notification.request.content.userInfo
        phasePush(userInfo)
        completionHandler()
    }
    
    func disconnectFcm() {
        Messaging.messaging().shouldEstablishDirectChannel = false
        print("Disconnected from FCM.")
    }
    
    func connectFcm() {
        guard InstanceID.instanceID().token() != nil else {
            return;
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = true
        print("Connected to FCM.")
    }
    
    // 푸시토큰 업데이트
    func updatePushToken() {
        
        if gMeInfo.token == nil {
            return
        }
        
        gProgress.show()
        Net.updatePushToken(
            accessToken     : gMeInfo.token,
            deviceToken     : gPushToken,
            success: { (result) -> Void in
                gProgress.hide()
                
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func phasePush(_ info: [AnyHashable : Any]) {
        
        gPushType = info["alarmType"] as! String
        gPushUid = info["targetUid"] as! String
        if isForeground {
            ((self.window?.rootViewController as! UINavigationController).visibleViewController as! BaseViewController).goDetailPage()
        }
    }
}
