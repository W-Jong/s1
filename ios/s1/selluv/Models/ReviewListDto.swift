//
//  ReviewListDto.swift
//  selluv
//  매너포인트 , 거래후기
//  Created by PJH on 22/03/18.
//

import Foundation
import SwiftyJSON

class ReviewListDto {
    
    var peerUsr       : UsrMiniDto! // 유저
    var review        : ReviewDao!  // 후기,

    init(_ json: JSON) {
        peerUsr = UsrMiniDto(json["peerUsr"])
        review = ReviewDao(json["review"])
    }
}
