//
//  SizeRefDto.swift
//  selluv
//  사이즈 정보
//  Created by PJH on 19/03/18.
//

import Foundation
import SwiftyJSON

class SizeRefDto {
    
    var sizeFirst            : Array<String> = [] // 사이즈타입이 4인 경우에만 유효, 첫번째 피커목록 ,
    var sizeSecond           : [[String]]    = []    // 사이즈타입이 4인 경우에만 유효, 두번째피커목록(null인 경우 두번째피커 없음) ,
    var sizeType             : Int!  //  사이즈타입, 1 - 가로x세로x폭, 2 - FREE SIZE & 직접입력, 3-일반
    
    
    init(_ json: JSON) {
        let list = json["sizeFirst"]
        for i in 0..<list.count {
            let cate = list[i].stringValue
            sizeFirst.append(cate)
        }
        
        let list1 = json["sizeSecond"]
        for i in 0..<list1.count {
            sizeSecond.append([])
            for k in 0..<list1[i].count {
                let cate = list1[i][k].stringValue
                sizeSecond[i].append(cate)
            }
        }
        
        sizeType = json["sizeType"].intValue
    }
}
