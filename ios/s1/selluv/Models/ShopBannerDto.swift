//
//  ShopBannerDto.swift
//  selluv
//  배너
//  Created by PJH on 10/03/18.
//

import Foundation
import SwiftyJSON

class ShopBannerDto {
    
    var bannerUid       : Int! // 카드번호
    var endTime         : Int!    // 종료 시간 ,
    var profileImg      : String!  //  배너이미지
    var regTime         : Int!    // 추가된 시간 ,
    var startTime       : Int! //  시작 시간 ,
    var status          : Int! //
    var targetUid       : Int! //분류에 따르는 타겟 UID ,
    var title           : String! // 배너타이틀
    var type            : Int! //분류 1-공지사항 2-테마 3-이벤트
    
    init(_ json: JSON) {
        bannerUid = json["bannerUid"].intValue
        endTime = json["endTime"].intValue
        profileImg = json["profileImg"].stringValue
        regTime = json["regTime"].intValue
        startTime = json["startTime"].intValue
        status = json["status"].intValue
        targetUid = json["targetUid"].intValue
        title = json["title"].stringValue
        type = json["type"].intValue
    }
}
