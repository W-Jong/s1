//
//  BrandRecommendedListDto.swift
//  selluv
//
//  Created by PJH on 21/03/18.
//

import Foundation
import SwiftyJSON

class BrandRecommendedListDto {
    var isSelect = false
    
    var brandUid          : Int!    // 브랜드UID,
    var nameEn            : String!  // 영문이름
    var nameKo            : String!  // 한글이름
    var pdtCount          : Int!    // 상품갯수
    var backImg           : String! // 백그라운드 이미지
    var logoImg           : String! // 로고이미지
 
    init(_ json: JSON) {
        
        brandUid = json["brandUid"].intValue
        nameEn = json["nameEn"].stringValue
        nameKo = json["nameKo"].stringValue
        pdtCount = json["pdtCount"].intValue
        backImg = json["backImg"].stringValue
        logoImg = json["logoImg"].stringValue
    }
}
