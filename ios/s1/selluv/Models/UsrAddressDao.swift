//
//  UsrAddressDao.swift
//  selluv
//  주소정보
//  Created by PJH on 04/04/18.
//

import Foundation
import SwiftyJSON

class UsrAddressDao {
    
    var addressDetail         : String! // 주소
    var addressDetailSub      : String!  // 상세주소(우편번호)
    var addressName           : String!  // 성함
    var addressOrder          : Int!  // 주소 순서 1,2,3만 가능 ,
    var addressPhone          : String!  // 연락처
    var regTime               : Int!  //
    var status                : Int!  //  1이면 정상, 2이면 해당 유저가 기본으로 선택한 주소 ,
    var usrAddressUid         : Int!  //
    var usrUid                : Int!  //  회원UID
    
    init(_ json: JSON) {
        addressDetail = json["addressDetail"].stringValue
        addressDetailSub = json["addressDetailSub"].stringValue
        addressName = json["addressName"].stringValue
        addressOrder = json["addressOrder"].intValue
        addressPhone = json["addressPhone"].stringValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        usrAddressUid = json["usrAddressUid"].intValue
        usrUid = json["usrUid"].intValue
    }
}
