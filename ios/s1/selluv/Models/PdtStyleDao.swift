//
//  PdtStyleDao.swift
//  selluv
//
//  Created by PJH on 09/03/18.
//

import Foundation
import SwiftyJSON

class PdtStyleDao {
    var isSelected : Bool = false
    
    var pdtStyleUid         : Int!    // 상품스타일UID ,
    var pdtUid              : Int!    // 상품UID
    var regTime             : Int!    // 추가된 시간
    var status              : Int!    //
    var styleImg            : String!    // 스타일사진
    var usrUid              : Int!    // 유저UID
    
    init(_ json: JSON) {
        pdtStyleUid = json["pdtStyleUid"].intValue
        pdtUid = json["pdtUid"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        styleImg = json["styleImg"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
