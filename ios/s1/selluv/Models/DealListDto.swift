//
//  DealListDto.swift
//  selluv
//  구매내역 정보 big
//  Created by PJH on 26/03/18.
//

import Foundation
import SwiftyJSON

class DealListDto {
    
    var deal             : DealMiniDto! // 거래정보
    var usr              : UsrMiniDto!  // 구매자 또는 판매자정보
    
    init(_ json: JSON) {
        deal = DealMiniDto(json["deal"])
        usr = UsrMiniDto(json["usr"])
    }
}
