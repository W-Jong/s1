//
//  PhotoType.swift
//  selluv
//
//  Created by Gambler on 12/26/17.
//

import Foundation
import Photos

class PhotoType {
    var name : String = ""
    var count : Int = 0
    var type : PHAssetCollectionSubtype?
    var identifier : String?
    var thumbnail : UIImage?
    var isSelected : Bool = false
}
