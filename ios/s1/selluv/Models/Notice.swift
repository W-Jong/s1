//
//  Notice.swift
//  selluv
//
//  Created by PJH on 02/03/18.
//

import Foundation
import SwiftyJSON

class Notice {
    
    var content             : String! // 내용,
    var noticeUid           : Int!    // noticeUid
    var regTime             : Int!    // 추가된 시간 ,
    var status              : Int!
    var title               : String! // 타이틀
    
    init(_ json: JSON) {
        content = json["content"].stringValue
        noticeUid = json["noticeUid"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        title = json["title"].stringValue
    }
}
