//
//  BrandMiniDto.swift
//  selluv
//
//  Created by PJH on 09/03/18.
//

import Foundation
import SwiftyJSON

class BrandMiniDto {
    
    var brandUid            : Int! // 브랜드UID ,
    var firstEn             : String!    // 영문첫글자 ,
    var firstKo             : String!    //  한글초성
    var nameEn              : String!    // 영문이름
    var nameKo              : String!    // 한글이름
    var profileImg          : String!    //썸네일이미지
    
    init(_ json: JSON) {
        
        brandUid = json["brandUid"].intValue
        firstEn = json["firstEn"].stringValue
        firstKo = json["firstKo"].stringValue
        nameEn = json["nameEn"].stringValue
        nameKo = json["nameKo"].stringValue
        profileImg = json["profileImg"].stringValue
        
    }
}
