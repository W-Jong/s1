//
//  CategoryMiniDto.swift
//  selluv
//
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class CategoryMiniDto {
    
    var categoryName          : String! // 브랜드
    var categoryUid           : Int!    // 카테고리UID
    var depth                 : Int!  //  뎁스
   
    init(_ json: JSON) {
        categoryName = json["categoryName"].stringValue
        categoryUid = json["categoryUid"].intValue
        depth = json["depth"].intValue
    }
}
