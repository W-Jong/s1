//
//  DeliveryHistoryDao.swift
//  selluv
//
//  Created by PJH on 26/03/18.
//

import Foundation
import SwiftyJSON

class DeliveryHistoryDao {
    
    var convenienceNumber   : String! // 편의점택배번호, 일반택배인 경우 빈문자열 ,
    var dealUid             : Int!    // 회원연관 거래UID ,
    var deliveryCompany     : String!  //  택배사이름
    var deliveryHistoryUid  : Int!    // 배송번호
    var deliveryNumber      : String! // 운송장번호
    var kind                : Int!    // 택배유형 1-편의점택배, 2-일반택배 ,
    var recipientAddress    : String!    // 수령자주소,
    var recipientNm         : String!    // 수령자이름
    var recipientPhone      : String!    // 수령자연락처
    var regTime             : Int!    // 추가된 시간 ,
    var type                : Int!    // 택배형태 1-구매자구매(판매자가 배송), 2-판매자반품(구매자가 배송) ,
    var usrUid              : Int!    // 발송자회원UID
    
    init(_ json: JSON) {
        convenienceNumber = json["convenienceNumber"].stringValue
        dealUid = json["dealUid"].intValue
        deliveryCompany = json["deliveryCompany"].stringValue
        deliveryHistoryUid = json["deliveryHistoryUid"].intValue
        deliveryNumber = json["deliveryNumber"].stringValue
        kind = json["kind"].intValue
        recipientAddress = json["recipientAddress"].stringValue
        recipientNm = json["recipientNm"].stringValue
        recipientPhone = json["recipientPhone"].stringValue
        regTime = json["regTime"].intValue
        type = json["type"].intValue
        usrUid = json["usrUid"].intValue
    }
}
