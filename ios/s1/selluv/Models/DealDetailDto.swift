//
//  DealDetailDto.swift
//  selluv
//
//  Created by PJH on 03/04/18.
//

import Foundation
import SwiftyJSON

class DealDetailDto {
    
    var deal           : DealDao! // 거래정보
    var refundPhotos   : Array<String> = [] // 반품첨부이미지
    var usr           : UsrMiniDto! // 구매자 또는 판매자정보
    
    init(_ json: JSON) {
        deal = DealDao(json["deal"])
        usr = UsrMiniDto(json["usr"])
        
        let list = json["refundPhotos"]
        for i in 0..<list.count {
            let img = list[i].stringValue
            refundPhotos.append(img)
        }
    }
}
