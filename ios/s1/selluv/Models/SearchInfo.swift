//
//  SearchInfo.swift
//  selluv
//  검색 정보
//  Created by PJh on 31/03/18.
//

import Foundation

class SearchInfo{
    
    var brandUidList        : Array<Int> = []  //브랜드UID 리스트, 전체인 경우 빈리스트 ,
    var brandName           : String = ""
    var categoryUid         : Int = 0           //카테고리UID, 전체인 경우 0 ,
    var categoryName        : String = ""      //카테고리명
    var keyword             : String = ""       // 검색키워드
    var pdtColorList        : Array<String> = []  //컬러 리스트, 전체인 경우 빈리스트 ,
    var pdtConditionList    : Array<String> = []  // 컨디션 리스트, 전체인 경우 빈리스트, NEW, BEST, GOOD, MIDDLE 만 해당 ,
    var pdtConditionName    : String = ""
    var pdtPriceMax         : Int = -1             // 가격 높은 값, -1이면 제한없음 ,
    var pdtPriceMin         : Int = -1             // 가격 낮은 값, -1이면 제한없음 ,
    var pdtSizeList         : Array<String> = []  // 사이즈 리스트, 전체인 경우 빈리스트 ,
    var pdtSizeName         : String = ""
    var searchOrderType     : String = "RECOMMEND"  // 정렬 = ['RECOMMEND', 'NEW', 'FAME', 'PRICELOW', 'PRICEHIGH'],
    var searchOrderName     : String = "추천순"   //정렬순명
    var soldOutExpect       : Bool = false       //
    
    var bEditBrand          : Bool = true
    var bEditOrderType      : Bool = true
    var bEditCategory       : Bool = true
    
    func getBrandUidList() -> Array<Int> {
        return brandUidList
    }
    
    func setBrandUidList(arr : Array<Int>)  {
        brandUidList = arr
    }
    
    func setEditBrand(status : Bool) {
        bEditBrand = status
    }
    
    func getEditBrand() -> Bool{
        return bEditBrand
    }
    
    func getCategoryUid() -> Int {
        return categoryUid
    }
    
    func setCategoryUid(_uid : Int)  {
        categoryUid = _uid
    }
    
    func getKeyword() -> String {
        return keyword
    }
    
    func setKeyword(value : String)  {
        keyword = value
    }
    
    func getPdtColorList() -> Array<String> {
        return pdtColorList
    }
    
    func setPdtColorList(arr : Array<String>)  {
        pdtColorList = arr
    }
    
    func getPdtConditionList() -> Array<String> {
        return pdtConditionList
    }
    
    func setPdtConditionList(arr : Array<String>)  {
        pdtConditionList = arr
    }
    
    func getPriceMax() -> Int {
        return pdtPriceMax
    }
    
    func setPriceMax(value : Int)  {
       pdtPriceMax = value
    }
    
    func getPriceMin() -> Int {
        return pdtPriceMin
    }
    
    func setPriceMin(value : Int)  {
        pdtPriceMin = value
    }
    
    func getPdtSizeList() -> Array<String> {
        return pdtSizeList
    }
    
    func setPdtSizeList(arr : Array<String>)  {
        pdtSizeList = arr
    }
    
    func setPdtModel(arr : Array<String>)  {
        pdtSizeList = arr
    }
    
    func getSearchOrderType() -> String {
        return searchOrderType
    }
    
    func setEditCategory(status : Bool) {
        bEditCategory = status
    }
    
    func getEditCategory() -> Bool{
        return bEditCategory
    }
    
    func setEditOrderType(status : Bool) {
        bEditOrderType = status
    }
    
    func getEditOrderType() -> Bool{
        return bEditOrderType
    }
    
    func setCategoryName(value : String) {
        categoryName = value
    }
    
    func getCategoryName() -> String{
        return categoryName
    }
    
    func setSearchOrderType(value : String)  {
       searchOrderType = value
    }
    
    func getSoldOutExpect() -> Bool {
        return soldOutExpect
    }
    
    func setSoldOutExpect(status : Bool)  {
        soldOutExpect = status
    }
    
    func getBrandName() -> String{
        return brandName
    }
    
    func setBrandName(value : String)  {
        brandName = value
    }
    
    func getPdtCondtionName() -> String{
        return pdtConditionName
    }
    
    func setPdtConditionName(value : String)  {
        pdtConditionName = value
    }
    
    func getPdtSizeName() -> String{
        return pdtSizeName
    }
    
    func setPdtSizeName(value : String)  {
        pdtSizeName = value
    }
    
    
    func initial() {
       brandUidList      = []  //브랜드UID 리스트, 전체인 경우 빈리스트 ,
       brandName         = ""
       categoryUid       = 0           //카테고리UID, 전체인 경우 0 ,
       keyword           = ""       // 검색키워드
       pdtColorList      = []  //컬러 리스트, 전체인 경우 빈리스트 ,
       pdtConditionList  = []  // 컨디션 리스트, 전체인 경우 빈리스트, NEW, BEST, GOOD, MIDDLE 만 해당 ,
       pdtConditionName  = ""
       pdtPriceMax       = -1             // 가격 높은 값, -1이면 제한없음 ,
       pdtPriceMin       = -1             // 가격 낮은 값, -1이면 제한없음 ,
       pdtSizeList       = []  // 사이즈 리스트, 전체인 경우 빈리스트 ,
       pdtSizeName       = ""
       searchOrderType   = "RECOMMEND"  // 정렬 = ['RECOMMEND', 'NEW', 'FAME', 'PRICELOW', 'PRICEHIGH'],
       soldOutExpect     = false       //
       searchOrderName   = "추천순"
       categoryName      = ""
        
       bEditBrand        = true
       bEditOrderType    = true
       bEditCategory     = true

    }
}
