//
//  ReviewDao.swift
//  selluv
//
//  Created by PJH on 22/03/18.
//

import Foundation
import SwiftyJSON

class ReviewDao {
    
    var content       : String! // 후기내용
    var dealUid       : Int!  // 거래UID
    var peerUsrUid    : Int!  // 대상회원UID ,
    var point         : Int!  // 점수 2-만족 1-보통 0-불만족 ,
    var regTime       : Int!  //
    var reviewUid     : Int!  //
    var type          : Int!  // 후기분류 1-판매자후기 2-구매자후기 ,
    var usrUid        : Int!  //회원UID
    
    init(_ json: JSON) {
        content = json["content"].stringValue
        dealUid = json["dealUid"].intValue
        peerUsrUid = json["peerUsrUid"].intValue
        point = json["point"].intValue
        regTime = json["regTime"].intValue
        reviewUid = json["reviewUid"].intValue
        type = json["type"].intValue
        usrUid = json["usrUid"].intValue
    }
}
