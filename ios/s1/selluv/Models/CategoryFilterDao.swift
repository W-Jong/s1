//
//  CategoryFilterDao.swift
//  selluv
//
//  Created by PJH on 02/04/18.
//

import Foundation
import SwiftyJSON

class CategoryFilterDao {
    
    var categoryName           : String!     // 카테고리명,
    var pdtCount               : Int!    // 카테고리 순서 ,
    var categoryUid            : Int!    // 카테고리UID

}
