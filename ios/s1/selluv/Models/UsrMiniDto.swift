//
//  FeedPdtDto.swift
//  selluv
//
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class UsrMiniDto {
    
    var profileImg          : String! // 프로필이미지
    var usrId               : String! // 아이디
    var usrNckNm            : String! // 닉네임
    var usrUid              : Int! // 회원UID
    
    init(_ json: JSON) {
        
        profileImg = json["profileImg"].stringValue
        usrId = json["usrId"].stringValue
        usrNckNm = json["usrNckNm"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
