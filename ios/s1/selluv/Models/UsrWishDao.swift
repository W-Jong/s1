//
//  UsrWishDao.swift
//  selluv
//  상품 알림설정 정보
//  Created by PJH on 19/03/18.
//

import Foundation
import SwiftyJSON

class UsrWishDao {
    
    var brandUid           : Int!    // 브랜드UID
    var categoryUid        : Int!    // 카테고리UID
    var pdtGroup           : Int! //상품군 1-남성, 2-여성, 4-키즈 ,
    var colorName          : String! // 컬러
    var pdtModel           : String! // 모델명
    var pdtSize            : String! // 사이즈
    var regTime            : Int! // 추가된 시간 ,
    var status             : Int! //
    var usrUid             : Int! // 유저UID
    var usrWishUid         : Int! //
    
    init(_ json: JSON) {
        brandUid = json["brandUid"].intValue
        categoryUid = json["categoryUid"].intValue
        pdtGroup = json["pdtGroup"].intValue
        colorName = json["colorName"].stringValue
        pdtModel = json["pdtModel"].stringValue
        pdtSize = json["pdtSize"].stringValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        usrUid = json["usrUid"].intValue
        usrWishUid = json["usrWishUid"].intValue
    }
}
