//
//  Brand.swift
//  selluv
//  브렌드
//  Created by Dev on 12/03/17.
//

import Foundation
import SwiftyJSON

class Brand {
    var isSelected : Bool = false
    
    var backImg             : String! // 백그라운드 이미지 ,
    var brandLikeCount      : Int!    // 팔로우 유저수 ,
    var brandLikeStatus     : Bool!    //  유저 팔로우 상태
    var brandUid            : Int!    // 브랜드UID
    var firstEn             : String!    // 영문첫글자
    var firstKo             : String!    // 한글초성
    var logoImg             : String!    // 로고이미지
    var nameEn              : String!    // 영문이름
    var nameKo              : String!    // 한글이름
    var pdtList             = [PdtInfoDto!]()    // 상품목록
    var profileImg          : String!    //썸네일이미지
    
    init(_ json: JSON) {
        backImg = json["backImg"].stringValue
        brandLikeCount = json["brandLikeCount"].intValue
        brandLikeStatus = json["brandLikeStatus"].boolValue
        brandUid = json["brandUid"].intValue
        firstEn = json["firstEn"].stringValue
        firstKo = json["firstKo"].stringValue
        logoImg = json["logoImg"].stringValue
        nameEn = json["nameEn"].stringValue
        nameKo = json["nameKo"].stringValue
        profileImg = json["profileImg"].stringValue
        
        let list = json["pdtList"]
        if list != .null {
            for (_, json) in list {
                pdtList.append(PdtInfoDto(json))
            }
        }
    }
}
