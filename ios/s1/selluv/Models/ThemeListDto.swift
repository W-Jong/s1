//
//  ThemeListDto.swift
//  selluv
//  테마
//  Created by PJH on 13/03/18.
//

import Foundation
import SwiftyJSON

class ThemeListDto {
    
    var pdtCount          : Int! //  테마상품갯수
    var pdtList           = [PdtListDto!]()  // 테마상품목록
    var theme             : ThemeDao!  //  테마
    
    init(_ json: JSON) {
        pdtCount = json["pdtCount"].intValue
        let list = json["pdtList"]
        if list != .null {
            for (_, json) in list {
                pdtList.append(PdtListDto(json))
            }
        }
        theme =  ThemeDao(json["theme"])

    }
}
