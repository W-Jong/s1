//
//  PromotionCodeDao.swift
//  selluv
//  프로모션 정보
//  Created by PJH on 17/03/18.
//

import Foundation
import SwiftyJSON

class PromotionCodeDao {
    
    var endTime             : Int! // 프로모션적용 끝날짜 ,
    var kind                : Int!    // 코드분류, 1-구매용 2-판매용 ,
    var price               : Int!    // 혜택가격,
    var priceUnit           : Int! //혜택단위 1-퍼센트단위 2-원단위 ,
    var promotionCodeUid    : Int! // 프로모션코드UID
    var startTime           : Int! // 프로모션적용 시작날짜 ,
    var title               : String! // 프로모션 코드

    init(_ json: JSON) {
        endTime = json["endTime"].intValue
        kind = json["kind"].intValue
        price = json["price"].intValue
        priceUnit = json["priceUnit"].intValue
        promotionCodeUid = json["promotionCodeUid"].intValue
        startTime = json["startTime"].intValue
        title = json["title"].stringValue
    }
}
