//
//  PdtListDto .swift
//  selluv
//  브렌드 상품군에 따르는 리스트 정보
//  Created by PJH on 13/03/18.
//

import Foundation
import SwiftyJSON

class PdtListDto {
    
    var brand           : BrandMiniDto!     //  브랜드
    var category        : CategoryMiniDto!  // 카테고리
    var pdt             : PdtMiniDto!       //  상품
    var pdtLikeCount    : Int!              // 상품좋아요 갯수,
    var pdtLikeStatus   : Bool!             // 유저 상품좋아요 상태
    var pdtStyle        : PdtStyleDao!      //스타일
    var usr             : UsrMiniDto!       //등록유저
    var pdtProfileImgHeight : Int! //상품프로필이미지 높이
    var pdtProfileImgWidth  : Int! //상품프로필이미지 너비
    var styleProfileImgHeight : Int! //스타일프로필이미지 높이
    var styleProfileImgWidth : Int! //스타일프로필이미지 너비

    
    init(_ json: JSON) {
        brand = BrandMiniDto(json["brand"])
        category = CategoryMiniDto(json["category"])
        pdt = PdtMiniDto(json["pdt"])
        pdtLikeCount = json["pdtLikeCount"].intValue
        pdtLikeStatus = json["pdtLikeStatus"].boolValue
        pdtStyle = PdtStyleDao(json["pdtStyle"])
        usr = UsrMiniDto(json["usr"])
        pdtProfileImgHeight = json["pdtProfileImgHeight"].intValue
        pdtProfileImgWidth = json["pdtProfileImgWidth"].intValue
        styleProfileImgHeight = json["styleProfileImgHeight"].intValue
        styleProfileImgWidth = json["styleProfileImgWidth"].intValue
    }
}
