//
//  PdtMiniDto.swift
//  selluv
//
//  Created by PJH on 09/03/18.
//

import Foundation
import SwiftyJSON

class PdtMiniDto {
    
    var brandUid             : Int!     // 브랜드UID ,
    var categoryUid          : Int!    // 카테고리 UID
    var colorName            : String!  // 영문첫글자
    var edtTime              : Int!    // 업데이트 시간
    var originPrice          : Int!    // 변경전 가격 ,
    var pdtModel             : String!  // 모델명
    var pdtSize              : String!  // 사이즈
    var pdtUid               : Int!    // 상품UID
    var price                : Int!    // 판매가격
    var profileImg           : String!  //대표사진
    var status               : Int!    // 상태 0-삭제, 1-판매중 2-판매완료, 3-판매중지, 4-휴가모드 ,
    var usrUid               : Int!    // 회원UID
    var edited               : Bool!    //
    
    init(_ json: JSON) {
        brandUid = json["brandUid"].intValue
        categoryUid = json["categoryUid"].intValue
        colorName = json["colorName"].stringValue
        edtTime = json["edtTime"].intValue
        originPrice = json["originPrice"].intValue
        pdtModel = json["pdtModel"].stringValue
        pdtSize = json["pdtSize"].stringValue
        pdtUid = json["pdtUid"].intValue
        price = json["price"].intValue
        profileImg = json["profileImg"].stringValue
        status = json["status"].intValue
        usrUid = json["usrUid"].intValue
        edited = json["edited"].boolValue
    }
}
