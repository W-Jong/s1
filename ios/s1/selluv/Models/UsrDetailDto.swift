//
//  UsrDetailDto.swift
//  selluv
//  판매유저 
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class UsrDetailDto {
    
    var accountNm       : String!   // 계좌주
    var accountNum      : Int!      // 계좌번호 ,
    var bankNm          : String!   //  은행명
    var description     : String!   // 자기소개,
    var freeSendPrice   : Int!      //  판매자무료배송최저가 -1-무료배송안함, 그외 정수 무료배송최저가 ,
    var gender          : Int!      // 성별 1-남, 2-여
    var likeGroup       : Int!      //관심상품군 1-남성, 2-여성, 4-키즈, 다중선택일때 합산값 ,
    var money           : Int!      // 셀럽머니
    var point           : Int!      //매너포인트
    var profileBackImg  : String!   //프로필배경이미지
    var profileImg      : String!   //프로필이미지
    var regTime         : Int!      //추가된 시간 ,
    var sleepTime       : Int!      //판매자휴가모드완료날짜 sleepYn = 1일때만 유효 ,
    var sleepYn         : Int!      //판매자휴가모드여부 1-휴가모드 0-휴가모드아님 ,
    var status          : Int!      // 상태 1- 정상, 2-탈퇴한 회원 ,
    var usrId           : Int!      //아이디
    var usrLoginType    : Int!      //로그인타입 0 - 일반로그인 1-페이스북 2-네이버 3-카카오톡 ,
    var usrNckNm        : String!   //닉네임
    var usrPhone        : String!   //휴대폰번호
    var usrUid          : Int!      //회원UID
    
    init(_ json: JSON) {
        accountNm = json["accountNmUID"].stringValue
        accountNum = json["accountNum"].intValue
        bankNm = json["bankNm"].stringValue
        description = json["description"].stringValue
        freeSendPrice = json["freeSendPrice"].intValue
        gender = json["gender"].intValue
        likeGroup = json["likeGroup"].intValue
        money = json["money"].intValue
        point = json["point"].intValue
        profileBackImg = json["profileBackImg"].stringValue
        profileImg = json["profileImg"].stringValue
        regTime = json["regTime"].intValue
        sleepTime = json["sleepTime"].intValue
        sleepYn = json["sleepYn"].intValue
        status = json["status"].intValue
        usrId = json["usrId"].intValue
        usrLoginType = json["usrLoginType"].intValue
        usrNckNm = json["usrNckNm"].stringValue
        usrPhone = json["usrPhone"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
