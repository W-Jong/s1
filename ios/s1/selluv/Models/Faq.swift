//
//  Faq.swift
//  selluv
//
//  Created by PJH on 02/03/18.
//

import Foundation
import SwiftyJSON

class Faq {
    
    var content             : String! // 내용,
    var faqUid              : Int!    // faqUid
    var kind                : Int!  //  분류 1-판매 2-구매 3-활동 ,
    var regTime             : Int!    // 추가된 시간 ,
    var status              : Int!
    var title               : String! // 타이틀
    
    init(_ json: JSON) {
        content = json["content"].stringValue
        faqUid = json["faqUid"].intValue
        kind = json["kind"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        title = json["title"].stringValue
    }
}
