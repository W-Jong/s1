//
//  UserInfo.swift
//  Selluv
//
//  Created by Dragon C. on 8/8/16.
//  Copyright © 2016 Dragon C. All rights reserved.
//

import Foundation
import SwiftyJSON

open class UserInfo: NSObject {
	
	// MARK: - Member vars
	
    var accountNm       : String!  //계좌주 ,
    var accountNum      : String! //계좌번호 ,
    var bankNm          : String!  //은행명 ,
    var desc            : String! //자기소개 ,
    var freeSendPrice   : String! // 판매자무료배송최저가 -1-무료배송안함, 그외 정수 무료배송최저가 ,
    var gender          : Int! // 성별 1-남, 2-여 ,
    var likeGroup       : Int! // 관심상품군 1-남성, 2-여성, 4-키즈, 다중선택일때 합산값 ,
    var money           : Int! // 셀럽머니 ,
    var point           : Int! //매너포인트 ,
    var profileBackImg  : String! // 프로필배경이미지 ,
    var profileImg      : String! //프로필이미지 ,
    var regTime         : Int! // 추가된 시간 ,
    var sleepTime       : Int! //판매자휴가모드완료날짜 sleepYn = 1일때만 유효 ,
    var sleepYn         : Int! //판매자휴가모드여부 1-휴가모드 0-휴가모드아님 ,
    var status          : Int! // 상태 1- 정상, 2-탈퇴한 회원 ,
    var usrId           : String! // 아이디 ,
    var usrPass         : String! // 패스워드 ,
    var usrLoginType    : Int! //로그인타입 0 - 일반로그인 1-페이스북 2-네이버 3-카카오톡 ,
    var usrMail         : String! // 이메일 ,
    var usrNckNm        : String! // 닉네임 ,
    var usrPhone        : String! //휴대폰번호 ,
    var usrUid          : Int! // 회원UID
	var token           : String! // 토큰
    var loginCnt        : Int!   //로그인 회수
    // MARK: - Login user
	
    static var me: UserInfo!
	
    class func getMe() -> UserInfo {
        if me == nil {
            me = UserInfo.load1()
        }
        return me!
    }
    
    // MARK: - Initializers
	
	// MARK: - Helper
	
    func set(_ json: JSON, pass : String, accesstoken : String) {
        accountNm       = json["accountNm"].stringValue
        accountNum      = json["accountNum"].stringValue
        bankNm          = json["bankNm"].stringValue
        desc            = json["description"].stringValue
        freeSendPrice   = json["freeSendPrice"].stringValue
        likeGroup       = json["likeGroup"].intValue
        gender          = json["gender"].intValue
        money           = json["money"].intValue
        point           = json["point"].intValue
        profileBackImg  = json["profileBackImg"].stringValue
        profileImg      = json["profileImg"].stringValue
        regTime         = json["regTime"].intValue
        sleepTime       = json["sleepTime"].intValue
        sleepYn         = json["sleepYn"].intValue
        status          = json["status"].intValue
        usrId           = json["usrId"].stringValue
        usrLoginType    = json["usrLoginType"].intValue
        usrNckNm        = json["usrNckNm"].stringValue
        usrPhone        = json["usrPhone"].stringValue
        usrUid          = json["usrUid"].intValue
        usrMail         = json["usrMail"].stringValue
        usrPass         = pass
        token           = accesstoken

        save()
	}
	
	func initial() {
		accountNm       = ""
		accountNum      = ""
		bankNm          = ""
		desc            = ""
		freeSendPrice   = ""
		gender          = 0
		likeGroup       = 0
		money           = 0
		point           = 0
		profileBackImg  = ""
		profileImg      = ""
		regTime         = 0
		sleepTime       = 0
		sleepYn         = 0
        status          = 0
		usrId           = nil
		usrUid          = 0
		usrLoginType    = 0
		usrNckNm        = ""
        usrPhone        = ""
        token           = ""
        usrPass         = nil
        
		save()
	}
	
	// MARK: - Load & Save the data into UserDefault.
    
    class func load1() -> UserInfo! {
        let userInfo: UserInfo! = UserInfo()
        let ud: UserDefaults = UserDefaults.standard
        
		userInfo.token = ud.string(forKey: "token")
		userInfo.accountNm = ud.string(forKey: "accountNm")
		userInfo.accountNum = ud.string(forKey: "accountNum")
		userInfo.bankNm = ud.string(forKey: "bankNm")
		userInfo.desc = ud.string(forKey: "description")
		userInfo.freeSendPrice = ud.string(forKey: "freeSendPrice")
		userInfo.gender = ud.integer(forKey: "gender")
		userInfo.likeGroup = ud.integer(forKey: "likeGroup")
		userInfo.money = ud.integer(forKey: "money")
		userInfo.point = ud.integer(forKey: "point")
		userInfo.profileBackImg = ud.string(forKey: "profileBackImg")
		userInfo.profileImg = ud.string(forKey: "profileImg")
		userInfo.regTime = ud.integer(forKey: "regTime")
		userInfo.sleepTime = ud.integer(forKey: "sleepTime")
		userInfo.sleepYn = ud.integer(forKey: "sleepYn")
		userInfo.status = ud.integer(forKey: "status")
		userInfo.usrId = ud.string(forKey: "usrId")
        userInfo.usrLoginType = ud.integer(forKey: "usrLoginType")
		userInfo.usrNckNm = ud.string(forKey: "usrNckNm")
		userInfo.usrPhone = ud.string(forKey: "usrPhone")
        userInfo.usrUid = ud.integer(forKey: "usrUid")
        userInfo.usrPass = ud.string(forKey: "usrPass")
        userInfo.loginCnt = ud.integer(forKey: "loginCnt")
		
        return userInfo
    }
	
    func save() {
        let ud: UserDefaults = UserDefaults.standard
		
		ud.set(token, forKey: "token")
		ud.set(accountNm, forKey: "accountNm")
		ud.set(accountNum, forKey: "accountNum")
		ud.set(bankNm, forKey: "bankNm")
		ud.set(desc, forKey: "description")
		ud.set(freeSendPrice, forKey: "freeSendPrice")
		ud.set(gender, forKey: "gender")
		ud.set(likeGroup, forKey: "likeGroup")
		ud.set(money, forKey: "money")
		ud.set(point, forKey: "point")
		ud.set(profileBackImg, forKey: "profileBackImg")
		ud.set(profileImg, forKey: "profileImg")
		ud.set(regTime, forKey: "regTime")
        ud.set(sleepTime, forKey: "sleepTime")
		ud.set(sleepYn, forKey: "sleepYn")
		ud.set(status, forKey: "status")
		ud.set(usrId, forKey: "usrId")
        ud.set(usrLoginType, forKey: "usrLoginType")
        ud.set(usrNckNm, forKey: "usrNckNm")
		ud.set(usrPhone, forKey: "usrPhone")
		ud.set(usrUid, forKey: "usrUid")
        ud.set(usrPass, forKey: "usrPass")
		
        ud.synchronize()
    }
	
}
