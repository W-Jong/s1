//
//  Event.swift
//  selluv
//
//  Created by PJH on 02/03/18.
//

import Foundation
import SwiftyJSON

class Event {
    
    var detailImg           : String! // 상세이미지
    var eventUid            : Int!    // eventUid
    var mypageYn            : Int!  //  마이페이지 노출여부 1-노출, 0-노출안함 ,
    var regTime             : Int!    // 추가된 시간 ,
    var status              : Int!
    var title               : String! // 타이틀
    var profileImg          : String! // 썸네일이미지
    
    init(_ json: JSON) {
        detailImg = json["detailImg"].stringValue
        eventUid = json["eventUid"].intValue
        mypageYn = json["mypageYn"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        title = json["title"].stringValue
        profileImg = json["profileImg"].stringValue
    }
}
