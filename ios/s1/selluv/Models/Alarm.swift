//
//  Alarm.swift
//  selluv
//
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class Alarm {
    
    var content           : String! // 알림내용
    var kind              : Int!    // 알람분류 1-거래, 2-소식 3-댓글 ,
    var pdtUid            : Int!  //  연관된 상품UID ,
    var regTime           : Int!    // 추가된 시간 ,
    var subKind           : Int! //부분분류, 앱기획서 208~210페이지 알림리스트유형에 해당 ,
    var targetUid         : Int! // 타겟 UID, subKind에 따라 결정됨 ,
    var usrAlarmUid       : Int!
    var usrUid            : Int! //회원 UID
    var profileImg        : String! // 연관된 상품이미지 ,
    var usrProfileImg     : String! // 연관된 유저 프로필이미지

    init(_ json: JSON) {
        content = json["content"].stringValue
        kind = json["kind"].intValue
        pdtUid = json["pdtUid"].intValue
        regTime = json["regTime"].intValue
        subKind = json["subKind"].intValue
        targetUid = json["targetUid"].intValue
        usrAlarmUid = json["usrAlarmUid"].intValue
        usrUid = json["usrUid"].intValue
        profileImg = json["profileImg"].stringValue
        usrProfileImg = json["usrProfileImg"].stringValue
    }
}
