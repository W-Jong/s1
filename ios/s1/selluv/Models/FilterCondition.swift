//
//  FilterCondition.swift
//  selluv
//
//  Created by Comet on 12/19/17.
//

import UIKit
enum SortType : String {
    case recommendOrder = "추천순"
    case newOrder = "신상품순"
    case popularOrder = "인기순"
    case cheapOrder = "가격 낮은순"
    case expensiveOrder = "가격 높은순"
}

enum ColorType : String {
    case black = "블랙"
    case grey = "그레이"
    case white = "화이트"
    case beizy = "베이지"
    case red = "레드"
    case pink = "핑크"
    case purple = "퍼플"
    case blue = "블루"
    case green = "그린"
    case yellow = "옐로우"
    case orange = "오렌지"
    case brown = "브라운"
    case gold = "골드"
    case silver = "실버"
    case multi = "멀티"
    
    func name() -> String {
        return rawValue
    }
}

enum Condition : String {
    case new = "새상품"
    case likenew = "최상"
    case best = "상"
    case good = "중상"
}

class PriceRange {
    var minValue : Int? = 0
    var maxValue : Int? = 0
}

class FilterCondition {
    var sortType : SortType! = SortType.recommendOrder
    var category : String? = ""
    var brand : [String]? = []
    var size : [String]? = []
    var condition : [Condition]? = []
    var colorType : [ColorType] = []
    var priceRange : PriceRange = PriceRange()
    var isExcludingOnSales : Bool = false
}
