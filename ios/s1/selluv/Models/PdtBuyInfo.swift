//
//  PdtBuyInfo.swift
//  selluv
//  상품구매정보
//  Created by PJh on 18/03/18.
//

import Foundation
import SwiftyJSON

class PdtBuyInfo {
    
    var name                : String = ""  //성함
    var contact             : String = ""  //연락처
    var addr                : String = ""  //주소
    var paymentType         : Int = 0 // 0: 신용/체크카드, 1: 네이버 페이, 2: 신용/체크카드 간편결제, 3: 실시간 계죄이체
    var appraisalType       : Int = 0 // 0:온라인 정품감정서비스 , 1: 본사정품감정 서비스
    var payType             : String = ""
    var promotionCode       : String = ""  //프로모션 코드
    var promotionSale       : Int = 0  //프로모션 양
    var selluvMoneyUse      : Int = 0  //사용할 셀럽머니
    var totalmoney          : Int = 0  //총 결제금액
    var jejuAreaSendPrice   : Int = 0  //제주도 지역 추가배송비
    var islandSendPrice     : Int = 0  //도서 지역 추가배송비

    //get
    func getName() -> String {
        return name
    }
    
    func getContact() -> String {
        return contact
    }
    
    func getAddress() -> String {
        return addr
    }
    
    func getPaymentType() -> Int {
        return paymentType
    }
    
    func getAppraisalType() -> Int {
        return appraisalType
    }
    
    func getPayType() -> String {
        return payType
    }
    
    func getPromotionCode() -> String {
        return promotionCode
    }
    
    func getPromotionSale() -> Int {
        return promotionSale
    }
    
    func getSelluvMoneyUse() -> Int {
        return selluvMoneyUse
    }
    
    func getTotalmoney() -> Int {
        return totalmoney
    }
    
    func getJejuAreaSendPrice() -> Int {
        return jejuAreaSendPrice
    }
    
    func getIslandSendPrice() -> Int {
        return islandSendPrice
    }
    
    //set
    func setName(value : String) {
        name = value
    }
    
    func setContact(value : String) {
        contact = value
    }
    
    func setAddress(value : String) {
        addr = value
    }
    
    func setPaymentType(value : Int) {
        paymentType = value
    }
    
    func setAppraisalType(value : Int) {
        appraisalType = value
    }
    
    func setPayType(value : String) {
        payType = value
    }
    
    func setPromotionCode(value : String) {
        promotionCode = value
    }
    
    func setPromotionSale(value : Int)  {
        promotionSale = value
    }
    
    func setSelluvMoneyUse(value : Int) {
        selluvMoneyUse = value
    }
    
    func setTotalmoney(value : Int) {
        totalmoney = value
    }
    
    func setJejuAreaSendPrice(value : Int) {
        jejuAreaSendPrice = value
    }
    
    func setIslandSendPrice(value : Int) {
        islandSendPrice = value
    }
   
    func initial() {
        name                 = ""  //성함
        contact              = ""  //연락처
        addr                 = ""  //주소
        paymentType          = 0 // 0: 신용/체크카드, 1: 네이버 페이, 2: 신용/체크카드 간편결제, 3: 실시간 계죄이체
        appraisalType        = 0 // 0:온라인 정품감정서비스 , 1: 본사정품감정 서비스
        payType              = ""
        promotionCode        = ""  //프로모션 코드
        promotionSale        = 0  //프로모션 양
        selluvMoneyUse       = 0  //사용할 셀럽머니
        totalmoney           = 0  //총 결제금액
        jejuAreaSendPrice    = 0
        islandSendPrice      = 0
    }
}
