//
//  RecommendSearchDto.swift
//  selluv
//
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class RecommendSearchDto {
    
    var brandList             = [BrandDao!]()    // 추천브랜드
    var categoryDaoList       : [[CategoryDao]]! = [[]] //추천카테고리
    var modelList             = [SearchModelDto!]()    // 추천모델명
    var tagWordList           : Array<String>!  = []//추천태그명
    
    init(_ json: JSON) {

        let list1 = json["brandList"]
        if list1 != .null {
            for (_, json) in list1 {
                brandList.append(BrandDao(json))
            }
        }
       
        let list2 = json["categoryDaoList"]
        for i in 0..<list2.count {
            for k in 0..<list2[i].count {
                categoryDaoList.append([CategoryDao(list2[i][k])])
            }
        }
        
        let list3 = json["modelList"]
        if list1 != .null {
            for (_, json) in list3 {
                modelList.append(SearchModelDto(json))
            }
        }
        
        let list4 = json["tagWordList"]
        for i in 0..<list4.count {
            let cate = list4[i].stringValue
            tagWordList.append(cate)
        }
    }
}
