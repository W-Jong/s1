//
//  UsrMoneyHisDto.swift
//  selluv
//  셀럽머니
//  Created by PJH on 26/03/18.
//

import Foundation
import SwiftyJSON

class UsrMoneyHisDto {
    
    var amount              : Int! //  변동금액
    var content             : String! // 내역
    var hintContent         : String!  //  힌트내용
    var kind                : Int! // 분류, 1-머니사용 2-판매대금 3-공유리워드 4-가품신고리워드 5-초대리워드 6-가입리워드 7-초대유저첫구매리워드 8-거래후기리워드 9-구매적립금 ,
    var moneyChangeHisUid   : Int! //  변경이력UID
    var regTime             : Int! // 추가된 시간
    var targetUid           : Int! //타겟 UID, kind가 1,2,3,8,9일 경우 dealUid, 4인 경우 pdtUid, 5,6,7인 경우 usrUid
 
    init(_ json: JSON) {
        amount = json["amount"].intValue
        content = json["content"].stringValue
        hintContent = json["hintContent"].stringValue
        kind = json["kind"].intValue
        moneyChangeHisUid = json["moneyChangeHisUid"].intValue
        regTime = json["regTime"].intValue
        targetUid = json["targetUid"].intValue
    }
}
