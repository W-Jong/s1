//
//  PdtDetailDto.swift
//  selluv
//  상품 상세 정보
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class PdtDetailDto {
    
    var brand               : BrandMiniDto!         // 브랜드
    var categoryList        = [CategoryMiniDto!]() // 상세카테고리목록(상품군/1단계/2단계/3단계 카테고리) ,
    var pdt                 : PdtDto!               // 상품
    var pdtLikeCount        : Int!                  // 상품좋아요 갯수 ,
    var pdtLikeStatus       : Bool!                  // 유저 상품좋아요 상태 ,
    var pdtReplyCount       : Int!                  // 상품 댓글수 ,
    var pdtStyleList        = [PdtStyleDto!]()      // 스타일리스트
    var pdtWishCount        : Int!                  // 상품잇템 유저수 ,
    var pdtWishStatus       : Bool!                 // 유저 상품잇템 상태 ,
    var relationStyleCount  : Int!                  // 연관 스타일 갯수 ,
    var seller              : UsrDetailDto!         // 판매유저
    var sellerFollowerCount : Int!                  // 등록유저 팔로워 유저수 ,
    var sellerItemCount     : Int!                  // 등록유저 아이템 갯수 ,
    var sellerPdtList       = [PdtInfoDto!]()       //등록유저 상품리스트
    var shareUrl            : String!               //공유URL
    var edited              : Bool!               //

    init(_ json: JSON) {
        brand = BrandMiniDto(json["brand"])
        
        let list1 = json["categoryList"]
        if list1 != .null {
            for (_, json) in list1 {
                categoryList.append(CategoryMiniDto(json))
            }
        }
        
        pdt = PdtDto(json["pdt"])
        pdtLikeCount = json["pdtLikeCount"].intValue
        pdtLikeStatus = json["pdtLikeStatus"].boolValue
        pdtReplyCount = json["pdtReplyCount"].intValue
        
        let list2 = json["pdtStyleList"]
        if list2 != .null {
            for (_, json) in list2 {
                pdtStyleList.append(PdtStyleDto(json))
            }
        }
        
        pdtWishCount = json["pdtWishCount"].intValue
        pdtWishStatus = json["pdtWishStatus"].boolValue
        relationStyleCount = json["relationStyleCount"].intValue
        seller = UsrDetailDto(json["seller"])
        sellerFollowerCount = json["sellerFollowerCount"].intValue
        sellerItemCount = json["sellerItemCount"].intValue
        shareUrl = json["shareUrl"].stringValue
        edited = json["edited"].boolValue
        let list3 = json["sellerPdtList"]
        if list3 != .null {
            for (_, json) in list3 {
                sellerPdtList.append(PdtInfoDto(json))
            }
        }
    }
}
