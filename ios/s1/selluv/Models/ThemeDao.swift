//
//  ThemeDao.swift
//  selluv
//  테마정보
//  Created by PJH on 13/03/18.
//

import Foundation
import SwiftyJSON

class ThemeDao {
    
    var endTime             : Int! //  종료시간
    var kind                : Int! // 테마분류 1-남성 2-여성 4-키즈 ,
    var profileImg          : String!  //  테마이미지
    var regTime             : Int!    // 추가된 시간 ,,
    var startTime           : Int! // 시작시간
    var status              : Int! //
    var themeUid            : Int! //테마UID
    var titleEn             : String!  //  테마타이틀 - 영어 ,
    var titleKo             : String!  //  테마타이틀 - 한국어

    init(_ json: JSON) {
        endTime = json["endTime"].intValue
        kind = json["kind"].intValue
        profileImg = json["profileImg"].stringValue
        startTime = json["startTime"].intValue
        status = json["status"].intValue
        themeUid = json["themeUid"].intValue
        themeUid = json["themeUid"].intValue
        titleEn = json["titleEn"].stringValue
        titleKo = json["titleKo"].stringValue
    }
}
