//
//  SystemSettingDto.swift
//  selluv
//  시스템 설정 정보
//  Created by PJH on 19/03/18.
//

import Foundation
import SwiftyJSON

class SystemSettingDto {
    
    var buyReviewReward             : Int! // 브랜드
    var dealReward                  : Int!    // 구매적립(%) ,
    var fakeReward                  : Int!  //  가품리워드(원) ,
    var invitedUserFirstDealReward  : Int!  //초대자에게 가입자의 첫 구매 후 지급되는 리워드(원) ,
    var invitedUserReward           : Int!  // 가입자에게 지급되는 가입리워드(원) ,
    var inviterRewrad               : Int!  // 초대자에게 지급되는 가입리워드(원) ,
    var sellReviewReward            : Int!  // 판매후기리워드(원) ,
    var shareRewrad                 : Int!  // 공유리워드(원)
  
    init(_ json: JSON) {
        buyReviewReward = json["buyReviewReward"].intValue
        dealReward = json["dealReward"].intValue
        fakeReward = json["fakeReward"].intValue
        invitedUserFirstDealReward = json["invitedUserFirstDealReward"].intValue
        invitedUserReward = json["invitedUserReward"].intValue
        inviterRewrad = json["inviterRewrad"].intValue
        sellReviewReward = json["sellReviewReward"].intValue
        shareRewrad = json["shareRewrad"].intValue
    }
}
