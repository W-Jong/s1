//
//  UsrFollowListDto.swift
//  selluv
//  잇템
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class UsrFollowListDto {
    
    var profileImg      : String!  //  프로필이미지
    var usrId           : String! // 카드번호
    var usrLikeStatus   : Bool!    // 회원 팔로우 여부 ,
    var usrNckNm        : String!  // 닉네임
    var usrUid          : Int!    //  회원UID
    
    init(_ json: JSON) {
        
        profileImg = json["profileImg"].stringValue
        usrId = json["usrId"].stringValue
        usrLikeStatus = json["usrLikeStatus"].boolValue
        usrNckNm = json["usrNckNm"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
