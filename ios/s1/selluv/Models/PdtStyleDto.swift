//
//  PdtStyleDto.swift
//  selluv
//
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class PdtStyleDto {
    
    var usr                : UsrMiniDto!         // 등록유저
    var pdtStyle           : PdtStyleDao!      // 스타일
    var styleProfileImgHeight : Int!          //스타일프로필이미지 높이
    var styleProfileImgWidth : Int!          //스타일프로필이미지 너비

    init(_ json: JSON) {
        usr = UsrMiniDto(json["usr"])
        pdtStyle = PdtStyleDao(json["pdtStyle"])
        styleProfileImgHeight = json["styleProfileImgHeight"].intValue
        styleProfileImgWidth = json["styleProfileImgWidth"].intValue
    }
}
