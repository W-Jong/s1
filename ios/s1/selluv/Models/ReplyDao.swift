//
//  ReplyDto.swift
//  selluv
//  댓글
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class ReplyDao {
    
    var content       : String! // 내용
    var pdtUid        : Int!    // 상품UID,
    var regTime       : Int!    // 추가된 시간 ,
    var replyUid      : Int! //
    var status        : Int! //
    var targetUids    : String! //@붙은 회원UID, 콤마로 구분, 0인 경우 해당 유저상세페이지로 이동불가 ,
    var usrUid        : Int! //회원UID
    
    init(_ json: JSON) {
        content = json["content"].stringValue
        pdtUid = json["pdtUid"].intValue
        regTime = json["regTime"].intValue
        replyUid = json["replyUid"].intValue
        status = json["status"].intValue
        targetUids = json["targetUids"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
