//
//  DealMiniDto.swift
//  selluv
//  구매내역 정보 big
//  Created by PJH on 26/03/18.
//

import Foundation
import SwiftyJSON

class DealMiniDto {
    
    var brandEn             : String! // 상품 영문브랜드
    var cashCompTime        : Int!  // 정산완료시간
    var cashPrice           : Int!  //정산금액
    var compTime            : Int!  // 거래완료시간
    var dealUid             : Int!  // 거래UID
    var deliveryTime        : Int!  // 배송완료시간
    var payTime             : Int!  // 결제시간
    var pdtImg              : String!  // 상품이미지
    var pdtPrice            : Int!  // 상품가격
    var pdtSize             : String!  // 상품 사이즈 ,
    var pdtTitle            : String!  // 상품 타이틀
    var pdtUid              : Int!  // 상품UID
    var price               : Int!  // 결제금액
    var regTime             : Int!  // 추가된 시간
    var reqPrice            : Int!  // 네고 및 카운터네고 제안금액
    var sellerUsrUid        : Int!  // 판매자 회원UID
    var sendPrice           : Int!  // 배송비
    var status              : Int!  // 거래상태 0-삭제 1-배송준비 2-배송진행 3-정품인증 4-배송완료 5-거래완료 6-정산완료 7-반품접수 8-반품승인 9-반품완료 10-주문취소 11-네고제안 12-카운터네고제안 ,
    var usrUid              : Int!  // 구매자 회원UID
    var verifyPrice         : Int!  // 정품인증가격, 0이면 정품인증요청안함
   
    init(_ json: JSON) {
        brandEn = json["brandEn"].stringValue
        cashCompTime = json["cashCompTime"].intValue
        cashPrice = json["cashPrice"].intValue
        compTime = json["compTime"].intValue
        dealUid = json["dealUid"].intValue
        deliveryTime = json["deliveryTime"].intValue
        payTime = json["payTime"].intValue
        pdtImg = json["pdtImg"].stringValue
        pdtPrice = json["pdtPrice"].intValue
        pdtSize = json["pdtSize"].stringValue
        pdtTitle = json["pdtTitle"].stringValue
        pdtUid = json["pdtUid"].intValue
        price = json["price"].intValue
        regTime = json["regTime"].intValue
        reqPrice = json["reqPrice"].intValue
        sellerUsrUid = json["sellerUsrUid"].intValue
        sendPrice = json["sendPrice"].intValue
        status = json["status"].intValue
        usrUid = json["usrUid"].intValue
        verifyPrice = json["verifyPrice"].intValue
    }
}
