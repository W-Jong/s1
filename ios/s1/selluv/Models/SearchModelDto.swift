//
//  SearchModelDto.swift
//  selluv
//
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class SearchModelDto {
    
    var modelName          : String! // 모델명
    var brandName          : String! //브랜드명
    
    init(_ json: JSON) {

        modelName = json["modelName"].stringValue
        brandName = json["brandName"].stringValue
    }
}
