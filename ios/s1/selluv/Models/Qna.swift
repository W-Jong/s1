//
//  Qna.swift
//  selluv
//
//  Created by PJH on 02/03/18.
//

import Foundation
import SwiftyJSON

class Qna {
    
    var answer              : String! // 답변,
    var compTime            : Int!    // 답변시간
    var content             : String!  //  내용
    var qnaUid              : Int!    // UID
    var regTime             : Int!    // 추가된 시간 ,
    var status              : Int!    // 1-답변완료, 2-답변대기(신청중) ,
    var title               : String! // 타이틀
    var usrUid              : Int!    // 회원UID
    
    init(_ json: JSON) {
        answer = json["answer"].stringValue
        compTime = json["compTime"].intValue
        content = json["content"].stringValue
        qnaUid = json["qnaUid"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        title = json["title"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
