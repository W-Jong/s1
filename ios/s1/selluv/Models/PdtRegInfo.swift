//
//  PdtRegInfo.swift
//  selluv
//  상품추가(등록)정보
//  Created by PJh on 21/03/18.
//

import Foundation
import SwiftyJSON

class PdtRegInfo{
    
    var brandUid            : Int = 0  //브랜드UID
    var nameKo              : String = ""  //한글 브렌드명
    var nameEn              : String = ""  //영문 브렌드명
    var categoryUid         : Int = 0  //카테고리UID
    var component           : String = "000000"  //부속품 예:110101 6자리문자열(0-없음, 1-있음) 첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백 ,
    var content             : String = "" // 상세설명
    var etc                 : String = "" // 기타
    var negoYn              : Int = 0  //네고허용여부 1-네고허용 2-네고 받지 않음 ,
    var pdtColor            : String = ""  //컬러
    var pdtCondition        : String = ""  //컨디션 = ['NEW', 'BEST', 'GOOD', 'MIDDLE'],
    var pdtGroup            : String = ""  // 상품군 = ['MALE', 'FEMALE', 'KIDS'],
    var pdtModel            : String = ""  // 모델명
    var pdtSize             : String = ""  // 상품사이즈
    var photos              : Array<String> = []  //상품이미지 리스트 ,
    var pdtThumb            : String = ""  //상품 대표이미지
    var price               : Int = 0  // 가격
    var promotionCode       : String = ""  //프로모션 코드
    var sendPrice           : Int = 0  // 배송비
    var signImg             : String = ""  //사인이미지
    var tag                 : String = ""  //태그
    var categoryName        : String = ""  //상세 카테고리명
    
    //발렛신청시 이용
    var name                : String = ""  //성함
    var contact             : String = ""   //연락처
    var address             : String = ""   //주소
    var sendType            : Int = 1   //발송방법, 1-배송키트 이용, 2-직접발송
    var reqPrice            : Int = 0  //추천가격의 경우 0, 직접설정의 경우 0이상값 ,
    
    func getBrandUid() -> Int {
        return brandUid
    }
    
    func setBrandUid(_uid : Int)  {
        brandUid = _uid
    }
    
    func getCategoryUid() -> Int {
        return categoryUid
    }
    
    func setCategoryUid(_uid : Int)  {
        categoryUid = _uid
    }
    
    func getComponent() -> String {
        return component
    }
    
    func setComponent(value : String)  {
        component = value
    }
    
    func getEtc() -> String {
        return etc
    }
    
    func setEtc(value : String)  {
        etc = value
    }
   
    func getNegoYn() -> Int {
        return negoYn
    }
    
    func setNegoYn(value : Int)  {
        negoYn = value
    }
    
    func getPdtColor() -> String {
        return pdtColor
    }
    
    func setPdtColor(value : String)  {
        pdtColor = value
    }
    
    func getPdtCondition() -> String {
        return pdtCondition
    }
    
    func setPdtCondition(value : String)  {
        
        if value == NSLocalizedString("new_product", comment:"") {
            pdtCondition = "NEW"
        } else if value == NSLocalizedString("best_product", comment:"") {
            pdtCondition = "BEST"
        } else if value == NSLocalizedString("top_product", comment:"") {
            pdtCondition = "GOOD"
        } else {
            pdtCondition = "MIDDLE"
        }
    }
    
    func getPdtGroup() -> String {
        return pdtGroup
    }
    
    func setPdtGroup(value : String)  {
        pdtGroup = value
    }
    
    func getPdtModel() -> String {
        return pdtModel
    }
    
    func setPdtModel(value : String)  {
        pdtModel = value
    }
    
    func getPdtSize() -> String {
        return pdtSize
    }
    
    func setPdtSize(value : String)  {
        pdtSize = value
    }
    
    func getPhotos() -> Array<String> {
        return photos
    }
    
    func setPhotos(value : Array<String>)  {
        photos = value
    }
    
    func getPrice() -> Int {
        return price
    }
    
    func setPrice(value : Int)  {
        price = value
    }
    
    func getPromotioncode() -> String {
        return promotionCode
    }
    
    func setPromotioncode(value : String)  {
        promotionCode = value
    }
    
    func getSendPrice() -> Int {
        return sendPrice
    }
    
    func setSendPrice(value : Int)  {
        sendPrice = value
    }
    
    func getSignimg() -> String {
        return signImg
    }
    
    func setSignimg(value : String)  {
        signImg = value
    }
    
    func getTag() -> String {
        return tag
    }
    
    func setTag(value : String)  {
        tag = value
    }
    
    func getContent() -> String {
        return content
    }
    
    func setContent(value : String)  {
        content = value
    }
    
    func getNameEn() -> String {
        return nameEn
    }
    
    func setNameEn(value : String)  {
        nameEn = value
    }
    
    func getNameko() -> String {
        return nameKo
    }
    
    func setNameKo(value : String)  {
        nameKo = value
    }
    
    func getCategoryName() -> String {
        return categoryName
    }
    
    func setCategoryName(value : String)  {
        categoryName = value
    }
    
    func getPdtThumb() -> String {
        return pdtThumb
    }
    
    func setPdtThumb(value : String)  {
        pdtThumb = value
    }
    
    func getName() -> String {
        return name
    }
    
    func setName(value : String)  {
        name = value
    }
    
    func getContact() -> String {
        return contact
    }
    
    func setContact(value : String)  {
        contact = value
    }
    
    func getAddress() -> String {
        return address
    }
    
    func setAddress(value : String)  {
        address = value
    }
    
    func getSendType() -> Int {
        return sendType
    }
    
    func setAddress(value : Int)  {
        sendType = value
    }
    
    func getReqPrice() -> Int {
        return reqPrice
    }
    
    func setReqPrice(value : Int)  {
        reqPrice = value
    }
    
    func initial() {
        brandUid = 1
        categoryUid = 5
        component = "000000"
        content = ""
        etc = ""
        negoYn = 2
        pdtColor = "멀티"
        pdtCondition = "GOOD"
        pdtGroup = "MALE"
        pdtModel = "기타"
        pdtSize = "FREE SIZE"
        photos = []
        price = 50000
        promotionCode = ""
        sendPrice = 0
        signImg = ""
        tag = ""
        pdtThumb = ""
        nameEn = ""
        nameKo = ""
        categoryName = ""
        name = ""
        address = ""
        contact = ""
        sendType = 1
        reqPrice = 0
    }
}
