//
//  FindItemDto.swift
//  selluv
//  첮눈 어아탬
//  Created by PJH on 05/03/18.
//

import Foundation
import SwiftyJSON

class FindItemDto {
    
    var brand               : JSON! // 브랜드
    var category            : JSON!    // 카테고리
    var usrWish             : JSON!  //  찾는 제품정보
    
    init(_ json: JSON) {
        brand = json["brand"]
        category = json["category"]
        usrWish = json["usrWish"]
    }
}
