//
//  UsrCardDto.swift
//  selluv
//  카드관리
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class UsrCardDto {
    
    var cardName        : String! // 카드이름
    var cardNumber      : String! // 카드번호
    var kind            : Int!    // 카드종류 1-개인 2-법인 ,
    var password        : String!  //  비밀번호
    var regTime         : Int!    // 추가된 시간 ,
    var usrCardUid      : Int! //
    var usrUid          : Int! // 회원UID
    var validDate       : String! //유효기간
    var validNum        : String! // 유효번호 개인인 경우 생년월일(YYMMDD), 법인일 경우 사업자번호

    init(_ json: JSON) {
        cardName = json["cardName"].stringValue
        cardNumber = json["cardNumber"].stringValue
        kind = json["kind"].intValue
        password = json["password"].stringValue
        regTime = json["regTime"].intValue
        usrCardUid = json["usrCardUid"].intValue
        usrUid = json["usrUid"].intValue
        validDate = json["validDate"].stringValue
        validNum = json["validNum"].stringValue
    }
}
