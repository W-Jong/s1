//
//  BrandDao.swift
//  selluv
//
//  Created by Dev on 12/03/17.
//

import Foundation
import SwiftyJSON

class BrandDao {
    
    var backImg         : String!
    var brandUid        : Int!
    var firstEn         : String!
    var firstKo         : String!
    var licenseUrl      : String!
    var logoImg         : String!
    var nameEn          : String!
    var nameKo          : String!
    var profileImg      : String!
    var pdtCount        : Int!
    var regTime         : Int!
    var status          : Int!
    var totalCount      : Int!
    var totalPrice      : Int!
    var usrUid          : Int!
    
    
    init(_ json: JSON) {
        backImg = json["backImg"].stringValue
        brandUid = json["brandUid"].intValue
        firstEn = json["firstEn"].stringValue
        firstKo = json["firstKo"].stringValue
        licenseUrl = json["licenseUrl"].stringValue
        logoImg = json["logoImg"].stringValue
        nameEn = json["nameEn"].stringValue
        nameKo = json["nameKo"].stringValue
        profileImg = json["profileImg"].stringValue
        pdtCount = json["pdtCount"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        totalCount = json["totalCount"].intValue
        totalPrice = json["totalPrice"].intValue
        usrUid = json["usrUid"].intValue
    }
}
