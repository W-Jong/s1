//
//  SearchWordDto.swift
//  selluv
//
//  Created by PJh on 28/03/18.
//

import Foundation
import SwiftyJSON

class SearchWordDto {

    var count         : Int! // 조회수
    var word          : String! //검색어

    init(_ json: JSON) {
        count = json["count"].intValue
        word = json["word"].stringValue
    }
}
