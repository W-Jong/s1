//
//  UsrInfoDto.swift
//  selluv
//  회원 정보
//  Created by PJH on 22/03/18.
//

import Foundation
import SwiftyJSON

class UsrInfoDto {
    
    var point               : Int! //  매너포인트
    var description         : String! // 자기소개
    var profileBackImg      : String!  //  프로필배경이미지
    var profileImg          : String!    // 프로필이미지,
    var usrBlockStatus      : Bool!   //유저 차단 상태
    var usrFollowerCount    : Int! // 팔로워수
    var usrFollowingCount   : Int! //팔로잉수
    var usrId               : String! // 아이디
    var usrLikeStatus       : Bool! // 유저 팔로우 상태
    var usrNckNm            : String! // 닉네임
    var usrUid              : Int! // 회원UID
    
    init(_ json: JSON) {
        point = json["point"].intValue
        description = json["description"].stringValue
        profileImg = json["profileImg"].stringValue
        profileBackImg = json["profileBackImg"].stringValue
        usrBlockStatus = json["usrBlockStatus"].boolValue
        usrFollowerCount = json["usrFollowerCount"].intValue
        usrFollowingCount = json["usrFollowingCount"].intValue
        usrId = json["usrId"].stringValue
        usrLikeStatus = json["usrLikeStatus"].boolValue
        usrNckNm = json["usrNckNm"].stringValue
        usrUid = json["usrUid"].intValue
    }
}
