//
//  BannerDto.swift
//  selluv
//  배너
//  Created by PJH on 12/03/18.
//

import Foundation
import SwiftyJSON

class BannerDto {
    
    var banner          : ShopBannerDto! 
    var target          : JSON!
    
    init(_ json: JSON) {
        banner = ShopBannerDto(json["banner"])
        target = json["target"]
    }
}
