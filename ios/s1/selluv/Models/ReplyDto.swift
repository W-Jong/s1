//
//  ReplyDto.swift
//  selluv
//  댓글
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class ReplyDto {
    
    var reply       : ReplyDao! // 댓글내용
    var usr         : UsrMiniDto!    // 등록유저,

    init(_ json: JSON) {
        reply = ReplyDao(json["reply"])
        usr = UsrMiniDto(json["usr"])
    }
}
