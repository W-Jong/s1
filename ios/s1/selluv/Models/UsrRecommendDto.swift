//
//  UsrRecommendDto.swift
//  selluv
//
//  Created by PJH on 29/03/18.
//

import Foundation
import SwiftyJSON

class UsrRecommendDto {
    
    var pdtCount           : Int!    // 아이템수
    var pdtList            = [PdtMiniDto!]()  // 인기상품목록
    var profileImg         : String!  // 프로필이미지
    var usrFollowerCount   : Int!    // 팔로워수
    var usrId              : String!  // 아이디
    var usrLikeStatus      : Bool!    // 회원 팔로우 여부
    var usrNckNm           : String!  // 닉네임
    var usrUid             : Int!    // 회원UID
  
    init(_ json: JSON) {
        pdtCount = json["pdtCount"].intValue
        profileImg = json["profileImg"].stringValue
        usrFollowerCount = json["usrFollowerCount"].intValue
        usrId = json["usrId"].stringValue
        usrLikeStatus = json["usrLikeStatus"].boolValue
        usrNckNm = json["usrNckNm"].stringValue
        usrUid = json["usrUid"].intValue
        
        let list = json["pdtList"]
        if list != .null {
            for (_, json) in list {
                pdtList.append(PdtMiniDto(json))
            }
        }
    }
}
