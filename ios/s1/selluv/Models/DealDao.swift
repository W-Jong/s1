//
//  DealDao.swift
//  selluv
//  네고
//  Created by PJH on 09/03/18.
//

import Foundation
import SwiftyJSON

class DealDao {
    
    var brandEn               : String!     // 상품 영문브랜드 ,
    var cancelReason          : String!    // 취소사유
    var cancelTime            : Int!  // 취소시간
    var cashAccount           : String!    // 정산계좌정보
    var cashCompTime          : Int!    // 정산완료시간,
    var cashPrice             : Int!  // 정산금액
    var cashPromotion         : String!  // 판매프로모션
    var cashPromotionPrice    : Int!    // 코드혜택금액
    var compTime              : Int!    // 거래완료시간
    var dealUid               : Int!  //거래UID
    var deliveryNumber        : String!    // 송장번호
    var deliveryTime          : Int!    // 배송완료시간
    var negoTime              : Int! //네고 및 카운터 요청시간(status에 따라 결정됨) ,
    var payFeePrice           : Int! //결제수수료
    var payImportMethod       : Int!  // 아임포트결제방법 1-간편결제신용카드 2-일반결제신용카드 3-실시간계좌이체 4-네이버페이 ,
    var payPromotion          : Int!  // 구매프로모션
    var payPromotionPrice     : Int! //코드할인금액
    var payTime               : Int! // 결제시간
    var pdtImg                : String! // 상품이미지
    var pdtPrice              : Int! // 상품가격
    var pdtSize               : String! // 상품 사이즈 ,
    var pdtTitle              : String! //상품 타이틀 ,
    var pdtUid                : Int! // 상품UID
    var price                 : Int! // 결제금액
    var recipientAddress      : String! // 수령자 주소 ,
    var recipientNm           : String! // 수령자 성함 ,
    var recipientPhone        : String! // 수령자 연락처 ,
    var refundCheckTime       : Int! // 반품승인시간 ,
    var refundCompTime        : Int! // 반품완료시간 ,
    var refundDeliveryNumber  : String! //반품송장
    var refundReason          : String! // 환불사유
    var refundReqTime         : Int! // 반품요청시간
    var regTime               : Int! // 추가된 시간 ,
    var reqPrice              : Int! // 네고 및 카운터네고 제안금액 ,
    var rewardPrice           : Int! // 사용적립금 ,
    var sellerUsrUid          : Int! // 판매자 회원UID ,
    var selluvPrice           : Int! // 셀럽이용료 ,
    var sendPrice             : Int! // 배송비
    var status                : Int! // 거래상태 0-삭제 1-배송준비 2-배송진행 3-정품인증 4-배송완료 5-거래완료 6-정산완료 7-반품접수 8-반품승인 9-반품완료 10-주문취소 11-네고제안 12-카운터네고제안 ,
    var usrUid                : Int! // 구매자 회원UID ,
    var valetUid              : Int! // 발렛UID 0-발렛상품 아님 0아니면 발렛상품 ,
    var verifyDeliveryNumber  : String! // 인증전송장 번호 ,
    var verifyPrice           : Int! // 정품인증가격

    init(_ json: JSON) {
        brandEn = json["brandEn"].stringValue
        cancelReason = json["cancelReason"].stringValue
        cancelTime = json["cancelTime"].intValue
        cashAccount = json["cashAccount"].stringValue
        cashCompTime = json["cashCompTime"].intValue
        cashPrice = json["cashPrice"].intValue
        cashPromotion = json["cashPromotion"].stringValue
        cashPromotionPrice = json["cashPromotionPrice"].intValue
        compTime = json["compTime"].intValue
        dealUid = json["dealUid"].intValue
        deliveryNumber = json["deliveryNumber"].stringValue
        deliveryTime = json["deliveryTime"].intValue
        negoTime = json["negoTime"].intValue
        payFeePrice = json["payFeePrice"].intValue
        payImportMethod = json["payImportMethod"].intValue
        payPromotion = json["payPromotion"].intValue
        payPromotionPrice = json["payPromotionPrice"].intValue
        payTime = json["payTime"].intValue
        pdtImg = json["pdtImg"].stringValue
        pdtPrice = json["pdtPrice"].intValue
        pdtSize = json["pdtSize"].stringValue
        pdtTitle = json["pdtTitle"].stringValue
        pdtUid = json["pdtUid"].intValue
        price = json["price"].intValue
        recipientAddress = json["recipientAddress"].stringValue
        recipientNm = json["recipientNm"].stringValue
        recipientPhone = json["recipientPhone"].stringValue
        refundCheckTime = json["refundCheckTime"].intValue
        refundCompTime = json["refundCompTime"].intValue
        refundDeliveryNumber = json["refundDeliveryNumber"].stringValue
        refundReason = json["refundReason"].stringValue
        refundReqTime = json["refundReqTime"].intValue
        regTime = json["regTime"].intValue
        reqPrice = json["reqPrice"].intValue
        rewardPrice = json["rewardPrice"].intValue
        sellerUsrUid = json["sellerUsrUid"].intValue
        selluvPrice = json["selluvPrice"].intValue
        sendPrice = json["sendPrice"].intValue
        status = json["status"].intValue
        usrUid = json["usrUid"].intValue
        valetUid = json["valetUid"].intValue
        verifyDeliveryNumber = json["verifyDeliveryNumber"].stringValue
        verifyPrice = json["verifyPrice"].intValue
        
    }
}

