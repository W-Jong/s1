//
//  CategoryDao.swift
//  selluv
//
//  Created by PJH on 21/03/18.
//

import Foundation
import SwiftyJSON

class CategoryDao {
    var isSelect = false
    
    var categoryName           : String!     // 카테고리명,
    var categoryOrder          : Int!    // 카테고리 순서 ,
    var categoryUid            : Int!    // 카테고리UID
    var depth                  : Int!    // 카테고리 뎁스
    var parentCategoryUid      : Int!  // 상위 카테고리UID
    var pdtCount               : Int!  // 카테고리에 해당한 상품수(하위 카테고리 포함) ,
    var regTime                : Int!    // 추가된 시간 ,
    var status                 : Int!    // 카테고리에 해당한 누적 거래건수(하위 카테고리 포함)
    var totalCount             : Int!  //
    var totalPrice             : Int!    // 카테고리에 해당한 누적 거래대금(하위 카테고리 포함)
    
    init(_ json: JSON) {
        
        categoryName = json["categoryName"].stringValue
        categoryOrder = json["categoryOrder"].intValue
        categoryUid = json["categoryUid"].intValue
        depth = json["depth"].intValue
        parentCategoryUid = json["parentCategoryUid"].intValue
        pdtCount = json["pdtCount"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        totalCount = json["totalCount"].intValue
        totalPrice = json["totalPrice"].intValue

    }
}
