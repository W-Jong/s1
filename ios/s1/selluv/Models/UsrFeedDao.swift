//
//  FeedPdtDto.swift
//  selluv
//
//  Created by PJH on 03/03/18.
//

import Foundation
import SwiftyJSON

class UsrFeedDao {
    
    var pdtUid              : Int! // 상품UID
    var peerUsrUid          : Int!    // 대상자 UID ,
    var regTime             : Int!  //  추가된 시간 ,
    var status              : Int!    //
    var type                : Int! //분류 1-찾았던 상품 2-좋아요 상품 가격인하 3-팔로우 브랜드 4-잇템 5-팔로우 유저 스타일 6-팔로우 유저 신상품 ,
    var usrFeedUid          : Int! // 스타일
    var usrUid              : Int! //회원UID
    
    init(_ json: JSON) {

        pdtUid = json["pdtUid"].intValue
        peerUsrUid = json["peerUsrUid"].intValue
        regTime = json["regTime"].intValue
        status = json["status"].intValue
        type = json["type"].intValue
        usrFeedUid = json["usrFeedUid"].intValue
        usrUid = json["usrUid"].intValue
    }
}
