//
//  BlockUsrDto.swift
//  selluv
//  차단된 유저
//  Created by PJH on 06/03/18.
//

import Foundation
import SwiftyJSON

class BlockUsrDto {
    
    var profileImg          : String!   // 프로필이미지
    var usrId               : String!   // 아이디
    var usrNckNm            : String!   // 닉네임
    var usrUid              : Int!      //  회원UID
    var usrLikeStatus       : Bool!    // 회원 팔로우 여부

    init(_ json: JSON) {
        
        usrUid = json["usrUid"].intValue
        usrNckNm = json["usrNckNm"].stringValue
        usrId = json["usrId"].stringValue
        profileImg = json["profileImg"].stringValue
        usrLikeStatus = json["usrLikeStatus"].boolValue
    }
}
