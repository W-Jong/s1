//
//  Keyword.swift
//  selluv
//
//  Created by Comet on 12/13/17.
//

import UIKit
import SwiftyJSON

class Keyword {
    var isRecommendItem : Bool = false
    var recommend_type : String = ""
    var keywordId :Int = -1
    var profile_url: String = ""
    var keyword_name: String = ""
    var keyword_name1: String = ""
    var visit_count: Int = 0
    
    init() {
        keywordId = -1
        profile_url = ""
        keyword_name = ""
        visit_count = 0
    }
    
    init(_ keywordId: Int, _ profile_url : String, _ keyword_name : String, _ keyword_name1 : String, _ visit_count: Int) {
        self.keywordId = keywordId
        self.profile_url = profile_url
        self.keyword_name = keyword_name
        self.keyword_name1 = keyword_name1
        self.visit_count = visit_count
    }
    
    init(json pJson: JSON) {
        self.keywordId = pJson["keywordId"].intValue
        self.profile_url = pJson["profile_url"].stringValue
        self.keyword_name = pJson["keyword_name"].stringValue
        self.visit_count = pJson["visit_count"].intValue
    }    
}
