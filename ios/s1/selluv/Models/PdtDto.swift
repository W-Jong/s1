//
//  PdtDto.swift
//  selluv
//  상품
//  Created by PJH on 15/03/18.
//

import Foundation
import SwiftyJSON

class PdtDto {
    
    var brandUid      : Int! // 브랜드UID
    var categoryUid   : Int!    // 카테고리UID
    var colorName     : String!  //  컬러명
    var component     : String!    // 구성품 6자리문자열(0-없음, 1-있음) 첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백 ,
    var content       : String! //  상세설명 ,
    var edtTime       : Int! // 업데이트 시간 ,
    var etc           : String! //기타
    var fameScore     : Int! // 인기점수
    var negoYn        : Int! //네고허용여부 1-허용 2-비허용 ,
    var originPrice   : Int! // 변경전 가격
    var pdtCondition  : Int! // 컨디션 1-새상품, 2-최상, 3-상, 4-중상 ,
    var pdtGroup      : Int! // 상품군 1-남성, 2-여성, 4-키즈 ,
    var pdtModel      : String! // 모델명
    var pdtSize       : String! // 사이즈
    var pdtUid        : Int! // 상품UID
    var photoList     : Array<String> = [] // 사진리스트
    var photoshopYn   : Int! // 포토샵전후여부 1-포토샵후 2-포토샵전 ,
    var price         : Int! // 판매가격
    var profileImg    : String! // 대표사진
    var regTime       : Int! // 추가된 시간 ,
    var sendPrice     : Int! // 배송비
    var signImg       : String! // 서명사진
    var status        : Int! // 상태 0-삭제, 1-판매중 2-판매완료, 3-판매중지, 4-휴가모드 ,
    var tag           : String! // 태그, #으로 시작 및 공백구분 ,
    var usrUid        : Int! // 회원UID
    var valetUid      : Int! // 발렛UID
    
    init(_ json: JSON) {
        brandUid = json["brandUid"].intValue
        categoryUid = json["categoryUid"].intValue
        colorName = json["colorName"].stringValue
        component = json["component"].stringValue
        content = json["content"].stringValue
        edtTime = json["edtTime"].intValue
        etc = json["etc"].stringValue
        fameScore = json["fameScore"].intValue
        negoYn = json["negoYn"].intValue
        originPrice = json["originPrice"].intValue
        pdtCondition = json["pdtCondition"].intValue
        pdtGroup = json["pdtGroup"].intValue
        pdtModel = json["pdtModel"].stringValue
        pdtSize = json["pdtSize"].stringValue
        pdtUid = json["pdtUid"].intValue
        
        let list = json["photoList"]
        for i in 0..<list.count {
            let img = list[i].stringValue
            photoList.append(img)
        }
        
        photoshopYn = json["photoshopYn"].intValue
        price = json["price"].intValue
        profileImg = json["profileImg"].stringValue
        regTime = json["regTime"].intValue
        sendPrice = json["sendPrice"].intValue
        signImg = json["signImg"].stringValue
        status = json["status"].intValue
        tag = json["tag"].stringValue
        usrUid = json["usrUid"].intValue
        valetUid = json["valetUid"].intValue
    }
}
