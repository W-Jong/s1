//
//  PdtInfoDto.swift
//  selluv
//  상뭎목록
//  Created by PJH on 10/03/18.
//

import Foundation
import SwiftyJSON

class PdtInfoDto {
    
    var pdtUid           : Int! //  상품UID
    var brandName        : String! // 브랜드명
    var profileImg       : String!  //  이미지
    var categoryName     : String!    // 카테고리명,
    var price            : Int! // 가격
    
    init(_ json: JSON) {
        pdtUid = json["pdtUid"].intValue
        brandName = json["brandName"].stringValue
        profileImg = json["profileImg"].stringValue
        categoryName = json["categoryName"].stringValue
        price = json["price"].intValue
    }
}
