//
//  ShopViewController.swift
//  selluv
//
//  Created by Gambler on 12/12/17.
//  modified by PJH on 10/03/18.

import UIKit
import SwiftyJSON
import DGElasticPullToRefresh

class ShopViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    
    @IBOutlet var vwSpin: [UIView]!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnMenuStyle: UIButton!
    @IBOutlet weak var btnMenuMen: UIButton!
    @IBOutlet weak var btnMenuWomen: UIButton!
    @IBOutlet weak var btnMenuKids: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var clvStyle: UICollectionView!
    
    @IBOutlet weak var scvMen: UIScrollView!
    @IBOutlet weak var vwMenCategoryAll: UIView!
    @IBOutlet weak var clvMenBrand: UICollectionView!
    @IBOutlet weak var tblMenTheme: ExpandableTableView!
    @IBOutlet weak var tblMenThemeHeight: NSLayoutConstraint!
    @IBOutlet weak var clvMen: UICollectionView!
    @IBOutlet weak var clvMenHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scvWomen: UIScrollView!
    @IBOutlet weak var vwWomenCategoryAll: UIView!
    @IBOutlet weak var clvWomenBrand: UICollectionView!
    @IBOutlet weak var tblWomenTheme: ExpandableTableView!
    @IBOutlet weak var tblWomenThemeHeight: NSLayoutConstraint!
    @IBOutlet weak var clvWomen: UICollectionView!
    @IBOutlet weak var clvWomenHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scvKids: UIScrollView!
    @IBOutlet weak var vwKidsCategoryAll: UIView!
    @IBOutlet weak var clvKidsBrand: UICollectionView!
    @IBOutlet weak var tblKidsTheme: ExpandableTableView!
    @IBOutlet weak var tblKidsThemeHeight: NSLayoutConstraint!
    @IBOutlet weak var clvKids: UICollectionView!
    @IBOutlet weak var clvKidsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var vwMenu: UIView!
    @IBOutlet weak var vwMenuBottom: NSLayoutConstraint!
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    
    //zoom효과관련
    weak var selectedImageView : UIImageView?
    var id : Int?
    
    var animator : ARNTransitionAnimator?
    
    enum EShopTab : Int {
        case style = 0
        case men
        case women
        case kids
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    var pageTimer : Timer?
    var styleHeaderView : ShopStyleGalleryView?
    
    var menThemeList = [ShopTheme]()
    var womenThemeList = [ShopTheme]()
    var kidsThemeList = [ShopTheme]()
    
    var styleList = [Int]()
    var menList = [Int]()
    var womenList = [Int]()
    var kidsList = [Int]()
    
    var arrBannerList               : Array<BannerDto> = []
    var arrStylePdtStyleList        : Array<PdtListDto> = []
    var arrMenStylePdtStyleList     : Array<PdtListDto> = []
    var arrWomenStylePdtStyleList   : Array<PdtListDto> = []
    var arrKidsStylePdtStyleList    : Array<PdtListDto> = []
    var arrMenThemeList             : Array<ThemeListDto> = []
    var arrWomenThemeList           : Array<ThemeListDto> = []
    var arrKidsThemeList            : Array<ThemeListDto> = []
    var arrMenBrandList             : Array<BrandMiniDto> = []
    var arrWomenBrandList           : Array<BrandMiniDto> = []
    var arrKidsBrandList            : Array<BrandMiniDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!
    var pdtGroup        : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.changePage()
        })

        addPullToRefresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        gSearchInfo.initial()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        gCategoryUid = 0
        initVars()
        
        setupLayout()
        
        registerNibs()
        
//        DispatchQueue.global().async {
//            Thread.sleep(forTimeInterval: 1)
//            DispatchQueue.main.async {
//                self.reloadListViews()
//            }
//        }
    
        // 상품이 새로 추가된것이 있다면
        btnFavorite.setImage(#imageLiteral(resourceName: "ic_heart_no_badge"), for: .normal)
        
        vwMenCategoryAll.borderColor = UIColor.black
        vwMenCategoryAll.layer.borderWidth = 2
        
        vwWomenCategoryAll.borderColor = UIColor(hex: 0xff3b7e)
        vwWomenCategoryAll.layer.borderWidth = 2
        
        vwKidsCategoryAll.borderColor = UIColor(hex: 0x3a0d7d)
        vwKidsCategoryAll.layer.borderWidth = 2
        
//        pageTimer?.invalidate()
//        pageTimer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(self.onGalleryTimer), userInfo: nil, repeats: true)
    }
    
    func addPullToRefresh() {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        clvStyle.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
                
            })
            }, loadingView: loadingView)
        clvStyle.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        clvStyle.dg_setPullToRefreshBackgroundColor(clvStyle.backgroundColor!)
        
        let loadingView1 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView1.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        scvMen.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
                
            })
            }, loadingView: loadingView1)
        scvMen.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        
        let loadingView2 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView2.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        scvWomen.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
                
            })
            }, loadingView: loadingView2)
        scvWomen.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        
        let loadingView3 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView3.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        scvKids.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
                
            })
            }, loadingView: loadingView3)
        scvKids.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
    }
    
    func initVars() {
        
        for _ in 0 ... 2 {
            
            let theme = ShopTheme()
            menThemeList.append(theme)
            
            let theme1 = ShopTheme()
            womenThemeList.append(theme1)
            
            let theme2 = ShopTheme()
            kidsThemeList.append(theme2)
            
        }
        
        for nInd in 0 ..< 20 {
            styleList.append(nInd)
            clvStyle.reloadData()
            vwSpin[0].isHidden = true
        }
        
    }
    
    func setupLayout() {
        
        // 컬렉션뷰들에 FlowLayout 적용하기
        let styleLayout = CHTCollectionViewWaterfallLayout()
        styleLayout.minimumColumnSpacing = 15.0
        styleLayout.minimumInteritemSpacing = 30.0
        clvStyle.collectionViewLayout = styleLayout
        
        let menLayout = CHTCollectionViewWaterfallLayout()
        menLayout.minimumColumnSpacing = 15.0
        menLayout.minimumInteritemSpacing = 30.0
        clvMen.collectionViewLayout = menLayout
        
        let womenLayout = CHTCollectionViewWaterfallLayout()
        womenLayout.minimumColumnSpacing = 15.0
        womenLayout.minimumInteritemSpacing = 30.0
        clvWomen.collectionViewLayout = womenLayout
        
        let kidsLayout = CHTCollectionViewWaterfallLayout()
        kidsLayout.minimumColumnSpacing = 15.0
        kidsLayout.minimumInteritemSpacing = 30.0
        clvKids.collectionViewLayout = kidsLayout
        
        tblMenTheme.expandableDelegate = self
        tblWomenTheme.expandableDelegate = self
        tblKidsTheme.expandableDelegate = self
        
    }
    
    func registerNibs() {
        
        let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
        clvStyle.register(viewNib, forCellWithReuseIdentifier: "cell")
        clvMen.register(viewNib, forCellWithReuseIdentifier: "cell")
        clvWomen.register(viewNib, forCellWithReuseIdentifier: "cell")
        clvKids.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        let styleHeaderNib = UINib(nibName: "ShopStyleGalleryView", bundle: nil)
        clvStyle.register(styleHeaderNib, forSupplementaryViewOfKind: CHTCollectionElementKindSectionHeader, withReuseIdentifier: "ShopStyleGalleryView")
        
        let brandNib = UINib(nibName: "ShopBrandGridCell", bundle: nil)
        clvMenBrand.register(brandNib, forCellWithReuseIdentifier: "brandCell")
        clvWomenBrand.register(brandNib, forCellWithReuseIdentifier: "brandCell")
        clvKidsBrand.register(brandNib, forCellWithReuseIdentifier: "brandCell")
        
        let themeNib = UINib(nibName: "ShopThemeListHeaderCell", bundle: nil)
        let lChildNib = UINib(nibName: "ShopThemeListLeftCell", bundle: nil)
        let rChildNib = UINib(nibName: "ShopThemeListRightCell", bundle: nil)
        
        tblMenTheme.register(themeNib, forCellReuseIdentifier: "menThemeCell")
        tblMenTheme.register(lChildNib, forCellReuseIdentifier: "menLChildCell")
        tblMenTheme.register(rChildNib, forCellReuseIdentifier: "menRChildCell")
        tblWomenTheme.register(themeNib, forCellReuseIdentifier: "womenThemeCell")
        tblWomenTheme.register(lChildNib, forCellReuseIdentifier: "womenLChildCell")
        tblWomenTheme.register(rChildNib, forCellReuseIdentifier: "womenRChildCell")
        tblKidsTheme.register(themeNib, forCellReuseIdentifier: "kidsThemeCell")
        tblKidsTheme.register(lChildNib, forCellReuseIdentifier: "kidsLChildCell")
        tblKidsTheme.register(rChildNib, forCellReuseIdentifier: "kidsRChildCell")
        
    }
    
    func reloadListViews() {
        
        clvMen.delegate = self
        clvMen.dataSource = self
        clvWomen.delegate = self
        clvWomen.dataSource = self
        clvKids.delegate = self
        clvKids.dataSource = self
        
        clvMen.reloadData()
        clvWomen.reloadData()
        clvKids.reloadData()
        
        for view in vwSpin {
            view.isHidden = true
        }
        
    }
    
    @objc func onGalleryTimer() {
        
        if styleHeaderView == nil {
            return
        }
        
        let newOffset = styleHeaderView!.scvBanner.contentOffset.x
        let pageNum = ((newOffset / (styleHeaderView!.scvBanner.frame.size.width)) as NSNumber).intValue % arrBannerList.count
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.styleHeaderView!.scvBanner.contentOffset.x = self.styleHeaderView!.scvBanner.frame.size.width * CGFloat(pageNum == self.arrBannerList.count - 1 ? 0 : pageNum + 1)
        })
        
        for subview in self.styleHeaderView!.stvIndicator.subviews {
            let ivPageNum = subview as! UIImageView
            ivPageNum.image = UIImage(named: ivPageNum.tag == (pageNum == self.arrBannerList.count - 1 ? 0 : pageNum + 1) ? "ic_shop_banner_indicator_active" : "ic_shop_banner_indicator_inactive")
        }
    }
    
    func initList(_ tab : EShopTab) {
        
        switch tab {
        case .style:
            guard styleList.count <= 0 else { return }
            for nInd in 0 ..< 20 {
                styleList.append(nInd)
            }
            clvStyle.reloadData()
            vwSpin[0].isHidden = true
        case .men:
            guard menList.count <= 0 else { return }
            for nInd in 0 ..< 20 {
                menList.append(nInd)
            }
            clvMen.reloadData()
            vwSpin[1].isHidden = true
        case .women:
            guard womenList.count <= 0 else { return }
            for nInd in 0 ..< 20 {
                womenList.append(nInd)
            }
            clvWomen.reloadData()
            vwSpin[2].isHidden = true
        case .kids:
            guard kidsList.count <= 0 else { return }
            for nInd in 0 ..< 20 {
                kidsList.append(nInd)
            }
            clvKids.reloadData()
            vwSpin[3].isHidden = true
        }
        
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuStyle.isSelected = false
        btnMenuMen.isSelected = false
        btnMenuWomen.isSelected = false
        btnMenuKids.isSelected = false
        
        switch gShopPageIndex {
        case EShopTab.style.rawValue :
            btnMenuStyle.isSelected = true
            clickedBtn = btnMenuStyle
            scrollPos = clvStyle.contentOffset.y
            initList(.style)
        case EShopTab.men.rawValue :
            btnMenuMen.isSelected = true
            clickedBtn = btnMenuMen
            scrollPos = scvMen.contentOffset.y
//            initList(.men)
        case EShopTab.women.rawValue :
            btnMenuWomen.isSelected = true
            clickedBtn = btnMenuWomen
            scrollPos = scvWomen.contentOffset.y
//            initList(.women)
        case EShopTab.kids.rawValue :
            btnMenuKids.isSelected = true
            clickedBtn = btnMenuKids
            scrollPos = scvKids.contentOffset.y
//            initList(.kids)
        default:
            btnMenuStyle.isSelected = true
            clickedBtn = btnMenuStyle
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        scrollUp()
        
        nPage = 0
        closeOpenTheme()
        getDataList()
    }
    
    func scrollUp() {
        if scrollDirection == .up {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
        })
    }
    
    func scrollDown() {
        if scrollDirection == .down {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwTop.frame.height))
            self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 3 * self.vwMenu.frame.height))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -130))
        })
    }
    
    func showInteractive(_ id : Int!) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = id
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func closeOpenTheme() {

        tblMenTheme.closeAll()
        tblWomenTheme.closeAll()
        tblKidsTheme.closeAll()
        
        for i in 0..<arrMenThemeList.count {
            let headerCell = tblMenTheme.cellForRow(at: IndexPath(row: i, section: 0)) as! ShopThemeListHeaderCell
            headerCell.ivMask.alpha = 0.3
            headerCell.btnMore.isHidden = true
            headerCell.ivClose.isHidden = true
            headerCell.lbItemCount.isHidden = false
        }
        
        for i in 0..<arrWomenBrandList.count {
            let headerCell = tblWomenTheme.cellForRow(at: IndexPath(row: i, section: 0)) as! ShopThemeListHeaderCell
            headerCell.ivMask.alpha = 0.3
            headerCell.btnMore.isHidden = true
            headerCell.ivClose.isHidden = true
            headerCell.lbItemCount.isHidden = false
        }
        
        for i in 0..<arrKidsThemeList.count {
            let headerCell = tblKidsTheme.cellForRow(at: IndexPath(row: i, section: 0)) as! ShopThemeListHeaderCell
            headerCell.ivMask.alpha = 0.3
            headerCell.btnMore.isHidden = true
            headerCell.ivClose.isHidden = true
            headerCell.lbItemCount.isHidden = false
        }
    }
    
    func changePage() {
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch gShopPageIndex {
        case EShopTab.style.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 0
        case EShopTab.men.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 1
        case EShopTab.women.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 2
        case EShopTab.kids.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 3
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        pageMoved()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnStyle(_ sender: Any) {
        
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            if btnMenuStyle.isSelected {
                cell = clvStyle.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvStyle.contentOffset.y
            } else if btnMenuMen.isSelected {
                cell = clvMen.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = scvMen.contentOffset.y - clvMen.frame.origin.y
            } else if btnMenuWomen.isSelected {
                cell = clvWomen.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = scvWomen.contentOffset.y - clvWomen.frame.origin.y
            } else {
                cell = clvKids.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = scvKids.contentOffset.y - clvKids.frame.origin.y
            }
            
            var dic : PdtListDto = PdtListDto("")
            if gShopPageIndex == EShopTab.men.rawValue {
                dic = arrMenStylePdtStyleList[indexPath.row]
            } else if gShopPageIndex == EShopTab.women.rawValue {
                dic = arrWomenStylePdtStyleList[indexPath.row]
            } else if gShopPageIndex == EShopTab.kids.rawValue {
                dic = arrKidsStylePdtStyleList[indexPath.row]
            } else {
                dic = arrStylePdtStyleList[indexPath.row]
            }
            let dicPdt      : PdtMiniDto  = dic.pdt  //상품
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
        }
       
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        var dic : PdtListDto = PdtListDto("")
        if btnMenuStyle.isSelected {
            dic = arrStylePdtStyleList[sender.tag]
        } else if btnMenuMen.isSelected {
            dic = arrMenStylePdtStyleList[sender.tag]
        } else if btnMenuWomen.isSelected {
            dic = arrWomenStylePdtStyleList[sender.tag]
        } else if btnMenuKids.isSelected {
            dic = arrKidsStylePdtStyleList[sender.tag]
        }
        
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        var dic : PdtListDto = PdtListDto("")
        if btnMenuStyle.isSelected {
            dic = arrStylePdtStyleList[sender.tag]
        } else if btnMenuMen.isSelected {
            dic = arrMenStylePdtStyleList[sender.tag]
        } else if btnMenuWomen.isSelected {
            dic = arrWomenStylePdtStyleList[sender.tag]
        } else if btnMenuKids.isSelected {
            dic = arrKidsStylePdtStyleList[sender.tag]
        }
        
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.pdtStyle.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnProduct(_ sender : UIButton) {
        var dic : PdtListDto = PdtListDto("")
        if btnMenuStyle.isSelected {
            dic = arrStylePdtStyleList[sender.tag]
        } else if btnMenuMen.isSelected {
            dic = arrMenStylePdtStyleList[sender.tag]
        } else if btnMenuWomen.isSelected {
            dic = arrWomenStylePdtStyleList[sender.tag]
        } else if btnMenuKids.isSelected {
            dic = arrKidsStylePdtStyleList[sender.tag]
        }
        
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dic.pdt.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func productAction(_ sender : UITapGestureRecognizer) {
    }
    
    @IBAction func searchAction(_ sender: Any) {
        pushVC("SEARCH_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        pushVC("LIKE_VIEW", storyboard: "Top", animated: true)
//        btnFavorite.setImage(#imageLiteral(resourceName: "ic_heart_no_badge"), for: .normal)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        let clickedBtn = sender as! UIButton
        
        replaceTab(clickedBtn.tag)
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        gShopPageIndex = clickedBtn.tag
        changePage()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuStyle.isSelected {
            clvStyle.scrollToTop(isAnimated: true)
        } else if btnMenuMen.isSelected {
            scvMen.scrollToTop(isAnimated: true)
        } else if btnMenuWomen.isSelected {
            scvWomen.scrollToTop(isAnimated: true)
        } else if btnMenuKids.isSelected {
            scvKids.scrollToTop(isAnimated: true)
        }
    }
    
    @IBAction func menCategoryAction(_ sender: Any) {
        let name = ["신상품", "인기", "가격인하", "의류", "카테고리 전체보기", "슈즈", "패션소품", "가방", "시계/쥬얼리"]
        let catUids = ["-1", "-2", "-3", "5", "1", "7", "8", "6", "9"]
        let btn = sender as! UIButton
        
        if btn.tag == 4 {
            ShopCategorySelectViewController.show(self, type: 0) { (name, categoryUid) in
                gSearchInfo.setEditCategory(status: false)
                gSearchInfo.setCategoryUid(_uid: categoryUid)
                if categoryUid < 20 {
                    gCategoryUid = categoryUid
                }
                gSearchInfo.setCategoryName(value: name)
                
                let vc = (UIStoryboard.init(name: "Shop", bundle: nil).instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
                vc.pageTitle = "남성 " + name
                vc.nCategoryUid = categoryUid
                vc.kind = .man
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if btn.tag < 3 {
            gSearchInfo.setKeyword(value: "남성")
            gSearchInfo.setCategoryName(value: "남성")
            gSearchInfo.setEditCategory(status: false)
            gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[btn.tag + 1])
            
            let vc = (UIStoryboard.init(name: "Top", bundle: nil).instantiateViewController(withIdentifier: "SEARCH_RESULT_VIEW")) as! SearchResultViewController
            vc.pageTitle = "남성 " + name[btn.tag]
            vc.goPage = WherePage.shop.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            gSearchInfo.setEditCategory(status: false)
            gSearchInfo.setCategoryUid(_uid: Int(catUids[btn.tag])!)
            if Int(catUids[btn.tag])! < 20 {
                gCategoryUid = Int(catUids[btn.tag])!
            }

            gSearchInfo.setCategoryName(value: name[btn.tag])
            
            let nav : UINavigationController! = self.navigationController
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Shop", bundle: nil)
            
            let vc = (storyboard.instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
            vc.pageTitle = "남성 " + name[btn.tag]
            vc.kind = .man
            vc.nCategoryUid = Int(catUids[btn.tag])
            nav.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func womenCategoryAction(_ sender: Any) {
        let name = ["신상품", "인기", "가격인하", "의류", "카테고리 전체보기", "슈즈", "패션소품", "핸드백", "시계/쥬얼리"]
        let catUids = ["-4", "-5", "-6", "10", "2", "12", "13", "11", "14"]
        let btn = sender as! UIButton
        
        if btn.tag == 4 {
            ShopCategorySelectViewController.show(self, type: 1) { (name, categoryUid) in
                gSearchInfo.setEditCategory(status: false)
                gSearchInfo.setCategoryUid(_uid: categoryUid)
                if categoryUid < 20 {
                    gCategoryUid = categoryUid
                }
                gSearchInfo.setCategoryName(value: name)
                
                let vc = (UIStoryboard.init(name: "Shop", bundle: nil).instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
                vc.pageTitle = "여성 " + name
                vc.nCategoryUid = categoryUid
                vc.kind = .woman
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if btn.tag < 3 {
            gSearchInfo.setKeyword(value: "여성")
            gSearchInfo.setCategoryName(value: "여성")
            gSearchInfo.setEditCategory(status: false)
            gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[btn.tag + 1])

            let vc = (UIStoryboard.init(name: "Top", bundle: nil).instantiateViewController(withIdentifier: "SEARCH_RESULT_VIEW")) as! SearchResultViewController
            vc.pageTitle = "여성 " + name[btn.tag]
            vc.goPage = WherePage.shop.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            gSearchInfo.setEditCategory(status: false)
            gSearchInfo.setCategoryUid(_uid: Int(catUids[btn.tag])!)
            if Int(catUids[btn.tag])! < 20 {
                gCategoryUid = Int(catUids[btn.tag])!
            }
            gSearchInfo.setCategoryName(value: name[btn.tag])
            
            let nav : UINavigationController! = self.navigationController
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Shop", bundle: nil)
            
            let vc = (storyboard.instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
            vc.pageTitle = "여성 " + name[btn.tag]
            vc.kind = .woman
            vc.nCategoryUid = Int(catUids[btn.tag])
            nav.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func kidsCategoryAction(_ sender: Any) {
        let name = ["신상품", "인기", "가격인하", "키즈보이", "카테고리 전체보기", "키즈걸", "틴보이", "베이비", "틴걸"]
        let catUids = ["-7", "-8", "-9", "16", "4", "17", "18", "15", "19"]
        let btn = sender as! UIButton
        
        if btn.tag == 4 {
            ShopCategorySelectViewController.show(self, type: 2) { (name, categoryUid) in
                gSearchInfo.setEditCategory(status: false)
                gSearchInfo.setCategoryUid(_uid: categoryUid)
                if categoryUid < 20 {
                    gCategoryUid = categoryUid
                }
                gSearchInfo.setCategoryName(value: name)
                
                let vc = (UIStoryboard.init(name: "Shop", bundle: nil).instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
                vc.pageTitle = name
                vc.nCategoryUid = categoryUid
                vc.kind = .kids
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if btn.tag < 3 {
            gSearchInfo.setKeyword(value: "키즈")
            gSearchInfo.setCategoryName(value: "키즈")
            gSearchInfo.setEditCategory(status: false)
            gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[btn.tag + 1])

            let vc = (UIStoryboard.init(name: "Top", bundle: nil).instantiateViewController(withIdentifier: "SEARCH_RESULT_VIEW")) as! SearchResultViewController
            vc.pageTitle = "키즈 " + name[btn.tag]
            vc.goPage = WherePage.shop.rawValue
            self.navigationController?.pushViewController(vc, animated: true)

        } else {
            gSearchInfo.setEditCategory(status: false)
            gSearchInfo.setCategoryUid(_uid: Int(catUids[btn.tag])!)
            if Int(catUids[btn.tag])! < 20 {
                gCategoryUid = Int(catUids[btn.tag])!
            }
            gSearchInfo.setCategoryName(value: name[btn.tag])
            
            let nav : UINavigationController! = self.navigationController
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Shop", bundle: nil)
            
            let vc = (storyboard.instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
            vc.pageTitle = name[btn.tag]
            vc.kind = .kids
            vc.nCategoryUid = Int(catUids[btn.tag])
            nav.pushViewController(vc, animated: true)
        }
    }
    
    @objc func bannerAction(_ sender : UITapGestureRecognizer) {
        let image = sender.view as! UIImageView
        
        let dic : BannerDto = arrBannerList[image.tag]
        
        if dic.target == nil {
            return
        }
        
        switch dic.banner.type {
        case 1: // 공지사항이동
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
            let vc : SelluvNewsDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_SELLUVNEWSDETAIL_VIEW") as! SelluvNewsDetailViewController)
            vc.noticeUid = dic.banner.targetUid
            self.navigationController?.pushViewController(vc, animated: true)
        case 2: // 테마
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Shop", bundle: nil)
            let vc : ThemeMainViewController = (storyboard.instantiateViewController(withIdentifier: "THEME_MAIN_VIEW") as! ThemeMainViewController)
            vc.themeUid = dic.banner.targetUid
            self.navigationController?.pushViewController(vc, animated: true)
        case 3: //이벤트

            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
            let vc : EventDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_EVENTDETAIL_VIEW") as! EventDetailViewController)
            vc.eventUid = dic.banner.targetUid
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            break
        }
    }
    
    @objc func menBrandAction(_ sender : UIButton) {
        
        let index : Int = sender.tag
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Brand", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW")) as! BrandFollowViewController
        
        var dic : BrandMiniDto = BrandMiniDto("")
        if btnMenuMen.isSelected {
            vc.nCurPage = ETab.men.rawValue
            dic = arrMenBrandList[index]
        } else if btnMenuWomen.isSelected {
            vc.nCurPage = ETab.women.rawValue
            dic = arrWomenBrandList[index]
        } else if btnMenuKids.isSelected {
            vc.nCurPage = ETab.kids.rawValue
            dic = arrKidsBrandList[index]
        }
        vc.barndUid = dic.brandUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    @objc func themeItemAction(_ sender : UIButton) {
//
//        for item in themeList {
//            item.isSelected = false
//        }
//
//        themeList[sender.tag].isSelected = true
//        tblMenTheme.reloadData()
//
//        tblMenTheme.open(at: IndexPath(row: sender.tag, section: 0))
//
//    }
//
    @objc func themeMoreAction(_ sender : UIButton) {
        
        var dic : ThemeListDto = ThemeListDto("")
        if gShopPageIndex == ETab.men.rawValue {
            dic = arrMenThemeList[sender.tag]
        } else if gShopPageIndex == ETab.women.rawValue {
            dic = arrWomenThemeList[sender.tag]
        } else {
            dic = arrKidsThemeList[sender.tag]
        }
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Shop", bundle: nil)
        let vc : ThemeMainViewController = (storyboard.instantiateViewController(withIdentifier: "THEME_MAIN_VIEW") as! ThemeMainViewController)
        vc.themeUid = dic.theme.themeUid
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc func themeCloseAction(_ sender : UIButton) {
        
        if btnMenuMen.isSelected {
            menThemeList[sender.tag].isSelected = false
        }
        if btnMenuWomen.isSelected {
            womenThemeList[sender.tag].isSelected = false
        }
        if btnMenuKids.isSelected {
            kidsThemeList[sender.tag].isSelected = false
        }
        
    }
   
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    func getDataList()  {
        
        switch gShopPageIndex {
        case EShopTab.style.rawValue:
            getBannerList()
            vwSpin[0].isHidden = true
            break
        case EShopTab.men.rawValue:
            pdtGroup = "MALE"
            vwSpin[1].isHidden = true
            getRecommendBrandList()
            break
        case EShopTab.women.rawValue:
            pdtGroup = "FEMALE"
            vwSpin[2].isHidden = true
            getRecommendBrandList()
            break
        case EShopTab.kids.rawValue:
            pdtGroup = "KIDS"
            vwSpin[3].isHidden = true
            getRecommendBrandList()
            break
        default:
            break
        }
    }
    // 배너 목록 얻기
    func getBannerList() {
        
        gProgress.show()
        Net.getBannerList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getBannerListResult
                self.getBannerResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //배너 문의 목록 얻기 결과
    func getBannerResult(_ data: Net.getBannerListResult) {
        
        arrBannerList = data.list
        clvStyle.reloadData()
        
        if arrBannerList.count > 0 {
            pageTimer?.invalidate()
            pageTimer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(self.onGalleryTimer), userInfo: nil, repeats: true)
        }
        
        getPdtLikeUpdateYn()
    }
    
    // 좋아요 업데이트뱃지 표시여부
    func getPdtLikeUpdateYn() {
        
        gProgress.show()
        Net.getLikeUpdateStatus(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.PdtLikeUpdateStatusResult
                if res.status {
                    self.btnFavorite.setImage(UIImage(named: "ic_thema_heart"), for: .normal)
                } else {
                    self.btnFavorite.setImage(UIImage(named: "ic_thema_heart_off"), for: .normal)
                }
                
                self.getAllPdtStyleList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //전체 상품스타일 목록 얻기
    func getAllPdtStyleList() {
        
        gProgress.show()
        Net.getAllPdtStyleList(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.GetAllPdtStyleListResult
                self.getAllPdtStyleListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //전체 상품 스타일 목록 얻기 결과
    func getAllPdtStyleListResult(_ data: Net.GetAllPdtStyleListResult) {
        
        self.clvStyle.dg_stopLoading()
        self.scrollUp()
        
        isLast = data.last
        if nPage == 0 {
            arrStylePdtStyleList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrStylePdtStyleList.append(dic)
        }
        
        if arrStylePdtStyleList.count > 0 {
            clvStyle.reloadData()
        }
    }
    
    //추천 브렌드 목록 얻기
    func getRecommendBrandList() {
        
        gProgress.show()
        Net.getRecommendBrandList(
            accessToken     : gMeInfo.token,
            pdtGroupType    : pdtGroup,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.RecommendBrandResult
                self.getRecommendBrandListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //추천 브렌드 목록 얻기 결과
    func getRecommendBrandListResult(_ data: Net.RecommendBrandResult) {
        
        if gShopPageIndex == EShopTab.men.rawValue {
            arrMenBrandList = data.list
            clvMenBrand.reloadData()
        } else if gShopPageIndex == EShopTab.women.rawValue {
            arrWomenBrandList = data.list
            clvWomenBrand.reloadData()
        } else {
            arrKidsBrandList = data.list
            clvKidsBrand.reloadData()
        }
        
        getPdtStyleList()
    }
    
    //상품스타일 목록 얻기
    func getPdtStyleList() {
        
        gProgress.show()
        Net.getPdtStyleList(
            accessToken     : gMeInfo.token,
            pdtGroup        : pdtGroup,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.GetPdtStyleListResult
                self.getPdtStyleListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //상품 스타일 목록 얻기 결과
    func getPdtStyleListResult(_ data: Net.GetPdtStyleListResult) {
        
        if data.list.count > 0 {
            if gShopPageIndex == EShopTab.men.rawValue {
                isLast = data.last
                if nPage == 0 {
                    arrMenStylePdtStyleList.removeAll()
                }
                
                for i in 0..<data.list.count {
                    let dic : PdtListDto = data.list[i]
                    arrMenStylePdtStyleList.append(dic)
                }
                clvMen.reloadData()
                scvMen.reloadInputViews()
            } else if gShopPageIndex == EShopTab.women.rawValue {
                isLast = data.last
                if nPage == 0 {
                    arrWomenStylePdtStyleList.removeAll()
                }
                
                for i in 0..<data.list.count {
                    let dic : PdtListDto = data.list[i]
                    arrWomenStylePdtStyleList.append(dic)
                }
                clvWomen.reloadData()
                scvWomen.reloadInputViews()
            } else {
                isLast = data.last
                if nPage == 0 {
                    arrKidsStylePdtStyleList.removeAll()
                }
                
                for i in 0..<data.list.count {
                    let dic : PdtListDto = data.list[i]
                    arrKidsStylePdtStyleList.append(dic)
                }
                clvKids.reloadData()
                scvKids.reloadInputViews()
            }
        }
        
        getThemeList()
    }
    
    //테마 목록 얻기
    func getThemeList() {
        
        gProgress.show()
        Net.getThemeList(
            accessToken     : gMeInfo.token,
            pdtGroupType    : pdtGroup,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.ThemeListResult
                self.getthemeListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //테마 목록 얻기 결과
    func getthemeListResult(_ data: Net.ThemeListResult) {
        
        if gShopPageIndex == EShopTab.men.rawValue {
            
            scvMen.dg_stopLoading()
            scrollUp()
            
            arrMenThemeList = data.list
            tblMenThemeHeight.constant = CGFloat(160 * arrMenThemeList.count)
            tblMenTheme.reloadData()
        } else if gShopPageIndex == EShopTab.women.rawValue {
            
            scvWomen.dg_stopLoading()
            scrollUp()

            arrWomenThemeList = data.list
            tblWomenThemeHeight.constant = CGFloat(160 * arrWomenThemeList.count)
            tblWomenTheme.reloadData()
        } else {
            
            scvKids.dg_stopLoading()
            scrollUp()

            arrKidsThemeList = data.list
            tblKidsThemeHeight.constant = CGFloat(160 * arrKidsThemeList.count)
            tblKidsTheme.reloadData()
        }
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                if gShopPageIndex == EShopTab.style.rawValue {
                    self.getAllPdtStyleList()
                } else {
                    self.getPdtStyleList()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                if gShopPageIndex == EShopTab.style.rawValue {
                    self.getAllPdtStyleList()
                } else {
                    self.getPdtStyleList()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, _pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, _uid: _pdtUid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, _uid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            controller.pdtUid = _uid
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension UIScrollView {
    func scrollToBottom(isAnimated animated : Bool) {
        if self.contentSize.height < self.bounds.size.height {
            return
        }

        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }

    func scrollToTop(isAnimated animated : Bool) {
        let topOffset = CGPoint(x: 0, y: 0)
        self.setContentOffset(topOffset, animated: animated)
    }

    func scrollToTop(isAnimated animated : Bool, Offset offset : Int) {
        let topOffset = CGPoint(x: 0, y: -offset)
        self.setContentOffset(topOffset, animated: animated)
    }
}

extension ShopViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
        
            case scvView:
                
                let newOffset = scrollView.contentOffset.x
                gShopPageIndex = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 4
                
                self.pageMoved()
            
            case clvStyle, scvMen, scvWomen, scvKids:
                
                self.scrollDirection = EScrollDirection.none
            
            case (styleHeaderView?.scvBanner)! :
                
                guard styleHeaderView != nil else {return}
            
                let newOffset = styleHeaderView!.scvBanner.contentOffset.x
                let pageNum = ((newOffset / (styleHeaderView!.scvBanner.frame.size.width)) as NSNumber).intValue % arrBannerList.count
                
                for subview in styleHeaderView!.stvIndicator.subviews {
                    let ivPageNum = subview as! UIImageView
                    ivPageNum.image = ivPageNum.tag == pageNum ? #imageLiteral(resourceName: "ic_shop_banner_indicator_active") : #imageLiteral(resourceName: "ic_shop_banner_indicator_inactive")
                }
            
            default:
                
                return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvStyle, scvMen, scvWomen, scvKids:
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }
            
            scrollPos = curScrollPos
            
        default:
            return
        }
    }
}

extension ShopViewController : UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC
        
        var dic : PdtListDto = PdtListDto("")
        if collectionView == clvMen {
            dic = arrMenStylePdtStyleList[indexPath.row]
        } else if collectionView == clvWomen {
            dic = arrWomenStylePdtStyleList[indexPath.row]
        } else if collectionView == clvKids {
            dic = arrKidsStylePdtStyleList[indexPath.row]
        } else {
            dic = arrStylePdtStyleList[indexPath.row]
        }
        
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, _pdtUid: dicPdtStyle.pdtUid)
            
    }
}

extension ShopViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
            case clvStyle:
                return arrStylePdtStyleList.count
            case clvMen:
                return arrMenStylePdtStyleList.count
            case clvWomen:
                return arrWomenStylePdtStyleList.count
            case clvKids:
                return arrKidsStylePdtStyleList.count
            case clvMenBrand:
                return arrMenBrandList.count
            case clvWomenBrand:
                return arrWomenBrandList.count
            case clvKidsBrand:
                return arrKidsBrandList.count
            default:
                return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == clvMenBrand  || collectionView == clvWomenBrand || collectionView == clvKidsBrand {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "brandCell", for: indexPath) as! ShopBrandGridCell
            var dic : BrandMiniDto = BrandMiniDto("")
            if collectionView == clvMenBrand {
                dic = arrMenBrandList[indexPath.row]
            } else if collectionView == clvWomenBrand {
                dic = arrWomenBrandList[indexPath.row]
            } else {
                dic = arrKidsBrandList[indexPath.row]
            }
            
            cell.ivBrandThumbnail.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell.btnCell.tag = indexPath.row
            cell.btnCell.addTarget(self, action: #selector(menBrandAction), for: .touchUpInside)
            
            return cell
        }
        
        var listCnt = 0
        var dic : PdtListDto = PdtListDto("")
        if collectionView == clvMen {
            dic = arrMenStylePdtStyleList[indexPath.row]
            listCnt = arrMenStylePdtStyleList.count
            clvMenHeight.constant = clvMen.contentSize.height
        } else if collectionView == clvWomen {
            dic = arrWomenStylePdtStyleList[indexPath.row]
            listCnt = arrWomenStylePdtStyleList.count
            clvWomenHeight.constant = clvWomen.contentSize.height
        } else if collectionView == clvKids {
            dic = arrKidsStylePdtStyleList[indexPath.row]
            listCnt = arrKidsStylePdtStyleList.count
            clvKidsHeight.constant = clvKids.contentSize.height
        } else {
            dic = arrStylePdtStyleList[indexPath.row]
            listCnt = arrStylePdtStyleList.count
        }

        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
        let blikeStatus     : Bool               = dic.pdtLikeStatus  //유저 상품좋아요 상태
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC

        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
            height = width * ratio
        }
        cell._imgComHeight.constant = height
        
        cell._imgHeart.image = UIImage(named: indexPath.row % 2 == 0 ? "ic_like_hart" : "ic_dislike_hart")
        cell._btnStyle.isHidden = true
        cell._imgEffect.isHidden = true
        
        cell._imgCom.kf.setImage(with: URL(string: dicPdtStyle.styleImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        if dicPdt.originPrice <= dicPdt.price {
            cell.hiddenDown(price: dicPdt.price)
        } else {
            cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
        }
        cell.lbSize.text = dicPdt.pdtSize
        cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
        
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
        cell._lbBrand.text = dicBrand.nameEn
        cell._lbLike.text = String(likeCnt)
        cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
        
        //user
        cell._lbName.text = dicUsr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        cell._btnProduct.tag = indexPath.row
        cell._btnProduct.addTarget(self, action:#selector(self.onBtnProduct(_:)), for: UIControlEvents.touchUpInside)
        
        //더보기
        if indexPath.row == listCnt - 1 && !isLast && collectionView.indexPathsForVisibleItems.contains(indexPath) {
            nPage = nPage + 1
            if gShopPageIndex == EShopTab.style.rawValue {
                getAllPdtStyleList()
            } else {
                getPdtStyleList()
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if self.styleHeaderView != nil {
//            return self.styleHeaderView!
            self.styleHeaderView = nil
        }

        self.styleHeaderView = (collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShopStyleGalleryView", for: indexPath as IndexPath) as! ShopStyleGalleryView)
        
        self.styleHeaderView!.scvBanner.delegate = self

        let bannerCnt = arrBannerList.count
        self.styleHeaderView!.scvBanner.contentSize = CGSize.init(width: (self.view.frame.size.width - 30) * CGFloat(bannerCnt), height: self.styleHeaderView!.scvBanner.frame.size.height)

        for nInd in 0 ..< bannerCnt {
            let ivBanner = UIImageView(frame: CGRect.init(origin: CGPoint(x: CGFloat(nInd) * (self.view.frame.size.width - 30), y: 0), size: CGSize(width: self.view.frame.size.width - 30, height: self.styleHeaderView!.scvBanner.frame.size.height)))

            let dic : BannerDto = arrBannerList[nInd]
            
            // dim
            ivBanner.backgroundColor = UIColor.black
            ivBanner.layer.opacity = 0.9
            
            ivBanner.isUserInteractionEnabled = true
            ivBanner.clipsToBounds = true
            ivBanner.contentMode = UIViewContentMode.scaleAspectFill
            ivBanner.tag = nInd
            
            ivBanner.kf.setImage(with: URL(string: dic.banner.profileImg), placeholder: UIImage(named: "img_default"), options: [], progressBlock: nil, completionHandler: nil)
            
            ivBanner.tag = nInd
            ivBanner.isUserInteractionEnabled = true
            ivBanner.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bannerAction(_ :))))
            
            self.styleHeaderView!.scvBanner.addSubview(ivBanner)
            
            let image = UIImage(named: nInd == 0 ? "ic_shop_banner_indicator_active" : "ic_shop_banner_indicator_inactive")
            let ivPageNum = UIImageView(frame: CGRect.init(origin: CGPoint(x: CGFloat(image!.size.width + 5.0) * CGFloat(nInd), y: 0), size: CGSize(width: image!.size.width, height: image!.size.height)))
            ivPageNum.tag = nInd
            ivPageNum.image = image
            self.styleHeaderView!.stvIndicator.addSubview(ivPageNum)
            self.styleHeaderView!.stvIndicatorWidth.constant = CGFloat(image!.size.width) * CGFloat(nInd + 1) + 5.0 * CGFloat(nInd)
        }

        return self.styleHeaderView!
    }
    
}

extension ShopViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if collectionView == clvMenBrand || collectionView == clvWomenBrand || collectionView == clvKidsBrand {
            return CGSize(width: 76, height: 76)
        }
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
    
        //pdt
        var dic : PdtListDto = PdtListDto("")
        if collectionView == clvMen {
            dic = arrMenStylePdtStyleList[indexPath.row]
        } else if collectionView == clvWomen {
            dic = arrWomenStylePdtStyleList[indexPath.row]
        } else if collectionView == clvKids {
            dic = arrKidsStylePdtStyleList[indexPath.row]
        } else {
            dic = arrStylePdtStyleList[indexPath.row]
        }
        
        if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
            height = width * ratio
        }
        
        if dic.pdt.originPrice <= dic.pdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        if collectionView == clvMenBrand || collectionView == clvWomenBrand || collectionView == clvKidsBrand {
            return 22.0
        }
        
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch collectionView {
            case clvStyle:
                return 349
            default:
                return 0
        }
        
    }
    
}

extension ShopViewController : ExpandableDelegate {
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        
        var cellId1 = "menLChildCell"
        var cellId2 = "menRChildCell"
        var pdtCnt = 0
        var arrTableCell = [UITableViewCell]()
        var dic : ThemeListDto = ThemeListDto("")
        
        switch expandableTableView {
        case tblMenTheme:
            cellId1 = "menLChildCell"
            cellId2 = "menRChildCell"
            dic = arrMenThemeList[indexPath.row]
            pdtCnt = dic.pdtList.count
        case tblWomenTheme:
            cellId1 = "womenLChildCell"
            cellId2 = "womenRChildCell"
            dic = arrWomenThemeList[indexPath.row]
            pdtCnt = dic.pdtList.count
        case tblKidsTheme:
            cellId1 = "kidsLChildCell"
            cellId2 = "kidsRChildCell"
            dic = arrKidsThemeList[indexPath.row]
            pdtCnt = dic.pdtList.count
        default:
            break
        }
        
//        cell1.vwProduct.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(productAction(_:))))
//        cell2.vwProduct.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(productAction(_:))))
//        cell3.vwProduct.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(productAction(_:))))
        
        if pdtCnt > 0 {
            let cell1 = expandableTableView.dequeueReusableCell(withIdentifier: cellId1) as! ShopThemeListLeftCell
            cell1.vwProduct.tag = indexPath.row * 10 + 0
            let dicPdt : PdtListDto = dic.pdtList[0]
            
            cell1.ivProduct.kf.setImage(with: URL(string: dicPdt.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell1.lbEBrand.text = dicPdt.brand.nameEn
            cell1.lbInfo.text = String.init(format: "%@ %@ %@", dicPdt.brand.nameKo, dicPdt.pdt.colorName, dicPdt.category.categoryName)
            cell1.lbCost.text = "₩ " + CommonUtil.formatNum(dicPdt.pdt.price)
            
            arrTableCell.append(cell1)
        }
        
        if pdtCnt > 1 {
            let cell2 = expandableTableView.dequeueReusableCell(withIdentifier: cellId2) as! ShopThemeListRightCell
            cell2.vwProduct.tag = indexPath.row * 10 + 1
            let dicPdt : PdtListDto = dic.pdtList[1]

            cell2.ivProduct.kf.setImage(with: URL(string: dicPdt.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell2.lbEBrand.text = dicPdt.brand.nameEn
            cell2.lbInfo.text = String.init(format: "%@ %@ %@", dicPdt.brand.nameKo, dicPdt.pdt.colorName, dicPdt.category.categoryName)
            cell2.lbCost.text = "₩ " + CommonUtil.formatNum(dicPdt.pdt.price)
            
            arrTableCell.append(cell2)
        }
        
        if pdtCnt > 2 {
            let cell3 = expandableTableView.dequeueReusableCell(withIdentifier: cellId1) as! ShopThemeListLeftCell
            cell3.vwProduct.tag = indexPath.row * 10 + 2
            let dicPdt : PdtListDto = dic.pdtList[2]
            cell3.ivProduct.kf.setImage(with: URL(string: dicPdt.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell3.lbEBrand.text = dicPdt.brand.nameEn
            cell3.lbInfo.text = String.init(format: "%@ %@ %@", dicPdt.brand.nameKo, dicPdt.pdt.colorName, dicPdt.category.categoryName)
            cell3.lbCost.text = "₩ " + CommonUtil.formatNum(dicPdt.pdt.price)
            arrTableCell.append(cell3)
        }
        
        return arrTableCell
      
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        
        var pdtCnt = 0
        var arrTableCellHeight = [CGFloat]()
        
        var cellId = "menThemeCell"
        switch expandableTableView {
        case tblMenTheme:
            cellId = "menThemeCell"
            let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellId) as! ShopThemeListHeaderCell
            if cell.btnMore.tag < 0 {
                arrTableCellHeight.append(0)
                return arrTableCellHeight
            }

            let dic : ThemeListDto = arrMenThemeList[cell.btnMore.tag]
            pdtCnt = dic.pdtList.count
        case tblWomenTheme:
            cellId = "womenThemeCell"
            let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellId) as! ShopThemeListHeaderCell
            
            if cell.btnMore.tag < 0 {
                arrTableCellHeight.append(0)
                return arrTableCellHeight
            }
            
            let dic : ThemeListDto = arrWomenThemeList[cell.btnMore.tag]
            pdtCnt = dic.pdtList.count
        case tblKidsTheme:
            cellId = "kidsThemeCell"
            let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellId) as! ShopThemeListHeaderCell
            
            if cell.btnMore.tag < 0 {
                arrTableCellHeight.append(0)
                return arrTableCellHeight
            }
            let dic : ThemeListDto = arrKidsThemeList[cell.btnMore.tag]
            pdtCnt = dic.pdtList.count
        default:
            break
        }
        
        if pdtCnt > 0 {
            arrTableCellHeight.append(160)
        } else {
            arrTableCellHeight.append(0)
        }
        
        if pdtCnt > 1 {
            arrTableCellHeight.append(160)
        } else {
            arrTableCellHeight.append(0)
        }
        
        if pdtCnt > 2 {
            arrTableCellHeight.append(160)
        } else {
            arrTableCellHeight.append(0)
        }
        return arrTableCellHeight
    }
    
    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        switch expandableTableView {
        case tblMenTheme:
            return arrMenThemeList.count
        case tblWomenTheme:
            return arrWomenThemeList.count
        case tblKidsTheme:
            return arrKidsThemeList.count
        default:
            return 0
        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, willExpandRowAt indexPath: IndexPath, expandSize: [CGFloat]?, isExpanded : Bool) {
        
        guard let addSize = expandSize else { return }
        
        var sum : CGFloat = 0
        for item in addSize {
            sum += item
        }
        
        let cell = expandableTableView.cellForRow(at: indexPath)
        
        if (cell?.isKind(of: ShopThemeListHeaderCell.self))! {
            switch expandableTableView {
            case tblMenTheme:
                tblMenThemeHeight.constant += isExpanded ? sum : -sum
            case tblWomenTheme:
                tblWomenThemeHeight.constant += isExpanded ? sum : -sum
            case tblKidsTheme:
                tblKidsThemeHeight.constant += isExpanded ? sum : -sum
            default:
                return
            }
        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = expandableTableView.cellForRow(at: indexPath)
//        
//        if (cell?.isKind(of: ShopThemeListHeaderCell.self))! {
//            switch expandableTableView {
//            case tblMenTheme:
//                tblMenThemeHeight.constant = tblMenTheme.contentSize.height
//            case tblMenTheme:
//                tblWomenThemeHeight.constant = tblWomenTheme.contentSize.height
//            case tblMenTheme:
//                tblKidsThemeHeight.constant = tblKidsTheme.contentSize.height
//            default:
//                return
//            }
//        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        
        var index  = -1
        var section = 0
        switch expandedCell.reuseIdentifier! {
        case "menLChildCell", "womenLChildCell", "kidsLChildCell":
            let cell = expandableTableView.cellForRow(at: indexPath) as! ShopThemeListLeftCell
            selectedImageView = cell.ivProduct
            index = cell.vwProduct.tag % 10
            section = cell.vwProduct.tag / 10
        case "menRChildCell", "womenRChildCell", "kidsRChildCell":
            let cell = expandableTableView.cellForRow(at: indexPath) as! ShopThemeListRightCell
            selectedImageView = cell.ivProduct
            index = cell.vwProduct.tag % 10
            section = cell.vwProduct.tag / 10
        default:
            return
        }
        
        
        var dicTheme : ThemeListDto = ThemeListDto("")
        switch expandableTableView {
        case tblMenTheme:
            dicTheme = arrMenThemeList[section]
        case tblWomenTheme:
            dicTheme = arrWomenThemeList[section]
        case tblKidsTheme:
            dicTheme = arrKidsThemeList[section]
        default:
            break
        }
        
        showInteractive(dicTheme.pdtList[index].pdt.pdtUid)
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellId = "menThemeCell"
        var dic : ThemeListDto = ThemeListDto("")
        switch expandableTableView {
        case tblMenTheme:
            cellId = "menThemeCell"
            dic = arrMenThemeList[indexPath.row]
        case tblWomenTheme:
            cellId = "womenThemeCell"
            dic = arrWomenThemeList[indexPath.row]
        case tblKidsTheme:
            cellId = "kidsThemeCell"
            dic = arrKidsThemeList[indexPath.row]
        default:
            cellId = "menThemeCell"
            dic = arrMenThemeList[indexPath.row]
        }
        
        let cell = expandableTableView.dequeueReusableCell(withIdentifier: cellId) as! ShopThemeListHeaderCell
        
        cell.ivBg.kf.setImage(with: URL(string: dic.theme.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        cell.lbItemCount.text = String(dic.pdtCount) + " Items"
        cell.lbThemeName.text = dic.theme.titleKo
        
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(themeMoreAction), for: .touchUpInside)
        
        return cell
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func expandableTableView(_ expandableTableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = expandableTableView.cellForRow(at: indexPath)
        
        if cell?.reuseIdentifier == "menThemeCell" ||  cell?.reuseIdentifier == "womenThemeCell" || cell?.reuseIdentifier == "kidsThemeCell" {
            
            let headerCell = cell as! ShopThemeListHeaderCell

            if headerCell.isExpanded() {
                headerCell.ivMask.alpha = 0.3
                headerCell.btnMore.isHidden = true
                headerCell.ivClose.isHidden = true
                headerCell.lbItemCount.isHidden = false
            } else {
                headerCell.ivMask.alpha = 0.8
                headerCell.btnMore.isHidden = false
                headerCell.ivClose.isHidden = false
                headerCell.lbItemCount.isHidden = true
            }
        }
    
    }
}

extension ShopViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return selectedImageView ?? UIImageView()
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        guard let selectedImageView = selectedImageView else { return CGRect.zero }
        return selectedImageView.convert(selectedImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        selectedImageView?.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        selectedImageView?.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        selectedImageView?.isHidden = false
    }
}
