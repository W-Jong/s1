//
//  ShopDetailViewController.swift
//  selluv
//  shop detail
//  Created by Gambler on 12/16/17.
//  modified by PJH on 31/03/18.

import UIKit

class ShopDetailViewController: BaseViewController{

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    
    @IBOutlet weak var vwBrand: UIView!
    @IBOutlet weak var clvBrand: UICollectionView!
    
    @IBOutlet weak var vwCategory: UIView!
    @IBOutlet weak var clvCategory: UICollectionView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnMenuBrand: UIButton!
    @IBOutlet weak var btnMenuCategory: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var lcvwMenuBarHeight: NSLayoutConstraint! //40
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var vwNone: UIView!
    @IBOutlet weak var vwNoPdtData: UIView!
    @IBOutlet weak var vwNoneTop: NSLayoutConstraint!
    @IBOutlet weak var vwMenu: UIView!
    @IBOutlet weak var vwMenuBottom: NSLayoutConstraint!
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    
    //zoom효과관련
    weak var selectedImageView : UIImageView?
    
    var animator : ARNTransitionAnimator?
    
    enum EShopDetail : Int {
        case brand = 0
        case category
    }
    
    enum ECategory : Int {
        case c0, c1, c2, c3
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    var brandHeaderView : ShopDetailBrandHeaderView?
    var categoryHeaderView : ShopDetailCategoryHeaderView?
    
    var pageTitle : String = ""
    var category = ECategory.c0
    var kind = EKind.man
    
    var nCurPage     : Int = EShopDetail.brand.rawValue
    var nCategoryUid : Int!
    var nBrandUid    : Int = 0
    var nTempCategoryUid : Int!

    var arrCategorys    : Array<CategoryDao> = []
    var arrBrands       : Array<BrandRecommendedListDto> = []
    
    var nPage       : Int = 0
    var isLast      : Bool!
    var arrPdtList  : Array<PdtListDto> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        nTempCategoryUid = nCategoryUid  //임시로 보관해두기 위해서이다.
        
        initVars()
        setupCLVLayout()
        registerNibs()
        lbTitle.text = pageTitle
        
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: 0.5)
            DispatchQueue.main.async {
                self.pageMoved()
            }
        }
    }
    
    func initVars() {
        
        
    }
    
    func setupCLVLayout() {
        
        // 컬렉션뷰들에 FlowLayout 적용하기
        let brandLayout = CHTCollectionViewWaterfallLayout()
        brandLayout.minimumColumnSpacing = 15.0
        brandLayout.minimumInteritemSpacing = 30.0
        clvBrand.collectionViewLayout = brandLayout
        
        let categoryLayout = CHTCollectionViewWaterfallLayout()
        categoryLayout.minimumColumnSpacing = 15.0
        categoryLayout.minimumInteritemSpacing = 30.0
        clvCategory.collectionViewLayout = categoryLayout
        
    }
    
    func registerNibs() {
        
        let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
        clvBrand.register(viewNib, forCellWithReuseIdentifier: "cell")
        clvCategory.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        let brandHeaderNib = UINib(nibName: "ShopDetailBrandHeaderView", bundle: nil)
        clvBrand.register(brandHeaderNib, forSupplementaryViewOfKind: CHTCollectionElementKindSectionHeader, withReuseIdentifier: "ShopDetailBrandHeaderView")
        let categoryHeaderNib = UINib(nibName: "ShopDetailCategoryHeaderView", bundle: nil)
        clvCategory.register(categoryHeaderNib, forSupplementaryViewOfKind: CHTCollectionElementKindSectionHeader, withReuseIdentifier: "ShopDetailCategoryHeaderView")
        
        brandHeaderView?.btnSetting.isSelected = false
        categoryHeaderView?.btnSetting.isSelected = false
        
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuBrand.isSelected = false
        btnMenuCategory.isSelected = false
    
        nCategoryUid = nTempCategoryUid
        nBrandUid = 0
        
        switch nCurPage {
        case EShopDetail.brand.rawValue :
            btnMenuBrand.isSelected = true
            clickedBtn = btnMenuBrand
        case EShopDetail.category.rawValue :
            btnMenuCategory.isSelected = true
            clickedBtn = btnMenuCategory
        default:
            btnMenuBrand.isSelected = true
            clickedBtn = btnMenuBrand
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        nPage = 0
        vwNone.isHidden = true
        vwNoPdtData.isHidden = true
        gSearchInfo.initial()
        gSearchInfo.setCategoryUid(_uid: nTempCategoryUid)
        getCategoryDetailList()
    }
    
    func scrollUp() {
        if scrollDirection == .up {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
        })
    }
    
    func scrollDown() {
        if scrollDirection == .down {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwTop.frame.height))
            self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 3 * self.vwMenu.frame.height))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -130))
        })
    }
    
    func showInteractive(_uid : Int) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = _uid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(toSearch), name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil)
    }
    
    @objc func toSearch(_ notification: NSNotification) {
        removeObserver()
        getSearchDetailList()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnStyle(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, pdtUid: dicPdtStyle.pdtUid)

    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            
            if btnMenuCategory.isSelected {
                cell = clvCategory.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvCategory.contentOffset.y
            } else {
                cell = clvBrand.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvBrand.contentOffset.y
            }
            
            let dic : PdtListDto = arrPdtList[indexPath.row]
            let dicPdt          : PdtMiniDto         = dic.pdt  //상품
            
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
        }
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        
        let dic1 : PdtListDto  = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic1.pdt.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        self.pushVC("LIKE_VIEW", storyboard: "Top", animated: true)        
    }
    
    @IBAction func menuAction(_ sender: Any) {
        let clickedBtn = sender as! UIButton
        self.replaceTab(clickedBtn.tag)
        
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case EShopDetail.brand.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 0
        case EShopDetail.category.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 1
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        nCurPage = clickedBtn.tag
        pageMoved()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuBrand.isSelected {
            clvBrand.scrollToTop(isAnimated: true)
        } else if btnMenuCategory.isSelected {
            clvCategory.scrollToTop(isAnimated: true)
        }
    }
    
    @objc func brandItemAction(_ sender : UIButton) {
        nBrandUid = 0
        for i in 0..<arrBrands.count {
            
            let dic : BrandRecommendedListDto = arrBrands[i]
            if i == sender.tag {
                if dic.isSelect {
                    dic.isSelect = false
                } else {
                    dic.isSelect = true
                    nBrandUid = dic.brandUid
                }
            } else {
                dic.isSelect = false
            }
        }
        brandHeaderView!.clvBrand.reloadData()
        getCategoryPdtList()
    }
    
    @objc func categoryItemAction(_ sender : UIButton) {
        
        nCategoryUid = nTempCategoryUid
        for i in 0..<arrCategorys.count {
            let dic : CategoryDao = arrCategorys[i]
            
            if i == sender.tag {
                if dic.isSelect {
                    dic.isSelect = false
                } else {
                    dic.isSelect = true
                    nCategoryUid = dic.categoryUid
                }
            } else {
                dic.isSelect = false
            }
        }
        
        categoryHeaderView!.clvCategory.reloadData()
        getCategoryPdtList()
    }
    
    @objc func brandSettingAction(_ sender : UIButton) {
        addObserver()
        gSearchInfo.setEditCategory(status: false)
        pushVC("FILTER_VIEW", storyboard: "Top", animated: true)
        
    }
    
    @objc func categorySettingAction(_ sender : UIButton) {
        pushVC("FILTER_VIEW", storyboard: "Top", animated: true)
        
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 좋아요 업데이트뱃지 표시여부
    func getPdtLikeUpdateYn() {
        
        gProgress.show()
        Net.getLikeUpdateStatus(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.PdtLikeUpdateStatusResult
                if res.status {
                    self.btnFavorite.setImage(UIImage(named: "ic_thema_heart"), for: .normal)
                } else {
                    self.btnFavorite.setImage(UIImage(named: "ic_thema_heart_off"), for: .normal)
                }
                
                
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    // 카테고리별 인기브랜드 및 하위카테고리 얻기
    func getCategoryDetailList() {
        gProgress.show()
        Net.getCategoryDetailList(
            accessToken     : gMeInfo.token,
            categoryUid     : nCategoryUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getCategoryDetailListResult
                self.getSubCategoryListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //카테고리별 인기브랜드 및 하위카테고리 얻기 결과
    func getSubCategoryListResult(_ data: Net.getCategoryDetailListResult) {

        arrCategorys = data.arrCategory
        arrBrands = data.arrBrand
        
        if arrCategorys.count == 0 && arrBrands.count == 0 {
            vwNoPdtData.isHidden = false
        } else {
            vwNoPdtData.isHidden = true
        }
        
        //하위카테고리가 없는 경우 상세카테고리 탭 없애기
        if arrCategorys.count == 0 {
            lcvwMenuBarHeight.constant = 0
        } else {
            lcvwMenuBarHeight.constant = 40
        }
    
        brandHeaderView!.clvBrand.reloadData()
        categoryHeaderView!.clvCategory.reloadData()
        
        getCategoryPdtList()
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, pdtUid: pdtUid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, pdtUid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            vc.pdtUid = pdtUid
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getCategoryPdtList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getCategoryPdtList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 해당 카테고리에 속하는 상품목록얻기(브랜드기준)
    func getCategoryPdtList() {
        gProgress.show()
        Net.getCategoryPdtList(
            accessToken     : gMeInfo.token,
            categoryUid     : nCategoryUid,
            page            : nPage,
            brandUid        : nBrandUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getCategoryPdtListResult
                self.getCategoryPdtListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
     
    }
    
    //해당 카테고리에 속하는 상품목록얻기(브랜드기준) 결과
    func getCategoryPdtListResult(_ data: Net.getCategoryPdtListResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrPdtList.append(dic)
        }
        
        if nCurPage == EShopDetail.brand.rawValue {
            brandHeaderView!.lbCnt.text = "( " + String(arrPdtList.count) + " )"
            if arrPdtList.count > 0 {
                vwNone.isHidden = true
                clvBrand.reloadData()
            } else {
                vwNone.isHidden = false
                vwNoneTop.constant = 277
            }
        } else {
            categoryHeaderView!.lbCnt.text = "( " + String(arrPdtList.count) + " )"
            if arrPdtList.count > 0 {
                vwNone.isHidden = true
                clvCategory.reloadData()
            } else {
                vwNone.isHidden = false
                vwNoneTop.constant = 210
            }
        }
        
        getPdtLikeUpdateYn()
    }
    
    //상세검색
    func getSearchDetailList() {
        
        gProgress.show()
        Net.getSearchDetail(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSearchDetailResult
                self.getSearchDetailResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //상세검색 결과
    func getSearchDetailResult(_ data: Net.getSearchDetailResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrPdtList.append(dic)
        }
        
        if nCurPage == EShopDetail.brand.rawValue {
            brandHeaderView!.lbCnt.text = "( " + String(arrPdtList.count) + " )"
            if arrPdtList.count > 0 {
                vwNone.isHidden = true
                clvBrand.reloadData()
            } else {
                vwNone.isHidden = false
                vwNoneTop.constant = 277
            }
        } else {
            categoryHeaderView!.lbCnt.text = "( " + String(arrPdtList.count) + " )"
            if arrPdtList.count > 0 {
                vwNone.isHidden = true
                clvCategory.reloadData()
            } else {
                vwNone.isHidden = false
                vwNoneTop.constant = 210
            }
        }
        
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension ShopDetailViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            nCurPage = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 4
            
            self.pageMoved()
            
        case clvBrand, clvCategory:
            
            self.scrollDirection = EScrollDirection.none
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvBrand, clvCategory:
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }
            
            scrollPos = curScrollPos
            
        default:
            return
        }
    }
}

extension ShopDetailViewController : UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC
        
        self.selectedImageView = cell._imgCom

        let dic : PdtListDto = arrPdtList[indexPath.row]
        self.showInteractive(_uid: dic.pdt.pdtUid)
    }
}

extension ShopDetailViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == clvBrand || collectionView == clvCategory {
            return arrPdtList.count
        } else {
            if self.brandHeaderView != nil && self.brandHeaderView!.clvBrand == collectionView {
                return arrBrands.count
            } else if self.categoryHeaderView != nil && self.categoryHeaderView!.clvCategory == collectionView {
                return arrCategorys.count
            }
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.brandHeaderView != nil && collectionView == self.brandHeaderView!.clvBrand {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShopDetailBrandGridCell
            
            let dic : BrandRecommendedListDto = arrBrands[indexPath.row]
            
            if dic.isSelect {
                cell.ivMask.alpha = 0.2
            } else {
                cell.ivMask.alpha = 0.8
            }
            
            cell.ivBg.kf.setImage(with: URL(string: dic.backImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

            if dic.logoImg == "" {
                cell.lbLogo.isHidden = false
                cell.ivLogo.isHidden = true
                cell.lbLogo.text = dic.nameEn
            } else {
                cell.lbLogo.isHidden = true
                cell.ivLogo.isHidden = false
                cell.ivLogo.kf.setImage(with: URL(string: dic.logoImg), placeholder: UIImage(named: ""), options: [], progressBlock: nil, completionHandler: nil)
            }
            cell.lbCnt.text = String(dic.pdtCount)
            
            cell.btnCell.tag = indexPath.row
            cell.btnCell.addTarget(self, action: #selector(brandItemAction), for: .touchUpInside)
            
            return cell
            
        } else if self.categoryHeaderView != nil && collectionView == self.categoryHeaderView!.clvCategory {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShopDetailCategoryGridCell
            
            let dic : CategoryDao = arrCategorys[indexPath.row]
            
            cell.btnCell.tag = indexPath.row
            cell.btnCell.setTitle(dic.categoryName, for: .normal)
            
            cell.btnCell.borderColor = dic.isSelect ? UIColor(hex : 0x42c2fe) : UIColor(hex : 0xc1c1c1)
            cell.btnCell.addTarget(self, action: #selector(categoryItemAction), for: .touchUpInside)
            
            return cell
        }

        let dic : PdtListDto = arrPdtList[indexPath.row]
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
        let blikeStatus     : Bool               = dic.pdtLikeStatus  //유저 상품좋아요 상태
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC
//        let width = (collectionView.bounds.width - 15) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
        
        //        cell._imgComWidth.constant = width
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
            height = width * ratio
        }
        cell._imgComHeight.constant = height
        
        cell._imgHeart.image = UIImage(named: indexPath.row % 2 == 0 ? "ic_like_hart" : "ic_dislike_hart")
        cell._btnStyle.isHidden = true
        cell._imgEffect.isHidden = true
        
        cell._imgCom.kf.setImage(with: URL(string: dicPdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        if dicPdt.originPrice <= dicPdt.price {
            cell.hiddenDown(price: dicPdt.price)
        } else {
            cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
        }
        cell.lbSize.text = dicPdt.pdtSize
        cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
        
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
        cell._lbBrand.text = dicBrand.nameEn
        cell._lbLike.text = String(likeCnt)
        cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
        
        //user
        cell._lbName.text = dicUsr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        cell._btnProduct.isHidden = true
        
        if indexPath.row == arrPdtList.count - 1 && !isLast {
            nPage = nPage + 1
            getCategoryPdtList()
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == clvBrand {
            
            if self.brandHeaderView != nil {
                return self.brandHeaderView!
            }
            
            self.brandHeaderView = (collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShopDetailBrandHeaderView", for: indexPath as IndexPath) as! ShopDetailBrandHeaderView)
            
            self.brandHeaderView!.clvBrand.delegate = self
            self.brandHeaderView!.clvBrand.dataSource = self
            self.brandHeaderView!.btnSetting.addTarget(self, action: #selector(brandSettingAction), for: .touchUpInside)
            
            return self.brandHeaderView!
            
        } else {
            
            if self.categoryHeaderView != nil {
                return self.categoryHeaderView!
            }
            
            self.categoryHeaderView = (collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ShopDetailCategoryHeaderView", for: indexPath as IndexPath) as! ShopDetailCategoryHeaderView)
            
            self.categoryHeaderView!.clvCategory.delegate = self
            self.categoryHeaderView!.clvCategory.dataSource = self
            self.categoryHeaderView!.btnSetting.addTarget(self, action: #selector(categorySettingAction), for: .touchUpInside)
            
            return self.categoryHeaderView!
            
        }
        
    }
    
}

extension ShopDetailViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if self.brandHeaderView != nil && collectionView == self.brandHeaderView!.clvBrand {
            return CGSize(width: self.brandHeaderView!.clvBrand.bounds.height, height: self.brandHeaderView!.clvBrand.bounds.height)
        }
        
        if self.categoryHeaderView != nil && collectionView == self.categoryHeaderView!.clvCategory {
            let dic : CategoryDao = arrCategorys[indexPath.row]
            return CGSize(width: dic.categoryName.widthToFit(32.0, UIFont.systemFont(ofSize: 13)) + 24, height: 32)
        }
        
//        let width = (collectionView.bounds.width - 15) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
//
//        return CGSize(width: width, height: height + 153)
        let dic : PdtListDto = arrPdtList[indexPath.row]
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        
        if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
            height = width * ratio
        }
        
        if dic.pdt.originPrice <= dic.pdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        if self.brandHeaderView != nil && collectionView == self.brandHeaderView!.clvBrand {
            return 8.0
        }
        
        if self.categoryHeaderView != nil && collectionView == self.categoryHeaderView!.clvCategory {
            return 7.0
        }
        
        return 0.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch collectionView {
            case clvBrand:
                return 277
            case clvCategory:
                return 210
            default:
                return 0
        }
        
    }
}

extension ShopDetailViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return selectedImageView ?? UIImageView()
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        guard let selectedImageView = selectedImageView else { return CGRect.zero }
        return selectedImageView.convert(selectedImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        selectedImageView?.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        selectedImageView?.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        selectedImageView?.isHidden = false
    }
}
