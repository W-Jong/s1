//
//  ShopCategorySelectViewController.swift
//  selluv
//
//  Created by Gambler on 12/15/17.
//

import UIKit

class ShopCategorySelectViewController: UIViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var ivBg: UIImageView!
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    enum EShopCategory : Int {
        case c1 = 1
        case c2
        case c3
    }
    
    public typealias callback = (_ category : String, _ categoryUid : Int) -> ()
    
    var cbOk : callback! = nil
    var type : Int = EKind.man.rawValue
    var categoryList = [(name: "", val: 0, img: "")]
    
    var nCategoryUid    : Int!
    var arrCategorys    : Array<CategoryDao> = []
    
    var step = EShopCategory.c1
    var category = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        btnBack.isHidden = true
        
        tblList.register(UINib(nibName: "ShopCategoryListCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        //        tblList.rowHeight = UITableViewAutomaticDimension
        //        tblList.estimatedRowHeight = 60
        
        switch type {
        case EKind.man.rawValue:
            vwBg.backgroundColor = UIColor(hex: 0x42C2FE)
            lbName.text = "남성"
            ivImage.image = UIImage(named: "ic_shop_menlist")
            categoryList = MenCategory1
            self.nCategoryUid = 1
        case EKind.woman.rawValue:
            vwBg.backgroundColor = UIColor(hex: 0xFF3B7E)
            lbName.text = "여성"
            ivImage.image = UIImage(named: "ic_shop_womenlist")
            categoryList = WomenCategory1
            self.nCategoryUid = 2
        case EKind.kids.rawValue:
            vwBg.backgroundColor = UIColor(hex: 0x3A0D7D)
            lbName.text = "키즈"
            ivImage.image = UIImage(named: "ic_shop_kids_list")
            categoryList = KidsCategory1
            self.nCategoryUid = 4
        default:
            vwBg.backgroundColor = UIColor(hex: 0x42C2FE)
            categoryList = MenCategory1
            self.nCategoryUid = 1
        }
        
    }
    
    static func show(_ vc: UIViewController, type: Int, okCallback: callback! = nil) {
        let vcSuggest = ShopCategorySelectViewController(okCallback: okCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        vcSuggest.type = type
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        step = .c1
        btnBack.isHidden = true
        
        switch type {
        case EKind.man.rawValue:
            vwBg.backgroundColor = UIColor(hex: 0x42C2FE)
            lbName.text = "남성"
            ivImage.image = UIImage(named: "ic_shop_menlist")
            categoryList = MenCategory1
            self.nCategoryUid = 1
        case EKind.woman.rawValue:
            vwBg.backgroundColor = UIColor(hex: 0xFF3B7E)
            lbName.text = "여성"
            ivImage.image = UIImage(named: "ic_shop_womenlist")
            categoryList = WomenCategory1
            self.nCategoryUid = 2
        case EKind.kids.rawValue:
            vwBg.backgroundColor = UIColor(hex: 0x3A0D7D)
            lbName.text = "키즈"
            ivImage.image = UIImage(named: "ic_shop_kids_list")
            categoryList = KidsCategory1
            self.nCategoryUid = 4
        default:
            vwBg.backgroundColor = UIColor(hex: 0x42C2FE)
            categoryList = MenCategory1
        }
        
        tblList.reloadData()
    }
    
    @objc func onItemSelect(_ sender : UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            
            if step == .c1 {
                if indexPath.row == 0 { // 모두 보기를 클릭한 경우
                    dismiss(animated: true, completion: nil)
                    
                    if cbOk != nil {
                        category = ""
                        
                        switch type {
                        case EKind.man.rawValue:
                            category = ""
                            nCategoryUid = 1
                        case EKind.woman.rawValue:
                            category = ""
                            nCategoryUid = 2
                        case EKind.kids.rawValue:
                            category = "키즈"
                            nCategoryUid = 4
                        default:
                            category = ""
                        }
                        
                        cbOk(category, nCategoryUid)
                    }
                } else {
                    step = .c2
                    btnBack.isHidden = false
                    
                    for item in categoryList {
                        if item.val == sender.tag {
                            lbName.text = item.name
                            ivImage.image = UIImage(named: item.img)
                        }
                    }
                    category = categoryList[indexPath.row].name
                    nCategoryUid = categoryList[indexPath.row].val
                    getSubCategoryList()
                }
            } else {
                dismiss(animated: true, completion: nil)
                
                if cbOk != nil {
                    
                    let dic : CategoryDao = arrCategorys[indexPath.row]
                    switch type {
                    case EKind.man.rawValue:
                        category = (indexPath.row == 0 ? lbName.text! : (sender.withValue!["title"]) as! String)
                    case EKind.woman.rawValue:
                        category = (indexPath.row == 0 ? lbName.text! : (sender.withValue!["title"]) as! String)
                    case EKind.kids.rawValue:
                        if indexPath.row == 0 {
                            category = (indexPath.row == 0 ? lbName.text! : (sender.withValue!["title"]) as! String)
                        } else {
                            category = category + " " + (indexPath.row == 0 ? lbName.text! : (sender.withValue!["title"]) as! String)
                        }
                        
                    default:
                        category = ""
                    }
                    
                    cbOk(category, dic.categoryUid)
                }
            }
        }
        
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 하위카테고리 목록 얻기
    func getSubCategoryList() {
        
        gProgress.show()
        Net.getSubCategoryList(
            accessToken     : gMeInfo.token,
            categoryUid     : nCategoryUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSubCategoryListResult
                self.getSubCategoryListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            CommonUtil .showToast(err)
        })
    }
    
    //하위카테고리 목록 얻기 결과
    func getSubCategoryListResult(_ data: Net.getSubCategoryListResult) {

        if arrCategorys.count > 0 {
            arrCategorys.removeAll()
        }
        
        let dicinfo : CategoryDao = CategoryDao("")
        dicinfo.categoryName = "모두 보기"
        dicinfo.categoryUid = nCategoryUid
        arrCategorys.append(dicinfo)
        
        for i in 0..<data.list.count {
            let dic : CategoryDao = data.list[i]
            arrCategorys.append(dic)
        }
        
        tblList.reloadData()
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension ShopCategorySelectViewController : UITableViewDelegate {
    
}

extension ShopCategorySelectViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if step == .c1 {
            return categoryList.count
        } else {
            return arrCategorys.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! ShopCategoryListCell
        
        cell.btnCell.removeTarget(self, action: #selector(onItemSelect(_:)), for: .touchUpInside)
        
        if step == .c1 {
            cell.lbName.text = categoryList[indexPath.row].name
            cell.btnCell.tag = categoryList[indexPath.row].val
        } else {
            let dic : CategoryDao = arrCategorys[indexPath.row]
            cell.lbName.text = dic.categoryName
        }
        
        cell.btnCell.withValue = ["index" : indexPath, "title" : cell.lbName.text!]
        cell.btnCell.addTarget(self, action: #selector(onItemSelect(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    
}

