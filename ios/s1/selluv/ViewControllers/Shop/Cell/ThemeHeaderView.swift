//
//  ThemeHeaderView.swift
//  selluv
//
//  Created by Gambler on 12/17/17.
//

import UIKit

class ThemeHeaderView: UIView {

    @IBOutlet weak var ivBg: UIImageView!
    
    @IBOutlet weak var vwContent: UIView!
    
    @IBOutlet weak var lbEName: UILabel!
    
    @IBOutlet weak var lbKName: UILabel!
    
}
