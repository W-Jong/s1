//
//  ShopCategoryListCell.swift
//  selluv
//
//  Created by Gambler on 12/15/17.
//

import UIKit

class ShopCategoryListCell: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var btnCell: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
