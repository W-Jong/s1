//
//  ShopBrandGridCell.swift
//  selluv
//
//  Created by Gambler on 12/14/17.
//

import UIKit

class ShopBrandGridCell: UICollectionViewCell {

    @IBOutlet weak var ivBrandThumbnail: UIImageView!
    
    @IBOutlet weak var btnCell: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
