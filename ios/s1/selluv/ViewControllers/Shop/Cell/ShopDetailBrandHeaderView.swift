//
//  ShopDetailBrandHeaderView.swift
//  selluv
//
//  Created by Gambler on 12/16/17.
//

import UIKit

class ShopDetailBrandHeaderView: UICollectionReusableView {
    
    
    @IBOutlet weak var clvBrand: UICollectionView!
    @IBOutlet weak var lbCnt: UILabel!
    @IBOutlet weak var btnSetting: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        btnSetting.layer.borderWidth = 1
        btnSetting.layer.borderColor = UIColor(hex: 0xd3d3d3).cgColor
        btnSetting.layer.cornerRadius = 13.5
        
        let cellNib = UINib(nibName: "ShopDetailBrandGridCell", bundle: nil)
        clvBrand.register(cellNib, forCellWithReuseIdentifier: "cell")
    }
    
}

