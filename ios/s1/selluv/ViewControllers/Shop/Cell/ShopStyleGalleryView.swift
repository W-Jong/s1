//
//  ShopStyleGalleryView.swift
//  selluv
//
//  Created by Gambler on 12/15/17.
//

import UIKit

class ShopStyleGalleryView: UICollectionReusableView {

    @IBOutlet weak var scvBanner: UIScrollView!
    
    @IBOutlet weak var stvIndicator: UIStackView!
    
    @IBOutlet weak var stvIndicatorWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
