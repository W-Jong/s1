//
//  ShopDetailBrandGridCell.swift
//  selluv
//
//  Created by Gambler on 12/16/17.
//

import UIKit

class ShopDetailBrandGridCell: UICollectionViewCell {
    
    
    @IBOutlet weak var vwCell: UIView!
    @IBOutlet weak var ivBg: UIImageView!
    @IBOutlet weak var ivMask: UIImageView!
    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var lbCnt: UILabel!
    @IBOutlet weak var btnCell: UIButton!
    @IBOutlet var lbLogo: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vwCell.layer.cornerRadius = 4
    }

}
