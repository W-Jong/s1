//
//  ShopListHeaderCell.swift
//  selluv
//
//  Created by Gambler on 12/14/17.
//

import UIKit

class ShopListHeaderCell: UITableViewCell {

    @IBOutlet weak var clvBrand: UICollectionView!
    @IBOutlet weak var lbShopping: UILabel!
    
    @IBOutlet weak var ivLogo11: UIImageView!
    @IBOutlet weak var ivMask11: UIImageView!
    @IBOutlet weak var ivBar11: UIImageView!
    @IBOutlet weak var lbBrand11: UILabel!
    @IBOutlet weak var btnCell11: UIButton!
    
    @IBOutlet weak var ivLogo12: UIImageView!
    @IBOutlet weak var ivMask12: UIImageView!
    @IBOutlet weak var ivBar12: UIImageView!
    @IBOutlet weak var lbBrand12: UILabel!
    @IBOutlet weak var btnCell12: UIButton!
    
    @IBOutlet weak var ivLogo13: UIImageView!
    @IBOutlet weak var ivMask13: UIImageView!
    @IBOutlet weak var ivBar13: UIImageView!
    @IBOutlet weak var lbBrand13: UILabel!
    @IBOutlet weak var btnCell13: UIButton!
    
    @IBOutlet weak var ivLogo21: UIImageView!
    @IBOutlet weak var ivMask21: UIImageView!
    @IBOutlet weak var ivBar21: UIImageView!
    @IBOutlet weak var lbBrand21: UILabel!
    @IBOutlet weak var btnCell21: UIButton!
    
    @IBOutlet weak var vwCell22: UIView!
    @IBOutlet weak var ivLogo22: UIImageView!
    @IBOutlet weak var ivMask22: UIImageView!
    @IBOutlet weak var ivBar22: UIImageView!
    @IBOutlet weak var lbBrand22: UILabel!
    @IBOutlet weak var btnCell22: UIButton!
    
    @IBOutlet weak var ivLogo23: UIImageView!
    @IBOutlet weak var ivMask23: UIImageView!
    @IBOutlet weak var ivBar23: UIImageView!
    @IBOutlet weak var lbBrand23: UILabel!
    @IBOutlet weak var btnCell23: UIButton!
    
    @IBOutlet weak var ivLogo31: UIImageView!
    @IBOutlet weak var ivMask31: UIImageView!
    @IBOutlet weak var ivBar31: UIImageView!
    @IBOutlet weak var lbBrand31: UILabel!
    @IBOutlet weak var btnCell31: UIButton!
    
    @IBOutlet weak var ivLogo32: UIImageView!
    @IBOutlet weak var ivMask32: UIImageView!
    @IBOutlet weak var ivBar32: UIImageView!
    @IBOutlet weak var lbBrand32: UILabel!
    @IBOutlet weak var btnCell32: UIButton!
    
    @IBOutlet weak var ivLogo33: UIImageView!
    @IBOutlet weak var ivMask33: UIImageView!
    @IBOutlet weak var ivBar33: UIImageView!
    @IBOutlet weak var lbBrand33: UILabel!
    @IBOutlet weak var btnCell33: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
