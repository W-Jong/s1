//
//  ShopThemeListHeaderCell.swift
//  selluv
//
//  Created by Gambler on 12/14/17.
//

import UIKit

class ShopThemeListHeaderCell: ExpandableCell {
    
    @IBOutlet weak var ivBg: UIImageView!
    @IBOutlet weak var ivMask: UIImageView!
    
    @IBOutlet weak var lbThemeName: UILabel!
    @IBOutlet weak var lbItemCount: UILabel!
    
    @IBOutlet weak var ivClose: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        initCell()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    func initCell() {
        
        // ExpandableCell에 있는 arrow숨기기
        arrowImageView.isHidden = true
        
        btnMore.layer.borderWidth = 1
        btnMore.layer.borderColor = UIColor.init(red: 0x42 / 255.0, green: 0xC2 / 255.0, blue: 0xFE / 255.0, alpha: 1.0).cgColor
        btnMore.layer.cornerRadius = 12.5
    }
    
}
