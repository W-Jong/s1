//
//  ShopDetailCategoryGridCell.swift
//  selluv
//
//  Created by Gambler on 12/16/17.
//

import UIKit

class ShopDetailCategoryGridCell: UICollectionViewCell {

    
    @IBOutlet weak var btnCell: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        btnCell.layer.borderColor = UIColor(hex: 0xc1c1c1).cgColor
        btnCell.layer.borderWidth = 1
        btnCell.layer.cornerRadius = 13.5
    }

}
