//
//  ShopThemeListRightCell.swift
//  selluv
//
//  Created by Gambler on 12/14/17.
//

import UIKit

class ShopThemeListRightCell: UITableViewCell {
    
    @IBOutlet var vwProduct: UIView!
    @IBOutlet weak var ivProduct: UIImageView!
    
    @IBOutlet weak var lbEBrand: UILabel!
    @IBOutlet weak var lbInfo: UILabel!
    @IBOutlet weak var lbCost: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
