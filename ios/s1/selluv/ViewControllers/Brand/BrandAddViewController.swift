//
//  BrandAddViewController.swift
//  selluv
//  브렌드 추가
//  Created by World on 12/21/17.
//  modified by PJH on 12/03/18.

import UIKit

class BrandAddViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnLang: [UIButton]!
    
    @IBOutlet weak var vwLangBar: UIView!
    @IBOutlet weak var vwLangHover: UIView!
    @IBOutlet weak var vwLangHoverLeading: NSLayoutConstraint!
    @IBOutlet weak var vwLangHoverTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var vwSearchBar: UIView!
    @IBOutlet weak var tfSearchTrailing: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: DesignableUITextField!
    @IBOutlet weak var vwPlaceHolder: UIView!
    @IBOutlet weak var vwPlaceHolderXPos: NSLayoutConstraint!
    @IBOutlet weak var lbPlaceHolder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tblBrand: UITableView!
    @IBOutlet weak var btnRegister: UIButton!
    
    var arrBrandList    : Array<Brand> = []
    var arrBrandEn = [[Brand!]](repeating: [Brand](), count: index_eng.count)
    var arrBrandKo = [[Brand!]](repeating: [Brand](), count: index_kor.count)
    
    var arrBrandEn1 = [[Brand!]](repeating: [Brand](), count: index_eng.count)
    var arrBrandKo1 = [[Brand!]](repeating: [Brand](), count: index_kor.count)
    var selectedIndex : IndexPath?
    enum ELang : Int {
        case eng = 0
        case kor
    }
    
    var lang = ELang.eng {
        didSet {
            btnLang[oldValue.rawValue].isSelected = false
            btnLang[lang.rawValue].isSelected = true
            
            indexView.removeFromSuperview()
            indexView = STBTableViewIndex()
            indexView.titles = lang == .eng ? index_eng : index_kor
            //            indexView.sections = lang == .eng ? sections_eng : sections_kor
            indexView.sections = lang == .eng ? index_eng : index_kor
            indexView.delegate = self
            navigationController?.view.addSubview(indexView)
            tblBrand.reloadData()
            
            vwLangBar.removeConstraint(vwLangHoverLeading)
            vwLangBar.removeConstraint(vwLangHoverTrailing)
            
            vwLangHoverLeading = NSLayoutConstraint(item: vwLangHover, attribute: .leading, relatedBy: .equal, toItem: btnLang[lang.rawValue], attribute: .leading, multiplier: 1.0, constant: 0.0)
            vwLangHoverTrailing = NSLayoutConstraint(item: vwLangHover, attribute: .trailing, relatedBy: .equal, toItem: btnLang[lang.rawValue], attribute: .trailing, multiplier: 1.0, constant: 0.0)
            
            vwLangBar.addConstraint(vwLangHoverLeading)
            vwLangBar.addConstraint(vwLangHoverTrailing)

            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    var sections_eng = ["A", "B", "C", "D", "E", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"]
    var sections_kor = ["ㄱ", "ㄴ", "ㄷ", "ㅎ"]
    var indexView = STBTableViewIndex()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        vwLangBar.borderColor = UIColor(hex: 0xC3C3C3)
        vwLangBar.layer.borderWidth = 1
        vwLangBar.layer.cornerRadius = 13.5
        
        vwLangHover.borderColor = UIColor(hex: 0xC3C3C3)
        vwLangHover.layer.borderWidth = 1
        vwLangHover.layer.cornerRadius = 13.5
        
        btnCancel.isHidden = true
        tfSearch.text = ""
        tfSearch.layer.cornerRadius = 5.0
        
        tblBrand.delegate = self
        tblBrand.dataSource = self
        tblBrand.register(UINib(nibName: "BrandCell", bundle: nil), forCellReuseIdentifier: "BrandCell")
        
        indexView.delegate = self
        indexView.titles = index_eng
        //        indexView.sections = sections_eng
        indexView.sections = index_eng
        navigationController?.view.addSubview(indexView)
        
        tfSearch.addTarget(self, action: #selector(self.searchTextChanged), for: .editingChanged)
        
        lang = .eng
        btnLang[ELang.eng.rawValue].isSelected = true
        btnLang[ELang.kor.rawValue].isSelected = false
    
        getAllBrandDetailList()
    }
    
    func getSearchResult(key : String) {
        
        arrBrandEn1 = [[Brand!]](repeating: [], count: index_eng.count)
        arrBrandKo1 = [[Brand!]](repeating: [], count: index_kor.count)
        if key == "" {
            arrBrandEn1 = arrBrandEn
            arrBrandKo1 = arrBrandKo
        } else {
            if lang == .eng {
                
                for i in 0..<index_eng.count {
                    for k in 0..<arrBrandEn[i].count {
                        let dic : Brand = arrBrandEn[i][k]
                        if dic.nameEn.lowercased().contains(key) {
                            arrBrandEn1[i].append(dic)
                        }
                    }
                }
            } else {
                for i in 0..<index_kor.count {
                    for k in 0..<arrBrandKo[i].count {
                        let dic : Brand = arrBrandEn[i][k]
                        if dic.nameKo.contains(key) {
                            arrBrandKo1[i].append(dic)
                        }
                    }
                }
            }
        }
        tblBrand.reloadData()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    
    @IBAction func backAction(_ sender: Any) {
        indexView.removeFromSuperview()
        popVC()
    }
    
    @IBAction func placeHolderAction(_ sender: Any) {
        
        tfSearch.becomeFirstResponder()
        
    }
    
    @IBAction func langAction(_ sender: Any) {
        
        let btn = sender as! UIButton
        lang = (btn.tag == 0) ? .eng : .kor
        tblBrand.reloadData()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        tfSearch.text = ""
        tfSearch.resignFirstResponder()
        lbPlaceHolder.isHidden = tfSearch.hasText
        vwSearchBar.removeConstraint(vwPlaceHolderXPos)
        
        vwPlaceHolderXPos = NSLayoutConstraint(item: vwPlaceHolder, attribute: .centerX, relatedBy: .equal, toItem: vwSearchBar, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        tfSearchTrailing.constant = 15.0
        btnCancel.isHidden = true
        vwSearchBar.addConstraint(vwPlaceHolderXPos)
        
        view.setNeedsLayout()
        UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
        btnRegister.setTitle("확인", for: .normal)
    }
    
    @IBAction func tapAction(_ sender: Any) {
        
        if !tfSearch.hasText {
            vwSearchBar.removeConstraint(vwPlaceHolderXPos)
            
            vwPlaceHolderXPos = NSLayoutConstraint(item: vwPlaceHolder, attribute: .centerX, relatedBy: .equal, toItem: vwSearchBar, attribute: .centerX, multiplier: 1.0, constant: 0.0)
            tfSearchTrailing.constant = 15.0
            btnCancel.isHidden = true
            vwSearchBar.addConstraint(vwPlaceHolderXPos)
            
            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
        
        tfSearch.resignFirstResponder()
        
    }
    
    @IBAction func registerAction(_ sender: Any) {
        indexView.removeFromSuperview()
        popVC()
    }
    
    @objc func searchTextChanged(_ sender : UITextField) {
        
        lbPlaceHolder.isHidden = tfSearch.hasText
        
        let str : String = sender.text!
        getSearchResult(key: str)
    }
    
    @objc func onBtnItemSelect(_ sender: UIButton) {
        let index = sender.tag
        var status = false
        var _uid = 0
        if lang == .eng {
            let dic : Brand = arrBrandEn1[index / 10][index % 10]
            _uid = dic.brandUid
            status = dic.brandLikeStatus
        } else {
            let dic1 : Brand = arrBrandKo1[index / 10][index % 10]
            _uid = dic1.brandUid
            status = dic1.brandLikeStatus
        }
        
        if status {
            delOneBrandFollow(_uid: _uid)
        } else {
            reqOneBrandFollow(_uid: _uid)
        }
    }

    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 모든 브랜드 디테일 목록얻기
    func getAllBrandDetailList() {
        
        gProgress.show()
        Net.getAllBrandDetailList(
            accessToken     : gMeInfo.token,
            column          : 1,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AllBrandDetailListResult
                self.AllBrandDetailListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func AllBrandDetailListResult(_ data: Net.AllBrandDetailListResult) {
        arrBrandList = data.list
        arrBrandEn = data.list_en
        arrBrandKo = data.list_ko
        arrBrandEn1 = data.list_en
        arrBrandKo1 = data.list_ko
        tblBrand.reloadData()
    }
    
    // 브렌드 팔로우하기
    func reqOneBrandFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqOneBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getAllBrandDetailList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //브렌드 취소하기
    func delOneBrandFollow(_uid : Int) {
        
        gProgress.show()
        Net.delOneBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getAllBrandDetailList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension BrandAddViewController : UITextFieldDelegate {
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        guard let text = textField.text else { return true }
//        let newLength = text.characters.count + string.characters.count - range.length
//
//        return true
//
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if !textField.hasText {
            vwSearchBar.removeConstraint(vwPlaceHolderXPos);
            
            vwPlaceHolderXPos = NSLayoutConstraint(item: vwPlaceHolder, attribute: .leading, relatedBy: .equal, toItem: vwSearchBar, attribute: .leading, multiplier: 1.0, constant: 27.0)
            tfSearchTrailing.constant = btnCancel.frame.size.width
            btnCancel.isHidden = false
            vwSearchBar.addConstraint(vwPlaceHolderXPos)
            
            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension BrandAddViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //        return lang == .eng ? sections_eng.count : sections_kor.count
        return lang == .eng ? index_eng.count : index_kor.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //        return String(format:"%@", lang == .eng ? sections_eng[section] : sections_kor[section])
        return String(format:"%@", lang == .eng ? index_eng[section] : index_kor[section])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lang == .eng {
            return arrBrandEn1[section].count
        } else {
            return arrBrandKo1[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandCell", for: indexPath) as! BrandCell
        if lang == .eng {
            let dic : Brand = arrBrandEn1[indexPath.section][indexPath.row]
            cell._lbBrandFirstName.text = dic.nameEn
            cell._lbBrandSecondName.text = dic.nameKo
            
            if !dic.brandLikeStatus {
                cell._imvSelectOn.image = UIImage.init(named: "ic_brand_select_off")
            } else {
                cell._imvSelectOn.image = UIImage.init(named: "ic_brand_select_on")
            }
        } else {
            let dic1 : Brand = arrBrandKo1[indexPath.section][indexPath.row]
            cell._lbBrandFirstName.text = dic1.nameKo
            cell._lbBrandSecondName.text = dic1.nameEn

            if !dic1.brandLikeStatus {
                cell._imvSelectOn.image = UIImage.init(named: "ic_brand_select_off")
            } else {
                cell._imvSelectOn.image = UIImage.init(named: "ic_brand_select_on")
            }
        }
        cell._imvSelectOn.isHidden = false
        cell._btnSelect.tag = indexPath.section * 10 + indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnItemSelect(_:)), for: UIControlEvents.touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//extension BrandAddViewController: BrandCellDelegate {
//    func BrandCellDelegate(_ brandCell: BrandCell, onBrandSelected sender: Any, tag: Int?) {
//        brandCell._imvSelectOn.isHidden = !brandCell._imvSelectOn.isHidden
//
//        if brandCell._imvSelectOn.isHidden {
//            selectedIndex = nil
//        } else {
//            selectedIndex = brandCell.getIndexPath()
//        }
//
//        tfSearch.resignFirstResponder()
//    }
//}

extension BrandAddViewController : STBTableViewIndexDelegate {
    
    func tableViewIndexChanged(_ index: Int, title: String) {
        let group = lang == .eng ? index_eng : index_kor
        let section = group.index(of: title) ?? 0
        
        var point = tblBrand.rect(forSection: section).origin
        point.y = min(point.y, tblBrand.contentSize.height - tblBrand.bounds.height)
        tblBrand.setContentOffset(point, animated: true)
    }
    
    func tableViewIndexTopLayoutGuideLength() -> CGFloat {
        return 135
    }
    
    func tableViewIndexBottomLayoutGuideLength() -> CGFloat {
        return 80
    }
    
}
