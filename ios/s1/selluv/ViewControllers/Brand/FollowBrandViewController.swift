//
//  FollowBrandViewController.swift
//  selluv
//  브렌드 편집
//  Created by Dev on 12/10/17.
//  modified by PJH on 12/03/18.

import UIKit

class FollowBrandViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    @IBOutlet weak var _btnOption: UIButton!
    
    var m_bOption = false
    var arrFollowBrandList  : Array<Brand> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getFollowBrandList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "FollowBrandTVC", bundle: nil), forCellReuseIdentifier: "FollowBrandTVC")
        
        _tvList.allowsSelection = false
        _tvList.reorder.delegate = self
        
        _tvList.reloadData()
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickAdd(_ sender: Any) {
        pushVC("BRAND_ADD_VIEW", storyboard: "Brand", animated: true)
    }
    
    @IBAction func onClickOption(_ sender: Any) {
        if _btnOption.isSelected {
            _btnOption.isSelected = false
            m_bOption = false
        } else {
            _btnOption.isSelected = true
            m_bOption = true
        }
        
        _tvList.reloadData()
    }
    
    @objc func onBtnGo2Brand( _ sendor: UIButton) {
        
        let dic : Brand = arrFollowBrandList[sendor.tag]
        
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onDelList( _ sendor: UIButton) {
        let dic : Brand = arrFollowBrandList[sendor.tag]
        delOneBrandFollow(_uid: dic.brandUid)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFollowBrandList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowBrandTVC", for: indexPath as IndexPath) as! FollowBrandTVC
        
        let dic : Brand = arrFollowBrandList[indexPath.row]
        cell.lbBrandEn.text = dic.nameEn
        cell.LBBrandKo.text = dic.nameKo
        
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(onBtnGo2Brand(_:)), for: UIControlEvents.touchUpInside)
        
        cell._btnDel.tag = indexPath.row
        cell._btnDel.addTarget(self, action:#selector(onDelList(_:)), for: UIControlEvents.touchUpInside)
        
        if m_bOption {
            cell.selectUI()
        } else {
            cell.initUI()
        }
        
        return cell
    }
    
    //MARK: - UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 팔로우 브렌드 목록 얻기
    func getFollowBrandList() {
        
        gProgress.show()
        Net.getFollowBrandList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.BrandLikeResult
                self.getFollowBrandResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //팔로우 브렌드 목록 얻기 결과
    func getFollowBrandResult(_ data: Net.BrandLikeResult) {
        
        arrFollowBrandList = data.list
        _tvList.reloadData()
    }
 
    // 브렌드 팔로우하기
    func reqOneBrandFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqOneBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getFollowBrandList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //브렌드 취소하기
    func delOneBrandFollow(_uid : Int) {
        
        gProgress.show()
        Net.delOneBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getFollowBrandList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension FollowBrandViewController: TableViewReorderDelegate {
    
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if self._btnOption.isSelected {
             self._tvList.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canReorderRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row != 6
    }
}

