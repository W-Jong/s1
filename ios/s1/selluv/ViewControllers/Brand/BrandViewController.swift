//
//  BrandViewController.swift
//  selluv
//
//  Created by Gambler on 12/12/17.
//  modified by PJH on 11/03/18.

import UIKit
import DGElasticPullToRefresh

class BrandViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, STBTableViewIndexDelegate {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var vwLineBar1: UIView!
    @IBOutlet weak var vwLineBar2: UIView!
    @IBOutlet weak var vwLineBar3: UIView!
    
    @IBOutlet var btnCom: [UIButton]!
    
    @IBOutlet weak var vwComBar: UIView!
    @IBOutlet weak var vwComHover: UIView!
    @IBOutlet weak var vwComHoverLeading: NSLayoutConstraint!
    @IBOutlet weak var vwComHoverTrailing: NSLayoutConstraint!
    
    @IBOutlet var btnLang: [UIButton]!
    
    @IBOutlet weak var vwLangBar: UIView!
    @IBOutlet weak var vwLangHover: UIView!
    @IBOutlet weak var vwLangHoverLeading: NSLayoutConstraint!
    @IBOutlet weak var vwLangHoverTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var scvView: UIScrollView!
    
    @IBOutlet weak var vwFollowNoData: UIView!
    @IBOutlet weak var tvT1: UITableView!
    @IBOutlet weak var tvT2: UITableView!
    @IBOutlet weak var tvT3: UITableView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnMenuT1: UIButton!
    @IBOutlet weak var btnMenuT2: UIButton!
    @IBOutlet weak var btnMenuT3: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    @IBOutlet weak var lbT2ComTitle: UILabel!
    @IBOutlet weak var lbT3BrandTitle: UILabel!
    
    @IBOutlet weak var vwMenu: UIView!
    @IBOutlet weak var vwMenuBottom: NSLayoutConstraint!
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    
    //zoom효과관련
    weak var selectedImageView : UIImageView?
    
    var animator : ARNTransitionAnimator?
    
    var arrFollowBrandList  : Array<Brand> = []
    
    enum ETab : Int {
        case T1 = 0
        case T2
        case T3
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    enum ELang : Int {
        case eng = 0
        case kor
    }
    
    enum EGender : Int {
        case men = 0
        case women
        case kids
    }
    
    var com = EGender.men {
        didSet {
            btnCom[oldValue.rawValue].isSelected = false
            btnCom[com.rawValue].isSelected = true
            
            vwComBar.removeConstraint(vwComHoverLeading)
            vwComBar.removeConstraint(vwComHoverTrailing)
            
            vwComHoverLeading = NSLayoutConstraint(item: vwComHover, attribute: .leading, relatedBy: .equal, toItem: btnCom[com.rawValue], attribute: .leading, multiplier: 1.0, constant: 0.0)
            vwComHoverTrailing = NSLayoutConstraint(item: vwComHover, attribute: .trailing, relatedBy: .equal, toItem: btnCom[com.rawValue], attribute: .trailing, multiplier: 1.0, constant: 0.0)
            
            vwComBar.addConstraint(vwComHoverLeading)
            vwComBar.addConstraint(vwComHoverTrailing)
            
            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    var lang = ELang.eng {
        didSet {
            btnLang[oldValue.rawValue].isSelected = false
            btnLang[lang.rawValue].isSelected = true
            
            indexView.removeFromSuperview()
            indexView = STBTableViewIndex()
            indexView.titles = lang == .eng ? index_eng : index_kor
            indexView.sections = lang == .eng ? sections_eng : sections_kor
            indexView.delegate = self
            navigationController?.view.addSubview(indexView)
            tvT3.reloadData()
            
            vwLangBar.removeConstraint(vwLangHoverLeading)
            vwLangBar.removeConstraint(vwLangHoverTrailing)
            
            vwLangHoverLeading = NSLayoutConstraint(item: vwLangHover, attribute: .leading, relatedBy: .equal, toItem: btnLang[lang.rawValue], attribute: .leading, multiplier: 1.0, constant: 0.0)
            vwLangHoverTrailing = NSLayoutConstraint(item: vwLangHover, attribute: .trailing, relatedBy: .equal, toItem: btnLang[lang.rawValue], attribute: .trailing, multiplier: 1.0, constant: 0.0)
            
            vwLangBar.addConstraint(vwLangHoverLeading)
            vwLangBar.addConstraint(vwLangHoverTrailing)
            
            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    var sections_eng = ["A", "B", "C", "D", "E", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"]
    var index_eng = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"]
    
    var sections_kor = ["ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ", "ㅂ", "ㅅ", "ㅇ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"]
    var index_kor = ["ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ", "ㅂ", "ㅅ", "ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"]
    var indexView = STBTableViewIndex()
    
    var loaded = false
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
        
        if gMeInfo.gender == 1 {
            com = EGender.men
            self.lbT2ComTitle.text = "남성 브랜드 순위"
        } else {
            com = EGender.women
            self.lbT2ComTitle.text = "여성 브랜드 순위"
        }
        
        addPullToRefresh()
        getPdtLikeUpdateYn()
    }
    
    deinit {
        tvT1.dg_removePullToRefresh()
        tvT2.dg_removePullToRefresh()
        tvT3.dg_removePullToRefresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if btnMenuT3.isSelected {
            navigationController?.view.addSubview(indexView)
            tvT3.reloadData()
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        
        if(loaded == true) {
            getDataList()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        vwLangBar.borderColor = UIColor(hex: 0xC3C3C3)
        vwLangBar.layer.borderWidth = 1
        vwLangBar.layer.cornerRadius = 13.5
        
        vwLangHover.borderColor = UIColor(hex: 0xC3C3C3)
        vwLangHover.layer.borderWidth = 1
        vwLangHover.layer.cornerRadius = 13.5
        
        vwComBar.borderColor = UIColor(hex: 0xC3C3C3)
        vwComBar.layer.borderWidth = 1
        vwComBar.layer.cornerRadius = 13.5
        
        vwComHover.borderColor = UIColor(hex: 0xC3C3C3)
        vwComHover.layer.borderWidth = 1
        vwComHover.layer.cornerRadius = 13.5
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        // Collection view attributes
        
        tvT1.delegate = self
        tvT1.dataSource = self
        tvT1.register(UINib.init(nibName: "T1TableViewCell", bundle: nil), forCellReuseIdentifier: "T1TableViewCell")
        
        tvT2.delegate = self
        tvT2.dataSource = self
        tvT2.register(UINib.init(nibName: "T2TableViewCell", bundle: nil), forCellReuseIdentifier: "T2TableViewCell")
        
        tvT3.delegate = self
        tvT3.dataSource = self
        tvT3.register(UINib.init(nibName: "T3TableViewCell", bundle: nil), forCellReuseIdentifier: "T3TableViewCell")
        
        indexView.delegate = self
        indexView.titles = index_eng
        indexView.sections = sections_eng
        
        vwFollowNoData.isHidden = true

    }
    
   func addPullToRefresh() {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        tvT1.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.getDataList()
            })
            }, loadingView: loadingView)
        tvT1.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        tvT1.dg_setPullToRefreshBackgroundColor(tvT1.backgroundColor!)
        
        let loadingView1 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView1.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        tvT2.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.getDataList()
            })
            }, loadingView: loadingView1)
        tvT2.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        tvT2.dg_setPullToRefreshBackgroundColor(tvT2.backgroundColor!)
        
        let loadingView2 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView2.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        tvT3.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.getDataList()
            })
            }, loadingView: loadingView2)
        tvT3.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        tvT3.dg_setPullToRefreshBackgroundColor(tvT3.backgroundColor!)
    }
    
    func tableViewIndexChanged(_ index: Int, title: String) {
        
        //var sections = lang == .eng ? sections_eng : sections_kor
        
//        for i in 0 ..< sections.count {
//            if sections[i] == title {
//                let indexPath = IndexPath(row: 0, section: i)
//                tvT3.scrollToRow(at: indexPath, at: .top, animated: true)
//                return
//            }
//        }
        
        for i in 0 ..< arrFollowBrandList.count {
            let dic : Brand = arrFollowBrandList[i]
            var str = ""
            if lang == .eng {
                str = dic.firstEn
            } else {
                str = dic.firstKo
            }
            if str == title {
                let indexPath = IndexPath(row: i, section: 0)
                tvT3.scrollToRow(at: indexPath, at: .top, animated: true)
                return
            }
        }
    }
    
    func tableViewIndexTopLayoutGuideLength() -> CGFloat {
        return 140
    }
    
    func tableViewIndexBottomLayoutGuideLength() -> CGFloat {
        return 80
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuT1.isSelected = false
        btnMenuT2.isSelected = false
        btnMenuT3.isSelected = false
        
        switch gBrandPageIndex {
        case ETab.T1.rawValue :
            indexView.removeFromSuperview()
            btnMenuT1.isSelected = true
//            tvT1.reloadData()
            clickedBtn = btnMenuT1
            vwLineBar1.isHidden = false
            vwLineBar2.isHidden = true
            vwLineBar3.isHidden = true
        case ETab.T2.rawValue :
            indexView.removeFromSuperview()
            btnMenuT2.isSelected = true
//            tvT2.reloadData()
            clickedBtn = btnMenuT2
            vwLineBar1.isHidden = true
            vwLineBar2.isHidden = false
            vwLineBar3.isHidden = true
        case ETab.T3.rawValue :
            // indexView init
            
            navigationController?.view.addSubview(indexView)
            
            btnMenuT3.isSelected = true
//            tvT3.reloadData()
            clickedBtn = btnMenuT3
            vwLineBar1.isHidden = true
            vwLineBar2.isHidden = true
            vwLineBar3.isHidden = false
        default:
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        scrollUp()
        getDataList()
    }
    
    func showInteractive(brandUid : Int) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func goBrandDetailPage(brandUid : Int) {
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func scrollUp() {
        
        if scrollDirection == .up {
            return
        }
        
        if btnMenuT1.isSelected || btnMenuT2.isSelected || btnMenuT3.isSelected {
            UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
                self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            })
        }
    }
    
    func scrollDown() {
        
        if scrollDirection == .down {
            return
        }
        
        if btnMenuT1.isSelected || btnMenuT2.isSelected {
            UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
                self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwTop.frame.height))
                self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 3 * self.vwMenu.frame.height))
                self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -130))
            })
        }
        
    }
    
    func changePage() {
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch gBrandPageIndex {
        case ETab.T1.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 0
        case ETab.T2.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 1
        case ETab.T3.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 2
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved()

    }
    
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnGo2Brand( _ sendor: UIButton) {
        
        let dic : Brand = arrFollowBrandList[sendor.tag]
        goBrandDetailPage(brandUid: dic.brandUid)
    }
    
    @IBAction func langAction(_ sender: Any) {
        
        let btn = sender as! UIButton
        lbT3BrandTitle.text = (btn.tag == 0) ? "브랜드 영문 정렬" : "브랜드 한글 정렬"
        lang = (btn.tag == 0) ? .eng : .kor
        getDataList()
    }
    
    @IBAction func comAction(_ sender: Any) {
        
        let btn = sender as! UIButton
        if btn.tag == 0 {
            com = .men
            self.lbT2ComTitle.text = "남성 브랜드 순위"
        } else if btn.tag == 1 {
            com = .women
            self.lbT2ComTitle.text = "여성 브랜드 순위"
        } else if btn.tag == 2 {
            com = .kids
            self.lbT2ComTitle.text = "키즈 브랜드 순위"
        }
        getDataList()
    }
    
    @IBAction func brandAddAction(_ sender: Any) {
        indexView.removeFromSuperview()
        pushVC("BRAND_ADD_VIEW", storyboard: "Brand", animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        indexView.removeFromSuperview()
        pushVC("SEARCH_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        indexView.removeFromSuperview()
        self.pushVC("LIKE_VIEW", storyboard: "Top", animated: true)      
    }
    
    @IBAction func addBrandAction(_ sender: Any) {
        indexView.removeFromSuperview()
        pushVC("BRAND_ADD_VIEW", storyboard: "Brand", animated: true)
    }
    
    @IBAction func editBrandAction(_ sender: Any) {
        indexView.removeFromSuperview()
        pushVC("FOLLOW_BRAND_VIEW", storyboard: "Brand", animated: true)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        let clickedBtn = sender as! UIButton
        
        indexView.removeFromSuperview()
        
        self.replaceTab(clickedBtn.tag)
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        gBrandPageIndex = clickedBtn.tag
        changePage()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuT1.isSelected {
            tvT1.scrollToTop(isAnimated: true)
        } else if btnMenuT2.isSelected {
            tvT2.scrollToTop(isAnimated: true)
        }
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
      
        let dic : Brand = arrFollowBrandList[sender.tag]
        
        let _uid : Int = dic.brandUid
        if dic.brandLikeStatus {
            delBrandFollow(brandUid: _uid)
        } else {
            setBrandFollow(brandUid: _uid)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if btnMenuT2.isSelected {
            return 1
        } else if btnMenuT1.isSelected{
            return 1
        } else {
            //return lang == .eng ? sections_eng.count : sections_kor.count
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if btnMenuT1.isSelected {
            return arrFollowBrandList.count
        } else if btnMenuT2.isSelected{
            return arrFollowBrandList.count
        } else {
            return arrFollowBrandList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tvT2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "T2TableViewCell", for: indexPath as IndexPath) as! T2TableViewCell
            
            let dic : Brand = arrFollowBrandList[indexPath.row]
            
            cell.delegate = self
            cell.lbRank.text = String(indexPath.row + 1)
            cell.lbBrandEn.text = dic.nameEn
            cell.lbBrandKo.text = dic.nameKo
            cell.ivBrandBG.kf.setImage(with: URL(string: dic.backImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell.ivBrandLogo.kf.setImage(with: URL(string: dic.logoImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell.btnFollow.setTitle(String(dic.brandLikeCount),for: .normal)
            
            if dic.brandLikeStatus {
                cell.btnFollow.setImage(UIImage.init(named: "ic_like_heart_big"), for: .normal)
            } else {
                cell.btnFollow.setImage(UIImage.init(named: "ic_dislike_heart_big"), for: .normal)
            }
            
            cell.setPdtData(index: indexPath.row, arr: dic.pdtList)
            
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action:#selector(self.onBtnLike(_:)), for: .touchUpInside)

            
            return cell
        } else if tableView == tvT3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "T3TableViewCell", for: indexPath as IndexPath) as! T3TableViewCell
            
            let dic : Brand = arrFollowBrandList[indexPath.row]

            cell.ivThumb.kf.setImage(with: URL(string: dic.backImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

            if dic.brandLikeStatus {
                cell.btnFollow.setImage(UIImage.init(named: "ic_like_heart_big"), for: .normal)
            } else {
                cell.btnFollow.setImage(UIImage.init(named: "ic_dislike_heart_big"), for: .normal)
            }
            
            if dic.logoImg == nil || lang == ELang.kor {
                cell.lbTitle.isHidden = false
                cell.ivLogo.isHidden = true
                cell.lbTitle.text = lang == .eng ? dic.nameEn : dic.nameKo
            } else {
                cell.lbTitle.isHidden = true
                cell.ivLogo.isHidden = false
                 cell.ivLogo.kf.setImage(with: URL(string: dic.logoImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            }

            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action:#selector(self.onBtnLike(_:)), for: .touchUpInside)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "T1TableViewCell", for: indexPath as IndexPath) as! T1TableViewCell
        
        cell.delegate = self
    
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnGo2Brand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrandForward.tag = indexPath.row
        cell._btnBrandForward.addTarget(self, action:#selector(self.onBtnGo2Brand(_:)), for: UIControlEvents.touchUpInside)
    
        let dic : Brand = arrFollowBrandList[indexPath.row]
    
        cell._btnBrand.setTitle(dic.nameEn, for: .normal)
        if dic.pdtList.count == 0 {
            cell._vwEmpty.isHidden = false
            cell._cvMemberList.isHidden = true
        } else {
            cell._cvMemberList.isHidden = false
            cell._vwEmpty.isHidden = true
            cell.setPdtData(index: indexPath.row, arr: dic.pdtList)
        }
    
        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var nLength = 0
        
        if tableView == tvT1 {
            nLength = 220
        } else if tableView == tvT2 {
            nLength = 340
        } else if tableView == tvT3 {
            nLength = 160
        }
        
        return CGFloat(nLength)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tvT3 {
            return 0.1
        } else {
            return 148
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 좋아요 업데이트뱃지 표시여부
    func getPdtLikeUpdateYn() {
        
        gProgress.show()
        Net.getLikeUpdateStatus(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.PdtLikeUpdateStatusResult
                if res.status {
                    self.btnRight.setImage(UIImage(named: "ic_thema_heart"), for: .normal)
                } else {
                    self.btnRight.setImage(UIImage(named: "ic_thema_heart_off"), for: .normal)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.getDataList()
                    self.loaded = true
                    self.changePage()

                })
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getDataList(){
        
        if gBrandPageIndex == ETab.T1.rawValue {
            arrFollowBrandList.removeAll()
            getFollowBrandList()
        } else if gBrandPageIndex == ETab.T2.rawValue {
            arrFollowBrandList.removeAll()
            getFameBrandList()
        } else {
            arrFollowBrandList.removeAll()
            getAllBrandList()
        }
    }
    
    // 팔로우 브렌드 목록 얻기
    func getFollowBrandList() {
        
        gProgress.show()
        Net.getFollowBrandList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                self.tvT1.dg_stopLoading()
                self.scrollUp()
                let res = result as! Net.BrandLikeResult
                self.getFollowBrandResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //팔로우 브렌드 목록 얻기 결과
    func getFollowBrandResult(_ data: Net.BrandLikeResult) {
        
        arrFollowBrandList = data.list
        
        if arrFollowBrandList.count == 0 {
            vwFollowNoData.isHidden = false
        } else {
            vwFollowNoData.isHidden = true
        }
        tvT1.reloadData()
    }
    
    // 모든 브렌드 detail 목록 얻기
    func getAllBrandList() {

        gProgress.show()
        Net.getAllBrandDetailList(
            accessToken     : gMeInfo.token,
            column          : lang == .eng ? 1 : 2,
            success: { (result) -> Void in
                gProgress.hide()
                self.tvT3.dg_stopLoading()
                self.scrollUp()
                let res = result as! Net.AllBrandDetailListResult
                self.getAllBrandResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //모든 브렌드 detail  목록 얻기 결과
    func getAllBrandResult(_ data: Net.AllBrandDetailListResult) {
        
        arrFollowBrandList = data.list
        tvT3.reloadData()
    }
    
    // 인기 브렌드 목록 얻기(10개)
    func getFameBrandList() {
        
        var pdtGroupType : String = ""
        if com == .men {
            pdtGroupType = "MALE"
        } else if com == .women {
            pdtGroupType = "FEMALE"
        } else {
            pdtGroupType = "KIDS"
        }

        gProgress.show()
        Net.getFameBrandList(
            accessToken     : gMeInfo.token,
            pdtGroupType    : pdtGroupType,
            success: { (result) -> Void in
                gProgress.hide()
                self.tvT2.dg_stopLoading()
                self.scrollUp()
                let res = result as! Net.FameBrandResult
                self.getFameBrandResult(res)

        }, failure: { (code, err) -> Void in
            gProgress.hide()

            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }

    //인기 브렌드 목록 얻기(10개) 결과
    func getFameBrandResult(_ data: Net.FameBrandResult) {

        arrFollowBrandList = data.list
        tvT2.reloadData()
    }
    
    // 브렌드 팔로우 하기
    func setBrandFollow(brandUid : Int) {
        
        gProgress.show()
        Net.setBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : brandUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("brand_follow_set", comment:""))
                self.getDataList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 브렌드 팔로우 취소하기
    func delBrandFollow(brandUid : Int) {
        
        gProgress.show()
        Net.delBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : brandUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("brand_follow_cancel", comment:""))
                self.getDataList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension BrandViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            gBrandPageIndex = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            self.pageMoved()
            
        case tvT1, tvT2, tvT3:

            self.scrollDirection = EScrollDirection.none

            //                let curScrollPos = scrollView.contentOffset.y
            //                if curScrollPos == 0 {
            //                    // scroll Down
            //                    self.scrollUp()
            //                } else if curScrollPos >= abs(scrollView.contentSize.height - scrollView.bounds.size.height)  {
            //                    // scroll Up
            //                    self.scrollDown()
            //                }
            //
            //                scrollPos = curScrollPos

            //                return

        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case tvT1, tvT2, tvT3:

            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }

            scrollPos = curScrollPos

        default:
            return
        }
    }
}

extension BrandViewController: T1CellDelegate {
    func T1CellDelegate(_ t1TableViewCell : T1TableViewCell, tag: Int?, parent : Int?) {
        self.selectedImageView = t1TableViewCell._imgCom
        
        let dic : Brand = arrFollowBrandList[parent!]
        let dicPdt : PdtInfoDto = dic.pdtList[tag!]
        self.showInteractive(brandUid: dicPdt.pdtUid)
    }
}

extension BrandViewController: T2CellDelegate {
    func T2CellDelegate(_ t2TableViewCell : T2TableViewCell, tag: Int?, parent : Int?) {
        self.selectedImageView = t2TableViewCell._imgCom
        
        let dic : Brand = arrFollowBrandList[parent!]
        let dicPdt : PdtInfoDto = dic.pdtList[tag!]
        self.showInteractive(brandUid: dicPdt.pdtUid)
    }
}

extension BrandViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return selectedImageView ?? UIImageView()
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        guard let selectedImageView = selectedImageView else { return CGRect.zero }
        return selectedImageView.convert(selectedImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        selectedImageView?.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        selectedImageView?.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        selectedImageView?.isHidden = false
    }
}

