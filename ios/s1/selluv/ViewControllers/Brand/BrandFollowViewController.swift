//
//  BrandFollowViewController.swift
//  selluv
//  브렌드 더보기
//  Created by Gambler on 12/12/17.
//  modified by PJH on 12/03/18.

import UIKit

class BrandFollowViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var scvView: UIScrollView!
    @IBOutlet weak var clvT1: UICollectionView!
    @IBOutlet weak var clvT2: UICollectionView!
    @IBOutlet weak var clvT3: UICollectionView!
    @IBOutlet weak var clvT4: UICollectionView!
    @IBOutlet weak var clvT5: UICollectionView!
    
    @IBOutlet weak var vwLine: UIView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnMenuT1: UIButton!
    @IBOutlet weak var btnMenuT2: UIButton!
    @IBOutlet weak var btnMenuT3: UIButton!
    @IBOutlet weak var btnMenuT4: UIButton!
    @IBOutlet weak var btnMenuT5: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var btnFindSettings: UIButton!
    @IBOutlet weak var lbComTitle: UILabel!
    
    @IBOutlet weak var lcTopTop: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    @IBOutlet var lbPdtCnt: UILabel!
    @IBOutlet var imvBackBG: UIImageView!
    @IBOutlet var lbBrandEn: UILabel!
    @IBOutlet var lbBrandLogo: UIImageView!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    var pStart : CGPoint?;
    var arrClvT : [UICollectionView] = []
    
    let MAXIMUM_TOP_HEIGHT : CGFloat = 300.0
    // 고정부분 높이
    let FIX_HEIGHT : CGFloat = (114.0 - 55 + 15)
    
    var branddLincese : String!
    var followStatus  : Bool!
    var barndUid      : Int!
    var nPage       : Int = 0
    var isLast      : Bool!
    var arrPdtList : Array<PdtListDto> = []
    var nCurPage   : Int = ETab.all.rawValue
    
//    enum ETab : Int {
//        case all = 0
//        case men
//        case women
//        case kids
//        case style
//    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
        getPdtLikeUpdateYn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        btnFindSettings.isSelected = false
//        if nCurPage != ETab.all.rawValue {
//
//            switch nCurPage {
//            case ETab.men.rawValue :
//                scvView.contentOffset.x = scvView.frame.size.width * 1
//            case ETab.women.rawValue :
//                scvView.contentOffset.x = scvView.frame.size.width * 2
//            case ETab.kids.rawValue :
//                scvView.contentOffset.x = scvView.frame.size.width * 3
//            default:
//                scvView.contentOffset.x = scvView.frame.size.width * 0
//            }
//
//            pageMoved()
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        arrClvT = [clvT1, clvT2, clvT3, clvT4, clvT5]
        
        // Collection view attributes
        clvT1.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        clvT1.alwaysBounceVertical = true
        
        for i in 0..<arrClvT.count {
            // Add the waterfall layout to your collection view
            arrClvT[i].collectionViewLayout = layout
            
            let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
            arrClvT[i].register(viewNib, forCellWithReuseIdentifier: "cell")
            
            arrClvT[i].isUserInteractionEnabled = false;
        }
        
        self.vwTop.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        self.btnScroll2Top.frame = CGRect.init(x: self.view.frame.size.width - self.btnScroll2Top.frame.size.width - 15, y: self.view.frame.size.height - 65, width: self.btnScroll2Top.frame.size.width, height: self.btnScroll2Top.frame.size.height)
        
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuT1.isSelected = false
        btnMenuT2.isSelected = false
        btnMenuT3.isSelected = false
        btnMenuT4.isSelected = false
        btnMenuT5.isSelected = false
        
        switch nCurPage {
        case ETab.all.rawValue :
            btnMenuT1.isSelected = true
            lbComTitle.text = btnMenuT1.title(for: .normal)
            clickedBtn = btnMenuT1
        case ETab.men.rawValue :
            btnMenuT2.isSelected = true
            lbComTitle.text = btnMenuT2.title(for: .normal)
            clickedBtn = btnMenuT2
        case ETab.women.rawValue :
            btnMenuT3.isSelected = true
            lbComTitle.text = btnMenuT3.title(for: .normal)
            clickedBtn = btnMenuT3
        case ETab.kids.rawValue :
            btnMenuT4.isSelected = true
            lbComTitle.text = btnMenuT4.title(for: .normal)
            clickedBtn = btnMenuT4
        case ETab.style.rawValue :
            btnMenuT5.isSelected = true
            lbComTitle.text = btnMenuT5.title(for: .normal)
            clickedBtn = btnMenuT5
        default:
            btnMenuT1.isSelected = true
            lbComTitle.text = btnMenuT1.title(for: .normal)
            clickedBtn = btnMenuT1
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        nPage = 0
//        initTabBar()
        getBrandDetailPdtList()
    }
    
    func initTabBar() {
        btnMenuT1.isHidden = true
        btnMenuT2.isHidden = true
        btnMenuT3.isHidden = true
        btnMenuT4.isHidden = true
        btnMenuT5.isHidden = true
    }
    
    func changePage()  {
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch nCurPage {
        case ETab.all.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 0
        case ETab.men.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 1
        case ETab.women.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 2
        case ETab.kids.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 3
        case ETab.style.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 4
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved()
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(toSearch), name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil)
    }
    
    @objc func toSearch(_ notification: NSNotification) {
        removeObserver()
        nPage = 0
        getBrandDetailPdtList()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func searchAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func linceseAction(_ sender: Any) {
        CommonUtil.showToast(branddLincese)
    }
    
    @IBAction func followAction(_ sender: Any) {
        
        if followStatus {
            delBrandFollow(brandUid: barndUid)
        } else {
            setBrandFollow(brandUid: barndUid)
        }
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        self.pushVC("LIKE_VIEW", storyboard: "Top", animated: true)      
    }
    
    @IBAction func findAction(_ sender: Any) {
        btnFindSettings.isSelected = true
        addObserver()
        self.pushVC("FILTER_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func menuAction(_ sender: Any) {
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        nCurPage = clickedBtn.tag
        changePage()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuT1.isSelected {
            clvT1.scrollToTop(isAnimated: true)
        } else if btnMenuT2.isSelected {
            clvT2.scrollToTop(isAnimated: true)
        } else if btnMenuT3.isSelected {
            clvT3.scrollToTop(isAnimated: true)
        } else if btnMenuT4.isSelected {
            clvT4.scrollToTop(isAnimated: true)
        } else if btnMenuT5.isSelected {
            clvT5.scrollToTop(isAnimated: true)
        }
    }
    
    @IBAction func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let clv : UICollectionView!
        if btnMenuT1.isSelected {
            clv = arrClvT[0]
        } else if btnMenuT2.isSelected {
            clv = arrClvT[1]
        } else if btnMenuT3.isSelected {
            clv = arrClvT[2]
        } else if btnMenuT4.isSelected {
            clv = arrClvT[3]
        } else {
            clv = arrClvT[4]
        }

        let touchPoint : CGPoint = recognizer.location(in: self.view)
        if (recognizer.state == UIGestureRecognizerState.began) {
            pStart = touchPoint;
        } else {
            // 상단부분 높이
            let TOP_HEIGHT : CGFloat = MAXIMUM_TOP_HEIGHT
            // 터치시 상단 리미트 값
            let TOUCH_LIMIT_Y : CGFloat = -(TOP_HEIGHT - FIX_HEIGHT);
            
            let delta : CGFloat = touchPoint.y - pStart!.y
            let nowH : CGFloat = lcTopTop.constant;
            
            if (nowH + delta <= TOUCH_LIMIT_Y) {
                lcTopTop.constant = TOUCH_LIMIT_Y;
                vwTopTop.alpha = 0
                lbTitle.alpha = 1
                clv.isUserInteractionEnabled = true
                NSLog("current pos y = %f", nowH + delta)
            } else if (nowH + delta > 0) {
                lcTopTop.constant = 0.0
                vwTopTop.alpha = 1
                lbTitle.alpha = 0
                NSLog("nowH + delta > 0")
            } else {
                lcTopTop.constant = nowH + delta;
                vwTopTop.alpha = 1-(lcTopTop.constant / TOUCH_LIMIT_Y)
                clv.isUserInteractionEnabled = false
                NSLog("else pos y = %f", nowH + delta)
                lbTitle.alpha = lcTopTop.constant / TOUCH_LIMIT_Y
            }
            pStart = touchPoint;
        }
    }
    
    @objc func onBtnStyle(_ sender: UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, pdtUid: dicPdt.pdtUid)
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            if nCurPage == ETab.all.rawValue {
                cell = clvT1.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT1.contentOffset.y
            } else if nCurPage == ETab.men.rawValue {
                cell = clvT2.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT2.contentOffset.y
            } else if nCurPage == ETab.women.rawValue {
                cell = clvT3.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT3.contentOffset.y
            } else if nCurPage == ETab.kids.rawValue {
                cell = clvT4.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT4.contentOffset.y
            } else {
                cell = clvT5.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT5.contentOffset.y
            }
            
            let dic : PdtListDto = arrPdtList[indexPath.row]
            let dicPdt          : PdtMiniDto         = dic.pdt  //상품
            
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
        }
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @objc func onBtnProduct(_ sender : UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dic.pdt.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 좋아요 업데이트뱃지 표시여부
    func getPdtLikeUpdateYn() {
        
        gProgress.show()
        Net.getLikeUpdateStatus(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.PdtLikeUpdateStatusResult
                if res.status {
                    self.btnRight.setImage(UIImage(named: "ic_thema_heart"), for: .normal)
                } else {
                    self.btnRight.setImage(UIImage(named: "ic_thema_heart_off"), for: .normal)
                }
                
                self.getBrandDetailInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 브렌드 상세보기
    func getBrandDetailInfo() {
        
        gProgress.show()
        Net.getBrandDetail(
            accessToken     : gMeInfo.token,
            brandUid        : barndUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.BrandDetailResult
                self.getBrandDetailInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getBrandDetailInfoResult(_ data: Net.BrandDetailResult) {
        
        //set data
        imvBackBG.kf.setImage(with: URL(string: data.backImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        if data.logoImg == "" {
            lbBrandEn.isHidden = false
            lbBrandLogo.isHidden = true
            lbBrandEn.text = data.nameEn
        } else {
            lbBrandEn.isHidden = true
            lbBrandLogo.isHidden = false
            lbBrandLogo.kf.setImage(with: URL(string: data.logoImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        }
        branddLincese = data.licenseUrl
        followStatus = data.brandLikeStatus
        if followStatus {
            btnFollow.isSelected = false
            btnFollow.setTitle(String.init(format: "   팔로잉 %d", data.brandLikeCount), for: .normal)
        } else {
            btnFollow.isSelected = true
            btnFollow.setTitle(String.init(format: "   팔로우 %d", data.brandLikeCount), for: .selected)
        }
        
        //리스트가 있는 탭만 현시
        let allCnt = data.malePdtCount + data.femalePdtCount + data.kidsPdtCount + data.stylePdtCount
        if allCnt > 0 {
            btnMenuT1.isHidden = false
        }

        if data.malePdtCount > 0 {
            btnMenuT2.isHidden = false
        }

        if data.femalePdtCount > 0 {
            btnMenuT3.isHidden = false
        }

        if data.kidsPdtCount > 0 {
            btnMenuT4.isHidden = false
        }

        if data.stylePdtCount > 0 {
            btnMenuT5.isHidden = false
        }

        
        
        changePage()
    }
    
    // 브렌드 상세보기
    func getBrandDetailPdtList() {
        var strTab = ""
        if nCurPage == ETab.men.rawValue {
            strTab = "MALE"
        } else if nCurPage == ETab.women.rawValue {
            strTab = "FEMALE"
        } else if nCurPage == ETab.kids.rawValue {
            strTab = "KIDS"
        } else if nCurPage == ETab.style.rawValue {
            strTab = "STYLE"
        } else {
            strTab = "ALL"
        }
        
        gProgress.show()
        Net.getBrandDetailPdtList(
            accessToken     : gMeInfo.token,
            brandUid        : barndUid,
            tab             : strTab,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AllBrandPdtListResult
                self.getAllBrandDetailListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getAllBrandDetailListResult(_ data: Net.AllBrandPdtListResult) {
        
        lbPdtCnt.text = "( " + String(data.totalElements) + " )"
        isLast = data.last
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrPdtList.append(dic)
        }
        
        if nCurPage == ETab.men.rawValue {
            clvT2.reloadData()
        } else if nCurPage == ETab.women.rawValue {
            clvT3.reloadData()
        } else if nCurPage == ETab.kids.rawValue {
            clvT4.reloadData()
        } else if nCurPage == ETab.style.rawValue {
            clvT5.reloadData()
        } else {
            clvT1.reloadData()
        }
    }
    
    // 브렌드 팔로우 하기
    func setBrandFollow(brandUid : Int) {
        
        gProgress.show()
        Net.setBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : brandUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBrandDetailInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 브렌드 팔로우 취소하기
    func delBrandFollow(brandUid : Int) {
        
        gProgress.show()
        Net.delBrandFollow(
            accessToken     : gMeInfo.token,
            brandUid        : brandUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBrandDetailInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, pdtUid: pdtUid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, pdtUid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            vc.pdtUid = pdtUid
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBrandDetailPdtList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBrandDetailPdtList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension BrandFollowViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            nCurPage = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 5
            
            btnScroll2TopTop.constant = 0
            
            self.pageMoved()
            
        default:
            
            return
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvT1, clvT2, clvT3, clvT4, clvT5:
            // 상단부분 높이
            let TOP_HEIGHT : CGFloat = MAXIMUM_TOP_HEIGHT
            // 터치시 상단 리미트 값
            let TOUCH_LIMIT_Y : CGFloat = -(TOP_HEIGHT - FIX_HEIGHT);
            
            if (scrollView.contentOffset.y < 0.0) {
                let nowH : CGFloat = lcTopTop.constant;
                if (nowH - scrollView.contentOffset.y > 0) {
                    lcTopTop.constant = 0;
                    vwTopTop.alpha = 1
                    lbTitle.alpha = 0
                } else {
                    lcTopTop.constant = nowH - scrollView.contentOffset.y;
                    vwTopTop.alpha = 1-(lcTopTop.constant / TOUCH_LIMIT_Y)
                    lbTitle.alpha = lcTopTop.constant / TOUCH_LIMIT_Y
                }
                scrollView.isUserInteractionEnabled = false;
            }
        default:
            return
        }
    }
}

extension BrandFollowViewController : UICollectionViewDelegate {
    
}

extension BrandFollowViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPdtList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC
        
        let  dic : PdtListDto = arrPdtList[indexPath.row]
            
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
        let blikeStatus     : Bool               = dic.pdtLikeStatus  //유저 상품좋아요 상태
        
//        let width = (collectionView.bounds.width - 15) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
        
        //        cell._imgComWidth.constant = width
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
            height = width * ratio
        }
        cell._imgComHeight.constant = height
        
        cell._imgHeart.image = UIImage(named: indexPath.row % 2 == 0 ? "ic_like_hart" : "ic_dislike_hart")
        cell._btnStyle.isHidden = true
        cell._imgEffect.isHidden = true
        
        cell._imgCom.kf.setImage(with: URL(string: dicPdtStyle.styleImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        if dicPdt.originPrice <= dicPdt.price {
            cell.hiddenDown(price: dicPdt.price)
        } else {
            cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
        }
        cell.lbSize.text = dicPdt.pdtSize
        cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
        
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
        cell._lbBrand.text = dicBrand.nameEn
        cell._lbLike.text = String(likeCnt)
        cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
        
        //user
        cell._lbName.text = dicUsr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        cell._btnProduct.tag = indexPath.row
        cell._btnProduct.addTarget(self, action:#selector(self.onBtnProduct(_:)), for: UIControlEvents.touchUpInside)
        
        //더보기
        if indexPath.row == arrPdtList.count - 1 && !isLast {
            nPage = nPage + 1
            getBrandDetailPdtList()
        }
        
        return cell
    }
}

extension BrandFollowViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
//        let width = (collectionView.bounds.width - 10) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
//
//        return CGSize(width: width, height: height + 153)
        let dic : PdtListDto = arrPdtList[indexPath.row]
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        
        if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
            height = width * ratio
        }
        
        if dic.pdt.originPrice <= dic.pdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 0)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StyleGridHeaderView", for: indexPath as IndexPath)
    }
    
}
