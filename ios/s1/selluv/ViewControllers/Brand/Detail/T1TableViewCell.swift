//
//  T1TableViewCell.swift
//  selluv
//
//  Created by World on 12/20/17.
//

import UIKit

protocol T1CellDelegate {
    func T1CellDelegate(_ t1TableViewCell : T1TableViewCell, tag: Int?, parent : Int?)
}

class T1TableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var delegate:  T1CellDelegate?
    
    @IBOutlet weak var _btnBrand: UIButton!
    @IBOutlet weak var _btnBrandForward: UIButton!
    @IBOutlet weak var _cvMemberList: UICollectionView!
    @IBOutlet weak var _vwEmpty: UIView!
    
    var _imgCom : UIImageView!
    var arrPdtList  : Array<PdtInfoDto> = []
    var parentIdx   : Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }
    
    func  setPdtData(index : Int, arr : [PdtInfoDto]) {
        arrPdtList = arr
        parentIdx = index
        _cvMemberList.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func initUI() {
        _cvMemberList.delegate = self
        _cvMemberList.dataSource = self
        _cvMemberList.register(UINib.init(nibName: "T1CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "T1CollectionViewCell")
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.size.width) / 3, height: (collectionView.bounds.size.width) / 3 * 150 / 120);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPdtList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! T1CollectionViewCell
        
        self._imgCom = cell.imgCom
        
        if(delegate != nil){
            delegate?.T1CellDelegate(self, tag: indexPath.row, parent: parentIdx)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = _cvMemberList.dequeueReusableCell(withReuseIdentifier: "T1CollectionViewCell", for: indexPath as IndexPath) as! T1CollectionViewCell
        
        let dic : PdtInfoDto = arrPdtList[indexPath.row]
        
        cell.imgCom.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

        cell.lbBrandName.text = String.init(format: "%@ %@", dic.brandName, dic.categoryName)
        cell.lbPrice.text = CommonUtil.formatNum(dic.price)
        
        return cell
    }
    
}
