//
//  T2TableViewCell.swift
//  selluv
//
//  Created by World on 12/20/17.
//

import UIKit

protocol T2CellDelegate {
    func T2CellDelegate(_ t2TableViewCell : T2TableViewCell, tag: Int?, parent : Int?)
}

class T2TableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var delegate:  T2CellDelegate?
    var _imgCom : UIImageView!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var _cvList: UICollectionView!
    @IBOutlet weak var lbRank: UILabel!
    @IBOutlet weak var lbBrandEn: UILabel!
    @IBOutlet weak var lbBrandKo: UILabel!
    @IBOutlet weak var ivBrandBG: UIImageView!
    @IBOutlet weak var ivBrandLogo: UIImageView!
    
    var arrPdtList  : Array<PdtInfoDto> = []
    var brandIdx    : Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initUI() {
        _cvList.delegate = self
        _cvList.dataSource = self
        _cvList.register(UINib.init(nibName: "T2CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "T2CollectionViewCell")
    }
    
//    @IBAction func followAction(_ sender: Any) {
//        if btnFollow.isSelected {
//            btnFollow.isSelected = false
//            var nLike = Int(((btnFollow.title(for: .normal)?.replacingOccurrences(of: ",", with:""))?.replacingOccurrences(of: " ", with: ""))!)
//            
//            if nLike! > 0 {
//                nLike! -= 1
//            }
//            
//            btnFollow.setTitle("  " + CommonUtil.formatNum(nLike!), for: .normal)
//            
//        }else {
//            btnFollow.isSelected = true
//            var nLike = Int(((btnFollow.title(for: .normal)?.replacingOccurrences(of: ",", with:""))?.replacingOccurrences(of: " ", with: ""))!)
//            
//            
//            if nLike! > 0 {
//                nLike! += 1
//            }
//            
//            btnFollow.setTitle("  " + CommonUtil.formatNum(nLike!), for: .normal)
//        }
//    }
    
    func  setPdtData(index : Int , arr : [PdtInfoDto]) {
        arrPdtList = arr
        brandIdx = index
        _cvList.reloadData()
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.size.width) / 3, height: (collectionView.bounds.size.width) / 3 * 150 / 148);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPdtList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! T2CollectionViewCell
        
        self._imgCom = cell.imgCom
        
        if(delegate != nil){
            delegate?.T2CellDelegate(self, tag: indexPath.row, parent: brandIdx)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = _cvList.dequeueReusableCell(withReuseIdentifier: "T2CollectionViewCell", for: indexPath as IndexPath) as! T2CollectionViewCell
        
        let dic : PdtInfoDto = arrPdtList[indexPath.row]
        
        cell.imgCom.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

                
        return cell
    }
}
