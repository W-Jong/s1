//
//  T3TableViewCell.swift
//  selluv
//
//  Created by pjh on 12/20/17.
//  modified by PJh on 12/03/17.

import UIKit

class T3TableViewCell: UITableViewCell {

    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var ivThumb: UIImageView!
    @IBOutlet weak var ivLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
//    @IBAction func followAction(_ sender: Any) {
//        if btnFollow.isSelected {
//            btnFollow.isSelected = false
//            var nLike = Int(((btnFollow.title(for: .normal)?.replacingOccurrences(of: ",", with:""))?.replacingOccurrences(of: " ", with: ""))!)
//
//            if nLike! > 0 {
//                nLike! -= 1
//            }
//
//            btnFollow.setTitle("  " + CommonUtil.formatNum(nLike!), for: .normal)
//
//        }else {
//            btnFollow.isSelected = true
//            var nLike = Int(((btnFollow.title(for: .normal)?.replacingOccurrences(of: ",", with:""))?.replacingOccurrences(of: " ", with: ""))!)
//
//
//            if nLike! > 0 {
//                nLike! += 1
//            }
//
//            btnFollow.setTitle("  " + CommonUtil.formatNum(nLike!), for: .normal)
//        }
//    }
    
}
