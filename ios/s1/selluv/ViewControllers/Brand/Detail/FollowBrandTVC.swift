//
//  FollowBrandTVC.swift
//  selluv
//
//  Created by PJH on 23/03/18.
//

import UIKit

class FollowBrandTVC: UITableViewCell {

    @IBOutlet weak var _wtBrandName: NSLayoutConstraint!
    @IBOutlet weak var _wtDel: NSLayoutConstraint!
    @IBOutlet weak var _btnDel: UIButton!
    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _imDel: UIImageView!
    @IBOutlet weak var _imRight: UIImageView!
    @IBOutlet weak var lbBrandEn: UILabel!
    @IBOutlet weak var LBBrandKo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initUI() {
        _wtDel.constant = 0
        _imDel.isHidden = true
        _wtBrandName.constant = 15
        _imRight.image = UIImage.init(named: "ic_mypage_forward")
    }
    
    func selectUI() {
        _wtDel.constant = 64
        _imDel.isHidden = false
        _wtBrandName.constant = 7
        _imRight.image = UIImage.init(named: "ic_brand_edit")
    }
    
}
