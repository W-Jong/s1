//
//  T2CollectionViewCell.swift
//  selluv
//
//  Created by World on 12/20/17.
//

import UIKit

class T2CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var vwImageBg: UIView!
    @IBOutlet weak var imgCom: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }
    
    func initUI() {
        vwImageBg.layer.cornerRadius = 6
    }

}
