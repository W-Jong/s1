//
//  T1CollectionViewCell.swift
//  selluv
//
//  Created by World on 12/20/17.
//

import UIKit

class T1CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var vwImageBg: UIView!
    @IBOutlet weak var imgCom: UIImageView!
    @IBOutlet weak var lbBrandName: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }
    
    func initUI() {
        vwImageBg.layer.cornerRadius = 6
    }

}
