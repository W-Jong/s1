//
//  WebviewViewController.swift
//  selluv
//  웹뷰
//  Created by PJh on 07/04/18.
//

import UIKit

class WebviewViewController: BaseViewController, UIWebViewDelegate {

    var cbCallback : callback! = nil
    public typealias callback = (_ name: String, _ phone : String, _ birth : String, _ gender : String) -> ()
    @IBOutlet weak var _webview: UIWebView!
    
    @IBOutlet weak var lblTitle: UILabel!
    var nPage : Int!
    var dealUid : Int!
    var paymentUrl : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(page: Int, payUrl : String, addrCallBack: callback! = nil) {
        self.init()
        cbCallback = addrCallBack
        self.nPage = page
        self.paymentUrl = payUrl
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, page : Int, url : String, _ callback: callback!) {
        let vcSuggest = WebviewViewController(page : page, payUrl : url, addrCallBack:callback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        _webview.delegate = self
        var weburl = ""
        switch nPage {
        case Webview.DANAL_SMS.rawValue:  //본인인증
            lblTitle.text = NSLocalizedString("self_auth", comment:"")
            weburl = Server_Url + F_API.WEB_DANALSMS.rawValue
            break
        case Webview.PAY.rawValue:  //결제진행

            lblTitle.text = NSLocalizedString("pay", comment:"")
            weburl = paymentUrl
            break
        default:
            break
        }
        
        _webview.loadRequest(URLRequest(url: URL(string: weburl)!))
    }
    
    func shoAlert(msg : String) {
        let encodeStr : String = msg.removingPercentEncoding!
        let alert = UIAlertController(title: "", message: encodeStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("confirm", comment:""), style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        
    }
    
    @nonobjc func webViewDidFinishLoad(webView: UIWebView!)
    {
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
   
        if (request.url?.absoluteString.hasPrefix("selluv://showToast"))!{
            let suburl = request.url?.absoluteString
            let fullNameArr = suburl?.components(separatedBy: "=")
            self.shoAlert(msg: fullNameArr![1])
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://closeWebview"))!{
            dismiss(animated: true, completion: nil)
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://danalFail"))!{
            dismiss(animated: true, completion: nil)
            return false
        }  else if (request.url?.absoluteString.hasPrefix("selluv://danalSuccess"))!{
            //본인인증
            //name, phone, birth, gender
            dismiss(animated: true, completion: nil)
            
            let parseStr = request.url?.absoluteString.components(separatedBy: "?")
            let subAddress = parseStr![1].components(separatedBy: "&")
            let usrName = subAddress[0].components(separatedBy: "=")[1].removingPercentEncoding
            let usrPhone = subAddress[1].components(separatedBy: "=")[1]
            let usrBirth = subAddress[2].components(separatedBy: "=")[1]
            let usrGender = subAddress[3].components(separatedBy: "=")[1]
            
            if cbCallback != nil {
                cbCallback(usrName!, usrPhone, usrBirth, usrGender)
            }
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://paymentSuccess"))!{
            //결제진행
            dismiss(animated: true, completion: nil)
            let parseStr = request.url?.absoluteString.components(separatedBy: "?")
            let dealUid = parseStr![1].components(separatedBy: "=")[1]
            if cbCallback != nil {
                cbCallback(dealUid, "", "", "")
            }
            
        }  else if (request.url?.absoluteString.hasPrefix("selluv://paymentFail"))!{
            dismiss(animated: true, completion: nil)
            return false
        }  else if (request.url?.absoluteString.hasPrefix("http://postcode.map.daum.net"))!{
            return false
        }

        return true
       
    }
    
}
