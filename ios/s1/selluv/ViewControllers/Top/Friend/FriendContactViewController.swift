//
//  FriendContactViewController.swift
//  selluv
//  연락처 친구
//  Created by Comet on 12/19/17.
//  modified by PJH on 24/03/18.

import UIKit
import Toast_Swift
import Contacts
import AddressBook
import MessageUI
import Social

class FriendContactViewController: BaseViewController,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {

    @IBOutlet weak var _tblContactFriend: UITableView!
    
    var arrPhones   : Array<String> = []
    var arrContactList : Array<UsrFollowListDto> = []
    var arrJoinList : Array<UsrFollowListDto> = []
    var arrUsrList : Array<String> = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
        getContacts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initUI() {
        _tblContactFriend.register(UINib.init(nibName: "FriendJoinedCell", bundle: nil), forCellReuseIdentifier: "FriendJoinedCell")
        _tblContactFriend.register(UINib.init(nibName: "FriendUnjoinedCell", bundle: nil), forCellReuseIdentifier: "FriendUnjoinedCell")
        _tblContactFriend.register(UINib.init(nibName: "FriendSection", bundle: nil), forCellReuseIdentifier: "FriendSection")
        
        _tblContactFriend.estimatedSectionHeaderHeight = 80
        _tblContactFriend.sectionHeaderHeight = UITableViewAutomaticDimension
        
    }
    
    //주소록 얻기
    func getContacts() {
        if arrPhones.count > 0{
            arrPhones.removeAll()
        }

        if #available(iOS 9.0, *) {
            let store = CNContactStore()
            // 주소록에서 얻어온 자료들을 저장할 배렬
            var contacts = [CNContact]()
            // 주소록에서 가져올 자료들의 Type, Name,  PhoneNum
            let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as CNKeyDescriptor]
            // 주소록에서 가져올 자료들의 속성을 저장하는 객체
            let request = CNContactFetchRequest(keysToFetch: keys)
            request.sortOrder = CNContactSortOrder.userDefault
            
            store.requestAccess(for: CNEntityType.contacts) { (isGranted, error) in
                if isGranted { // 권한 허용시
                    do {
                        try store.enumerateContacts(with: request) { (contact, stop) -> Void in
                            //do something with contact
                            if contact.phoneNumbers.count > 0 {
                                contacts.append(contact)
                            }
                        }
                        
                        for info in contacts {
                            guard let phone = info.phoneNumbers[0].value.value(forKey: "digits") else {
                                return
                            }
                            let name = info.familyName + info.givenName
                            print(phone)
                            print(name)
                            
                            self.arrPhones.append(String(describing: phone))
                            
                            let dic : UsrFollowListDto = UsrFollowListDto("")
                            dic.usrId = String(describing: phone)
                            dic.usrNckNm = String(describing: name)
                            self.arrContactList.append(dic)
                        }
                        if self.arrPhones.count > 0 {
                            self.getFriendAddressList()
                        } else {
                            self._tblContactFriend.reloadData()
                        }
                        
                    } catch let e as NSError {
                        print(e.localizedDescription)
                        
                    }
                }
                print(isGranted)
            }
            
        } else {
            // Fallback on earlier versions - os 8.0
            var isGranted = true
            if ABAddressBookGetAuthorizationStatus() == .denied || ABAddressBookGetAuthorizationStatus() == .restricted {
                isGranted = false
            } else if ABAddressBookGetAuthorizationStatus() == .notDetermined {
                ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(nil, nil) as ABAddressBook, {(granted, error) in
                    isGranted = granted
                })
            }
            if isGranted {
                let addressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue() as ABAddressBook
                
                let contactsList = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
                for info in contactsList {
                    let firstName =  ABRecordCopyValue(info as ABRecord, kABPersonFirstNameProperty).takeRetainedValue() as! String
                    let lastName =  ABRecordCopyValue(info as ABRecord, kABPersonLastNameProperty).takeRetainedValue() as! String
                    let name = lastName + firstName
                    let phones = ABRecordCopyValue(info as ABRecord, kABPersonPhoneProperty).takeRetainedValue() as ABMultiValue
                    if ABMultiValueGetCount(phones) < 1 {
                        continue
                    }
                    let phonenumber = ABMultiValueCopyValueAtIndex(phones, 0).takeRetainedValue() as! String
                    
                   let dic : UsrFollowListDto = UsrFollowListDto("")
                    dic.usrId = String(describing: phonenumber)
                    dic.usrNckNm = String(describing: name)
                    self.arrContactList.append(dic)
                   
                    self.arrPhones.append(phonenumber)
                }
                if self.arrPhones.count > 0 {
                    self.getFriendAddressList()
                } else {
                    self._tblContactFriend.reloadData()
                }
            } else {
                
                
            }
        }
    }
    
    //전화번호로부터 이름 얻기
    func getNameFrimPhone(num : String) -> String {
        for i in 0..<arrContactList.count {
            let dic : UsrFollowListDto = arrContactList[i]
            if num == dic.usrId {
                return dic.usrNckNm
            }
        }
        return ""
    }
    
    //send msg
    @objc func onBtnInvite(_ sender: UIButton) {
        
        let msg = "selluv"
        let num = arrUsrList[sender.tag]
        
        let img = UIImageView()
        img.kf.setImage(with: URL(string: gMeInfo.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = msg
            controller.recipients = [num]
            let imageData = UIImagePNGRepresentation(img.image!)
            controller.addAttachmentData(imageData!, typeIdentifier: "public.data", filename: "profile.png")
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        } else {
            MsgUtil.showUIAlert("메일을 보낼 수 없습니다.")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result.rawValue) {
        case MessageComposeResult.cancelled.rawValue:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.failed.rawValue:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.sent.rawValue:
            MsgUtil.showUIAlert("초대 성공하였습니다.")
            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
    
    @objc func onBtnProfile(_ sender: UIButton) {
        
        let dic : UsrFollowListDto = arrJoinList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
        
    }


    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        popVC()
    }
    
    @objc func onBtnFollow(_ sender: UIButton) {
        
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            let cell = _tblContactFriend.cellForRow(at: indexPath) as! FriendJoinedCell
            let dic : UsrFollowListDto = arrJoinList[indexPath.row]
            if dic.usrLikeStatus {
                let headerView = UIView()
                headerView.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 100))
                
                let ivProfile = UIImageView()
                ivProfile.frame = CGRect(x: headerView.frame.width / 2 - 30, y: 20, width: 40, height: 40)
                ivProfile.image = cell._imvProfile.image
                ivProfile.layer.cornerRadius = 20
                ivProfile.clipsToBounds = true
                headerView.addSubview(ivProfile)
                
                let lbTitle = UILabel()
                
                let 블랙볼드스타일 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x000000)]
                let 블랙스타일 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x999999)]
                
                let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  cell._lbNickname.text!, attributes: 블랙볼드스타일))
                contentStr.append(NSAttributedString(string: "님의 팔로우를 취소하시겠어요?", attributes: 블랙스타일))
                
                lbTitle.attributedText = contentStr
                lbTitle.frame = CGRect(x: -10, y: 70, width: headerView.bounds.size.width, height: 20)
                lbTitle.textAlignment = .center
                headerView.addSubview(lbTitle)
                
                let  actionSheet : TOActionSheet = TOActionSheet.init(headerView: headerView)
                actionSheet.style = TOActionSheetStyle.user
                actionSheet.addButton(withTitle: "팔로우 취소", tappedBlock: {
                    self.delFollow(_uid: dic.usrUid)
                })
                
                actionSheet.show(from: self.view, in: self.view)
            } else {
                self.reqFollow(_uid: dic.usrUid)
            }
        }
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 연락처 친구찾기
    func getFriendAddressList() {
        
        gProgress.show()
        Net.getFriendAddressList(
            accessToken     : gMeInfo.token,
            list            : arrPhones,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getFriendAddressResult
                self.getFriendAddressResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //연락처 친구찾기 결과
    func getFriendAddressResult(_ data: Net.getFriendAddressResult) {
        arrJoinList = data.joinedList
        arrUsrList = data.unJoinedPhoneList
        _tblContactFriend.reloadData()
    }
    
    // 유저 팔로우 하기
    func reqFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getFriendAddressList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 해제하기
    func delFollow(_uid : Int) {
        
        gProgress.show()
        Net.delFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getFriendAddressList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension FriendContactViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrJoinList.count
        } else {
            return arrUsrList.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendSection") as! FriendSection
        
        if(section == 0 && arrJoinList.count > 0) {
            cell._lbFriendSectionTitle1.text = String(format: "연락처 친구 중 %d명이 셀러버입니다.", arrJoinList.count)
            return cell
        } else if(section == 1) {
            cell._lbFriendSectionTitle1.text = "가입하지 않은 친구 초대하여"
            cell._lbFriendSectionTitle1.textColor = UIColor(hex: 0x666666)
            cell._lbFriendSectionTitle1.font = .systemFont(ofSize : 13)
            cell._lbFriendSectionTitle2.text = "친구 가입으로 무제한 적립금 받아보세요."
            cell._lbFriendSectionTitle3.text = "아직 가입하지 않은 친구"
            return cell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) { // 연락처 친구
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendJoinedCell", for: indexPath) as! FriendJoinedCell
            let dic : UsrFollowListDto = arrJoinList[indexPath.row]
            
            if dic.usrLikeStatus {
                cell.setFollowingState()
            } else {
                cell.setFollowState()
            }
            
            cell._imvProfile.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
            cell._lbID.text = dic.usrId
            cell._lbNickname.text = dic.usrNckNm
            
            cell._btnProfile.tag = indexPath.row
            cell._btnProfile.addTarget(self, action:#selector(self.onBtnProfile(_:)), for: UIControlEvents.touchUpInside)
            cell._btnFollow.withValue = ["index" : indexPath]
            cell._btnFollow.addTarget(self, action:#selector(self.onBtnFollow(_:)), for: UIControlEvents.touchUpInside)

            return cell
        } else { // 가입하지 않은 친구.
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendUnjoinedCell", for: indexPath) as! FriendUnjoinedCell
            
            let phont_num = arrUsrList[indexPath.row]
            cell.lbPhonenum.text = phont_num.replacingOccurrences(of: "(\\d{3})(\\d{3,4})(\\d{4})", with: "$1-$2-$3", options: .regularExpression, range: nil)
            cell.lbNickname.text = getNameFrimPhone(num: phont_num)
            
            cell._btnInvite.tag = indexPath.row
            cell._btnInvite.addTarget(self, action:#selector(onBtnInvite(_:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}


