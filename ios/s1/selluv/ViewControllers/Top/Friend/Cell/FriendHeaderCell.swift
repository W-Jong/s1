//
//  FriendHeaderCell.swift
//  selluv
//
//  Created by Comet on 12/17/17.
//

import UIKit

protocol FriendHeaderCellDelegate {
    func FriendHeaderCellDelegate(_ friendHeaderCell : FriendHeaderCell, onBtnSearchFB sender : Any, tag: String?)
    func FriendHeaderCellDelegate(_ friendHeaderCell : FriendHeaderCell, onBtnSearchContact sender : Any, tag: String?)
    func FriendHeaderCellDelegate(_ friendHeaderCell : FriendHeaderCell, onBtnInviteFriend sender : Any, tag: String?)
}

class FriendHeaderCell: UITableViewCell {

    var delegate:  FriendHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnSearchFB(_ sender: Any) {
        if(delegate != nil){
            delegate?.FriendHeaderCellDelegate(self, onBtnSearchFB: sender, tag: nil)
        }
    }
    @IBAction func onBtnSearchContact(_ sender: Any) {
        if(delegate != nil){
            delegate?.FriendHeaderCellDelegate(self, onBtnSearchContact: sender, tag: nil)
        }
    }
    @IBAction func onBtnInviteFriend(_ sender: Any) {
        if(delegate != nil){
            delegate?.FriendHeaderCellDelegate(self, onBtnInviteFriend: sender, tag: nil)
        }
    }
}
