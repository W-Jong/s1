//
//  FriendUnjoinedCell.swift
//  selluv
//
//  Created by Comet on 12/19/17.
//  modified by PJH on 24/03/18.

import UIKit

class FriendUnjoinedCell: UITableViewCell {

    @IBOutlet weak var lbPhonenum: UILabel!
    @IBOutlet weak var lbNickname: UILabel!
    @IBOutlet weak var _btnInvite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _btnInvite.layer.cornerRadius = 4.3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
