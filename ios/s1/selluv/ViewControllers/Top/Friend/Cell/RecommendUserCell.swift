//
//  RecommendUserCell.swift
//  selluv
//
//  Created by Comet on 12/17/17.
//

import UIKit

class RecommendUserCell: UITableViewCell {

    @IBOutlet weak var _imvProfile: UIImageView!
    @IBOutlet weak var _lbNickname: UILabel!
    @IBOutlet weak var _lbItem: UILabel!
    @IBOutlet weak var _lbItemCount: UILabel!
    @IBOutlet weak var _lbFollow: UILabel!
    @IBOutlet weak var _lbFollowCount: UILabel!
    @IBOutlet weak var _stvButton: UIStackView!
    @IBOutlet weak var _btnFollow: UIButton!
    @IBOutlet weak var _btnHide: UIButton!
    @IBOutlet weak var _imvItem1: UIImageView!
    @IBOutlet weak var _imvItem2: UIImageView!
    @IBOutlet weak var _imvItem3: UIImageView!
    @IBOutlet var _btnUser: UIButton!
    @IBOutlet var _btnFollowCnt: UIButton!
    @IBOutlet var _btnProfile: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _btnFollow.layer.cornerRadius = 4.3
        _btnHide.layer.cornerRadius = 4.3
        _btnHide.layer.borderWidth = 1
        _btnHide.layer.borderColor = UIColor(hex: 0xc5c5c5).cgColor
        
        _imvItem1.layer.cornerRadius = 4.3
        _imvItem2.layer.cornerRadius = 4.3
        _imvItem3.layer.cornerRadius = 4.3

        _imvProfile.layer.cornerRadius = _imvProfile.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPdtData(arrPdt : [PdtMiniDto])  {
        
        _imvItem1.kf.setImage(with: URL(string: arrPdt[0].profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        _imvItem2.kf.setImage(with: URL(string: arrPdt[1].profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        _imvItem3.kf.setImage(with: URL(string: arrPdt[2].profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
    }
    
    func setFollowingState() {
        _btnHide.isHidden = true
        _btnFollow.setTitle("팔로잉", for: .normal)
        _btnFollow.backgroundColor = UIColor(hex: 0xffffff)
        _btnFollow.setTitleColor(UIColor(hex:0x333333), for: .normal)
        _btnFollow.setBorder(color: UIColor(hex:0xc7c7c7), width: 1)
    }
    
    func setFollowState() {
        _btnHide.isHidden = false
        _btnFollow.setTitle("팔로우", for: .normal)
        _btnFollow.backgroundColor = UIColor(hex: 0x42c2fe)
        _btnFollow.setTitleColor(UIColor(hex:0xffffff), for: .normal)
        _btnFollow.setBorder(color: UIColor(hex:0x42c2fe), width: 1)
    }
}
