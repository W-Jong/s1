//
//  FriendSection.swift
//  selluv
//
//  Created by Comet on 12/19/17.
//

import UIKit

class FriendSection: UITableViewCell {
    @IBOutlet weak var _lbFriendSectionTitle1: UILabel!
    @IBOutlet weak var _lbFriendSectionTitle2: UILabel!
    @IBOutlet weak var _lbFriendSectionTitle3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
