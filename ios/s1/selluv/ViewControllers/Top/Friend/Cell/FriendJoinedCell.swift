//
//  FriendJoinedCell.swift
//  selluv
//
//  Created by Comet on 12/19/17.
//  modified by PJH on 15/03/18.

import UIKit

class FriendJoinedCell: UITableViewCell {

    @IBOutlet weak var _imvProfile: UIImageView!
    @IBOutlet weak var _lbNickname: UILabel!
    @IBOutlet weak var _lbID: UILabel!
    @IBOutlet weak var _btnFollow: UIButton!
    @IBOutlet weak var _btnProfile: UIButton!
    
    @IBOutlet weak var btnProfile: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _btnFollow.layer.cornerRadius = 4.3
        _imvProfile.layer.cornerRadius = 21
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setFollowingState() {
        _btnFollow.setTitle("팔로잉", for: .normal)
        _btnFollow.backgroundColor = UIColor(hex: 0xffffff)
        _btnFollow.setTitleColor(UIColor(hex:0x333333), for: .normal)
        _btnFollow.setBorder(color: UIColor(hex:0xc7c7c7), width: 1)
    }
    
    func setFollowState() {
        _btnFollow.setTitle("팔로우", for: .normal)
        _btnFollow.backgroundColor = UIColor(hex: 0x42C2FE)
        _btnFollow.setTitleColor(UIColor(hex:0xffffff), for: .normal)
        _btnFollow.setBorder(color: UIColor(hex:0x42C2FE), width: 1)
    }
}
