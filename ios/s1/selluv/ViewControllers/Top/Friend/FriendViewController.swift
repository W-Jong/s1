//
//  FriendViewController.swift
//  selluv
//  친구 찾기
//  Created by Comet on 12/17/17.
//  modified by PJH on 29/03/18.

import UIKit

class FriendViewController: BaseViewController {

    @IBOutlet weak var _tblFindingFriend: UITableView!
    
    var arrUsrList : Array<UsrRecommendDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initUI()
        getRecommendUsrList()
    }

    func initUI() {
        _tblFindingFriend.register(UINib.init(nibName: "RecommendUserCell", bundle: nil), forCellReuseIdentifier: "RecommendUserCell")
        _tblFindingFriend.register(UINib.init(nibName: "FriendHeaderCell", bundle: nil), forCellReuseIdentifier: "FriendHeaderCell")
        
        _tblFindingFriend.estimatedRowHeight = 100
        _tblFindingFriend.estimatedSectionHeaderHeight = 100
        _tblFindingFriend.sectionHeaderHeight = UITableViewAutomaticDimension
        _tblFindingFriend.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        popVC()
    }
    
    @objc func FollowAction(_ sender : UIButton) {
        
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            let cell = _tblFindingFriend.cellForRow(at: indexPath) as! RecommendUserCell
            let dic : UsrRecommendDto = arrUsrList[sender.tag]
            if dic.usrLikeStatus {
                let headerView = UIView()
                headerView.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 100))
                
                let ivProfile = UIImageView()
                ivProfile.frame = CGRect(x: headerView.frame.width / 2 - 30, y: 20, width: 40, height: 40)
                ivProfile.image = cell._imvProfile.image
                ivProfile.layer.cornerRadius = 20
                ivProfile.clipsToBounds = true
                headerView.addSubview(ivProfile)
                
                let lbTitle = UILabel()
                
                let 블랙볼드스타일 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x000000)]
                let 블랙스타일 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x999999)]
                
                let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  cell._lbNickname.text!, attributes: 블랙볼드스타일))
                contentStr.append(NSAttributedString(string: "님의 팔로우를 취소하시겠어요?", attributes: 블랙스타일))
                
                lbTitle.attributedText = contentStr
                lbTitle.frame = CGRect(x: -10, y: 70, width: headerView.bounds.size.width, height: 20)
                lbTitle.textAlignment = .center
                headerView.addSubview(lbTitle)
                
                let  actionSheet : TOActionSheet = TOActionSheet.init(headerView: headerView)
                actionSheet.style = TOActionSheetStyle.user
                actionSheet.addButton(withTitle: "팔로우 취소", tappedBlock: {
                    self.delFollow(_uid: dic.usrUid)
                })
                
                actionSheet.show(from: self.view, in: self.view)
            } else {
                self.reqFollow(_uid: dic.usrUid)
            }
        }
    }
    
    @objc func HideAction(_ sender : UIButton) {
        let dic : UsrRecommendDto = arrUsrList[sender.tag]
        delRecommendUsr(_uid: dic.usrUid)
    }
    
    @objc func UserAction(_ sender : UIButton) {
        let dic : UsrRecommendDto = arrUsrList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func FollowCntAction(_ sender : UIButton) {
        let dic : UsrRecommendDto = arrUsrList[sender.tag]
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
        vc.following = false
        vc.usrUid = dic.usrUid
        vc.topName = dic.usrNckNm
        self.navigationController?.pushViewController(vc, animated: true)
    }

    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 셀리버 추천 유저 목록
    func getRecommendUsrList() {
        
        gProgress.show()
        Net.getRecommendUsrList(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getRecommendUsrListResult
                self.getRecommendUsrListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //network 결과
    func getRecommendUsrListResult(_ data: Net.getRecommendUsrListResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrUsrList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : UsrRecommendDto = data.list[i]
            arrUsrList.append(dic)
        }
        _tblFindingFriend.reloadData()
    }
    
    // 유저 숨기기
    func delRecommendUsr(_uid : Int) {
        
        gProgress.show()
        Net.hideRecommendUsr(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
               self.getRecommendUsrList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 하기
    func reqFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getRecommendUsrList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 해제하기
    func delFollow(_uid : Int) {
        
        gProgress.show()
        Net.delFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getRecommendUsrList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension FriendViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsrList.count
    }
    
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendHeaderCell") as! FriendHeaderCell
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecommendUserCell", for: indexPath) as! RecommendUserCell //최근검색페지일때
        
        let dic : UsrRecommendDto = arrUsrList[indexPath.row]
        
        cell._imvProfile.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbNickname.text = dic.usrNckNm
        cell._lbItemCount.text = String(dic.pdtCount)
        cell._lbFollowCount.text = String(dic.usrFollowerCount)
        
        if dic.usrLikeStatus {
            cell.setFollowingState()
        } else {
            cell.setFollowState()
        }
        cell.setPdtData(arrPdt: dic.pdtList)
        
        cell._btnFollow.withValue = ["index" : indexPath]
        cell._btnFollow.addTarget(self, action: #selector(FollowAction), for: .touchUpInside)

        cell._btnHide.tag = indexPath.row
        cell._btnHide.addTarget(self, action: #selector(HideAction), for: .touchUpInside)

        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action: #selector(UserAction), for: .touchUpInside)

        cell._btnProfile.tag = indexPath.row
        cell._btnProfile.addTarget(self, action: #selector(UserAction), for: .touchUpInside)

        cell._btnFollowCnt.tag = indexPath.row
        cell._btnFollowCnt.addTarget(self, action: #selector(FollowCntAction), for: .touchUpInside)

        if indexPath.row == arrUsrList.count - 1 && !isLast {
            nPage = nPage + 1
            getRecommendUsrList()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}


extension FriendViewController: FriendHeaderCellDelegate {
    func FriendHeaderCellDelegate(_ friendHeaderCell : FriendHeaderCell, onBtnSearchFB sender : Any, tag: String?) {
        pushVC("FRIEND_FB_VIEW", storyboard: "Top", animated: true)
    }
    func FriendHeaderCellDelegate(_ friendHeaderCell : FriendHeaderCell, onBtnSearchContact sender : Any, tag: String?) {
        pushVC("FRIEND_CONTACT_VIEW", storyboard: "Top", animated: true)
    }
    func FriendHeaderCellDelegate(_ friendHeaderCell : FriendHeaderCell, onBtnInviteFriend sender : Any, tag: String?) {
        pushVC("FRIEND_INVITE_VIEW", storyboard: "Top", animated: true)
    }
}

