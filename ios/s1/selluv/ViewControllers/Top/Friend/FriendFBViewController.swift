//
//  FriendFBViewController.swift
//  selluv
//  페이스북 친구찾기
//  Created by Comet on 12/19/17.
//  modified by PJh on 25/03/18.

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class FriendFBViewController: BaseViewController {

    @IBOutlet weak var _tblFBFriend: UITableView!
    
    var arrUsrList : Array<UsrFollowListDto> = []
    var arrFBList  : Array<String> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
        loginFacebook()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func initUI() {
        _tblFBFriend.register(UINib.init(nibName: "FriendJoinedCell", bundle: nil), forCellReuseIdentifier: "FriendJoinedCell")
        _tblFBFriend.register(UINib.init(nibName: "FriendSection", bundle: nil), forCellReuseIdentifier: "FriendSection")
        
        _tblFBFriend.estimatedSectionHeaderHeight = 80
        _tblFBFriend.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    func loginFacebook() {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        loginManager.loginBehavior = .web
        loginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                if(result?.isCancelled)! {
                    MsgUtil.showUIAlert("페이스북 로그인에 실패했습니다.")
                } else {
                    self.getFBFriendList()
                }
            }
        }
    }
  
    //페이스북 친구리스트 얻기
    func getFBFriendList() {
      
        let params = ["fields": "id, first_name, last_name, name, email, picture"]
        let graphRequest = FBSDKGraphRequest(graphPath: "/me/friends", parameters: params)
        let connection = FBSDKGraphRequestConnection()
        connection.add(graphRequest, completionHandler: { (connection, result, error) in
            if error == nil {
                
                let resultdict = result as! NSDictionary
                print("Result Dict: \(resultdict)")
                let data : NSArray = resultdict.object(forKey: "data") as! NSArray
                
                for i in 0 ..< data.count
                {
                    let valueDict : NSDictionary = data[i] as! NSDictionary
                    let strid = valueDict.object(forKey: "id") as! String
//                    let name = valueDict.object(forKey: "name") as! String
//                    let picture  = ""
//                    var snsId = (w_result["id"] as? String) ?? ""
//                    var snsName = (w_result["name"] as? String) ?? ""
//                    var snsEmail = (w_result["email"] as? String) ?? ""
                    
//                    let dic : UsrFollowListDto = UsrFollowListDto("")
//                    dic.usrId = strid
//                    dic.usrNckNm = name
//                    dic.profileImg = picture
//                    self.arrFBList.append(dic)
                    self.arrFBList.append(strid)
                }
                self.getFriendFacebookList()
            } else {
                CommonUtil.showToast("친구리스트 얻기에 실패했습니다.")
            }
        })
        connection.start()
    }
    
    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        popVC()
    }
    
    @objc func onBtnFollow(_ sender: UIButton) {
        
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            let cell = _tblFBFriend.cellForRow(at: indexPath) as! FriendJoinedCell
            let dic : UsrFollowListDto = arrUsrList[indexPath.row]
            if dic.usrLikeStatus {
                let headerView = UIView()
                headerView.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 100))
                
                let ivProfile = UIImageView()
                ivProfile.frame = CGRect(x: headerView.frame.width / 2 - 30, y: 20, width: 40, height: 40)
                ivProfile.image = cell._imvProfile.image
                ivProfile.layer.cornerRadius = 20
                ivProfile.clipsToBounds = true
                headerView.addSubview(ivProfile)
                
                let lbTitle = UILabel()
                
                let 블랙볼드스타일 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x000000)]
                let 블랙스타일 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x999999)]
                
                let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  cell._lbNickname.text!, attributes: 블랙볼드스타일))
                contentStr.append(NSAttributedString(string: "님의 팔로우를 취소하시겠어요?", attributes: 블랙스타일))
                
                lbTitle.attributedText = contentStr
                lbTitle.frame = CGRect(x: -10, y: 70, width: headerView.bounds.size.width, height: 20)
                lbTitle.textAlignment = .center
                headerView.addSubview(lbTitle)
                
                let  actionSheet : TOActionSheet = TOActionSheet.init(headerView: headerView)
                actionSheet.style = TOActionSheetStyle.user
                actionSheet.addButton(withTitle: "팔로우 취소", tappedBlock: {
                    self.delFollow(_uid: dic.usrUid)
                })
                
                actionSheet.show(from: self.view, in: self.view)
            } else {
                self.reqFollow(_uid: dic.usrUid)
            }
        }
    }
    
    @objc func onBtnProfile(_ sender: UIButton) {
        let dic : UsrFollowListDto = arrUsrList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 페이스북 친구찾기
    func getFriendFacebookList() {
        
        gProgress.show()
        Net.getFriendFacebookList(
            accessToken     : gMeInfo.token,
            list            : arrFBList,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getFriendFacebookResult
                self.getFriendFacebookResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //페이스북 친구찾기 결과
    func getFriendFacebookResult(_ data: Net.getFriendFacebookResult) {
       
        arrUsrList = data.list
        _tblFBFriend.reloadData()
    }
    
    // 유저 팔로우 하기
    func reqFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getFriendFacebookList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 해제하기
    func delFollow(_uid : Int) {
        
        gProgress.show()
        Net.delFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getFriendFacebookList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension FriendFBViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsrList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendSection") as! FriendSection
        cell._lbFriendSectionTitle1.text = String(format: "페이스북 친구 중 %d명이 셀러버입니다.", 5)
        cell._lbFriendSectionTitle3.text = "페이스북 친구"
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendJoinedCell", for: indexPath) as! FriendJoinedCell
        let dic : UsrFollowListDto = arrUsrList[indexPath.row]
        
        if dic.usrLikeStatus {
            cell.setFollowingState()
        } else {
            cell.setFollowState()
        }
        
        cell._imvProfile.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbID.text = dic.usrId
        cell._lbNickname.text = dic.usrNckNm
        
        cell._btnProfile.tag = indexPath.row
        cell._btnProfile.addTarget(self, action:#selector(self.onBtnProfile(_:)), for: UIControlEvents.touchUpInside)
        cell._btnFollow.withValue = ["index" : indexPath]
        cell._btnFollow.addTarget(self, action:#selector(self.onBtnFollow(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

