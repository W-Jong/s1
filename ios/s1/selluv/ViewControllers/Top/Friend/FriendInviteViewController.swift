//
//  FriendInviteViewController.swift
//  selluv
//  친구초대
//  Created by World on 1/23/18.
//  modified by PJH on 24/03/18.

import UIKit
import Toast_Swift

class FriendInviteViewController: BaseViewController {

    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var lbReconCode: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getInviteInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        vwMain.layer.shadowColor = UIColor.gray.cgColor
        vwMain.layer.shadowOpacity = 0.3
        vwMain.layer.shadowOffset = .zero
        vwMain.layer.shadowRadius = 6
        vwMain.layer.masksToBounds = false
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickKakao(_ sender: Any) {
        self.view.makeToast("카카오톡으로 보내기")
    }
    
    @IBAction func onClickSendCode(_ sender: Any) {
//        self.view.makeToast("추천인 코드 보내기")
        let strContent = "https://play.google.com/store/apps/details?id=com.reviewdar&hl=ko"
        let shareItems : Array = [strContent]
        let shareActivity: UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        shareActivity.excludedActivityTypes = []
        self.present(shareActivity, animated: true, completion: nil)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 초대정보 얻기
    func getInviteInfo() {
        
        gProgress.show()
        Net.getInviteInfo(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.InviteInfoResult
                self.getInviteInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //초대정보 얻기 결과
    func getInviteInfoResult(_ data: Net.InviteInfoResult) {
        lbReconCode.text = data.inviteCode
    }
}
