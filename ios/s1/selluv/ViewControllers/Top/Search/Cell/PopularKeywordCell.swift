//
//  PopularKeywordCell.swift
//  selluv
//
//  Created by Comet on 12/11/17.
//

import UIKit


protocol PopularKeywordCellDelegate {
    func PopularKeywordCellDelegate(_ popularKeywordCell : PopularKeywordCell, onBtnGo2SearchResult sender : Any, tag: String!)
}

class PopularKeywordCell: UITableViewCell {
    
    var delegate:  PopularKeywordCellDelegate?
    
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var keyword: UILabel!
    @IBOutlet weak var visit_count: UILabel!
    @IBOutlet weak var metaInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnDetail(_ sender: Any) {
        if(delegate != nil){
            delegate?.PopularKeywordCellDelegate(self, onBtnGo2SearchResult: sender, tag: self.keyword.text)
        }
    }
}
