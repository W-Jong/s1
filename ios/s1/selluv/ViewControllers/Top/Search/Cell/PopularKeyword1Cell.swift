//
//  PopularKeyword1Cell.swift
//  selluv
//
//  Created by Comet on 12/11/17.
//

import UIKit


protocol PopularKeyword1CellDelegate {
    func PopularKeyword1CellDelegate(_ popularKeywordCell : PopularKeyword1Cell, onBtnGo2SearchResult sender : Any, tag: String!)
}

class PopularKeyword1Cell: UITableViewCell {
    
    var delegate:  PopularKeyword1CellDelegate?
    
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var keyword: UILabel!
    @IBOutlet weak var metaInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnDetail(_ sender: Any) {
        if(delegate != nil){
            delegate?.PopularKeyword1CellDelegate(self, onBtnGo2SearchResult: sender, tag: self.keyword.text)
        }
    }
}
