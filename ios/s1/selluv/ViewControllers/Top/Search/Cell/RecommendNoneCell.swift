//
//  RecommendNoneCell.swift
//  selluv
//
//  Created by Comet on 12/12/17.
//

import UIKit

protocol RecommendNoneCellDelegate {
    func RecommendNoneCellDelegate(_ recommendNoneCell : RecommendNoneCell, onBtnGo2SearchResult sender : Any, tag: Int?)
}

class RecommendNoneCell: UITableViewCell {

    var delegate:  RecommendNoneCellDelegate?
    
    @IBOutlet weak var m_lblGuide: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnDetail(_ sender: Any) {
        if(delegate != nil){
            delegate?.RecommendNoneCellDelegate(self, onBtnGo2SearchResult: sender, tag: nil)
        }
    }
}
