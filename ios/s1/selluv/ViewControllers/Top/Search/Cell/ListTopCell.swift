//
//  ListTopCell.swift
//  selluv
//
//  Created by Comet on 12/11/17.
//

import UIKit

protocol ListTopCellDelegate {
    func ListTopCellDelegate(_ listTopCell : ListTopCell, onBtnRemoveHistoryKeyList sender : Any, tag: Int)
}

class ListTopCell: UITableViewCell {
    
    var delegate:  ListTopCellDelegate?
    
    @IBOutlet weak var m_lbTopLabel: UILabel!
    @IBOutlet weak var m_btnRemove: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnRemoveHistoryKeyList(_ sender: Any) {
        if delegate != nil {
            delegate?.ListTopCellDelegate(self, onBtnRemoveHistoryKeyList: sender, tag: 0)
        }
    }
}
