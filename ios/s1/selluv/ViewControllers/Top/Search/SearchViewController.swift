//
//  SearchViewController.swift
//  selluv
//  검색
//  Created by Comet on 12/11/17.
//  modified by PJH on 30/03/18.

import UIKit

class SearchViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var m_tfKeyword: DesignableUITextField!
    @IBOutlet weak var m_tbList: UITableView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    
    @IBOutlet var vwClose: UIView!
    @IBOutlet var vwContentBottom: NSLayoutConstraint!
    
    var m_popularKeywords : [Keyword]? = nil
    var m_historyKeywords : [Keyword]? = nil
    var m_recommendKeywords : [Keyword]? = nil
    
    var m_searchState: Int! = -1   // -1 : popularSearch, 0 : historySearch, 1 : recommendSearch, 2: non recommend
    
    var arrFameList     : Array<SearchWordDto> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        m_popularKeywords = []
        m_historyKeywords = []
        m_recommendKeywords = []
        
        m_tfKeyword.addTarget(self, action: #selector(self.searchTextChanged), for: .editingChanged)
        
        m_tbList.register(UINib.init(nibName: "ListTopCell", bundle: nil), forCellReuseIdentifier: "ListTopCell")
        m_tbList.register(UINib.init(nibName: "RecommendNoneCell", bundle: nil), forCellReuseIdentifier: "RecommendNoneCell")
        m_tbList.register(UINib.init(nibName: "PopularKeywordCell", bundle: nil), forCellReuseIdentifier: "PopularKeywordCell")
        m_tbList.register(UINib.init(nibName: "PopularKeyword1Cell", bundle: nil), forCellReuseIdentifier: "PopularKeyword1Cell")
        registerForKeyboardNotifications()
        
        showPopularSearchList()
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //인기검색
    func showPopularSearchList() {
        m_searchState = -1
        getFameSearchList()
    }
    
    //최근검색
    func showHistorySearchList() {
        m_searchState = 0
        getHistoryKeywordList()
    }
    
    //추천검색
    func showRecommendSearchList(_data : String) {
        m_searchState = 1
//        getRecommendKeywordList()
//        m_tbList.reloadData()
        getSearchRecommendList(_key: _data)
    }
    
    //
    // call api functions
    //
    
    func getHistoryKeywordList() {
        m_historyKeywords?.removeAll()
        
        let arrKeyList  : NSMutableArray! = DBManager.getSharedInstance().getKeywords()
        
        for i in 0 ..< arrKeyList.count {
            let m : KeywordModel = arrKeyList.object(at: i) as! KeywordModel
            let obj = Keyword(i, "", m.keyword, "", Int(m.count)!)
            m_historyKeywords?.append(obj)
        }
        
        m_tbList.reloadData()
    }
    
//    func getRecommendKeywordList() {
//        m_recommendKeywords?.removeAll()
//        for i in 0 ..< 15 {
//            let obj = Keyword(i, "", String(format: "추천 %d", i), i)
//            if i == 0 {
//                obj.isRecommendItem = true
//                obj.recommend_type = "브랜드"
//            } else if (i == 4) {
//                obj.isRecommendItem = true
//                obj.recommend_type = "카테고리"
//            } else if (i == 9) {
//                obj.isRecommendItem = true
//                obj.recommend_type = "모델명"
//            } else if i == 13 {
//                obj.isRecommendItem = true
//                obj.recommend_type = "태그"
//            }
//            m_recommendKeywords?.append(obj)
//        }
//    }
    
    func go2SearchResultPage(_ keyword_name : String!) {
        hideKeyboard()
        let vc = (UIStoryboard.init(name: "Top", bundle: nil).instantiateViewController(withIdentifier: "SEARCH_RESULT_VIEW")) as! SearchResultViewController
        vc.pageTitle = keyword_name
        vc.goPage = WherePage.search.rawValue
        gSearchInfo.setKeyword(value: keyword_name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func hideKeyboard() {
        m_tfKeyword.resignFirstResponder()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func searchTextChanged(_ sender : UITextField) {
        if(m_tfKeyword.text != "") {
//            if(strlen(m_tfKeyword.text) >= 5) { // for Test Mode
//                m_searchState = 1
//                m_recommendKeywords?.removeAll()
//                m_tbList.reloadData()
//            } else {
             showRecommendSearchList(_data: m_tfKeyword.text!)
        } else {
            showHistorySearchList()
        }
    }
    
    @IBAction func hideKeyboardAction(_ sender: Any) {
        if m_tfKeyword != nil {
            m_tfKeyword?.resignFirstResponder()
        }
    }
    
    @IBAction func onBtnCancel(_ sender: Any) {
        m_tfKeyword.text = ""
        hideKeyboardAction(self)
        popVC()
//        switch(m_searchState) {
//        case 0: // HistorySearch so, back to PopuarSearch Screen.
//            showPopularSearchList()
//            break;
//        case 1: //Recommend or non-RecommendSearch so, back to HistorySearch Screen.
//            showHistorySearchList()
//            break;
//        default: //PopularSearch so, back to before Page.
//            popVC()
//            break;
//        }
    }
    
    @IBAction func onBtnSearch(_ sender: Any) {
        if(m_tfKeyword.text == "" || m_tfKeyword.text == nil) {
            onBtnCancel(self)
            return
        } else {
            go2SearchResultPage(m_tfKeyword.text)
        }
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 인기검색
    func getFameSearchList() {
        
        gProgress.show()
        Net.getSearchFame(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSearchFameResult
                self.getSearchFameResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //인기검색 결과
    func getSearchFameResult(_ data: Net.getSearchFameResult) {
        
        arrFameList.removeAll()
        arrFameList.append(SearchWordDto(""))//header
        for i in 0..<data.list.count {
            let dic : SearchWordDto = data.list[i]
            arrFameList.append(dic)
        }
        m_tbList.reloadData()
    }
    
    // 추천검색
    func getSearchRecommendList(_key : String) {
        
        let searchKey = _key.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)

        gProgress.show()
        Net.getSearchRecommend(
            accessToken     : gMeInfo.token,
            keyword         : searchKey!,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSearchRecommendResult
                self.getSearchRecommendResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //추천검색 결과
    func getSearchRecommendResult(_ data: Net.getSearchRecommendResult) {
        
        m_recommendKeywords?.removeAll()
        let dicSearchDto : RecommendSearchDto = data.searchDto
        
        if dicSearchDto.brandList.count > 0 {
            // add header
            let obj = Keyword(0, "", "", "", 0)
            obj.isRecommendItem = true
            obj.recommend_type = "브랜드"
            m_recommendKeywords?.append(obj)
            for i in 0 ..< dicSearchDto.brandList.count {
                let dicBrand : BrandDao = dicSearchDto.brandList[i]
                let obj = Keyword(0, "", dicBrand.nameEn,dicBrand.nameKo, 0)
                m_recommendKeywords?.append(obj)
            }
        }

        if dicSearchDto.categoryDaoList[0].count > 0 {
            // add header
            let obj = Keyword(0, "", "", "", 0)
            obj.isRecommendItem = true
            obj.recommend_type = "카테고리"
            m_recommendKeywords?.append(obj)
            for i in 0 ..< dicSearchDto.categoryDaoList.count {
                var str1 = ""
                var str2 = ""
                
                let dic1 : CategoryDao = dicSearchDto.categoryDaoList[i][0]
                if dic1.categoryUid == 4 {
                    //키즈
                    if dicSearchDto.categoryDaoList[i].count > 2 {
                        let dic1 : CategoryDao = dicSearchDto.categoryDaoList[i][1]
                        str1 = dic1.categoryName
                        let dic2 : CategoryDao = dicSearchDto.categoryDaoList[i][2]
                        str2 = dic2.categoryName
                    } else if dicSearchDto.categoryDaoList[i].count > 1 {
                        let dic1 : CategoryDao = dicSearchDto.categoryDaoList[i][1]
                        str1 = dic1.categoryName
                    }
                } else {
                    //남성 여성
                    str1 = dic1.categoryName
                    if dicSearchDto.categoryDaoList[i].count > 1  {
                        let dic2 : CategoryDao = dicSearchDto.categoryDaoList[i][dicSearchDto.categoryDaoList[i].count - 1]
                        str2 = dic2.categoryName
                    }
                }
                
                let obj = Keyword(0, "", str2, str1, 0)
                m_recommendKeywords?.append(obj)
            }
        }

        if dicSearchDto.modelList.count > 0 {
            // add header
            let obj = Keyword(0, "", "", "", 0)
            obj.isRecommendItem = true
            obj.recommend_type = "모델명"
            m_recommendKeywords?.append(obj)
            for i in 0 ..< dicSearchDto.modelList.count {
                let dicModel : SearchModelDto = dicSearchDto.modelList[i]
                let obj = Keyword(0, "", dicModel.modelName,"", 0)
                m_recommendKeywords?.append(obj)
            }
        }
        
        if dicSearchDto.tagWordList.count > 0 {
            // add header
            let obj = Keyword(0, "", "", "", 0)
            obj.isRecommendItem = true
            obj.recommend_type = "태그"
            m_recommendKeywords?.append(obj)
            for i in 0 ..< dicSearchDto.tagWordList.count {
                let obj = Keyword(0, "", dicSearchDto.tagWordList[i],"", 0)
                m_recommendKeywords?.append(obj)
            }
        }
        m_tbList.reloadData()
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == m_tfKeyword {
            if m_tfKeyword.text == "" {
                showHistorySearchList()
            }
        }
    }    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        onBtnSearch(self)
        
        return true
    }
}

extension SearchViewController {
    
    //키보드생길때
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.vwContentBottom.constant = keyboardSize.height
                    self.vwClose.isHidden = false
                    self.lcContentBottom.constant = keyboardSize.height + self.vwClose.frame.size.height
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    //키보드없어질때
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.vwContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.lcContentBottom.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
}

extension SearchViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch m_searchState {
        case 0: // HistorySearch
            return (m_historyKeywords?.count)! + 1
        case 1: //Recommend
            if m_recommendKeywords?.count == 0 {
                return 2
            } else {
                return (m_recommendKeywords?.count)!
            }
        default: //PopularSearch
            return arrFameList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(m_searchState) {
        case 0: // HistorySearch
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListTopCell", for: indexPath) as! ListTopCell //최근검색페지일때
                cell.delegate = self
                cell.m_lbTopLabel.text = "최근 검색"
                cell.m_btnRemove.isHidden = false
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PopularKeywordCell", for: indexPath) as! PopularKeywordCell
                cell.delegate = self
                cell.keyword.text = m_historyKeywords![indexPath.row - 1].keyword_name
                cell.visit_count.text = String(format:"%d items", m_historyKeywords![indexPath.row - 1].visit_count)
                cell.metaInfo.isHidden = true
                return cell
            }
        case 1: //Recommend
            if m_recommendKeywords?.count == 0 {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ListTopCell", for: indexPath) as! ListTopCell
                    cell.delegate = self
                    cell.m_lbTopLabel.text = "검색"
                    cell.m_btnRemove.isHidden = true
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "RecommendNoneCell", for: indexPath) as! RecommendNoneCell
                    cell.delegate = self
                    cell.m_lblGuide.text = String(format:"\"%@\"로 검색하기", m_tfKeyword.text!)
                    return cell
                }
            } else {
                if m_recommendKeywords?[indexPath.row].isRecommendItem == true { //RecommendLabel
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ListTopCell", for: indexPath) as! ListTopCell
                    cell.delegate = self
                    cell.m_lbTopLabel.text = m_recommendKeywords?[indexPath.row].recommend_type
                    cell.m_btnRemove.isHidden = true
                    return cell
                } else { // Keyword Item
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "PopularKeyword1Cell", for: indexPath) as! PopularKeyword1Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PopularKeywordCell", for: indexPath) as! PopularKeywordCell
                    cell.delegate = self
                    cell.keyword.text = m_recommendKeywords![indexPath.row].keyword_name
                    cell.metaInfo.text = m_recommendKeywords![indexPath.row].keyword_name1
                    cell.metaInfo.isHidden = false
                    cell.visit_count.text = String(format:"%d views", m_recommendKeywords![indexPath.row].visit_count)
                    return cell
                }
            }
        default: //PopularSearch
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ListTopCell", for: indexPath) as! ListTopCell
                cell.delegate = self
                cell.m_lbTopLabel.text = "인기 검색"
                cell.m_btnRemove.isHidden = true
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PopularKeywordCell", for: indexPath) as! PopularKeywordCell
                
                let dic : SearchWordDto = arrFameList[indexPath.row]
                cell.delegate = self
                cell.keyword.text = dic.word
                cell.visit_count.text = String(format:"%d views", dic.count)
                cell.metaInfo.isHidden = true
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch(m_searchState) {
        case 0: // HistorySearch
            if(indexPath.row == 0) {
                return 53
            } else {
                return 63
            }
        case 1: //Recommend
            if(m_recommendKeywords?.count == 0) {
                if(indexPath.row == 0) {
                    return 53
                } else {
                    return 63
                }
            } else {
                if(m_recommendKeywords?[indexPath.row].isRecommendItem == true) { //RecommendLabel
                    return 53
                } else { // Keyword Item
                    return 63
                }
            }
        default: //PopularSearch
            if(indexPath.row == 0) {
                return 53
            } else {
                return 63
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension SearchViewController: ListTopCellDelegate {
    func ListTopCellDelegate(_ listTopCell : ListTopCell, onBtnRemoveHistoryKeyList sender : Any, tag: Int) {
        DBManager.getSharedInstance().deleteKeyword()
        m_historyKeywords?.removeAll()
        m_tbList.reloadData()
    }
}

extension SearchViewController: PopularKeywordCellDelegate {
    func PopularKeywordCellDelegate(_ popularKeywordCell : PopularKeywordCell, onBtnGo2SearchResult sender : Any, tag: String!) {
        go2SearchResultPage(tag)
    }
}

extension SearchViewController: PopularKeyword1CellDelegate {
    func PopularKeyword1CellDelegate(_ popularKeywordCell: PopularKeyword1Cell, onBtnGo2SearchResult sender: Any, tag: String!) {
        go2SearchResultPage(tag)
    }
}

extension SearchViewController: RecommendNoneCellDelegate {
    func RecommendNoneCellDelegate(_ recommendNoneCell : RecommendNoneCell, onBtnGo2SearchResult sender : Any, tag: Int?) {
        onBtnSearch(self)
    }
}

