//
//  LikeViewController.swift
//  selluv
//  좋아요목록
//  Created by Gambler on 12/12/17.
//  modified by PJH on 10/03/18.

import UIKit

class LikeViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    @IBOutlet weak var clvT1: UICollectionView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    
    @IBOutlet weak var lbAllCnt: UILabel!
    @IBOutlet weak var vwMenuBar: UIView!
    
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnLeft: UIButton!
    
    var isStyleMode = false
    
    var arrLikeList     : Array<FeedPdtDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!
    var nSale           : Int = 0

    //zoom효과관련
    weak var selectedImageView : UIImageView?
    var id : Int?
    
    var animator : ARNTransitionAnimator?
    
    //collectionView관련
    
    enum ETab : Int {
        case T1 = 0
        case T2
        case T3
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    var isSaleShow : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
        getLikeList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        // Collection view attributes
        clvT1.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        clvT1.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        clvT1.collectionViewLayout = layout
        
        let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
        clvT1.register(viewNib, forCellWithReuseIdentifier: "cell")
        
    }
    
    func scrollUp() {
        if scrollDirection == .up {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
        })
    }
    
    func scrollDown() {
        if scrollDirection == .down {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -3 * self.vwTop.frame.height))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -130))
        })
    }
    
    func showInteractive() {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = self.id!
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnStyle(_ sender: UIButton) {
        
        let dic : FeedPdtDto = arrLikeList[sender.tag]
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid)
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            
            cell = clvT1.cellForItem(at: indexPath) as! MainItemCVC
            scrollPos = clvT1.contentOffset.y
            
            let dic         : FeedPdtDto   = arrLikeList[indexPath.row]
            let dicPdt      : PdtMiniDto  = dic.pdt  //상품
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
        }
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        let dic : FeedPdtDto = arrLikeList[sender.tag]

        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : FeedPdtDto = arrLikeList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnProduct(_ sender : UIButton) {
        if let path = sender.withValue {
            let indexPath = path["path"] as! IndexPath
            
            if isStyleMode {
                selectedImageView = nil
            } else {
                let cell = clvT1.cellForItem(at: indexPath) as! MainItemCVC
                selectedImageView = cell._imgCom
            }
            
            pushVC("DETAIL_VIEW", storyboard: "Detail", animated: true)
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        btnRight.isSelected = !btnRight.isSelected
        isStyleMode = btnRight.isSelected
        if isStyleMode {
            getLikeStyleList()
        } else {
            getLikeList()
        }
        clvT1.reloadData()
    }
    
    @IBAction func saleAction(_ sender: Any) {
        isSaleShow = !isSaleShow
        btnLeft.isSelected = isSaleShow
        if isSaleShow {
            nSale = 1
        } else {
            nSale = 0
        }
        
        if isStyleMode {
            getLikeStyleList()
        } else {
            getLikeList()
        }
        clvT1.reloadData()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        clvT1.scrollToTop(isAnimated: true)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    //좋아요 목록 얻기
    func getLikeList() {
        
        gProgress.show()
        Net.getLikeList(
            accessToken     : gMeInfo.token,
            sale            : nSale,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.GetLikeListResult
                self.getLikeListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //좋아요 목록 얻기 결과
    func getLikeListResult(_ data: Net.GetLikeListResult) {
        
        isLast = data.last
        lbAllCnt.text = "(" + String(data.totalElements) + ")"
        if nPage == 0 {
            arrLikeList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : FeedPdtDto = data.list[i]
            arrLikeList.append(dic)
        }
        clvT1.reloadData()
        
    }
    
    //좋아요 스타일 목록 얻기
    func getLikeStyleList() {
        
        gProgress.show()
        Net.getLikeStyleList(
            accessToken     : gMeInfo.token,
            sale            : nSale,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.GetLikeStyleResult
                self.getLikeStyleListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //좋아요 스타일 목록 얻기 결과
    func getLikeStyleListResult(_ data: Net.GetLikeStyleResult) {
        
        isLast = data.last
        lbAllCnt.text = "(" + String(data.totalElements) + ")"
        if nPage == 0 {
            arrLikeList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : FeedPdtDto = data.list[i]
            arrLikeList.append(dic)
        }
        clvT1.reloadData()
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                //                self.showHeart(self, status, cell, pos)
                if self.isStyleMode {
                    self.getLikeStyleList()
                } else {
                    self.getLikeList()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                //                self.showHeart(self, status, cell, pos)
                if self.isStyleMode {
                    self.getLikeStyleList()
                } else {
                    self.getLikeList()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            self.pushVC("DETAIL_VIEW", storyboard: "Detail", animated: true)
        }
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension LikeViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            break
            
            
        case clvT1:
            
            self.scrollDirection = EScrollDirection.none
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvT1:
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }
            
            scrollPos = curScrollPos
            
        default:
            return
        }
    }
    
}

extension LikeViewController : UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC
        
        if indexPath.row % 2 == 0 {
            id = 1
        } else {
            id = 2
        }
        
        selectedImageView = cell._imgCom
        
        if isStyleMode {
            selectedImageView = nil
            let dic : FeedPdtDto = arrLikeList[indexPath.row]
            let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
            getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid)
        } else {
            pushVC("DETAIL_VIEW", storyboard: "Detail", animated: true)
        }
        
    }
}

extension LikeViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLikeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC
        
        let dic : FeedPdtDto = arrLikeList[indexPath.row]
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
        let blikeStatus     : Bool               = dic.pdtLikeStatus  //유저 상품좋아요 상태
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if btnRight.isSelected {
            cell._btnStyle.isHidden = true
            cell._imgEffect.isHidden = true
            cell._imgCom.kf.setImage(with: URL(string: dicPdtStyle.styleImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
                height = width * ratio
            }

        } else {
            cell._btnStyle.isHidden = false
            cell._imgEffect.isHidden = false
            cell._imgCom.kf.setImage(with: URL(string: dicPdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell._btnStyle.kf.setImage(with: URL(string: dicPdtStyle.styleImg), for: .normal, placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
                height = width * ratio
            }
        }
//        let width = (collectionView.bounds.width - 15) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!

        if dicPdt.originPrice <= dicPdt.price {
            cell.hiddenDown(price: dicPdt.price)
        } else {
            cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
        }
        cell.lbSize.text = dicPdt.pdtSize
        
        cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
        
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
        cell._lbBrand.text = dicBrand.nameEn
        cell._lbLike.text = String(likeCnt)
        cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
        //        cell._imgComWidth.constant = width
        cell._imgComHeight.constant = height
        
        //user
        cell._lbName.text = dicUsr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
        
//        cell._btnStyle.isHidden = isStyleMode
//        cell._imgEffect.isHidden = isStyleMode
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: .touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: .touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: .touchUpInside)
        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: .touchUpInside)
        cell._btnProduct.withValue = ["path" : indexPath]
        cell._btnProduct.addTarget(self, action: #selector(self.onBtnProduct(_:)), for: .touchUpInside)
        
        //더보기
        if indexPath.row == arrLikeList.count - 1 && !isLast {
            nPage = nPage + 1
            if isStyleMode {
                getLikeStyleList()
            } else {
                getLikeList()
            }
        }
        return cell
    }
}

extension LikeViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
//        let width = (collectionView.bounds.width - 15) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
//
//        return CGSize(width: width, height: height + 153)
        let dic : FeedPdtDto = arrLikeList[indexPath.row]
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if btnRight.isSelected {
            if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
                height = width * ratio
            }
        } else {
            if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
                height = width * ratio
            }
        }
        
        if dicPdt.originPrice <= dicPdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 119
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 119)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StyleGridHeaderView", for: indexPath as IndexPath)
    }
    
}

extension LikeViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return selectedImageView ?? UIImageView()
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        guard let selectedImageView = selectedImageView else { return CGRect.zero }
        return selectedImageView.convert(selectedImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        selectedImageView?.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        selectedImageView?.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        selectedImageView?.isHidden = false
    }
}
