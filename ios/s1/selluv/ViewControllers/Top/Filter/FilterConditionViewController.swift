//
//  FilterConditionViewController.swift
//  selluv
//  컨디션
//  Created by Comet on 12/16/17.
//  modified by PJH on 31/03/18.

import UIKit

class FilterConditionViewController: BaseViewController {

    @IBOutlet weak var _vwConditionGuide: UIView!
    
    @IBOutlet weak var _imvNewSelectOn: UIImageView!
    @IBOutlet weak var _imvLikeNewSelectOn: UIImageView!
    @IBOutlet weak var _imvBestSelectOn: UIImageView!
    @IBOutlet weak var _imvGoodSelectOn: UIImageView!
    @IBOutlet var _lbResultCnt: UILabel!
    var m_condition     : [Condition] = []
    var arrCondNames    : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        _vwConditionGuide.setRoundRect(radius: 5.0)
        _vwConditionGuide.setBorder(color: UIColor(hex:0xc0c0c0), width: 1.0)
//        m_condition = self.dic["condition"] as! [Condition]
//        markupSelectedItems()
        
        getSearchcount()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func markupSelectedItems() {
        m_condition.forEach { (condition) in
            switch(condition) {
            case Condition.new:
                _imvNewSelectOn.isHidden = false
                break
            case Condition.likenew:
                _imvLikeNewSelectOn.isHidden = false
                break
            case Condition.best:
                _imvBestSelectOn.isHidden = false
                break
            case Condition.good:
                _imvGoodSelectOn.isHidden = false
                break
            default:
                break
            }
        }
    }
    
    func removeItem(value : String) {
        for i in 0..<arrCondNames.count {
            if arrCondNames[i] == value {
                arrCondNames.remove(at: i)
                
                switch(value) {
                case "NEW":
                    _imvNewSelectOn.isHidden = true
                    break
                case "BEST":
                    _imvLikeNewSelectOn.isHidden = true
                    break
                case "GOOD":
                    _imvBestSelectOn.isHidden = true
                    break
                case "MIDDLE":
                    _imvGoodSelectOn.isHidden = true
                    break
                default:
                    break
                }
                return
            }
        }
    }

    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_CONDITION_CHANGE), object: m_condition))
        popVC()
    }
    @IBAction func onBtnReset(_ sender: Any) {
        
        arrCondNames.removeAll()
        
        _imvNewSelectOn.isHidden = false
        _imvLikeNewSelectOn.isHidden = false
        _imvBestSelectOn.isHidden = false
        _imvGoodSelectOn.isHidden = false
        
        gSearchInfo.setPdtConditionList(arr: arrCondNames)
        getSearchcount()
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC(-2)
    }
    
    @IBAction func onBtnNew(_ sender: Any) {
        _imvNewSelectOn.isHidden = !(_imvNewSelectOn.isHidden)
        if(_imvNewSelectOn.isHidden == false) {
            m_condition.append(Condition.new)
            arrCondNames.append("NEW")
        } else {
            m_condition.remove(at: m_condition.index(of: Condition.new)!)
            removeItem(value: "NEW")
        }
        gSearchInfo.setPdtConditionList(arr: arrCondNames)
        getSearchcount()
    }
    
    @IBAction func onBtnLikeNew(_ sender: Any) {
        _imvLikeNewSelectOn.isHidden = !(_imvLikeNewSelectOn.isHidden)
        if(_imvLikeNewSelectOn.isHidden == false) {
            m_condition.append(Condition.likenew)
            arrCondNames.append("BEST")
        } else {
            m_condition.remove(at: m_condition.index(of: Condition.likenew)!)
            removeItem(value: "BEST")
        }
        gSearchInfo.setPdtConditionList(arr: arrCondNames)
        getSearchcount()
    }
    
    @IBAction func onBtnBest(_ sender: Any) {
        _imvBestSelectOn.isHidden = !(_imvBestSelectOn.isHidden)
        if(_imvBestSelectOn.isHidden == false) {
            m_condition.append(Condition.best)
            arrCondNames.append("GOOD")
        } else {
            m_condition.remove(at: m_condition.index(of: Condition.best)!)
            removeItem(value: "GOOD")
        }
        gSearchInfo.setPdtConditionList(arr: arrCondNames)
        getSearchcount()
    }
    
    @IBAction func onBtnGood(_ sender: Any) {
        _imvGoodSelectOn.isHidden = !(_imvGoodSelectOn.isHidden)
        if(_imvGoodSelectOn.isHidden == false) {
            m_condition.append(Condition.good)
            arrCondNames.append("MIDDLE")
        } else {
            m_condition.remove(at: m_condition.index(of: Condition.good)!)
            removeItem(value: "MIDDLE")
        }
        gSearchInfo.setPdtConditionList(arr: arrCondNames)
        getSearchcount()
    }
    
    @IBAction func onBtnConditionGuide(_ sender: Any) {
        CondGuide.show(self)
    }

    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self._lbResultCnt.text = "검색결과 보기 ( " + String(res.count) + " )"
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
