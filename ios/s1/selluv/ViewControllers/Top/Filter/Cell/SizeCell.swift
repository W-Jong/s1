//
//  SizeCell.swift
//  selluv
//
//  Created by Comet on 12/19/17.
//

import UIKit

protocol SizeCellDelegate {
    func SizeCellDelegate(_ sizeCell : SizeCell, sizeSelected sender : Any, tag: Int?)
}
class SizeCell: UITableViewCell {

    var delegate:  SizeCellDelegate?
    @IBOutlet weak var _lbSize: UILabel!
    
    @IBOutlet weak var _lbItemCount: UILabel!
    @IBOutlet weak var _imvSelectOn: UIImageView!
    @IBOutlet var _btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
