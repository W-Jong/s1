//
//  ColorCell.swift
//  selluv
//
//  Created by Comet on 12/15/17.
//

import UIKit

protocol ColorCellDelegate {
    func ColorCellDelegate(_ colorCell : ColorCell, onItemClicked sender : Any, tag: Int!)
}

class ColorCell: UICollectionViewCell {
    
    var delegate:  ColorCellDelegate?
    
    @IBOutlet weak var _imvColor: UIImageView!
    @IBOutlet weak var _lbColor: UILabel!
    @IBOutlet weak var _imvSelectOn: UIImageView!
    @IBOutlet weak var _btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _imvColor.layer.cornerRadius = 23.5
        _imvColor.layer.borderWidth = 1
    }

    @IBAction func onItemClicked(_ sender: Any) {
        if(delegate != nil) {
            delegate?.ColorCellDelegate(self, onItemClicked: sender, tag: tag)
        }
    }
}
