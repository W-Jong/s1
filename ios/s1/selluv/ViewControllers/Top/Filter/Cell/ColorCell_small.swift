//
//  ColorCell_small.swift
//  selluv
//
//  Created by Comet on 12/21/17.
//

import UIKit

class ColorCell_small: UICollectionViewCell {

    @IBOutlet weak var _imvColor: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _imvColor.layer.cornerRadius = 10
        _imvColor.layer.borderWidth = 1
    }

}
