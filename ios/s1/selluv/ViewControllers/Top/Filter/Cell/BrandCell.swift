//
//  BrandCell.swift
//  selluv
//
//  Created by Comet on 12/15/17.
//

import UIKit

protocol BrandCellDelegate {
    func BrandCellDelegate(_ brandCell : BrandCell, onBrandSelected sender : Any, tag: Int?)
}

class BrandCell: UITableViewCell {

    var delegate:  BrandCellDelegate?
    var indexPath : IndexPath?
    
    @IBOutlet weak var _lbBrandFirstName: UILabel!
    
    @IBOutlet weak var _lbBrandSecondName: UILabel!
    
    @IBOutlet weak var _imvSelectOn: UIImageView!
    
    @IBOutlet weak var _btnSelect: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    @IBAction func onItemClicked(_ sender: Any) {
//        
//        if _btnSelect.isSelected {
//            _btnSelect.isSelected = false
//            _imvSelectOn.image = UIImage.init(named: "ic_brand_select_off")
//        } else {
//            _btnSelect.isSelected = true
//            _imvSelectOn.image = UIImage.init(named: "ic_brand_select_on")
//        }
//        
//        if(delegate != nil){
//            delegate?.BrandCellDelegate(self, onBrandSelected: self, tag: self.tag)
//        }
//    }
    
    func setIndexPath (_ path : IndexPath) {
        self.indexPath = path
    }
    
    func getIndexPath () -> IndexPath? {
        return indexPath
    }
    
}
