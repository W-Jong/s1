//
//  SizeFooterCell.swift
//  selluv
//
//  Created by Comet on 12/19/17.
//

import UIKit

protocol SizeFooterCellDelegate {
    func SizeFooterCellDelegate(_ sizeFooterCell : SizeFooterCell, onBtnSizeGuide sender : Any, tag: Int?)
}
class SizeFooterCell: UITableViewCell {

    var delegate:  SizeFooterCellDelegate?
    @IBOutlet weak var _vwSizeGuide: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _vwSizeGuide.setRoundRect(radius: 5.0)
        _vwSizeGuide.setBorder(color: UIColor(hex:0xc0c0c0), width: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func onBtnSizeGuide(_ sender: Any) {
        if(delegate != nil){
            delegate?.SizeFooterCellDelegate(self, onBtnSizeGuide: sender, tag: self.tag)
        }
    }
    
    
}
