//
//  CategoryCell.swift
//  selluv
//
//  Created by Comet on 12/25/17.
//

import UIKit

class CategoryCell: BaseViewController {

    @IBOutlet weak var _lbCategoryName: UILabel!
    @IBOutlet weak var _lbCategoryCount: UILabel!
    @IBOutlet weak var _imvSelectOn: UIImageView!
    @IBOutlet weak var _btnItem: UIButton!
    @IBOutlet weak var _cstrCatNameLeading: NSLayoutConstraint!
    
    var categoryDepth: Int! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
