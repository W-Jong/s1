//
//  FilterSizeViewController.swift
//  selluv
//  사이즈
//  Created by Comet on 12/16/17.
//  modified by PJH on 31/03/18.

import UIKit

class FilterSizeViewController: BaseViewController {
    
    @IBOutlet weak var _tblSize: UITableView!
    @IBOutlet var _lbResultCnt: UILabel!
    var arrSize = [String]()
    
    let sizeLabelArray = ["XXS", "XS", "S", "M", "L", "XL", "XXXL"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
        getSearchcount()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initUI() {
        _tblSize.register(UINib.init(nibName: "SizeFooterCell", bundle: nil), forCellReuseIdentifier: "SizeFooterCell")
        _tblSize.register(UINib.init(nibName: "SizeCell", bundle: nil), forCellReuseIdentifier: "SizeCell")
        
        _tblSize.estimatedSectionFooterHeight = 80
        _tblSize.estimatedSectionFooterHeight = UITableViewAutomaticDimension
        _tblSize.estimatedRowHeight = 40
        _tblSize.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    @objc func onBtnCell(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var sizeCell : SizeCell!
            
            sizeCell = _tblSize.cellForRow(at: indexPath) as! SizeCell

            if sizeCell._imvSelectOn.isHidden {
                arrSize.append(sizeLabelArray[indexPath.row])
            } else {
                var pos = -1
                for i in 0..<arrSize.count {
                    if sizeLabelArray[indexPath.row] == arrSize[i] {
                        pos = i
                    }
                }
                if pos > -1 {
                    arrSize.remove(at: pos)
                }
            }
            sizeCell._imvSelectOn.isHidden = !(sizeCell._imvSelectOn.isHidden)
            _tblSize.reloadData()
            
            gSearchInfo.setPdtSizeList(arr: arrSize)
            getSearchcount()
        }
    }
    
    func isCompareSize(size : String) -> Bool {
        
        var result  = false
        
        for i in 0..<gSearchInfo.getPdtSizeList().count {
            if gSearchInfo.getPdtSizeList()[i] == size {
                result =  true
                break
            }
        }
        
        return result
    }

    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func onBtnBack(_ sender: Any) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_SIZE_CHANGE), object: arrSize))
        popVC()
    }
    
    @IBAction func onBtnReset(_ sender: Any) {
        
        arrSize.removeAll()
        var sizeCell : SizeCell!
        
        for i in 0..<sizeLabelArray.count {
            let indexPath = IndexPath(row: i, section: 0)
            sizeCell = _tblSize.cellForRow(at: indexPath) as! SizeCell
            sizeCell._imvSelectOn.isHidden = true
        }
        _tblSize.reloadData()

        gSearchInfo.setPdtSizeList(arr: arrSize)
        getSearchcount()
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC(-2)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self._lbResultCnt.text = "검색결과 보기 ( " + String(res.count) + " )"
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }

}

extension FilterSizeViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sizeLabelArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SizeFooterCell") as! SizeFooterCell
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SizeCell", for: indexPath) as! SizeCell
        cell.tag = indexPath.row
        
        cell._lbSize.text = sizeLabelArray[indexPath.row]
        if  isCompareSize(size: sizeLabelArray[indexPath.row]){
            cell._imvSelectOn.isHidden = false
        } else {
            cell._imvSelectOn.isHidden = true
        }
        
        cell._btnSelect.withValue = ["index" : indexPath]
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnCell(_:)), for: UIControlEvents.touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
}

//extension FilterSizeViewController: SizeCellDelegate {
//    func SizeCellDelegate(_ sizeCell: SizeCell, sizeSelected sender: Any, tag: Int?) {
//        if sizeCell._imvSelectOn.isHidden {
//            arrSize.append(sizeLabelArray[tag!])
//        } else {
//            arrSize.remove(at: tag!)
//        }
//
//        sizeCell._imvSelectOn.isHidden = !(sizeCell._imvSelectOn.isHidden)
//    }
//}

extension FilterSizeViewController: SizeFooterCellDelegate {
    func SizeFooterCellDelegate(_ sizeFooterCell: SizeFooterCell, onBtnSizeGuide sender: Any, tag: Int?) {
//        pushVC("FILTER_SIZEGUIDE_VIEW", storyboard: "Top", animated: true)
        let storyboard = UIStoryboard(name: "Top", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FILTER_SIZEGUIDE_VIEW")
        self.present(controller, animated: true, completion: nil)

    }
}

