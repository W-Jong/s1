//
//  FilterViewController.swift
//  selluv
//  상세 검색
//  Created by Comet on 12/14/17.
//  modified by PJH on 31/03/18.

import UIKit
import UserNotifications

class FilterViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    var filterCondition : FilterCondition? = FilterCondition()

    //정렬
    @IBOutlet weak var m_lbSortType: UILabel!
    @IBOutlet var _btnSorttype: UIButton!
    @IBOutlet var vwSort: UIView!
    
    //카테고리
    @IBOutlet weak var m_lbCategory: UILabel!
    @IBOutlet var _btnCategory: UIButton!
    @IBOutlet var vwCategory: UIView!
    
    //브렌드
    @IBOutlet weak var m_lbBrand: UILabel!
    
    //사이즈
    @IBOutlet weak var m_lbSize: UILabel!
    
    //컨디션
    @IBOutlet weak var m_lbCondition: UILabel!
    
    //컬러
    @IBOutlet weak var m_cvColorList: UICollectionView!
    @IBOutlet weak var m_lbPriceLeft: UILabel!
    @IBOutlet weak var m_lbPriceRight: UILabel!
    @IBOutlet weak var m_lbPriceMin: UILabel!
    @IBOutlet weak var m_lbPriceMax: UILabel!
    @IBOutlet weak var m_lbResultCount: UILabel!
    @IBOutlet weak var m_vwPrice: RangeSlider!
    @IBOutlet weak var m_swtSoldOut: UISwitch!
    
    var m_minPrice : CGFloat = 0
    var m_maxPrice : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        m_vwPrice.addTarget(self, action: #selector(self.onChangePrice(_:)), for: .valueChanged)
        
        initUI()
//        drawFilterCondition()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        m_vwPrice.setNeedsDisplay()
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initUI() {
        
        self.m_cvColorList.dataSource  = self
        self.m_cvColorList.delegate = self
        self.m_cvColorList.layer.transform = CATransform3DConcat(self.m_cvColorList.layer.transform, CATransform3DMakeRotation(CGFloat.pi, 0.0, 1.0, 0.0))
        
        //Register nibs
        registerNibs()

        setSettingData()
    }
    
    func setSettingData() {
        getSearchcount()
        
        if !gSearchInfo.getEditOrderType() {
            _btnSorttype.isEnabled = false
            vwSort.backgroundColor = UIColor(hex: 0xF8F8FA)
            vwSort.alpha = 0.7
            m_lbSortType.text = CommonUtil.getNameFromStortType(data: gSearchInfo.getSearchOrderType())
        }
        
        if !gSearchInfo.getEditCategory() {
            _btnCategory.isEnabled = false
            vwCategory.backgroundColor = UIColor(hex: 0xF8F8FA)
            vwCategory.alpha = 0.7
            m_lbCategory.text = gSearchInfo.getCategoryName()
        }
        
        m_lbBrand.text = gSearchInfo.getBrandName()
        m_lbSize.text = gSearchInfo.getPdtSizeName()
        m_lbCondition.text = gSearchInfo.getPdtCondtionName()
        m_swtSoldOut.isOn = false
        m_lbCategory.text = gSearchInfo.getCategoryName()
        filterCondition?.colorType = []
        m_cvColorList.reloadData()
    }
    
    // Register CollectionView Nibs
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "ColorCell_small", bundle: nil)
        m_cvColorList.register(viewNib, forCellWithReuseIdentifier: "cell")
    }

    
    func addObserver(_index : Int) {
        switch _index {
        case 1:  //sort
            NotificationCenter.default.addObserver(self, selector: #selector(onLocalNotificationReceived), name: NSNotification.Name(rawValue: NOTI_SORTTYPE_CHANGE), object: nil)
            break
        case 2: //brand
            NotificationCenter.default.addObserver(self, selector: #selector(onLocalNotificationReceived), name: NSNotification.Name(rawValue: NOTI_BRAND_CHANGE), object: nil)
            break
        case 3: //cate
            NotificationCenter.default.addObserver(self, selector: #selector(onLocalNotificationReceived), name: NSNotification.Name(rawValue: NOTI_CONDITION_CHANGE), object: nil)
            break
        case 4:
            NotificationCenter.default.addObserver(self, selector: #selector(onLocalNotificationReceived), name: NSNotification.Name(rawValue: NOTI_COLOR_CHANGE), object: nil)
            break
        case 5:
            NotificationCenter.default.addObserver(self, selector: #selector(onLocalNotificationReceived), name: NSNotification.Name(rawValue: NOTI_SIZE_CHANGE), object: nil)
            break
        case 6:
            NotificationCenter.default.addObserver(self, selector: #selector(onLocalNotificationReceived), name: NSNotification.Name(rawValue: NOTI_CATEGORY_CHANGE), object: nil)
            break

        default:
            break
        }
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_SORTTYPE_CHANGE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_BRAND_CHANGE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_CONDITION_CHANGE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_COLOR_CHANGE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_SIZE_CHANGE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_CATEGORY_CHANGE), object: nil)
    }
    
    /**
     * Notification 수신 처리부
     */
    @objc func onLocalNotificationReceived(_ notification : Notification) {
        switch notification.name {
        case Notification.Name(NOTI_SORTTYPE_CHANGE as String) :
            let msg = notification.object as! SortType
            filterCondition?.sortType = msg
            drawFilterCondition()
        case Notification.Name(NOTI_BRAND_CHANGE as String) :
            let msg = notification.object as? [String]
            filterCondition?.brand = msg
            drawFilterCondition()
        case Notification.Name(NOTI_CONDITION_CHANGE as String) :
            let msg = notification.object as? [Condition]
            filterCondition?.condition = msg
            drawFilterCondition()
        case Notification.Name(NOTI_COLOR_CHANGE as String) :
            let msg = notification.object as? [ColorType]
            filterCondition?.colorType = msg!
            drawFilterCondition()
        case Notification.Name(NOTI_SIZE_CHANGE as String) :
            let msg = notification.object as? [String]
            filterCondition?.size = msg
            drawFilterSize()
        case Notification.Name(NOTI_CATEGORY_CHANGE as String) :
            let msg = notification.object as! String
            m_lbCategory.text = msg
        default:
            break
        }
        getSearchcount()
    }
    
    func drawFilterSize() {
        filterCondition?.size?.forEach({ (size) in
            m_lbSize.text = m_lbSize.text! + size + ", "
        })
        m_lbSize.text = (m_lbSize.text?.isEmpty)! ? "" : m_lbSize.text?.substring(to: (m_lbSize.text?.count)! - 2)
        gSearchInfo.setPdtSizeName(value: m_lbSize.text!)
    }
    
    func drawFilterCondition() {
        m_lbSortType.text = (filterCondition?.sortType).map { $0.rawValue }
        
        m_lbBrand.text = ""
        filterCondition?.brand?.forEach({ (brand) in
            m_lbBrand.text = m_lbBrand.text! + brand + ", "
        })
        m_lbBrand.text = !(m_lbBrand.text?.isEmpty)! ? m_lbBrand.text?.substring(to: (m_lbBrand.text?.count)! - 2) : ""
        gSearchInfo.setBrandName(value: m_lbBrand.text!)
        
        m_lbCondition.text = ""
        filterCondition?.condition?.forEach({ (condition) in
            m_lbCondition.text = m_lbCondition.text! + condition.rawValue + ", "
        })
        m_lbCondition.text = !(m_lbCondition.text?.isEmpty)! ? m_lbCondition.text?.substring(to: (m_lbCondition.text?.count)! - 2) : ""
        gSearchInfo.setPdtConditionName(value: m_lbCondition.text!)
        
        m_cvColorList.reloadData()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func onBtnclose(_ sender: Any) {        
        let acAlert = UIAlertController(title: "",
                                        message: NSLocalizedString("popup_close_alert_msg", comment:""),
                                        preferredStyle: .alert)
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            self.popVC()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        }))
        
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(acAlert, animated: true, completion: nil)
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC()
    }
    
    @IBAction func onBtnReset(_ sender: Any) {
        //필터 초기화처리
        var orderType = ""
        var categoryName = ""
        var categoryId = 0
        var bEditOdertType = true
        var bEditCategory = true
        
        if !gSearchInfo.getEditOrderType() {
            bEditOdertType = false
            orderType = gSearchInfo.getSearchOrderType()
        }
        
        if !gSearchInfo.getEditCategory() {
            bEditCategory = false
            categoryName = gSearchInfo.getCategoryName()
            categoryId = gSearchInfo.getCategoryUid()
        }
        
        //초기화하기전에 초기화되지 말아야 할 자료들을 미리 보관한다.
        gSearchInfo.initial()
        gSearchInfo.setEditOrderType(status: bEditOdertType)
        gSearchInfo.setEditCategory(status: bEditCategory)
        gSearchInfo.setCategoryUid(_uid: categoryId)
        gSearchInfo.setCategoryName(value: categoryName)
        gSearchInfo.setSearchOrderType(value: orderType)
        
        setSettingData()
    }
    
    @IBAction func onBtnSort(_ sender: Any) {
        addObserver(_index: 1)
        dic["sorttype"] = filterCondition?.sortType
        pushVC("FILTER_SORT_VIEW", storyboard: "Top", animated: true, dic:dic)
    }
    
    @IBAction func onBtnCategory(_ sender: Any) {
        addObserver(_index: 6)
        dic["brand"] = filterCondition?.category
        pushVC("FILTER_CATEGORY_VIEW", storyboard: "Top", animated: true, dic:dic)
    }
    
    @IBAction func onBtnBrand(_ sender: Any) {
        addObserver(_index: 2)
        dic["brand"] = filterCondition?.brand
        pushVC("FILTER_BRAND_VIEW", storyboard: "Top", animated: true, dic:dic)
    }
    
    @IBAction func onBtnSize(_ sender: Any) {
        addObserver(_index: 5)
        pushVC("FILTER_SIZE_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func onBtnCondition(_ sender: Any) {
        addObserver(_index: 3)
        dic["condition"] = filterCondition?.condition
        pushVC("FILTER_CONDITION_VIEW", storyboard: "Top", animated: true, dic:dic)
    }
    
    @IBAction func onBtnColor(_ sender: Any) {
        addObserver(_index: 4)
        dic["colortype"] = filterCondition?.colorType
        pushVC("FILTER_COLOR_VIEW", storyboard: "Top", animated: true, dic:dic)
    }
    
    @IBAction func onChangePrice(_ sender: Any) {
        print("Range slider value changed: (\(m_vwPrice.lowerValue) , \(m_vwPrice.upperValue))")
        var leftValue : CGFloat = 0
        var rightValue : CGFloat = 0
        leftValue = m_minPrice + (m_maxPrice - m_minPrice) * CGFloat(m_vwPrice.lowerValue)
        rightValue = m_minPrice + (m_maxPrice - m_minPrice) * CGFloat(m_vwPrice.upperValue)
        m_lbPriceLeft.text = String(format:"%@", getCommaStringFromInteger(Int(leftValue) / 1000 * 1000))
        m_lbPriceRight.text = String(format:"%@", getCommaStringFromInteger(Int(rightValue) / 1000 * 1000))
        
        gSearchInfo.setPriceMin(value: Int(leftValue))
        gSearchInfo.setPriceMax(value: Int(rightValue))
        getSearchcount()
    }
    
    func getCommaStringFromInteger(_ intValue : Int) -> String {
        let tmpStr : NSString = NSString(format:"%d", intValue)
        var resStr : String = ""
        var i = tmpStr.length
        while i > 0 {
            if(i != 0 && (tmpStr.length - i) != 0 &&  (tmpStr.length - i)%3 == 0) {
                resStr = resStr + ","
            }
            resStr = resStr + tmpStr.substring(with: NSRange((i-1)...(i-1)))
            i = i-1
        }
        
        return String(resStr.reversed())
    }
    
    @IBAction func onSoldOutPolicyChanged(_ sender: Any) {
        filterCondition?.isExcludingOnSales = !(filterCondition?.isExcludingOnSales)!
        
        gSearchInfo.setSoldOutExpect(status: (filterCondition?.isExcludingOnSales)!)
        getSearchcount()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self.m_lbResultCount.text = "검색결과 보기 ( " + String(res.count) + " )"
                
                self.getSearchPriceRange()
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //가격 최대값, 최소값 얻기
    func getSearchPriceRange() {
        
        gProgress.show()
        Net.getSearchPriceRange(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSearchPriceRangeResult
                self.m_minPrice = CGFloat(res.min)
                self.m_maxPrice = CGFloat(res.max)
                self.m_lbPriceMin.text = "" + CommonUtil.formatNum(Float(self.m_minPrice))
                self.m_lbPriceLeft.text = "" + CommonUtil.formatNum(Float(self.m_minPrice))
                self.m_lbPriceMax.text = "" + CommonUtil.formatNum(Float(self.m_maxPrice))
                self.m_lbPriceRight.text = "" + CommonUtil.formatNum(Float(self.m_maxPrice))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
   
}


extension FilterViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 20, height: 20)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (filterCondition?.colorType.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Create the cell and return the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ColorCell_small
        cell.tag = indexPath.row
        switch (filterCondition?.colorType[indexPath.row])! {
        case ColorType.black :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.image = nil
            
        case ColorType.grey :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3c3c3c).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4a4a4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.white :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xdddddd).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffffff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.beizy :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc4bab0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xf5e9dc).cgColor
            cell._imvColor.image = nil
            
        case ColorType.red :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc2900).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff3300).cgColor
            cell._imvColor.image = nil
            
        case ColorType.pink :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc868ba).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xfa82e8).cgColor
            cell._imvColor.image = nil
            
        case ColorType.purple :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x7e27cf).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x9b30ff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.blue :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3e75b3).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4e92e0).cgColor
            cell._imvColor.image = nil
            
        case ColorType.green :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x33b13b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x40dd4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.yellow :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xccac00).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffd700).cgColor
            cell._imvColor.image = nil
            
        case ColorType.orange :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc4a0b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff5d03).cgColor
            cell._imvColor.image = nil
            
        case ColorType.brown :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x6e4626).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x8a572f).cgColor
            cell._imvColor.image = nil
            
        case ColorType.gold :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_gold")
            
        case ColorType.silver :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_silver")
            
        case ColorType.multi :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_multi")
        }
        // Add image to cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}
