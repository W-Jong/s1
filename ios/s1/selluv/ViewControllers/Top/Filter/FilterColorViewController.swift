//
//  FilterColorViewController.swift
//  selluv
//  컬러
//  Created by Comet on 12/15/17.
//  modified by PJH on 31/03/18.

import UIKit

class FilterColorViewController: BaseViewController {

    let colorLabelArray : [ColorType]  = [ColorType.black, ColorType.grey, ColorType.white, ColorType.beizy, ColorType.red, ColorType.pink, ColorType.purple, ColorType.blue, ColorType.green, ColorType.yellow, ColorType.orange, ColorType.brown, ColorType.gold, ColorType.silver, ColorType.multi]
    
    @IBOutlet weak var _cvList: UICollectionView!
    @IBOutlet weak var _htBtnApply: NSLayoutConstraint!
    @IBOutlet var _lbResultCnt: UILabel!
    var m_selectedColorType : [ColorType] = []
    var arrColorNames       : [String] = []
    var m_bFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //
    // Helper functions
    //
    func initUI() {
        initData()
        self._cvList.dataSource  = self
        self._cvList.delegate = self
        //Register nibs
        registerNibs()
        
        if m_bFlag {
            _htBtnApply.constant = 0
        } else {
            _htBtnApply.constant = 45
        }
        
        getSearchcount()
    }
    
    func initData() {
        m_selectedColorType = self.dic["colortype"] as! [ColorType]
        m_bFlag = (self.dic["btnStatus"] != nil)
    }
    
    // Register CollectionView Nibs
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "ColorCell", bundle: nil)
        _cvList.register(viewNib, forCellWithReuseIdentifier: "cell")
    }
    
    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_COLOR_CHANGE), object: m_selectedColorType))
        popVC()
    }
    @IBAction func onBtnReset(_ sender: Any) {
        gSearchInfo.setPdtColorList(arr: [])
        
        m_selectedColorType.removeAll()
        arrColorNames.removeAll()
        _cvList.reloadData()
        
        getSearchcount()
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC(-2)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self._lbResultCnt.text = "검색결과 보기 ( " + String(res.count) + " )"
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}

extension FilterColorViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width)/4.0, height: (collectionView.frame.size.width)/4.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorLabelArray.count
    }
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Create the cell and return the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ColorCell
        cell._lbColor.text = colorLabelArray[indexPath.row].rawValue
        cell.delegate = self
        cell.tag = indexPath.row
        if((m_selectedColorType.index(of: colorLabelArray[indexPath.row])) != nil) {
            cell._imvSelectOn.isHidden = false
        } else {
            cell._imvSelectOn.isHidden = true
        }
        
        switch(colorLabelArray[indexPath.row]) {
        case ColorType.black :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.image = nil
            
        case ColorType.grey :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3c3c3c).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4a4a4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.white :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xdddddd).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffffff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.beizy :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc4bab0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xf5e9dc).cgColor
            cell._imvColor.image = nil
            
        case ColorType.red :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc2900).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff3300).cgColor
            cell._imvColor.image = nil
            
        case ColorType.pink :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc868ba).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xfa82e8).cgColor
            cell._imvColor.image = nil
            
        case ColorType.purple :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x7e27cf).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x9b30ff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.blue :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3e75b3).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4e92e0).cgColor
            cell._imvColor.image = nil
            
        case ColorType.green :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x33b13b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x40dd4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.yellow :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xccac00).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffd700).cgColor
            cell._imvColor.image = nil
            
        case ColorType.orange :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc4a0b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff5d03).cgColor
            cell._imvColor.image = nil
            
        case ColorType.brown :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x6e4626).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x8a572f).cgColor
            cell._imvColor.image = nil
            
        case ColorType.gold :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_gold")
            
        case ColorType.silver :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_silver")
            
        case ColorType.multi :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_multi")
        }
        // Add image to cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension FilterColorViewController: ColorCellDelegate {
    func ColorCellDelegate(_ colorCell : ColorCell, onItemClicked sender : Any, tag: Int!) {
        colorCell._imvSelectOn.isHidden = !colorCell._imvSelectOn.isHidden
        if(colorCell._imvSelectOn.isHidden == false) {
            m_selectedColorType.append(colorLabelArray[tag])
            arrColorNames.append(colorLabelArray[tag].rawValue)
        } else {
            let index = m_selectedColorType.index(of: colorLabelArray[tag])!
            m_selectedColorType.remove(at: index)
            arrColorNames.remove(at: index)
        }
        
        gSearchInfo.setPdtColorList(arr: arrColorNames)
        getSearchcount()

    }
}
