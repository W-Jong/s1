//
//  FilterSizeGuideViewController.swift
//  selluv
//
//  Created by Comet on 12/16/17.
//

import UIKit

class FilterSizeGuideViewController: BaseViewController {

    @IBOutlet weak var _imvSizeGuide_man: UIImageView!
    @IBOutlet weak var _imvSizeGuide_woman: UIImageView!
    @IBOutlet weak var _imvSizeGuide_baby: UIImageView!
    @IBOutlet weak var _imvSizeGuide_teen: UIImageView!
    @IBOutlet weak var _imvSizeGuide_kids: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadImage() {
        _imvSizeGuide_man.isHidden = true
        _imvSizeGuide_woman.isHidden = true
        _imvSizeGuide_kids.isHidden = true
        _imvSizeGuide_baby.isHidden = true
        _imvSizeGuide_teen.isHidden = true
        
        if gCategoryUid == 1 || (gCategoryUid > 4 && gCategoryUid < 10) {
            _imvSizeGuide_man.isHidden = false
        }
        
        if gCategoryUid == 2 || (gCategoryUid > 9 && gCategoryUid < 15) {
            _imvSizeGuide_woman.isHidden = false
        }

        if gCategoryUid == 4 || (gCategoryUid > 14 && gCategoryUid < 20) {
            if gCategoryUid == 15 {
                _imvSizeGuide_baby.isHidden = false
            } else if gCategoryUid == 19 {
                _imvSizeGuide_teen.isHidden = false
            } else {
                _imvSizeGuide_kids.isHidden = false
            }
        }
    }
    
    @IBAction func onBtnBack(_ sender: Any) {
//        popVC()
        dismiss(animated: false, completion: nil)
    }

   
}
