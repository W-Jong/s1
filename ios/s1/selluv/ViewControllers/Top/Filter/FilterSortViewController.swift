//
//  FilterSortViewController.swift
//  selluv
//  정렬
//  Created by Comet on 12/15/17.
//  modified by PJH on 31/03/18.

import UIKit

class FilterSortViewController: BaseViewController {

    @IBOutlet weak var m_imvCheckRecommend: UIImageView!
    @IBOutlet weak var m_imvCheckNewly: UIImageView!
    @IBOutlet weak var m_imvCheckPopular: UIImageView!
    @IBOutlet weak var m_imvCheckCheap: UIImageView!
    @IBOutlet weak var m_imvCheckExpensive: UIImageView!
    
    @IBOutlet var _btnResultCnt: UIButton!
    var sortType : SortType = SortType.recommendOrder
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sortType = self.dic["sorttype"] as! SortType
        setSelectedType()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func setSelectedType() {
        m_imvCheckRecommend.isHidden = true
        m_imvCheckNewly.isHidden = true
        m_imvCheckPopular.isHidden = true
        m_imvCheckCheap.isHidden = true
        m_imvCheckExpensive.isHidden = true
        switch sortType {
        case SortType.recommendOrder:
            m_imvCheckRecommend.isHidden = false
            break
        case SortType.newOrder:
            m_imvCheckNewly.isHidden = false
            break
        case SortType.popularOrder:
            m_imvCheckPopular.isHidden = false
            break
        case SortType.cheapOrder:
            m_imvCheckCheap.isHidden = false
            break
        case SortType.expensiveOrder:
            m_imvCheckExpensive.isHidden = false
            break
        default:
            m_imvCheckRecommend.isHidden = false
            break
        }
    }
  
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func onBtnBack(_ sender: Any) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_SORTTYPE_CHANGE), object: sortType))
        popVC()
    }
    
    @IBAction func onBtnRecommendOrder(_ sender: Any) {
        sortType = SortType.recommendOrder
        setSelectedType()
        gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[0])
        getSearchcount()
    }
    
    @IBAction func onBtnNewlyOrder(_ sender: Any) {
        sortType = SortType.newOrder
        setSelectedType()
        gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[1])
        getSearchcount()
    }
    
    @IBAction func onBtnPopularOrder(_ sender: Any) {
        sortType = SortType.popularOrder
        setSelectedType()
        gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[2])
        getSearchcount()
    }
    
    @IBAction func onBtnCheapOrder(_ sender: Any) {
        sortType = SortType.cheapOrder
        setSelectedType()
        gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[3])
        getSearchcount()
    }
    
    @IBAction func onBtnExpensiveOrder(_ sender: Any) {
        sortType = SortType.expensiveOrder
        setSelectedType()
        gSearchInfo.setSearchOrderType(value: CatrgoryTypeName[4])
        getSearchcount()
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC(-2)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self._btnResultCnt.setTitle("검색결과 보기 ( " + String(res.count) + " )", for: .normal)
                self._btnResultCnt.setTitle("검색결과 보기 ( " + String(res.count) + " )", for: .selected)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
  
}
