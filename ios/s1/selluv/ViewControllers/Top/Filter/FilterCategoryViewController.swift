//
//  FilterCategoryViewController.swift
//  selluv
//  카테고리
//  Created by Comet on 12/25/17.
//  modified by PJH on 31/03/18.

import UIKit

class FilterCategoryViewController: BaseViewController {

    @IBOutlet weak var _stvManList: UIStackView!
    @IBOutlet weak var _stvWomanList: UIStackView!
    @IBOutlet weak var _stvKidList: UIStackView!
    @IBOutlet weak var _cstrStvManListHeight: NSLayoutConstraint!
    @IBOutlet weak var _cstrStvWomanListHeight: NSLayoutConstraint!
    @IBOutlet weak var _cstrStvKidListHeight: NSLayoutConstraint!
    
    @IBOutlet weak var _imvManSelectOn: UIImageView!
    @IBOutlet weak var _imvWomanSelectOn: UIImageView!
    @IBOutlet weak var _imvKidSelectOn: UIImageView!
    @IBOutlet weak var _lbMan: UILabel!
    @IBOutlet weak var _lbWoman: UILabel!
    @IBOutlet weak var _lbKid: UILabel!
    @IBOutlet weak var _lbManCategoryCount: UILabel!
    @IBOutlet weak var _lbWomanCategoryCount: UILabel!
    @IBOutlet weak var _lbKidCategoryCount: UILabel!
    
    var _curStvList : UIStackView!                      //현재 선택된 UIStackView 지적자
    var _curCstrStvListHeight: NSLayoutConstraint!      //현재 선택된 stackview의 높이 지적자
    var _curImvSelected: UIImageView!                   //현재 선택된 cateogry의 image 지적자, man, woman, kid인 3개만을 지적한다.
    
    @IBOutlet var _lbResultCnt: UILabel!
    
    var nCurStep  = 1
    var arrCatList      : Array<CategoryDao> = []
    var listParents : [(String, Int, Int)]  = []  //name, count, uid
    var selectedCategory = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        _curStvList = _stvManList
        _curCstrStvListHeight = _cstrStvManListHeight
        _curImvSelected = _imvManSelectOn

        getSearchcount()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //
    // 제일 웃준위카테고리 색갈을 변경시키는 함수
    //
    func changeMainCategoryColor(_ type: String) {
        _lbMan.textColor = UIColor(hex:0x000000)
        _lbManCategoryCount.textColor = UIColor(hex:0x999999)
        _lbWoman.textColor = UIColor(hex:0x000000)
        _lbWomanCategoryCount.textColor = UIColor(hex:0x999999)
        _lbKid.textColor = UIColor(hex:0x000000)
        _lbKidCategoryCount.textColor = UIColor(hex:0x999999)
        switch type {
        case "man":
            _lbMan.textColor = UIColor(hex:0x42c2fe)
            _lbManCategoryCount.textColor = UIColor(hex:0x42c2fe)
            break
        case "woman":
            _lbWoman.textColor = UIColor(hex:0x42c2fe)
            _lbWomanCategoryCount.textColor = UIColor(hex:0x42c2fe)
            break
        case "kid":
            _lbKid.textColor = UIColor(hex:0x42c2fe)
            _lbKidCategoryCount.textColor = UIColor(hex:0x42c2fe)
            break
        default:
            break
        }
    }
    @IBAction func onBtnManList(_ sender: UIButton) {
        changeMainCategoryColor("man")
        _curStvList.subviews.forEach({ (view) in
            view.removeFromSuperview()
        })
        
        nCurStep = 1
        _curStvList = _stvManList
        _curCstrStvListHeight.constant = 0
        
        _curCstrStvListHeight = _cstrStvManListHeight
        
        _curImvSelected.isHidden = true
        _curImvSelected = _imvManSelectOn
        _curImvSelected.isHidden = false

        gSearchInfo.setCategoryUid(_uid: 1)
        gCategoryUid = 1
        getSubCategoryList(_uid: 1)
    }
    
    @IBAction func onBtnWomanList(_ sender: UIButton) {
        changeMainCategoryColor("woman")
        _curStvList.subviews.forEach({ (view) in
            view.removeFromSuperview()
        })
        
        nCurStep = 1
        _curStvList = _stvWomanList
        _curCstrStvListHeight.constant = 0
        
        _curCstrStvListHeight = _cstrStvWomanListHeight
        
        _curImvSelected.isHidden = true
        _curImvSelected = _imvWomanSelectOn
        _curImvSelected.isHidden = false

        gSearchInfo.setCategoryUid(_uid: 2)
        gCategoryUid = 2
        getSubCategoryList(_uid: 2)
    }
    
    @IBAction func onBtnKidList(_ sender: UIButton) {
        changeMainCategoryColor("kid")
        _curStvList.subviews.forEach({ (view) in
            view.removeFromSuperview()
        })
        
        nCurStep = 1
        
        _curStvList = _stvKidList
        _curCstrStvListHeight.constant = 0
        
        _curCstrStvListHeight = _cstrStvKidListHeight
        
        _curImvSelected.isHidden = true
        _curImvSelected = _imvKidSelectOn
        _curImvSelected.isHidden = false

        gSearchInfo.setCategoryUid(_uid: 4)
        gCategoryUid = 4
        getSubCategoryList(_uid: 4)
    }
    
    @IBAction func onItemClicked(_ sender: UIButton) {
        changeMainCategoryColor("none")
        
        _curStvList.subviews.forEach({ (view) in
            view.removeFromSuperview()
        })
        
        nCurStep = (sender.withValue!["depth"] as! Int) + 1
        
        _curImvSelected.isHidden = true

        listParents = (sender.withValue?["listParents"] as? [(String, Int, Int)])!

        let dic : CategoryDao = arrCatList[sender.tag - nCurStep + 1]
        gSearchInfo.setCategoryUid(_uid: dic.categoryUid)
        if dic.categoryUid < 20 {
            gCategoryUid = dic.categoryUid
        }
        getSubCategoryList(_uid: dic.categoryUid)
    }
    
    
    //
    //stackview를 그리기해주는 함수이다. 여기서 카테고리를 그려준다
    //
    
    /*
     list : 마지막 카테고리 리스트
     categoryDepth: 카테고리 단계를 나타내는 값
     listParents: 자기보다 웃단계의 카테고리 (계승구조)
     */
    func drawList() {
        
        for j in 0..<(nCurStep - 1 ) { // 여기는 categoryParent 즉 curList의 웃단계 category항목을 현시해준다.
            let vwProductItem = CategoryCell(nibName: "CategoryCell", bundle: Bundle.main)
            vwProductItem.view.translatesAutoresizingMaskIntoConstraints = false
            vwProductItem._lbCategoryName.text = listParents[j].0
//            vwProductItem._lbCategoryCount.text = String(format:"%d", (listParents![j].1))
            if( j == nCurStep - 2) {       //현재 선택된 카테고리이다.
                vwProductItem._lbCategoryName.textColor = UIColor(hex:0x42c2fe)
                vwProductItem._lbCategoryCount.textColor = UIColor(hex:0x42c2fe)
                vwProductItem._imvSelectOn.isHidden = false
                selectedCategory = vwProductItem._lbCategoryName.text!
            }
            
            vwProductItem.view.removeConstraint(vwProductItem._cstrCatNameLeading);
            
            vwProductItem._cstrCatNameLeading = NSLayoutConstraint(item: vwProductItem._lbCategoryName, attribute: .leading, relatedBy: .equal, toItem: vwProductItem.view, attribute: .leading, multiplier: 1.0, constant: CGFloat(15 + 23 * (j + 1)))
            vwProductItem.view.addConstraint(vwProductItem._cstrCatNameLeading)
            
            vwProductItem._btnItem.tag = j
            
            var params : [String: Any] = [:]
            params["depth"] = j
            
            var curListParent : [(String, Int, Int)] = []
            for k in 0..<j {
                curListParent.append(listParents[k])
            }
            
            params["listParents"] = curListParent
            params["selectedCategory"] = (listParents[j].0, listParents[j].1)
            vwProductItem._btnItem.withValue = params
            
            vwProductItem._btnItem.addTarget(self, action: #selector(onItemClicked(_:)), for: .touchUpInside)
            
            _curStvList.insertArrangedSubview(vwProductItem.view, at: j)
        }
        
        for i in 0..<arrCatList.count {   // 여기서 기본 카테고리를 현시해준다.
            let vwProductItem = CategoryCell(nibName: "CategoryCell", bundle: Bundle.main)
            let dicCat : CategoryDao = arrCatList[i]
            vwProductItem.view.translatesAutoresizingMaskIntoConstraints = false
            vwProductItem.view.removeConstraint(vwProductItem._cstrCatNameLeading);
            vwProductItem._cstrCatNameLeading = NSLayoutConstraint(item: vwProductItem._lbCategoryName, attribute: .leading, relatedBy: .equal, toItem: vwProductItem.view, attribute: .leading, multiplier: 1.0, constant: CGFloat(15 + 23 * nCurStep))
            vwProductItem.view.addConstraint(vwProductItem._cstrCatNameLeading)
            
            vwProductItem._lbCategoryName.text = dicCat.categoryName
//            vwProductItem._lbCategoryCount.text = String(format:"%d", list[i].1)
//            if(list[i].1 == 0) {
//                vwProductItem._lbCategoryName.textColor = UIColor(hex: 0xe1e1e1)
//                vwProductItem._lbCategoryCount.textColor = UIColor(hex: 0xe1e1e1)
//                vwProductItem._lbCategoryCount.text = ""
//            } else {
                vwProductItem._btnItem.tag = nCurStep + i
                
                var params : [String: Any] = [:]
                params["depth"] = nCurStep
                var curParents = listParents
                curParents.append((dicCat.categoryName, 0, dicCat.categoryUid))
                params["listParents"] = curParents
                params["selectedCategory"] = (dicCat.categoryName, 0)
                vwProductItem._btnItem.withValue = params
                vwProductItem._btnItem.addTarget(self, action: #selector(onItemClicked(_:)), for: .touchUpInside)
//            }
            
            _curStvList.insertArrangedSubview(vwProductItem.view, at: nCurStep - 1 + i)
        }
    }
    
    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_CATEGORY_CHANGE), object: selectedCategory))
        popVC()
    }
    
    @IBAction func onBtnReset(_ sender: Any) {
        
        gSearchInfo.setCategoryUid(_uid: 0)
        gCategoryUid = 0
        getSubCategoryList(_uid: 0)
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC(-2)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self._lbResultCnt.text = "검색결과 보기 ( " + String(res.count) + " )"
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 하위카테고리 목록 얻기
    func getSubCategoryList( _uid : Int) {
        
        gProgress.show()
        Net.getSubCategoryList(
            accessToken     : gMeInfo.token,
            categoryUid     : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSubCategoryListResult
                self.getSubCategoryListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //하위카테고리 목록 얻기 결과
    func getSubCategoryListResult(_ data: Net.getSubCategoryListResult) {
        arrCatList = data.list
        if nCurStep == 1 {
            _curCstrStvListHeight.constant = CGFloat(45 * arrCatList.count)
        } else {
            _curCstrStvListHeight.constant = CGFloat(45 * (arrCatList.count + nCurStep - 1))
        }
        drawList()
        getSearchcount()
    }
}
