//
//  FilterBrandViewController.swift
//  selluv
//  브렌드
//  Created by Comet on 12/15/17.
//  modified by PJH on 31/03/18.

import UIKit

class FilterBrandViewController: BaseViewController, STBTableViewIndexDelegate {

    @IBOutlet weak var m_tbBrandList: UITableView!
    @IBOutlet weak var m_tfKeyword: DesignableUITextField!
    @IBOutlet weak var _cstrPlaceHolderXPos: NSLayoutConstraint!
    @IBOutlet weak var m_lbPlaceholder: UILabel!
    @IBOutlet weak var m_vwPlaceholder: UIView!
    @IBOutlet weak var m_vwSearchBar: UIView!
    @IBOutlet var _lbResultCnt: UILabel!
    var m_selectedBrandList : [String]? = []
    var m_selectedBrandUids : [Int]? = []
    
    var indexView = STBTableViewIndex()
    
    var brandList : [[Bool]] = [[]]
    var arrBrandList    : Array<Brand> = []
    var arrBrandEn = [[Brand!]](repeating: [Brand](), count: index_eng.count)
    var arrBrandKo = [[Brand!]](repeating: [Brand](), count: index_kor.count)
    
    var arrBrandEn1 = [[Brand!]](repeating: [Brand](), count: index_eng.count)
    var arrBrandKo1 = [[Brand!]](repeating: [Brand](), count: index_kor.count)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        indexView.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        indexView.removeFromSuperview()
        indexView = STBTableViewIndex()
        indexView.titles = index_eng
        indexView.sections = index_eng
        indexView.delegate = self
        navigationController?.view.addSubview(indexView)
        
    }
    //
    // Helper functions
    //
    
    func initUI() {
        m_tbBrandList.delegate = self
        m_tbBrandList.dataSource = self
        m_tbBrandList.register(UINib.init(nibName: "BrandCell", bundle: nil), forCellReuseIdentifier: "BrandCell")
       
        navigationController?.view.addSubview(indexView)
        
        m_tfKeyword.delegate = self        
        m_tfKeyword.addTarget(self, action: #selector(self.searchTextChanged), for: .editingChanged)
        
        getAllBrandDetailList()
    }
    
    func getSearchResult(key : String) {
        
        arrBrandEn1 = [[Brand!]](repeating: [], count: index_eng.count)
        arrBrandKo1 = [[Brand!]](repeating: [], count: index_kor.count)
        if key == "" {
            arrBrandEn1 = arrBrandEn
            arrBrandKo1 = arrBrandKo
        } else {
         
            for i in 0..<index_eng.count {
                for k in 0..<arrBrandEn[i].count {
                    let dic : Brand = arrBrandEn[i][k]
                    if dic.nameEn.lowercased().contains(key) {
                        arrBrandEn1[i].append(dic)
                    }
                }
            }
        }
        m_tbBrandList.reloadData()
    }
    
    @objc func onBtnItemSelect(_ sender: UIButton) {
        let index = sender.tag
        let dic : Brand = arrBrandEn1[index / 10][index % 10]
        dic.isSelected = !dic.isSelected
        
        if dic.isSelected {
            m_selectedBrandList?.append(dic.nameEn)
            m_selectedBrandUids?.append(dic.brandUid)
        } else {
            
            var pos : Int = -1
            if let list = m_selectedBrandUids {
                for i in 0..<list.count {
                    if dic.brandUid == m_selectedBrandUids![i] {
                        pos = i
                    }
                }
            }
            
            if pos > -1 {
                m_selectedBrandList?.remove(at: pos)
                m_selectedBrandUids?.remove(at: pos)
            }
        }
        
        gSearchInfo.setBrandUidList(arr: m_selectedBrandUids!)
        getSearchcount()
        m_tbBrandList.reloadData()
    }
    
    func tableViewIndexChanged(_ index: Int, title: String) {
        let group = index_eng
        let section = group.index(of: title) ?? 0
        
        var point = m_tbBrandList.rect(forSection: section).origin
        point.y = min(point.y, m_tbBrandList.contentSize.height - m_tbBrandList.bounds.height)
        m_tbBrandList.setContentOffset(point, animated: true)
    }
    
    func tableViewIndexTopLayoutGuideLength() -> CGFloat {
        return 135
    }
    
    func tableViewIndexBottomLayoutGuideLength() -> CGFloat {
        return 80
    }
    
    //
    // Event Handlers
    //
    
    @objc func searchTextChanged(_ sender : UITextField) {
        if(m_tfKeyword.text == "") {
            m_lbPlaceholder.isHidden = false
        } else {
            m_lbPlaceholder.isHidden = true
        }
        let str : String = sender.text!
        getSearchResult(key: str)
    }
    
    @IBAction func onBtnBack(_ sender: Any) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_BRAND_CHANGE), object: m_selectedBrandList))
        popVC()
    }
    
    @IBAction func onBtnReset(_ sender: Any) {
        
      gSearchInfo.setBrandUidList(arr: [])
      getAllBrandDetailList()
    }
    
    @IBAction func onBtnApply(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_SEARCH), object: nil, userInfo: nil)
        popVC(-2)
    }
    
    @IBAction func onBtnPlaceholder(_ sender: Any) {
        m_tfKeyword.becomeFirstResponder()
    }
    
    @IBAction func hideKeyboardAction(_ sender: Any) {
        if (m_tfKeyword != nil) {
            if(m_tfKeyword.text == "") {
                m_vwSearchBar.removeConstraint(_cstrPlaceHolderXPos);
                
                UIView.beginAnimations("menuHoverAnim", context: nil)
                UIView.setAnimationDuration(TabAnimationTime)
                UIView.setAnimationDelegate(self)
                
                _cstrPlaceHolderXPos = NSLayoutConstraint(item: self.m_vwPlaceholder, attribute: .centerX, relatedBy: .equal, toItem: self.m_vwSearchBar, attribute: .centerX, multiplier: 1.0, constant: 0.0)
                m_vwSearchBar.addConstraint(_cstrPlaceHolderXPos)   
                
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                
                UIView.commitAnimations()
            }
            m_tfKeyword?.resignFirstResponder()
        }
    }
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //검색 결과 갯수 얻기
    func getSearchcount() {
        
        gProgress.show()
        Net.SearchCount(
            accessToken         : gMeInfo.token,
            brandUidList        : gSearchInfo.getBrandUidList(),
            categoryUid         : gSearchInfo.getCategoryUid(),
            keyword             : gSearchInfo.getKeyword(),
            pdtColorList        : gSearchInfo.getPdtColorList(),
            pdtConditionList    : gSearchInfo.getPdtConditionList(),
            pdtPriceMax         : gSearchInfo.getPriceMax(),
            pdtPriceMin         : gSearchInfo.getPriceMin(),
            pdtSizeList         : gSearchInfo.getPdtSizeList(),
            searchOrderType     : gSearchInfo.getSearchOrderType(),
            soldOutExpect       : gSearchInfo.getSoldOutExpect(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SearchCountResult
                self._lbResultCnt.text = "검색결과 보기 ( " + String(res.count) + " )"
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    // 모든 브랜드 디테일 목록얻기
    func getAllBrandDetailList() {
        
        gProgress.show()
        Net.getAllBrandDetailList(
            accessToken     : gMeInfo.token,
            column          : 1,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AllBrandDetailListResult
                self.AllBrandDetailListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func AllBrandDetailListResult(_ data: Net.AllBrandDetailListResult) {
        arrBrandList = data.list
        arrBrandEn = data.list_en
        arrBrandKo = data.list_ko
        arrBrandEn1 = data.list_en
        arrBrandKo1 = data.list_ko
        
        m_selectedBrandUids = []
        m_selectedBrandList = []
        m_tbBrandList.reloadData()
        
        getSearchcount()
    }
}

extension FilterBrandViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == m_tfKeyword) {
            if(m_tfKeyword.text == "") {
                m_vwSearchBar.removeConstraint(_cstrPlaceHolderXPos);
                
                UIView.beginAnimations("menuHoverAnim", context: nil)
                UIView.setAnimationDuration(TabAnimationTime)
                UIView.setAnimationDelegate(self)
                
                _cstrPlaceHolderXPos = NSLayoutConstraint(item: self.m_vwPlaceholder, attribute: .leading, relatedBy: .equal, toItem: self.m_vwSearchBar, attribute: .leading, multiplier: 1.0, constant: 25.0)
                m_vwSearchBar.addConstraint(_cstrPlaceHolderXPos)
                
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                
                UIView.commitAnimations()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension FilterBrandViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return index_eng.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(format:"%@", index_eng[section])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrBrandEn1[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandCell", for: indexPath) as! BrandCell
        
        let dic : Brand = arrBrandEn1[indexPath.section][indexPath.row]
        cell._lbBrandFirstName.text = dic.nameEn
        cell._lbBrandSecondName.text = dic.nameKo
        
//        cell.delegate = self
//        cell.setIndexPath(indexPath)
//
        cell._imvSelectOn.image = arrBrandEn1[indexPath.section][indexPath.row].isSelected ?  #imageLiteral(resourceName: "ic_login_confirm") : #imageLiteral(resourceName: "ic_brand_select_off")
        cell._imvSelectOn.isHidden = false
        
        cell._btnSelect.tag = indexPath.section * 10 + indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnItemSelect(_:)), for: UIControlEvents.touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("asdfd")
    }
}


//extension FilterBrandViewController: BrandCellDelegate {
//    func BrandCellDelegate(_ brandCell: BrandCell, onBrandSelected sender: Any, tag: Int?) {
//        if let indexPath = brandCell.getIndexPath() {
//            brandList[indexPath.section][indexPath.row] = !brandList[indexPath.section][indexPath.row]
//
//            if brandList[indexPath.section][indexPath.row] {
//                m_selectedBrandList?.append(brandCell._lbBrandSecondName.text!)
//            } else {
//                let pos = (m_selectedBrandList?.count)! - 1
//                m_selectedBrandList?.remove(at: pos)
//            }
//
//            m_tbBrandList.reloadData()
//        }
//    }
//}

