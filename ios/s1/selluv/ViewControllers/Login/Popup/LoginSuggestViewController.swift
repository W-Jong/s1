//
//  LoginSuggestViewController.swift
//  selluv
//
//  Created by Dev on 12/8/17.
//  modified by PJh on 24/03/18

import UIKit

class LoginSuggestViewController: BaseViewController {

    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var ivBg: UIImageView!
    @IBOutlet var vwVideo: UIView!
    
    var m_img = ["bg_login_suggest_1@3x.png",
                 "bg_login_suggest_2@3x.png",
                 "bg_login_suggest_3@3x.png",
                 "bg_login_suggest_4@3x.png"]
    
    var m_cnt = -1
    var m_timer : Timer?
    var cbOk: callback! = nil
    
    var playerView : AVPlayer?
    
    public typealias callback = (_ type : Int) -> ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let fileUrl = NSURL(fileURLWithPath: Bundle.main.path(forResource: "test", ofType: "MP4")!)
        playerView = AVPlayer(url: fileUrl as URL)
        
        let playerLayer = AVPlayerLayer(player: playerView)
        playerLayer.frame = vwVideo.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resize
        
        vwVideo.layer.addSublayer(playerLayer)
        playerView?.play()
        NotificationCenter.default.addObserver(self, selector: #selector(playEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
//        ivBg.image = UIImage(named: m_img[0])
//
//        m_timer?.invalidate()
//        m_timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.onTimer), userInfo: nil, repeats: true)
        
    }
    
    static func show(_ vc: UIViewController, okCallback: callback! = nil) {
        let vcSuggest = LoginSuggestViewController(okCallback: okCallback)
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    @objc func onTimer() {
        m_cnt += 1
        if m_cnt > 3 {
            m_cnt = 0
        }
        
        ivBg.image = UIImage(named: m_img[m_cnt])
    }
    
    @objc func playEnd(_ sender : Any) {
        playerView?.seek(to: CMTimeMake(0, Int32(NSEC_PER_SEC)), completionHandler: { (finish) in
            self.playerView?.play()
        })
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func registerAction(_ sender: Any) {
        m_timer?.invalidate()
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(SIGNUP)
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        m_timer?.invalidate()
        dismiss(animated: true, completion: nil)

        if cbOk != nil {
            cbOk(SKIP)
        }
    }
}
