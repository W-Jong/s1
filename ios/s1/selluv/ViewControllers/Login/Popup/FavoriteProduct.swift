//
//  FavoriteProduct.swift
//  selluv
//  관심상품군 선택
//  Created by Gambler on 12/30/17.
//  modified by PJH on 24/03/18.

import UIKit

class FavoriteProduct: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var ivWomenCheck: UIImageView!
    @IBOutlet weak var ivMenCheck: UIImageView!
    @IBOutlet weak var ivKidsCheck: UIImageView!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var cbOk: callback! = nil
    public typealias callback = (_ isChildren: Bool, _ isFemale: Bool, _ isMale: Bool , _ status : Bool) -> ()

    var isMale    :Bool!
    var isfemale  :Bool!
    var isKids    :Bool!

    var firstMale    :Bool!
    var firstFemale  :Bool!
    var fitstKids    :Bool!
    var changeStatus : Bool! // false : 변화되지 않은 상태  true : 변화된 상태
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
//        sex = kind.rawValue
        cbOk = okCallback
   
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, okCallback: callback! = nil) {
        let vcSuggest = FavoriteProduct(okCallback: okCallback)
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        
        if gMeInfo.likeGroup & 1 != 0 {
            isMale = true
            ivMenCheck.isHidden = false
        } else {
            isMale = false
            ivMenCheck.isHidden = true
        }
        firstMale = isMale
        
        if gMeInfo.likeGroup & 2 != 0 {
            isfemale = true
            ivWomenCheck.isHidden = false
        } else {
            isfemale = false
            ivWomenCheck.isHidden = true
        }
        firstFemale = isfemale
        
        if gMeInfo.likeGroup & 4 != 0 {
            isKids = true
            ivKidsCheck.isHidden = false
        } else {
            isKids = false
            ivKidsCheck.isHidden = true
        }
        fitstKids = isKids
        
        changeStatus = false
        
        btnConfirm.backgroundColor = ColorBtnInactive
        btnConfirm.isSelected = false
    }
    
    func changeButton() {
        if isMale == firstMale && isfemale == firstFemale && isKids == fitstKids {
            btnConfirm.isSelected = false
            btnConfirm.backgroundColor = ColorBtnInactive
            changeStatus = false
        } else {
            btnConfirm.isSelected = true
            btnConfirm.backgroundColor = ColorBtnActive
            changeStatus = true
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func womenSelectAction(_ sender: Any) {
        
        if ivWomenCheck.isHidden {
            isfemale = true
            ivWomenCheck.isHidden = false
        } else {
            isfemale = false
            ivWomenCheck.isHidden = true
        }
        changeButton()
    }
    
    @IBAction func menSelectAction(_ sender: Any) {
    
        if ivMenCheck.isHidden {
            ivMenCheck.isHidden = false
            isMale = true
        } else {
            ivMenCheck.isHidden = true
            isMale = false
        }
        changeButton()
    }
    
    @IBAction func kidsSelectAction(_ sender: Any) {
        
        if ivKidsCheck.isHidden {
            ivKidsCheck.isHidden = false
            isKids = true
        } else {
            ivKidsCheck.isHidden = true
            isKids = false
        }
        changeButton()
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(isKids, isfemale, isMale, changeStatus)
        }
    }
}
