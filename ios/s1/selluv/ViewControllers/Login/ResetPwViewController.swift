//
//  ResetPwViewController.swift
//  selluv
//
//  Created by Dev on 12/6/17.
//

import UIKit

class ResetPwViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    @IBOutlet weak var scvViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tfEmail: DesignableUITextField!
    @IBOutlet weak var btnReset: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnReset.isEnabled = false
        btnReset.backgroundColor = ColorBtnInactive
        
        tfEmail.layer.borderWidth = 1
        tfEmail.layer.borderColor = ColorWhite.cgColor
        tfEmail.placeholderColor = ColorBtnInactive
        tfEmail.delegate = self
        tfEmail.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        tfEmail.becomeFirstResponder()
    }
    
    @objc func textChanged(_ sender : UITextField) {
        btnReset.isEnabled = tfEmail.hasText
        btnReset.backgroundColor = btnReset.isEnabled ? ColorBtnActive : ColorBtnInactive
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                scvViewBottom.constant = keyboardSize.height - 60
                
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        scvViewBottom.constant = 0
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickConfirm(_ sender: Any) {
        hideKeyboard(self)
        if !CommonUtil.validEmail(tfEmail.text!) {
            self.view.makeToast(NSLocalizedString("demail_type_wrong", comment:""))
            return
        }
        getFindPass()
    }
    
    
    @IBAction func hideKeyboard(_ sender: Any) {
        tfEmail.resignFirstResponder()
    }
    
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 비밀번호 찾기
    func getFindPass() {
        
        hideKeyboard(self)
        
        gProgress.show()
        Net.UserPassFind(
            email      : tfEmail.text!,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.userPassFindResult()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //회원로그인 결과
    func userPassFindResult() {
        CommonUtil .showToast(NSLocalizedString("user_pass_find_success", comment:""))
        popVC()
    }
    
}

/////////////////////////////////////////////////////
// MARK: - Delegates
/////////////////////////////////////////////////////

extension ResetPwViewController : UITextFieldDelegate {
    
}
