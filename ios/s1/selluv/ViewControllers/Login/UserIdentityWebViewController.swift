//
//  UserIdentityWebViewController.swift
//  selluv
//  다날본인인증
//  Created by Dev on 12/6/17.
//  modified by PJh on 05/04/18.

import UIKit
import WebKit

class UserIdentityWebViewController: BaseViewController, UIWebViewDelegate {

    var vc : UIViewController?
    var m_nNumber = 0
    @IBOutlet weak var _webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    func initVC() {
        _webview.delegate = self
        
        let weburl = Server_Url + F_API.WEB_DANALSMS.rawValue
        _webview.loadRequest(URLRequest(url: URL(string: weburl)!))
    }
    
    func shoAlert(msg : String) {
        let encodeStr : String = msg.removingPercentEncoding!
        let alert = UIAlertController(title: "", message: encodeStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("confirm", comment:""), style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if m_nNumber == 1 {
            popVC()
        } else {
            pushVC("LOGIN_BRAND_VIEW", storyboard: "Login", animated: true)
        }        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if (request.url?.absoluteString.hasPrefix("selluv://showToast"))!{
            let suburl = request.url?.absoluteString
            let fullNameArr = suburl?.components(separatedBy: "=")
            self.shoAlert(msg: fullNameArr![1])
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://closeWebview"))!{
            dismiss(animated: true, completion: nil)
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://danalSuccess"))!{
            //address, postcode
            dismiss(animated: true, completion: nil)
            
            let findAddress = request.url?.absoluteString.components(separatedBy: "?")
            let subAddress = findAddress![1].components(separatedBy: "&")
            let address = subAddress[0].components(separatedBy: "=")[1].removingPercentEncoding
            let postcode = subAddress[1].components(separatedBy: "=")[1]
            
//            if cbCallback != nil {
//                cbCallback(address!, postcode)
//            }
            return false
        } else if (request.url?.absoluteString.hasPrefix("http://postcode.map.daum.net"))!{
            
            return false
        }
        
        return true
        
    }
    
}
