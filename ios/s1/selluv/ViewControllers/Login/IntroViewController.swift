//
//  IntroViewController.swift
//  selluv
//
//  Created by Dev on 12/8/17.
//  modified by PJH on 01/03/18.

import UIKit

class IntroViewController: BaseViewController {

    var _id : String = ""
    var pass : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: 1)
            DispatchQueue.main.async {
                self.goNext()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    func goNext() {
        
        var strType = ""
        if gMeInfo.usrLoginType == FACEBOOK {
            strType = "FACEBOOK"
        } else if gMeInfo.usrLoginType == NAVER {
            strType = "NAVER"
        } else if gMeInfo.usrLoginType == KAKAOTALK {
            strType = "KAKAOTALK"
        } else {
            strType = "NORMAL"
        }

        if gMeInfo.usrId == nil && gMeInfo.usrPass == nil {
            // 처음 가입하는 회원이라면
            pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
        } else if gMeInfo.usrId != nil && gMeInfo.usrPass == nil {
            // 로그아웃후 재방문이라면
            pushVC("REVISIT_VIEW", storyboard: "Login", animated: true)
        } else {
            if gMeInfo.loginCnt == 2 {  //가입 2번째 실행이라면 관심상품군 페지로 이행
                FavoriteProduct.show(self) { (isChildren, isFemale, isMale, status) in
                    if status {
                        self.updateUsrLikeGroup(kids: isChildren, female: isFemale, male: isMale, loginType: strType)
                    } else {
                        self.reqLogin(loginType: strType)
                    }
                }
            } else {
                reqLogin(loginType: strType)
            }
        }
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
 
    // 회원로그인
    func reqLogin(loginType : String) {
        
        if gMeInfo.usrLoginType == NORMAL {
            _id = gMeInfo.usrId
            pass = gMeInfo.usrPass
        } else {
            _id = gMeInfo.usrPass
            pass = gMeInfo.usrPass
        }
        
        gProgress.show()
        Net.UserLogin(
            loginType   : loginType,
            osTp        : "IOS",
            password    : pass,
            usrId       : _id,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UserLoginResult
                self.userLoginResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }
            
        })
    }
    
    //회원로그인 결과
    func userLoginResult(_ data: Net.UserLoginResult) {
    
        let logCnt = gMeInfo.loginCnt + 1
        let ud: UserDefaults = UserDefaults.standard
        ud.set(logCnt, forKey: "loginCnt")
        ud.synchronize()
        
        gMeInfo.set(data.usr, pass: gMeInfo.usrPass, accesstoken: data.accessToken)
        gUsrAddress = data.addressList
        
        replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
    }
    
    // 관심상품군 업데이트
    func updateUsrLikeGroup(kids : Bool, female : Bool, male : Bool, loginType : String) {
        
        gProgress.show()
        Net.updateUsrLikeGroup(
            accessToken  : gMeInfo.token,
            isChildren   : kids,
            isFemale     : female,
            isMale       : male,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.reqLogin(loginType: loginType)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }
            
        })
    }
}
