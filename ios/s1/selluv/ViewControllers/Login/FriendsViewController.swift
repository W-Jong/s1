//
//  FriendsViewController.swift
//  selluv
//
//  Created by Dev on 12/8/17.
//  modified by PJH on 27/03/18.

import UIKit

class FriendsViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    //profile1
    @IBOutlet var vwProfile1: UIView!
    @IBOutlet var btnProfile1: UIButton!
    @IBOutlet var ivLoginType1: UIImageView!
    @IBOutlet var lbNickname1: UILabel!
    @IBOutlet var ivProfile1: UIImageView!
    
    //profile2
    @IBOutlet var vwProfile2: UIView!
    @IBOutlet var btnProfile2: UIButton!
    @IBOutlet var ivLoginType2: UIImageView!
    @IBOutlet var lbNickname2: UILabel!
    @IBOutlet var ivProfile2: UIImageView!
    
    var user1  : UserModel!
    var user2  : UserModel!
    var bEdit  : Bool!
    var vcRevisit : RevisitViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        initData()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        ivProfile1.layer.cornerRadius = ivProfile1.frame.size.width / 2
        ivProfile2.layer.cornerRadius = ivProfile2.frame.size.width / 2
    }
    
    func initData() {
        
        if user1 == nil {
            vwProfile1.isHidden = true
        } else {
            vwProfile1.isHidden = false
            
            ivProfile1.kf.setImage(with: URL(string: user1.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

            lbNickname1.text = user1.usrNcknm
            
            if bEdit {
                ivLoginType1.image = UIImage(named: "btn_login_account_del")
            } else {
                if user1.loginType == "1" {
                    ivLoginType1.image = UIImage.init(named: "ic_login_fb_badge")
                } else if user1.loginType == "2" {
                    ivLoginType1.image = UIImage.init(named: "ic_login_naver_badge")
                } else if user1.loginType == "3" {
                    ivLoginType1.image = UIImage.init(named: "ic_login_kakao_badge")
                } else {
                    ivLoginType1.image = UIImage.init(named: "ic_login_email_badge")
                }
            }
        }
        
        if user2 == nil {
            vwProfile2.isHidden = true
        } else {
            vwProfile2.isHidden = false
         
            ivProfile2.kf.setImage(with: URL(string: user2.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

            lbNickname2.text = user2.usrNcknm
            
            if bEdit {
                ivLoginType2.image = UIImage(named: "btn_login_account_del")
            } else {
                if user2.loginType == "1" {
                    ivLoginType2.image = UIImage.init(named: "ic_login_fb_badge")
                } else if user2.loginType == "2" {
                    ivLoginType2.image = UIImage.init(named: "ic_login_naver_badge")
                } else if user2.loginType == "3" {
                    ivLoginType2.image = UIImage.init(named: "ic_login_kakao_badge")
                } else {
                    ivLoginType2.image = UIImage.init(named: "ic_login_email_badge")
                }
            }
        }
    }

    //////////////////////////////////////////
    // MARK: - Actions
    //////////////////////////////////////////
    @IBAction func onProfile1Click(_ sender: Any) {
        let info = ["loginType": user1.loginType, "usrId": user1.usrId, "usrPass": user1.usrPass, "usrUid": user1.usrUid ] as [String : Any]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_RE_VISIT), object: nil, userInfo: info)
    }
  
    @IBAction func onProfile2Click(_ sender: Any) {
        let info = ["loginType": user2.loginType, "usrId": user2.usrId, "usrPass": user2.usrPass , "usrUid": user2.usrUid ] as [String : Any]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_RE_VISIT), object: nil, userInfo: info)
    }
}
