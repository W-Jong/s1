//
//  FravoriteBrandViewController.swift
//  selluv
//
//  Created by Dev on 12/6/17.
//  modified by PJH on 03/01/18.

import UIKit

class FavoriteBrandViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var clvBrands: UICollectionView!
    @IBOutlet weak var btnSelect: UIButton!
    
    var arrBrands : [Brand] = []
    var from = "UserIdentityView"
    
    var email : String = ""
    var _id : String = ""
    var nickname : String = ""
    var password : String = ""
    var birthday : String = ""
    var gender : Int!
    var loginType : String = ""
    var recCode : String = ""
    var snsId : String = ""
    var snsProfileImg : String = ""
    var usrNm : String = ""
    var usrPhone : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        getBrandFramAll()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    

    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        btnSelect.backgroundColor = ColorBtnActive
        btnSelect.isEnabled = true
        
        clvBrands.register(UINib(nibName: "FavoriteBrandGridCell", bundle: nil), forCellWithReuseIdentifier: "brandCell")
        
    }
    
    func setSelectBtn() {
//        btnSelect.backgroundColor = ColorBtnInactive
//        btnSelect.isEnabled = false
//
//        for brand in arrBrands {
//            if brand.isSelected {
//                btnSelect.backgroundColor = ColorBtnActive
//                btnSelect.isEnabled = true
//            }
//        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func selectAction(_ sender: Any) {
        
        if from == "UserIdentityView" {
            // 회원가입화면으로부터 온 경우
//            replaceVC("MAIN_VIEW", storyboard: "Main", animated: true)
            reqSingup()
        } else {
            // 메인화면으로부터 진입 한 경우
            popVC()
        }
        
    }
    
    @objc func brandSelectAction(_ sender : UIButton) {
        
        if from == "UserIdentityView" {
            arrBrands[sender.tag].isSelected = true
            arrBrands[sender.tag].brandLikeCount  = arrBrands[sender.tag].brandLikeCount + 1
            clvBrands.reloadData()
            setSelectBtn()
        } else {
            let dic : Brand = arrBrands[sender.tag]
            setbrandFollow(_uid: dic.brandUid)
        }
    }
    
    @objc func brandDeSelectAction(_ sender : UIButton) {
        if from == "UserIdentityView" {
            arrBrands[sender.tag].isSelected = false
            arrBrands[sender.tag].brandLikeCount  = arrBrands[sender.tag].brandLikeCount - 1
            clvBrands.reloadData()
            setSelectBtn()
        } else {
            let dic : Brand = arrBrands[sender.tag]
            delbrandFollow(_uid: dic.brandUid)
        }

    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    func getAllFameBrandResult(_ data: Net.AllFramBrandResult) {
        print("%d", data.list.count)
        
        arrBrands = data.list
        clvBrands.reloadData()
    }
    
    //회원로그인 결과
    func userSingupResult(_ data: Net.SingupResult) {
        
        CommonUtil.showToast(NSLocalizedString("signup_success", comment:""))
        
        gMeInfo.set(data.usr, pass: self.password, accesstoken: data.accessToken)
        gUsrAddress = data.addressList
        
        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
    }

    // 관심 브렌드 목록 얻기
    func getBrandFramAll() {
        var pdtGroupType = "FEMALE"
        
        if from != "UserIdentityView" {
            gender = gMeInfo.gender
        }
        
        if gender == 1 {
            pdtGroupType = "MALE"
        } else {
            pdtGroupType = "FEMALE"
        }
        
        gProgress.show()
        Net.getBrandFramAll(
            pdtGroupType: pdtGroupType,
            token       : from == "UserIdentityView" ? "temp" : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AllFramBrandResult
                self.getAllFameBrandResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
    // 회원가입
    func reqSingup() {
        
        var arrLikeBarnds = [String]()
        for i in 0..<arrBrands.count {
            let dic : Brand = arrBrands[i]
            if dic.isSelected {
                arrLikeBarnds.append(String(dic.brandUid))
            }
        }
        
        gProgress.show()
        Net.UsrSingup(
            birthday        : birthday,
            gender          : gender,
            inviteCode      : recCode,
            likeBrands      : arrLikeBarnds,
            loginType       : loginType,
            password        : password,
            snsId           : snsId,
            snsProfileImg   : snsProfileImg,
            usrId           : _id,
            usrMail         : email,
            usrNckNm        : nickname,
            usrNm           : usrNm,
            usrPhone        : usrPhone,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.SingupResult
                self.userSingupResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
                self.popVC()
            }
            
        })
    }
    
    // 브렌드 팔로우 하기
    func setbrandFollow(_uid : Int) {
        
        gProgress.show()
        Net.setbrandFollow(
            accessToken     : gMeInfo.token,
            brandUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBrandFramAll()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 브렌드 팔로우 취소하기
    func delbrandFollow(_uid : Int) {
        
        gProgress.show()
        Net.delbrandFollow(
            accessToken     : gMeInfo.token,
            brandUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBrandFramAll()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension FavoriteBrandViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBrands.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "brandCell", for: indexPath) as! FavoriteBrandGridCell

        let dic : Brand = arrBrands[indexPath.row]
        
        if from == "UserIdentityView" {
            if dic.isSelected {
                cell.ivMask.isHidden = true
                cell.btnDeSelect.isHidden = false
                cell.btnSelect.isHidden = true
                cell.ivPlus.isHidden = true
            } else {
                cell.ivMask.isHidden = false
                cell.btnDeSelect.isHidden = true
                cell.btnSelect.isHidden = false
                cell.ivPlus.isHidden = false
            }
        } else {
            if dic.brandLikeStatus {
                cell.ivMask.isHidden = true
                cell.btnDeSelect.isHidden = false
                cell.btnSelect.isHidden = true
                cell.ivPlus.isHidden = true
            } else {
                cell.ivMask.isHidden = false
                cell.btnDeSelect.isHidden = true
                cell.btnSelect.isHidden = false
                cell.ivPlus.isHidden = false
            }
        }
        
        cell.ivBg.kf.setImage(with: URL(string: dic.backImg), placeholder: UIImage(named: "img_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell.ivLogo.kf.setImage(with: URL(string: dic.logoImg), placeholder: UIImage(named: ""), options: [], progressBlock: nil, completionHandler: nil)
        cell.btnDeSelect.setTitle(String(dic.brandLikeCount),for: .normal)
        
        cell.btnDeSelect.tag = indexPath.row
        cell.btnSelect.tag = indexPath.row
        
        cell.btnDeSelect.addTarget(self, action: #selector(self.brandDeSelectAction(_:)), for: .touchUpInside)
        cell.btnSelect.addTarget(self, action: #selector(self.brandSelectAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
}

extension FavoriteBrandViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 10) / 2
        return CGSize(width: width, height: width)
    }
    
}
