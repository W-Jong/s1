//
//  RevisitViewController.swift
//  selluv
//  재방분
//  Created by Dev on 12/8/17.
//  modified by PJH on 09/03/18.

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AEXML

class RevisitViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var ivBg: UIImageView!
    @IBOutlet var ivBgMask: UIImageView!
    @IBOutlet var ivLogo: UIImageView!
    
    @IBOutlet var vwMainLeading: NSLayoutConstraint!
    @IBOutlet var vwContentTop: NSLayoutConstraint!
    
    @IBOutlet var vwMain: UIView!
    @IBOutlet var ivLogoMain: UIImageView!
    @IBOutlet var scvUser: UIScrollView!
    @IBOutlet var vwUser: UIView!
    @IBOutlet var btnUser: UIButton!
    @IBOutlet var ivUser: UIImageView!
    @IBOutlet var lbUser: UILabel!
    @IBOutlet var ivMarkUser: UIImageView!
    @IBOutlet var ivProfile: UIImageView!
    
    @IBOutlet var pcUser: UIPageControl!
    
    @IBOutlet var btnManage: UIButton!
    
    @IBOutlet var vwSelect: UIView!
    @IBOutlet var ivLogoSelect: UIImageView!
    
    @IBOutlet var vwLogin: UIView!
    @IBOutlet var scvLogin: UIScrollView!
    @IBOutlet var scvLoginTop: NSLayoutConstraint!
    @IBOutlet var scvLoginBottom: NSLayoutConstraint!
    @IBOutlet var ivLogoLogin: UIImageView!
    @IBOutlet var tfId: DesignableUITextField!
    @IBOutlet var tfPw: DesignableUITextField!
    @IBOutlet var btnLogin: UIButton!
    
    @IBOutlet var vwRegister: UIView!
    @IBOutlet var scvRegister: UIScrollView!
    @IBOutlet var scvRegisterBottom: NSLayoutConstraint!
    @IBOutlet var tfEmail: DesignableUITextField!
    @IBOutlet var tfRegId: DesignableUITextField!
    @IBOutlet var ivRegIdConfirm: UIImageView!
    @IBOutlet var tfNickname: DesignableUITextField!
    @IBOutlet var tfRegPw: DesignableUITextField!
    @IBOutlet var tfRecCode: DesignableUITextField!
    @IBOutlet var btnRegister: UIButton!
    
    enum EPageStatus: Int {
        case main = 0
        case login
        case select
    }
    
    var tfActive : UITextField?
    var isIdValidate = true
    var pageStatus = EPageStatus.main
    var isManageMode = false
    
    var btnInfo     : UIButton!
    var loginType   : String!
    
    //sns info
    var snsId       : String = ""
    var snsNickname : String = ""
    var snsEmail    : String = ""
    var snsPass     : String = ""
    var snsBirth    : String = ""
    var snsGener    : Int    = 1
    var snsReconcode: String = ""
    var snsPhone    : String = ""
    var snsName     : String = ""
    
    var arrUsrList  : NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        ivLogo.isHidden = false
        ivLogoMain.isHidden = true
        ivLogoLogin.isHidden = true
        ivLogoSelect.isHidden = true
        
        tfId.text = ""
        tfId.layer.borderWidth = 0.6
        tfId.layer.borderColor = ColorWhite.cgColor
        tfId.placeholderColor = ColorBtnInactive
        tfId.delegate = self
        tfId.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        tfPw.text = ""
        tfPw.layer.borderWidth = 0.6
        tfPw.layer.borderColor = ColorWhite.cgColor
        tfPw.placeholderColor = ColorBtnInactive
        tfPw.delegate = self
        tfPw.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        tfEmail.text = ""
        tfEmail.layer.borderWidth = 0.6
        tfEmail.layer.borderColor = ColorWhite.cgColor
        tfEmail.placeholderColor = ColorBtnInactive
        tfEmail.delegate = self
        tfEmail.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        tfRegId.text = ""
        tfRegId.layer.borderWidth = 0.6
        tfRegId.layer.borderColor = ColorWhite.cgColor
        tfRegId.placeholderColor = ColorBtnInactive
        tfRegId.delegate = self
        tfRegId.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        ivRegIdConfirm.isHidden = true
        
        tfNickname.text = ""
        tfNickname.layer.borderWidth = 0.6
        tfNickname.layer.borderColor = ColorWhite.cgColor
        tfNickname.placeholderColor = ColorBtnInactive
        tfNickname.delegate = self
        tfNickname.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        tfRegPw.text = ""
        tfRegPw.layer.borderWidth = 0.6
        tfRegPw.layer.borderColor = ColorWhite.cgColor
        tfRegPw.placeholderColor = ColorBtnInactive
        tfRegPw.delegate = self
        tfRegPw.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        tfRecCode.text = ""
        tfRecCode.layer.borderWidth = 0.6
        tfRecCode.layer.borderColor = ColorWhite.cgColor
        tfRecCode.placeholderColor = ColorBtnInactive
        tfRecCode.delegate = self
        tfRecCode.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        btnLogin.isEnabled = false
        btnLogin.backgroundColor = ColorBtnInactive
        
        btnRegister.isEnabled = false
        btnRegister.backgroundColor = ColorBtnInactive
        
        btnUser.layer.cornerRadius = 40
        
        ivProfile.clipsToBounds = true
        ivProfile.layer.cornerRadius = ivProfile.frame.size.width / 2
      
    }

    var userCnt : Int = 0
    func initData() {
        arrUsrList = DBManager.getSharedInstance().getUsers()
        
        if arrUsrList.count == 1 {
            //1개일때는 스크롤뷰에 배치안함
            vwUser.isHidden = false
            scvUser.isHidden = true
            pcUser.isHidden = true
            
            let m : UserModel = arrUsrList.object(at: 0) as! UserModel
            
            ivProfile.kf.setImage(with: URL(string: m.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            
            lbUser.text = m.usrNcknm
            
            if isManageMode {
                ivUser.image = UIImage(named: "btn_login_account_del")
            } else {
                if m.loginType == "1" {
                    ivUser.image = UIImage.init(named: "ic_login_fb_badge")
                } else if m.loginType == "2" {
                    ivUser.image = UIImage.init(named: "ic_login_naver_badge")
                } else if m.loginType == "3" {
                    ivUser.image = UIImage.init(named: "ic_login_kakao_badge")
                } else {
                    ivUser.image = UIImage.init(named: "ic_login_email_badge")
                }
            }
            
        } else if arrUsrList.count == 0 {
            vwUser.isHidden = true
            scvUser.isHidden = true
            pcUser.isHidden = true
        } else {
            vwUser.isHidden = true
            scvUser.isHidden = false
            pcUser.isHidden = false
            
            for i in 0..<scvUser.subviews.count {
                scvUser.subviews[i].removeFromSuperview()
            }
            scvUser.setNeedsDisplay()

            if arrUsrList.count > 0 {
                if arrUsrList.count % 2 == 0 {
                    userCnt = arrUsrList.count / 2
                } else {
                    userCnt = arrUsrList.count / 2 + 1
                }
            }
            
            scvUser.contentSize = CGSize(width: scvUser.frame.size.width * CGFloat(userCnt), height: scvUser.frame.size.height)
            scvUser.isPagingEnabled = true
            pcUser.numberOfPages = userCnt
            
            addObserver()
            var i = 0
            while (i < userCnt) {
                
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "FRIENDS_VIEW") as! FriendsViewController
                self.addChildViewController(VC)
                if i * 2 >= arrUsrList.count {
                    VC.user1 = nil
                } else {
                    VC.user1 = arrUsrList.object(at: i * 2) as! UserModel
                }
                
                if i * 2 + 1 >= arrUsrList.count {
                    VC.user2 = nil
                } else {
                    VC.user2 = arrUsrList.object(at: i * 2 + 1) as! UserModel
                }

                VC.bEdit = isManageMode
                VC.vcRevisit = self
                let x : CGFloat = scvUser.frame.size.width * CGFloat(i)
                VC.view.frame = CGRect(x: x , y: 0, width: scvUser.bounds.size.width, height: scvUser.bounds.size.height)
                scvUser.addSubview(VC.view)
                
                i = i + 1
            }
        }
    }
    
    @objc func reviistUser(notification: NSNotification) {
        removeObserver()
        
        let type = notification.userInfo?["loginType"]  as! String
        let usrId = notification.userInfo?["usrId"]  as! String
        let usrPass = notification.userInfo?["usrPass"]  as! String
        let usrUid = notification.userInfo?["usrUid"]  as! String
        
        if isManageMode {
            DBManager.getSharedInstance().deleteUser(usrUid)
            initData()
        } else {
            
            if type == "1" {
                loginType = "FACEBOOK"
            } else if type == "2" {
                loginType = "NAVER"
            } else if type == "3" {
                loginType = "KAKAOTALK"
            } else {
                loginType = "NORMAL"
            }
            snsId = usrId
            snsPass = usrPass
            reqLogin()
        }
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reviistUser),
                                               name: NSNotification.Name(rawValue: NOTI_RE_VISIT),
                                               object: nil)
    }
    func removeObserver(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_RE_VISIT), object: nil)
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                if tfActive == tfId || tfActive == tfPw {
                    scvLoginBottom.constant = keyboardSize.height - 120
                    scvLoginTop.constant = -keyboardSize.height + 120
                } else {
                    scvRegisterBottom.constant = keyboardSize.height
                }
                
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        if tfActive == tfId || tfActive == tfPw {
            scvLoginTop.constant = 0
            scvLoginBottom.constant = 0
        } else {
            scvRegisterBottom.constant = 0
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func setLoginBtn() {
        btnLogin.isEnabled = tfId.hasText && tfPw.hasText
        if btnLogin.isEnabled {
            btnLogin.backgroundColor = ColorBtnActive
        } else {
            btnLogin.backgroundColor = ColorBtnInactive
        }
    }
    
    func setRegisterBtn() {
        btnRegister.isEnabled =
            (tfEmail.isHidden || (!tfEmail.isHidden && tfEmail.hasText))
            && tfRegId.hasText
            && tfNickname.hasText
            && (tfRegPw.isHidden || (!tfRegPw.isHidden && tfRegPw.hasText))
            && isIdValidate
        
        if btnRegister.isEnabled {
            btnRegister.backgroundColor = ColorBtnActive
        } else {
            btnRegister.backgroundColor = ColorBtnInactive
        }
    }
    
    @objc func textChanged(_ sender : UITextField) {
        switch sender {
        case tfId:
            setLoginBtn()
        case tfPw:
            setLoginBtn()
        case tfEmail:
            setRegisterBtn()
        case tfRegId:
            setRegisterBtn()
            ivRegIdConfirm.isHidden = true
        case tfNickname:
            setRegisterBtn()
        case tfRegPw:
            setRegisterBtn()
        default:
            return
        }
    }
    
    func showRegisterView(sender : UIButton) {
        
        tfEmail.text = ""
        tfRegId.text = ""
        tfNickname.text = ""
        tfRegPw.text = ""
        ivRegIdConfirm.isHidden = true
        tfRecCode.text = ""
        
        tfEmail.isHidden = sender.tag == 0
        tfRegPw.isHidden = sender.tag == 0
        
        setRegisterBtn()
        
        UIView.animateAndChain(withDuration: 0.2, delay: 0.0, options: [], animations: {
            
            self.vwContentTop.constant = 10
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: LoginViewAnimationTime, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [.curveEaseInOut],
                                    animations: { () -> Void in
                                        
            self.vwContentTop.constant = -self.view.frame.size.height
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            
            self.ivBgMask.alpha = 0.55
            if !self.tfEmail.isHidden {
                self.tfEmail.becomeFirstResponder()
            } else {
                self.tfRegId.becomeFirstResponder()
            }
            
        })
    }
    
    @objc func animationDidStop(animationID : String, finished : Int, context : NSObject) {
        switch animationID {
        case "showMainAnim" :
            ivLogo.isHidden = true
            ivLogoSelect.isHidden = true
            ivLogoMain.isHidden = false
            ivLogoLogin.isHidden = true
            ivBgMask.alpha = 0.35
        case "showLoginAnim" :
            ivLogo.isHidden = true
            ivLogoSelect.isHidden = true
            ivLogoMain.isHidden = true
            ivLogoLogin.isHidden = false
            ivBgMask.alpha = 0.55
        case "showSelectAnim" :
            ivLogo.isHidden = true
            ivLogoSelect.isHidden = false
            ivLogoMain.isHidden = true
            ivLogoLogin.isHidden = true
            ivBgMask.alpha = 0.55
        case "showRegAnim" :
            ivBgMask.alpha = 0.55
            if !tfEmail.isHidden {
                tfEmail.becomeFirstResponder()
            } else {
                tfRegId.becomeFirstResponder()
            }
        default :
            return
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func hideKeyboardAction(_ sender: Any) {
        if (tfActive != nil) {
            tfActive?.resignFirstResponder()
        }
    }
    
    @IBAction func startFbAction(_ sender: Any) {
        loginType = "FACEBOOK"
        
        btnInfo = sender as! UIButton
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        loginManager.loginBehavior = .web
        loginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                if(result?.isCancelled)! {
                    MsgUtil.showUIAlert("페이스북 로그인에 실패했습니다.")
                } else {
                    self.getFacebookInfo()
                }
            }
        }
    }
    
    @IBAction func startKakaoAction(_ sender: Any) {
        loginType = "KAKAOTALK"
        btnInfo = sender as! UIButton
        
        let session: KOSession = KOSession.shared();
        if session.isOpen() {
            session.close()
        }
        session.presentingViewController = self
        session.open { (error) in
            if error != nil{
                MsgUtil.showUIAlert((error?.localizedDescription)!)
                print(error?.localizedDescription as Any)
            }else if session.isOpen() == true{
                KOSessionTask.meTask(completionHandler: { (profile , error) -> Void in
                    if profile != nil{
                        DispatchQueue.main.async(execute: { () -> Void in
                            let kakao : KOUser = profile as! KOUser
                            session.logoutAndClose(completionHandler: nil)
                            self.snsId = NumberFormatter().string(from: kakao.id)!  //아이디를 패스워드로 쓰기로 했다.
                            self.snsName = (kakao.properties?["nickname"] as? String) ?? ""
                            self.snsEmail = kakao.email ?? ""
                            self.snsPass = self.snsId
                            self.reqLogin()
                        })
                    } else {
                        MsgUtil.showUIAlert("카카오톡 정보를 불러오는데 실패했습니다.")
                    }
                })
            }else{
                print("isNotOpen")
            }
        }
    }
    
    @IBAction func startNaverAction(_ sender: Any) {
        loginType = "NAVER"
        btnInfo = sender as! UIButton
        
        let tlogin : NaverThirdPartyLoginConnection = NaverThirdPartyLoginConnection.getSharedInstance()
        tlogin.delegate = self
        tlogin.consumerKey = kConsumerKey
        tlogin.consumerSecret = kConsumerSecret
        tlogin.serviceUrlScheme = kServiceAppUrlScheme
        tlogin.appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String // 앱이름
        tlogin.requestThirdPartyLogin()
    }
    
    @IBAction func loginBackAction(_ sender: Any) {
        pageStatus = .main
        
        ivLogo.isHidden = false
        ivLogoMain.isHidden = true
        ivLogoLogin.isHidden = true
        ivLogoSelect.isHidden = true
        
        UIView.animateAndChain(withDuration: 0.2, delay: 0.0, options: [], animations: {
            
            self.vwMainLeading.constant = -self.view.frame.size.width - 10
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: LoginViewAnimationTime, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [.curveEaseInOut],
                                    animations: { () -> Void in
                                        
            self.vwMainLeading.constant = 0
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            
            self.ivLogo.isHidden = true
            self.ivLogoSelect.isHidden = true
            self.ivLogoMain.isHidden = false
            self.ivLogoLogin.isHidden = true
            self.ivBgMask.alpha = 0.35
            
        })
        
    }
    
    @IBAction func emailRegisterAction(_ sender: Any) {
        showRegisterView(sender: sender as! UIButton)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        loginType = "NORMAL"
        snsId = tfId.text!
        snsPass = tfPw.text!

        reqLogin()
    }
    
    //이메일 회원가입
    @IBAction func doRegisterAction(_ sender: Any) {
        hideKeyboardAction(self)
        
        if loginType == "NORMAL" {
            if !CommonUtil.validEmail(tfEmail.text!) {
                self.view.makeToast(NSLocalizedString("demail_type_wrong", comment:""))
                return
            }
            
            snsEmail = tfEmail.text!
            snsNickname = tfNickname.text!
            snsPass = tfRegPw.text!
            snsId = ""
            snsReconcode = tfRecCode.text!
        } else {
            
            snsNickname = tfNickname.text!
            snsReconcode = tfRecCode.text!
        }
        
        //        pushVC("USER_IDENTITY_VIEW", storyboard: "Login", animated: true)
        
        WebviewViewController.show(self, page: Webview.DANAL_SMS.rawValue, url: "") { (name, phone, birth, gender) in
            
            self.snsName = name
            self.snsPhone = phone
            self.snsBirth = birth
            self.snsGener = Int(gender)!
            
            if self.snsReconcode == "" {
                self.goFavoriteBrandPage()
            } else {
                self.CheckReconCode()
            }
        }

    }    
    
    func goFavoriteBrandPage(){
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Login", bundle: nil)
        let vc : FavoriteBrandViewController = (storyboard.instantiateViewController(withIdentifier: "LOGIN_BRAND_VIEW") as! FavoriteBrandViewController)
        vc.email = snsEmail
        vc._id = tfRegId.text!
        vc.snsId = snsId
        vc.nickname = snsNickname
        vc.password = snsPass
        vc.birthday = snsBirth
        vc.gender = snsGener
        vc.loginType = loginType
        vc.recCode = snsReconcode
        vc.usrPhone = snsPhone
        vc.usrNm = snsName
        vc.from = "UserIdentityView"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //페이스북 정보 얻기
    func getFacebookInfo() {
        let params = ["fields": "id,name,email,gender, birthday" ]
        let request = FBSDKGraphRequest(graphPath: "me", parameters: params)!
        request.start(completionHandler: { (connection, result, error) in
            if result != nil {
                print(result!)
                let w_result = result as! [String: AnyObject?]
                
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                
                //                let first_name    = (w_result["first_name"] as? String) ?? ""
                //                let last_name = (w_result["last_name"] as? String) ?? ""
                self.snsId = (w_result["id"] as? String) ?? ""  //아이디를 패스워드로 쓰기로 했다.
                self.snsName = (w_result["name"] as? String) ?? ""
                self.snsEmail = (w_result["email"] as? String) ?? ""
                self.snsPass = self.snsId
                
                if (self.snsEmail.isEmpty) {
                    MsgUtil.showUIAlert("페이스북 정보를 불러오는데 실패했습니다.")
                    return
                }
                
                self.reqLogin()
                
            } else {
                MsgUtil.showUIAlert("페이스북 정보를 불러오는데 실패했습니다.")
            }
        })
    }
    
    @IBAction func forgetPwAction(_ sender: Any) {
        if tfActive != nil {
            tfActive!.resignFirstResponder()
            tfActive = nil
        }
        
        pushVC("RESET_PW_VIEW", storyboard: "Login", animated: true)
    }
    
    @IBAction func fbRegisterAction(_ sender: Any) {
        loginType = "FACEBOOK"
        //        showRegisterView(sender: sender as! UIButton)
        btnInfo = sender as! UIButton
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        loginManager.loginBehavior = .web
        loginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                if(result?.isCancelled)! {
                    MsgUtil.showUIAlert("페이스북 로그인에 실패했습니다.")
                } else {
                    self.getFacebookInfo()
                }
            }
        }
    }
    
    @IBAction func naverRegisterAction(_ sender: Any) {
        loginType = "NAVER"
        
        btnInfo = sender as! UIButton
        
        let tlogin : NaverThirdPartyLoginConnection = NaverThirdPartyLoginConnection.getSharedInstance()
        tlogin.delegate = self
        tlogin.consumerKey = kConsumerKey
        tlogin.consumerSecret = kConsumerSecret
        tlogin.serviceUrlScheme = kServiceAppUrlScheme
        tlogin.appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String // 앱이름
        tlogin.requestThirdPartyLogin()
    }
    
    @IBAction func kakaoRegisterAction(_ sender: Any) {
        loginType = "KAKAOTALK"
        //        showRegisterView(sender: sender as! UIButton)
        btnInfo = sender as! UIButton
        
        let session: KOSession = KOSession.shared();
        if session.isOpen() {
            session.close()
        }
        session.presentingViewController = self
        session.open { (error) in
            if error != nil{
                MsgUtil.showUIAlert((error?.localizedDescription)!)
                print(error?.localizedDescription as Any)
            }else if session.isOpen() == true{
                KOSessionTask.meTask(completionHandler: { (profile , error) -> Void in
                    if profile != nil{
                        DispatchQueue.main.async(execute: { () -> Void in
                            let kakao : KOUser = profile as! KOUser
                            session.logoutAndClose(completionHandler: nil)
                            self.snsId = NumberFormatter().string(from: kakao.id)!  //아이디를 패스워드로 쓰기로 했다.
                            self.snsName = (kakao.properties?["nickname"] as? String) ?? ""
                            self.snsEmail = kakao.email ?? ""
                            self.snsPass = self.snsId
                            self.reqLogin()
                        })
                    } else {
                        MsgUtil.showUIAlert("카카오톡 정보를 불러오는데 실패했습니다.")
                    }
                })
            }else{
                print("isNotOpen")
            }
        }
    }
    
    @IBAction func emailStartAction(_ sender: Any) {
        showRegisterView(sender: sender as! UIButton)
    }
    
    @IBAction func goback2MainAction(_ sender: Any) {
        if tfActive != nil {
            tfActive?.resignFirstResponder()
        }
        
        if pageStatus == .login {
            tfId.text = ""
            tfPw.text = ""
            setLoginBtn()
        }
        
        UIView.animateAndChain(withDuration: 0.2, delay: 0.0, options: [], animations: {
            
            self.vwContentTop.constant = -self.view.frame.size.height - 10
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: LoginViewAnimationTime, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [.curveEaseInOut],
                                    animations: { () -> Void in
                                        
            self.vwContentTop.constant = 0
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            
            if self.pageStatus == .login {
                self.ivLogo.isHidden = true
                self.ivLogoSelect.isHidden = true
                self.ivLogoMain.isHidden = true
                self.ivLogoLogin.isHidden = false
                self.ivBgMask.alpha = 0.55
            } else {
                self.ivLogo.isHidden = true
                self.ivLogoSelect.isHidden = false
                self.ivLogoMain.isHidden = true
                self.ivLogoLogin.isHidden = true
                self.ivBgMask.alpha = 0.55
            }
            
        })
        
    }
    
    @IBAction func go2GuideAction(_ sender: Any) {
        
        pushVC("MYPAGE_SALEPOLICY_VIEW", storyboard: "Mypage", animated: true)
        
    }
    
    // 회원가입버튼클릭
    @IBAction func registerAction(_ sender: Any) {
        pageStatus = .select
        
        vwSelect.isHidden = false
        vwLogin.isHidden = true
        
        ivLogo.isHidden = false
        ivLogoMain.isHidden = true
        ivLogoLogin.isHidden = true
        ivLogoSelect.isHidden = true
        
        UIView.animateAndChain(withDuration: 0.2, delay: 0.0, options: [], animations: {
            
            self.vwMainLeading.constant = 10
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: LoginViewAnimationTime, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [.curveEaseInOut],
                                    animations: { () -> Void in
                                        
            self.vwMainLeading.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            
            self.ivLogo.isHidden = true
            self.ivLogoSelect.isHidden = false
            self.ivLogoMain.isHidden = true
            self.ivLogoLogin.isHidden = true
            self.ivBgMask.alpha = 0.55
            
        })
    }
    
    // 다른 계정으로 로그인 클릭
    @IBAction func anotherLoginAction(_ sender: Any) {
        pageStatus = .login
        
        vwSelect.isHidden = true
        vwLogin.isHidden = false
        
        ivLogo.isHidden = false
        ivLogoMain.isHidden = true
        ivLogoLogin.isHidden = true
        ivLogoSelect.isHidden = true
        
        tfId.text = ""
        tfPw.text = ""
        setLoginBtn()
        
        UIView.animateAndChain(withDuration: 0.2, delay: 0.0, options: [], animations: {
            
            self.vwMainLeading.constant = 10
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: LoginViewAnimationTime, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [.curveEaseInOut],
                                    animations: { () -> Void in
                                        
            self.vwMainLeading.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            
            self.ivLogo.isHidden = true
            self.ivLogoSelect.isHidden = true
            self.ivLogoMain.isHidden = true
            self.ivLogoLogin.isHidden = false
            self.ivBgMask.alpha = 0.55
            
        })
        

    }
    
    // 계정관리 클릭
    @IBAction func manageAction(_ sender: Any) {
        isManageMode = !isManageMode
        
        if isManageMode {
            btnManage.setImage(nil, for: .normal)
            btnManage.setTitle("완료", for: .normal)
            
        } else {
            btnManage.setImage(UIImage(named: "btn_login_edit"), for: .normal)
            btnManage.setTitle("", for: .normal)
        }
        initData()
    }
    
    // User 클릭
    @IBAction func userAction(_ sender: Any) {
        let m : UserModel = arrUsrList.object(at: 0) as! UserModel
        if isManageMode {
            DBManager.getSharedInstance().deleteUser(m.usrUid)
            initData()
        } else {
            
            if m.loginType == "1" {
                loginType = "FACEBOOK"
            } else if m.loginType == "2" {
                loginType = "NAVER"
            } else if m.loginType == "3" {
                loginType = "KAKAOTALK"
            } else {
                loginType = "NORMAL"
            }
            snsId = m.usrId
            snsPass = m.usrPass
            reqLogin()
        }
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //회원로그인 결과
    func userLoginResult(_ data: Net.UserLoginResult) {
        
        gMeInfo.set(data.usr, pass: snsPass, accesstoken: data.accessToken)
        gUsrAddress = data.addressList
        
        replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
    }
    
    // 아이디 중복검사
    func CheckUsrid() {
        
        gProgress.show()
        Net.CheckUsrid(
            userid: tfRegId.text!,
            success: { (result) -> Void in
                gProgress.hide()
                self.ivRegIdConfirm.isHidden = !self.tfRegId.hasText
                
                self.ivRegIdConfirm.image = UIImage.init(named: "ic_login_confirm.png")
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                self.ivRegIdConfirm.isHidden = !self.tfRegId.hasText
                self.ivRegIdConfirm.image = UIImage.init(named: "ic_login_wrong.png")
                CommonUtil .showToast(err)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
    // 추천인 코드 중복검사
    func CheckReconCode() {
        
        gProgress.show()
        Net.checkReconCode(
            inviteCode: snsReconcode,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.goFavoriteBrandPage()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_INVITE_CODE_ERROR {  //
                CommonUtil .showToast(err)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
    // 회원로그인
    func reqLogin() {
        
        hideKeyboardAction(self)
        
        gProgress.show()
        Net.UserLogin(
            loginType   : loginType,
            osTp        : "IOS",
            password    : snsPass,
            usrId       : snsId,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UserLoginResult
                self.userLoginResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_NOT_EXISTS {
                CommonUtil .showToast(err)
                
                if self.loginType == "FACEBOOK" {
                    if self.btnInfo != nil{
                        self.showRegisterView(sender: self.btnInfo)
                    }
                } else if self.loginType == "KAKAOTALK" {
                    if self.btnInfo != nil{
                        self.showRegisterView(sender: self.btnInfo)
                    }
                } else if self.loginType == "NAVER" {
                    if self.btnInfo != nil{
                        self.showRegisterView(sender: self.btnInfo)
                    }
                } else {
                    CommonUtil .showToast(err)
                }
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
}

/////////////////////////////////////////////////////
// MARK: - Delegates
/////////////////////////////////////////////////////

extension RevisitViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfActive = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tfActive = nil
        
        if textField == tfRegId {
            if tfRegId.text == "" {
                return
            }
            
            self.CheckUsrid()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        switch textField {
        case tfId:
            tfPw.becomeFirstResponder()
        case tfEmail:
            tfRegId.becomeFirstResponder()
        case tfRegId:
            tfNickname.becomeFirstResponder()
        case tfNickname:
            if tfRegPw.isHidden {
                tfRecCode.becomeFirstResponder()
            } else {
                tfRegPw.becomeFirstResponder()
            }
        case tfRegPw:
            tfRecCode.becomeFirstResponder()
        default:
            return true
        }
        
        return true
    }
    
}

extension RevisitViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == scvUser {
            let newOffset = scrollView.contentOffset.x
            pcUser.currentPage = Int(newOffset / scvUser.frame.size.width)
        }
    }
}

extension RevisitViewController : NaverThirdPartyLoginConnectionDelegate {
    
    func parseXML(_ str: String) {
        do {
            let xmlDoc = try AEXMLDocument(xml: str)
            if (xmlDoc.root["result"]["resultcode"].string == "00") {
                snsId = xmlDoc.root["response"]["id"].string
                snsName = xmlDoc.root["response"]["nickname"].string
                //                let snsProfileImagePath = xmlDoc.root["response"]["profile_image"].string
                snsEmail = xmlDoc.root["response"]["email"].string
                self.snsPass = self.snsId
                self.reqLogin()
                
            } else {
                self.view.makeToast("Can't get Naver profile information!)")
            }
        }
        catch {
            print("\(error)")
        }
    }
    
    func naverSDKDidLoginSuccess() {
        /* 네이버 회원 정보 조회 */
        let loginConn = NaverThirdPartyLoginConnection.getSharedInstance()
        let tokenType = loginConn?.tokenType
        let accessToken = loginConn?.accessToken
        
        // Get User Profile
        if let url = URL(string: "https://apis.naver.com/nidlogin/nid/getUserProfile.xml") {
            if tokenType != nil && accessToken != nil {
                let authorization = "\(tokenType!) \(accessToken!)"
                var request = URLRequest(url: url)
                request.setValue(authorization, forHTTPHeaderField: "Authorization")
                let dataTask = URLSession.shared.dataTask(with: request) {(data, response, error) in
                    if let str = String(data: data!, encoding: .utf8) {
                        print(str)
                        loginConn?.resetToken()
                        
                        DispatchQueue.main.async {
                            self.parseXML(str)
                        }
                    }
                }
                dataTask.resume()
            }
        }
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        // 로그인이 성공했을 경우 호출
        naverSDKDidLoginSuccess()
        //        g_ProgressUtil.hideProgress()
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        /* 로그인 실패시에 호출되며 실패 이유와 메시지 확인 가능합니다. */
        print("oauth20Connection")
    }
    
    func oauth20ConnectionDidOpenInAppBrowser(forOAuth request: URLRequest!) {
        // 네이버 앱이 설치되어있지 않은 경우에 인앱 브라우저로 열리는데 이때 호출되는 함수
        
        let naverInappBrower = NLoginThirdPartyOAuth20InAppBrowserViewController(request: request)
        naverInappBrower?.modalPresentationStyle = .overFullScreen
        self.present(naverInappBrower!, animated: true, completion: nil)
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        // 이미 로그인이 되어있는 경우 access 토큰을 업데이트 하는 경우
        print("oauth20ConnectionDidFinishRequestACTokenWithRefreshToken")
    }
    
    func oauth20ConnectionDidFinishDeleteToken() {
        // 로그아웃이나 토큰이 삭제되는 경우
        print("oauth20ConnectionDidFinishDeleteToken")
    }
}
