//
//  FavoriteBrandGridCell.swift
//  selluv
//
//  Created by Dev on 12/6/17.
//

import UIKit

class FavoriteBrandGridCell: UICollectionViewCell {

    @IBOutlet weak var ivBg: UIImageView!
    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var ivMask: UIImageView!
    @IBOutlet weak var ivPlus: UIImageView!
    
    @IBOutlet weak var btnDeSelect: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
