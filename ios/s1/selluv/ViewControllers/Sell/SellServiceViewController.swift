//
//  SellServiceViewController.swift
//  selluv
//
//  Created by Gambler on 1/7/18.
//

import UIKit

class SellServiceViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwService: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        for subview in vwService {
        
            subview.layer.shadowColor = UIColor.gray.cgColor
            subview.layer.shadowOpacity = 0.3
            subview.layer.shadowOffset = .zero
            subview.layer.shadowRadius = 6
            subview.layer.masksToBounds = false
            
        }
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func nextAction(_ sender: Any) {
        pushVC("SELL_GROUP_VIEW", storyboard: "Sell", animated: true)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        popVC()
    }
    
    
}
