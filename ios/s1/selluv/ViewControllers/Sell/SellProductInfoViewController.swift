//
//  SellProductInfoViewController.swift
//  selluv
//  상품 정보 페지
//  Created by Gambler on 1/9/18.
//  modified by PJH on 20/03/18.

import UIKit

protocol SellProductInfoViewControllerDelegate {
    func setSize( size : String)
    func setTag( tag : String)
    func setCondition( condition : String)
}

class SellProductInfoViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    var delegate:  SellProductInfoViewControllerDelegate?
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    
    @IBOutlet weak var vwSize: UIView!
    @IBOutlet weak var vwSizeHeader: UIView!
    @IBOutlet weak var vwBagSize: UIView!
    @IBOutlet weak var tfWidth: DesignableUITextField!
    @IBOutlet weak var tfHeight: DesignableUITextField!
    @IBOutlet weak var tfDepth: DesignableUITextField!
    @IBOutlet weak var vwAccSize: UIView!
    @IBOutlet weak var btnFreeSize: UIButton!
    @IBOutlet weak var vwSizeDirect: UIView!
    @IBOutlet weak var lbSizeDirect: UILabel!
    @IBOutlet weak var tfSizeDirect: DesignableUITextField!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var ivSize: UIImageView!
    
    @IBOutlet weak var vwCond: UIView!
    @IBOutlet weak var vwCondHeader: UIView!
    @IBOutlet weak var lbCond: UILabel!
    @IBOutlet weak var ivCond: UIImageView!
    
    @IBOutlet var btnConds: [UIButton]!
    
    @IBOutlet weak var vwTag: UIView!
    @IBOutlet weak var vwTagHeader: UIView!
    @IBOutlet weak var lbTag: UILabel!
    @IBOutlet weak var ivTag: UIImageView!
    @IBOutlet var tvTag: KMPlaceholderTextView!
    @IBOutlet weak var tblTag: UITableView!
    
    @IBOutlet weak var vwLabel: UIView!
    @IBOutlet weak var vwBtn: UIView!
    @IBOutlet weak var vwClose: UIView!
    @IBOutlet weak var lcContentBottom: NSLayoutConstraint!
    
    var dicSizeInfo     : SizeRefDto!
    var PageStatus      : String!
    var isEdit          : Bool!
    var sizeType        : Int = 0
    var categoryUid     : Int!
    
    var pdtSize         : String!
    var pdtTag          : String!
    var pdtCondition    : String!
    var arrTags         : Array<String> = []
    
    var size11 = "" {
        didSet {
            lbSize.text = size11
//            btnNext.isEnabled = !size11.isEmpty && !condition.isEmpty
//            btnNext.isSelected = !size11.isEmpty && !condition.isEmpty
            checkNextButtonStatus()
        }
    }
    
    var condition = "" {
        didSet {
            lbCond.text = condition
//            btnNext.isEnabled = !size11.isEmpty && !condition.isEmpty
//            btnNext.isSelected = !size11.isEmpty && !condition.isEmpty
            checkNextButtonStatus()
        }
    }
    
    var tag = "" {
        didSet {
            lbTag.text = tag
        }
    }
    
    var activeTf : DesignableUITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnNext.isEnabled = false
        
        vwSizeHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sizeAction)))
        vwCondHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(condAction)))
        vwTagHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tagAction)))
        
        vwBagSize.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        vwAccSize.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        vwSizeDirect.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(directAction)))
        
        tfWidth.placeholderColor = UIColor(hex: 0xb5b5b5)
        tfHeight.placeholderColor = UIColor(hex: 0xb5b5b5)
        tfDepth.placeholderColor = UIColor(hex: 0xb5b5b5)
        tfSizeDirect.placeholderColor = UIColor(hex: 0x999999)
        tfSizeDirect.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        
        for btn in btnConds {
            btn.centerLabelVerticallyWithPadding(spacing: 20)
        }
        
        tvTag.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        tblTag.register(UINib(nibName: "TagListCell", bundle: nil), forCellReuseIdentifier: "Cell")
        //tblTag.isHidden = true
        tvTag.placeholder = "#태그 #태그 #태그 형식으로 입력해주세요."
        tvTag.delegate = self
        
        if PageStatus == "SIZE" {
            //직접 판매하기에서 들어왓다면(상품등록시) 사이즈목록api를 통해 다시 얻고 다른 페이지에서 들어왔다면 sizeType를 그대로 쓴다.
            if !isEdit {
                getSizeList()
            } else {  //상품 상세페이지, 상품 알람페이지에서 들어와다면
                size11 = pdtSize
                showSizeView(type: sizeType)
            }
        } else if PageStatus == "TAG" {
            if isEdit {
                tag = pdtTag
                tvTag.text = pdtTag
                tagAction(self)
            }
        } else if PageStatus == "CONDITION" {
            if isEdit {
                condition = pdtCondition
                for btn in btnConds {
                    btn.isSelected = condition == (btn.titleLabel?.text!)!
                    btn.borderColor = condition == (btn.titleLabel?.text!)! ? UIColor(hex: 0x42c2fe) : UIColor(hex: 0xe1e1e1)
                }
                condAction(self)
           }
        } else {
            getSizeList()
        }
    }
    
    @objc func hideKeyboard() {
        
        if activeTf != nil {
            activeTf?.resignFirstResponder()
        }
        
        tvTag.resignFirstResponder()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = keyboardSize.height
                    self.vwClose.isHidden = false
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func textChanged(_ sender : DesignableUITextField) {
        if sender.hasText {
            vwSizeDirect.borderColor = UIColor(hex: 0x42C2FE)
            lbSizeDirect.textColor = UIColor(hex: 0x42C2FE)
        } else {
            vwSizeDirect.borderColor = UIColor(hex: 0xE1E1E1)
            lbSizeDirect.textColor = UIColor(hex: 0x333333)
        }
    }
    
    @objc func directAction(_ sender : Any) {
        
        tfSizeDirect.becomeFirstResponder()
        btnFreeSize.isSelected = false
        btnFreeSize.borderColor = UIColor(hex: 0xe1e1e1)
        
    }
    
    func showSizeView(type : Int) {
        if type == 1 {  // 가로x세로x폭
            vwBagSize.isHidden = false
            vwAccSize.isHidden = true
            
        } else if type == 2 {  // FREE SIZE && 직접입력
            vwBagSize.isHidden = true
            vwAccSize.isHidden = false
        } else { //일반
            vwBagSize.isHidden = true
            vwAccSize.isHidden = true

            SizeSelect.show(self, dicSizeInfo: dicSizeInfo) { (size) in
                self.size11 = size
                self.lbSize.text = size
                gPdtRegInfo.setPdtSize(value: size)
            }
            return
        }
        
        if vwSize.tag == 0 {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.vwSize.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwSize.frame.origin.y))
                self.vwCond.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwTag.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwBtn.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                
                self.ivSize.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
                
            }, completion: { (finish) in
                
                if type == 1 {  // 가로x세로x폭
                    
                    if !self.size11.isEmpty {
                        let nsSize = self.size11 as NSString
                        let units = nsSize.components(separatedBy: " X ")
                        
                        if units.count < 3 {
                            self.tfWidth.text = ""
                            self.tfHeight.text = ""
                            self.tfDepth.text = ""
                        } else {
                            self.tfWidth.text = String(units[0])
                            self.tfHeight.text = String(units[1])
                            self.tfDepth.text = String(units[2])
                        }
                    } else {
                        self.tfWidth.text = ""
                        self.tfHeight.text = ""
                        self.tfDepth.text = ""
                    }
                    
                } else if type == 2 {  // FREE SIZE && 직접입력
                    
                    if !self.size11.isEmpty {
                        if self.size11 == "FREE SIZE" {
                            self.btnFreeSize.isSelected = true
                            self.btnFreeSize.borderColor = UIColor(hex: 0x42c2fe)
                            self.vwSizeDirect.borderColor = UIColor(hex: 0xE1E1E1)
                            self.lbSizeDirect.textColor = UIColor(hex: 0x333333)
                            self.tfSizeDirect.text = ""
                        } else {
                            self.btnFreeSize.isSelected = false
                            self.btnFreeSize.borderColor = UIColor(hex: 0xe1e1e1)
                            self.tfSizeDirect.text = self.size11
                            self.vwSizeDirect.borderColor = UIColor(hex: 0x42C2FE)
                            self.lbSizeDirect.textColor = UIColor(hex: 0x42C2FE)
                        }
                    } else {
                        self.btnFreeSize.isSelected = false
                        self.btnFreeSize.borderColor = UIColor(hex: 0xe1e1e1)
                        self.vwSizeDirect.borderColor = UIColor(hex: 0xE1E1E1)
                        self.lbSizeDirect.textColor = UIColor(hex: 0x333333)
                        self.tfSizeDirect.text = ""
                    }
                }
            })
            
        } else {
            
            hideKeyboard()
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwSize.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwCond.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwTag.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwBtn.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                
                self.ivSize.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(0)))
                
            }, completion: { (finish) in
                
                if type == 1 {  // 가로x세로x폭
                    if self.tfWidth.hasText && self.tfHeight.hasText && self.tfDepth.hasText {
                        self.size11 = String(format: "%@ X %@ X %@", self.tfWidth.text!, self.tfHeight.text!, self.tfDepth.text!)
                    }
                } else if type == 2 {    // 패션소품인 경우
                    if self.btnFreeSize.isSelected {
                        self.size11 = "FREE SIZE"
                    } else if self.tfSizeDirect.hasText {
                        self.size11 = self.tfSizeDirect.text!
                    }
                }
                gPdtRegInfo.setPdtSize(value: self.size11)
            })
        }
        vwSize.tag = 1 - vwSize.tag
    }
    
    @objc func checkNextButtonStatus(){
        if !size11.isEmpty && !condition.isEmpty {
            btnNext.isEnabled = true
            btnNext.setTitleColor(UIColor(hex: 0x00A6FF), for: .normal)
            btnNext.setTitleColor(UIColor(hex: 0x00A6FF), for: .selected)
        } else {
            btnNext.isEnabled = false
            btnNext.setTitleColor(UIColor(hex: 0x999999), for: .normal)
            btnNext.setTitleColor(UIColor(hex: 0x999999), for: .selected)
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////

    @IBAction func backAction(_ sender: Any) {
        
        popVC()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        /*
        if gPdtRegInfo.getPdtColor() == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_color_name", comment:""))
            return
        }
        
        if gPdtRegInfo.getPdtModel() == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_model_name", comment:""))
            return
        }
        
        if gPdtRegInfo.getContent() == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_description", comment:""))
            return
        }
        */
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_COST_VIEW")) as! CostSetViewController
        vc.isEdit = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func moreAction(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_SUB_INFO_VIEW")) as! SellProductSubInfoViewController
        vc.isEdit = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func sizeAction(_ sender : Any) {
        
        if !isEdit {
            showSizeView(type: dicSizeInfo.sizeType)
        } else {
            if sizeType == 1 {  // 가로x세로x폭
                if self.tfWidth.hasText && self.tfHeight.hasText && self.tfDepth.hasText {
                    self.size11 = String(format: "%@ * %@ * %@", self.tfWidth.text!, self.tfHeight.text!, self.tfDepth.text!)
                }
            } else if sizeType == 2 {    // 패션소품인 경우
                if self.btnFreeSize.isSelected {
                    self.size11 = "FREE SIZE"
                } else if self.tfSizeDirect.hasText {
                    self.size11 = self.tfSizeDirect.text!
                }
            }
            self.delegate?.setSize(size: size11)
            popVC()
        }
    }
    
    @objc func condAction(_ sender : Any) {
        
        if vwCond.tag == 0 {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwSize.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwCond.frame.origin.y + self.vwSizeHeader.frame.size.height))
                self.vwCond.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwCond.frame.origin.y))
                self.vwLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwTag.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwBtn.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                
                self.ivCond.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
                
            }, completion: { (finish) in
                
            })
            
        } else {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwSize.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwCond.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwTag.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwBtn.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                
                self.ivCond.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(0)))
                
            }, completion: { (finish) in
                
            })
        }
        vwCond.tag = 1 - vwCond.tag
    }
    
    @objc func tagAction(_ sender : Any) {
        
        if vwTag.tag == 0 {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwSize.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwSize.frame.origin.y))
                self.vwCond.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwCond.frame.origin.y))
                self.vwLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.vwLabel.frame.size.height))
                self.vwTag.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwTag.frame.origin.y))
                self.vwBtn.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                
                self.ivTag.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
                
            }, completion: { (finish) in
                
            })
            
        } else {
            
            if isEdit {
                self.delegate?.setTag(tag: tvTag.text!)
                popVC()
            } else {
                hideKeyboard()
                
                UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                    
                    self.vwSize.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                    self.vwCond.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                    self.vwLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                    self.vwTag.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                    self.vwBtn.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                    
                    self.ivTag.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(0)))
                    
                }, completion: { (finish) in
                    
                    self.tag = self.tvTag.text!
                    gPdtRegInfo.setTag(value: self.tvTag.text!)
                    
                })
            }
        }
        vwTag.tag = 1 - vwTag.tag
    }
    
    @objc func tagCellAction(_ sender : UIButton) {
//        if let sharpPos = tvTag.text.lastIndex(of: "#") {
//            tvTag.text = tvTag.text.replaceAll(tvTag.text.substring(from: sharpPos), with: "#구찌 ")
//        }
        tvTag.text = tvTag.text + arrTags[sender.tag]
    }
    
    @IBAction func condSelectAction(_ sender: UIButton) {
        
        for btn in btnConds {
            btn.isSelected = btn == sender
            btn.borderColor = btn == sender ? UIColor(hex: 0x42c2fe) : UIColor(hex: 0xe1e1e1)
        }
        
        condition = (sender.titleLabel?.text!)!
        
        if isEdit {
            self.delegate?.setCondition(condition: condition)
            popVC()
        } else {
            gPdtRegInfo.setPdtCondition(value: condition)
            condAction(sender)
        }
    }
    
    @IBAction func condGuideAction(_ sender: Any) {
        
        CondGuide.show(self)
    }
    
    @IBAction func inputAction(_ sender: Any) {
        
        hideKeyboard()
    }
    
    @IBAction func freeSizeAction(_ sender: Any) {
        
        size11 = "FREE SIZE"
        btnFreeSize.isSelected = true
        btnFreeSize.borderColor = UIColor(hex: 0x42c2fe)
        
        vwSizeDirect.borderColor = UIColor(hex: 0xE1E1E1)
        lbSizeDirect.textColor = UIColor(hex: 0x333333)
        
        sizeAction(sender)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 사이즈 목록 얻기
    func getSizeList() {
        
        gProgress.show()
        Net.getSizeList(
            accessToken     : gMeInfo.token,
            categoryUid     : gPdtRegInfo.getCategoryUid(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSizeListResult
                self.getSizeListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //사이즈 목록 얻기 결과
    func getSizeListResult(_ data: Net.getSizeListResult) {
        dicSizeInfo = data.dicInfo
    }
    
    // 태그검색
    func getSearchTag(searchkey : String) {
        
        let encodeStr : String = searchkey.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
        
        gProgress.show()
        Net.getSearchTag(
            accessToken : gMeInfo.token,
            keyword     : encodeStr,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSearchTagResult
                self.getSearchTagResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //태그검색 얻기 결과
    func getSearchTagResult(_ data: Net.getSearchTagResult) {
        arrTags = data.list
        tblTag.reloadData()
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension SellProductInfoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if textField == tfSizeDirect && newLength >= 11 {
            return false
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTf = textField as? DesignableUITextField
        
        if textField == tfSizeDirect {
            btnFreeSize.isSelected = false
            btnFreeSize.borderColor = UIColor(hex: 0xe1e1e1)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTf = nil
    }
    
}

extension SellProductInfoViewController : UITextViewDelegate {
 
    func textViewDidChange(_ textView: UITextView) {
        
//        lbTagPh.isHidden = textView.hasText
  
//        if textView.hasText {
//            if let sharpPos = textView.text.lastIndex(of: "#") {
//                let whiteRegex = try! NSRegularExpression(pattern: "\\s")
//                let spaceCnt = whiteRegex.numberOfMatches(in: textView.text, range: NSRange(location: 0, length: textView.text.utf16.count))
//
//                let sharpRegex = try! NSRegularExpression(pattern: "#")
//                let sharpCnt = sharpRegex.numberOfMatches(in: textView.text, range: NSRange(location: 0, length: textView.text.utf16.count))
//
//                let tagToSearch = textView.text.substring(from: sharpPos)
//                if tagToSearch.count > 1 && sharpCnt > spaceCnt {
//                    tblTag.isHidden = false
//                } else {
//                    tblTag.isHidden = true
//                }
//            }
//        } else {
//            tblTag.isHidden = true
//        }
        let tempstr : String = textView.text
        let index = tempstr.index(tempstr.endIndex, offsetBy: -1)
        let mySubstring = tempstr.suffix(from: index)
        print(">>>>>>>>>>>>>", mySubstring)
        let  char = mySubstring.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if textView == tvTag {
            
            if (isBackSpace == -60) {
                tvTag.text = tvTag.text + " #"
                self.getSearchTag(searchkey: "")
            } else {
                if let sharpPos = textView.text.lastIndex(of: "#") {
                    let tagToSearch = textView.text.substring(from: sharpPos)
                    self.getSearchTag(searchkey: tagToSearch)
                }
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == tvTag {
            
            if textView.text == "" || textView.text == " " {
                tvTag.text = tvTag.text + " #"
                self.getSearchTag(searchkey: "")
            } else {
                self.getSearchTag(searchkey: textView.text)
            }
            
        }
    }
}

extension SellProductInfoViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTags.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TagListCell
        
        cell.lbName.text = "#" + arrTags[indexPath.row]
        cell.lbCnt.isHidden = true
        cell.btnCell.tag = indexPath.row
        cell.btnCell.addTarget(self, action: #selector(tagCellAction), for: .touchUpInside)
        return cell
    }
    
}
