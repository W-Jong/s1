//
//  SellServiceRequestViewController.swift
//  selluv
//  발렛신청
//  Created by Gambler on 1/13/18.
//  modified by PJh ont 26/03/18.

import UIKit

class SellServiceRequestViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var lcContentBottom: NSLayoutConstraint!
    
    @IBOutlet weak var vwKit: UIView!
    @IBOutlet weak var ivKit: UIImageView!
    @IBOutlet weak var lbKitTitle: UILabel!
    @IBOutlet weak var lbKitDesc: UILabel!
    @IBOutlet weak var vwDirect: UIView!
    @IBOutlet weak var ivDirect: UIImageView!
    @IBOutlet weak var lbDirectTitle: UILabel!
    @IBOutlet weak var lbDirectDesc: UILabel!
    
    @IBOutlet var btnAddrs: [UIButton]!
    @IBOutlet weak var btnAddrSelect: UIButton!
    
    @IBOutlet weak var tfName: DesignableUITextField!
    @IBOutlet weak var tfContact: DesignableUITextField!
    @IBOutlet weak var tfAddr: DesignableUITextField!
    
    @IBOutlet weak var lbAddr: UILabel!
    @IBOutlet weak var vwRCost: UIView!
    @IBOutlet weak var ivRCost: UIImageView!
    @IBOutlet weak var vwDCost: UIView!
    @IBOutlet weak var ivDCost: UIImageView!
    @IBOutlet weak var btnDCost: UIButton!
    
    @IBOutlet weak var btnRequest: UIButton!
    
    var addrTabIndex : Int = 0
    var addrInfo =
        [(name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : "")]

    var dCost : Double = 0 {
        didSet {
            btnDCost.isHidden = dCost <= 0
            btnDCost.setTitle(CommonUtil.formatNum(Int(dCost)) + "원", for: .normal)
            ivRCost.image = dCost == 0 ? #imageLiteral(resourceName: "ic_checkbox_on") : #imageLiteral(resourceName: "ic_checkbox_off")
            ivDCost.image = dCost == 0 ? #imageLiteral(resourceName: "ic_checkbox_off") : #imageLiteral(resourceName: "ic_checkbox_on")
        }
    }
    
    var sendType : Int = 0
    var reqPrice : Int = 0
    var name     : String = ""
    var contact  : String = ""
    var address  : String = ""
    var brandName : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnAddrs[0].isSelected = true
        btnAddrs[0].backgroundColor = UIColor(hex: 0x42c2fe)
        
        vwKit.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(typeAction)))
        vwDirect.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(typeAction)))
        
        vwRCost.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(costAction)))
        vwDCost.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(costAction)))
        
        tfName.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        tfContact.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        tfAddr.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        
        tfAddr.isEnabled = false
        
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            addrInfo[i].name = dic.addressName
            addrInfo[i].contact = dic.addressPhone
            addrInfo[i].addr = dic.addressDetail
            addrInfo[i].code = dic.addressDetailSub
        }
        
        addrTabIndex = 0
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            if dic.status == 2 {
                addrTabIndex = i
            }
        }
        selectTabBtn(index: addrTabIndex)
    }
    
    func selectTabBtn(index : Int) {
        
        for i in 0..<3 {
            btnAddrs[i].isSelected = false
            btnAddrs[i].backgroundColor = UIColor(hex: 0xffffff)
        }
        
        btnAddrs[index].isSelected = true
        btnAddrs[index].backgroundColor = UIColor(hex: 0x42c2fe)
        
        addrTabIndex = index
        
        tfName.text = addrInfo[index].name
        tfContact.text = addrInfo[index].contact
        if addrInfo[index].addr != "" && addrInfo[index].code != ""{
            lbAddr.text = String.init(format: "%@\n[%@]", addrInfo[index].addr, addrInfo[index].code)
            tfAddr.isHidden = true
            lbAddr.isHidden = false
        } else {
            lbAddr.text = ""
            tfAddr.isHidden = false
            lbAddr.isHidden = true
        }
        
        setRequestBtn()
    }
    
    @objc func textChanged(_ sender : DesignableUITextField) {
        
        switch sender {
            case tfName :
                addrInfo[addrTabIndex].name = sender.text!
                name = sender.text!
            case tfContact:
                addrInfo[addrTabIndex].contact = sender.text!
                contact = sender.text!
            default:
                break
        }
        setRequestBtn()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                lcContentBottom.constant = -keyboardSize.height
                
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        
        lcContentBottom.constant = 0
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func setRequestBtn() {
        
        var addrInputed = false
        
        if !(addrInfo[addrTabIndex].name.isEmpty) && !(addrInfo[addrTabIndex].contact.isEmpty) && !(addrInfo[addrTabIndex].addr.isEmpty) && !(addrInfo[addrTabIndex].code.isEmpty) {
            addrInputed = true
        }

        if !addrInputed || sendType == 0{
            btnRequest.isEnabled = false
        } else {
            btnRequest.isEnabled = true
        }
        btnRequest.backgroundColor = btnRequest.isEnabled ? UIColor.black : UIColor(hex: 0x999999)
    }
    
    @objc func typeAction(_ sender : UITapGestureRecognizer) {
        
        if sender.view == vwKit {
            ivKit.image = sender.view?.tag == 0 ? #imageLiteral(resourceName: "ic_kit_delivery_active") : #imageLiteral(resourceName: "ic_kit_delivery")
            lbKitTitle.textColor = sender.view?.tag == 0 ? UIColor(hex: 0x42c2fe) : UIColor.black
            lbKitDesc.textColor = sender.view?.tag == 0 ? UIColor(hex: 0x42c2fe) : UIColor(hex: 0x999999)
            vwKit.layer.borderColor = sender.view?.tag == 0 ? UIColor(hex: 0x42c2fe).cgColor : UIColor(hex: 0xe1e1e1).cgColor
            
            ivDirect.image = sender.view?.tag == 0 ? #imageLiteral(resourceName: "ic_direct_delivery") : ivDirect.image
            lbDirectTitle.textColor = sender.view?.tag == 0 ? UIColor.black : lbDirectTitle.textColor
            lbDirectDesc.textColor = sender.view?.tag == 0 ? UIColor(hex: 0x999999) : lbDirectDesc.textColor
            vwDirect.layer.borderColor = sender.view?.tag == 0 ? UIColor(hex: 0xe1e1e1).cgColor : vwDirect.layer.borderColor
            
            sendType = 1
        } else {
            ivDirect.image = sender.view?.tag == 0 ? #imageLiteral(resourceName: "ic_direct_delivery_active") : #imageLiteral(resourceName: "ic_direct_delivery")
            lbDirectTitle.textColor = sender.view?.tag == 0 ? UIColor(hex: 0x42c2fe) : UIColor.black
            lbDirectDesc.textColor = sender.view?.tag == 0 ? UIColor(hex: 0x42c2fe) : UIColor(hex: 0x999999)
            vwDirect.layer.borderColor = sender.view?.tag == 0 ? UIColor(hex: 0x42c2fe).cgColor : UIColor(hex: 0xe1e1e1).cgColor
            
            ivKit.image = sender.view?.tag == 0 ? #imageLiteral(resourceName: "ic_kit_delivery") : ivKit.image
            lbKitTitle.textColor = sender.view?.tag == 0 ? UIColor.black : lbKitTitle.textColor
            lbKitDesc.textColor = sender.view?.tag == 0 ? UIColor(hex: 0x999999) : lbKitDesc.textColor
            vwKit.layer.borderColor = sender.view?.tag == 0 ? UIColor(hex: 0xe1e1e1).cgColor : vwKit.layer.borderColor
            
            sendType = 2
        }
        
        sender.view?.tag = 1 - (sender.view?.tag)!
        
        setRequestBtn()
    }
    
    @objc func costAction(_ sender : UITapGestureRecognizer) {
        
        if sender.view == vwRCost {
            
            dCost = 0
            reqPrice = 0
        } else {
            
            ValetCost.show(self, cost: Double(reqPrice), okCallback: { (cost) in
                self.dCost = cost
                self.reqPrice = Int(cost)
            })
            
        }
        
    }
    
    func goRequsetFinishPage() {
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Sell", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "SELL_VALET_COMPLETE_VIEW")) as! ValetCompleteViewController
        
        vc.sendType = sendType
        vc.reqPrice = reqPrice
        vc.name = addrInfo[addrTabIndex].name
        vc.contact = addrInfo[addrTabIndex].contact
        vc.address = (addrInfo[addrTabIndex].code + "\n" + addrInfo[addrTabIndex].addr)
        vc.brand = brandName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func backAction(_ sender: Any) {
        
        popVC()
        
    }
    
    @IBAction func addrNumAction(_ sender: UIButton) {
        
       selectTabBtn(index: sender.tag)
    }
    
    @IBAction func addrSearchAction(_ sender: Any) {
        
        AddrSearch.show(self) { (addr, code) in
            
            self.lbAddr.text = String.init(format: "%@\n[%@]", addr, code)
            if self.lbAddr.text == "" {
                self.tfAddr.isHidden = false
                self.lbAddr.isHidden = true
            } else {
                self.tfAddr.isHidden = true
                self.lbAddr.isHidden = false
            }
            
            self.addrInfo[self.addrTabIndex].addr = addr
            self.addrInfo[self.addrTabIndex].code = code
        }
        setRequestBtn()
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        
        tfName.resignFirstResponder()
        tfContact.resignFirstResponder()
    }
    
    @IBAction func requestAction(_ sender: Any) {
        var likegroup = ""
        if gPdtRegInfo.getPdtGroup() == "MALE" {
            likegroup = NSLocalizedString("male", comment:"")
        } else if gPdtRegInfo.getPdtGroup() == "FEMALE" {
            likegroup = NSLocalizedString("female", comment:"")
        } else {
            likegroup = NSLocalizedString("kids", comment:"")
        }
        
        brandName = String.init(format: "%@ %@ %@", gPdtRegInfo.getNameEn() , likegroup, gPdtRegInfo.getCategoryName())
        ValetPolicy.show(self, name: brandName, okCallback: { (sign) in
            
            let data = UIImageJPEGRepresentation(sign, 0.8)
            self.uploadFile(data: data!)

        }) {
            self.pushVC("MYPAGE_SALEPOLICY_VIEW", storyboard: "Mypage", animated: true)
        }
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 한개 파일 업로드
    func uploadFile( data : Data) {
        
        gProgress.show()
        Net.uploadFile(
            file            : data,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UploadFileResult
                self.reqValet(_singimg: res.fileName)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 발렛신청
    func reqValet( _singimg : String){
        
        gProgress.show()
        Net.reqValet(
            accessToken     : gMeInfo.token,
            brandUid        : gPdtRegInfo.getBrandUid(),
            categoryUid     : gPdtRegInfo.getCategoryUid(),
            reqAddress      : (addrInfo[addrTabIndex].code + "\n" + addrInfo[addrTabIndex].addr),
            reqName         : addrInfo[addrTabIndex].name,
            reqPhone        : addrInfo[addrTabIndex].contact,
            reqPrice        : reqPrice,
            sendType        : sendType,
            signImg         : _singimg,
            success: { (result) -> Void in
                gProgress.hide()
                
                gPdtRegInfo.initial()
                self.goRequsetFinishPage()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension SellServiceRequestViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfName {
            tfContact.becomeFirstResponder()
        } else if textField == tfAddr {
            tfAddr.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
        
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        for btn in btnAddrs {
//            if btn.isSelected {
//                switch textField {
//                case tfName :
//                    addrInfo[btn.tag].name = textField.text!
//                case tfContact:
//                    addrInfo[btn.tag].contact = textField.text!
//                case tfAddr:
//                    addrInfo[btn.tag].addr = textField.text!
//                default:
//                    break
//                }
//            }
//        }
//
//        setRequestBtn()
//
//    }
    
}

