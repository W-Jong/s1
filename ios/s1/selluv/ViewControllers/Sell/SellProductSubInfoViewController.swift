//
//  SellProductSubInfoViewController.swift
//  selluv
//
//  Created by Gambler on 1/9/18.
//

import UIKit

protocol SellProductSubInfoViewControllerDelegate {
    func setModel( model : String)
    func setAcc( acc : String, etc : String, arrIndexs : String)
    func setDesc( desc : String)
}

class SellProductSubInfoViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    var delegate:  SellProductSubInfoViewControllerDelegate?
    @IBOutlet weak var scvMainTop: NSLayoutConstraint!
    
    @IBOutlet weak var vwEtcLabel: UIView!
    
    @IBOutlet weak var vwAcc: UIView!
    @IBOutlet weak var vwAccHeader: UIView!
    @IBOutlet weak var ivAcc: UIImageView!
    @IBOutlet weak var lbAcc: UILabel!
    @IBOutlet weak var scvAcc: UIScrollView!
    @IBOutlet weak var scvAccBottom: NSLayoutConstraint!
    @IBOutlet var btnAccs: [UIButton]!
    @IBOutlet var ivAccs: [UIImageView]!
    @IBOutlet var lbAccs: [UILabel]!
    
    @IBOutlet weak var btnEtc: UIButton!
    @IBOutlet weak var tfDirectAcc: DesignableUITextField!
    
    @IBOutlet weak var vwColor: UIView!
    @IBOutlet weak var vwColorHeader: UIView!
    @IBOutlet weak var lbColor: UILabel!
    @IBOutlet weak var ivColor: UIImageView!
    @IBOutlet weak var clvColor: UICollectionView!
    
    @IBOutlet weak var vwModel: UIView!
    @IBOutlet weak var vwModelHeader: UIView!
    @IBOutlet weak var lbModel: UILabel!
    @IBOutlet weak var lbReconModel: UILabel!
    @IBOutlet weak var ivModel: UIImageView!
    @IBOutlet weak var tfModel: DesignableUITextField!
    @IBOutlet weak var tblModel: UITableView!
    
    @IBOutlet weak var vwDetailLabel: UIView!
    @IBOutlet weak var vwDetail: UIView!
    @IBOutlet weak var vwDetailHeader: UIView!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var vwDetailContent: UIView!
    @IBOutlet weak var vwDetailContentHeight: NSLayoutConstraint!

    @IBOutlet weak var lbDetailCnt: UILabel!
    @IBOutlet var tvDetail: KMPlaceholderTextView!
    
    @IBOutlet weak var vwClose: UIView!
    @IBOutlet weak var lcContentBottom: NSLayoutConstraint!
    
    var arrModelList    : Array<String> = []
    var PageStatus      : String!
    var isEdit          : Bool!
    var brandUid        : Int!
    var pdtModel        : String!
    var pdtAccr         : String!
    var pdtContent      : String!
    var pdtEtc          : String!

    var acc = "" {
        didSet {
            lbAcc.text = acc
        }
    }
    
    var model = "" {
        didSet {
            lbModel.text = model
        }
    }
    
    var color : ColorType? {
        didSet {
            lbColor.text = color?.name()
        }
    }
    
    var desc = "" {
        didSet {
            if vwDetail.tag == 1 {
                btnDetail.setTitle("닫기", for: .normal)
            } else {
                btnDetail.setTitle(desc.isEmpty ? "입력" : "수정", for: .normal)
            }
            lbDetailCnt.text = String.init(format: "%d", desc.count)
        }
    }
    
    let colorLabelArray : [ColorType]  = [ColorType.black, ColorType.grey, ColorType.white, ColorType.beizy, ColorType.red, ColorType.pink, ColorType.purple, ColorType.blue, ColorType.green, ColorType.yellow, ColorType.orange, ColorType.brown, ColorType.gold, ColorType.silver, ColorType.multi]
    
    let accList = [#imageLiteral(resourceName: "ic_sell_tag"), #imageLiteral(resourceName: "ic_sell_card"), #imageLiteral(resourceName: "ic_sell_receipt"), #imageLiteral(resourceName: "ic_sell_acc"), #imageLiteral(resourceName: "ic_sell_box"), #imageLiteral(resourceName: "ic_sell_dustbag"), #imageLiteral(resourceName: "ic_sell_etc")]
    let activeAccList = [#imageLiteral(resourceName: "ic_sell_tag_active"), #imageLiteral(resourceName: "ic_sell_card_active"), #imageLiteral(resourceName: "ic_sell_receipt_active"), #imageLiteral(resourceName: "ic_sell_acc_active"), #imageLiteral(resourceName: "ic_sell_box_active"), #imageLiteral(resourceName: "ic_sell_dustbag_active"), #imageLiteral(resourceName: "ic_sell_etc_active")]
    let componentNames = ["상품택", "개런티카드", "영수증", "여분부속품", "브랜드박스", "더스트백"]
    
    var activeTf : DesignableUITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwAccHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(accHeaderAction)))
        vwColorHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorHeaderAction)))
        vwModelHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(modelHeaderAction)))
        vwDetailHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(detailHeaderAction)))
        
        tblModel.register(UINib(nibName: "ShopCategoryListCell", bundle: nil), forCellReuseIdentifier: "Cell")
        clvColor.register(UINib(nibName: "ColorCell", bundle: nil), forCellWithReuseIdentifier: "ColorCell")
        
        tfDirectAcc.text = ""
        tfDirectAcc.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        tfModel.text = ""
        vwDetailContent.isHidden = true
        tvDetail.placeholder = NSLocalizedString("empty_pdt_desc", comment:"")
        
        if PageStatus == "ACCR" {
            if isEdit {
                
                for btn in btnAccs {
                    btn.borderColor = UIColor(hex: 0xe1e1e1)
                    lbAccs[btn.tag].textColor = UIColor(hex: 0x999999)
                    ivAccs[btn.tag].image = accList[btn.tag]
                }
                
                let names = pdtAccr.components(separatedBy: " ")
                for i in 0 ..< names.count {
                    if names[i] == "" {
                        continue
                    }
                    
                    let index = isContainNames(name: names[i])
                    if index == -1 {
//                        gitanames = gitanames + names[i]
//                        btnEtc.borderColor = UIColor(hex: 0x42c2fe)
//                        lbAccs[6].textColor = UIColor(hex: 0x42c2fe)
//                        ivAccs[6].image = activeAccList[6]
                    } else {
                        btnAccs[index].borderColor = UIColor(hex: 0x42c2fe)
                        lbAccs[btnAccs[index].tag].textColor = UIColor(hex: 0x42c2fe)
                        ivAccs[btnAccs[index].tag].image = activeAccList[btnAccs[index].tag]
                    }
                }
                
                if pdtEtc != "" {
                    btnEtc.borderColor = UIColor(hex: 0x42c2fe)
                    lbAccs[6].textColor = UIColor(hex: 0x42c2fe)
                    ivAccs[6].image = activeAccList[6]
                }
                
                lbAcc.text = pdtEtc
                acc = pdtAccr + pdtEtc
                accHeaderAction(self)
            }
        } else if PageStatus == "MODEL" {
            if isEdit {
                model = pdtModel
                tfModel.text = model
                modelHeaderAction(self)
                getModelNmsList()
            }
        } else if PageStatus == "DESC" {
            if isEdit {
                desc = pdtContent
                tvDetail.text = desc
                detailHeaderAction(self)
            }
        }
    }
    
    func isContainNames(name : String) -> Int {
        
        for k in 0 ..< componentNames.count {
            if name == componentNames[k] {
                return k
            }
        }
        return -1
    }
    
    @objc func hideKeyboard() {
        
        tfModel.resignFirstResponder()
        tfDirectAcc.resignFirstResponder()
        tvDetail.resignFirstResponder()
    
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    if self.activeTf != nil && self.activeTf == self.tfDirectAcc {
                        self.scvAccBottom.constant = keyboardSize.height + self.vwClose.frame.size.height
                    }
                    if self.activeTf == nil {
                        self.scvMainTop.constant = -keyboardSize.height
                    }
                    
                    self.lcContentBottom.constant = keyboardSize.height
                    self.vwClose.isHidden = false
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.scvAccBottom.constant = 0
            self.scvMainTop.constant = 0
            self.lcContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func backAction(_ sender: Any) {
        
        if lbAcc.text != "" || lbColor.text != "" || lbModel.text != "" || tvDetail.text != "" {
            
            let alertController = UIAlertController(title: NSLocalizedString("alarm", comment:""), message: NSLocalizedString("pdt_reg_alert_msg", comment:""), preferredStyle: .alert)
            let actOK = UIAlertAction(title: NSLocalizedString("yes1", comment:""), style: .default) { (action:UIAlertAction) in
                
                gPdtRegInfo.setPdtCondition(value: "")
                gPdtRegInfo.setPdtColor(value: "")
                gPdtRegInfo.setPdtModel(value: "")
                gPdtRegInfo.setContact(value: "")
                self.popVC()
            }
            
            let actCancel = UIAlertAction(title: NSLocalizedString("no", comment:""), style: .default) { (action:UIAlertAction) in

                gPdtRegInfo.setPdtCondition(value: self.lbAcc.text!)
                gPdtRegInfo.setPdtColor(value: self.lbColor.text!)
                gPdtRegInfo.setPdtModel(value: self.self.lbModel.text!)
                gPdtRegInfo.setContact(value: self.tvDetail.text)
                self.popVC()
            }
            
            alertController.addAction(actOK)
            alertController.addAction(actCancel)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            popVC(  )
        }
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        if isEdit {
            self.delegate?.setDesc(desc: self.tvDetail.text)
        } else {
            gPdtRegInfo.setContent(value: self.tvDetail.text)
        }
        popVC()
    }
    
    @IBAction func accAction(_ sender: UIButton) {
        
//        btnEtc.isSelected = false
//        lbAccs[6].textColor = UIColor(hex: 0x999999)
//        ivAccs[6].image = #imageLiteral(resourceName: "ic_sell_etc")

//        for btn in btnAccs {
//            btn.isSelected = btn == sender
//
//            if btn == sender {
//                btn.borderColor = UIColor(hex: 0x42c2fe)
//                lbAccs[btn.tag].textColor = UIColor(hex: 0x42c2fe)
//                ivAccs[btn.tag].image = activeAccList[btn.tag]
//            } else {
//                btn.borderColor = UIColor(hex: 0xe1e1e1)
//                lbAccs[btn.tag].textColor = UIColor(hex: 0x999999)
//                ivAccs[btn.tag].image = accList[btn.tag]
//            }
//        }
//
//        accHeaderAction(sender)
        
        let index : Int = sender.tag
        if btnAccs[index].isSelected {
            btnAccs[index].borderColor = UIColor(hex: 0xe1e1e1)
            lbAccs[index].textColor = UIColor(hex: 0x999999)
            ivAccs[index].image = accList[index]
            btnAccs[index].isSelected = false
        } else {
            btnAccs[index].borderColor = UIColor(hex: 0x42c2fe)
            lbAccs[index].textColor = UIColor(hex: 0x42c2fe)
            ivAccs[index].image = activeAccList[index]
            btnAccs[index].isSelected = true
        }
    }
    
    @IBAction func inputAction(_ sender: Any) {
        
        hideKeyboard()
        
    }
    
    @objc func textChanged(_ sender : DesignableUITextField) {
        
        if sender == tfDirectAcc {
            btnEtc.isSelected = sender.hasText
            
            if btnEtc.isSelected {
                lbAccs[6].textColor = UIColor(hex: 0x42c2fe)
                ivAccs[6].image = #imageLiteral(resourceName: "ic_sell_etc_active")
                
//                for btn in btnAccs {
//                    if btn != btnEtc {
//                        btn.isSelected = false
//                        btn.borderColor = UIColor(hex: 0xe1e1e1)
//                        lbAccs[btn.tag].textColor = UIColor(hex: 0x999999)
//                        ivAccs[btn.tag].image = accList[btn.tag]
//                    }
//                }
            } else {
                lbAccs[6].textColor = UIColor(hex: 0x999999)
                ivAccs[6].image = #imageLiteral(resourceName: "ic_sell_etc")
            }
        }
        
    }
    
    
    @objc func accHeaderAction(_ sender : Any) {
        
        hideKeyboard()
        
        if vwAcc.tag == 0 {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.vwAcc.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwAcc.frame.origin.y))
                self.vwColor.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwModel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwDetailLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwDetail.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                
                self.ivAcc.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
                
            }, completion: { (finish) in
                
//                if self.acc == self.tfDirectAcc.text {
//                    self.btnEtc.isSelected = true
//                } else {
//                    self.btnEtc.isSelected = false
//
//                    for nInd in 0 ..< self.lbAccs.count {
//                        if self.lbAccs[nInd].text == self.acc {
//                            self.btnAccs[nInd].isSelected = true
//                            self.btnAccs[nInd].borderColor = UIColor(hex: 0x42c2fe)
//                            self.lbAccs[nInd].textColor = UIColor(hex: 0x42c2fe)
//                            self.ivAccs[nInd].image = self.activeAccList[nInd]
//                        } else {
//                            self.btnAccs[nInd].isSelected = false
//                            self.btnAccs[nInd].borderColor = UIColor(hex: 0xe1e1e1)
//                            self.lbAccs[nInd].textColor = UIColor(hex: 0x999999)
//                            self.ivAccs[nInd].image = self.accList[nInd]
//                        }
//                    }
//                }
                
            })
            
        } else {
            
            var names : String = ""
            var indexs : String = ""
            for i in 0..<6 {
                if self.btnAccs[i].isSelected {
                    names = String.init(format: "%@ %@", names , componentNames[i])
                    indexs = indexs + "1"
                } else {
                    indexs = indexs + "0"
                }
            }
            let trimmedIndexs = indexs.trimmingCharacters(in: .whitespaces)
            
            var etcnames : String = ""
            if self.btnEtc.isSelected {
                etcnames = self.tfDirectAcc.text!
            }
            self.acc = names
            
            if self.isEdit {
                lbAcc.text = names
                self.delegate?.setAcc(acc: names, etc: etcnames, arrIndexs: trimmedIndexs)
                self.popVC()
            }
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwAcc.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwColor.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwModel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwDetailLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwDetail.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                
                self.ivAcc.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(0)))
                
            }, completion: { (finish) in
                
//                accHeaderAction(sender)
                self.lbAcc.text = names
                gPdtRegInfo.setEtc(value: etcnames)
                gPdtRegInfo.setComponent(value: trimmedIndexs)
            })
        }
        
        vwAcc.tag = 1 - vwAcc.tag
        
    }
    
    @objc func colorHeaderAction(_ sender : Any) {
        
        hideKeyboard()
        
        if vwColor.tag == 0 {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.vwAcc.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwAcc.frame.origin.y))
                self.vwColor.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwColor.frame.origin.y))
                self.vwModel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwDetailLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwDetail.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                
                self.ivColor.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
                
            }, completion: { (finish) in
                
                self.clvColor.reloadData()
                
            })
            
        } else {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwAcc.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwColor.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwModel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwDetailLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwDetail.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                
                self.ivColor.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(0)))
                
            }, completion: { (finish) in
                
            })
        }
        
        vwColor.tag = 1 - vwColor.tag
        
    }
    
    @objc func modelHeaderAction(_ sender : Any) {
        
        hideKeyboard()
        
        if vwModel.tag == 0 {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.lbReconModel.text = gPdtRegInfo.getNameko()
                
                self.vwAcc.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwAcc.frame.origin.y))
                self.vwColor.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwColor.frame.origin.y))
                self.vwModel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwModel.frame.origin.y))
                self.vwDetailLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                self.vwDetail.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: self.view.frame.size.height))
                
                self.ivModel.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
                
            }, completion: { (finish) in
                
                self.getModelNmsList()
            })
            
        } else {
            
            if isEdit {
                self.delegate?.setModel(model: tfModel.text!)
                self.popVC()
            }
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseIn, animations: {
                
                self.vwAcc.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwColor.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwModel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwDetailLabel.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                self.vwDetail.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
                
                self.ivModel.layer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(0)))
                
            }, completion: { (finish) in
                
                self.model = self.tfModel.text!
                gPdtRegInfo.setPdtModel(value: self.tfModel.text!)
                
            })
        }
        vwModel.tag = 1 - vwModel.tag
        
    }
    
    @objc func detailHeaderAction(_ sender : Any) {
        
        if vwDetail.tag == 0 {
            
            vwDetailContentHeight.constant = 0
            vwDetailContent.isHidden = false
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.vwDetailContentHeight.constant = 115
                self.view.layoutIfNeeded()
                
            }, completion: { (finish) in
                
                self.btnDetail.setTitle("닫기", for: .normal)
                
            })
            
        } else {
            
            UIView.animate(withDuration: SellAnimationTime, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.vwDetailContentHeight.constant = 0
                self.view.layoutIfNeeded()
                
            }, completion: { (finish) in
                
                self.vwDetailContent.isHidden = true
                self.btnDetail.setTitle(self.desc.isEmpty ? "입력" : "수정", for: .normal)
            })
        }
        
        vwDetail.tag = 1 - vwDetail.tag
        
    }
    
    @objc func modelSelAction(_ sender : UIButton) {
        
        tfModel.text = arrModelList[sender.tag]
        model = arrModelList[sender.tag]
        if isEdit {
            self.delegate?.setModel(model: model)
            popVC()
        } else {
            gPdtRegInfo.setPdtModel(value: model)
            modelHeaderAction(sender)
        }
    }
    
    //상세 입력
    @IBAction func onClickDetailInput(_ sender: Any) {
        detailHeaderAction(self)
    }
    
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 브랜드에 따르는 추천모델명 리스트 얻기
    func getModelNmsList() {
        
        gProgress.show()
        Net.getBrandModelNmsList(
            accessToken     : gMeInfo.token,
            brandUid        : gPdtRegInfo.getBrandUid(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getBrandModelNmsListResult
                self.getBrandModelNmsListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //브랜드에 따르는 추천모델명 리스트 얻기 결과
    func getBrandModelNmsListResult(_ data: Net.getBrandModelNmsListResult) {
        arrModelList = data.list
        tblModel.reloadData()
    }
}

extension SellProductSubInfoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if (textField == tfDirectAcc || textField == tfModel) && newLength >= 11 {
            return false
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTf = (textField as! DesignableUITextField)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        activeTf = nil
    }
    
}

extension SellProductSubInfoViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let strNew = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let len = strNew.count
        
        if textView == tvDetail && len > 300 { //. 300자 제한
            return false
        }
        
        return true
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        desc = textView.text
        
    }
    
}

extension SellProductSubInfoViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrModelList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ShopCategoryListCell
        
        cell.lbName.text = arrModelList[indexPath.row]
        cell.lbName.textColor = UIColor(hex: 0x333333)
        cell.lbName.font = UIFont.systemFont(ofSize: 14)
        cell.btnCell.tag = indexPath.row
        cell.btnCell.addTarget(self, action: #selector(self.modelSelAction), for: .touchUpInside)
        cell.backgroundColor = UIColor(hex: 0xf8f8fa)
        
        return cell
        
    }
    
}

extension SellProductSubInfoViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.size.width)/4.0, height: (collectionView.frame.size.width)/4.0)
        
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorLabelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath as IndexPath) as! ColorCell
        
        cell._lbColor.text = colorLabelArray[indexPath.row].rawValue
        cell.delegate = self
        cell.tag = indexPath.row
        
        cell._imvSelectOn.isHidden = !(color == colorLabelArray[indexPath.row])
        
        switch colorLabelArray[indexPath.row] {
        
        case ColorType.black :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.image = nil
            
        case ColorType.grey :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3c3c3c).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4a4a4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.white :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xdddddd).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffffff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.beizy :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc4bab0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xf5e9dc).cgColor
            cell._imvColor.image = nil
            
        case ColorType.red :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc2900).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff3300).cgColor
            cell._imvColor.image = nil
            
        case ColorType.pink :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc868ba).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xfa82e8).cgColor
            cell._imvColor.image = nil
            
        case ColorType.purple :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x7e27cf).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x9b30ff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.blue :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3e75b3).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4e92e0).cgColor
            cell._imvColor.image = nil
            
        case ColorType.green :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x33b13b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x40dd4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.yellow :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xccac00).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffd700).cgColor
            cell._imvColor.image = nil
            
        case ColorType.orange :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc4a0b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff5d03).cgColor
            cell._imvColor.image = nil
            
        case ColorType.brown :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x6e4626).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x8a572f).cgColor
            cell._imvColor.image = nil
            
        case ColorType.gold :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_gold")
            
        case ColorType.silver :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_silver")
            
        case ColorType.multi :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_multi")
            
        }
        
        return cell
    }
    
}

extension SellProductSubInfoViewController : ColorCellDelegate {
    
    func ColorCellDelegate(_ colorCell : ColorCell, onItemClicked sender : Any, tag: Int!) {
        colorCell._imvSelectOn.isHidden = !colorCell._imvSelectOn.isHidden
        
        if colorCell._imvSelectOn.isHidden == false {
            color = colorLabelArray[tag]
            gPdtRegInfo.setPdtColor(value: (color?.name())!)
            clvColor.reloadData()
            colorHeaderAction(colorCell)
        } else {
            color = nil
        }
        
    }
    
}
