//
//  SellPolicy.swift
//  selluv
//
//  Created by Gambler on 12/29/17.
//

import UIKit

class SellPolicy: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var ivProduct: UIImageView!
    @IBOutlet var lbEBrand: UILabel!
    @IBOutlet var lbKBrand: UILabel!
    @IBOutlet var lbSize: UILabel!
    @IBOutlet var lbCost: UILabel!
    @IBOutlet var vwSign: UIView!
    @IBOutlet var ivSign: UIImageView!
    @IBOutlet var dsvSign: YPDrawSignatureView!
    @IBOutlet var btnOk: UIButton!
    
    var cbOk: callback! = nil
    var cbGuide: callback1! = nil
    
    public typealias callback = (_ sign : UIImage) -> ()
    public typealias callback1 = () -> ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(okCallback: callback! = nil, guideCallback: callback1! = nil) {
        self.init()
        
        cbOk = okCallback
        cbGuide = guideCallback
    }
    
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, okCallback: callback! = nil, guideCallback: callback1! = nil) {
        let sizePopup = SellPolicy(okCallback: okCallback, guideCallback: guideCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        vwSign.layer.borderWidth = 1
        vwSign.layer.borderColor = UIColor(hex: 0xe1e1e1).cgColor
        
        vwDlg.layer.cornerRadius = 6
        
        dsvSign.delegate = self
        
        //set data
        lbEBrand.text = gPdtRegInfo.getNameEn()
        lbSize.text = gPdtRegInfo.getPdtSize()
        lbCost.text = "₩ " + CommonUtil.formatNum(gPdtRegInfo.getPrice())
        
        var likegroup : String = ""
        if gPdtRegInfo.getPdtGroup() == "MALE" {
            likegroup = NSLocalizedString("male", comment:"")
        } else if gPdtRegInfo.getPdtGroup() == "FEMALE" {
            likegroup = NSLocalizedString("female", comment:"")
        } else {
            likegroup = NSLocalizedString("kids", comment:"")
        }
        
        let pdtInfo = String.init(format: "%@ %@ %@ %@ %@", gPdtRegInfo.nameKo, likegroup, gPdtRegInfo.getPdtColor(), gPdtRegInfo.getPdtModel(), gPdtRegInfo.getCategoryName())
        lbKBrand.text = pdtInfo
        
        ivProduct.kf.setImage(with: URL(string: gPdtRegInfo.getPdtThumb()), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func guideAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbGuide != nil {
            cbGuide()
        }
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(dsvSign.getSignature()!)
        }
    }
    
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension SellPolicy : YPSignatureDelegate {
    
    func didStart() {
        ivSign.isHidden = true
        btnOk.isEnabled = true
        btnOk.layer.backgroundColor = UIColor.black.cgColor
    }
    
    func didFinish() {
        print("finish")
    }
    
}

