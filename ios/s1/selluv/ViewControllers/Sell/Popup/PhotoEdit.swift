//
//  PhotoEdit.swift
//  selluv
//
//  Created by Gambler on 1/8/18.
//

import UIKit
import Toast_Swift

class PhotoEdit: BaseViewController {
    
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    var imgList = [UIImage]() {
        didSet {
            lbTitle.text = String.init(format: "%d / %d", curInd, imgList.count)
        }
    }
    
    var curInd = 1 {
        didSet {
            lbTitle.text = String.init(format: "%d / %d", curInd, imgList.count)
            btnPrev.isHidden = curInd == 1
            btnNext.isHidden = curInd == imgList.count
        }
    }
    
    var assetsList : [UIImage] = []
    
    var isCropMode = false {
        didSet {
            btnPrev.isHidden = isCropMode || curInd == 1
            btnNext.isHidden = isCropMode || curInd == imgList.count
            vwBtns.isHidden = isCropMode
            vwCrop.isHidden = !isCropMode
            vwFrame.isHidden = isCropMode
            
            if isCropMode {
                btnClose.setTitle("취소", for: .normal)
                btnClose.setImage(nil, for: .normal)
            } else {
                btnClose.setTitle("", for: .normal)
                btnClose.setImage(UIImage(named: "ic_popup_close_white"), for: .normal)
            }
            
        }
    }
    
    var cropView: LyEditImageView?
    
    var cbOk: callback! = nil
    public typealias callback = (_ img : [UIImage]) -> ()
    
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var lbTitle: UILabel!
    
    @IBOutlet var vwFrame: UIView!
    @IBOutlet var vwCrop: UIView!
    @IBOutlet var btnPrev: UIButton!
    @IBOutlet var ivScalablePhoto: ScalableImageView!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet weak var vwBtns: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ivScalablePhoto.setImage(imgList[curInd - 1])
        
        cropView = LyEditImageView(frame: CGRect(origin: .zero, size: CGSize(width: vwCrop.frame.size.width, height: vwCrop.frame.size.height)))
        vwCrop.addSubview(cropView!)
        vwCrop.isHidden = true
    }
    
    override func awakeFromNib() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(img : [UIImage], index : Int = 1, okCallback : callback! = nil) {
        self.init()
        
        assetsList = img
        imgList = img
        curInd = index
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, imgList : [UIImage], index : Int = 1, okCallback: callback! = nil) {
        
        let sizePopup = PhotoEdit(img: imgList, index : index, okCallback : okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
                
        lbTitle.text = String.init(format: "%d / %d", curInd, imgList.count)
        
        btnPrev.isHidden = curInd == 1
        btnNext.isHidden = curInd == imgList.count
        
    }
    
    func rotateImage(source: UIImage, withOrientation orientation: UIImageOrientation) -> UIImage {
        UIGraphicsBeginImageContext(source.size)
        let context = UIGraphicsGetCurrentContext()
        if orientation == .right {
            context?.ctm.rotated(by: CGFloat.pi / 2)
        } else if orientation == .left {
            context?.ctm.rotated(by: -(CGFloat.pi / 2))
        } else if orientation == .down {
            // do nothing
        } else if orientation == .up {
            context?.ctm.rotated(by: CGFloat.pi / 2)
        }
        source.draw(at: CGPoint.zero)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        if isCropMode {
            isCropMode = false
            cropView?.removeFromSuperview()
            cropView = nil
        } else {
            var changeImg = false
            
            for id in 0..<imgList.count {
                if assetsList[id] != imgList[id] {
                    changeImg = true
                }
            }
            
            if changeImg {
                let acAlert = UIAlertController(title: "",
                                                message: "내용이 변경되었습니다. 저장하지 않으시겠습니까?",
                                                preferredStyle: .alert)
                acAlert.addAction(UIAlertAction(title: "네", style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                acAlert.addAction(UIAlertAction(title: "아니요", style: .cancel, handler: nil))
                
                self.present(acAlert, animated: true, completion: nil)
            } else {
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func finishAction(_ sender: Any?) {
        if isCropMode {
            if let image = cropView?.getCroppedImage() {
                imgList[curInd - 1] = image
                ivScalablePhoto.setImage(image)
                cropView?.removeFromSuperview()
                cropView = nil
            }
            
            isCropMode = false
        } else {
            dismiss(animated: true, completion: nil)
            
            if cbOk != nil {
                cbOk(imgList)
            }
        }
        
    }
    
    @IBAction func prevAction(_ sender: Any) {
        
        guard curInd > 1 else { return }
        
        curInd -= 1
        ivScalablePhoto.setImage(imgList[curInd - 1])
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        guard curInd < imgList.count else { return }
        
        curInd += 1
        ivScalablePhoto.setImage(imgList[curInd - 1])
        
    }
    
    @IBAction func cropAction(_ sender: Any) {
        
        isCropMode = true
        
        cropView = LyEditImageView(frame: CGRect(origin: .zero, size: CGSize(width: vwCrop.frame.size.width, height: vwCrop.frame.size.height)))
        vwCrop.addSubview(cropView!)
        cropView?.initWithImage(image: imgList[curInd - 1])
        
    }
    
    @IBAction func rotateAction(_ sender: Any) {
        
        let image = UIImage(cgImage: imgList[curInd - 1].cgImage!, scale: 1.0, orientation: .right)
        let newImage = rotateImage(source: image, withOrientation: .right)
        
        imgList[curInd - 1] = newImage
        ivScalablePhoto.setImage(newImage)
        
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "삭제하시겠습니까?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "네", style: .default, handler: { (UIAlertAction) in
            self.imgList.remove(at: self.curInd - 1)
            
            if self.imgList.count < 1 {
                self.finishAction(nil)
                return
            }
            
            self.curInd = self.curInd == 1 ? 1 : self.curInd - 1
            self.ivScalablePhoto.setImage(self.imgList[self.curInd - 1])
        }))
        alert.addAction(UIAlertAction(title: "아니요", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
