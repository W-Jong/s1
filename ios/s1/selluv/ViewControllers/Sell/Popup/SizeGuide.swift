//
//  SizeGuide.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class SizeGuide: BaseViewController {
    @IBOutlet weak var _imvSizeGuide_man: UIImageView!
    @IBOutlet weak var _imvSizeGuide_woman: UIImageView!
    @IBOutlet weak var _imvSizeGuide_baby: UIImageView!
    @IBOutlet weak var _imvSizeGuide_teen: UIImageView!
    @IBOutlet weak var _imvSizeGuide_kids: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        initVC()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        _imvSizeGuide_man.isHidden = true
        _imvSizeGuide_woman.isHidden = true
        _imvSizeGuide_kids.isHidden = true
        _imvSizeGuide_baby.isHidden = true
        _imvSizeGuide_teen.isHidden = true
        
        if gCategoryUid == 1 || (gCategoryUid > 4 && gCategoryUid < 10) {
            _imvSizeGuide_man.isHidden = false
        }
        
        if gCategoryUid == 2 || (gCategoryUid > 9 && gCategoryUid < 15) {
            _imvSizeGuide_woman.isHidden = false
        }
        
        if gCategoryUid == 4 || (gCategoryUid > 14 && gCategoryUid < 20) {
            if gCategoryUid == 15 {
                _imvSizeGuide_baby.isHidden = false
            } else if gCategoryUid == 19 {
                _imvSizeGuide_teen.isHidden = false
            } else {
                _imvSizeGuide_kids.isHidden = false
            }
        }
    
    }
    
    static func show(_ vc : UIViewController) {
        let guidePopup = SizeGuide()
        
        guidePopup.modalPresentationStyle = .overCurrentContext
        guidePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(guidePopup, animated: true, completion: nil)
        
    }
    

    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    

}
