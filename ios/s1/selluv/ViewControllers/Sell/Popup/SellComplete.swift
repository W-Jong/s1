//
//  SellComplete.swift
//  selluv
//  상품 완료
//  Created by Gambler on 12/29/17.
//  modified by PJh on 22/03/18

import UIKit
import TwitterKit
import Social
import FBSDKShareKit
import Photos

class SellComplete: BaseViewController, FBSDKSharingDelegate {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    var dicPdtInfo  : PdtDetailDto!
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func okAction(_ sender: Any) {
        // 등록한 상품이 카테고리 3까지 있을 경우
        popVC(-8, animated: true)
        
        // 카테고리2까지 있는 경우
//        popVC(-7, animated: true)
    }
    
    @IBAction func guideAction(_ sender: Any) {
        
        SNSGuide.show(self)
        
    }
    
    @IBAction func productAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dicPdtInfo.pdt.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func itemAction(_ sender: UIButton) {
        
        switch sender.tag {
        case 0: // 페이스북
            let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
            content.contentURL = URL(string: dicPdtInfo.shareUrl)
            content.quote = "상품이 추가되었습니다."
            let shareDialog: FBSDKShareDialog = FBSDKShareDialog()
            shareDialog.shareContent = content
            shareDialog.delegate = self
            shareDialog.fromViewController = self
            shareDialog.show()
        case 1: // 셀럽
            addPdtitem()
        case 2: // 네이버 블로그
            let shareString = String.init(format: "http://share.naver.com/web/shareView.nhn?title=%@&url=%@", "상품이 추가되었습니다.\n\n", self.dicPdtInfo.shareUrl)
            // encode a space to %20 for example
            let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            // cast to an url
            guard let url = URL(string: escapedShareString) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }

        case 3: // 카카오 스토리
            // Feed 타입 템플릿 오브젝트 생성
            let template = KLKTextTemplate.init { (feedTemplateBuilder) in
                feedTemplateBuilder.text = "상품이 추가되었습니다.\n\n" + self.dicPdtInfo.shareUrl
                // 컨텐츠
                
                feedTemplateBuilder.link = KLKLinkObject.init(builderBlock: { (linkBuilder) in
                    linkBuilder.mobileWebURL = URL.init(string: self.dicPdtInfo.shareUrl)
                })
            }
            
            // 카카오링크 실행
            KLKTalkLinkCenter.shared().sendDefault(with: template, success: { (warningMsg, argumentMsg) in
                
                // 성공
                print("warning message: \(String(describing: warningMsg))")
                print("argument message: \(String(describing: argumentMsg))")
                CommonUtil.showToast("공유 성공했습니다.")
                
            }, failure: { (error) in
                print("\(error)")
                // 실패
                CommonUtil.showToast("카카오톡 공유 실패했습니다. 카카오톡 앱이 설치되어있는지 확인해 주세요.")
            })
        case 4: // 인스타그램
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            let imageInstagram = UIImage(view:self.view)
            
            let photoLibrary = PHPhotoLibrary.shared()
            photoLibrary.savePhoto(image: imageInstagram, albumName: "selluv", completion: { (assets) in
                
                let u = "instagram://library?LocalIdentifier=" + (assets?.localIdentifier)! + "&InstagramCaption=상품이 추가되었습니다."
                let realUrl = u.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let url = URL(string: realUrl!)!
                
                DispatchQueue.main.async {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(URL(string: u)!)
                    } else {
                        CommonUtil.showToast("인스타그램 공유 실패했습니다. 인스타그램 앱이 설치되어있는지 확인해 주세요.")
                    }
                }
            })

        case 5: // 트위터
            let shareString = String.init(format: "https://twitter.com/intent/tweet?text=%@&url=%@", "상품이 추가되었습니다.", dicPdtInfo.shareUrl)
            // encode a space to %20 for example
            let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            // cast to an url
            guard let url = URL(string: escapedShareString) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        default:
            break
        }
        
    }
    
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        CommonUtil.showToast("페이스북 공유 실패했습니다. 페이스북 앱이 설치되어있는지 확인해 주세요.")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        CommonUtil.showToast("공유 성공했습니다.")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("sharer NSError")
        print(error.localizedDescription)
    }
    
    // 상품잇템 추가하기
    func addPdtitem() {
        
        gProgress.show()
        Net.addPdtItem(
            accessToken     : gMeInfo.token,
            pdtUid          : dicPdtInfo.pdt.pdtUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("add_item_success", comment:""))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}
