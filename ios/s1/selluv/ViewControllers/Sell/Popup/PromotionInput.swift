//
//  PromotionInput.swift
//  selluv
//  프로모션 팝업
//  Created by Gambler on 12/28/17.
//  modified by PJH on 20/03/18.

import UIKit

class PromotionInput: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var tfCode: DesignableUITextField!
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    @IBOutlet var vwClose: UIView!
    
    var code = ""
    var cbOk: callback! = nil
    public typealias callback = (_ code: String) -> ()
    var dicPromotionInfo: PromotionCodeDao!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(code: String, okCallback: callback! = nil) {
        self.init()
        
        self.code = code
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, code: String = "", okCallback: callback! = nil) {
        let sizePopup = PromotionInput(code: code, okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwDlg.layer.cornerRadius = 6
        vwClose.isHidden = true
        
        tfCode.placeholderColor = UIColor(hex: 0xcccccc)
        tfCode.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        tfCode.text = code
        
        btnOk.isEnabled = tfCode.hasText
        btnOk.layer.backgroundColor = tfCode.hasText ? UIColor.black.cgColor : UIColor(hex: 0x999999).cgColor
        
    }
    
    func hideKeyboard() {
        tfCode.resignFirstResponder()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = keyboardSize.height
                    self.vwClose.isHidden = false
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func textChanged(_ sender : UITextField) {
        btnOk.isEnabled = sender.hasText
        btnOk.layer.backgroundColor = sender.hasText ? UIColor.black.cgColor : UIColor(hex: 0x999999).cgColor
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        if cbOk != nil {
            cbOk(tfCode.text!)
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func hideAction(_ sender: Any) {
        
        hideKeyboard()
        
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

