//
//  CondGuide.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class CondGuide: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
    }
    
    static func show(_ vc : UIViewController) {
        let guidePopup = CondGuide()
        
        guidePopup.modalPresentationStyle = .overCurrentContext
        guidePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(guidePopup, animated: true, completion: nil)
        
    }
    

    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    

}
