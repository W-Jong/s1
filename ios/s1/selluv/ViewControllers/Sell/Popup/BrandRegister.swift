//
//  BrandRegister.swift
//  selluv
//
//  Created by Gambler on 12/22/17.
//

import UIKit

class BrandRegister: BaseViewController {
    
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var lbInfo: UILabel!
    
    public typealias callback = () -> ()    
    var cbOk : callback! = nil
    var brandName = ""
    var brandString : NSMutableAttributedString!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    
    convenience init(brand: String!, okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    
    func initVC() {
        vwContent.layer.cornerRadius = 6
        
        brandString = NSMutableAttributedString(attributedString: NSAttributedString(string: brandName, attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: 0x42c2fe)]))
        brandString.append(NSAttributedString(string: "은\n등록되어 있지 않은 브랜드입니다.", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black]))
        lbInfo.attributedText = brandString
    }
    
    static func show(_ vc: UIViewController, brand: String!, okCallback: callback! = nil) {
        let vcSuggest = BrandRegister(brand: brand, okCallback: okCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        vcSuggest.brandName = brand
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func bgAction(_ sender: Any) {
        closeAction(sender)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk()
        }
        
    }
    
}
