//
//  TransCostSet.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class TransCostSet: BaseViewController {

    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var tfCost: UITextField!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!    
    @IBOutlet var vwClose: UIView!
    
    var cost : Double = 3000
    var cbOk: callback! = nil
    public typealias callback = (_ cost: Double) -> ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(cost: Double, okCallback: callback! = nil) {
        self.init()
        
        self.cost = cost
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, cost: Double = 3000, okCallback: callback! = nil) {
        let sizePopup = TransCostSet(cost: cost, okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwDlg.layer.cornerRadius = 6
        vwClose.isHidden = true
        
        tfCost.layer.borderWidth = 1
        tfCost.layer.borderColor = UIColor(hex: 0xd3d3d3).cgColor
        tfCost.text = String(cost)
        
    }
    
    func hideKeyboard() {
        tfCost.resignFirstResponder()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = -keyboardSize.height
                    self.vwClose.isHidden = false
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
   
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        if !tfCost.hasText {
            tfCost.text = "3000"
        }
        
        if cbOk != nil {
            let strcost = tfCost.text?.replacingOccurrences(of: ",", with: "")
            if let cost = Double(strcost!) {
                cbOk(cost)
            } else {
                MsgUtil.showUIAlert("잘못된 숫자형식입니다.")
                return
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func guideAction(_ sender: Any) {
    }
    
    @IBAction func 닫기액션(_ sender: Any) {
        
        hideKeyboard()
        
    }
}

extension TransCostSet : UITextFieldDelegate {
    
}
