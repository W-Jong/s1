//
//  CameraGuide.swift
//  selluv
//
//  Created by Gambler on 12/25/17.
//

import UIKit

class CameraGuide: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func show(_ vc : UIViewController) {
        let guidePopup = CameraGuide()
        
        guidePopup.modalPresentationStyle = .overCurrentContext
        guidePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(guidePopup, animated: true, completion: nil)
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
