//
//  ValetCost.swift
//  selluv
//
//  Created by Gambler on 1/13/18.
//

import UIKit

class ValetCost: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var tfCost: UITextField!
    @IBOutlet weak var btnPlus: UIButton!
    
    @IBOutlet weak var btnSum: UIButton!
    @IBOutlet weak var vwClose: UIView!
    @IBOutlet weak var lcContentBottom: NSLayoutConstraint!
    
    var cost : Double = 0
    var cbOk: callback! = nil
    public typealias callback = (_ cost: Double) -> ()
    
    var pageTimer : Timer?
    var delay = 0
    var longPressed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    convenience init(code: Double, okCallback: callback! = nil) {
        self.init()
        
        self.cost = code
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, cost: Double = 0, okCallback: callback! = nil) {
        
        let sizePopup = ValetCost(code: cost, okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        tfCost.becomeFirstResponder()
        tfCost.addTarget(self, action: #selector(costChanged), for: .editingChanged)
        
        if cost == 0 {
            cost = 50000
        }
        let cost1 = cost / 10000
        tfCost.text = String(cost / 10000)
        
        if cost1 <= 14 {
            if cost1 > 3 {
                btnSum.setTitle(String(format: "%@%@", CommonUtil.formatNum(Int((cost1 - 3) * 10000)), "원"), for: .normal)
            } else {
                btnSum.setTitle("", for: .normal)
            }
        } else {
            btnSum.setTitle(String(format: "%@%@", CommonUtil.formatNum(Int(cost1 * 10000 * 0.79)), "원"), for: .normal)
        }
        
    }
    
    @objc func costChanged(_ sender : Any) {
        
        if let cost = Double(tfCost.text!) {
            if cost <= 14 {
                if cost > 3 {
                    btnSum.setTitle(String(format: "%@%@", CommonUtil.formatNum(Int((cost - 3) * 10000)), "원"), for: .normal)
                } else {
                    btnSum.setTitle("", for: .normal)
                }
            } else {
                btnSum.setTitle(String(format: "%@%@", CommonUtil.formatNum(Int(cost * 10000 * 0.79)), "원"), for: .normal)
            }
            
            btnMinus.isEnabled = cost > 5.0
        } else {
            btnSum.setTitle("", for: .normal)
            btnMinus.isEnabled = false
        }
        
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = keyboardSize.height
                    self.vwClose.isHidden = false
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    func hideKeyboard() {
        
        tfCost.resignFirstResponder()
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        hideKeyboard()
        
        let newCost = Double(tfCost.text!)! * 10000
        
        if newCost != cost {
            
            let acAlert = UIAlertController(title: "", message: NSLocalizedString("change_price_alert", comment:""), preferredStyle: .alert)
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                if self.cbOk != nil {
                    self.cbOk(self.cost)
                }
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                if self.cbOk != nil {
                    self.cbOk(Double(self.tfCost.text!)! * 10000.0)
                }
            }))
            self.present(acAlert, animated: true, completion: nil)
            
        } else {
            
            dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        hideKeyboard()
        
        let newCost = Double(tfCost.text!)! * 10000
        
        if newCost != cost {
            let acAlert = UIAlertController(title: "", message: NSLocalizedString("change_price_alert", comment:""), preferredStyle: .alert)
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                if self.cbOk != nil {
                    self.cbOk(self.cost)
                }
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                if self.cbOk != nil {
                    self.cbOk(Double(self.tfCost.text!)! * 10000.0)
                }
            }))
            self.present(acAlert, animated: true, completion: nil)
        } else {
            
            dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func minusAction(_ sender: Any) {
        
        let cost = Double(tfCost.text!)!
        
        if cost <= 5.0 {
            return
        }
        
//        if cost - 5.0 < 5.0  {
//            tfCost.text = String(0.5)
//        } else {
            tfCost.text = String(cost - 0.5)
//        }
        
        costChanged(sender)
        
        delay += 1
        if longPressed {
            if delay >= 6 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(minusAction(_:)), userInfo: nil, repeats: true)
            }
            if delay >= 16 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(minusAction(_:)), userInfo: nil, repeats: true)
            }
        }
        
    }
    
    @IBAction func longMinusAction(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            delay = -1
            longPressed = true
            pageTimer?.invalidate()
            pageTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(minusAction(_:)), userInfo: nil, repeats: true)
            
        case .ended:
            delay = -1
            longPressed = false
            pageTimer?.invalidate()
        default:
            return
        }
    }
    
    @IBAction func longPlusAction(_ sender: UILongPressGestureRecognizer) {        
        switch sender.state {
        case .began:
            delay = -1
            longPressed = true
            pageTimer?.invalidate()
            pageTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(plusAction(_:)), userInfo: nil, repeats: true)
        case .ended:
            delay = -1
            longPressed = false
            pageTimer?.invalidate()
        default:
            return
        }
    }
    
    @IBAction func plusAction(_ sender: Any) {
        
        let cost = Double(tfCost.text!)!
        
        tfCost.text = String(cost + 0.5)
        costChanged(sender)
        
        delay += 1
        if longPressed {
            if delay >= 6 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(plusAction(_:)), userInfo: nil, repeats: true)
            }
            if delay >= 16 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(plusAction(_:)), userInfo: nil, repeats: true)
            }
        }

        
    }
    
    @IBAction func inputAction(_ sender: Any) {
        
        if let cost = Double(tfCost.text!) {
            
            if cost < 5.0 {
                tfCost.text = "15"
            } else {
                tfCost.text = String(format: "%1.2f", cost)
            }
            
        } else {
            
            if tfCost.hasText {
                MsgUtil.showUIAlert("잘못된 숫자형식입니다.")
            }
            
            tfCost.text = "15"
            
        }
        
        hideKeyboard()
        costChanged(sender)
        
    }
    
}
