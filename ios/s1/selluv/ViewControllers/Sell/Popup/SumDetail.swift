//
//  SumDetail.swift
//  selluv
//  정산 상세
//  Created by Gambler on 12/29/17.
//  modified by PJH on 20/03/18.

import UIKit

class SumDetail: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnSum: UIButton!
    
    @IBOutlet var vwProduct: UIView!
    @IBOutlet var vwProductHeight: NSLayoutConstraint!
    @IBOutlet var lbProduct: UILabel!
    @IBOutlet var lbTransCost: UILabel!
    @IBOutlet var lbSelluvCost: UILabel!
    @IBOutlet var lbCash: UILabel!
    @IBOutlet var lbPayback: UILabel!
    @IBOutlet var lbSum: UILabel!
    @IBOutlet var lbTransCostTitle: UILabel!
    
    @IBOutlet var vwMax: UIView!
    @IBOutlet var vwMaxHeight: NSLayoutConstraint!
    @IBOutlet var btnMax: UIButton!
    @IBOutlet var lbSNS: UILabel!
    @IBOutlet var lbContactCost: UILabel!
    
    @IBOutlet var lbTotal: UILabel!
    
    let 검은색13 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x333333)]
    let 검은색15 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x333333)]
    let 블루색13 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x42c2fe)]
    let 검은색볼드15 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x333333)]
    let 레드색14 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor(hex: 0xff3b7e)]
    let 검은색14 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x333333)]
    let 레드색13 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0xff3b7e)]
    let 레드색볼드15 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(hex: 0xff3b7e)]
    
    var sumPrice            : Double = 0  //정산가격
    var salePrice           : Double = 0  //판매가격
    var sendPrice           : Double!     //배송비
    var selluvUseMoney      : Double = 0  //셀럽 이용료
    var payUseMoney         : Double = 0  //결제수수료
    var promotionPay        : Double = 0  //프로모션 코드 페이백
    var promotionCode       : String!
    var dicPdtInfo          : PdtDetailDto!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        getSystemSettingInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    convenience init(salePrcie : Double, sendPrice:Double, selluvmoney : Double, pay : Double, promotion : Double, dicInfo : PdtDetailDto) {
//        self.init()
//
//
//    }
//
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
//    static func show(_ vc : UIViewController, salePrcie : Double, sendPrice:Double, selluvmoney : Double, pay : Double, promotion : Double, dicInfo : PdtDetailDto) {
//
//        let guidePopup = SumDetail()
//
//        guidePopup.modalPresentationStyle = .overCurrentContext
//        guidePopup.modalTransitionStyle = .crossDissolve
//
//        vc.present(guidePopup, animated: true, completion: nil)
//
//    }
    
    func initVC() {
        
        vwProduct.layer.borderWidth = 1
        vwProduct.layer.borderColor = UIColor(hex: 0xe9e9e9).cgColor
        
        vwMax.layer.borderWidth = 1
        vwMax.layer.borderColor = UIColor(hex: 0xe9e9e9).cgColor
        
        btnSum.setTitle(CommonUtil.formatNum(Int(sumPrice)) + " 원", for: .normal)
        btnSum.setTitle(CommonUtil.formatNum(Int(sumPrice)) + " 원", for: .selected)
        
        lbProduct.text = "+ " + CommonUtil.formatNum(salePrice) + " 원"
        if sendPrice == -1 {
            lbTransCost.text = "+ " + CommonUtil.formatNum(Int(sendPrice)) + " 원"
        } else {
            lbTransCost.text = "+ " + CommonUtil.formatNum(0) + " 원"
            lbTransCostTitle.text = "배송비(무료배송 서비스 설정)"
        }
        
        lbSelluvCost.text = "- " + CommonUtil.formatNum(Int(selluvUseMoney)) + " 원"
        lbCash.text = "- " + CommonUtil.formatNum(Int(payUseMoney)) + " 원"
        
        let payCost = NSMutableAttributedString(attributedString: NSAttributedString(string: "+ " + CommonUtil.formatNum(promotionPay), attributes: 블루색13))
        payCost.append(NSAttributedString(string: " 원", attributes: 검은색13))
        lbPayback.attributedText = payCost
        
        let sumCost = NSMutableAttributedString(attributedString: NSAttributedString(string:  CommonUtil.formatNum(Int(sumPrice)), attributes: 검은색볼드15))
        sumCost.append(NSAttributedString(string: " 원", attributes: 검은색15))
        lbSum.attributedText = sumCost

    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func okAction(_ sender: Any) {
        
//        dismiss(animated: true, completion: nil)
        popVC()
    }
    
    @IBAction func sumAction(_ sender: Any) {
        
        btnSum.isSelected = !btnSum.isSelected
        vwProduct.isHidden = btnSum.isSelected
        vwProductHeight.constant = btnSum.isSelected ? 0 : 200
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
    }
    
    @IBAction func maxAction(_ sender: Any) {
        
        btnMax.isSelected = !btnMax.isSelected
        vwMax.isHidden = btnMax.isSelected
        vwMaxHeight.constant = btnMax.isSelected ? 0 : 120.5
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 시스템 설정 정보 얻기
    func getSystemSettingInfo(){
        
        gProgress.show()
        Net.getSystemSettingInfo(
            accessToken         : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSystemSettingInfoResult
                self.getSystemSettingInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //시스템 설정 정보 얻기결과
    func getSystemSettingInfoResult(_ data: Net.getSystemSettingInfoResult) {
        
        let dicSysstemInfo : SystemSettingDto = data.dicInfo
        let buyReviewReward : Int = dicSysstemInfo.buyReviewReward
        let shareRewrad  : Int = dicSysstemInfo.shareRewrad * 5
        
        let maxCost = NSMutableAttributedString(attributedString: NSAttributedString(string:  CommonUtil.formatNum(shareRewrad + buyReviewReward), attributes: 레드색14))
        maxCost.append(NSAttributedString(string: " 원", attributes: 검은색14))
        btnMax.setAttributedTitle(maxCost, for: .normal)
        btnMax.setAttributedTitle(maxCost, for: .selected)
        
        let snsCost = NSMutableAttributedString(attributedString: NSAttributedString(string:  CommonUtil.formatNum(shareRewrad), attributes: 레드색13))
        snsCost.append(NSAttributedString(string: " 원", attributes: 검은색13))
        lbSNS.attributedText = snsCost
        
        let contactCost = NSMutableAttributedString(attributedString: NSAttributedString(string:  CommonUtil.formatNum(buyReviewReward), attributes: 레드색13))
        contactCost.append(NSAttributedString(string: " 원", attributes: 검은색13))
        lbContactCost.attributedText = contactCost
        
        let maxCost1 = NSMutableAttributedString(attributedString: NSAttributedString(string:  CommonUtil.formatNum(shareRewrad + buyReviewReward), attributes: 레드색볼드15))
        maxCost1.append(NSAttributedString(string: " 원", attributes: 검은색15))
        lbTotal.attributedText = maxCost1
    }
    
}
