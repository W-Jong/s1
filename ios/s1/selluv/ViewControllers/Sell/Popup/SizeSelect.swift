//
//  SizeSelect.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class SizeSelect: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnGuide: UIButton!
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var picker1: UIPickerView!
    @IBOutlet var picker2: UIPickerView!
    @IBOutlet weak var vwBg: UIView!
    
    var cbOk: callback! = nil
    public typealias callback = (_ size1: String) -> ()
    var dicSizeInfo     : SizeRefDto!
    var arrlist1        : [String] = []
    var arrlist2        : [[String]] = [[]]
    
    var firstIndex      : Int = 0
    var secondeIndex    : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animateAndChain(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }) { (finish) in
            self.vwBg.alpha = 0.8
        }
    }
    
    convenience init(okCallback: callback! = nil, dicSizeInfo : SizeRefDto) {
        self.init()
        
        cbOk = okCallback
        arrlist1 = dicSizeInfo.sizeFirst
        arrlist2 = dicSizeInfo.sizeSecond
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, dicSizeInfo : SizeRefDto,  okCallback: callback! = nil) {
        let sizePopup = SizeSelect(okCallback: okCallback, dicSizeInfo : dicSizeInfo)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .coverVertical
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        btnOk.layer.cornerRadius = 5
        
    }
    
    func effectVC() {
        UIView.animateAndChain(withDuration: 0.1, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.0
        }, completion: nil).animateAndChain(withDuration: 0.1, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0
        }) { (finish) in
            self.vwBg.alpha = 0
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func okAction(_ sender: Any) {
        
        effectVC()
        
        if cbOk != nil {
            
            if arrlist1.count == 1 {
                cbOk(arrlist2[firstIndex][secondeIndex])
            } else {
                cbOk(String.init(format: "%@ %@", arrlist1[firstIndex] ,arrlist2[firstIndex][secondeIndex]))
            }
        }
    }
    
    @IBAction func guideAction(_ sender: Any) {
//        pushVC("FILTER_SIZEGUIDE_VIEW", storyboard: "Top", animated: true)
//        SizeGuide.show(self)
//        let storyboard = UIStoryboard(name: "Top", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "FILTER_SIZEGUIDE_VIEW") as! FilterSizeGuideViewController
//        self.navigationController?.pushViewController(controller, animated: true)
        
        let storyboard = UIStoryboard(name: "Top", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FILTER_SIZEGUIDE_VIEW")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender : Any) {
        effectVC()
    }
}

extension SizeSelect : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == picker1 {
            return arrlist1.count
        } else {
            return arrlist2[firstIndex].count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView === picker1 {
            firstIndex = row
            picker2.reloadAllComponents()
        } else {
            secondeIndex = row
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == picker1 {
            return arrlist1[row]
        } else {
            return arrlist2[firstIndex][row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 48
    }
    
    
}
