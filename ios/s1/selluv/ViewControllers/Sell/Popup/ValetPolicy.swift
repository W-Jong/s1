//
//  ValetPolicy.swift
//  selluv
//
//  Created by Gambler on 1/13/18.
//

import UIKit

class ValetPolicy: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lbKBrand: UILabel!
    @IBOutlet var vwSign: UIView!
    @IBOutlet var ivSign: UIImageView!
    @IBOutlet var dsvSign: YPDrawSignatureView!
    @IBOutlet var btnOk: UIButton!
    
    var cbOk: callback! = nil
    var cbGuide: callback1! = nil
    var name : String!
    
    public typealias callback = (_ sign : UIImage) -> ()
    public typealias callback1 = () -> ()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(pName: String!, okCallback: callback! = nil, guideCallback: callback1! = nil) {
        self.init()
        
        name = pName
        cbOk = okCallback
        cbGuide = guideCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, name : String!, okCallback: callback! = nil, guideCallback: callback1! = nil) {
        
        let sizePopup = ValetPolicy(pName: name, okCallback: okCallback, guideCallback: guideCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        vwSign.layer.borderWidth = 1
        vwSign.layer.borderColor = UIColor(hex: 0xe1e1e1).cgColor
        
        vwDlg.layer.cornerRadius = 6
        lbKBrand.text = name
        
        dsvSign.delegate = self
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func guideAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbGuide != nil {
            cbGuide()
        }
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(dsvSign.getSignature()!)
        }
    }
    
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension ValetPolicy : YPSignatureDelegate {
    
    func didStart() {
        ivSign.isHidden = true
        btnOk.isEnabled = true
        btnOk.layer.backgroundColor = UIColor.black.cgColor
    }
    
    func didFinish() {
        print("finish")
    }
    
}
