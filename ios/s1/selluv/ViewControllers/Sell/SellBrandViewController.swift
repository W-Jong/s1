//
//  BrandViewController.swift
//  selluv
//  브렌드
//  Created by Gambler on 12/21/17.
//  modified by PJH on 21/03/18.

import UIKit

class SellBrandViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnLang: [UIButton]!
    
    @IBOutlet weak var vwLangBar: UIView!
    @IBOutlet weak var vwLangHover: UIView!
    @IBOutlet weak var vwLangHoverLeading: NSLayoutConstraint!
    @IBOutlet weak var vwLangHoverTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var vwSearchBar: UIView!
    @IBOutlet weak var tfSearchTrailing: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: DesignableUITextField!
    @IBOutlet weak var vwPlaceHolder: UIView!
    @IBOutlet weak var vwPlaceHolderXPos: NSLayoutConstraint!
    @IBOutlet weak var lbPlaceHolder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var tblBrand: UITableView!
    @IBOutlet weak var btnRegister: UIButton!
    
    var arrBrandList    : Array<BrandMiniDto> = []
    var arrBrandEn = [[BrandMiniDto!]](repeating: [BrandMiniDto](), count: index_eng.count)
    var arrBrandKo = [[BrandMiniDto!]](repeating: [BrandMiniDto](), count: index_kor.count)

    var arrBrandEn1 = [[BrandMiniDto!]](repeating: [BrandMiniDto](), count: index_eng.count)
    var arrBrandKo1 = [[BrandMiniDto!]](repeating: [BrandMiniDto](), count: index_kor.count)

    var categoryUid     : Int!
    
    enum ELang : Int {
        case eng = 0
        case kor
    }
    
    var lang = ELang.eng {
        didSet {
            btnLang[oldValue.rawValue].isSelected = false
            btnLang[lang.rawValue].isSelected = true
            
            indexView.removeFromSuperview()
            indexView = STBTableViewIndex()
            indexView.titles = lang == .eng ? index_eng : index_kor
//            indexView.sections = lang == .eng ? sections_eng : sections_kor
            indexView.sections = lang == .eng ? index_eng : index_kor
            indexView.delegate = self
            navigationController?.view.addSubview(indexView)
            tblBrand.reloadData()
            
            vwLangBar.removeConstraint(vwLangHoverLeading)
            vwLangBar.removeConstraint(vwLangHoverTrailing)
            
            vwLangHoverLeading = NSLayoutConstraint(item: vwLangHover, attribute: .leading, relatedBy: .equal, toItem: btnLang[lang.rawValue], attribute: .leading, multiplier: 1.0, constant: 0.0)
            vwLangHoverTrailing = NSLayoutConstraint(item: vwLangHover, attribute: .trailing, relatedBy: .equal, toItem: btnLang[lang.rawValue], attribute: .trailing, multiplier: 1.0, constant: 0.0)
            
            vwLangBar.addConstraint(vwLangHoverLeading)
            vwLangBar.addConstraint(vwLangHoverTrailing)

            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    var sections_eng = ["A", "B", "C", "D", "E", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"]
    
    var sections_kor = ["ㄱ", "ㄴ", "ㄷ", "ㅎ"]
    
    var indexView = STBTableViewIndex()
    
//    var selectedIndex : IndexPath?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tfSearch.resignFirstResponder()
        indexView.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        
        indexView.removeFromSuperview()
        indexView = STBTableViewIndex()
        indexView.titles = lang == .eng ? index_eng : index_kor
//        indexView.sections = lang == .eng ? sections_eng : sections_kor
        indexView.sections = lang == .eng ? index_eng : index_kor
        indexView.delegate = self
        navigationController?.view.addSubview(indexView)
        
    }
    
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        vwLangBar.borderColor = UIColor(hex: 0xC3C3C3)
        vwLangBar.layer.borderWidth = 1
        vwLangBar.layer.cornerRadius = 13.5
        
        vwLangHover.borderColor = UIColor(hex: 0xC3C3C3)
        vwLangHover.layer.borderWidth = 1
        vwLangHover.layer.cornerRadius = 13.5
        
        btnCancel.isHidden = true
        tfSearch.text = ""
        tfSearch.layer.cornerRadius = 5.0
        
        tblBrand.register(UINib(nibName: "BrandCell", bundle: nil), forCellReuseIdentifier: "BrandCell")
        
        indexView.delegate = self
        indexView.titles = index_eng
//        indexView.sections = sections_eng
        indexView.sections = index_eng
        navigationController?.view.addSubview(indexView)
        
        tfSearch.addTarget(self, action: #selector(self.searchTextChanged), for: .editingChanged)

        lang = .eng
        btnLang[ELang.eng.rawValue].isSelected = true
        btnLang[ELang.kor.rawValue].isSelected = false
        
        btnRegister.isHidden = true
        
        getAllBrandList()
        
    }
    
    func getSearchResult(key : String) {
        
        arrBrandEn1 = [[BrandMiniDto!]](repeating: [], count: index_eng.count)
        arrBrandKo1 = [[BrandMiniDto!]](repeating: [], count: index_kor.count)
        if key == "" {
            arrBrandEn1 = arrBrandEn
            arrBrandKo1 = arrBrandKo
        } else {
            if lang == .eng {
                
                for i in 0..<index_eng.count {
                    for k in 0..<arrBrandEn[i].count {
                        let dic : BrandMiniDto = arrBrandEn[i][k]
                        if dic.nameEn.lowercased().contains(key) {
                            arrBrandEn1[i].append(dic)
                        }
                    }
                }
            } else {
                for i in 0..<index_kor.count {
                    for k in 0..<arrBrandKo[i].count {
                        let dic : BrandMiniDto = arrBrandKo[i][k]
                        if dic.nameKo.contains(key) {
                            arrBrandKo1[i].append(dic)
                        }
                    }
                }
            }
        }
        tblBrand.reloadData()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func placeHolderAction(_ sender: Any) {
        tfSearch.becomeFirstResponder()
    }
    
    @IBAction func langAction(_ sender: Any) {
        
        let btn = sender as! UIButton
        lang = (btn.tag == 0) ? .eng : .kor
        tblBrand.reloadData()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        tfSearch.text = ""
        tfSearch.resignFirstResponder()
        lbPlaceHolder.isHidden = tfSearch.hasText
        vwSearchBar.removeConstraint(vwPlaceHolderXPos)
        
        btnCancel.isHidden = true
        
        vwPlaceHolderXPos = NSLayoutConstraint(item: vwPlaceHolder, attribute: .centerX, relatedBy: .equal, toItem: vwSearchBar, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        tfSearchTrailing.constant = 15.0
        btnCancel.isHidden = true
        vwSearchBar.addConstraint(vwPlaceHolderXPos)
        
        view.setNeedsLayout()
        UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func tapAction(_ sender: Any) {
        
        if !tfSearch.hasText {
            vwSearchBar.removeConstraint(vwPlaceHolderXPos)
            
            vwPlaceHolderXPos = NSLayoutConstraint(item: vwPlaceHolder, attribute: .centerX, relatedBy: .equal, toItem: vwSearchBar, attribute: .centerX, multiplier: 1.0, constant: 0.0)
            tfSearchTrailing.constant = 15.0
            btnCancel.isHidden = true
            vwSearchBar.addConstraint(vwPlaceHolderXPos)
            
            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
        
        tfSearch.resignFirstResponder()
        
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        BrandRegister.show(self, brand: tfSearch.text) {
            // 해당 브랜드명으로 서버에 새로 등록한다.
           self.regBrand()
        }
    }
    
    @objc func searchTextChanged(_ sender : UITextField) {
        
        lbPlaceHolder.isHidden = tfSearch.hasText
        
        let str : String = sender.text!
        let newLength = str.count
        
        if newLength > 0 {
            btnRegister.setTitle("'" + str + "'브랜드로 상품 등록", for: .normal)
            btnRegister.isHidden = false
        } else {
            btnRegister.isHidden = true
        }
        getSearchResult(key: str)
        
    }
    
    @objc func onBtnItemSelect(_ sender: UIButton) {
        let index = sender.tag

        if lang == .eng {
            let dic : BrandMiniDto = arrBrandEn1[index / 10][index % 10]
            gPdtRegInfo.setBrandUid(_uid: dic.brandUid)
            gPdtRegInfo.setNameEn(value: dic.nameEn)
            gPdtRegInfo.setNameKo(value: dic.nameKo)
        } else {
            let dic1 : BrandMiniDto = arrBrandKo1[index / 10][index % 10]
            gPdtRegInfo.setBrandUid(_uid: dic1.brandUid)
            gPdtRegInfo.setNameEn(value: dic1.nameEn)
            gPdtRegInfo.setNameKo(value: dic1.nameKo)
            
        }
        
        tblBrand.reloadData()
        
        // 캐머러페이지로 이동
        if actionType == .sell {
            
            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
            vc.m_bFlag = false
            vc.isEdit = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            self.pushVC("SELL_SERVICE_REQUEST_VIEW", storyboard: "Sell", animated: true)
        }
        
    }
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 모든 브렌드 목록 얻기
    func getAllBrandList() {
        
        gProgress.show()
        Net.getAllBrandList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AllBrandResult
                self.getAllBrandResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //모든 브렌드 목록 얻기 결과
    func getAllBrandResult(_ data: Net.AllBrandResult) {
        
        arrBrandList = data.list
        arrBrandEn = data.list_en
        arrBrandKo = data.list_ko
        arrBrandEn1 = data.list_en
        arrBrandKo1 = data.list_ko

        tblBrand.reloadData()
    }
    
    // 브렌드 등록
    func regBrand() {

        let keyword : String = tfSearch.text!
        let str = keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        gProgress.show()
        Net.regBrand(
            accessToken     : gMeInfo.token,
            brandName       : str!,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.regBrandResult
                self.regBrandResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //브렌드 동록 결과
    func regBrandResult(_ data: Net.regBrandResult) {

        gPdtRegInfo.setBrandUid(_uid: data.dicInfo.brandUid)
        gPdtRegInfo.setNameEn(value: data.dicInfo.nameEn)
        gPdtRegInfo.setNameKo(value: data.dicInfo.nameKo)

        // 캐머러페이지로 이동
        if actionType == .sell {
            gPdtRegInfo.setBrandUid(_uid: data.dicInfo.brandUid)
            
            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
            vc.m_bFlag = false
            vc.isEdit = false
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.pushVC("SELL_SERVICE_REQUEST_VIEW", storyboard: "Sell", animated: true)
        }
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension SellBrandViewController : UITextFieldDelegate {
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
////        guard let text = textField.text else { return true }
////        let newLength = text.characters.count + string.characters.count - range.length
//        let str : String = textField.text!
//        let newLength = str.count
//
//        if newLength > 0 {
//            btnRegister.setTitle("'" + str + "'브랜드로 상품 등록", for: .normal)
//            btnRegister.isHidden = false
//        } else {
//            btnRegister.isHidden = true
//        }
//        getSearchResult(key: str)
//        return true
//
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if !textField.hasText {
            vwSearchBar.removeConstraint(vwPlaceHolderXPos);
            
            vwPlaceHolderXPos = NSLayoutConstraint(item: vwPlaceHolder, attribute: .leading, relatedBy: .equal, toItem: vwSearchBar, attribute: .leading, multiplier: 1.0, constant: 27.0)
            tfSearchTrailing.constant = btnCancel.frame.size.width
            btnCancel.isHidden = false
            vwSearchBar.addConstraint(vwPlaceHolderXPos)
            
            view.setNeedsLayout()
            UIView.animate(withDuration: TabAnimationTime, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension SellBrandViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        return lang == .eng ? sections_eng.count : sections_kor.count
        return lang == .eng ? index_eng.count : index_kor.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return String(format:"%@", lang == .eng ? sections_eng[section] : sections_kor[section])
        return String(format:"%@", lang == .eng ? index_eng[section] : index_kor[section])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lang == .eng {
           return arrBrandEn1[section].count
        } else {
           return arrBrandKo1[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandCell", for: indexPath) as! BrandCell
        
        var brandUid : Int = 0
        if lang == .eng {
            let dic : BrandMiniDto = arrBrandEn1[indexPath.section][indexPath.row]
            cell._lbBrandFirstName.text = dic.nameEn
            cell._lbBrandSecondName.text = dic.nameKo
            brandUid = dic.brandUid
        } else {
            let dic1 : BrandMiniDto = arrBrandKo1[indexPath.section][indexPath.row]
            cell._lbBrandFirstName.text = dic1.nameKo
            cell._lbBrandSecondName.text = dic1.nameEn
            brandUid = dic1.brandUid
        }
        
        if gPdtRegInfo.getBrandUid() == brandUid {
            cell._imvSelectOn.isHidden = false
        } else {
            cell._imvSelectOn.isHidden = true
        }
        cell._imvSelectOn.image = gPdtRegInfo.getBrandUid() == brandUid ?  #imageLiteral(resourceName: "ic_login_confirm") : #imageLiteral(resourceName: "ic_brand_select_off")
        
        cell._btnSelect.tag = indexPath.section * 10 + indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnItemSelect(_:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//extension SellBrandViewController: BrandCellDelegate {
//
//    func BrandCellDelegate(_ brandCell: BrandCell, onBrandSelected sender: Any, tag: Int?) {
//        brandCell._imvSelectOn.isHidden = !brandCell._imvSelectOn.isHidden
//
//        if brandCell._imvSelectOn.isHidden {
//            selectedIndex = nil
//        } else {
//            selectedIndex = brandCell.getIndexPath()
//        }
//
//        tfSearch.resignFirstResponder()
//
//        // 캐머러페이지로 이동
//        if actionType == .sell {
//
//            if lang == .eng {
//                let dic : BrandMiniDto = arrBrandEn1[selectedIndex!.section][selectedIndex!.row]
//                gPdtRegInfo.setBrandUid(_uid: dic.brandUid)
//                gPdtRegInfo.setNameEn(value: dic.nameEn)
//                gPdtRegInfo.setNameKo(value: dic.nameKo)
//            } else {
//                let dic1 : BrandMiniDto = arrBrandKo1[selectedIndex!.section][selectedIndex!.row]
//                gPdtRegInfo.setBrandUid(_uid: dic1.brandUid)
//                gPdtRegInfo.setNameEn(value: dic1.nameEn)
//                gPdtRegInfo.setNameKo(value: dic1.nameKo)
//            }
//
//            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
//            vc.m_bFlag = false
//            vc.isEdit = false
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        } else {
//            self.pushVC("SELL_SERVICE_REQUEST_VIEW", storyboard: "Sell", animated: true)
//        }
//
//    }
//
//}

extension SellBrandViewController : STBTableViewIndexDelegate {
    
    func tableViewIndexChanged(_ index: Int, title: String) {
        let group = lang == .eng ? index_eng : index_kor
        let section = group.index(of: title) ?? 0
        
        var point = tblBrand.rect(forSection: section).origin
        point.y = min(point.y, tblBrand.contentSize.height - tblBrand.bounds.height)
        tblBrand.setContentOffset(point, animated: true)
    }
    
    func tableViewIndexTopLayoutGuideLength() -> CGFloat {
        return 135
    }
    
    func tableViewIndexBottomLayoutGuideLength() -> CGFloat {
        return 80
    }
    
}


