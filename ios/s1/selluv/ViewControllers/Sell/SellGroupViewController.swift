//
//  SellGroupViewController.swift
//  selluv
//  상품군 
//  Created by Gambler on 1/5/18.
//  modified by PJH on 20/03/18.

import UIKit

class SellGroupViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwContent: UIView!
    @IBOutlet var lbTitle: UILabel!
    
    @IBOutlet var vwWomen: UIStackView!
    @IBOutlet var vwMen: UIStackView!
    @IBOutlet var vwKids: UIStackView!
    
    @IBOutlet weak var imgWomen: UIImageView!
    @IBOutlet weak var imgMen: UIImageView!
    @IBOutlet weak var imgKids: UIImageView!
    
    @IBOutlet var lbWomen: UILabel!
    @IBOutlet var lbMen: UILabel!
    @IBOutlet var lbKids: UILabel!
    
    @IBOutlet var btnCell: [UIButton]!
    
    
    var radiusX : CGFloat {
        get {
            return vwContent.frame.size.width / 4
        }
    }
    
    var radiusY : CGFloat {
        get {
            return #imageLiteral(resourceName: "ic_login_interest_women").size.width + 10
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        //상품 저장 정보를 초기화
        gPdtRegInfo.initial()
        
        for btn in btnCell {
            btn.centerLabelVerticallyWithPadding(spacing: 12)
            btn.isHidden = true
        }
        
        vwWomen.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectAction(_:))))
        vwMen.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectAction(_:))))
        vwKids.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectAction(_:))))
        
        vwWomen.isUserInteractionEnabled = false
        vwMen.isUserInteractionEnabled = false
        vwKids.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: SellAnimationTime, delay: 0.2,  options: [],
            animations: { () -> Void in
            
            self.vwWomen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.radiusX))
                
            self.vwMen.layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusX * CGFloat(sin(60 / 180 * Double.pi)), y: self.radiusX * CGFloat(sin(30 / 180 * Double.pi)) + 24))
            
            self.vwKids.layer.setAffineTransform(CGAffineTransform(translationX: self.radiusX * CGFloat(sin(60 / 180 * Double.pi)), y: self.radiusX * CGFloat(sin(30 / 180 * Double.pi)) + 24))
            
                                        
        }, completion: { (Bool) -> Void in
            self.vwWomen.isUserInteractionEnabled = true
            self.vwMen.isUserInteractionEnabled = true
            self.vwKids.isUserInteractionEnabled = true
        })
    }
    
    @objc func selectAction(_ sender : UITapGestureRecognizer) {
        
        vwWomen.isUserInteractionEnabled = false
        vwMen.isUserInteractionEnabled = false
        vwKids.isUserInteractionEnabled = false
        
        lbWomen.isHidden = true
        lbMen.isHidden = true
        lbKids.isHidden = true
        
        lbTitle.text = NSLocalizedString("select_category", comment:"")
        
        switch sender.view!.tag {
        case 2:
            imgWomen.isHighlighted = true
            gCategoryUid = 2
            showWomenCells()
            gPdtRegInfo.setPdtGroup(value: "FEMALE")
        case 1:
            imgMen.isHighlighted = true
            gCategoryUid = 1
            showMenCells()
            gPdtRegInfo.setPdtGroup(value: "MALE")
        case 4:
            imgKids.isHighlighted = true
            gCategoryUid = 4
            showKidsCells()
            gPdtRegInfo.setPdtGroup(value: "KIDS")
        default:
            break
        }
        
    }
    
    func showWomenCells() {
        
        UIView.animateAndChain(withDuration: SellAnimationTime, delay: 0.0, options: [], animations: {
            
            self.vwWomen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwMen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwKids.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwMen.alpha = 0
            self.vwKids.alpha = 0
            
        }, completion: {(finish) -> Void in
            
            for nInd in 0 ... 4 {
                self.btnCell[nInd].isHidden = false
            }
            
        }).animate(withDuration: SellAnimationTime, delay: 0.0, options: [],
                                    animations: { () -> Void in
                                        
            self.btnCell[0].layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.radiusY))
            self.btnCell[1].layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusY, y: 0))
            self.btnCell[2].layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusY * CGFloat(sin(30 / 180 * Double.pi)), y: self.radiusY * CGFloat(sin(60 / 180 * Double.pi)) + 20))
            self.btnCell[4].layer.setAffineTransform(CGAffineTransform(translationX: self.radiusY, y: 0))
            self.btnCell[3].layer.setAffineTransform(CGAffineTransform(translationX: self.radiusY * CGFloat(sin(30 / 180 * Double.pi)), y: self.radiusY * CGFloat(sin(60 / 180 * Double.pi)) + 20))
                                      
                                        
        }, completion: { (Bool) -> Void in
            self.imgWomen.isHighlighted = false
        })
    }
    
    func showMenCells() {
        
        UIView.animateAndChain(withDuration: SellAnimationTime, delay: 0.0, options: [], animations: {
            
            self.vwWomen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwMen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwKids.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwWomen.alpha = 0
            self.vwKids.alpha = 0
            
        }, completion: {(finish) -> Void in
            
            for nInd in 5 ... 9 {
                self.btnCell[nInd].isHidden = false
            }
            
        }).animate(withDuration: SellAnimationTime, delay: 0.0, options: [],
                   animations: { () -> Void in
                    
                    self.btnCell[5].layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.radiusY))
                    self.btnCell[6].layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusY, y: 0))
                    self.btnCell[7].layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusY * CGFloat(sin(30 / 180 * Double.pi)), y: self.radiusY * CGFloat(sin(60 / 180 * Double.pi)) + 20))
                    self.btnCell[9].layer.setAffineTransform(CGAffineTransform(translationX: self.radiusY, y: 0))
                    self.btnCell[8].layer.setAffineTransform(CGAffineTransform(translationX: self.radiusY * CGFloat(sin(30 / 180 * Double.pi)), y: self.radiusY * CGFloat(sin(60 / 180 * Double.pi)) + 20))
                    
        }, completion: { (Bool) -> Void in
            self.imgMen.isHighlighted = false
        })
    }
    
    func showKidsCells() {
        
        UIView.animateAndChain(withDuration: SellAnimationTime, delay: 0.0, options: [], animations: {
            
            self.vwWomen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwMen.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwKids.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            
            self.vwMen.alpha = 0
            self.vwWomen.alpha = 0
            
        }, completion: {(finish) -> Void in
            
            for nInd in 10 ... 14 {
                self.btnCell[nInd].isHidden = false
            }
            
        }).animate(withDuration: SellAnimationTime, delay: 0.0, options: [],
                   animations: { () -> Void in
                    
                    self.btnCell[10].layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.radiusY))
                    self.btnCell[11].layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusY, y: 0))
                    self.btnCell[12].layer.setAffineTransform(CGAffineTransform(translationX: -self.radiusY * CGFloat(sin(30 / 180 * Double.pi)), y: self.radiusY * CGFloat(sin(60 / 180 * Double.pi)) + 20))
                    self.btnCell[14].layer.setAffineTransform(CGAffineTransform(translationX: self.radiusY, y: 0))
                    self.btnCell[13].layer.setAffineTransform(CGAffineTransform(translationX: self.radiusY * CGFloat(sin(30 / 180 * Double.pi)), y: self.radiusY * CGFloat(sin(60 / 180 * Double.pi)) + 20))
                    
        }, completion: { (Bool) -> Void in
            self.imgKids.isHighlighted = false
        })
    }
    
    //하위 카테고리 페이지로 이동
    func goSubcategoryPage(_cid : Int, arr : [CategoryDao])  {
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Sell", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "SELL_CATEGORY_VIEW")) as! SellCategoryViewController
        
        switch _cid {
        case 5 ... 9:
            vc.type = EKind.man.rawValue
            break
        case 10 ... 14:
            vc.type = EKind.woman.rawValue
            break
        case 15 ... 19:
            vc.type = EKind.kids.rawValue
            break
        default:
            break
        }
        vc.cId = _cid
        vc.arrCtegory = arr
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // 브랜드등록페이지로 이동
    func goBrandPage(_uid : Int) {
        
        gPdtRegInfo.setCategoryUid(_uid: _uid)
        if _uid < 20 {
            gCategoryUid = _uid
        }
        
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Sell", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "SELL_BRAND_VIEW")) as! SellBrandViewController
        
        vc.categoryUid = _uid
        nav.pushViewController(vc, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func cellAction(_ sender: UIButton) {
        
        for btn in btnCell {
            btn.isSelected = false
        }
        
        sender.isSelected = true
        
        getSubCategoryList(_uid: sender.tag)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 하위카테고리 목록 얻기
    func getSubCategoryList( _uid : Int) {
        
        gProgress.show()
        Net.getSubCategoryList(
            accessToken     : gMeInfo.token,
            categoryUid     : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSubCategoryListResult
                self.getSubCategoryListResult(res, _uid: _uid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //하위카테고리 목록 얻기 결과
    func getSubCategoryListResult(_ data: Net.getSubCategoryListResult, _uid : Int) {
        
        if _uid < 20 {
            gCategoryUid = _uid
        }

        //하위카테고리가 0이면 곧바로 브렌드 페이지로 이행  , 있다면 하위카테고리 페이지로 이행
        if data.list.count == 0 {
            gPdtRegInfo.setCategoryName(value: bigCategory[_uid - 5])
            goBrandPage(_uid: _uid)
        } else {
            goSubcategoryPage(_cid: _uid, arr: data.list)
        }
    }
}

