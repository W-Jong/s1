//
//  SellCameraViewController.swift
//  selluv
//  카메라뷰
//  Created by Gambler on 12/22/17.
//  modified by PJH on 16/03/18.

import UIKit
import AVFoundation
import Photos
import Toast_Swift

protocol SellCameraViewControllerDelegate {
    func setPhotos( photos : Array<String>, photosFiles : Array<String>)
}

class SellCameraViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    var delegate:  SellCameraViewControllerDelegate?
    @IBOutlet var btnFlash: UIButton!
    @IBOutlet var btnGuide: UIButton!
    @IBOutlet var btnRotate: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnUpload: UIButton!
    @IBOutlet var vwCamera: UIView!
    @IBOutlet var clvImage: UICollectionView!
    
    var captureSession = AVCaptureSession()
    var stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    
    var m_bFlag = false  //상품 상세페이지에서 들어오면 true, 상품 추가페이지에서 들어오면 false로 처리하자
    var isEdit : Bool!
    var pdtUid : Int!
    var assetsList = [UIImage]() {
        didSet {
            if m_bFlag {
//                btnUpload.isHidden = assetsList.count < 1
            } else {
                btnNext.isHidden = assetsList.count < 1
            }
        }
    }
    
    enum EFlashMode {
        case on
        case off
    }
    
    var flashMode = EFlashMode.off {
        didSet {
            btnFlash.isSelected = flashMode == .on
            
            if captureDevice != nil {
                do {
                try captureDevice?.lockForConfiguration()
                    captureDevice?.torchMode = flashMode == .on ? AVCaptureDevice.TorchMode.on : AVCaptureDevice.TorchMode.off
                    captureDevice?.unlockForConfiguration()
                } catch  {
                    print("Change Torch Mode Failed")
                }
            }
        }
    }
    
    var isDeleteMode = false {
        didSet {
            clvImage.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        findDevice(type: btnRotate.isSelected ? 1 : 0)

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if previewLayer != nil {
            previewLayer!.frame = CGRect.init(x: 0, y: 0, width: self.vwCamera.frame.width, height: self.vwCamera.frame.height)
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        btnRotate.isSelected = false
        
        vwCamera.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.touchAction)))
        
        (clvImage.collectionViewLayout as! RAReorderableLayout).scrollDirection = .horizontal
        clvImage.register(UINib(nibName: "PhotoGridCell", bundle: nil), forCellWithReuseIdentifier: "photoCell")
        
        if m_bFlag {
            btnGuide.setImage(nil, for: .normal)
            btnGuide.isEnabled = false
            btnUpload.isHidden = false
        } else {
            btnGuide.setImage(UIImage(named: "ic_camera_guide"), for: .normal)
            btnGuide.isEnabled = true
        }
        
    }
    
    func findDevice(type: Int) {
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        
        if let devices = AVCaptureDevice.devices() as? [AVCaptureDevice] {
            // Loop through all the capture devices on this phone
            for device in devices {
                // Make sure this particular device supports video
                if (device.hasMediaType(AVMediaType.video)) {
                    if type == 0 {
                        // Finally check the position and confirm we've got the back camera
                        if(device.position == AVCaptureDevice.Position.back) {
                            captureDevice = device
                            
                            if captureDevice != nil {
                                print("Capture device found")
                                beginSession()
                            }
                        }
                    } else {
                        if(device.position == AVCaptureDevice.Position.front) {
                            captureDevice = device
                            
                            if captureDevice != nil {
                                print("Capture device found")
                                beginSession()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func beginSession() {
        
        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice!))
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecType.jpeg]
            
            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }
            
        }
        catch {
            print("error: \(error.localizedDescription)")
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        if previewLayer == nil {
            return
        }
        
        self.vwCamera.layer.addSublayer(previewLayer!)
        previewLayer!.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        captureSession.startRunning()
        
    }
    
    func initCamera() {
        do {
            captureSession.stopRunning()
            previewLayer = nil
            captureSession.removeOutput(stillImageOutput)
            stillImageOutput = AVCaptureStillImageOutput()
            try captureSession.removeInput(AVCaptureDeviceInput(device: captureDevice!))
            captureDevice = nil
            captureSession = AVCaptureSession()
        }
        catch {
            print("error: \(error.localizedDescription)")
        }
    }
    
    func saveToCamera() {
        
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
            
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (CMSampleBuffer, Error) in
                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(CMSampleBuffer!) {
                    
                    if let cameraImage = UIImage(data: imageData) {
                        self.assetsList.append(cameraImage)
                        self.clvImage.reloadData()
                    }
                }
            })
        }
    }
    
    func presentCropViewController(image: UIImage) {
        
//        let cropViewController = TOCropViewController(image: image)
//        cropViewController.delegate = self
//        present(cropViewController, animated: true, completion: nil)
    }
    
    func getImageFromAsset(assetsList : [PHAsset]) -> [UIImage] {
        var resImg = [UIImage]()
        
        let option = PHImageRequestOptions()
        option.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        option.isSynchronous = true
        
        for asset in assetsList {
            let imageSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
            
            PHCachingImageManager.default().requestImage(for: asset, targetSize: imageSize, contentMode: .aspectFill, options: option) { (result, _) in
                if let image = result {
                    resImg.append(image)
                }
            }
        }
        
        return resImg
    }

    //상품 정보 페이지로
    func getPdtInfoPage() {
        
        //등록하던 상품정보를 저장한다., 이어하기 기능을 위해 사용한다.
        let ud: UserDefaults = UserDefaults.standard
        ud.set(gPdtRegInfo.brandUid, forKey: "brandUid")
        ud.set(gPdtRegInfo.nameKo, forKey: "nameKo")
        ud.set(gPdtRegInfo.nameEn, forKey: "nameEn")
        ud.set(gPdtRegInfo.categoryUid, forKey: "categoryUid")
        ud.set(gPdtRegInfo.pdtGroup, forKey: "pdtGroup")
        ud.set(gPdtRegInfo.pdtThumb, forKey: "pdtThumb")
        ud.set(gPdtRegInfo.categoryName, forKey: "categoryName")
        ud.set(gPdtRegInfo.photos, forKey: "photos")
        ud.synchronize()
        
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_INFO_VIEW")) as! SellProductInfoViewController
        vc.sizeType = 0
        vc.isEdit = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////

    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func flashAction(_ sender: Any) {
        
        switch flashMode {
        case .on:
            flashMode = .off
        case .off:
            flashMode = .on
        }
        
    }
    
    @IBAction func rotateAction(_ sender: Any) {
        
        btnRotate.isSelected = !btnRotate.isSelected
        initCamera()
        findDevice(type: btnRotate.isSelected ? 1 : 0)
        
    }
    
    @IBAction func nextAction(_ sender: Any) {

        var imgdata : [Data] = []
        for i in 0..<assetsList.count {
            let data = UIImageJPEGRepresentation(self.assetsList[i], 0.5)
            imgdata.append(data!)
        }
        uploadFiles(data: imgdata)
        
    }
    
    @objc func sizeGuideAction(_ sender : UIButton) {
        
        SizeGuide.show(self)
        
    }
    
    @IBAction func shutterAction(_ sender: Any) {
        
        saveToCamera()
        
    }
    
    @IBAction func galleryAction(_ sender: Any) {
        
        PhotoGallery.show(self) { (photo) in
            
            guard photo.count > 0 else {
                return
            }
            
            self.assetsList.append(contentsOf: self.getImageFromAsset(assetsList: photo))
            self.clvImage.reloadData()
        }
        
    }
    
    @IBAction func guideAction(_ sender: Any) {
        
//        CameraGuide.show(self)
        SellUseGuide.show(self)
        
    }
    
    @IBAction func uploadAction(_ sender: Any) {
        if m_bFlag {
            
            var imgdata : [Data] = []
            for i in 0..<assetsList.count {
                let data = UIImageJPEGRepresentation(self.assetsList[i], 0.5)
                imgdata.append(data!)
            }
            uploadFiles(data: imgdata)
        } else {
            
        }
    }
    
    @objc func delAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        assetsList.remove(at: clickedBtn.tag)
        
        clvImage.reloadData()
    }
    
    @objc func selAction(_ sender: UIButton) {
        
        PhotoEdit.show(self, imgList: assetsList, index: sender.tag + 1) { (img) in
            self.assetsList.removeAll()
            self.assetsList.append(contentsOf: img)
            self.clvImage.reloadData()
            
        }
        
    }
    
    @objc func touchAction(_ sender : Any) {
        isDeleteMode = false
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 여러개 파일 업로드
    func uploadFiles( data : [Data]) {
        
        gProgress.show()
        Net.uploadFiles(
            file            : data,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UploadFilesResult
                if self.isEdit {
                    self.delegate?.setPhotos(photos: res.fileUrls, photosFiles: res.fileNames)
                    self.popVC()
                } else {
                    if self.m_bFlag {
                        //스타일 등록하자
                        self.regMultiStyleInfo(imgNames: res.fileNames)
                    } else {
                        //상품 등록시
                        gPdtRegInfo.setPhotos(value: res.fileNames)
                        gPdtRegInfo.setPdtThumb(value: res.fileUrls[0])
                        self.getPdtInfoPage()
                    }
                }
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 여러개 파일 업로드
    func regMultiStyleInfo( imgNames : Array<String>) {
        
        gProgress.show()
        Net.regMultiStyleInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            styleImgList    : imgNames,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("reg_style_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}


//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

//extension SellCameraViewController : AVCapturePhotoCaptureDelegate {
//
//    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
//        let image = UIImage(data: photo.fileDataRepresentation()!)
//        imageList.append(image!)
//    }
//
//}

extension SellCameraViewController : UICollectionViewDelegate, RAReorderableLayoutDataSource, RAReorderableLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assetsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoGridCell
        
        let assets = assetsList[indexPath.row]
        cell.ivCell.image = assets
        
        cell.btnDel.tag = indexPath.row
        cell.btnDel.addTarget(self, action: #selector(self.delAction), for: .touchUpInside)
        cell.btnDel.isHidden = !isDeleteMode
        
        cell.btnCell.tag = indexPath.row
        cell.btnCell.addTarget(self, action: #selector(self.selAction), for: .touchUpInside)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected")
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoGridCell
        presentCropViewController(image: cell.ivCell.image!)
    }
    
    func collectionView(_ collectionView: UICollectionView, at: IndexPath, willMoveTo toIndexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, at: IndexPath, didMoveTo toIndexPath: IndexPath) {
        let book = assetsList.remove(at: (at as NSIndexPath).item)
        assetsList.insert(book, at: (toIndexPath as NSIndexPath).item)
    }
    
    func scrollTrigerEdgeInsetsInCollectionView(_ collectionView: UICollectionView) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 45, 0, 45)
    }
    
    func scrollSpeedValueInCollectionView(_ collectionView: UICollectionView) -> CGFloat {
        return 15.0
    }
    
    func collectionView(_ collectionView: UICollectionView, collectionView layout: RAReorderableLayout, willBeginDraggingItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoGridCell
        cell.btnDel.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, collectionView layout: RAReorderableLayout, didBeginDraggingItemAt indexPath: IndexPath) {
        
        isDeleteMode = true
        
    }
    
    func collectionView(_ collectionView: UICollectionView, collectionView layout: RAReorderableLayout, didEndDraggingItemTo indexPath: IndexPath) {
        
        collectionView.reloadData()
        
    }
}
