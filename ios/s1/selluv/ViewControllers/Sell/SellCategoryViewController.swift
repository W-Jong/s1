//
//  SellCategoryViewController.swift
//  selluv
//  카테고리1
//  Created by Gambler on 12/20/17.
//  modified by PJH on 21/03/18.

import UIKit

class SellCategoryViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var ivBg: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tblList: UITableView!
    
    enum EShopCategory : Int {
        case c1 = 1
        case c2
        case c3
    }
    
    var type        : Int = EKind.man.rawValue
    var step        = EShopCategory.c2
    var cId         = 1  //카테고리 아이디
    var name        : String!
    var arrCtegory  : [CategoryDao] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        tblList.register(UINib(nibName: "ShopCategoryListCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        if step == .c2 {
            switch type {
            case EKind.man.rawValue:
                ivBg.backgroundColor = UIColor(hex: 0x42C2FE)
                lbName.text = MenCategory1[(cId - 5 ) % 5 + 1].name
                ivIcon.image = UIImage(named: MenCategory1[(cId - 5 ) % 5 + 1].img)
            case EKind.woman.rawValue:
                ivBg.backgroundColor = UIColor(hex: 0xFF3B7E)
                lbName.text = WomenCategory1[(cId - 5 ) % 5 + 1].name
                ivIcon.image = UIImage(named: WomenCategory1[(cId - 5 ) % 5 + 1].img)
            case EKind.kids.rawValue:
                ivBg.backgroundColor = UIColor(hex: 0x3A0D7D)
                lbName.text = KidsCategory1[(cId - 5 ) % 5 + 1].name
                ivIcon.image = UIImage(named: KidsCategory1[(cId - 5 ) % 5 + 1].img)
            default:
                ivBg.backgroundColor = UIColor(hex: 0x42C2FE)
            }
        } else {
            lbName.text = name
        }

    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @objc func selectAction(_ sender : UIButton) {
        let dic : CategoryDao = arrCtegory[sender.tag]
        
        gPdtRegInfo.setCategoryName(value: dic.categoryName)
        
        if step == .c2 {
            getSubCategoryList(_uid: dic.categoryUid, name: dic.categoryName)
        } else if step == .c3 {
            goBrandPage(_uid: dic.categoryUid)
        }
    }
    
    // 브랜드등록페이지로 이동
    func goBrandPage(_uid : Int) {
        
        gPdtRegInfo.setCategoryUid(_uid: _uid)
        if _uid < 20 {
            gCategoryUid = _uid
        }
     
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Sell", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "SELL_BRAND_VIEW")) as! SellBrandViewController
        
        vc.categoryUid = _uid
        nav.pushViewController(vc, animated: true)
    }
    
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 하위카테고리 목록 얻기
    func getSubCategoryList( _uid : Int, name : String) {
        
        gProgress.show()
        Net.getSubCategoryList(
            accessToken     : gMeInfo.token,
            categoryUid     : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSubCategoryListResult
                self.getSubCategoryListResult(res, _uid: _uid, title: name)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //하위카테고리 목록 얻기 결과
    func getSubCategoryListResult(_ data: Net.getSubCategoryListResult, _uid : Int, title : String) {
        
        if data.list.count > 0 {
            
            // 카테고리3이 있는 경우 카테고리3페이지로 이동
            let nav : UINavigationController! = self.navigationController
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Sell", bundle: nil)
            let vc = (storyboard.instantiateViewController(withIdentifier: "SELL_CATEGORY_VIEW")) as! SellCategoryViewController
            
            vc.type = type
            vc.step = .c3
            vc.cId = cId
            vc.name = title
            vc.arrCtegory = data.list
            nav.pushViewController(vc, animated: true)
            
        } else {
            // 브랜드등록페이지로 이동
            goBrandPage(_uid: _uid)
        }
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension SellCategoryViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCtegory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! ShopCategoryListCell
        
        let dic : CategoryDao = arrCtegory[indexPath.row]
        
        cell.lbName.text = dic.categoryName
        cell.btnCell.tag = indexPath.row
        cell.btnCell.addTarget(self, action: #selector(self.selectAction), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}

