//
//  ValetCompleteViewController.swift
//  selluv
//  발렛신청 완료
//  Created by Gambler on 1/13/18.
//  modified by PJh on 26/03/18.

import UIKit

class ValetCompleteViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var lbGoodsName: UILabel!
    @IBOutlet var lbTrans: UILabel!
    @IBOutlet var lbCost: UILabel!
    
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbPhone: UILabel!
    @IBOutlet var lbAddr: UILabel!
    
    var sendType : Int!
    var reqPrice : Int!
    var name     : String!
    var contact  : String!
    var address  : String!
    var brand    : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        lbGoodsName.text = brand
        if sendType == 1 {
            lbTrans.text = "배송키로 이용"
        } else {
            lbTrans.text = "직접 발송"
        }
        
        lbCost.text = reqPrice == 0 ? "추천가격" : CommonUtil.formatNum(reqPrice) + " 원"
        lbName.text = name
        lbPhone.text = contact
        lbAddr.text = address
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////

    @IBAction func okAction(_ sender: Any) {
        popVC(-7)
    }
}
