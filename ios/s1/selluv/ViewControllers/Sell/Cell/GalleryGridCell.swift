//
//  GalleryGridCell.swift
//  selluv
//
//  Created by Gambler on 12/25/17.
//

import UIKit

class GalleryGridCell: UICollectionViewCell {

    @IBOutlet var ivPhoto: UIImageView!
    @IBOutlet var ivMask: UIImageView!
    @IBOutlet var ivTick: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initCell()
    }
    
    func initCell() {
        ivMask.isHidden = true
        ivTick.isHidden = true
    }

}
