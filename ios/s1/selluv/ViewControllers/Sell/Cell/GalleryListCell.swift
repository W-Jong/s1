//
//  GalleryListCell.swift
//  selluv
//
//  Created by Gambler on 12/25/17.
//

import UIKit

class GalleryListCell: UITableViewCell {

    @IBOutlet var ivPhoto: UIImageView!
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
