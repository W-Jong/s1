//
//  SellMainViewController.swift
//  selluv
//
//  Created by Gambler on 1/5/18.
//

import UIKit
import Toast_Swift

class SellMainViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    @IBOutlet var ivSymbol: UIImageView!
    
    @IBOutlet var vwTop: NSLayoutConstraint!
    @IBOutlet var vwBottom: NSLayoutConstraint!
    
    @IBOutlet var symbolTop: NSLayoutConstraint!
    @IBOutlet var logoTop: NSLayoutConstraint!
    
    @IBOutlet var btnSellTop: NSLayoutConstraint!
    @IBOutlet var btnServicetop: NSLayoutConstraint!
    
    @IBOutlet var vwContinueBottom: NSLayoutConstraint!
    @IBOutlet var btnCloseBottom: NSLayoutConstraint!
    @IBOutlet var vwGuideBottom: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        vwTop.constant = self.view.frame.size.height
        vwBottom.constant = -self.view.frame.size.height
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func delay(seconds: Double, completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
    }
    
    func initVC() {
        gCategoryUid = 0
        UIView.animateAndChain(withDuration: LoginViewAnimationTime, delay: 0.1,usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: .curveEaseInOut, animations: {
            
            self.vwTop.constant = 5
            self.vwBottom.constant = -5
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: 0.1, delay: 0.0, options: [],
                                    animations: { () -> Void in
            
            self.vwTop.constant = 0
            self.vwBottom.constant = 0
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            
        })
    }
    
    //상품 정보 페이지로
    func getPdtInfoPage() {
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_INFO_VIEW")) as! SellProductInfoViewController
        vc.sizeType = 0
        vc.isEdit = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////

    @IBAction func sellAction(_ sender: Any) {
        
        gPdtRegInfo.initial()
        clearPdtStore()
        
        actionType = .sell
        pushVC("SELL_GROUP_VIEW", storyboard: "Sell", animated: true)
    }
    
    @IBAction func serviceAction(_ sender: Any) {
        
        gPdtRegInfo.initial()
        clearPdtStore()
        
        actionType = .valet
        pushVC("SELL_SERVICE_VIEW", storyboard: "Sell", animated: true)
    }
    
    @IBAction func guideAction(_ sender: Any) {
        
        SellUseGuide.show(self)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        UIView.animateAndChain(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.vwTop.constant = 5
            self.vwBottom.constant = -5
            self.view.layoutIfNeeded()
            
        }, completion: nil).animate(withDuration: LoginViewAnimationTime, delay: 0.0, options: [],
                                    animations: { () -> Void in
                                        
            self.vwTop.constant = self.view.frame.size.height
            self.vwBottom.constant = -self.view.frame.size.height
            self.view.layoutIfNeeded()
                                        
        }, completion: { (Bool) -> Void in
            self.popVC(-1, animated: false)
        })
        
    }
    
    //이어서 등록
    @IBAction func continueAction(_ sender: Any) {
        
        gPdtRegInfo.initial()
        
        let ud: UserDefaults = UserDefaults.standard
        gPdtRegInfo.setBrandUid(_uid: ud.integer(forKey: "brandUid"))
        gPdtRegInfo.setNameKo(value: ud.string(forKey: "nameKo")!)
        gPdtRegInfo.setNameEn(value: ud.string(forKey: "nameEn")!)
        gPdtRegInfo.setCategoryUid(_uid: ud.integer(forKey: "categoryUid"))
        gPdtRegInfo.setPdtGroup(value: ud.string(forKey: "pdtGroup")!)
        gPdtRegInfo.setPdtThumb(value: ud.string(forKey: "pdtThumb")!)
        gPdtRegInfo.setCategoryName(value: ud.string(forKey: "categoryName")!)
        gPdtRegInfo.setPhotos(value: ud.stringArray(forKey: "photos")!)
        
        //포토리스트만 따져도 사진완료단게까지 했다는것을 알수 있음
        if gPdtRegInfo.getPhotos().count > 0  {
            let alertController = UIAlertController(title: NSLocalizedString("continue_reg", comment:""), message: NSLocalizedString("delete_alert_msg", comment:""), preferredStyle: .alert)
            let actOK = UIAlertAction(title: NSLocalizedString("continue", comment:""), style: .default) { (action:UIAlertAction) in
                //등록하던 상품 게속 등록하기
                self.getPdtInfoPage()
            }
            
            let actCancel = UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(actOK)
            alertController.addAction(actCancel)
            self.present(alertController, animated: true, completion: nil)
        } else {
            CommonUtil.showToast(NSLocalizedString("empty_reg_working", comment:""))
        }
    }
    
    func clearPdtStore(){
        
        //등록하던 상품정보를 초기화한다.
        let ud: UserDefaults = UserDefaults.standard
        ud.set(1, forKey: "brandUid")
        ud.set("", forKey: "nameKo")
        ud.set("", forKey: "nameEn")
        ud.set(0, forKey: "categoryUid")
        ud.set("", forKey: "pdtGroup")
        ud.set("", forKey: "pdtThumb")
        ud.set("", forKey: "categoryName")
        ud.set([], forKey: "photos")
        ud.synchronize()

    }
}
