//
//  CostSetViewController.swift
//  selluv
//  가격설정
//  Created by Gambler on 12/28/17.
//  modified by PJH on 20/03/18.

import UIKit
protocol CostSetViewControllerDelegate {
    func setValue( salePrice : Double, sendPrice : Double, nagoYn : Bool, code : String)
}

class CostSetViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    var delegate:  CostSetViewControllerDelegate?
    @IBOutlet var tfCost: UITextField!
    @IBOutlet var btnTransCost: UIButton!
    @IBOutlet var lbSum: UILabel!
    @IBOutlet var schNego: UISwitch!
    @IBOutlet var ivPromotion: UIImageView!
    @IBOutlet var btnPromotion: UIButton!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var vwClose: UIView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    @IBOutlet var vwTransCost: UIView!
    @IBOutlet var vwProCode: UIView!
    
    var pageTimer : Timer?
    var delay = 0
    var longPressed = false
    
    var isEdit              : Bool!
    var dicPdtInfo          : PdtDetailDto!
    
    var sumPrice            : Double = 0  //정산가격
    var salePrice           : Double = 0  //판매가격
    var sendPrice           : Double = -1 //배송비
    var selluvUseMoney      : Double = 0  //셀럽 이용료
    var payUseMoney         : Double = 0  //결제수수료
    var promotionPay        : Double = 0  //프로모션 코드 페이백
    var promotionCode       : String = ""
    
    var cost = 5.0 {
        didSet {
            if cost < 5.0 {
                cost = 5.0
            }
            
            tfCost.text = String(cost)
            btnMinus.isEnabled  = cost > 5.0
        }
    }
    
    var transCost : Double = 3000 {
        didSet {
            btnTransCost.setTitle(CommonUtil.formatNum(transCost) + "원", for: .normal)
        }
    }
    
    var proCode = "" {
        didSet {
            ivPromotion.isHidden = proCode == ""
            btnPromotion.setTitle(proCode, for: .normal)
        }
    }
    
    var negoEnable = true {
        didSet {
            schNego.isOn = negoEnable
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        tfCost.becomeFirstResponder()
//        tfCost.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        tfCost.text = String(cost)
        
        btnTransCost.setTitle(CommonUtil.formatNum(transCost) + "원", for: .normal)
        
        ivPromotion.isHidden = true
        btnPromotion.setTitle("", for: .normal)
        
        vwTransCost.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(vwTransAction)))
        vwProCode.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(vwProAction)))

        cost = 5.0
        if isEdit {
            transCost = Double(dicPdtInfo.pdt.sendPrice)
            cost = Double(dicPdtInfo.pdt.price) / 10000
            
            if dicPdtInfo.pdt.negoYn == 1 {
                negoEnable = true
            } else {
                negoEnable = false
            }
        }
        lbSum.text = calcMoneySum()
    }
    
    func hideKeyboard() {
        tfCost.resignFirstResponder()
    }
    
    @objc func vwTransAction(_ sender : Any) {
        transCostAction(sender)
    }
    
    @objc func vwProAction(_ sender : Any) {
        promotionAction(sender)
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = keyboardSize.height
                    self.vwClose.isHidden = false
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.vwClose.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
    
    //정산금액 계산
    func calcMoneySum() -> String {
     
        salePrice = cost * 10000
        sendPrice = transCost
        selluvUseMoney = (salePrice + sendPrice) * 0.089
        payUseMoney = (salePrice + sendPrice) * 0.035
        
        if salePrice > 120000 {
            sumPrice = salePrice + sendPrice - selluvUseMoney - payUseMoney + promotionPay
        } else {
            sumPrice = salePrice + sendPrice - 15000 - payUseMoney + promotionPay
        }
        
        return String.init(format: "%.2f", sumPrice / 10000)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func textChanged(_ sender : UITextField) {
        
        if sender == tfCost {
            if let tcost = Double(tfCost.text!) {
                cost = tcost
                lbSum.text = calcMoneySum()
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        if isEdit {
            self.delegate?.setValue(salePrice: salePrice, sendPrice: sendPrice, nagoYn: schNego.isOn, code: promotionCode)
            popVC()
        } else {
            
            gPdtRegInfo.setSendPrice(value: Int(sendPrice))
            gPdtRegInfo.setPrice(value: Int(salePrice))
            gPdtRegInfo.setNegoYn(value: schNego.isOn ? 1 : 2)
            gPdtRegInfo.setPromotioncode(value: promotionCode)
            
            SellPolicy.show(self, okCallback: { (sign) in

                let data = UIImageJPEGRepresentation(sign, 0.8)
                self.uploadFile(data: data!)

            }, guideCallback: {
                self.pushVC("MYPAGE_SALEPOLICY_VIEW", storyboard: "Mypage", animated: true)
            })
        }
    }
    
    @IBAction func minusAction(_ sender: Any) {
        
        if cost < 5.0 {
            return
        }
        
        cost -= 0.5
        
        delay += 1
        if longPressed {
            if delay >= 6 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(minusAction(_:)), userInfo: nil, repeats: true)
            }
            if delay >= 16 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(minusAction(_:)), userInfo: nil, repeats: true)
            }
        }
        lbSum.text = calcMoneySum()
    }
    
    @IBAction func longMinusAction(_ sender: UILongPressGestureRecognizer) {
        
        switch sender.state {
        case .began:
            delay = -1
            longPressed = true
            pageTimer?.invalidate()
            pageTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(minusAction(_:)), userInfo: nil, repeats: true)
            
        case .ended:
            delay = -1
            longPressed = false
            pageTimer?.invalidate()
        default:
            return
        }
        
    }
    
    
    @IBAction func plusAction(_ sender: Any) {
        
        cost += 0.5
        
        delay += 1
        if longPressed {
            if delay >= 6 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(plusAction(_:)), userInfo: nil, repeats: true)
            }
            if delay >= 16 {
                pageTimer?.invalidate()
                pageTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(plusAction(_:)), userInfo: nil, repeats: true)
            }
        }
        lbSum.text = calcMoneySum()
    }
    
    @IBAction func longPlusAction(_ sender: UILongPressGestureRecognizer) {
        
        switch sender.state {
        case .began:
            delay = -1
            longPressed = true
            pageTimer?.invalidate()
            pageTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(plusAction(_:)), userInfo: nil, repeats: true)
        case .ended:
            delay = -1
            longPressed = false
            pageTimer?.invalidate()
        default:
            return
        }
        
    }
    
    @IBAction func transCostAction(_ sender: Any) {
        
        hideKeyboard()
        
        TransCostSet.show(self, cost: transCost) { (cost) in
            self.transCost = cost
            self.lbSum.text = String(self.calcMoneySum())
        }
    }
    
    @IBAction func guideAction(_ sender: Any) {

        let nav : UINavigationController! = self.navigationController
        let vc = SumDetail(nibName: "SumDetail", bundle: nil)
        vc.salePrice = salePrice
        vc.sendPrice = sendPrice
        vc.selluvUseMoney = selluvUseMoney
        vc.payUseMoney = payUseMoney
        vc.promotionPay = promotionPay
        vc.sumPrice = sumPrice
        nav.pushViewController(vc, animated: true)
    }
    
    @IBAction func negoAction(_ sender: Any) {
        negoEnable = !negoEnable
    }
    
    @IBAction func promotionAction(_ sender: Any) {
        
        hideKeyboard()
        
        PromotionInput.show(self, code: proCode) { (code) in
            self.getPromotionInfo(code: code)
        }
    }
    
    @IBAction func inputAction(_ sender: Any) {
        
        if let digit = Double(tfCost.text!) {
            cost = digit
        } else {
            if tfCost.hasText {
                MsgUtil.showUIAlert("잘못된 숫자형식입니다.")
            }
            cost = 0.0
        }
        
        cost = cost >= 5.0 ? cost.round(2) : 5.0
        lbSum.text = calcMoneySum()
        hideKeyboard()
    }

    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 프로모션 정보 얻기
    func getPromotionInfo(code : String){
        
        gProgress.show()
        Net.getPromotionInfo(
            accessToken     : gMeInfo.token,
            brandUid        : dicPdtInfo.brand.brandUid,
            categoryUid     : dicPdtInfo.pdt.categoryUid,
            dealType        : "BUYER",
            pdtUid          : dicPdtInfo.pdt.pdtUid,
            promotionCode   : code,
            success: { (result) -> Void in
                gProgress.hide()
                self.promotionCode = code
                let res = result as! Net.getPromotionInfoResult
                self.getPromotionInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                self.proCode = ""
                CommonUtil .showToast(err)
            }
        })
    }
    
    //프로모션 정보 얻기 결과
    func getPromotionInfoResult(_ data: Net.getPromotionInfoResult) {
        
        //promotionSale 값 결정
        if data.dicInfo.priceUnit == 1 {  //단위가 퍼센트
            self.proCode = "판매이용료" + String(data.dicInfo.price) + "% 할인"
            promotionPay = cost * (Double(data.dicInfo.price) / 100)
        } else if data.dicInfo.priceUnit == 2 { //단위가 원
            promotionPay = Double(data.dicInfo.price)
            self.proCode = CommonUtil.formatNum(data.dicInfo.price)
        }
        self.lbSum.text = self.calcMoneySum()
    }
    
    // 상품 등록
    func regPdtInfo( _singimg : String){
        
        gProgress.show()
        Net.reqPdtInfo(
            accessToken     : gMeInfo.token,
            brandUid        : gPdtRegInfo.getBrandUid(),
            categoryUid     : gPdtRegInfo.getCategoryUid(),
            component       : gPdtRegInfo.getComponent(),
            content         : gPdtRegInfo.getContent(),
            etc             : gPdtRegInfo.getEtc(),
            negoYn          : gPdtRegInfo.getNegoYn(),
            pdtColor        : gPdtRegInfo.getPdtColor(),
            pdtCondition    : gPdtRegInfo.getPdtCondition(),
            pdtGroup        : gPdtRegInfo.getPdtGroup(),
            pdtModel        : gPdtRegInfo.getPdtModel(),
            pdtSize         : gPdtRegInfo.getPdtSize(),
            photos          : gPdtRegInfo.getPhotos(),
            price           : gPdtRegInfo.getPrice(),
            promotionCode   : gPdtRegInfo.getPromotioncode(),
            sendPrice       : gPdtRegInfo.sendPrice,
            signImg         : _singimg,
            tag             : gPdtRegInfo.getTag(),
            success: { (result) -> Void in
                gProgress.hide()
                
                
                let res = result as! Net.regPdtInfoResult
                self.regPdtInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                self.proCode = ""
                CommonUtil .showToast(err)
            }
        })
    }
    
    func regPdtInfoResult(_ data: Net.regPdtInfoResult) {
        gPdtRegInfo.initial()
        CommonUtil.showToast(NSLocalizedString("pdt_reg_success", comment:""))
        
        let vc = SellComplete(nibName: "SellComplete", bundle: nil)
        vc.dicPdtInfo = data.dicPdtInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // 한개 파일 업로드
    func uploadFile( data : Data) {
        
        gProgress.show()
        Net.uploadFile(
            file            : data,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UploadFileResult
                self.regPdtInfo(_singimg: res.fileName)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
