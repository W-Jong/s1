//
//  MainViewController.swift
//  selluv
//  Main
//  Created by Gambler on 12/12/17.
//  modified by PJH on 03/03/18.

import UIKit
import SwiftyJSON
import DGElasticPullToRefresh

class MainViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var vwEmptyComT1: UIView!
    @IBOutlet weak var vwEmptyStyleT1: UIView!
    
    @IBOutlet weak var vwEmptyComT2: UIView!
    @IBOutlet weak var vwEmptyStyleT2: UIView!
    
    @IBOutlet weak var vwEmptyComT3: UIView!
    @IBOutlet weak var vwEmptyStyleT3: UIView!
    
    @IBOutlet weak var scvView: UIScrollView!
    @IBOutlet weak var clvT1: UICollectionView!
    @IBOutlet weak var clvT2: UICollectionView!
    @IBOutlet weak var clvT3: UICollectionView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnMenuT1: UIButton!
    @IBOutlet weak var btnMenuT2: UIButton!
    @IBOutlet weak var btnMenuT3: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var vwMenu: UIView!
    @IBOutlet weak var vwMenuBottom: NSLayoutConstraint!
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    
    //zoom효과관련
    weak var selectedImageView : UIImageView?
    
    var animator : ARNTransitionAnimator?
    var nLikeIndex : Int = 0
    
    var nPage           : Int = 0
    var isLast          : Bool!
    var arrPdtList      : Array<FeedPdtDto> = []
    var fSeed           : CLong!
    
    //collectionView관련
    
    enum ETab : Int {
        case T1 = 0
        case T2
        case T3
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    deinit {
        clvT1.dg_removePullToRefresh()
        clvT2.dg_removePullToRefresh()
        clvT3.dg_removePullToRefresh()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()

//        nCurPage = ETab.T2.rawValue
//        changePage()
        updatePushToken()

        // 페이지 진입시 인기탭으로 절환애니메이션타임을 1초 -> 10밀리초로 수정
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10), execute: {
            self.changePage()
        })
     
        addPullToRefresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        // Collection view attributes
        clvT1.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        clvT1.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        clvT1.collectionViewLayout = layout
        clvT2.collectionViewLayout = layout
        clvT3.collectionViewLayout = layout
        
        vwEmptyComT1.isHidden = true
        vwEmptyStyleT1.isHidden = true
        vwEmptyComT2.isHidden = true
        vwEmptyStyleT2.isHidden = true
        vwEmptyComT3.isHidden = true
        vwEmptyStyleT3.isHidden = true
        
        let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
        clvT1.register(viewNib, forCellWithReuseIdentifier: "cell")
        clvT2.register(viewNib, forCellWithReuseIdentifier: "cell")
        clvT3.register(viewNib, forCellWithReuseIdentifier: "cell")

    }
    
    func addPullToRefresh() {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        
        clvT1.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
            })
            }, loadingView: loadingView)
        clvT1.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        clvT1.dg_setPullToRefreshBackgroundColor(clvT1.backgroundColor!)
        
        let loadingView1 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView1.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        
        clvT2.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
                
            })
            }, loadingView: loadingView1)
        clvT2.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        clvT2.dg_setPullToRefreshBackgroundColor(clvT2.backgroundColor!)
        
        let loadingView2 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView2.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        
        clvT3.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getDataList()
                
            })
            }, loadingView: loadingView2)
        clvT3.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        clvT3.dg_setPullToRefreshBackgroundColor(clvT3.backgroundColor!)

    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuT1.isSelected = false
        btnMenuT2.isSelected = false
        btnMenuT3.isSelected = false
        
        switch gMainPageIndex {
        case ETab.T1.rawValue :
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
        case ETab.T2.rawValue :
            btnMenuT2.isSelected = true
            clickedBtn = btnMenuT2
        case ETab.T3.rawValue :
            btnMenuT3.isSelected = true
            clickedBtn = btnMenuT3
        default:
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        scrollUp()
        
        nPage = 0
        getDataList()
    }
    
    func scrollUp() {
        if scrollDirection == .up {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
        })
    }
    
    func scrollDown() {
        if scrollDirection == .down {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwTop.frame.height))
            self.vwMenu.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 3 * self.vwMenu.frame.height))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -130))
        })
    }
   
    func goPdtDetailPage(_pdtuid : Int) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        vc.pdtUid = _pdtuid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnAlarm(_ sender: Any) {
        
        pushVC("MYPAGE_FINDITEM_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @objc func onBtnLoveItem(_ sender: Any) {
        
        pushVC("LIKE_VIEW", storyboard: "Top", animated: true)
    }
    
    @objc func onBtnFollow(_ sender: UIButton) {
        
        let dic : FeedPdtDto = arrPdtList[sender.tag]
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dicBrand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)

//        pushVC("BRAND_MORE_VIEW", storyboard: "Brand", animated: true)
    }
    
    @objc func onBtnUserItem(_ sender: UIButton) {
        
        dic["style"] = false
        dic["item"] = true
        let dic1 : FeedPdtDto  = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic1.pdtStyle.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @objc func onBtnUserStyle(_ sender: UIButton) {
        
        dic["item"] = false
        dic["style"] = true
        
        let dic1 : FeedPdtDto  = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic1.pdtStyle.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnTitle(_ sender: UIButton) {
        let dic : FeedPdtDto = arrPdtList[sender.tag]
        goPdtDetailPage(_pdtuid: dic.pdt.pdtUid)
    }
    
    @objc func onBtnStyle(_ sender: UIButton) {
        
        let dic : FeedPdtDto = arrPdtList[sender.tag]
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, _pdtUid: dicPdtStyle.pdtUid)

    }
        
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            if btnMenuT1.isSelected {
                cell = clvT1.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT1.contentOffset.y
            } else if btnMenuT2.isSelected {
                cell = clvT2.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT2.contentOffset.y
            } else if btnMenuT3.isSelected {
                cell = clvT3.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT3.contentOffset.y
            }
            
            nLikeIndex = indexPath.row
            let dic         : FeedPdtDto   = arrPdtList[indexPath.row]
            let dicPdt      : PdtMiniDto  = dic.pdt  //상품
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
            
        }
        
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        let dic : FeedPdtDto = arrPdtList[sender.tag]
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dicBrand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : FeedPdtDto = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        pushVC("FRIEND_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func brandAction(_ sender: Any) {
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Login", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "LOGIN_BRAND_VIEW") as! FavoriteBrandViewController
        vc.from = "Main"
        nav.pushViewController(vc, animated: true)
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        if btnRight.isSelected {
            btnRight.isSelected = false
            
//            clvT1.isHidden = false
//            vwEmptyComT1.isHidden = true
//            vwEmptyStyleT1.isHidden = true
//
//            clvT3.isHidden = false
//            vwEmptyComT3.isHidden = true
//            vwEmptyStyleT3.isHidden = true
        } else {
            btnRight.isSelected = true
            
//            clvT1.isHidden = false
//            vwEmptyComT1.isHidden = true
//            vwEmptyStyleT1.isHidden = true
//
//            clvT3.isHidden = true
//            vwEmptyComT3.isHidden = true
//            vwEmptyStyleT3.isHidden = false
        }
        
//        clvT1.reloadData()
//        clvT2.reloadData()
//        clvT3.reloadData()
        nPage = 0
        getDataList()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        self.replaceTab(clickedBtn.tag)
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        gMainPageIndex = clickedBtn.tag
       changePage()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuT1.isSelected {
            clvT1.scrollToTop(isAnimated: true)
        } else if btnMenuT2.isSelected {
            clvT2.scrollToTop(isAnimated: true)
        } else if btnMenuT3.isSelected {
            clvT3.scrollToTop(isAnimated: true)
        }
    }
    
    func changePage() {
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch gMainPageIndex {
        case ETab.T1.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 0
            break
        case ETab.T2.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 1
            break
        case ETab.T3.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 2
            break
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved()
    }
    
    func changeLikeStatus(status : Bool) {

        let dic         : FeedPdtDto   = arrPdtList[nLikeIndex]
        let dicPdt      : PdtMiniDto  = dic.pdt  //상품

        if btnRight.isSelected {
            for i in 0..<arrPdtList.count {
                let dicFeed : FeedPdtDto = arrPdtList[i]
                if dicPdt.pdtUid == dicFeed.pdt.pdtUid {
                    let  dicFeed1 : FeedPdtDto = dicFeed
                    dicFeed1.pdtLikeStatus = status
                    if status{
                        dicFeed1.pdtLikeCount = dicFeed.pdtLikeCount + 1
                    } else {
                        dicFeed1.pdtLikeCount = dicFeed.pdtLikeCount - 1
                    }
                    arrPdtList.remove(at: nLikeIndex)
                    arrPdtList.insert(dicFeed1, at: nLikeIndex)
                }
            }
        } else {
            let  dicFeed : FeedPdtDto = dic
            dicFeed.pdtLikeStatus = status
            if status{
              dicFeed.pdtLikeCount = dic.pdtLikeCount + 1
            } else {
              dicFeed.pdtLikeCount = dic.pdtLikeCount - 1
            }
            arrPdtList.remove(at: nLikeIndex)
            arrPdtList.insert(dicFeed, at: nLikeIndex)
        }
        

        if gMainPageIndex == ETab.T1.rawValue {  //피드
            clvT1.reloadData()
        } else if gMainPageIndex == ETab.T2.rawValue {  //인기
            clvT2.reloadData()
        } else { // 신상
            clvT3.reloadData()
        }
        
    }
    
    //서버로부터 데이터 가져오기
    func getDataList(){
        if btnRight.isSelected {
            if gMainPageIndex == ETab.T1.rawValue {  //피드
                getFeedStyleList()
            } else if gMainPageIndex == ETab.T2.rawValue {  //인기
                getFameStyleList()
            } else { // 신상
                getNewStyleList()
            }
        } else {
            if gMainPageIndex == ETab.T1.rawValue {  //피드
                getFeedList()
            } else if gMainPageIndex == ETab.T2.rawValue {  //인기
                getFameList()
            } else { // 신상
                getNewList()
            }
        }
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 피드목록 얻기
    func getFeedList() {
        
        gProgress.show()
        Net.getHomeFeed(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self.clvT1.dg_stopLoading()
                let res = result as! Net.FeedPdtResult
                self.getFeedListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not user,
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 피드스타일목록 얻기
    func getFeedStyleList() {
        
        gProgress.show()
        Net.getHomeFeedStyle(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self.clvT1.dg_stopLoading()
                let res = result as! Net.FeedPdtResult
                self.getFeedListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS  {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 인기목록 얻기
    func getFameList() {
        
        if nPage == 0 {
             fSeed = CLong(arc4random())
        }
        
        gProgress.show()
        Net.getHomeFame(
            accessToken     : gMeInfo.token,
            page            : nPage,
            seed: fSeed,
            success: { (result) -> Void in
                gProgress.hide()
                self.clvT2.dg_stopLoading()
                let res = result as! Net.FeedPdtResult
                self.getFeedListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 인기스타일목록 얻기
    func getFameStyleList() {
        
        if nPage == 0 {
            fSeed = CLong(arc4random())
        }
        
        gProgress.show()
        Net.getHomeFameStyle(
            accessToken     : gMeInfo.token,
            page            : nPage,
            seed: fSeed,
            success: { (result) -> Void in
                gProgress.hide()
                self.clvT2.dg_stopLoading()
                let res = result as! Net.FeedPdtResult
                self.getFeedListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 신상목록 얻기
    func getNewList() {
        
        gProgress.show()
        Net.getHomeNew(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self.clvT3.dg_stopLoading()
                let res = result as! Net.FeedPdtResult
                self.getFeedListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 신상스타일목록 얻기
    func getNewStyleList() {
        
        gProgress.show()
        Net.getHomeNewStyle(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self.clvT3.dg_stopLoading()
                let res = result as! Net.FeedPdtResult
                self.getFeedListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //피드목록 얻기 결과
    func getFeedListResult(_ data: Net.FeedPdtResult) {
//        clvT2.endRefreshing(at: .top)
        scrollUp()
        isLast = data.last
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : FeedPdtDto = data.list[i]
            arrPdtList.append(dic)
        }
    
        vwEmptyComT1.isHidden = true
        vwEmptyStyleT1.isHidden = true
        
        vwEmptyComT2.isHidden = true
        vwEmptyStyleT2.isHidden = true
        
        vwEmptyComT3.isHidden = true
        vwEmptyStyleT3.isHidden = true
        
        clvT1.isHidden = true
        clvT2.isHidden = true
        clvT3.isHidden = true
        
        if arrPdtList.count == 0 {
            
            if btnRight.isSelected {
                if gMainPageIndex == ETab.T1.rawValue {  //피드
                    vwEmptyStyleT1.isHidden = false
                } else if gMainPageIndex == ETab.T2.rawValue {  //인기
                    vwEmptyStyleT2.isHidden = false
                } else { // 신상
                    vwEmptyStyleT3.isHidden = false
                }
            } else {
                if gMainPageIndex == ETab.T1.rawValue {  //피드
                    vwEmptyComT1.isHidden = false
                } else if gMainPageIndex == ETab.T2.rawValue {  //인기
                    vwEmptyComT2.isHidden = false
                } else { // 신상
                    vwEmptyComT3.isHidden = false
                }
            }
        } else {
            if gMainPageIndex == ETab.T1.rawValue {  //피드
                clvT1.isHidden = false
                clvT1.reloadData()
            } else if gMainPageIndex == ETab.T2.rawValue {  //인기
                clvT2.isHidden = false
                clvT2.reloadData()
            } else { // 신상
                clvT3.isHidden = false
                clvT3.reloadData()
            }
        }
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                //self.getDataList()
                self.changeLikeStatus(status: true)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
//                self.getDataList()
                self.changeLikeStatus(status: false)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, _pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, _uid: _pdtUid)

        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, _uid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            controller.pdtUid = _uid
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // 푸시토큰 업데이트
    func updatePushToken() {

        gProgress.show()
        Net.updatePushToken(
            accessToken     : gMeInfo.token,
            deviceToken     : gPushToken,
            success: { (result) -> Void in
                gProgress.hide()

                if !gPushUid.isEmpty && !gPushType.isEmpty {
                    self.goDetailPage()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()

            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            }  else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension MainViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            gMainPageIndex = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            self.pageMoved()
            
        case clvT1, clvT2, clvT3:
            
            self.scrollDirection = EScrollDirection.none
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvT1, clvT2, clvT3:
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
                print("<<<<<<<<<<%d", scrollView.contentOffset.y)
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
                print(">>>>>>>>>>%d", scrollView.contentOffset.y)
            }
            
            scrollPos = curScrollPos
            
        default:
            return
        }
    }
    
}

extension MainViewController : UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC
        
        self.selectedImageView = cell._imgCom
        
        let dic : FeedPdtDto = arrPdtList[indexPath.row]
        if btnRight.isSelected {
            let dic : FeedPdtDto = arrPdtList[indexPath.row]
            let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
            getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, _pdtUid: dicPdtStyle.pdtUid)

//            MainPopup.show(self, image: [cell._imgCom.image!, #imageLiteral(resourceName: "bg_burberry.png"), #imageLiteral(resourceName: "bg_login_suggest_4.png")]) {
//            }
        } else {
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            controller.pdtUid = dic.pdt.pdtUid
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}

extension MainViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return arrPdtList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC
        
        let dic : FeedPdtDto = arrPdtList[indexPath.row]
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
        let blikeStatus     : Bool               = dic.pdtLikeStatus  //유저 상품좋아요 상태
        let dicUsrFeed      : UsrFeedDao         = dic.usrFeed  //피드데이터
        let dicPeerUsr      : UsrMiniDto         = dic.peerUsr  //피드데이터
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if btnRight.isSelected {
            cell._btnStyle.isHidden = true
            cell._imgEffect.isHidden = true
            cell._imgCom.kf.setImage(with: URL(string: dicPdtStyle.styleImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
                height = width * ratio
            }
        } else {
            cell._btnStyle.isHidden = false
            cell._imgEffect.isHidden = false
            cell._imgCom.kf.setImage(with: URL(string: dicPdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            
            if dicPdtStyle.styleImg == nil ||  dicPdtStyle.styleImg == "" {
                cell._btnStyle.isHidden = true
                cell._imgEffect.isHidden = true
            }
            
            if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
                height = width * ratio
            }
            
            cell._btnStyle.kf.setImage(with: URL(string: dicPdtStyle.styleImg), for: .normal, placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        }
        
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
        cell._imgComHeight.constant = height

        if dicPdt.originPrice <= dicPdt.price {
            cell.hiddenDown(price: dicPdt.price)
        } else {
            cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
        }
        cell.lbSize.text = dicPdt.pdtSize
        
        cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
        
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
        cell._lbBrand.text = dicBrand.nameEn
        cell._lbLike.text = String(likeCnt)
        cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
//        cell._imgComWidth.constant = width
        
        //user
        cell._lbName.text = dicUsr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        if dicPdt.edited {
            cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime) + " " + NSLocalizedString("modify", comment:"")
        } else {
            cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
        }
        
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
        
        cell._btnUser.removeTarget(self, action:#selector(self.onBtnAlarm(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.removeTarget(self, action:#selector(self.onBtnLoveItem(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.removeTarget(self, action:#selector(self.onBtnFollow(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.removeTarget(self, action:#selector(self.onBtnUserItem(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.removeTarget(self, action:#selector(self.onBtnUserStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.removeTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        
        if collectionView == clvT1 {
            if dicUsrFeed.type == 1 {
                cell._imgProfile.image = UIImage.init(named: "ic_alarm_main")
                cell._lbName.text = "찾았던 상품"
                cell._btnUser.addTarget(self, action:#selector(self.onBtnAlarm(_:)), for: UIControlEvents.touchUpInside)
            } else if dicUsrFeed.type == 2 {
                cell._imgProfile.image = UIImage.init(named: "ic_loveitem_main")
                cell._lbName.text = "LOVE ITEM 가격인하"
                cell._btnUser.addTarget(self, action:#selector(self.onBtnLoveItem(_:)), for: UIControlEvents.touchUpInside)
            } else if dicUsrFeed.type == 3 {
                cell._imgProfile.image = UIImage.init(named: "ic_follow_main")
                cell._lbName.text = "팔로우 브랜드"
                cell._btnUser.addTarget(self, action:#selector(self.onBtnFollow(_:)), for: UIControlEvents.touchUpInside)
            } else if dicUsrFeed.type == 4 {
                cell._lbName.text =  dicPeerUsr.usrNckNm + " 잇템"
                cell._imgProfile.kf.setImage(with: URL(string: dicPeerUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
                cell._btnUser.addTarget(self, action:#selector(self.onBtnUserItem(_:)), for: UIControlEvents.touchUpInside)
            } else if dicUsrFeed.type == 5 {
                cell._imgProfile.kf.setImage(with: URL(string: dicPeerUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
                cell._lbName.text = dicPeerUsr.usrNckNm + " 스타일"
                cell._btnUser.addTarget(self, action:#selector(self.onBtnUserStyle(_:)), for: UIControlEvents.touchUpInside)
            } else if dicUsrFeed.type == 6 {
                cell._imgProfile.kf.setImage(with: URL(string: dicPeerUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
                cell._lbName.text = dicPeerUsr.usrNckNm + " 스타일"
                cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
            }
            cell._lbTime.text = CommonUtil.diffTime(dicUsrFeed.regTime)
        } else {
            cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        }
        cell._btnUser.tag = indexPath.row
        
        self.selectedImageView = cell._imgCom
        
        cell._btnProduct.tag = indexPath.row
        cell._btnProduct.addTarget(self, action:#selector(self.onBtnTitle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnDownMoney.tag = indexPath.row
        cell._btnDownMoney.addTarget(self, action:#selector(self.onBtnTitle(_:)), for: UIControlEvents.touchUpInside)
        
        //더보기
        if indexPath.row == arrPdtList.count - 1 && !isLast {
            nPage = nPage + 1
            getDataList()
        }
        
        return cell
    }
}

extension MainViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
//        let width = (collectionView.bounds.width - 15) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
        let dic : FeedPdtDto = arrPdtList[indexPath.row]
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if btnRight.isSelected {
            if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
                height = width * ratio
            }
        } else {
            if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
                height = width * ratio
            }
        }
        
        if dicPdt.originPrice <= dicPdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 119
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.size.width, height: 119)
//    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StyleGridHeaderView", for: indexPath as IndexPath)
    }
}

extension MainViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return selectedImageView ?? UIImageView()
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        guard let selectedImageView = selectedImageView else { return CGRect.zero }
        return selectedImageView.convert(selectedImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        selectedImageView?.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        selectedImageView?.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        selectedImageView?.isHidden = false
    }
}
