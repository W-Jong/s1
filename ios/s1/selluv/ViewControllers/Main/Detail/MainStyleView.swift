//
//  MainStyleView.swift
//  BoraTalk
//
//  Created by Dev on 11/10/17.
//  Copyright © 2017 Dev. All rights reserved.
//

import UIKit

class MainStyleView: BaseDialog {
    
    @IBOutlet weak var _vwBg: UIView!
    @IBOutlet weak var _btnClose: UIButton!
    @IBOutlet weak var _btnCom: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func initUI() {
    }
    
}
