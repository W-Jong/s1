//
//  MainItemCVC.swift
//  selluv
//
//  Created by Dev on 12/4/17.
//

import UIKit

class MainItemCVC: UICollectionViewCell {

    @IBOutlet weak var _imgProfile: UIImageView!
    @IBOutlet weak var _lbName: UILabel!
    @IBOutlet weak var _lbTime: UILabel!
    @IBOutlet weak var _vwImage: UIView!
    @IBOutlet weak var _imgCom: UIImageView!
//    @IBOutlet weak var _imgComWidth: NSLayoutConstraint!
    @IBOutlet weak var _imgComHeight: NSLayoutConstraint!
    @IBOutlet weak var _vwLike: UIView!
    @IBOutlet weak var _vwLikeBg: UIView!
    @IBOutlet weak var _btnStyle: UIButton!
    @IBOutlet weak var _lbWen: UILabel!
    @IBOutlet weak var _lbMoney: UILabel!
    @IBOutlet weak var _lbComName: UILabel!
    @IBOutlet weak var _lbBrand: UILabel!
    @IBOutlet weak var _imgSoldOut: UIImageView!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var _lbWon: UILabel!
    @IBOutlet weak var _lbWonNumber: UILabel!
    @IBOutlet weak var _lbPercent: UILabel!
    @IBOutlet weak var _imgDown: UIImageView!
    @IBOutlet weak var _btnLike: UIButton!
    @IBOutlet weak var _btnUser: UIButton!
    @IBOutlet weak var _btnBrand: UIButton!
    @IBOutlet weak var _btnProduct: UIButton!
    @IBOutlet weak var _btnDownMoney: UIButton!
    
    @IBOutlet weak var _imgHeart: UIImageView!
    @IBOutlet weak var _lbLike: UILabel!
    
    @IBOutlet weak var _htDownMoney: NSLayoutConstraint!
    @IBOutlet weak var _imgEffect: UIImageView!
    
    var nLike : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }
    
    func initUI() {
        _vwImage.layer.cornerRadius = 10
        _vwLike.layer.cornerRadius = 8
        _vwLikeBg.layer.cornerRadius = 8
        
//        showDown()
    }
    
    func hiddenDown(price : Int) {
        _lbWon.isHidden = true
        _lbWonNumber.isHidden = true
        _lbPercent.isHidden = true
        _imgDown.isHidden = true
        
        _htDownMoney.constant = 9
        _lbMoney.textColor = UIColor.black
        _lbMoney.attributedText = NSAttributedString.init(string: "₩ " + CommonUtil.formatNum(price))
    }
    
    func showDown(origin : Int, price : Int) {
        _lbWon.isHidden = false
        _lbWonNumber.isHidden = false
        _lbPercent.isHidden = false
        _imgDown.isHidden = false
        
        _htDownMoney.constant = 20
        _lbMoney.textColor = UIColor.gray
        
        let attr : NSAttributedString = NSAttributedString.init(string: "₩ " + CommonUtil.formatNum(origin), attributes: [NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: Int8(NSUnderlineStyle.styleSingle.rawValue))]);
        _lbMoney.attributedText = attr
        
        _lbWonNumber.text = "₩ " + CommonUtil.formatNum(price)
        
        //percent
        let diff = origin - price
        let percent = Int((Float (diff) / Float (origin)) * 100)
        _lbPercent.text = CommonUtil.formatNum(percent) + "%"
    }
    
    @IBAction func likeAction(_ sender: Any) {
//        if _btnLike.isSelected {
//            _btnLike.isSelected = false
//
//            if _imgHeart.image == UIImage.init(named: "ic_like_hart") {
//                _imgHeart.image = UIImage.init(named: "ic_dislike_hart")
//                var nLike = Int(_lbLike.text!)!
////                if nLike > 0 {
////                    nLike += 1
////                }
//
//                _lbLike.text = String(nLike)
//                _btnLike.tag = 0
//
//            } else {
//                _imgHeart.image = UIImage.init(named: "ic_like_hart")
//                var nLike = Int(_lbLike.text!)!
////                if nLike > 0 {
////                    nLike -= 1
////                }
//
//                _lbLike.text = String(nLike)
//                _btnLike.tag = 1
//            }
//
//
//        } else {
//            _btnLike.isSelected = true
//
//            if _imgHeart.image == UIImage.init(named: "ic_like_hart") {
//                _imgHeart.image = UIImage.init(named: "ic_dislike_hart")
//                var nLike = Int(_lbLike.text!)!
////                if nLike > 0 {
////                    nLike += 1
////                }
//
//                _lbLike.text = String(nLike)
//                _btnLike.tag = 0
//            } else {
//                _imgHeart.image = UIImage.init(named: "ic_like_hart")
//                var nLike = Int(_lbLike.text!)!
////                if nLike > 0 {
////                    nLike -= 1
////                }
//
//                _lbLike.text = String(nLike)
//                _btnLike.tag = 1
//            }
//        }
    }
    
}

