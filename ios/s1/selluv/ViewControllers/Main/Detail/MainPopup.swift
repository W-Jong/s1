//
//  MainPopup.swift
//  selluv
//
//  Created by Dev on 12/22/17.
//

import UIKit

class MainPopup: ARNModalImageTransitionViewController, ARNImageTransitionZoomable {
    
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    
    @IBOutlet weak var vwContent: UIView!
    
    @IBOutlet weak var vevBg: UIVisualEffectView!
    
    @IBOutlet weak var scvBanner: UIScrollView!
    
    @IBOutlet weak var stvIndicator: UIStackView!
    
    @IBOutlet weak var stvIndicatorWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imageView: UIImageView!
    
    public typealias callback = () -> ()    
    var cbOk : callback! = nil
    
    var imgList = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scvBanner.contentSize = CGSize(width: (scvBanner.frame.size.width) * CGFloat(imgList.count), height: scvBanner.frame.size.height)
        
        for nInd in 0 ..< imgList.count {
            let ivBanner = ScalableImageView(frame: CGRect(x: scvBanner.frame.size.width * CGFloat(nInd), y: 0, width: scvBanner.frame.size.width, height: scvBanner.frame.size.height))
            
//            ivBanner.frame = CGRect(x: 0, y: 0, width: imgList[nInd].size.width, height: imgList[nInd].size.height)
//            let frame = AVMakeRect(aspectRatio: ivBanner.frame.size, insideRect: CGRect(origin: CGPoint(x: CGFloat(nInd) * (scvBanner.frame.size.width), y: 0), size: CGSize(width: scvBanner.frame.size.width, height: scvBanner.frame.size.height)))
//            ivBanner.frame = frame
            
//            ivBanner.isUserInteractionEnabled = true
//            ivBanner.clipsToBounds = true
//            ivBanner.contentMode = UIViewContentMode.scaleAspectFit
            ivBanner.tag = nInd
            ivBanner.setFileURL(imgList[nInd])
//            ivBanner.image = imgList[nInd]
            scvBanner.addSubview(ivBanner)
            
            let image = UIImage(named: nInd == 0 ? "ic_shop_banner_indicator_active" : "ic_shop_banner_indicator_inactive")
            let ivPageNum = UIImageView(frame: CGRect(origin: CGPoint(x: CGFloat(image!.size.width + 5.0) * CGFloat(nInd), y: 0), size: CGSize(width: image!.size.width, height: image!.size.height)))
            ivPageNum.tag = nInd
            ivPageNum.image = image
            
            stvIndicator.addSubview(ivPageNum)
            stvIndicatorWidth.constant = CGFloat(image!.size.width) * CGFloat(nInd + 1) + 5.0 * CGFloat(nInd)
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    convenience init(photo: [String], okCallback: callback! = nil) {
        self.init()
        
        imgList = photo
        cbOk = okCallback
    }
    
    func initVC() {
        
        vwContent.layer.cornerRadius = 6
        scvBanner.layer.cornerRadius = 6
        
//        scvBanner.maximumZoomScale = 5.0
//        scvBanner.minimumZoomScale = 1.0
        
    }
    
    static func show(_ vc: UIViewController, image: [String], okCallback: callback! = nil) {
        let vcSuggest = MainPopup(photo: image, okCallback: okCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func createTransitionImageView() -> UIImageView {
        let imageView = UIImageView(image: self.imageView.image)
        imageView.contentMode = self.imageView.contentMode
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        imageView.frame = self.imageView!.frame
        return imageView
    }
    
    func presentationBeforeAction() {
        self.imageView.isHidden = true
    }
    
    func presentationCompletionAction(_ completeTransition: Bool) {
        self.imageView.isHidden = false
    }
    
    func dismissalBeforeAction() {
        self.imageView.isHidden = true
    }
    
    func dismissalCompletionAction(_ completeTransition: Bool) {
        if !completeTransition {
            self.imageView.isHidden = false
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func bgAction(_ sender: Any) {
        closeAction(sender)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk()
        }
        
    }
    
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension MainPopup : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let newOffset = scrollView.contentOffset.x
        let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % imgList.count
        
        for subview in stvIndicator.subviews {
            let ivPageNum = subview as! UIImageView
            ivPageNum.image = ivPageNum.tag == pageNum ? #imageLiteral(resourceName: "ic_shop_banner_indicator_active") : #imageLiteral(resourceName: "ic_shop_banner_indicator_inactive")
        }
    }
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        let newOffset = scrollView.contentOffset.x
//        let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % imgList.count
//
//        for subview in scrollView.subviews {
//            let image = subview as! UIImageView
//            if image.tag == pageNum {
//                return image
//            }
//        }
//
//        return nil
//    }
    
}
