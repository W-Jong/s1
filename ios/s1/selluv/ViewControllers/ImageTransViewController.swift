//
//  ImageTransViewController.swift
//  selluv
//
//  Created by World on 1/6/18.
//

import UIKit

class ImageTransViewController: BaseViewController {
    
    weak var interactiveAnimator : ARNTransitionAnimator?
    var currentOperation : UINavigationControllerOperation = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
}

extension ImageTransViewController : UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.currentOperation = operation

        if let _interactiveAnimator = self.interactiveAnimator {
            return _interactiveAnimator
        }

        if operation == .push {
            return ARNImageZoomTransition.createAnimator(.push, fromVC: fromVC, toVC: toVC)
        } else if operation == .pop {
            return ARNImageZoomTransition.createAnimator(.pop, fromVC: fromVC, toVC: toVC)
        }
        
        return nil
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if let _interactiveAnimator = self.interactiveAnimator {
            if  self.currentOperation == .pop {
                return _interactiveAnimator
            }
        }
        return nil
    }
    
}
