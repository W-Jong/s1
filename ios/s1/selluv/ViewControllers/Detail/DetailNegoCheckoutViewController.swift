//
//  DetailNegoCheckoutViewController.swift
//  selluv
//  체크아웃
//  Created by Comet on 1/4/18.
//  modified by PJH on 16/03/18.

import UIKit

class DetailNegoCheckoutViewController: BaseViewController {

    @IBOutlet weak var _vwAddrList: UIView!
    @IBOutlet weak var _btnAddr1: UIButton!
    @IBOutlet weak var _btnAddr2: UIButton!
    @IBOutlet weak var _btnAddr3: UIButton!
    
    var _curListNum : Int = 0
    
    @IBOutlet weak var _tfName: DesignableUITextField!
    @IBOutlet weak var _tfContact: DesignableUITextField!
    @IBOutlet weak var _tfAddress: DesignableUITextField!
    @IBOutlet weak var _lbAddrSearch: UILabel!
    @IBOutlet weak var _btnAddrSearch: UIButton!
    
    @IBOutlet weak var lbCardNum: UILabel!
    @IBOutlet weak var lbCardname: UILabel!
    
    //결제 정보관련
    @IBOutlet weak var _lbDesc1: UILabel!
    @IBOutlet weak var _lbDesc2: UILabel!
    @IBOutlet weak var _vwCardOk: UIView!
    @IBOutlet weak var _vwCardNone: UIView!
    @IBOutlet weak var _btnCardChange: UIButton!
    @IBOutlet weak var _cstrCardOkHeight: NSLayoutConstraint!
    @IBOutlet weak var _cstrCardNoneHeight: NSLayoutConstraint!
    var cardRegistered : Bool = false
    
    // 배송정보 관련
    var addrInfo =
        [(name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : "")]

    var reqPrice : Int!
    var addPrice : Int = 0
    var nCardId  = 0
    var dicPdtInfo  : PdtDetailDto!
    var dicDealDao  : DealDao!
    override func viewDidLoad() {
        super.viewDidLoad()

        initData()
        initUI()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidLoad()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        _tfContact.resignFirstResponder()
        _tfName.resignFirstResponder()
        _tfAddress.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    

    func initUI() {
        _vwAddrList.layer.cornerRadius = 2
        _vwAddrList.layer.borderWidth = 1
        _vwAddrList.layer.borderColor = UIColor(hex:0xc7c7c7).cgColor
        _btnAddr1.addTarget(self, action: #selector(self.onBtnAddrList(_ :) ), for: .touchUpInside)
        _btnAddr2.addTarget(self, action: #selector(self.onBtnAddrList(_ :) ), for: .touchUpInside)
        _btnAddr3.addTarget(self, action: #selector(self.onBtnAddrList(_ :) ), for: .touchUpInside)
        _tfName.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _tfContact.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _tfAddress.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _btnAddrSearch.layer.cornerRadius = 2.0
        _btnCardChange.layer.cornerRadius = 2.0
        _tfAddress.isEnabled = false
        
        _vwCardOk.layer.cornerRadius = 4.0
        _vwCardNone.layer.cornerRadius = 4.0
//        self._lbAddrSearch.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.onAddrSearch(_ :) )))
        self._btnAddrSearch.addTarget(self, action: #selector(self.onAddrSearch(_ :) ), for: .touchUpInside)
        
        _tfName.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfContact.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfAddress.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
    }
    
    func initData() {
        
        _curListNum = 0
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            addrInfo[i].name = dic.addressName
            addrInfo[i].contact = dic.addressPhone
            addrInfo[i].addr = dic.addressDetail
            addrInfo[i].code = dic.addressDetailSub
        }
        
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            if dic.status == 2 {
                _curListNum = i
            }
        }
        selectTabBtn(index: _curListNum)
    }
    
    func selectTabBtn(index : Int) {
        
        _btnAddr1.backgroundColor = UIColor(hex: 0xffffff)
        _btnAddr1.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        _btnAddr2.backgroundColor = UIColor(hex: 0xffffff)
        _btnAddr2.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        _btnAddr3.backgroundColor = UIColor(hex: 0xffffff)
        _btnAddr3.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        switch index {
        case 0:
            _curListNum = 0
            _btnAddr1.backgroundColor = UIColor(hex: 0x42c2fe)
            _btnAddr1.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
            break
        case 1:
            _curListNum = 1
            _btnAddr2.backgroundColor = UIColor(hex: 0x42c2fe)
            _btnAddr2.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
            break
        case 2:
            _curListNum = 2
            _btnAddr3.backgroundColor = UIColor(hex: 0x42c2fe)
            _btnAddr3.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
            break
        default:
            break
        }
        redrawShipInfoSection()
    }
    
    func redrawShipInfoSection() {
        _tfName.text = addrInfo[_curListNum].name
        _tfContact.text = addrInfo[_curListNum].contact
        if addrInfo[_curListNum].addr != "" && addrInfo[_curListNum].code != ""{
            _lbAddrSearch.text = String.init(format: "%@\n[%@]", addrInfo[_curListNum].addr, addrInfo[_curListNum].code)
            _tfAddress.isHidden = true
            _lbAddrSearch.isHidden = false
        } else {
            _lbAddrSearch.text = ""
            _tfAddress.isHidden = false
            _lbAddrSearch.isHidden = true
        }
    }
    
    func isvalidValue() -> Bool {
        
        if addrInfo[_curListNum].name == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_name", comment:""))
            return false
        }
        
        if addrInfo[_curListNum].contact == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_contact", comment:""))
            return false
        }
        
        if addrInfo[_curListNum].addr == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_address", comment:""))
            return false
        }

        if nCardId == 0 {
            MsgUtil.showUIAlert(NSLocalizedString("empty_card_num", comment:""))
            return false
        }
        
        return true
    }
    
    //도서직역 체크
    func isGitaArea(areanm : String) -> Bool {
        
        for i in 0..<GitaAreas.count {
            if areanm.lowercased().range(of:GitaAreas[i]) != nil {
                return true
            }
        }
        return false
    }
    
    //
    // EventHandlers
    //
    @IBAction func onBtnBack(_ sender: Any) {       
        popVC()
    }
    
    @IBAction func onBtnConfirm(_ sender: Any) {
        if isvalidValue() {
            
            //제주도 지역라면
            if addrInfo[_curListNum].addr.lowercased().range(of:"제주도") != nil {
               addPrice =  3000
            } else if isGitaArea(areanm: addrInfo[_curListNum].addr) { //도서지역이라면
                addPrice = 4000
            }
            reqPrice = reqPrice + addPrice
            reqDealNego()
        }
    }
    
    @IBAction func hideKeyboardAction(_ sender: Any) {
        if (_tfName != nil) {
            _tfName?.resignFirstResponder()
        }
        
        if (_tfContact != nil) {
            _tfContact?.resignFirstResponder()
        }
        
        if (_tfAddress != nil) {
            _tfAddress?.resignFirstResponder()
        }
    }
    
    @objc func onAddrSearch(_ sender: Any) {
        AddrSearch.show(self) { (addr, code) in
            
            self._lbAddrSearch.text = String.init(format: "%@\n[%@]", addr, code)
            if self._lbAddrSearch.text == "" {
                self._tfAddress.isHidden = false
                self._lbAddrSearch.isHidden = true
            } else {
                self._tfAddress.isHidden = true
                self._lbAddrSearch.isHidden = false
            }
            
            self.addrInfo[self._curListNum].addr = addr
            self.addrInfo[self._curListNum].code = code
        }
    }
    
    @objc func onBtnAddrList(_ sender: UIButton) {
       
        switch sender {
        case _btnAddr1:
            selectTabBtn(index: 0)
            break
        case _btnAddr2:
            selectTabBtn(index: 1)
            break
        case _btnAddr3:
            selectTabBtn(index: 2)
            break
        default:
            break
        }
    }
    
    @objc func textChanged(_ sender : UITextField) {
        if(sender == _tfName) {
            addrInfo[_curListNum].name = _tfName.text!
        } else if (sender == _tfContact) {
            addrInfo[_curListNum].contact = _tfContact.text!
        }
    }
    
    @IBAction func onCardChange(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_CARDVIEW_VIEW")) as! CardViewViewController
        vc.delegate = self
        vc.pageStatus = "NEGO"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onCardClick(_ sender: Any) {
        
        cardRegistered = true
//        pushVC("MYPAGE_CARD_VIEW", storyboard: "Mypage", animated: true)
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_CARDVIEW_VIEW")) as! CardViewViewController
        vc.delegate = self
        vc.pageStatus = "NEGO"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 네고 신청
    func reqDealNego() {
        
        gProgress.show()
        Net.reqDealNego(
            accessToken         : gMeInfo.token,
            islandSendPrice     : addPrice,
            pdtUid              : dicPdtInfo.pdt.pdtUid,
            recipientAddress    : String.init(format: "%@\n%@", addrInfo[_curListNum].addr, addrInfo[_curListNum].code),
            recipientNm         : addrInfo[_curListNum].name,
            recipientPhone      : addrInfo[_curListNum].contact,
            reqPrice            : reqPrice,
            usrCardUid          : self.nCardId,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.reqDealNegoResult
                self.reqDealNegoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //네고 신청 결과
    func reqDealNegoResult(_ data: Net.reqDealNegoResult) {
        
        dicDealDao = data.dealDao
        
        //네고 제안 완료페지로
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "CHECKOUT_VIEW")) as! CheckOutViewController
        vc.dicDealInfo = dicDealDao
        vc.reqPrice = reqPrice
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension DetailNegoCheckoutViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == _tfName) {
            _tfName.resignFirstResponder()
            _tfContact.becomeFirstResponder()
            _tfAddress.becomeFirstResponder()
        }
        
        return true
    }
}

extension DetailNegoCheckoutViewController: CardViewViewControllerDelegate {
    func onConfirm(datainfo : UsrCardDto) {
        self.nCardId = datainfo.usrCardUid
        self.lbCardNum.text = CommonUtil.convertNum(data: datainfo.cardNumber)
        self.lbCardname.text = datainfo.cardName
        if(cardRegistered == true) {
            _lbDesc1.text = "신용/체크카드 간편결제"
            _lbDesc2.text = "(네고 제안이 승인되면 상품이 결제될 카드입니다.)"
            _cstrCardOkHeight.constant = 175
            _cstrCardNoneHeight.constant = 0
            _btnCardChange.isHidden = false
        } else {
            _lbDesc1.text = "결제 카드 등록"
            _lbDesc2.text = "(네고 제안이 승인될 경우 결제될 카드를 등록해주세요.)"
            _cstrCardOkHeight.constant = 0
            _cstrCardNoneHeight.constant = 175
            _btnCardChange.isHidden = true
        }
    }
}

