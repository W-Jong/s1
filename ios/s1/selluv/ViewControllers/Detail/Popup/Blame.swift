//
//  Blame.swift
//  selluv
//
//  Created by Gambler on 12/30/17.
//

import UIKit

class Blame: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var lbReason: UILabel!
    @IBOutlet var lbPlaceHolder: UILabel!
    @IBOutlet var tvReason: UITextView!
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    
    @IBOutlet var vwReason: UIView!
    var reason : String = ""
    
    public typealias callback = (_ reason : String, _ detail : String) -> ()
    var cbOk : callback! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, okCallback: callback! = nil) {
        let vcSuggest = Blame(okCallback: okCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwDlg.layer.cornerRadius = 6
        
        vwReason.backgroundColor = UIColor.white
        vwReason.layer.borderWidth = 1
        vwReason.layer.borderColor = UIColor(hex : 0xe1e1e1).cgColor
        
        tvReason.backgroundColor = UIColor.white
        tvReason.layer.borderWidth = 1
        tvReason.layer.borderColor = UIColor(hex : 0xe1e1e1).cgColor
    }
    
    func hideKeyboard() {
        tvReason.resignFirstResponder()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = keyboardSize.height
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func reasonAction(_ sender: Any) {
        
        let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        acAlert.addAction(UIAlertAction(title: "광고", style: .default, handler: { (action: UIAlertAction!) in
            self.lbReason.textColor = UIColor.init(hex: 333333)
            self.lbReason.text = "광고"
            self.reason = "AD_SPAM"
        }))
        acAlert.addAction(UIAlertAction(title: "스팸", style: .default, handler: { (action: UIAlertAction!) in
            self.lbReason.textColor = UIColor.init(hex: 333333)
            self.lbReason.text = "스팸"
            self.reason = "AD_SPAM"
        }))
        acAlert.addAction(UIAlertAction(title: "휴먼 판매자", style: .default, handler: { (action: UIAlertAction!) in
            self.lbReason.textColor = UIColor.init(hex: 333333)
            self.lbReason.text = "휴먼 판매자"
            self.reason = "SLEEP_SELLER"
        }))
        acAlert.addAction(UIAlertAction(title: "직거래 유도", style: .default, handler: { (action: UIAlertAction!) in
            self.lbReason.textColor = UIColor.init(hex: 333333)
            self.lbReason.text = "직거래 유도"
            self.reason = "DIRECT_DEAL"
        }))
        acAlert.addAction(UIAlertAction(title: "기타", style: .default, handler: { (action: UIAlertAction!) in
            self.lbReason.textColor = UIColor.init(hex: 333333)
            self.lbReason.text = "기타"
            self.reason = "OTHER"
        }))
        acAlert.addAction(UIAlertAction(title: "취소", style: .cancel, handler: { (action: UIAlertAction!) in
            self.lbReason.textColor = UIColor.init(hex: 333333)
            self.lbReason.text = "취소"
            self.reason = "CANCEL"
        }))
        present(acAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func bgAction(_ sender: Any) {
        
        tvReason.resignFirstResponder()
        
    }
    
    @IBAction func blameAction(_ sender: Any) {

        tvReason.resignFirstResponder()
        if reason == "" {
            self.view.makeToast(NSLocalizedString("empty_report_type", comment:""))
            return
        }

        let msg = tvReason.text
        if msg == "" {
            self.view.makeToast(NSLocalizedString("empty_report_msg", comment:""))
            return
        }
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(reason, tvReason.text)
        }
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
}

extension Blame : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let strNew = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let len = strNew.characters.count
        
        if textView == tvReason && len > 200 { //. 200자 제한
            return false
        }
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        lbPlaceHolder.isHidden = textView.text != ""
    }
    
}
