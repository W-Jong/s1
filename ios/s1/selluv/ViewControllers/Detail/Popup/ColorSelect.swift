//
//  ColorSelect.swift
//  selluv
//
//  Created by PJH on 19/03/18.
//

import UIKit

class ColorSelect: BaseViewController {

    @IBOutlet var _clvcolor: UICollectionView!
    var cbCallback : callback! = nil
    public typealias callback = (_ colorname: String) -> ()
    
    let colorLabelArray : [ColorType]  = [ColorType.black, ColorType.grey, ColorType.white, ColorType.beizy, ColorType.red, ColorType.pink, ColorType.purple, ColorType.blue, ColorType.green, ColorType.yellow, ColorType.orange, ColorType.brown, ColorType.gold, ColorType.silver, ColorType.multi]

    var nCurSel     : Int = -1
    var colorName   : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(_ addrCallBack: callback! = nil, colorname : String) {
        self.init()
        cbCallback = addrCallBack
        colorName = colorname
    }
    
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "ColorCell", bundle: nil)
        _clvcolor.register(viewNib, forCellWithReuseIdentifier: "cell")
    }
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, colorname : String, _ callback: callback!) {
        let vcSuggest = ColorSelect(callback, colorname : colorname)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        registerNibs()
        
        if colorName == "" {
            nCurSel = -1
        } else {
            for i in 0..<colorLabelArray.count {
                if colorName == colorLabelArray[i].rawValue {
                    nCurSel = i;
                }
            }
        }
        
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
        if cbCallback != nil {
            if nCurSel == -1 {
                MsgUtil.showUIAlert(NSLocalizedString("empty_color_name", comment:""))
                return
            }
            cbCallback(colorLabelArray[nCurSel].rawValue)
        }
    }
}

extension ColorSelect:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width)/4.0, height: (collectionView.frame.size.width)/4.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorLabelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Create the cell and return the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ColorCell
        cell._lbColor.text = colorLabelArray[indexPath.row].rawValue
        cell.delegate = self
        cell.tag = indexPath.row
//        if((m_selectedColorType.index(of: colorLabelArray[indexPath.row])) != nil) {
//            cell._imvSelectOn.isHidden = false
//        }
        
        if nCurSel == indexPath.row {
            cell._imvSelectOn.isHidden = false
        } else {
            cell._imvSelectOn.isHidden = true
        }
        
        switch(colorLabelArray[indexPath.row]) {
        case ColorType.black :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x000000).cgColor
            cell._imvColor.image = nil
            
        case ColorType.grey :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3c3c3c).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4a4a4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.white :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xdddddd).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffffff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.beizy :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc4bab0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xf5e9dc).cgColor
            cell._imvColor.image = nil
            
        case ColorType.red :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc2900).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff3300).cgColor
            cell._imvColor.image = nil
            
        case ColorType.pink :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc868ba).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xfa82e8).cgColor
            cell._imvColor.image = nil
            
        case ColorType.purple :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x7e27cf).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x9b30ff).cgColor
            cell._imvColor.image = nil
            
        case ColorType.blue :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3e75b3).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4e92e0).cgColor
            cell._imvColor.image = nil
            
        case ColorType.green :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x33b13b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x40dd4a).cgColor
            cell._imvColor.image = nil
            
        case ColorType.yellow :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xccac00).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffd700).cgColor
            cell._imvColor.image = nil
            
        case ColorType.orange :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc4a0b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff5d03).cgColor
            cell._imvColor.image = nil
            
        case ColorType.brown :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x6e4626).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x8a572f).cgColor
            cell._imvColor.image = nil
            
        case ColorType.gold :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_gold")
            
        case ColorType.silver :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_silver")
            
        case ColorType.multi :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_multi")
        }
        // Add image to cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension ColorSelect: ColorCellDelegate {
    func ColorCellDelegate(_ colorCell : ColorCell, onItemClicked sender : Any, tag: Int!) {
        
        if nCurSel == tag {
            nCurSel = -1
        } else {
            nCurSel = tag
        }
        _clvcolor.reloadData()
    }
}
