//
//  NegoPopup.swift
//  selluv
//  네고제안 팝업
//  Created by Gambler on 12/28/17.
//  modified by PJH on 16/03/18.

import UIKit

class NegoPopup: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    @IBOutlet weak var lbMoney: UILabel!
    @IBOutlet weak var lbSlider: UISlider!
    @IBOutlet weak var lbMinMoney: UILabel!
    @IBOutlet weak var lbMaxMoney: UILabel!
    @IBOutlet weak var vwImage: UIView!
    @IBOutlet weak var _tfMoney: UITextField!
    @IBOutlet weak var _htBody: NSLayoutConstraint!
    
    @IBOutlet weak var _htBottom: NSLayoutConstraint!
    @IBOutlet weak var _vwKeyheader: UIView!
    
    @IBOutlet weak var ivPdt: UIImageView!
    @IBOutlet weak var lbBrandEn: UILabel!
    @IBOutlet weak var lbPdtName: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    var dicPdtInfo  : PdtDetailDto!
    var cbOk: callback! = nil
    var minPrice : Int!
    var maxPrice : Int!
    public typealias callback = (_ price : Int) -> ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        setPdtData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(dicInfo: PdtDetailDto, okCallback: callback! = nil) {
        self.init()
        
        self.dicPdtInfo = dicInfo
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, dicPdtInfo: PdtDetailDto, okCallback: callback! = nil) {
        let sizePopup = NegoPopup(dicInfo: dicPdtInfo, okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        sizePopup.dicPdtInfo = dicPdtInfo
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwDlg.layer.cornerRadius = 6
        vwImage.layer.cornerRadius = 6
        
    }
    
    func setPdtData(){
        
        //pdt info
        var likegroup = ""
        
        if dicPdtInfo.pdt.pdtGroup & 1 != 0 {
            likegroup = NSLocalizedString("male", comment:"")
        }
        
        if dicPdtInfo.pdt.pdtGroup & 2 != 0 {
            likegroup = NSLocalizedString("female", comment:"")
        }
        
        if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
            likegroup = NSLocalizedString("kids", comment:"")
        }
        
        var cateName = ""
        if dicPdtInfo.categoryList.count > 0 {
            cateName = dicPdtInfo.categoryList[dicPdtInfo.categoryList.count - 1].categoryName
        }
        
        lbPdtName.text = dicPdtInfo.brand.nameKo + " " + likegroup + " " + dicPdtInfo.pdt.colorName + " " + dicPdtInfo.pdt.pdtModel + " " + cateName
        lbBrandEn.text = dicPdtInfo.brand.nameEn
        lbSize.text = dicPdtInfo.pdt.pdtSize
        lbPrice.text = "￦ " + CommonUtil.formatNum(dicPdtInfo.pdt.price)
        ivPdt.kf.setImage(with: URL(string: dicPdtInfo.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

        lbMinMoney.text = CommonUtil.formatNum(dicPdtInfo.pdt.price / 2)
        lbMaxMoney.text = CommonUtil.formatNum(dicPdtInfo.pdt.price)
        
        minPrice = dicPdtInfo.pdt.price / 2
        maxPrice = dicPdtInfo.pdt.price
        lbSlider.minimumValue = Float(minPrice)
        lbSlider.maximumValue = Float(maxPrice)
        
        _tfMoney.text = CommonUtil.formatNum(dicPdtInfo.pdt.price / 2)

    }
    
    @objc func keyboardChange(_ aNotification: Notification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                
                self._htBody.constant = -200
                self._vwKeyheader.isHidden = false
                self._htBottom.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardHide(_ aNotification: Notification) {
        self._htBottom.constant = 0
        self._vwKeyheader.isHidden = true
        self._htBody.constant = 0
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeKeyboard(_ sender: Any) {
        _tfMoney.resignFirstResponder()
    }
    
    @IBAction func hideAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func slideAction(_ sender: Any) {
        _tfMoney.text = CommonUtil.formatNum(Int(lbSlider.value))
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        _tfMoney.resignFirstResponder()
    }
    
    @IBAction func proposalAction(_ sender: Any) {
        _tfMoney.resignFirstResponder()
        
        if _tfMoney.text == "" {
            self.view.makeToast(NSLocalizedString("empty_proposal_money", comment:""))
            return
        }
        let money = _tfMoney.text?.replaceAll(",", with: "")
        let proposalPrice : Int  = Int(money!)!
        if proposalPrice < minPrice  || proposalPrice > maxPrice {
            self.view.makeToast(NSLocalizedString("reg_money_wrong", comment:""))
            return
        }
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(proposalPrice)
        }
        
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension NegoPopup : UITextFieldDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let strNew = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let len = strNew.count
        
        if textView == _tfMoney {
            _tfMoney.text = CommonUtil.formatNum(Int(strNew)!)
            return false
        }
        
        return true
    }
    
}
