//
//  ModelSelect.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class ModelSelect: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var picker2: UIPickerView!
    @IBOutlet weak var vwBg: UIView!
    
    var cbOk: callback! = nil
    public typealias callback = (_ size2: String) -> ()
    
    var arrNames : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animateAndChain(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }) { (finish) in
            self.vwBg.alpha = 0.8
            
        }
        
    }
    
    convenience init(okCallback: callback! = nil, names : [String]) {
        self.init()
        
        cbOk = okCallback
        arrNames = names
    }
    
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController,names : [String], okCallback: callback! = nil) {
        let sizePopup = ModelSelect(okCallback: okCallback, names : names)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .coverVertical
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        btnOk.layer.cornerRadius = 5
    }
    
    func effectVC() {
        UIView.animateAndChain(withDuration: 0.1, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.0
        }, completion: nil).animateAndChain(withDuration: 0.1, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0
        }) { (finish) in
            self.vwBg.alpha = 0
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func okAction(_ sender: Any) {
        
        effectVC()
        
        if cbOk != nil {
            cbOk(String(arrNames[picker2.selectedRow(inComponent: 0)]))
        }
        
    }
    
    @IBAction func closeAction(_ sender : Any) {
    
        effectVC()
    
    }
}

extension ModelSelect : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(arrNames[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 48
    }
    
    
}
