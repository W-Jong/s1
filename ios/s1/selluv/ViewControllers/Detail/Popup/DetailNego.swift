//
//  DetailNego.swift
//  selluv
//
//  Created by Gambler on 1/4/18.
//

import UIKit

class DetailNego: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var ivImg: UIImageView!
    @IBOutlet var lbEName: UILabel!
    @IBOutlet var lbKName: UILabel!
    @IBOutlet var lbSize: UILabel!
    @IBOutlet var lbCost: UILabel!
    
    @IBOutlet var lbNego: UILabel!
    @IBOutlet var lbMin: UILabel!
    @IBOutlet var lbMax: UILabel!
    @IBOutlet var sldVal: UISlider!
    
    public typealias callback = (_ value : Float) -> ()
    var cbOk : callback! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, okCallback: callback! = nil) {
        let vcSuggest = DetailNego(okCallback: okCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        
        vwDlg.setRoundRect(radius: 6)
        
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////

    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(sldVal.value)
        }
        
    }
    
    @IBAction func slideAction(_ sender: Any) {
        
        lbNego.text = "￦ " + CommonUtil.formatNum(Int(sldVal.value))
        
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////
