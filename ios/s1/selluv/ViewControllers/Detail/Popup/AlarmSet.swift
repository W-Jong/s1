//
//  AlarmSet.swift
//  selluv
//
//  Created by Gambler on 1/4/18.
//  modified by PJH on 19/03/18.

import UIKit

class AlarmSet: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    var filterCondition : FilterCondition? = FilterCondition()
    
    @IBOutlet var lbBrand: UILabel!
    @IBOutlet var lbProduct: UILabel!
    @IBOutlet var lbCategory: UILabel!
    
    @IBOutlet var lbModel: UILabel!
    @IBOutlet var lbSize: UILabel!
    @IBOutlet var lbColor: UILabel!
    
    @IBOutlet weak var m_cvColorList: UICollectionView!
    
    public typealias callback = (_ model : String, _ size : String, _ color : String) -> ()
    var cbOk : callback! = nil
    
    var dicPdtInfo      : PdtDetailDto!
    var dicSizeInfo     : SizeRefDto!
    var arrModelList    : Array<String> = []
    
    var colorname       : String = ""
    var pdtModel        : String = ""
    var pdtSize         : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    convenience init(okCallback: callback! = nil, dicInfo : PdtDetailDto) {
        self.init()
        
        cbOk = okCallback
        self.dicPdtInfo = dicInfo
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, dicInfo : PdtDetailDto, okCallback: callback! = nil) {
        let vcSuggest = AlarmSet(okCallback: okCallback, dicInfo: dicInfo)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        
        lbBrand.text = dicPdtInfo.brand.nameKo
        var cateName = ""
        if dicPdtInfo.categoryList.count > 0 {
            cateName = dicPdtInfo.categoryList[dicPdtInfo.categoryList.count - 1].categoryName
        }

        lbCategory.text = cateName
        
        var likegroup = ""
        if dicPdtInfo.pdt.pdtGroup & 1 != 0 {
            likegroup = NSLocalizedString("male", comment:"")
        }
        
        if dicPdtInfo.pdt.pdtGroup & 2 != 0 {
            likegroup = NSLocalizedString("female", comment:"")
        }
        
        if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
            likegroup = NSLocalizedString("kids", comment:"")
        }
        lbProduct.text = likegroup
        
        self.m_cvColorList.dataSource  = self
        self.m_cvColorList.delegate = self
        self.m_cvColorList.layer.transform = CATransform3DConcat(self.m_cvColorList.layer.transform, CATransform3DMakeRotation(CGFloat.pi, 0.0, 1.0, 0.0))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onLocalNotificationReceived(_:)), name: nil, object: nil)
        //Register nibs
        registerNibs()
        
        getModelNmsList()
    }
    
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "ColorCell_small", bundle: nil)
        m_cvColorList.register(viewNib, forCellWithReuseIdentifier: "cell")
    }
    
    @objc func onLocalNotificationReceived(_ notification : Notification) {
        
        switch notification.name {
        case Notification.Name(NOTI_COLOR_CHANGE as String) :
            let msg = notification.object as? [ColorType]
            filterCondition?.colorType = msg!
            m_cvColorList.reloadData()
            lbColor.isHidden = true
            break
        default:
            break
        }
    }
    
    func isvalidValue() -> Bool {
        
        if pdtModel == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_model_name", comment:""))
            return false
        }

        if pdtSize == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_size_name", comment:""))
            return false
        }

        if colorname == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_color_name", comment:""))
            return false
        }

        return true
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
         popVC()
    }
    
    @IBAction func modelAction(_ sender: Any) {
        ModelSelect.show(self, names: arrModelList) { (size2) in
            self.pdtModel = size2
            self.lbModel.text = self.pdtModel
        }
    }
    
    @IBAction func sizeAction(_ sender: Any) {
        
        if dicSizeInfo.sizeType == 1 || dicSizeInfo.sizeType == 2 {  // 가로x세로x폭, FREE SIZE && 직접입력
            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_INFO_VIEW")) as! SellProductInfoViewController
            vc.sizeType = dicSizeInfo.sizeType
            vc.pdtSize = pdtSize
            vc.PageStatus = "SIZE"
            vc.isEdit = true
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        } else { //일반
            SizeSelect.show(self, dicSizeInfo: dicSizeInfo) { (size) in
                self.lbSize.text = size
                self.pdtSize = size
            }
        }
    }
    
    @IBAction func colorAction(_ sender: Any) {
        ColorSelect.show(self, colorname: colorname) { (size) in
            self.lbColor.text = size
            self.colorname = size
        }
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        updatePdtAlarm()
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 사이즈 목록 얻기
    func getSizeList() {
        
        gProgress.show()
        Net.getSizeList(
            accessToken     : gMeInfo.token,
            categoryUid     : dicPdtInfo.pdt.categoryUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSizeListResult
                self.getSizeListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //사이즈 목록 얻기 결과
    func getSizeListResult(_ data: Net.getSizeListResult) {
        dicSizeInfo = data.dicInfo
    }
    
    // 찾는 상품 알림설정
    func updatePdtAlarm() {
        
        var pdtGroupType = ""
        if dicPdtInfo.pdt.pdtGroup & 1 != 0 {
            pdtGroupType = "MALE"
        }
        
        if dicPdtInfo.pdt.pdtGroup & 2 != 0 {
            pdtGroupType = "FEMALE"
        }
        
        if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
            pdtGroupType = "KIDS"
        }
        
        gProgress.show()
        Net.updatePdtAlarm(
            accessToken     : gMeInfo.token,
            brandUid        : dicPdtInfo.brand.brandUid,
            categoryUid     : dicPdtInfo.pdt.categoryUid,
            colorName       : colorname,
            pdtGroupType    : pdtGroupType,
            pdtModel        : pdtModel,
            pdtSize         : pdtSize,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("alarm_setting_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 브랜드에 따르는 추천모델명 리스트 얻기
    func getModelNmsList() {

        gProgress.show()
        Net.getBrandModelNmsList(
            accessToken     : gMeInfo.token,
            brandUid        : dicPdtInfo.brand.brandUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getBrandModelNmsListResult
                self.getBrandModelNmsListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //브랜드에 따르는 추천모델명 리스트 얻기 결과
    func getBrandModelNmsListResult(_ data: Net.getBrandModelNmsListResult) {
        arrModelList = data.list
        getSizeList()
    }
}

extension AlarmSet:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 20, height: 20)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (filterCondition?.colorType.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Create the cell and return the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ColorCell_small
        cell.tag = indexPath.row
        switch(filterCondition?.colorType[indexPath.row])! {
        case ColorType.black :
            cell._imvColor.layer.borderColor = UIColor.init(red:0x00/255.0 , green: 0x00/255.0, blue: 0x00/255.0, alpha: 1.0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor.init(red:0x00/255.0 , green: 0x00/255.0, blue: 0x00/255.0, alpha: 1.0).cgColor
            break
        case ColorType.grey :
            cell._imvColor.layer.borderColor = UIColor.init(red:0x3c/255.0 , green: 0x3c/255.0, blue: 0x3c/255.0, alpha: 1.0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor.init(red:0x4a/255.0 , green: 0x4a/255.0, blue: 0x4a/255.0, alpha: 1.0).cgColor
            break
        case ColorType.white :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xdddddd).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffffff).cgColor
            break
        case ColorType.beizy :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc4bab0).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xf5e9dc).cgColor
            break
        case ColorType.red :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc2900).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff3300).cgColor
            break
        case ColorType.pink :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xc868ba).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xfa82e8).cgColor
            break
        case ColorType.purple :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x7e27cf).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x9b30ff).cgColor
            break
        case ColorType.blue :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x3e75b3).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x4e92e0).cgColor
            break
        case ColorType.green :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x33b13b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x40dd4a).cgColor
            break
        case ColorType.yellow :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xccac00).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xffd700).cgColor
            break
        case ColorType.orange :
            cell._imvColor.layer.borderColor = UIColor(hex: 0xcc4a0b).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0xff5d03).cgColor
            break
        case ColorType.brown :
            cell._imvColor.layer.borderColor = UIColor(hex: 0x6e4626).cgColor
            cell._imvColor.layer.backgroundColor = UIColor(hex: 0x8a572f).cgColor
            break
        case ColorType.gold :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_gold")
            break
        case ColorType.silver :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_silver")
            break
        case ColorType.multi :
            cell._imvColor.layer.borderColor = UIColor.clear.cgColor
            cell._imvColor.image = UIImage(named: "ic_color_multi")
            break
        default:
            break
        }
        // Add image to cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension AlarmSet: SellProductInfoViewControllerDelegate {
    func setSize( size : String){
        if size == "" {
            self.lbSize.text = "사이즈 선택"
            self.pdtSize = size
        } else {
            self.lbSize.text = size
            self.pdtSize = size
        }
    }
    
    func setTag( tag : String){
        
    }
    func setCondition( condition : String){
        
    }
}

