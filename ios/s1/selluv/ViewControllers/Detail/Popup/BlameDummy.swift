//
//  BlameDummy.swift
//  selluv
//
//  Created by Gambler on 12/30/17.
//

import UIKit

class BlameDummy: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var lbContent: UILabel!
    @IBOutlet var tvReason: UITextView!
    @IBOutlet var lbReason: UILabel!
    
    public typealias callback = (_ reason : String) -> ()
    var cbOk : callback! = nil
    
    var count = 0
    var money = 0

    static var pdtUid : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, _uid : Int, okCallback: callback! = nil) {
        pdtUid = _uid
        let vcSuggest = BlameDummy(okCallback: okCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
        
        vcSuggest.getPdtTakeInfo()
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwDlg.layer.cornerRadius = 6
        
        tvReason.backgroundColor = UIColor.white
        tvReason.layer.borderWidth = 1
        tvReason.layer.borderColor = UIColor(hex : 0xe1e1e1).cgColor
        
    }
    
    func hideKeyboard() {
        tvReason.resignFirstResponder()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.lcContentBottom.constant = keyboardSize.height
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.lcContentBottom.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func bgTouched(_ sender: Any) {
        
        tvReason.resignFirstResponder()
        
    }
    
    @IBAction func blameAction(_ sender: Any) {
       reqPdtTakeInfo()
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 가품신고 정보 얻기
    func getPdtTakeInfo() {
        
        gProgress.show()
        Net.getPdtFakeInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : BlameDummy.pdtUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getPdtFakeInfoResult
                self.getPdtTakeInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //가품신고 정보 얻기 결과
    func getPdtTakeInfoResult(_ data: Net.getPdtFakeInfoResult) {
        
        count = data.totalCount
        money = data.reward
        
        let 블루 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x42C2FE)]
        let 블루볼드 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x42C2FE)]
        let 블랙 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.black]
        let 그레이 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x666666)]
        
        let str = String(format: "지금까지 %d번의 신고가 접수되었습니다.", count)
        let attr = NSMutableAttributedString(string: str, attributes: 블랙)
        attr.setAttributes(블루볼드, range: NSMakeRange(5, String(count).count))
        lbTitle.attributedText = attr
        
        let str1 = String(format: "가장 먼저 정확한 가품 사유를 기재해주신 분께\n상품이 가품으로 판정될 경우,\n셀럽 머니 %@원을 드립니다.", CommonUtil.formatNum(money))
        let attr1 = NSMutableAttributedString(string: str1, attributes: 그레이)
        attr1.setAttributes(블루, range: NSMakeRange(25, CommonUtil.formatNum(money).count + 1))
        lbContent.attributedText = attr1
        
    }
    
    // 가품신고 하기
    func reqPdtTakeInfo() {
        tvReason.resignFirstResponder()
        let msg = tvReason.text
        if msg == "" {
            self.view.makeToast(NSLocalizedString("empty_non_product_msg", comment:""))
            return
        }
        
        gProgress.show()
        Net.reqPdtFakeInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : BlameDummy.pdtUid,
            content         : msg!,
            success: { (result) -> Void in
                gProgress.hide()
              
            CommonUtil.showToast(NSLocalizedString("report_submist_success", comment:""))
            self.dismiss(animated: true, completion: nil)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension BlameDummy : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let strNew = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let len = strNew.count
        
        if textView == tvReason && len > 200 { //. 200자 제한
            return false
        }
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        lbReason.isHidden = textView.text != ""
    }
    
}
