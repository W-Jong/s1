//
//  AddrSearch.swift
//  selluv
//
//  Created by Comet on 1/6/18.
//

import UIKit

class AddrSearch: BaseViewController, UIWebViewDelegate {

    var cbCallback : callback! = nil
    public typealias callback = (_ addr: String, _ code : String) -> ()
    @IBOutlet weak var _webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(_ addrCallBack: callback! = nil) {
        self.init()
        cbCallback = addrCallBack
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, _ callback: callback!) {
        let vcSuggest = AddrSearch(callback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        _webview.delegate = self
        
        let weburl = Server_Url + F_API.WEB_ADDRESS.rawValue
        _webview.loadRequest(URLRequest(url: URL(string: weburl)!))
    }
    
    func shoAlert(msg : String) {
        let encodeStr : String = msg.removingPercentEncoding!
        let alert = UIAlertController(title: "", message: encodeStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("confirm", comment:""), style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        
    }
    
    @nonobjc func webViewDidFinishLoad(webView: UIWebView!)
    {
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
   
        if (request.url?.absoluteString.hasPrefix("selluv://showToast"))!{
            let suburl = request.url?.absoluteString
            let fullNameArr = suburl?.components(separatedBy: "=")
            self.shoAlert(msg: fullNameArr![1])
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://closeWebview"))!{
            dismiss(animated: true, completion: nil)
            return false
        } else if (request.url?.absoluteString.hasPrefix("selluv://findAddress"))!{
            //address, postcode
            dismiss(animated: true, completion: nil)
            
            let findAddress = request.url?.absoluteString.components(separatedBy: "?")
            let subAddress = findAddress![1].components(separatedBy: "&")
            let address = subAddress[0].components(separatedBy: "=")[1].removingPercentEncoding
            let postcode = subAddress[1].components(separatedBy: "=")[1]
            
            if cbCallback != nil {
                cbCallback(address!, postcode)
            }
            return false
        } else if (request.url?.absoluteString.hasPrefix("http://postcode.map.daum.net/guide"))!{
            
            return false
        }

        return true
       
    }
    
}
