//
//  StyleRegister.swift
//  selluv
//
//  Created by Gambler on 12/30/17.
//

import UIKit

class StyleRegister: BaseViewController {

    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    public typealias callback = () -> ()
    var cbPhoto : callback! = nil
    var cbAlbum : callback! = nil
    
    @IBOutlet var vwDlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(photoCallback: callback! = nil, albumCallback: callback! = nil) {
        self.init()
        
        cbPhoto = photoCallback
        cbAlbum = albumCallback
    }
    

    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, photoCallback: callback! = nil, albumCallback: callback! = nil) {
        let vcSuggest = StyleRegister(photoCallback: photoCallback, albumCallback: albumCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        
        vwDlg.layer.cornerRadius = 6
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func photoAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbPhoto != nil {
            cbPhoto()
        }
        
    }
    
    @IBAction func albumAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if cbAlbum != nil {
            cbAlbum()
        }
        
    }
    
}
