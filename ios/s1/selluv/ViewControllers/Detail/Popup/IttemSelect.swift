//
//  IttemSelect.swift
//  selluv
//
//  Created by Gambler on 12/30/17.
//

import UIKit

class IttemSelect: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnItem: [UIButton]!
    
    public typealias callback = (_ type: Int) -> ()
    var cbCallback : callback! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for btn in btnItem {
            btn.layer.setAffineTransform(CGAffineTransform(scaleX: 0.1, y: 0.1))
            btn.alpha = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showBtns()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(photoCallback: callback! = nil) {
        self.init()
        
        cbCallback = photoCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, itemCallback: callback! = nil) {
        let vcSuggest = IttemSelect(photoCallback: itemCallback)
        
        vcSuggest.modalPresentationStyle = .overCurrentContext
        vcSuggest.modalTransitionStyle = .crossDissolve
        
        vc.present(vcSuggest, animated: true, completion: nil)
    }
    
    func initVC() {
        
    }
    
    func showBtns() {
        UIView.animateAndChain(withDuration: 0.4, delay: 0.1, options: .curveEaseOut, animations: {
            self.btnItem[0].alpha = 1
            self.btnItem[0].layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: 1))
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.btnItem[1].alpha = 1
            self.btnItem[1].layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: 1))
        }, completion: nil).animateAndChain(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
            self.btnItem[2].alpha = 1
            self.btnItem[2].layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: 1))
        }, completion: nil).animateAndChain(withDuration: 0.15, delay: 0.0, options: .curveEaseOut, animations: {
            self.btnItem[3].alpha = 1
            self.btnItem[3].layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: 1))
        }, completion: nil).animateAndChain(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btnItem[4].alpha = 1
            self.btnItem[4].layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: 1))
        }, completion: nil).animateAndChain(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btnItem[5].alpha = 1
            self.btnItem[5].layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: 1))
        }, completion: nil)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func itemAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
        if cbCallback != nil {
            cbCallback(sender.tag)
        }
    }
    
    @IBAction func bgAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
