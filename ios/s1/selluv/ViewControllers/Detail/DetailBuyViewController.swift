//
//  DetailBuyViewController.swift
//  selluv
//  상품구매
//  Created by Comet on 1/7/18.
//  modified by PJH on 17/03/18.

import UIKit

class DetailBuyViewController: BaseViewController {
    
    @IBOutlet weak var _scvList: UIScrollView!
    // 상품 정보 관련 변수들
    @IBOutlet weak var lcContentBottom: NSLayoutConstraint!
    @IBOutlet weak var _imvItem: UIImageView!
    @IBOutlet weak var _lbEngBrand: UILabel!
    @IBOutlet weak var _lbItemTitle: UILabel!
    @IBOutlet weak var _lbSize: UILabel!
    @IBOutlet weak var _lbPrice: UILabel!
    @IBOutlet weak var _btnAppraisalNone: UIButton!
    @IBOutlet weak var _btnAppraisalActive: UIButton!

    var _curListNum : Int = 0
    // 배송정보 관련
    var addrInfo =
        [(name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : "")]
    
    @IBOutlet weak var _vwAddrList: UIView!
    @IBOutlet weak var _btnAddr1: UIButton!
    @IBOutlet weak var _btnAddr2: UIButton!
    @IBOutlet weak var _btnAddr3: UIButton!
    
    @IBOutlet weak var _tfName: DesignableUITextField!
    @IBOutlet weak var _tfContact: DesignableUITextField!
    @IBOutlet weak var _tfAddress: DesignableUITextField!
    @IBOutlet weak var _lbAddrSearch: UILabel!
    @IBOutlet weak var _btnAddrSearch: UIButton!
    
    // 할인 적용 관련
    @IBOutlet weak var _lbOwnedSelluvMoney: UILabel!
    @IBOutlet weak var _tfPromotionCode: DesignableUITextField!
    @IBOutlet weak var _tfSelluvMoney: DesignableUITextField!
    @IBOutlet weak var _btnPromotionApply: UIButton!
    @IBOutlet weak var _btnSelluvMoneyUse: UIButton!
    
    // 금액 상세 관련
    @IBOutlet weak var _lbPaymentMoney: UILabel!
    @IBOutlet weak var _imvArrowPayment: UIImageView!
    @IBOutlet weak var _cstrPaymentAmountDetail: NSLayoutConstraint! // 227
    @IBOutlet weak var _cstrPaymentAmountDetail1: NSLayoutConstraint! //212
    @IBOutlet weak var _cstrPaymentAmountDetail3: NSLayoutConstraint! //154
    @IBOutlet weak var vwJejuArea: UIView!
    @IBOutlet weak var vwGitaArea: UIView!
    @IBOutlet weak var vwPromotion: UIView!
    @IBOutlet weak var vwSelluvMoney: UIView!
    
    @IBOutlet weak var _lbCostItem: UILabel!
    @IBOutlet weak var _lbCostShip: UILabel!
    @IBOutlet weak var _lbCostAppraisal: UILabel!
    @IBOutlet weak var _lbCostPromotionDiscount: UILabel!
    @IBOutlet weak var _lbCostSelluvMoneyUse: UILabel!
    @IBOutlet weak var _lbPaymentMoneySum: UILabel!
    
    @IBOutlet weak var _lbSelluvMoney: UILabel!
    @IBOutlet weak var _imvArrowSelluvMoney: UIImageView!
    @IBOutlet weak var _cstrSelluvMoneyDetail: NSLayoutConstraint!
    @IBOutlet weak var _lbBuyMoneyPercent: UILabel!
    @IBOutlet weak var _lbBuyMoney: UILabel!
    @IBOutlet weak var _lbLatterMoney: UILabel!
    @IBOutlet weak var _lbSelluvMoneySum: UILabel!
    
    //결제 정보 관련
    @IBOutlet weak var _imvRadioCard: UIImageView!
    @IBOutlet weak var _vwCardOk: UIView!
    @IBOutlet weak var _vwCardNone: UIView!
    @IBOutlet weak var _btnCardChange: UIButton!
    @IBOutlet weak var _cstrCardOkHeight: NSLayoutConstraint!
    @IBOutlet weak var _cstrCardNoneHeight: NSLayoutConstraint!
    @IBOutlet weak var _imvRadioNeighbour: UIImageView!
    @IBOutlet weak var _imvRadioCredit: UIImageView!
    @IBOutlet weak var _imvRadioTransfer: UIImageView!
    var cardRegistered : Bool = false
    
    @IBOutlet weak var lbCardNum: UILabel!
    @IBOutlet weak var lbCardName: UILabel!
    
//    var paymentType     : Int = 0 // 0: 신용/체크카드, 1: 네이버 페이, 2: 신용/체크카드 간편결제, 3: 실시간 계죄이체
//    var appraisalType   : Int = 0 // 0:온라인 정품감정서비스 , 1: 본사정품감정 서비스
//    var payType         : String!
    var m_activeTextField : UITextField?
    var dicPdtInfo      : PdtDetailDto!
    var dicPromotionInfo: PromotionCodeDao!
    var nCardId         = -1        //결제카드 uid
    var addPrice        : Int = 0 //도서직역 추가 배송비
//    var promotionCode   : String!  //프로모션 코드
//    var promotionSale   : Int = 0  //프로모션 양
//    var selluvMoneyUse  : Int = 0  //사용할 셀럽머니
//    var totalmoney      : Int = 0  //총 결제금액
    var dicPdtBuyInfo   : PdtBuyInfo!
    var vwPdtInfoHeight : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        dicPdtBuyInfo   = PdtBuyInfo()
        initData()
        initUI()
        setPdtData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func initUI() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // 상품 정보 관련
        _imvItem.layer.cornerRadius = 3
        
        //배송 정보 관련
        _vwAddrList.layer.cornerRadius = 2
        _vwAddrList.layer.borderWidth = 1
        _vwAddrList.layer.borderColor = UIColor(hex:0xc7c7c7).cgColor
        _btnAddr1.addTarget(self, action: #selector(self.onBtnAddrList(_ :) ), for: .touchUpInside)
        _btnAddr2.addTarget(self, action: #selector(self.onBtnAddrList(_ :) ), for: .touchUpInside)
        _btnAddr3.addTarget(self, action: #selector(self.onBtnAddrList(_ :) ), for: .touchUpInside)
        _tfName.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _tfContact.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _tfAddress.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _btnAddrSearch.layer.cornerRadius = 2.0
        
        //할인적용 관련
        _tfPromotionCode.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _tfSelluvMoney.setBorder(color: UIColor(hex:0xffffff), width: 2.0)
        _btnPromotionApply.layer.cornerRadius = 2.0
        _btnSelluvMoneyUse.layer.cornerRadius = 2.0
        
        //결제정보 관련
        _btnCardChange.layer.cornerRadius = 2.0
        
        _vwCardOk.layer.cornerRadius = 4.0
        _vwCardNone.layer.cornerRadius = 4.0
//        self._lbAddrSearch.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.onAddrSearch(_ :) )))
        self._btnAddrSearch.addTarget(self, action: #selector(self.onAddrSearch(_ :) ), for: .touchUpInside)
        
        _tfName.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfContact.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfAddress.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        if(cardRegistered == true) {
            _cstrCardOkHeight.constant = 175
            _cstrCardNoneHeight.constant = 0
            _btnCardChange.isHidden = false
        } else {
            _cstrCardOkHeight.constant = 0
            _cstrCardNoneHeight.constant = 175
            _btnCardChange.isHidden = true
        }
        
        redrawShipInfoSection()
        
        _tfPromotionCode.delegate = self
        _tfSelluvMoney.delegate = self
        _tfContact.delegate = self
        _tfName.delegate = self
        _tfAddress.delegate = self
        _tfAddress.isEnabled = false
    }
    
    func initData() {
        _curListNum = 0
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            addrInfo[i].name = dic.addressName
            addrInfo[i].contact = dic.addressPhone
            addrInfo[i].addr = dic.addressDetail
            addrInfo[i].code = dic.addressDetailSub
        }
        
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            if dic.status == 2 {
                _curListNum = i
            }
        }
        selectTabBtn(index: _curListNum)
    }
    
    func selectTabBtn(index : Int) {
        
        _btnAddr1.backgroundColor = UIColor(hex: 0xffffff)
        _btnAddr1.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        _btnAddr2.backgroundColor = UIColor(hex: 0xffffff)
        _btnAddr2.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        _btnAddr3.backgroundColor = UIColor(hex: 0xffffff)
        _btnAddr3.setTitleColor(UIColor(hex: 0x999999), for: .normal)
        switch index {
        case 0:
            _curListNum = 0
            _btnAddr1.backgroundColor = UIColor(hex: 0x42c2fe)
            _btnAddr1.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
            break
        case 1:
            _curListNum = 1
            _btnAddr2.backgroundColor = UIColor(hex: 0x42c2fe)
            _btnAddr2.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
            break
        case 2:
            _curListNum = 2
            _btnAddr3.backgroundColor = UIColor(hex: 0x42c2fe)
            _btnAddr3.setTitleColor(UIColor(hex: 0xffffff), for: .normal)
            break
        default:
            break
        }
        redrawShipInfoSection()
    }
    
    func setPdtData(){
        
        //pdt info
        var likegroup = ""
        
        if dicPdtInfo.pdt.pdtGroup & 1 != 0 {
            likegroup = NSLocalizedString("male", comment:"")
        }
        
        if dicPdtInfo.pdt.pdtGroup & 2 != 0 {
            likegroup = NSLocalizedString("female", comment:"")
        }
        
        if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
            likegroup = NSLocalizedString("kids", comment:"")
        }
        
        var cateName = ""
        if dicPdtInfo.categoryList.count > 0 {
            cateName = dicPdtInfo.categoryList[dicPdtInfo.categoryList.count - 1].categoryName
        }
        
        _lbItemTitle.text = dicPdtInfo.brand.nameKo + " " + likegroup + " " + dicPdtInfo.pdt.colorName + " " + dicPdtInfo.pdt.pdtModel + " " + cateName
        _lbEngBrand.text = dicPdtInfo.brand.nameEn
        _lbSize.text = dicPdtInfo.pdt.pdtSize
        _lbPrice.text = "￦ " + CommonUtil.formatNum(dicPdtInfo.pdt.price)
        _imvItem.kf.setImage(with: URL(string: dicPdtInfo.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        _lbOwnedSelluvMoney.text = CommonUtil.formatNum(gMeInfo.money)
        
        setMoneyDetail()
    }
    
     //금액 성새 관련
    func setMoneyDetail() {
        var totalmoney : Int = 0
        addPrice = 0
        _lbCostItem.text = CommonUtil.formatNum(dicPdtInfo.pdt.price)
        
        if dicPdtBuyInfo.getAppraisalType() == 0 {
            _lbCostAppraisal.text = NSLocalizedString("free", comment:"")
            totalmoney = dicPdtInfo.pdt.price
        } else {
            _lbCostAppraisal.text = CommonUtil.formatNum(39000)
            totalmoney = dicPdtInfo.pdt.price + 39000
        }
        
        if dicPdtInfo.seller.freeSendPrice == -1 {
            _lbCostShip.text = CommonUtil.formatNum(dicPdtInfo.pdt.sendPrice)
            totalmoney = totalmoney + dicPdtInfo.pdt.sendPrice
        } else {
            _lbCostShip.text =  NSLocalizedString("free", comment:"")
        }
        
        //이제부터 쓰지 않는 뷰들을 22만큼 씩 없앤다.
        vwPdtInfoHeight = 227
        _cstrPaymentAmountDetail.constant = 227
        _cstrPaymentAmountDetail1.constant = 212
        _cstrPaymentAmountDetail3.constant = 154
        
        //도서직역에 있다면
        if addrInfo[_curListNum].addr != "" && isGitaArea(areanm: addrInfo[_curListNum].addr) {
            vwGitaArea.isHidden = false
            dicPdtBuyInfo.setTotalmoney(value: dicPdtBuyInfo.getTotalmoney() + 4000)
            addPrice = 4000
            totalmoney = totalmoney + addPrice
            dicPdtBuyInfo.setIslandSendPrice(value: 4000)
        } else {
            vwGitaArea.isHidden = true
            _cstrPaymentAmountDetail.constant = 205
            vwPdtInfoHeight = 205
            _cstrPaymentAmountDetail1.constant = 190
            _cstrPaymentAmountDetail3.constant = 132
            dicPdtBuyInfo.setIslandSendPrice(value: 0)
        }
        
        //제주도라면 있다면
        if addrInfo[_curListNum].addr != "" && addrInfo[_curListNum].addr.lowercased().range(of:"제주도") != nil {
            vwJejuArea.isHidden = false
            addPrice = 3000
            totalmoney = totalmoney + addPrice
            dicPdtBuyInfo.setJejuAreaSendPrice(value: 3000)
        } else {
            vwJejuArea.isHidden = true
            _cstrPaymentAmountDetail.constant = 183
            vwPdtInfoHeight = 183
            _cstrPaymentAmountDetail1.constant = 168
            _cstrPaymentAmountDetail3.constant = 110
            dicPdtBuyInfo.setJejuAreaSendPrice(value: 0)
        }
        
        if dicPdtBuyInfo.getPromotionSale() == 0 {
            _cstrPaymentAmountDetail.constant = 161
            _cstrPaymentAmountDetail1.constant = 146
            _cstrPaymentAmountDetail3.constant = 88
            vwPromotion.isHidden = true
        } else {
            _lbCostPromotionDiscount.text = "- " + CommonUtil.formatNum(dicPdtBuyInfo.getPromotionSale())
            vwPromotion.isHidden = false
            dicPdtBuyInfo.setTotalmoney(value: dicPdtBuyInfo.getTotalmoney() - dicPdtBuyInfo.getPromotionSale())
            totalmoney = totalmoney - dicPdtBuyInfo.getPromotionSale()
        }
        
        if dicPdtBuyInfo.getSelluvMoneyUse() == 0 {
            _cstrPaymentAmountDetail.constant = 139
            vwPdtInfoHeight = 139
            _cstrPaymentAmountDetail1.constant = 124
            _cstrPaymentAmountDetail3.constant = 66
            vwSelluvMoney.isHidden = true
        } else {
            _lbCostSelluvMoneyUse.text = "- " + CommonUtil.formatNum(dicPdtBuyInfo.getSelluvMoneyUse())
            vwSelluvMoney.isHidden = false
            totalmoney = totalmoney - dicPdtBuyInfo.getSelluvMoneyUse()
        }
        
        _lbPaymentMoney.text = CommonUtil.formatNum(totalmoney) + " 원"
        _lbPaymentMoneySum.text = CommonUtil.formatNum(totalmoney)
        dicPdtBuyInfo.setTotalmoney(value: totalmoney)
        
        getSystemSettingInfo(money: totalmoney)
    }
    
    //도서직역 체크
    func isGitaArea(areanm : String) -> Bool {
        
        for i in 0..<GitaAreas.count {
            if areanm.lowercased().range(of:GitaAreas[i]) != nil {
                return true
            }
        }
        return false
    }
    
    // 배송 정보 관련
    func redrawShipInfoSection() {
        _tfName.text = addrInfo[_curListNum].name
        _tfContact.text = addrInfo[_curListNum].contact
        if addrInfo[_curListNum].addr != "" && addrInfo[_curListNum].code != ""{
            _lbAddrSearch.text = String.init(format: "%@\n[%@]", addrInfo[_curListNum].addr, addrInfo[_curListNum].code)
            _tfAddress.isHidden = true
            _lbAddrSearch.isHidden = false
        } else {
            _lbAddrSearch.text = ""
            _tfAddress.isHidden = false
            _lbAddrSearch.isHidden = true
        }
    }
    
    //
    // EventHandlers
    //
    
    //general events
    @IBAction func onBtnBack(_ sender: Any) {
        dicPdtBuyInfo.initial()
        popVC()
    }
    
    @IBAction func onBtnBg(_ sender: Any) {
        _tfName.resignFirstResponder()
        _tfContact.resignFirstResponder()
        _tfAddress.resignFirstResponder()
        _tfSelluvMoney.resignFirstResponder()
        _tfPromotionCode.resignFirstResponder()
    }
    
    @IBAction func onBtnConfirm(_ sender: Any) {
        if isvalidValue() {
            reqpdtBuy()
        }
    }
    
    // 상품정보 관련 사건들
    @IBAction func onServiceGuide(_ sender: Any) {
        BestQualityPopup.show(self)
    }
    
    @IBAction func onAppraisalNone(_ sender: Any) {
        dicPdtBuyInfo.setAppraisalType(value: 0)
        _btnAppraisalNone.setImage(UIImage(named:"ic_radio_on"), for: .normal)
        _btnAppraisalActive.setImage(UIImage(named:"ic_radio_off"), for: .normal)
        setMoneyDetail()
    }
    
    @IBAction func onAppraisalActive(_ sender: Any) {
        dicPdtBuyInfo.setAppraisalType(value: 1)
        _btnAppraisalNone.setImage(UIImage(named:"ic_radio_off"), for: .normal)
        _btnAppraisalActive.setImage(UIImage(named:"ic_radio_on"), for: .normal)
        setMoneyDetail()
    }
    
    // 배송정보 관련 사건들
    @objc func onAddrSearch(_ sender: Any) {
        AddrSearch.show(self) { (addr, code) in
            
            self._lbAddrSearch.text = String.init(format: "%@\n[%@]", addr, code)
            if self._lbAddrSearch.text == "" {
                self._tfAddress.isHidden = false
                self._lbAddrSearch.isHidden = true
            } else {
                self._tfAddress.isHidden = true
                self._lbAddrSearch.isHidden = false
            }
            
            self.addrInfo[self._curListNum].addr = addr
            self.addrInfo[self._curListNum].code = code
        }
    }
    
    @objc func onBtnAddrList(_ sender: UIButton) {
        switch sender {
        case _btnAddr1:
            selectTabBtn(index: 0)
            break
        case _btnAddr2:
            selectTabBtn(index: 1)
            break
        case _btnAddr3:
            selectTabBtn(index: 2)
            break
        default:
            break
        }
    }
    
    func isvalidValue() -> Bool {
        
        if addrInfo[_curListNum].name == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_name", comment:""))
            return false
        }
        
        if addrInfo[_curListNum].contact == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_contact", comment:""))
            return false
        }
        
        if addrInfo[_curListNum].addr == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_address", comment:""))
            return false
        }
        
        if dicPdtBuyInfo.getPaymentType() == 0 {
            if nCardId == -1 {
                MsgUtil.showUIAlert(NSLocalizedString("empty_card_num1", comment:""))
                return false
            }
        }
        
        return true
    }
    
    //셀럽 머니 사용 여부 체크
    func isSelluvMomeyUse( money : Int) -> Bool {
        
        if money > gMeInfo.money || money > dicPdtInfo.pdt.price + dicPdtInfo.pdt.sendPrice {
            return false
        }
        
        return true
    }
    
    @objc func textChanged(_ sender : UITextField) {
        if(sender == _tfName) {
            addrInfo[_curListNum].name = _tfName.text!
        } else if (sender == _tfContact) {
            addrInfo[_curListNum].contact = _tfContact.text!
        }
    }
    
    @objc func keyboardChange(_ aNotification: Notification) {
        let kbSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let default_offset : CGFloat = 50 + 40 + 20// status bar + Topbarheigth
        var aRect = self.view.frame
        aRect.size.height -= (kbSize?.height)!
        var point : CGPoint
        
        if m_activeTextField == nil {
            return
        }
        
        point = m_activeTextField!.frame.origin
        point.y += m_activeTextField!.frame.height + m_activeTextField!.superview!.frame.origin.y
        point.y += default_offset // plus header size
        
        if m_activeTextField != nil {
            point = m_activeTextField!.frame.origin
            
            if m_activeTextField == _tfName || m_activeTextField == _tfPromotionCode{
                point.y += m_activeTextField!.frame.height + (m_activeTextField!.superview!.superview?.frame.origin.y)! +  m_activeTextField!.frame.height
                point.y += default_offset // plus header size
            } else if m_activeTextField == _tfSelluvMoney || m_activeTextField == _tfContact {
                point.y += m_activeTextField!.frame.height + (m_activeTextField!.superview!.superview?.frame.origin.y)! +  m_activeTextField!.frame.height
                point.y += default_offset // plus header size
                point.y += m_activeTextField!.frame.height
            } else {
                point.y += m_activeTextField!.frame.height + m_activeTextField!.superview!.frame.origin.y +  m_activeTextField!.frame.height
                point.y += default_offset // plus header size
            }
            if !aRect.contains(point) {
                let scrollPoint = CGPoint.init(x: 0.0, y: point.y - aRect.size.height)
                _scvList.setContentOffset(scrollPoint, animated: true)
            }
        }
    }
    
    @objc func keyboardHide(_ aNotification: Notification) {
        _scvList.setContentOffset(CGPoint.init(x: 0.0, y: 0.0), animated: true)
    }
    
    // 프모모션 코드 적용 체크
    @IBAction func onPromotionCodeApply(_ sender: Any) {
        
        onBtnBg(self)
        
        if !_btnPromotionApply.isSelected {
            dicPdtBuyInfo.setPromotionCode(value: _tfPromotionCode.text!)
            if dicPdtBuyInfo.getPromotionCode()  == "" {
                MsgUtil.showUIAlert(NSLocalizedString("empty_promotion_code", comment:""))
                return
            }
            
            getPromotionInfo()
        } else {
            dicPdtBuyInfo.setPromotionCode(value: "")
            dicPdtBuyInfo.setPromotionSale(value: 0)
            _tfPromotionCode.text = ""
            _btnPromotionApply.isSelected = false
            _btnPromotionApply.setTitle("적용", for: .normal)
            _tfPromotionCode.isEnabled = true
            
            setMoneyDetail()
        }
    }
    
    //셀럽 머니 사용 여부 판정
    @IBAction func onSelluvMoneyUse(_ sender: Any) {
        
        onBtnBg(self)
        
        if !_btnSelluvMoneyUse.isSelected {
            
            var value : Int = 0
            if _tfSelluvMoney.text != "" {
                value = Int(_tfSelluvMoney.text!)!
            }
            
            if !isSelluvMomeyUse(money: value) {
                _tfSelluvMoney.text = ""
                MsgUtil.showUIAlert(NSLocalizedString("selluv_money_input_wrong", comment:""))
                return
            }
            self.view.makeToast(NSLocalizedString("selluv_money_input_success", comment:""))
            _btnSelluvMoneyUse.isSelected = true
            _btnSelluvMoneyUse.setTitle("사용해제", for: .normal)
            dicPdtBuyInfo.setSelluvMoneyUse(value: value)
        } else {
            dicPdtBuyInfo.setSelluvMoneyUse(value: 0)
            _tfSelluvMoney.text = ""
            _btnSelluvMoneyUse.isSelected = false
            _btnSelluvMoneyUse.setTitle("사용", for: .normal)
        }
        setMoneyDetail()
    }
    
    @IBAction func onClickSelectCard(_ sender: Any) {
        dicPdtBuyInfo.setPaymentType(value: 0)
        refreshRadios()
    }
    
    // 금액 상세 관련
    @IBAction func onPaymentAmount(_ sender: Any) {
        if(_cstrPaymentAmountDetail.constant == 0) {
            _cstrPaymentAmountDetail.constant = vwPdtInfoHeight
            _imvArrowPayment.image = UIImage(named:"ic_arrow_up")
        } else {
            _cstrPaymentAmountDetail.constant = 0
            _imvArrowPayment.image = UIImage(named:"ic_arrow_down")
        }
    }
    @IBAction func onMaxSelluvMoney(_ sender: Any) {
        if(_cstrSelluvMoneyDetail.constant == 0) {
            _cstrSelluvMoneyDetail.constant = 121
            _imvArrowSelluvMoney.image = UIImage(named:"ic_arrow_up")
        } else {
            _cstrSelluvMoneyDetail.constant = 0
            _imvArrowSelluvMoney.image = UIImage(named:"ic_arrow_down")
        }
    }
    
    // 결제 정보 관련
    @IBAction func onCardChange(_ sender: Any) {
        pushVC("MYPAGE_CARDVIEW_VIEW", storyboard: "Mypage", animated: true)
    }
    @IBAction func onCardClick(_ sender: Any) {
        dicPdtBuyInfo.setPaymentType(value: 0)
        dicPdtBuyInfo.setPayType(value: "SIMPLECARD")
        refreshRadios()
        
        cardRegistered = true
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_CARDVIEW_VIEW")) as! CardViewViewController
        vc.delegate = self
        vc.pageStatus = "NEGO"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onNeighbourPay(_ sender: Any) {
        dicPdtBuyInfo.setPaymentType(value: 1)
        dicPdtBuyInfo.setPayType(value: "")
        refreshRadios()
    }
    @IBAction func onCreditPay(_ sender: Any) {
        dicPdtBuyInfo.setPaymentType(value: 2)
        dicPdtBuyInfo.setPayType(value: "NORMALCARD")
        refreshRadios()
    }
    @IBAction func onTransferPay(_ sender: Any) {
        dicPdtBuyInfo.setPaymentType(value: 3)
        dicPdtBuyInfo.setPayType(value: "REALTIMEPAY")
        refreshRadios()
    }
    
    func refreshRadios() {
        _imvRadioCard.image = UIImage(named: "ic_radio_off")
        _imvRadioNeighbour.image = UIImage(named: "ic_radio_off")
        _imvRadioCredit.image = UIImage(named: "ic_radio_off")
        _imvRadioTransfer.image = UIImage(named: "ic_radio_off")
        switch(dicPdtBuyInfo.getPaymentType()) {
        case 0:
            _imvRadioCard.image = UIImage(named: "ic_radio_on")
            break
        case 1:
            _imvRadioNeighbour.image = UIImage(named: "ic_radio_on")
            break
        case 2:
            _imvRadioCredit.image = UIImage(named: "ic_radio_on")
            break
        case 3:
            _imvRadioTransfer.image = UIImage(named: "ic_radio_on")
            break
        default:
            break
        }
    }
    
    //주문완료페이지로..
    func goPdtBuyFinishPage(_uid : Int) {
        //주문완료페이지로 ..
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "COM_BUY_VIEW")) as! ComBuyViewController
        vc.dicPdtInfo = dicPdtInfo
        vc.dicPdtBuyInfo = dicPdtBuyInfo
        vc.dealUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 프로모션 정보 얻기
    func getPromotionInfo(){
        
        gProgress.show()
        Net.getPromotionInfo(
            accessToken     : gMeInfo.token,
            brandUid        : dicPdtInfo.brand.brandUid,
            categoryUid     : dicPdtInfo.pdt.categoryUid,
            dealType        : "BUYER",
            pdtUid          : dicPdtInfo.pdt.pdtUid,
            promotionCode   : dicPdtBuyInfo.getPromotionCode(),
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getPromotionInfoResult
                self.getPromotionInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                self._tfPromotionCode.text = ""
                CommonUtil .showToast(err)
            }
        })
    }
    
    //프로모션 정보 얻기 결과
    func getPromotionInfoResult(_ data: Net.getPromotionInfoResult) {
        dicPromotionInfo = data.dicInfo
        
        dicPdtBuyInfo.setPromotionCode(value: _tfPromotionCode.text!)
        dicPdtBuyInfo.setPromotionSale(value: 0)

        _btnPromotionApply.isSelected = true
        _tfPromotionCode.isEnabled = false
        _btnPromotionApply.setTitle("적용해제", for: .normal)

        //promotionSale 값 결정
        if data.dicInfo.priceUnit == 1 {  //단위가 퍼센트
            dicPdtBuyInfo.setPromotionSale(value: dicPdtBuyInfo.getTotalmoney() * (data.dicInfo.price / 100))
            _tfPromotionCode.text = String(data.dicInfo.price) + "%"
        } else if data.dicInfo.priceUnit == 2 { //단위가 원
            dicPdtBuyInfo.setPromotionSale(value: data.dicInfo.price)
            _tfPromotionCode.text = CommonUtil.formatNum(data.dicInfo.price)
        }
        
        setMoneyDetail()
    }
    
    // 상품 구매
    func reqpdtBuy(){
        
        gProgress.show()
        Net.reqpdtBuy(
            accessToken         : gMeInfo.token,
            islandSendPrice     : addPrice,
            payPromotion        : dicPdtBuyInfo.getPromotionCode(),
            payType             : dicPdtBuyInfo.getPayType(),
            pdtUid              : dicPdtInfo.pdt.pdtUid,
            pdtVerifyEnabled    : dicPdtBuyInfo.getAppraisalType(),
            recipientAddress    : String.init(format: "%@\n%@", addrInfo[_curListNum].addr, addrInfo[_curListNum].code),
            recipientNm         : addrInfo[_curListNum].name,
            recipientPhone      : addrInfo[_curListNum].contact,
            reqPrice            : dicPdtBuyInfo.getTotalmoney(),
            rewardPrice         : dicPdtBuyInfo.getSelluvMoneyUse(),
            usrCardUid          : dicPdtBuyInfo.getPaymentType() == 0 ? nCardId : 0,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.reqDealBuyResult
                self.reqDealBuyResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //상품 구매 결과
    func reqDealBuyResult(_ data: Net.reqDealBuyResult) {
        
//        CommonUtil.showToast(NSLocalizedString("reg_pdt_buy_success", comment:""))
        
        dicPdtBuyInfo.setName(value: addrInfo[_curListNum].name)
        dicPdtBuyInfo.setContact(value: addrInfo[_curListNum].contact)
        dicPdtBuyInfo.setAddress(value: addrInfo[_curListNum].addr)
        
        if dicPdtBuyInfo.getPaymentType() == 0 {
            
            //주문완료페이지로 ..
            goPdtBuyFinishPage(_uid: data.dealDao.dealUid)
        } else {
            let payUrl = data.paymentUrl
            WebviewViewController.show(self, page: Webview.PAY.rawValue, url: payUrl!) { (name, phone, birth, gender) in
                //name파라미터에 dealuid를 내려보냄
                self.goPdtBuyFinishPage(_uid: Int(name)!)
            }
        }
    }
    
    // 시스템 설정 정보 얻기
    func getSystemSettingInfo( money : Int ){
        
        gProgress.show()
        Net.getSystemSettingInfo(
            accessToken         : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSystemSettingInfoResult
                self.getSystemSettingInfoResult(res, money: money)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //시스템 설정 정보 얻기결과
    func getSystemSettingInfoResult(_ data: Net.getSystemSettingInfoResult, money : Int) {
    
        let dicSysstemInfo : SystemSettingDto = data.dicInfo
        let maxSelluvmoney : Int = (money * dicSysstemInfo.dealReward ) / 100
        let letterMoney : Int = dicSysstemInfo.buyReviewReward

        _lbBuyMoney.text = CommonUtil.formatNum(maxSelluvmoney)
        _lbLatterMoney.text = CommonUtil.formatNum(letterMoney)
        _lbSelluvMoney.text = CommonUtil.formatNum(maxSelluvmoney + letterMoney)
        _lbSelluvMoneySum.text = CommonUtil.formatNum(maxSelluvmoney + letterMoney)
    }
}

extension DetailBuyViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        m_activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        m_activeTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == _tfName) {
            _tfName.resignFirstResponder()
            _tfContact.becomeFirstResponder()
            _tfAddress.becomeFirstResponder()
        }
        
        return true
    }
}

extension DetailBuyViewController: CardViewViewControllerDelegate {
    func onConfirm(datainfo : UsrCardDto) {
        self.nCardId = datainfo.usrCardUid
        self.lbCardNum.text = CommonUtil.convertNum(data: datainfo.cardNumber)
        self.lbCardName.text = datainfo.cardName
        _cstrCardOkHeight.constant = 175
        _cstrCardNoneHeight.constant = 0
        _btnCardChange.isHidden = false
    }
}
