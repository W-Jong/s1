//
//  DetailLikeUserViewController.swift
//  selluv
//  좋아요 유저 목록
//  Created by Comet on 1/8/18.
//  modified by PJH on 15/03/18.

import UIKit

class DetailLikeUserViewController: BaseViewController {

    @IBOutlet weak var _tblUser: UITableView!
    var pdtUid      : Int!
    var nPage       : Int = 0
    var isLast      : Bool!
    var arrUsrList : Array<UsrFollowListDto> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
        getUsrList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tblUser.register(UINib.init(nibName: "FriendJoinedCell", bundle: nil), forCellReuseIdentifier: "FriendJoinedCell")
    }
    //
    // Event Handlers
    //
    @IBAction func onBtnBack(_ sender: Any) {
        popVC()
    }

    @objc func onBtnFollow(_ sender: UIButton) {
        
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            let cell = _tblUser.cellForRow(at: indexPath) as! FriendJoinedCell
            let dic : UsrFollowListDto = arrUsrList[indexPath.row]
            if dic.usrLikeStatus {
                let headerView = UIView()
                headerView.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 100))
                
                let ivProfile = UIImageView()
                ivProfile.frame = CGRect(x: headerView.frame.width / 2 - 30, y: 20, width: 40, height: 40)
                ivProfile.image = cell._imvProfile.image
                ivProfile.layer.cornerRadius = 20
                ivProfile.clipsToBounds = true
                headerView.addSubview(ivProfile)
                
                let lbTitle = UILabel()
                
                let 블랙볼드스타일 = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x000000)]
                let 블랙스타일 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x999999)]
                
                let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  cell._lbNickname.text!, attributes: 블랙볼드스타일))
                contentStr.append(NSAttributedString(string: "님의 팔로우를 취소하시겠어요?", attributes: 블랙스타일))
                
                lbTitle.attributedText = contentStr
                lbTitle.frame = CGRect(x: -10, y: 70, width: headerView.bounds.size.width, height: 20)
                lbTitle.textAlignment = .center
                headerView.addSubview(lbTitle)
                
                let  actionSheet : TOActionSheet = TOActionSheet.init(headerView: headerView)
                actionSheet.style = TOActionSheetStyle.user
                actionSheet.addButton(withTitle: "팔로우 취소", tappedBlock: {
                    self.delFollow(_uid: dic.usrUid)
                })
                
                actionSheet.show(from: self.view, in: self.view)
            } else {
                self.reqFollow(_uid: dic.usrUid)
            }
        }
    }
    
    @objc func onBtnProfile(_ sender: UIButton) {
        let dic : UsrFollowListDto = arrUsrList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }

    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 좋아요 유저 목록 얻기
    func getUsrList() {
        
        gProgress.show()
        Net.getLikeUsrList(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getLikeUsrListResult
                self.getLikeUsrListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 좋아요 유저 목록 얻기 결과
    func getLikeUsrListResult(_ data: Net.getLikeUsrListResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrUsrList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : UsrFollowListDto = data.list[i]
            arrUsrList.append(dic)
        }
        _tblUser.reloadData()
    }
    
    // 유저 팔로우 하기
    func reqFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.nPage = 0
                self.getUsrList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 해제하기
    func delFollow(_uid : Int) {
        
        gProgress.show()
        Net.delFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.nPage = 0
                self.getUsrList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension DetailLikeUserViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendJoinedCell", for: indexPath) as! FriendJoinedCell
        
        let dic : UsrFollowListDto = arrUsrList[indexPath.row]
        
        cell._imvProfile.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        
        cell._lbNickname.text = dic.usrNckNm
        cell._lbID.text = dic.usrId
        
        if dic.usrLikeStatus {
            cell.setFollowingState()
        } else {
            cell.setFollowState()
        }
        
        if gMeInfo.usrUid == dic.usrUid {
            cell._btnFollow.isHidden = true
        } else {
            cell._btnFollow.isHidden = false
        }
        
        cell.btnProfile.tag = indexPath.row
        cell.btnProfile.addTarget(self, action:#selector(self.onBtnProfile(_:)), for: UIControlEvents.touchUpInside)
        cell._btnFollow.withValue = ["index" : indexPath]
        cell._btnFollow.addTarget(self, action:#selector(self.onBtnFollow(_:)), for: UIControlEvents.touchUpInside)

        if indexPath.row == arrUsrList.count - 1 && !isLast {
            nPage = nPage + 1
            getUsrList()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}
