//
//  DetailCommentViewController.swift
//  selluv
//  댓글 작성 및 목록
//  Created by Comet on 1/8/18.
//  modified by PJH on 15/03/18.

import UIKit

import Foundation

class DetailCommentViewController: BaseViewController {

    @IBOutlet weak var _tblComment: UITableView!
    
    @IBOutlet weak var _tfComment: DesignableUITextField!
    @IBOutlet weak var _btnRegister: UIButton!
    @IBOutlet weak var lbNoData: UILabel!
    @IBOutlet weak var _cstrBottomSpace: NSLayoutConstraint!

    var m_tblType   : Int = 0 // 0: table이 댓글리스트를 현시, 1 : 유저검색리스트 현시
    var m_nNickName : String = ""
    var pdtUid      : Int!
    var nPage       : Int = 0
    var isLast      : Bool!
    var arrReplyList : Array<ReplyDto> = []
    var arrUsrList : Array<UsrMiniDto> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
        getPdtReplyList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func initUI() {
        
        lbNoData.isHidden = true
        _btnRegister.isEnabled = false
        
        _tblComment.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        _tblComment.register(UINib(nibName: "UserSimpleInfoCell", bundle: nil), forCellReuseIdentifier: "UserSimpleInfoCell")
        
        _tblComment.estimatedRowHeight = 1
        _tblComment.rowHeight = UITableViewAutomaticDimension
        _tfComment.setBorder(color: .white, width: 2)
        _tfComment.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        _btnRegister.layer.cornerRadius = 4
        _btnRegister.setBorder(color: UIColor(hex: 0xc3c3c3), width: 1)
        
        registerForKeyboardNotifications()
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func showSheet(index : Int) {
        let dic : ReplyDto = arrReplyList[index]
        if dic.reply.usrUid == gMeInfo.usrUid {
            
            let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("delete", comment:""), style: .destructive, handler: { (action: UIAlertAction!) in
                let acAlert = UIAlertController(title: "", message: NSLocalizedString("delete_alert_msg", comment:""), preferredStyle: .alert)
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                    self.delPdtReplyInfo(_uid: dic.reply.replyUid)
                }))
                
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                
                self.present(acAlert, animated: true, completion: nil)
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
            present(acAlert, animated: true, completion: nil)
        } else {
            let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("reply", comment:""), style: .default, handler: { (action: UIAlertAction!) in self._tfComment.becomeFirstResponder()
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("report", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                Blame.show(self) { (reasson, detail) in
                    self.reqReport(_uid: self.pdtUid, kind: 2, type: reasson, content: detail)
                }
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
            present(acAlert, animated: true, completion: nil)
        }
    }
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnGo2User(_ sender: UIButton) {
        let dic : ReplyDto = arrReplyList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.reply.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //답글달기
    @objc func onBtnReplyTalk(_ sender: UIButton) {
        let dic : ReplyDto = arrReplyList[sender.tag]
        
        _tfComment.text = String.init(format: "@%@ ", dic.usr.usrNckNm)//"@홍회원 "
        _btnRegister.setBorder(color: UIColor(hex: 0x42c2fe), width: 1.0)
        _btnRegister.setTitleColor(UIColor(hex:0x42c2fe), for: .normal)
        _tfComment.becomeFirstResponder()
    }
    
    @objc func onBtnGo2Return(_ sender: UIButton) {
        _tfComment.text = "@" + m_nNickName
        m_tblType = 0
        _tblComment.reloadData()
    }
    
    @objc func onBtnGo2Alert(_ sender: UIButton) {
        _tfComment.resignFirstResponder()
        showSheet(index: sender.tag)
    }
    
    @IBAction func onBtnBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func hideKeyboardAction(_ sender: Any) {
        _tfComment?.resignFirstResponder()
    }
    
    func needsUserList() -> Bool {
        if((_tfComment.text?.hasPrefix("@"))!/* && (_tfComment.text?.contains(" ") == false)*/) {
            return true
        }
        return false
    }
    
    @objc func textChanged(_ sender : UITextField) {
        if(_tfComment.text != "") {
            _btnRegister.setBorder(color: UIColor(hex: 0x42c2fe), width: 1.0)
            _btnRegister.setTitleColor(UIColor(hex:0x42c2fe), for: .normal)
            _btnRegister.isEnabled = true
        } else {
            _btnRegister.setBorder(color: UIColor(hex: 0xc3c3c3), width: 1.0)
            _btnRegister.setTitleColor(UIColor(hex:0x999999), for: .normal)
            _btnRegister.isEnabled = false
        }
        
        if(needsUserList() == true) {
            m_tblType = 1
            getPdtReplyUsrList(_uid: pdtUid)
        } else {
            if _tfComment.text == "" {
                m_tblType = 0
                getPdtReplyList()
            }
        }
        
    }
    
    @IBAction func onRegister(_ sender: Any) {
        hideKeyboardAction(self)
        
        if _tfComment.text == "" {
            view.makeToast(NSLocalizedString("empty_comment_msg", comment:""))
            return
        }
        if m_tblType == 1 {
            if(_tfComment.text?.hasPrefix("@") == false) {
                view.makeToast(NSLocalizedString("username_type_wrong", comment:""))
                return
            }
        }
        
        reqpdtReplyInfo()
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 댓글 목록 얻기
    func getPdtReplyList() {
        
        gProgress.show()
        Net.getPdtReplyList(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getPdtReplyListResult
                self.getPdtReplyListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 댓글 목록 얻기 결과
    func getPdtReplyListResult(_ data: Net.getPdtReplyListResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrReplyList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : ReplyDto = data.list[i]
            arrReplyList.append(dic)
        }
        
        if arrReplyList.count == 0 {
            lbNoData.isHidden = false
        } else {
            lbNoData.isHidden = true
        }
        m_tblType = 0
        _tblComment.reloadData()
    }
    
    // 댓글 작성
    func reqpdtReplyInfo() {
        let msg : String = _tfComment.text!
        let str = msg.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        
        gProgress.show()
        Net.reqPdtReplyInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            content         : msg,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.view.makeToast(NSLocalizedString("reply_comment_success", comment:""))
                self._tfComment.text = ""
                self.getPdtReplyList()
//                let res = result as! Net.setPdtReplyResult
//                self.reqpdtReplyInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
//    // 댓글 작성 결과
//    func reqpdtReplyInfoResult(_ data: Net.setPdtReplyResult) {
//        let dicReply : ReplyDto = data.replyDto
//    }
    
    // 상품 댓글가능 회원목록얻기
    func getPdtReplyUsrList(_uid : Int) {
        let msg : String = _tfComment.text!
       //let str = msg.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        
        gProgress.show()
        Net.getPdtReplyUsrList(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            keyword         : msg,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getPdtReplyUsrListResult
                self.getPdtReplyUsrListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 댓글가능 회원목록얻기 결과
    func getPdtReplyUsrListResult(_ data: Net.getPdtReplyUsrListResult) {
        
        arrUsrList = data.list
        _tblComment.reloadData()
    }
    
    // 신고 하기
    func reqReport(_uid : Int, kind : Int, type : String, content : String) {
        
        gProgress.show()
        Net.reqReport(
            accessToken     : gMeInfo.token,
            targetUid       : _uid,
            kind            : kind,
            content         : content,
            reportType      : type,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("report_submist_success", comment:""))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 댓글 삭제
    func delPdtReplyInfo(_uid : Int) {
        
        gProgress.show()
        Net.delPdtReplyInfo(
            accessToken     : gMeInfo.token,
            replyUid        : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.view.makeToast(NSLocalizedString("reply_comment_delete_success", comment:""))
               
                self.getPdtReplyList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension DetailCommentViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if m_tblType == 0 {
           return arrReplyList.count
        }
        
        return arrUsrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(m_tblType == 0) { //
            let dic : ReplyDto = arrReplyList[indexPath.row]
            
            let 검은색볼드12 = [NSAttributedStringKey.link: NSURL(string: "http://com.kyad.selluv/\(indexPath.row)")!, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.black]
            let 검은색12 = [NSAttributedStringKey.link: NSURL(string: "htts://com.kyad.selluv/\(indexPath.row)")!, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x000000)]
            let 푸른색12 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x1c3c6b)]
            
//            let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  "송회원", attributes: 검은색볼드12))
//            contentStr.append(NSAttributedString(string: " @김유저", attributes: 푸른색12))
//            contentStr.append(NSAttributedString(string: "어디서 구매하였는지 모르겠지요?ㄴ이라ㅓ니ㅏㅇ러ㅣ남ㅇ러ㅣㅏㅁㄴ어리ㅏㅁㄴ어리ㅏㅓㄴㅁ아ㅣ러ㅣ만ㅇ러ㅣㅏㄴㅁ어라ㅣㄴㅁ어리ㅏㅓㄴㅁ아ㅣ러ㅣㅏㄴㅁㅇ러ㅏㅣㄴㅁ어라ㅣㄴㅁ어라ㅣㅁㄴ어라ㅣㄴㅁ어라ㅓㄴㅁ아ㅣ런ㅁ아ㅣ러ㅏ님ㅇ러ㅏㅣㄴㅁㅇ러ㅏㅣㄴㅁㅇ러ㅏㅣㄴㅇㄹ", attributes: 검은색12))
            let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  dic.reply.content, attributes: 검은색12))

            let contentOtherStr = NSAttributedString(string: dic.reply.content, attributes: 검은색12)
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            
//            cell.delegate = self
            
            cell._btnPhoto.kf.setImage(with: URL(string: dic.usr
                .profileImg), for: .normal, placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell.lbTime.text = CommonUtil.diffTime1(dic.reply.regTime)
            
            if(dic.reply.targetUids != "") {
                cell._lbComment.attributedText = contentStr
                cell._tvComment.attributedText = contentStr
            } else {
                cell._lbComment.attributedText = contentOtherStr
                cell._tvComment.attributedText = contentOtherStr
            }
            
            if gMeInfo.usrUid == dic.reply.usrUid {
                cell._btnReplyTalk.isHidden = true
            } else {
                cell._btnReplyTalk.isHidden = false
            }
            
            cell._tvComment.delegate = self
            cell._btnSelect.tag = indexPath.row
            cell._btnSelect.addTarget(self, action:#selector(self.onBtnGo2Alert(_:)), for: UIControlEvents.touchUpInside)
            cell._btnPhoto.tag = indexPath.row
            cell._btnPhoto.addTarget(self, action:#selector(self.onBtnGo2User(_:)), for: UIControlEvents.touchUpInside)
            cell._btnReplyTalk.tag = indexPath.row
            cell._btnReplyTalk.addTarget(self, action:#selector(self.onBtnReplyTalk(_:)), for: UIControlEvents.touchUpInside)

            if indexPath.row == arrReplyList.count - 1 && !isLast {
                nPage = nPage + 1
                getPdtReplyList()
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserSimpleInfoCell", for: indexPath) as! UserSimpleInfoCell
            m_nNickName = cell._lbUserName.text!
            
            let dic : UsrMiniDto = arrUsrList[indexPath.row]
            cell._imvProfile.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell._lbUserId.text = dic.usrId
            cell._lbUserName.text = dic.usrNckNm
            
            cell._btnSelect.tag = indexPath.row
            cell._btnSelect.addTarget(self, action:#selector(self.onBtnGo2Return(_:)), for: UIControlEvents.touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

//extension DetailCommentViewController: CommentCellDelegate {
//    func CommentCellDelegate(_ commentCell : CommentCell, onCommentWrite sender : Any, tag: Int?) {
//        _tfComment.text = "@홍회원 "
//        _btnRegister.setBorder(color: UIColor(hex: 0x42c2fe), width: 1.0)
//        _btnRegister.setTitleColor(UIColor(hex:0x42c2fe), for: .normal)
//        _tfComment.becomeFirstResponder()
//    }
//}

extension DetailCommentViewController {
    //키보드생길때
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self._cstrBottomSpace.constant = -keyboardSize.height
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    //키보드없어질때
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self._cstrBottomSpace.constant = 0
            self.view.layoutIfNeeded()
        })
    }
}

extension DetailCommentViewController : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        let url = URL.absoluteString
        
        let str : String = url.substring(from: url.lastIndex(of: "/")! + 1) // user id
        
        if url.substring(to: 4) == "http" {
            pushVC("USER_VIEW", storyboard: "User", animated: true)
        } else {
            if m_tblType == 0 {
                if str != "" {
                    let index = Int(str)
                    showSheet(index: index!)
                }
            }
        }
        
        return false
        
    }
}
