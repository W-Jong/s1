//
//  DetailStyleViewController.swift
//  selluv
//  연관스타일
//  Created by Gambler on 12/12/17.
//  modified by PJH on 15/03/18.

import UIKit

class DetailStyleViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    @IBOutlet weak var clvT1: UICollectionView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    
    @IBOutlet weak var vwMenuBar: UIView!
    
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbAllCnt: UILabel!
    var pageTitle : String = NSLocalizedString("relative_style", comment:"")
    var pdtUid    : Int!
    //zoom효과관련
    weak var selectedImageView : UIImageView?
    var nPage       : Int = 0
    var isLast      : Bool!
    var arrPdtList  : Array<PdtListDto> = []

    var animator : ARNTransitionAnimator?
    
    //collectionView관련
    
    enum ETab : Int {
        case T1 = 0
        case T2
        case T3
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
        getPdtLikeUpdateYn()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        // Collection view attributes
        clvT1.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        clvT1.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        clvT1.collectionViewLayout = layout
        
        let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
        clvT1.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        lbTitle.text! = pageTitle
        
    }
    
    func scrollUp() {
        
        if scrollDirection == .up {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
        })
        
    }
    
    func scrollDown() {
        
        if scrollDirection == .down {
            return
        }
        
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.vwTop.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -self.vwTop.frame.height))
            self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 130))
        })
    }
    
    func showInteractive(_uid : Int) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = _uid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnStyle(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, pdtUid: dicPdt.pdtUid)
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            
            cell = clvT1.cellForItem(at: indexPath) as! MainItemCVC
            scrollPos = clvT1.contentOffset.y
            
            let dic : PdtListDto = arrPdtList[indexPath.row]
            let dicPdt          : PdtMiniDto         = dic.pdt  //상품
            
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
        }
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        self.pushVC("LIKE_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func saleAction(_ sender: Any) {
        pushVC("FILTER_VIEW", storyboard: "Top", animated: true)
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        clvT1.scrollToTop(isAnimated: true)
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 연관스타일 목록 얻기
    func getRelationStyleList() {
        
        gProgress.show()
        Net.getRelationStyleList(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getRelationStyleListResult
                self.getRelationStyleListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //연관스타일 목록 얻기 결과
    func getRelationStyleListResult(_ data: Net.getRelationStyleListResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrPdtList.append(dic)
        }
        lbAllCnt.text = "(" + String(data.totalElements) + ")"
        clvT1.reloadData()
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, pdtUid: pdtUid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, pdtUid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            vc.pdtUid = pdtUid
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getRelationStyleList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()

                self.getRelationStyleList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 좋아요 업데이트뱃지 표시여부
    func getPdtLikeUpdateYn() {
        
        gProgress.show()
        Net.getLikeUpdateStatus(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.PdtLikeUpdateStatusResult
                
                if res.status {
                    self.btnRight.setImage(UIImage(named: "ic_thema_heart"), for: .normal)
                } else {
                    self.btnRight.setImage(UIImage(named: "ic_thema_heart_off"), for: .normal)
                }

                self.getRelationStyleList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension DetailStyleViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            break
            
        case clvT1:
            
            self.scrollDirection = EScrollDirection.none
        
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvT1:
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }
            
            scrollPos = curScrollPos
            
        default:
            return
        }
    }
    
}

extension DetailStyleViewController : UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC
        
        self.selectedImageView = cell._imgCom
        
        let dic : PdtListDto = arrPdtList[indexPath.row]
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품

        self.showInteractive(_uid: dicPdt.pdtUid)
    }
}

extension DetailStyleViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPdtList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC

        let dic : PdtListDto = arrPdtList[indexPath.row]
        let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
        let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
        let dicPdt          : PdtMiniDto         = dic.pdt  //상품
        let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
        let blikeStatus     : Bool              = dic.pdtLikeStatus  //유저 상품좋아요 상태
        
        //pdt
        cell._btnStyle.isHidden = false
        cell._imgEffect.isHidden = false
        cell._imgCom.kf.setImage(with: URL(string: dicPdtStyle.styleImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        cell._btnStyle.kf.setImage(with: URL(string: dicPdtStyle.styleImg), for: .normal, placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        cell._btnStyle.isHidden = true
        cell._imgEffect.isHidden = true

        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
            height = width * ratio
        }
        
        if dicPdt.originPrice <= dicPdt.price {
            cell.hiddenDown(price: dicPdt.price)
        } else {
            cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
        }
        cell.lbSize.text = dicPdt.pdtSize
        
        cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
        
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
        cell._lbBrand.text = dicBrand.nameEn
        cell._lbLike.text = String(likeCnt)
        cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
        //        cell._imgComWidth.constant = width
        cell._imgComHeight.constant = height
        
        //user
        cell._lbName.text = dicUsr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == arrPdtList.count - 1 && !isLast {
            nPage = nPage + 1
            getRelationStyleList()
        }
        
        return cell
    }
}

extension DetailStyleViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {

        let dic : PdtListDto = arrPdtList[indexPath.row]
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        
        if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
            height = width * ratio
        }
        
        if dic.pdt.originPrice <= dic.pdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 119
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 119)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StyleGridHeaderView", for: indexPath as IndexPath)
    }
    
}

extension DetailStyleViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return selectedImageView ?? UIImageView()
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        guard let selectedImageView = selectedImageView else { return CGRect.zero }
        return selectedImageView.convert(selectedImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        selectedImageView?.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        selectedImageView?.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        selectedImageView?.isHidden = false
    }
}

