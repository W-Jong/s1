//
//  DetailViewController.swift
//  selluv
//  상품 상세
//  Created by World on 1/4/18.
//  modified by PJH on 14/03/18.

import UIKit
import Photos
import Toast_Swift
import TwitterKit
import Social
import FBSDKShareKit

class DetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout, FBSDKSharingDelegate, UIDocumentInteractionControllerDelegate {
    
    //user
    @IBOutlet weak var ivUerProfile: UIImageView!
    @IBOutlet weak var lbNickname: UILabel! 
    @IBOutlet weak var lbRegTime: UILabel!
    
    @IBOutlet weak var vwsaleStop: UIView!
    @IBOutlet weak var vwNego: UIView!
    @IBOutlet weak var vwBuy: UIView!
    @IBOutlet weak var lbNago: UILabel!
    @IBOutlet weak var lbBuy: UILabel!
    
    @IBOutlet weak var lbComName: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var lbDownPrice: UILabel!
    
    @IBOutlet weak var ivPercent: UIImageView!
    @IBOutlet weak var lbDownPricePercent: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var imgHeart: UIImageView!
    @IBOutlet weak var lbHeart: UILabel!
    @IBOutlet weak var lbHeart1: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgIttem: UIImageView!
    @IBOutlet weak var lbIttem1: UILabel!
    @IBOutlet weak var lbIttem: UILabel!
    @IBOutlet weak var btnIttem: UIButton!
    @IBOutlet weak var scvMain: UIScrollView!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var clvCom: UICollectionView!
    @IBOutlet weak var clvStyle: UICollectionView!
    @IBOutlet weak var htTableCell: NSLayoutConstraint!
    @IBOutlet weak var htCollectionCell: NSLayoutConstraint!
    @IBOutlet weak var htBottomBar: NSLayoutConstraint!
    @IBOutlet weak var htStyleBtnBottom: NSLayoutConstraint!
    @IBOutlet weak var lbComment1: UILabel!
    @IBOutlet weak var lbComment: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var btnMoreView: UIButton!
    @IBOutlet weak var vwImage: UIView!
    @IBOutlet weak var imgSoldOut: UIImageView!
    
    @IBOutlet weak var vwRelationStyle: UIView!
    @IBOutlet weak var vwBanner: UIView!
    @IBOutlet weak var scvBanner: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stvIndicator: UIStackView!
    @IBOutlet weak var stvIndicatorWidth: NSLayoutConstraint!
    @IBOutlet weak var smallImageView: UIImageView!
    
    @IBOutlet weak var btnStyle: UIButton!
    @IBOutlet weak var lbStylecnt: UILabel!
    @IBOutlet weak var lcvwNoStyleHeight: NSLayoutConstraint!
    @IBOutlet weak var lccvSellerPdtHeight: NSLayoutConstraint!
    
    //pdtinfo
    @IBOutlet weak var btnLikeGroup: UIButton!
    @IBOutlet weak var btnCategory1: UIButton!
    @IBOutlet weak var btnCategory2: UIButton!
    @IBOutlet weak var btnCategory3: UIButton!
    @IBOutlet weak var btnBrand: UIButton!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var btnModel: UIButton!
    @IBOutlet weak var btnTag: UIButton!
    
    //seller info
    @IBOutlet weak var ivSellerProfile: UIImageView!
    @IBOutlet weak var lbSellerNickname: UILabel!
    @IBOutlet weak var lbSellerPoint: UILabel!
    @IBOutlet weak var lbItemCnt: UILabel!
    @IBOutlet weak var lbFollerCnt: UILabel!
    
    //additional info
    @IBOutlet weak var lbDeliverPrice: UILabel!
    @IBOutlet weak var lbPdtQuality: UILabel!
    @IBOutlet weak var ivPdtQuality: UIImageView!
    @IBOutlet weak var vPdtComp1: UIView!
    @IBOutlet weak var ivPdtComp1: UIImageView!
    @IBOutlet weak var lbPdtComp1: UILabel!
    @IBOutlet weak var vPdtComp2: UIView!
    @IBOutlet weak var ivPdtComp2: UIImageView!
    @IBOutlet weak var lbPdtComp2: UILabel!
    @IBOutlet weak var vPdtComp3: UIView!
    @IBOutlet weak var ivPdtComp3: UIImageView!
    @IBOutlet weak var lbPdtComp3: UILabel!
    @IBOutlet weak var vPdtComp4: UIView!
    @IBOutlet weak var ivPdtComp4: UIImageView!
    @IBOutlet weak var lbPdtComp4: UILabel!
    @IBOutlet weak var vPdtComp5: UIView!
    @IBOutlet weak var ivPdtComp5: UIImageView!
    @IBOutlet weak var lbPdtComp5: UILabel!
    @IBOutlet weak var vPdtComp6: UIView!
    @IBOutlet weak var ivPdtComp6: UIImageView!
    @IBOutlet weak var lbPdtComp6: UILabel!
    @IBOutlet weak var vPdtEtc: UIView!
    @IBOutlet weak var ivPdtEtc: UIImageView!
    @IBOutlet weak var lbPdtEtc: UILabel!
    @IBOutlet weak var icAdditionalSvHeight: NSLayoutConstraint!
    @IBOutlet var icAdditionalContentHeight: NSLayoutConstraint!
    
    
    var pdtlikeGroup : String = ""
    var m_bFlag     = false
    var pdtUid      : Int!
    var imgList     : Array<String> = []
    var dicPdtInfo  : PdtDetailDto!
    var nPage       : Int = 0
    var isLast      : Bool!
    var arrPdtList  : Array<PdtListDto> = []

    var m_isLoad = false
    var iitemsStatus : Bool = false
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        m_isLoad = true
        getPdtDetailInfo()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    // MARK: - Helper
    
    func initUI() {
        
        htStyleBtnBottom.constant = 60
        
        scvMain.delegate = self
        scvBanner.delegate = self
        
        let layout = CHTCollectionViewWaterfallLayout()
        
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 0.0
        
        clvStyle.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        clvStyle.alwaysBounceVertical = true
        
        clvStyle.delegate = self
        clvStyle.dataSource = self
        clvStyle.collectionViewLayout = layout
        
        let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
        clvStyle.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        tvList.delegate = self
        tvList.dataSource = self
        tvList.register(UINib.init(nibName: "StyleCell", bundle: nil), forCellReuseIdentifier: "StyleCell")
        
        clvCom.delegate = self
        clvCom.dataSource = self
        clvCom.register(UINib.init(nibName: "T1CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "T1CollectionViewCell")
        
    }

    //서버로부터 받은 데이터로 설정한다.
    func setPdtData()  {
        
        //상품 사진 리스트
        imgList = dicPdtInfo.pdt.photoList
        setPdtImgs()
        
        //상단 유저
        ivUerProfile.kf.setImage(with: URL(string: dicPdtInfo.seller.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        lbNickname.text = dicPdtInfo.seller.usrNckNm
        if dicPdtInfo.pdt.edtTime == 0{
            if dicPdtInfo.edited {
                lbRegTime.text = CommonUtil.diffTime(dicPdtInfo.pdt.regTime) + " " + "modify".localized
            } else {
                lbRegTime.text = CommonUtil.diffTime(dicPdtInfo.pdt.regTime)
            }
        } else {
            if dicPdtInfo.edited {
                lbRegTime.text = CommonUtil.diffTime(dicPdtInfo.pdt.edtTime) + " " + "modify".localized
            } else {
                lbRegTime.text = CommonUtil.diffTime(dicPdtInfo.pdt.edtTime)
            }
            
        }
        
        if dicPdtInfo.pdtStyleList.count > 0 {
            btnStyle.isHidden = false
        } else {
            btnStyle.isHidden = true
        }
        
        //판매 상태 설정
        switch dicPdtInfo.pdt.status {
        case 1: //판매중
            vwsaleStop.isHidden = true
            imgSoldOut.isHidden = true
            vwBuy.alpha = 1
            break
        case 2: //판매완료
            vwsaleStop.isHidden = false
            imgSoldOut.isHidden = false
            vwBuy.alpha = 0.3
            break
        case 3: //판매중지
            vwsaleStop.isHidden = false
            imgSoldOut.isHidden = true
            vwBuy.alpha = 0.3
            break
        case 4: //휴가모드
            vwsaleStop.isHidden = false
            imgSoldOut.isHidden = true
            vwBuy.alpha = 0.3
            break
        default:
            break
        }
        
        //하단 버튼 설정
        if gMeInfo.usrUid != dicPdtInfo.pdt.usrUid {
            lbNago.text = NSLocalizedString("nago", comment:"")
            lbBuy.text = NSLocalizedString("buy", comment:"")
            
            if dicPdtInfo.pdt.negoYn == 1 {
                vwNego.alpha = 1
                if dicPdtInfo.pdt.status != 1 { //판매중 상태가 아닌 다른 상태이면 리턴
                    vwNego.alpha = 0.3
                }
            } else {
                vwNego.alpha = 0.3
            }
            
        } else { //  등록한 유저
            lbNago.text = NSLocalizedString("delete", comment:"")
            lbBuy.text = NSLocalizedString("modify", comment:"")
            vwBuy.alpha = 1.0
            vwNego.alpha = 1.0
        }

        if dicPdtInfo.pdtLikeStatus {
            lbHeart.textColor = UIColor(red: 0xfe / 255.0, green: 0x26 / 255.0, blue: 0x3b / 255.0, alpha: 1.0)
             imgHeart.image = UIImage.init(named: "ic_like_heart")
        } else {
            lbHeart.textColor = UIColor(red: 0xff / 255.0, green: 0xff / 255.0, blue: 0xff / 255.0, alpha: 1.0)
             imgHeart.image = UIImage.init(named: "ic_dislike_heart_big")
        }
        lbHeart.text = String(dicPdtInfo.pdtLikeCount)
        lbHeart1.text = String(dicPdtInfo.pdtLikeCount)

        iitemsStatus = dicPdtInfo.pdtWishStatus
        if iitemsStatus {
            imgIttem.image = UIImage.init(named: "ic_detail_ittem_on")
        } else {
            imgIttem.image = UIImage.init(named: "ic_detail_ittem_off")
        }
        
        lbIttem.text = String(dicPdtInfo.pdtWishCount)
        lbIttem1.text = String(dicPdtInfo.pdtWishCount)
        
        lbComment.text = String(dicPdtInfo.pdtReplyCount)
        lbComment1.text = String(dicPdtInfo.pdtReplyCount)

        //pdt info
        var likegroup = ""
        if dicPdtInfo.pdt.pdtGroup & 1 != 0 {
           likegroup = NSLocalizedString("male", comment:"")
        }

        if dicPdtInfo.pdt.pdtGroup & 2 != 0 {
            likegroup = NSLocalizedString("female", comment:"")
        }

        if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
            likegroup = NSLocalizedString("kids", comment:"")
        }

        var cateName = ""
        if dicPdtInfo.categoryList.count > 0 {
            cateName = dicPdtInfo.categoryList[dicPdtInfo.categoryList.count - 1].categoryName
        }
        pdtlikeGroup = likegroup
        lbComName.text = dicPdtInfo.brand.nameKo + " " + likegroup + " " + dicPdtInfo.pdt.colorName + " " + dicPdtInfo.pdt.pdtModel + " " + cateName
        lbSize.text = dicPdtInfo.pdt.pdtSize
        lbDesc.text = dicPdtInfo.pdt.content
        
        var price = ""
        if dicPdtInfo.pdt.originPrice <= dicPdtInfo.pdt.price {
            //실가격
            price = CommonUtil.formatNum(dicPdtInfo.pdt.price)
            ivPercent.isHidden = true
            lbDownPricePercent.isHidden = true
            lbDownPrice.isHidden = true
            
            lbPrice.textColor = UIColor.black
            lbPrice.text = "₩ " + price
        } else {
            //할인 가격 표시
            ivPercent.isHidden = false
            lbDownPricePercent.isHidden = false
            lbDownPrice.isHidden = false
            
            //percent
            let origin = dicPdtInfo.pdt.originPrice
            let price1 = dicPdtInfo.pdt.price
            let diff = origin! - price1!
            let percent = Int((Float (diff) / Float (origin!)) * 100)

            lbDownPricePercent.text = String(percent) + "%"
            lbDownPrice.text = "₩ " + CommonUtil.formatNum(dicPdtInfo.pdt.price)
            
            lbPrice.textColor = UIColor.gray
            
            let attr : NSAttributedString = NSAttributedString.init(string: "₩ " + CommonUtil.formatNum(dicPdtInfo.pdt.originPrice), attributes: [NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: Int8(NSUnderlineStyle.styleSingle.rawValue))]);
            lbPrice.attributedText = attr
        }
        
        if likegroup != "" {
            btnLikeGroup.setTitle("#" + likegroup, for: .normal)
        }
        
        if dicPdtInfo.categoryList.count > 3 && dicPdtInfo.categoryList[3].categoryName != "" {
            btnCategory3.setTitle("#" + dicPdtInfo.categoryList[3].categoryName, for: .normal)
        }
        
        if dicPdtInfo.categoryList.count > 2 && dicPdtInfo.categoryList[2].categoryName != "" {
            btnCategory2.setTitle("#" + dicPdtInfo.categoryList[2].categoryName, for: .normal)
        }

        if dicPdtInfo.categoryList.count > 1 && dicPdtInfo.categoryList[1].categoryName != "" {
            btnCategory1.setTitle("#" + dicPdtInfo.categoryList[1].categoryName, for: .normal)
        }
        
        if dicPdtInfo.brand.nameKo != "" {
            btnBrand.setTitle("#" + dicPdtInfo.brand.nameKo, for: .normal)
        }
        if dicPdtInfo.pdt.colorName != "" {
            btnColor.setTitle("#" + dicPdtInfo.pdt.colorName, for: .normal)
        }
        if dicPdtInfo.pdt.pdtModel != "" {
            btnModel.setTitle("#" + dicPdtInfo.pdt.pdtModel, for: .normal)
        }
        if dicPdtInfo.pdt.tag != "" {
            btnTag.setTitle(dicPdtInfo.pdt.tag, for: .normal)
        }
        
        lbStylecnt.text = String.init(format: "(%d)", dicPdtInfo.relationStyleCount)
        if dicPdtInfo.relationStyleCount == 0 {
            vwRelationStyle.isHidden = true
        } else {
            vwRelationStyle.isHidden = false
        }
        
        var styleTBHeight : CGFloat = 0
        let width = UIScreen.main.bounds.size.width
        for i in 0..<dicPdtInfo.pdtStyleList.count {
            let dic : PdtStyleDto  = dicPdtInfo.pdtStyleList[i]
            
            var height : CGFloat = 0
            var ratio : CGFloat = 0
            
            if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
                height = width * ratio
            }
            styleTBHeight = styleTBHeight + height
        }
        htTableCell.constant = styleTBHeight
        
        if dicPdtInfo.pdtStyleList.count == 0 {
            lcvwNoStyleHeight.constant = 100
        } else {
            lcvwNoStyleHeight.constant = 0
            tvList.reloadData()
        }
        
        //판매자 정보
        ivSellerProfile.kf.setImage(with: URL(string: dicPdtInfo.seller.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        lbSellerNickname.text = dicPdtInfo.seller.usrNckNm
        lbSellerPoint.text = String(dicPdtInfo.seller.point)
        lbItemCnt.text = String.init(format: "%@ %d", NSLocalizedString("item", comment:""), dicPdtInfo.sellerItemCount)
        lbFollerCnt.text = String.init(format: "%@ %d", NSLocalizedString("follower", comment:""), dicPdtInfo.sellerFollowerCount)
        if dicPdtInfo.sellerPdtList.count == 0 {
            lccvSellerPdtHeight.constant = 0
        } else {
            lccvSellerPdtHeight.constant = 170
            clvCom.reloadData()
        }
        
        //기타 정보
        if dicPdtInfo.pdt.sendPrice == 0 {
            lbDeliverPrice.text = "무료 배송"
        }
        else {
            lbDeliverPrice.text = "₩ " + CommonUtil.formatNum(dicPdtInfo.pdt.sendPrice)
        }
        
        let arrConditionImg = ["ic_detail_info_new", "ic_detail_info_best", "ic_detail_info_good", "ic_detail_info_likenew"]
        let arrConditionText = ["새상품", "최상", "상", "중상"]
        if dicPdtInfo.pdt.pdtCondition >= 1 && dicPdtInfo.pdt.pdtCondition <= 4 {
            ivPdtQuality.image = UIImage(named:arrConditionImg[(dicPdtInfo.pdt.pdtCondition - 1)])
            lbPdtQuality.text = arrConditionText[(dicPdtInfo.pdt.pdtCondition - 1)]
        }
        
        // 110001 => other
        let arrCompView = [vPdtComp1, vPdtComp2, vPdtComp3, vPdtComp4, vPdtComp5, vPdtComp6]
        let arrCompLabel = [lbPdtComp1, lbPdtComp2, lbPdtComp3, lbPdtComp4, lbPdtComp5, lbPdtComp6]
        let arrCompImgView = [ivPdtComp1, ivPdtComp2, ivPdtComp3, ivPdtComp4, ivPdtComp5, ivPdtComp6]
        let arrCompImg = ["ic_detail_tag", "ic_detail_guarnteecard", "ic_detail_receipt", "ic_detail_accessories", "ic_detail_box", "ic_detail_dustbag"]
        let arrCompTitle = ["상품택", "게런티카드", "영수증", "여분부속품", "브랜드박스", "더스트백"]
        let arrCompCharacter = Array(dicPdtInfo.pdt.component)
        
        var compLen = 0
        for nInd in 0 ..< arrCompCharacter.count  {
            let val = Int(String(arrCompCharacter[nInd]))
            
            if val == 1 {
                arrCompLabel[compLen]?.text = arrCompTitle[nInd]
                arrCompImgView[compLen]?.image = UIImage(named:arrCompImg[nInd])
                compLen = compLen + 1
            }
        }
        
        if dicPdtInfo.pdt.etc != "" {
            if compLen == 6 {
                ivPdtEtc.image = UIImage(named:"ic_detail_etc")
                lbPdtEtc.text = dicPdtInfo.pdt.etc
                vPdtEtc.isHidden = false
            }
            else {
                arrCompLabel[compLen]?.text = dicPdtInfo.pdt.etc
                arrCompImgView[compLen]?.image = UIImage(named:"ic_detail_etc")
                vPdtEtc.isHidden = true
            }
            compLen = compLen + 1
        }
        
        if compLen >= 5 {
            icAdditionalSvHeight.constant = 122
            icAdditionalContentHeight.constant = 122
        }
        else {
            icAdditionalSvHeight.constant = 60
            icAdditionalContentHeight.constant =  60
        }
        
        if(compLen < 6) {
            for nInd in compLen ..< arrCompView.count {
                arrCompView[nInd]?.isHidden = true
            }
        }
    }
    
    func setPdtImgs() {
        
        scvBanner.contentSize = CGSize(width: (self.view.frame.size.width) * CGFloat(imgList.count), height: scvBanner.frame.size.height)
        
        for nInd in 0 ..< imgList.count {
            let ivBanner = UIImageView(frame: CGRect(x: self.view.frame.size.width * CGFloat(nInd), y: 0, width: scvBanner.frame.size.width, height: scvBanner.frame.size.height))

            ivBanner.isUserInteractionEnabled = true
            ivBanner.clipsToBounds = true
            ivBanner.contentMode = UIViewContentMode.scaleAspectFit
            ivBanner.kf.setImage(with: URL(string: imgList[nInd]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

            ivBanner.tag = nInd
            scvBanner.addSubview(ivBanner)

            let image = UIImage(named: nInd == 0 ? "ic_shop_banner_indicator_active" : "ic_shop_banner_indicator_inactive")
            let ivPageNum = UIImageView(frame: CGRect(origin: CGPoint(x: CGFloat(image!.size.width + 5.0) * CGFloat(nInd), y: 0), size: CGSize(width: image!.size.width, height: image!.size.height)))
            ivPageNum.tag = nInd
            ivPageNum.image = image
            
//            if !m_isLoad{
                stvIndicator.addSubview(ivPageNum)
                stvIndicatorWidth.constant = CGFloat(image!.size.width) * CGFloat(nInd + 1) + 5.0 * CGFloat(nInd)
//            }
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            ivBanner.isUserInteractionEnabled = true
            tapGestureRecognizer.numberOfTapsRequired = 1
            ivBanner.addGestureRecognizer(tapGestureRecognizer)
            
            let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageDoubleTapped(tapGestureRecognizer:)))
            doubleTapGestureRecognizer.numberOfTapsRequired = 2
            ivBanner.addGestureRecognizer(doubleTapGestureRecognizer)
            
            tapGestureRecognizer.require(toFail: doubleTapGestureRecognizer)
            tapGestureRecognizer.delaysTouchesBegan = true
            doubleTapGestureRecognizer.delaysTouchesBegan = true
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        MainPopup.show(self, image: imgList) {
            
        }
    }
    
    @objc func imageDoubleTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        var status = -1
        if dicPdtInfo.pdtLikeStatus {
            status = 1
            delPdtLike(_uid: dicPdtInfo.pdt.pdtUid)
        } else {
            status = 0
            setPdtLike(_uid: dicPdtInfo.pdt.pdtUid)
        }
        showHeart(self, status)
    }
    
    func getImageFromAsset(assetsList : [PHAsset]) -> [UIImage] {
        var resImg = [UIImage]()
        
        let option = PHImageRequestOptions()
        option.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        option.isSynchronous = true
        
        for asset in assetsList {
            let imageSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
            
            PHCachingImageManager.default().requestImage(for: asset, targetSize: imageSize, contentMode: .aspectFill, options: option) { (result, _) in
                if let image = result {
                    resImg.append(image)
                }
            }
        }
        
        return resImg
    }
    
    func scrollUp() {
        
        if scrollDirection == .up {
            return
        }
        
//        htBottomBar.constant = 0
        htStyleBtnBottom.constant = 60
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollDown() {
        
        if scrollDirection == .down {
            return
        }
        
//        htBottomBar.constant = -45
        htStyleBtnBottom.constant = -60
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func shareAll() {
        let text = ""
        let image = UIImage(named: "ic_logo")
        let myWebsite = NSURL(string:dicPdtInfo.shareUrl)
        let shareAll = [text , image! , myWebsite as Any] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareInstagram() {
        let img  = UIImageView()
        img.kf.setImage(with: URL(string: dicPdtInfo.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

        let image = img.image
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let checkValidation = FileManager.default
        let getImagePath = paths.appending("image.igo")
        try?  checkValidation.removeItem(atPath: getImagePath)
        let imageData =  UIImageJPEGRepresentation(image!, 1.0)
        try? imageData?.write(to: URL.init(fileURLWithPath: getImagePath), options: .atomicWrite)
        var documentController : UIDocumentInteractionController!
        documentController = UIDocumentInteractionController.init(url: URL.init(fileURLWithPath: getImagePath))
        documentController.uti = "com.instagram.exclusivegram"
        documentController.presentOptionsMenu(from:self.view.frame, in: self.view, animated: true)
    }
    
    //공유
    func goshare(index : Int) {
        switch index {
        case 0:
            addPdtitem()
        case 1:  //인스타그람
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            let imageInstagram = UIImage(view:self.view)
            
            let photoLibrary = PHPhotoLibrary.shared()
            photoLibrary.savePhoto(image: imageInstagram, albumName: "selluv", completion: { (assets) in
                
                let u = "instagram://library?LocalIdentifier=" + (assets?.localIdentifier)! + "&InstagramCaption=SELLUV"
                let url = URL(string: u)!
                
                DispatchQueue.main.async {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(URL(string: u)!)
                    } else {
                        CommonUtil.showToast("인스타그램 공유 실패했습니다. 인스타그램 앱이 설치되어있는지 확인해 주세요.")
                    }
                }
            })
            break
        case 2: //네이버불로그
            //shareAll()  //임시로 해놓은 부분
            let shareString = String.init(format: "http://share.naver.com/web/shareView.nhn?title=%@&url=%@", "SELLUV", dicPdtInfo.shareUrl)
            // encode a space to %20 for example
            let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            // cast to an url
            guard let url = URL(string: escapedShareString) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            break
        case 3: //페이스북
            let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
            content.contentURL = URL(string: dicPdtInfo.shareUrl)
            content.quote = "SELLUV\n\n" + self.dicPdtInfo.shareUrl
            let shareDialog: FBSDKShareDialog = FBSDKShareDialog()
            
            shareDialog.shareContent = content
            shareDialog.delegate = self
            shareDialog.fromViewController = self
            shareDialog.show()
            break
        case 4: //트위터
            
//            let vc = TWTRComposer()
//            vc.setText("SELLUV")
//            vc.setImage(#imageLiteral(resourceName: "ic_logo"))
//            vc.setURL(URL(string: dicPdtInfo.shareUrl))
//            vc.show(from: self) { (result) in
//                if result == TWTRComposerResult.cancelled {
//                    CommonUtil.showToast("트위터 공유 실패했습니다. 트위터 앱이 설치되어있는지 확인해 주세요.")
//                } else {
//                    CommonUtil.showToast("공유 성공했습니다.")
//                }
//            }
//            let shareString = "https://twitter.com/intent/tweet?text=\("SELLUV")&url=\(dicPdtInfo.shareUrl)"
            let shareString = String.init(format: "https://twitter.com/intent/tweet?text=%@&url=%@", "SELLUV", dicPdtInfo.shareUrl)
            // encode a space to %20 for example
            let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            // cast to an url
            guard let url = URL(string: escapedShareString) else {
                return //be safe
            }
                                                                                                                                                                
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            break
        case 5: //카카오스토리
            // Feed 타입 템플릿 오브젝트 생성
            let template = KLKTextTemplate.init { (feedTemplateBuilder) in
                feedTemplateBuilder.text = "SELLUV\n\n" + self.dicPdtInfo.shareUrl
                // 컨텐츠
                
                feedTemplateBuilder.link = KLKLinkObject.init(builderBlock: { (linkBuilder) in
                    linkBuilder.mobileWebURL = URL.init(string: self.dicPdtInfo.shareUrl)
                })
            }
            
            // 카카오링크 실행
            KLKTalkLinkCenter.shared().sendDefault(with: template, success: { (warningMsg, argumentMsg) in
                
                // 성공
                print("warning message: \(String(describing: warningMsg))")
                print("argument message: \(String(describing: argumentMsg))")
                CommonUtil.showToast("공유 성공했습니다.")
                
            }, failure: { (error) in
                print("\(error)")
                // 실패
                CommonUtil.showToast("카카오톡 공유 실패했습니다. 카카오톡 앱이 설치되어있는지 확인해 주세요.")
            })
            break

        default:
            break
        }
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        CommonUtil.showToast("페이스북 공유 실패했습니다. 페이스북 앱이 설치되어있는지 확인해 주세요.")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        CommonUtil.showToast("공유 성공했습니다.")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("sharer NSError")
        print(error.localizedDescription)
    }
    
    // MARK: - Action
    
    @objc func onBtnStyle(_ sender: UIButton) {
//
//        let dic : PdtListDto = arrPdtList[sender.tag]
//        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
//        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid)

    }
    
    @objc func onBtnTClose(_ sender: UIButton) {
        
        let acAlert = UIAlertController(title: "",
                                        message: NSLocalizedString("delete_alert_msg", comment:""),
                                        preferredStyle: .alert)
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            
            let dic : PdtStyleDto  = self.dicPdtInfo.pdtStyleList[sender.tag]
            self.delPdtStyleInfo(pdtStyleUid: dic.pdtStyle.pdtStyleUid)
        }))
        
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(acAlert, animated: true, completion: nil)
    }
    
    @objc func onBtnTUser(_ sender: UIButton) {
        let dic : PdtStyleDto  = dicPdtInfo.pdtStyleList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.pdtStyle.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            
            cell = clvStyle.cellForItem(at: indexPath) as! MainItemCVC
            scrollPos = scvMain.contentOffset.y - (clvStyle.superview?.frame.origin.y)! - clvStyle.contentOffset.y - clvStyle.frame.origin.y
            
            let dic : PdtListDto = arrPdtList[indexPath.row]
            let dicPdt          : PdtMiniDto         = dic.pdt  //상품
            
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
        }
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.pdt.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @objc func onBtnTPopup(_ sender: UIButton) {
//        DetailPopup.show(self, image: [#imageLiteral(resourceName: "bg_style_banner.png"), #imageLiteral(resourceName: "bg_burberry.png"), #imageLiteral(resourceName: "bg_login_suggest_4.png")])
        let dic : PdtStyleDto  = dicPdtInfo.pdtStyleList[sender.tag]
        getPdtStyleDetailInfo(_pdtStyleUid: dic.pdtStyle.pdtStyleUid, pdtUid: dic.pdtStyle.pdtUid)
    }
    
    @IBAction func moreViewAction(_ sender: Any) {
        btnMoreView.isHidden = true
        lbDesc.numberOfLines = 0
    }
    
    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func profileAction(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicPdtInfo.pdt.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func alarmAction(_ sender: Any) {
        let nav : UINavigationController! = self.navigationController
        let vc = AlarmSet(nibName: "AlarmSet", bundle: nil)
        vc.dicPdtInfo = dicPdtInfo
        nav.pushViewController(vc, animated: true)
    }
    
    //더보기
    @IBAction func moreAction(_ sender: Any) {
        let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("item1", comment:""), style: .default, handler: { (action: UIAlertAction!) in
            IttemSelect.show(self) { (type) in
                self.goshare(index: type)
            }

        }))
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("share", comment:""), style: .default, handler: { (action: UIAlertAction!) in
            IttemSelect.show(self) { (type) in
                self.goshare(index: type)
            }
        }))
        acAlert.addAction(UIAlertAction(title: NSLocalizedString("report", comment:""), style: .destructive, handler: { (action: UIAlertAction!) in
            
            let acAlert1 = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            acAlert1.addAction(UIAlertAction(title: NSLocalizedString("non_product", comment:""), style: .destructive, handler: { (action: UIAlertAction!) in
                BlameDummy.show(self, _uid: self.pdtUid)
            }))
            acAlert1.addAction(UIAlertAction(title: NSLocalizedString("gita", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                Blame.show(self) { (reasson, detail) in
                    self.reqReport(_uid: self.pdtUid, kind: 2, type: reasson, content: detail)
                }
            }))
            acAlert1.addAction(UIAlertAction(title:NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
            self.present(acAlert1, animated: true, completion: nil)
            
            
        }))
        acAlert.addAction(UIAlertAction(title:NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
        present(acAlert, animated: true, completion: nil)
    }
    
    @IBAction func sizeGuideAction(_ sender: Any) {
//        pushVC("FILTER_SIZEGUIDE_VIEW", storyboard: "Top", animated: true)
        
        gCategoryUid = dicPdtInfo.categoryList[0].categoryUid
        if gCategoryUid == 4 && dicPdtInfo.categoryList.count > 1 {
            if dicPdtInfo.categoryList[1].categoryUid == 15 || dicPdtInfo.categoryList[1].categoryUid == 19 {
                gCategoryUid = dicPdtInfo.categoryList[1].categoryUid //베이비 이거나 틴
            }
        }
        
        let storyboard = UIStoryboard(name: "Top", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FILTER_SIZEGUIDE_VIEW")
        self.present(controller, animated: true, completion: nil)
    }
    
    //하단 좋아요
    @IBAction func likeAction(_ sender: Any) {
        
        if dicPdtInfo.pdtLikeStatus {
            delPdtLike(_uid: pdtUid)
        } else {
            setPdtLike(_uid: pdtUid)
        }
    }
    
    //하단 공유페지로
    @IBAction func ittemAction(_ sender: Any) {
        
        IttemSelect.show(self) { (type) in
            self.goshare(index: type)
        }
    }
    
    @IBAction func commentAction(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_COMMENT_VIEW")) as! DetailCommentViewController
        vc.pdtUid = pdtUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //스타일 등록
    @IBAction func styleAction(_ sender: Any) {
        StyleRegister.show(self, photoCallback: {
            
            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
            vc.m_bFlag = true
            vc.isEdit = false
            vc.pdtUid = self.pdtUid
            self.navigationController?.pushViewController(vc, animated: true)
            
        }, albumCallback: {
            PhotoGallery.show(self) { (photo) in
                
                guard photo.count > 0 else {
                    return
                }
                
                let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
                vc.m_bFlag = true
                vc.isEdit = false
                vc.pdtUid = self.pdtUid
                vc.assetsList = self.getImageFromAsset(assetsList: photo)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    //네고
    @IBAction func negoAction(_ sender: Any) {
        
        if gMeInfo.usrUid == dicPdtInfo.pdt.usrUid {  //  등록한 유저
            //삭제
            
            let alertController = UIAlertController(title: NSLocalizedString("alarm", comment:""), message: NSLocalizedString("delete_alert_msg", comment:""), preferredStyle: .alert)
            let actOK = UIAlertAction(title: NSLocalizedString("confirm", comment:""), style: .default) { (action:UIAlertAction) in
                self.delPdtInfo()
            }
            
            let actCancel = UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(actOK)
            alertController.addAction(actCancel)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            if dicPdtInfo.pdt.status != 1 { //판매중 상태가 아닌 다른 상태이면 리턴
                MsgUtil.showUIAlert(NSLocalizedString("pdt_detail_alert1", comment:""))
                return
            }
            //네고
            NegoPopup.show(self, dicPdtInfo: dicPdtInfo) { (price) in
                //체크아웃 페이지로
                let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_NEGO_CHECKOUT_VIEW")) as! DetailNegoCheckoutViewController
                vc.dicPdtInfo = self.dicPdtInfo
                vc.reqPrice = price
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    //구매
    @IBAction func buyAction(_ sender: Any) {
        
        if gMeInfo.usrUid == dicPdtInfo.pdt.usrUid {  //  등록한 유저
            //수정
            if dicPdtInfo.pdt.status == 0 || dicPdtInfo.pdt.status == 2 { //
                MsgUtil.showUIAlert(NSLocalizedString("pdt_detail_alert2", comment:""))
                return
            }
            //네고 비허용
            if dicPdtInfo.pdt.negoYn == 0 {
                MsgUtil.showUIAlert(NSLocalizedString("pdt_nego_disable", comment:""))
                return
            }
            addObserver()
            let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_EDIT_VIEW")) as! DetailEditViewController
            vc.dicPdtInfo = self.dicPdtInfo
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            //구매
            if dicPdtInfo.pdt.status != 1 { //판매중 상태가 아닌 다른 상태이면 리턴
                MsgUtil.showUIAlert(NSLocalizedString("pdt_detail_alert1", comment:""))
                return
            }
            
            let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_BUY_VIEW")) as! DetailBuyViewController
            vc.dicPdtInfo = self.dicPdtInfo
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    //좋아요 유저목록
    @IBAction func cLikeAction(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_LIKEUSER_VIEW")) as! DetailLikeUserViewController
        vc.pdtUid = pdtUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    //잇템
    @IBAction func cIttemAction(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_ITTEMUSER_VIEW")) as! DetailIttemUserViewController
        vc.pdtUid = pdtUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //댓글페지로
    @IBAction func cCommentAction(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_COMMENT_VIEW")) as! DetailCommentViewController
        vc.pdtUid = pdtUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func userAction(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicPdtInfo.seller.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func userFollowAction(_ sender: Any) {
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
        vc.following = false
        vc.usrUid = dicPdtInfo.seller.usrUid
        vc.topName = dicPdtInfo.seller.usrNckNm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func userLikeAction(_ sender: Any) {

        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserLikeViewController = (storyboard.instantiateViewController(withIdentifier: "USER_LIKE_VIEW") as! UserLikeViewController)
        vc.usrUid = dicPdtInfo.seller.usrUid
        vc.point = dicPdtInfo.seller.point
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func userItemAction(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicPdtInfo.seller.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //기타정보(무료배송, 최상, 더스트백)
    @IBAction func infoAction(_ sender: UIButton) {
        if sender.tag == 0 {
            view.makeToast("배송비는 " + CommonUtil.formatNum(dicPdtInfo.pdt.sendPrice) + "원 입니다.")
        } else if sender.tag == 1 {
            if dicPdtInfo.pdt.pdtCondition == 1 {
                view.makeToast("현재 상품은 " + NSLocalizedString("new_product", comment:"") + "입니다.")
            } else if dicPdtInfo.pdt.pdtCondition == 1 {
                view.makeToast("현재 상품은 " + NSLocalizedString("best_product", comment:"") + "입니다.")
            } else if dicPdtInfo.pdt.pdtCondition == 1 {
                view.makeToast("현재 상품은 " + NSLocalizedString("top_product", comment:"") + "입니다.")
            } else {
                view.makeToast("현재 상품은 " + NSLocalizedString("middle_product", comment:"") + "입니다.")
            }
            
        } else {
            var maketext : String = ""
            let component : String = dicPdtInfo.pdt.component
            
            let btnIdx = sender.tag - 2
            let arrCompCharacter = Array(component)
            let arrCompToast = ["component1", "component2", "component3", "component4", "component5", "component6"]
            var compLen = 0
            for nInd in 0 ..< arrCompCharacter.count  {
                let val = Int(String(arrCompCharacter[nInd]))
                
                if val == 1 {
                   if(compLen == btnIdx) {
                      maketext = String.init(format: "%@ %@", maketext, NSLocalizedString(arrCompToast[nInd], comment:""))
                   }
                   compLen = compLen + 1
                }
            }
            
            if dicPdtInfo.pdt.etc != "" && maketext == "" {
                maketext = dicPdtInfo.pdt.etc
            }

            view.makeToast("현재 상품은 " + maketext +  "로 되어있습니다.")
        }
    }
    
    //연관스타일
    @IBAction func connectStyleAction(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_STYLE_VIEW")) as! DetailStyleViewController
        vc.pdtUid = pdtUid
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //스타일 등록
    @IBAction func saveStyleAction(_ sender: Any) {
//        pushVC("DETAIL_STYLE_VIEW", storyboard: "Detail", animated: true)
        StyleRegister.show(self, photoCallback: {
            
            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
            vc.m_bFlag = true
            vc.isEdit = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }, albumCallback: {
            PhotoGallery.show(self) { (photo) in
                
                guard photo.count > 0 else {
                    return
                }
                
                let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
                vc.m_bFlag = true
                vc.isEdit = false
                vc.assetsList = self.getImageFromAsset(assetsList: photo)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    @IBAction func protectAction(_ sender: Any) {
        SellProtect.show(self)
    }
    
    @IBAction func comInfoAction(_ sender: UIButton) {
//        let name = ["여성", "여성 핸드백", "여성 토트백", "샤넬", "블랙", "캐비어", "인생템"]
        
        if sender.tag == 4 || sender.tag == 5 {
            
            let storyboard = UIStoryboard(name: "Brand", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
            controller.barndUid = dicPdtInfo.brand.brandUid
            self.navigationController?.pushViewController(controller, animated: true)

        } else if sender.tag == 6 || sender.tag == 7 {
            let nav : UINavigationController! = self.navigationController
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Detail", bundle: nil)
            
            let vc = (storyboard.instantiateViewController(withIdentifier: "DETAIL_STYLE_VIEW")) as! DetailStyleViewController
            if sender.tag == 5 {
                vc.pageTitle = dicPdtInfo.pdt.pdtModel
            } else {
                vc.pageTitle = dicPdtInfo.pdt.tag
            }
            vc.pdtUid = pdtUid
            nav.pushViewController(vc, animated: true)
            
        } else {
            let nav : UINavigationController! = self.navigationController
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "Shop", bundle: nil)
            
            let vc = (storyboard.instantiateViewController(withIdentifier: "SHOP_DETAIL_VIEW")) as! ShopDetailViewController
            
            if sender.tag == 0 {
                vc.pageTitle = pdtlikeGroup
                vc.nCategoryUid = dicPdtInfo.pdt.pdtGroup
            } else if sender.tag == 1 {
                
                if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
                    vc.pageTitle = dicPdtInfo.categoryList[1].categoryName
                } else {
                    vc.pageTitle = dicPdtInfo.categoryList[0].categoryName + " " + dicPdtInfo.categoryList[1].categoryName
                }
                vc.nCategoryUid = dicPdtInfo.categoryList[1].categoryUid
            } else if sender.tag == 2 {
                if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
                    vc.pageTitle = dicPdtInfo.categoryList[1].categoryName + " " + dicPdtInfo.categoryList[2].categoryName
                } else {
                    vc.pageTitle = dicPdtInfo.categoryList[0].categoryName + " " + dicPdtInfo.categoryList[2].categoryName
                }
                vc.nCategoryUid = dicPdtInfo.categoryList[2].categoryUid
            } else {
                if dicPdtInfo.pdt.pdtGroup & 4 != 0 {
                    vc.pageTitle = dicPdtInfo.categoryList[2].categoryName + " " + dicPdtInfo.categoryList[3].categoryName
                } else {
                    vc.pageTitle = dicPdtInfo.categoryList[0].categoryName + " " + dicPdtInfo.categoryList[3].categoryName
                }
                vc.nCategoryUid = dicPdtInfo.categoryList[3].categoryUid
            }
            
            nav.pushViewController(vc, animated: true)
        }
    }
    
    func showInteractive(_uid : Int) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = _uid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dicPdtInfo == nil {
            return 0
        } else {
            return dicPdtInfo.pdtStyleList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StyleCell", for: indexPath as IndexPath) as! StyleCell
        
        let dic : PdtStyleDto  = dicPdtInfo.pdtStyleList[indexPath.row]
        
        cell.imgBanner.kf.setImage(with: URL(string: dic.pdtStyle.styleImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        cell.ivProfile.kf.setImage(with: URL(string: dic.usr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell.lbNickname.text = dic.usr.usrNckNm
        
        if dic.pdtStyle.usrUid == gMeInfo.usrUid {
            cell.btnClose.isHidden = false
        } else {
            cell.btnClose.isHidden = true
        }
        
        cell.btnClose.tag = indexPath.row
        cell.btnClose.addTarget(self, action:#selector(self.onBtnTClose(_:)), for: UIControlEvents.touchUpInside)
        cell.btnUser.tag = indexPath.row
        cell.btnUser.addTarget(self, action:#selector(self.onBtnTUser(_:)), for: UIControlEvents.touchUpInside)
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action:#selector(self.onBtnTPopup(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dic : PdtStyleDto  = dicPdtInfo.pdtStyleList[indexPath.row]
        let width = UIScreen.main.bounds.size.width
        var height : CGFloat = 0
        var ratio : CGFloat = 0

        if dic.styleProfileImgWidth == nil || dic.styleProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.styleProfileImgHeight) / CGFloat(dic.styleProfileImgWidth)
            height = width * ratio
        }

        return height
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    // CollectionView
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var _uid : Int = 0
        if collectionView == clvCom {
            let cellCom = collectionView.cellForItem(at: indexPath) as! T1CollectionViewCell
            
            let dic : PdtInfoDto = dicPdtInfo.sellerPdtList[indexPath.row]
            _uid = dic.pdtUid
            
            self.smallImageView = cellCom.imgCom
        } else {
            let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC

            let dic : PdtListDto = arrPdtList[indexPath.row]
            
            _uid = dic.pdt.pdtUid
            self.smallImageView = cell._imgCom
        }
        
        m_bFlag = true
        self.showInteractive(_uid: _uid)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if collectionView == clvCom {
            return CGSize(width: (collectionView.bounds.size.width) / 3, height: (collectionView.bounds.size.width) / 3 * 150 / 120);
        } else {

            let width = (collectionView.bounds.width - 30) / 2
            var height : CGFloat = 0
            var ratio : CGFloat = 0
            //pdt
            
            let dic : PdtListDto = arrPdtList[indexPath.row]
            
            if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
                height = width * ratio
            }
            
            if dic.pdt.originPrice <= dic.pdt.price {
                height = height + 113
            } else {
                height = height + 133
            }
            
            return CGSize(width: width, height: height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == clvCom {
            if dicPdtInfo == nil {
                return 0
            }
            return dicPdtInfo.sellerPdtList.count
        } else {
            return arrPdtList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //판매자정보 상품 리스트
        if collectionView == clvCom {
            let cell = clvCom.dequeueReusableCell(withReuseIdentifier: "T1CollectionViewCell", for: indexPath as IndexPath) as! T1CollectionViewCell
            
            let dic : PdtInfoDto = dicPdtInfo.sellerPdtList[indexPath.row]
            cell.imgCom.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell.lbBrandName.text = dic.brandName + " " + dic.categoryName
            cell.lbPrice.text = CommonUtil.formatNum(dic.price)
            
            return cell
        } else {
            //추천상품 리스트
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC

            htCollectionCell.constant = clvStyle.contentSize.height
            
            let dic : PdtListDto = arrPdtList[indexPath.row]
            let dicBrand        : BrandMiniDto      = dic.brand  //브랜드
            let dicCategory     : CategoryMiniDto    = dic.category  //카테고리
            let dicPdt          : PdtMiniDto         = dic.pdt  //상품
            let likeCnt         : Int               = dic.pdtLikeCount  //상품좋아요 갯수
            let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
            let dicUsr          : UsrMiniDto         = dic.usr  //등록유저
            let blikeStatus     : Bool              = dic.pdtLikeStatus  //유저 상품좋아요 상태
            
            //pdt
            cell._btnStyle.isHidden = false
            cell._imgEffect.isHidden = false
            cell._imgCom.kf.setImage(with: URL(string: dicPdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            cell._btnStyle.kf.setImage(with: URL(string: dicPdtStyle.styleImg), for: .normal, placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

            let width = (collectionView.bounds.width - 30) / 2
            var height : CGFloat = 0
            var ratio : CGFloat = 0
            //pdt
            if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
                height = width
            } else {
                ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
                height = width * ratio
            }
            
            if dicPdt.originPrice <= dicPdt.price {
                cell.hiddenDown(price: dicPdt.price)
            } else {
                cell.showDown(origin: dicPdt.originPrice, price: dicPdt.price)
            }
            cell.lbSize.text = dicPdt.pdtSize
            
            cell._imgSoldOut.isHidden = dicPdt.status == 2 ? false : true //판매 완료 상태체크
            
            cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dicBrand.nameKo, dicPdt.colorName, dicPdt.pdtModel, dicCategory.categoryName)
            cell._lbBrand.text = dicBrand.nameEn
            cell._lbLike.text = String(likeCnt)
            cell._imgHeart.image = UIImage(named: blikeStatus ? "ic_like_hart" : "ic_dislike_hart")
            //        cell._imgComWidth.constant = width
            cell._imgComHeight.constant = height
            
            //user
            cell._lbName.text = dicUsr.usrNckNm
            cell._imgProfile.kf.setImage(with: URL(string: dicUsr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
            cell._lbTime.text = CommonUtil.diffTime(dicPdt.edtTime)
            
            cell._btnStyle.tag = indexPath.row
            cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
            cell._btnLike.withValue = ["index" : indexPath]
            cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
            cell._btnBrand.tag = indexPath.row
            cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
            cell._btnUser.tag = indexPath.row
            cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
            
            if indexPath.row == arrPdtList.count - 1 && !isLast {
                nPage = nPage + 1
                getRecommendPdtList()
            }
            
            return cell
        }
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(toPdtDel), name: NSNotification.Name(rawValue: NOTI_PDT_DEL), object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTI_PDT_DEL), object: nil)
    }
    
    @objc func toPdtDel(_ notification: NSNotification) {
        removeObserver()
        popVC()
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 제품 상세 정보 얻기
    func getPdtDetailInfo() {
        
        gProgress.show()
        Net.getPdtDetailInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getPdtDetailInfoResult
                self.getPdtDetailInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //제품 상세 정보 얻기 결과
    func getPdtDetailInfoResult(_ data: Net.getPdtDetailInfoResult) {
        
        dicPdtInfo = data.dicInfo
        setPdtData()
        
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: 0.5)
            DispatchQueue.main.async {
                self.getRecommendPdtList()
            }
        }
        
    }
    
    // 상품 삭제
    func delPdtInfo() {
        
        gProgress.show()
        Net.delPdtInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("pdt_delete_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 스타일 삭제
    func delPdtStyleInfo(pdtStyleUid : Int) {
        
        gProgress.show()
        Net.delPdtStyle(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("style_delete_success", comment:""))
                self.getPdtDetailInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 추천상품 목록 얻기
    func getRecommendPdtList() {
        
        gProgress.show()
        Net.getRecommendPdtList(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getRecommendPdtListResult
                self.getRecommendPdtListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //추천상품 목록 얻기 결과
    func getRecommendPdtListResult(_ data: Net.getRecommendPdtListResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrPdtList.append(dic)
        }
        clvStyle.reloadData()
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getPdtDetailInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getPdtDetailInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, pdtUid: pdtUid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, pdtUid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            vc.pdtUid = pdtUid
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // 상품잇템 추가하기
    func addPdtitem() {
        
        gProgress.show()
        Net.addPdtItem(
            accessToken     : gMeInfo.token,
            pdtUid          : pdtUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                if !self.iitemsStatus {
                    self.dicPdtInfo.pdtWishCount =  self.dicPdtInfo.pdtWishCount! + 1
                    self.lbIttem.text = String(self.dicPdtInfo.pdtWishCount)
                    self.lbIttem1.text = String(self.dicPdtInfo.pdtWishCount)
                    self.imgIttem.image = UIImage.init(named: "ic_detail_ittem_on")
                    self.iitemsStatus = true
                }
                
                CommonUtil.showToast(NSLocalizedString("add_item_success", comment:""))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 신고 하기
    func reqReport(_uid : Int, kind : Int, type : String, content : String) {
        
        gProgress.show()
        Net.reqReport(
            accessToken     : gMeInfo.token,
            targetUid       : _uid,
            kind            : kind,
            content         : content,
            reportType      : type,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("report_submist_success", comment:""))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension DetailViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
        case scvBanner:
            let newOffset = scrollView.contentOffset.x
            let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % imgList.count
            
            for subview in stvIndicator.subviews {
                let ivPageNum = subview as! UIImageView
                ivPageNum.image = ivPageNum.tag == pageNum ? #imageLiteral(resourceName: "ic_shop_banner_indicator_active") : #imageLiteral(resourceName: "ic_shop_banner_indicator_inactive")
            }
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case scvMain:
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }
            
            scrollPos = curScrollPos
            
        default:
            return
        }
    }
}

// MARK: - ZoomTransitionSourceDelegate

extension DetailViewController: ZoomTransitionSourceDelegate {
    
    func transitionSourceImageView() -> UIImageView {
        return smallImageView
    }
    
    func transitionSourceImageViewFrame(forward: Bool) -> CGRect {
        return smallImageView.convert(smallImageView.bounds, to: view)
    }
    
    func transitionSourceWillBegin() {
        smallImageView.isHidden = true
    }
    
    func transitionSourceDidEnd() {
        smallImageView.isHidden = false
    }
    
    func transitionSourceDidCancel() {
        smallImageView.isHidden = false
    }
}


// MARK: - ZoomTransitionDestinationDelegate

extension DetailViewController: ZoomTransitionDestinationDelegate {
    
    func transitionDestinationImageViewFrame(forward: Bool) -> CGRect {
        if let image = imgPhoto.image {
            imgPhoto.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            let frame = AVMakeRect(aspectRatio: imgPhoto.frame.size, insideRect: vwImage.frame)
            imgPhoto.frame = frame
        }
        if forward {
//            let x =  0
//            let y = 48
//            let width = imgPhoto.frame.width
//            let height = imgPhoto.frame.height
//            return CGRect(x: CGFloat(x), y: CGFloat(y), width: width, height: height)
            return imgPhoto.frame
        } else {
            return imgPhoto.convert(imgPhoto.bounds, to: view)
        }
    }
    
    func transitionDestinationWillBegin(transitioningImageView imageView: UIImageView) {
        imgPhoto.isHidden = true
        imgPhoto.image = imageView.image
    }
    
    func transitionDestinationDidEnd(transitioningImageView imageView: UIImageView) {
        imgPhoto.isHidden = false
        imgPhoto.image = imageView.image
    }
    
    func transitionDestinationDidCancel() {
        imgPhoto.isHidden = false
    }
}





