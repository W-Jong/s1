//
//  DetailEditViewController.swift
//  selluv
//  상품 수정
//  Created by Comet on 1/8/18.
//  modified by PJH on 19/03/18.

import UIKit

class DetailEditViewController: BaseViewController {

    @IBOutlet var clvPhoto: UICollectionView!
    // 판매가격 관련 변수들
    @IBOutlet weak var _lbItemPrice: UILabel!
    @IBOutlet weak var _lbShippingCharge: UILabel!
    
    // 필수 정보 관련 변수들
    @IBOutlet weak var _lbSize: UILabel!
    @IBOutlet weak var _lbCondition: UILabel!
    
    // 선택 정보 관련 변수들
    @IBOutlet weak var _lbTag: UILabel!
    @IBOutlet weak var _lbAccessories: UILabel!
    @IBOutlet weak var _lbColor: UILabel!
    @IBOutlet weak var _lbModel: UILabel!
    @IBOutlet weak var _lbDescription: UILabel!
    
    @IBOutlet var swSelling: UISwitch!
    @IBOutlet var swNego: UISwitch!
    //판매 가격 관련 변수들
    var m_negoAllow : Bool = true
    var m_isSelling : Bool = true
    var dicSizeInfo     : SizeRefDto!
    var dicPdtInfo  : PdtDetailDto!
    
    var colorname       : String = ""
    var pdtModel        : String = ""
    var pdtSize         : String = ""
    var pdtComponent    : String = ""
    var pdtComponentInxexs   : String = ""
    var pdtEtc          : String = ""
    var pdtContent      : String = ""
    var pdtCondition    : String = ""
    var pdtTag          : String = ""
    var promotionCode   : String = ""
    var arrPhotos       : Array<String> = []
    var arrPhotosFiles  : Array<String> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initUI()
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
    }
    
    func initData() {
        _lbItemPrice.text = CommonUtil.formatNum(dicPdtInfo.pdt.price)
        _lbShippingCharge.text = CommonUtil.formatNum(dicPdtInfo.pdt.sendPrice)
        _lbSize.text = dicPdtInfo.pdt.pdtSize
        pdtSize  = dicPdtInfo.pdt.pdtSize
        switch dicPdtInfo.pdt.pdtCondition {
        case 1:
           _lbCondition.text = NSLocalizedString("new_product", comment:"")
            break
        case 2:
            _lbCondition.text = NSLocalizedString("best_product", comment:"")
            break
        case 3:
            _lbCondition.text = NSLocalizedString("top_product", comment:"")
            break
        case 4:
            _lbCondition.text = NSLocalizedString("middle_product", comment:"")
            break
        default:
            break
        }
        pdtCondition = _lbCondition.text!
        _lbTag.text = dicPdtInfo.pdt.tag
        pdtTag = dicPdtInfo.pdt.tag
        
        //부속품
        var maketext : String = ""
        let component : String = dicPdtInfo.pdt.component
        pdtComponentInxexs = dicPdtInfo.pdt.component
        let str1 =  component.substring(with: component.index(component.startIndex, offsetBy: 0)..<component.index(component.startIndex, offsetBy: 1))
        let str2 =  component.substring(with: component.index(component.startIndex, offsetBy: 1)..<component.index(component.startIndex, offsetBy: 2))
        let str3 =  component.substring(with: component.index(component.startIndex, offsetBy: 2)..<component.index(component.startIndex, offsetBy: 3))
        let str4 =  component.substring(with: component.index(component.startIndex, offsetBy: 3)..<component.index(component.startIndex, offsetBy: 4))
        let str5 =  component.substring(with: component.index(component.startIndex, offsetBy: 4)..<component.index(component.startIndex, offsetBy: 5))
        let str6 =  component.substring(with: component.index(component.startIndex, offsetBy: 5)..<component.index(component.startIndex, offsetBy: 6))
        
        if str1 == "1" {
            maketext = String.init(format: "%@ %@", maketext, NSLocalizedString("component1", comment:""))
        }
        
        if str2 == "1" {
            maketext = String.init(format: "%@ %@", maketext, NSLocalizedString("component2", comment:""))
        }
        
        if str3 == "1" {
            maketext = String.init(format: "%@ %@", maketext, NSLocalizedString("component3", comment:""))
        }
        
        if str4 == "1" {
            maketext = String.init(format: "%@ %@", maketext, NSLocalizedString("component4", comment:""))
        }
        
        if str5 == "1" {
            maketext = String.init(format: "%@ %@", maketext, NSLocalizedString("component5", comment:""))
        }
        
        if str6 == "1" {
            maketext = String.init(format: "%@ %@", maketext, NSLocalizedString("component6", comment:""))
        }
        pdtComponent = maketext
        pdtEtc = dicPdtInfo.pdt.etc
        _lbAccessories.text = maketext + pdtEtc
        
        _lbColor.text = dicPdtInfo.pdt.colorName
        colorname = dicPdtInfo.pdt.colorName
        _lbModel.text = dicPdtInfo.pdt.pdtModel
        pdtModel = dicPdtInfo.pdt.pdtModel
        _lbDescription.text = dicPdtInfo.pdt.content
        pdtContent = dicPdtInfo.pdt.content
       
        if dicPdtInfo.pdt.negoYn == 1 {
            m_negoAllow = true
        } else {
            m_negoAllow = false
        }
        swNego.setOn(m_negoAllow, animated: false)
        
        if dicPdtInfo.pdt.status == 1 {
            m_isSelling = true
        } else {
            m_isSelling = false
        }
        swSelling.setOn(m_isSelling, animated: false)
        
        arrPhotos = dicPdtInfo.pdt.photoList
        for _ in 0..<dicPdtInfo.pdt.photoList.count {
            arrPhotosFiles.append("")
        }
        
        if dicPdtInfo.pdt.negoYn == 1 {
            swNego.isOn = true
        } else {
            swNego.isOn = false
        }
        
        if dicPdtInfo.pdt.status == 1 {
            swSelling.isOn = true
        } else {
            swSelling.isOn = false
        }
        
        getSizeList()
    }
    
    // 상품 사진 관련 함수들
    @IBAction func onPhotoEdit(_ sender: Any) {
        var arrImgs : [UIImage] = []
        for i in 0..<arrPhotos.count {
            let img = UIImageView()
            img.kf.setImage(with: URL(string: dicPdtInfo.pdt.photoList[i]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            arrImgs.append(img.image!)
        }
        
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "CAMERA_VIEW")) as! SellCameraViewController
        vc.m_bFlag = true
        vc.isEdit = true
        vc.delegate = self
        vc.assetsList = arrImgs
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // 판재 가격 관련 함수들
    @IBAction func onItemPrice(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_COST_VIEW")) as! CostSetViewController
        vc.dicPdtInfo = dicPdtInfo
        vc.isEdit = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onShippingCharge(_ sender: Any) {
        TransCostSet.show(self, cost: Double((_lbShippingCharge.text?.replacingOccurrences(of: ",", with: ""))!)!) { (cost) in
            self._lbShippingCharge.text = CommonUtil.formatNum(cost)
        }
    }
    
    // 필수 정보 관련 함수들
    @IBAction func onSize(_ sender: Any) {

        if dicSizeInfo.sizeType == 1 || dicSizeInfo.sizeType == 2 {  // 가로x세로x폭, FREE SIZE && 직접입력
            let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_INFO_VIEW")) as! SellProductInfoViewController
            vc.sizeType = dicSizeInfo.sizeType
            vc.pdtSize = pdtSize
            vc.isEdit = true
            vc.PageStatus = "SIZE"
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        } else { //일반
            SizeSelect.show(self, dicSizeInfo: dicSizeInfo) { (size) in
                self._lbSize.text = size
            }
        }
    }
    
    @IBAction func onCondition(_ sender: Any) {
        // condition페지 련동.
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_INFO_VIEW")) as! SellProductInfoViewController
        vc.pdtCondition = pdtCondition
        vc.isEdit = true
        vc.PageStatus = "CONDITION"
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // 선택 정보 관련 함수들
    @IBAction func onTag(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_INFO_VIEW")) as! SellProductInfoViewController
        vc.pdtTag = pdtTag
        vc.isEdit = true
        vc.PageStatus = "TAG"
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onAccessories(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_SUB_INFO_VIEW")) as! SellProductSubInfoViewController
        vc.isEdit = true
        vc.pdtAccr = pdtComponent
        vc.pdtEtc  = pdtEtc
        vc.PageStatus = "ACCR"
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onColor(_ sender: Any) {
        ColorSelect.show(self, colorname: colorname) { (size) in
            self._lbColor.text = size
            self.colorname = size
        }
    }
    
    @IBAction func onModel(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_SUB_INFO_VIEW")) as! SellProductSubInfoViewController
        vc.isEdit = true
        vc.pdtModel = pdtModel
        vc.brandUid = dicPdtInfo.brand.brandUid
        vc.PageStatus = "MODEL"
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onDescription(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SELL_PRODUCT_SUB_INFO_VIEW")) as! SellProductSubInfoViewController
        vc.isEdit = true
        vc.pdtContent = pdtContent
        vc.PageStatus = "DESC"
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // 판재 설정 관련 함수들
    @IBAction func onNegoSet(_ sender: UISwitch) {
        swNego.isOn = !swNego.isOn
    }
    
    @IBAction func onStatusSet(_ sender: UISwitch) {
        swSelling.isOn = !swSelling.isOn
    }
    
    // 상품 삭제 관련 함수들
    @IBAction func onItemDel(_ sender: Any) {
        let alertController = UIAlertController(title: NSLocalizedString("alarm", comment:""), message: NSLocalizedString("delete_alert_msg", comment:""), preferredStyle: .alert)
        let actOK = UIAlertAction(title: NSLocalizedString("confirm", comment:""), style: .default) { (action:UIAlertAction) in
            self.delPdtInfo()
        }
        
        let actCancel = UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .default) { (action:UIAlertAction) in
            
        }
        
        alertController.addAction(actOK)
        alertController.addAction(actCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any) {
        popVC()
    }
    
    //수정하기
    @IBAction func onModify(_ sender: Any) {
        reqpdtModify()
    }
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 상품 삭제
    func delPdtInfo() {
        
        gProgress.show()
        Net.delPdtInfo(
            accessToken     : gMeInfo.token,
            pdtUid          : dicPdtInfo.pdt.pdtUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("pdt_delete_success", comment:""))
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_PDT_DEL), object: nil, userInfo: nil)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 사이즈 목록 얻기
    func getSizeList() {
        
        gProgress.show()
        Net.getSizeList(
            accessToken     : gMeInfo.token,
            categoryUid     : dicPdtInfo.pdt.categoryUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSizeListResult
                self.getSizeListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //사이즈 목록 얻기 결과
    func getSizeListResult(_ data: Net.getSizeListResult) {
        dicSizeInfo = data.dicInfo
    }
    
    // 상품 정보 수정
    func reqpdtModify() {
        
        let str = _lbItemPrice.text?.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range:nil)
        var price = 0
        if str != "" {
            price = Int(str!)!
        }
        
        var condition = ""
        if pdtCondition == NSLocalizedString("new_product", comment:"") {
            condition = "NEW"
        } else if pdtCondition == NSLocalizedString("best_product", comment:"") {
            condition = "BEST"
        } else if pdtCondition == NSLocalizedString("top_product", comment:"") {
            condition = "GOOD"
        } else if pdtCondition == NSLocalizedString("middle_product", comment:"") {
            condition = "MIDDLE"
        }
        
        var sendPrice : Int = 0
        if (_lbShippingCharge.text?.isEmpty)! {
            sendPrice = 0
        } else {
            let temp = _lbShippingCharge.text?.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range:nil)
            sendPrice = Int(temp!)!
        }
        
        gProgress.show()
        Net.reqpdtModify(
            accessToken     : gMeInfo.token,
            pdtUid          : dicPdtInfo.pdt.pdtUid,
            component       : pdtComponentInxexs,
            content         : pdtContent,
            etc             : pdtEtc,
            negoYn          : swNego.isOn ? 1 : 2,
            pdtColor        : colorname,
            pdtCondition    : condition,
            pdtModel        : pdtModel,
            pdtSize         : pdtSize,
            photos          : arrPhotosFiles,
            price           : price,
            promotionCode   : promotionCode,
            sell            : swSelling.isOn ? true : false,
            sendPrice       : sendPrice,
            tag             : pdtTag,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("pdt_modify_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension DetailEditViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Create the cell and return the cell

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PdtCollectionViewCell
   
        let img = arrPhotos[indexPath.row]
        cell.ivPdtThumb.kf.setImage(with: URL(string: img), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension DetailEditViewController: SellProductInfoViewControllerDelegate {
    func setSize( size : String){
        if size == "" {
            self._lbSize.text = dicPdtInfo.pdt.pdtSize
            self.pdtSize = dicPdtInfo.pdt.pdtSize
        } else {
            self._lbSize.text = size
            self.pdtSize = size
        }
    }
    func setTag( tag : String){
        if tag == "" {
            self._lbTag.text = dicPdtInfo.pdt.tag
            self.pdtTag = dicPdtInfo.pdt.tag
        } else {
            self._lbTag.text = tag
            self.pdtTag = tag
        }
    }
    
    func setCondition( condition : String){
        if condition == "" {
            self._lbCondition.text = pdtCondition
        } else {
            self._lbCondition.text = condition
            self.pdtCondition = condition
        }
    }
}

extension DetailEditViewController: SellCameraViewControllerDelegate {
    func setPhotos( photos : Array<String>, photosFiles : Array<String>){
        arrPhotos = photos
        arrPhotosFiles = photosFiles
        clvPhoto.reloadData()
    }
}

extension DetailEditViewController: CostSetViewControllerDelegate {
    func setValue(salePrice: Double, sendPrice: Double, nagoYn: Bool, code: String) {
        _lbItemPrice.text = CommonUtil.formatNum(salePrice)
        _lbShippingCharge.text = CommonUtil.formatNum(sendPrice)
        if nagoYn {
            swNego.isOn = true
        } else {
            swNego.isOn = false
        }
        promotionCode = code
    }
}

extension DetailEditViewController: SellProductSubInfoViewControllerDelegate {
    func setModel( model : String){
        if model == "" {
            _lbModel.text = pdtModel
        } else {
            pdtModel = model
            _lbModel.text = model
        }
    }
    
    func setAcc( acc : String, etc : String, arrIndexs : String){
        if acc == "" {
            _lbAccessories.text = pdtComponent + pdtEtc
        } else {
            pdtComponent = acc
            pdtEtc = etc
            _lbAccessories.text = acc + etc
            pdtComponentInxexs = arrIndexs
        }
    }
    
    func setDesc( desc : String){
        if desc == "" {
            _lbDescription.text = pdtContent
        } else {
            pdtContent = desc
            _lbDescription.text = desc
        }
    }
}
