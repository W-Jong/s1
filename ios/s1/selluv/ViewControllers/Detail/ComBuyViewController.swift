//
//  ComBuyViewController.swift
//  selluv
//  주문완료
//  Created by World on 1/23/18.
//  modified by PJH on 17/03/18.

import UIKit

class ComBuyViewController: BaseViewController {

    @IBOutlet weak internal var scvList: UIScrollView!
    @IBOutlet weak internal var vwNormalLine: UIView!
    @IBOutlet weak internal var vwMostLine: UIView!
//    @IBOutlet weak internal var htNormal: NSLayoutConstraint!
    @IBOutlet weak internal var htMost: NSLayoutConstraint!
    @IBOutlet weak internal var btnNormal: UIButton!
    @IBOutlet weak internal var btnMost: UIButton!
    @IBOutlet weak internal var imgNormal: UIImageView!
    @IBOutlet weak internal var imgMost: UIImageView!
    
    // 금액 상세 관련
    @IBOutlet weak var _lbPaymentMoney: UILabel!
    @IBOutlet weak var _cstrPaymentAmountDetail: NSLayoutConstraint! // 227
    @IBOutlet weak var _cstrPaymentAmountDetail1: NSLayoutConstraint! //212
    @IBOutlet weak var _cstrPaymentAmountDetail3: NSLayoutConstraint! //154
    @IBOutlet weak var vwJejuArea: UIView!
    @IBOutlet weak var vwGitaArea: UIView!
    @IBOutlet weak var vwPromotion: UIView!
    @IBOutlet weak var vwSelluvMoney: UIView!

    @IBOutlet weak var _lbCostItem: UILabel!
    @IBOutlet weak var _lbCostShip: UILabel!
    @IBOutlet weak var _lbCostAppraisal: UILabel!
    @IBOutlet weak var _lbCostPromotionDiscount: UILabel!
    @IBOutlet weak var _lbCostSelluvMoneyUse: UILabel!
    @IBOutlet weak var _lbPaymentMoneySum: UILabel!
    
    //user info
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbContact: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    //셀럽머니 적립금
    @IBOutlet weak var _lbSelluvMoney: UILabel!
    @IBOutlet weak var _lbBuyMoney: UILabel!
    @IBOutlet weak var _lbLatterMoney: UILabel!
    @IBOutlet weak var _lbSelluvMoneySum: UILabel!
    
    var vwPdtInfoHeight : CGFloat = 0
    
    var dicPdtInfo          : PdtDetailDto!
    var dicPdtBuyInfo       : PdtBuyInfo!
    var dealUid             : Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        setMoneyDetail()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
    }
    
    //굼약 성새 관련
    func setMoneyDetail() {
        var totalmoney : Int = 0
        _lbCostItem.text = CommonUtil.formatNum(dicPdtInfo.pdt.price)
        
        if dicPdtBuyInfo.getAppraisalType() == 0 {
            _lbCostAppraisal.text = NSLocalizedString("free", comment:"")
            totalmoney = dicPdtInfo.pdt.price
        } else {
            _lbCostAppraisal.text = CommonUtil.formatNum(39000)
            totalmoney = dicPdtInfo.pdt.price + 39000
        }
        
        if dicPdtInfo.seller.freeSendPrice == -1 {
            _lbCostShip.text = CommonUtil.formatNum(dicPdtInfo.pdt.sendPrice)
            totalmoney = totalmoney + dicPdtInfo.pdt.sendPrice
        } else {
            _lbCostShip.text =  NSLocalizedString("free", comment:"")
        }
        
        //이제부터 쓰지 않는 뷰들을 22만큼 씩 없앤다.
        vwPdtInfoHeight = 227
        _cstrPaymentAmountDetail.constant = 227
        _cstrPaymentAmountDetail1.constant = 212
        _cstrPaymentAmountDetail3.constant = 154
        
        //도서직역에 있다면
        if dicPdtBuyInfo.getIslandSendPrice() > 0 {
            vwGitaArea.isHidden = false
            dicPdtBuyInfo.setTotalmoney(value: dicPdtBuyInfo.getTotalmoney() + 4000)
            totalmoney = totalmoney + 4000
            dicPdtBuyInfo.setIslandSendPrice(value: 4000)
        } else {
            vwGitaArea.isHidden = true
            _cstrPaymentAmountDetail.constant = 205
            vwPdtInfoHeight = 205
            _cstrPaymentAmountDetail1.constant = 190
            _cstrPaymentAmountDetail3.constant = 132
            dicPdtBuyInfo.setIslandSendPrice(value: 0)
        }
        
        //제주도라면 있다면
        if dicPdtBuyInfo.getJejuAreaSendPrice() > 0 {
            vwJejuArea.isHidden = false
            totalmoney = totalmoney + 3000
            dicPdtBuyInfo.setJejuAreaSendPrice(value: 3000)
        } else {
            vwJejuArea.isHidden = true
            _cstrPaymentAmountDetail.constant = 183
            vwPdtInfoHeight = 183
            _cstrPaymentAmountDetail1.constant = 168
            _cstrPaymentAmountDetail3.constant = 110
            dicPdtBuyInfo.setJejuAreaSendPrice(value: 0)
        }
        
        if dicPdtBuyInfo.getPromotionSale() == 0 {
            _cstrPaymentAmountDetail.constant = 161
            _cstrPaymentAmountDetail1.constant = 146
            _cstrPaymentAmountDetail3.constant = 88
            vwPromotion.isHidden = true
        } else {
            _lbCostPromotionDiscount.text = "- " + CommonUtil.formatNum(dicPdtBuyInfo.getPromotionSale())
            vwPromotion.isHidden = false
            dicPdtBuyInfo.setTotalmoney(value: dicPdtBuyInfo.getTotalmoney() - dicPdtBuyInfo.getPromotionSale())
            totalmoney = totalmoney - dicPdtBuyInfo.getPromotionSale()
        }
        
        if dicPdtBuyInfo.getSelluvMoneyUse() == 0 {
            _cstrPaymentAmountDetail.constant = 139
            vwPdtInfoHeight = 139
            _cstrPaymentAmountDetail1.constant = 124
            _cstrPaymentAmountDetail3.constant = 66
            vwSelluvMoney.isHidden = true
        } else {
            _lbCostSelluvMoneyUse.text = "- " + CommonUtil.formatNum(dicPdtBuyInfo.getSelluvMoneyUse())
            vwSelluvMoney.isHidden = false
            totalmoney = totalmoney - dicPdtBuyInfo.getSelluvMoneyUse()
        }
        
        _lbPaymentMoney.text = CommonUtil.formatNum(totalmoney) + " 원"
        _lbPaymentMoneySum.text = CommonUtil.formatNum(totalmoney)
        dicPdtBuyInfo.setTotalmoney(value: totalmoney)
        
        //user
        lbName.text = dicPdtBuyInfo.getName()
        lbContact.text = dicPdtBuyInfo.getContact()
        lbAddress.text = dicPdtBuyInfo.getAddress()
        
        getSystemSettingInfo(money: totalmoney)
    }
    
    @IBAction func onClickNego(_ sender: Any) {
        dicPdtBuyInfo.initial()
        
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = dealUid
        nav.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickShopping(_ sender: Any) {
        dicPdtBuyInfo.initial()
        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
        
    @IBAction func onClickNormal(_ sender: Any) {
        if btnNormal.isSelected {
            btnNormal.isSelected = false
            _cstrPaymentAmountDetail.constant = 0
            vwNormalLine.isHidden = false
            imgNormal.image = UIImage.init(named: "ic_undo_arrow")
        } else {
            btnNormal.isSelected = true
            _cstrPaymentAmountDetail.constant = vwPdtInfoHeight
            vwNormalLine.isHidden = true
            imgNormal.image = UIImage.init(named: "ic_undo_arrow_rotate")
            scvList.scrollToBottom(isAnimated: true)
        }
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
    }
    
    @IBAction func onClickMost(_ sender: Any) {
        if btnMost.isSelected {
            btnMost.isSelected = false
            htMost.constant = 0
            vwMostLine.isHidden = false
            imgMost.image = UIImage.init(named: "ic_undo_arrow")
        } else {
            btnMost.isSelected = true
            htMost.constant = 120
            vwMostLine.isHidden = true
            imgMost.image = UIImage.init(named: "ic_undo_arrow_rotate")
            scvList.scrollToBottom(isAnimated: true)
        }
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
    }
    
    // 시스템 설정 정보 얻기
    func getSystemSettingInfo( money : Int ){
        
        gProgress.show()
        Net.getSystemSettingInfo(
            accessToken         : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSystemSettingInfoResult
                self.getSystemSettingInfoResult(res, money: money)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //시스템 설정 정보 얻기결과
    func getSystemSettingInfoResult(_ data: Net.getSystemSettingInfoResult, money : Int) {
        
        let dicSysstemInfo : SystemSettingDto = data.dicInfo
        let maxSelluvmoney : Int = (money * dicSysstemInfo.dealReward ) / 100
        let letterMoney : Int = dicSysstemInfo.buyReviewReward
        
        _lbBuyMoney.text = CommonUtil.formatNum(maxSelluvmoney)
        _lbLatterMoney.text = CommonUtil.formatNum(letterMoney)
        _lbSelluvMoney.text = CommonUtil.formatNum(maxSelluvmoney + letterMoney)
        _lbSelluvMoneySum.text = CommonUtil.formatNum(maxSelluvmoney + letterMoney)
    }
    
}
