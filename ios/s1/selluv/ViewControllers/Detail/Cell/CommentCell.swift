//
//  CommentCell.swift
//  selluv
//
//  Created by Comet on 1/8/18.
//  modified by PJH on 15/03/18.

import UIKit
import Material

//protocol CommentCellDelegate {
//    func CommentCellDelegate(_ commentCell : CommentCell, onCommentWrite sender : Any, tag: Int?)
//}

class CommentCell: UITableViewCell {

//    var delegate:  CommentCellDelegate?
    
    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _btnPhoto: UIButton!
    @IBOutlet weak var _lbComment: UILabel!
    @IBOutlet var _tvComment: TextView!
    @IBOutlet weak var _btnReplyTalk: UIButton!
    
    @IBOutlet weak var lbTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _tvComment.contentInset = UIEdgeInsets(top: 0, left: -4, bottom: 0, right: 0)
        _tvComment.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : UIColor.black]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    @IBAction func onCommentWrite(_ sender: Any) {
//        if(delegate != nil){
//            delegate?.CommentCellDelegate(self, onCommentWrite: sender, tag: nil)
//        }
//    }
}
