//
//  StyleCell.swift
//  selluv
//
//  Created by Comet on 1/11/18.
//

import UIKit

class StyleCell: UITableViewCell {

    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var ivBannerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lbNickname: UILabel!
    @IBOutlet weak var ivProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
