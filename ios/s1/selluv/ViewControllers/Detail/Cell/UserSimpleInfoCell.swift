//
//  UserSimpleInfoCell.swift
//  selluv
//
//  Created by Comet on 1/8/18.
//

import UIKit

class UserSimpleInfoCell: UITableViewCell {

    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _imvProfile: UIImageView!
    @IBOutlet weak var _lbUserName: UILabel!
    @IBOutlet weak var _lbUserId: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _imvProfile.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
