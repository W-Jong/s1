//
//  CheckOutViewController.swift
//  selluv
//  체크아웃 완료
//  Created by World on 1/23/18.
//  modified by PJH on 14/03/18.

import UIKit

class CheckOutViewController: BaseViewController {

    @IBOutlet weak var lbDownPrice: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var lbSendPrice: UILabel!
    @IBOutlet weak var lbTotalPrice: UILabel!
    
    var dicDealInfo  : DealDao!
    var reqPrice    : Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        if reqPrice == dicDealInfo.pdtPrice {
            lbDownPrice.isHidden = true
        } else {
            lbDownPrice.isHidden = false
            let attr : NSAttributedString = NSAttributedString.init(string: CommonUtil.formatNum(dicDealInfo.pdtPrice), attributes: [NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: Int8(NSUnderlineStyle.styleSingle.rawValue))]);
            lbDownPrice.attributedText = attr
        }
        
        lbPrice.text = CommonUtil.formatNum(reqPrice)
        lbSendPrice.text = CommonUtil.formatNum(dicDealInfo.sendPrice) + " 원"
        lbTotalPrice.text = CommonUtil.formatNum(dicDealInfo.sendPrice + reqPrice) + " 원"
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickShoping(_ sender: Any) {
        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
    }
    
    @IBAction func onClickNego(_ sender: Any) {
        //주문상세 페지로 이해
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = dicDealInfo.dealUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
