//
//  OrderDetailViewController.swift
//  selluv
//  주문상세
//  Created by Dev on 12/12/17.
//  modfied by PJH on 03/04/18.

import UIKit

class OrderDetailViewController: BaseViewController {

    @IBOutlet weak internal var scvList: UIScrollView!
    @IBOutlet weak internal var vwNormalLine: UIView!
    @IBOutlet weak internal var htNormal: NSLayoutConstraint!
    @IBOutlet weak internal var htMost: NSLayoutConstraint!
    @IBOutlet weak internal var btnNormal: UIButton!
    @IBOutlet weak internal var btnMost: UIButton!
    @IBOutlet weak internal var imgNormal: UIImageView!
    @IBOutlet weak internal var imgMost: UIImageView!
    
    @IBOutlet weak internal var lbStatus: UILabel!
    @IBOutlet weak internal var lbNegoStatus: UILabel!
    @IBOutlet weak internal var lbBottomStatus: UILabel!
    @IBOutlet weak internal var btnNego: UIButton!
    @IBOutlet weak internal var htBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblOrderNum: UILabel! //주분번호
    @IBOutlet weak var lblOrderDate: UILabel!  //주문일자
    @IBOutlet weak var lbCost: UILabel!//운임비용
    
    //pdt info
    @IBOutlet weak var ivPdt: UIImageView!
    @IBOutlet weak var lbBrandEn: UILabel!
    @IBOutlet weak var lbPdtContent: UILabel!
    @IBOutlet weak var lblNickname: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    //정산금액
    @IBOutlet weak var lbSumMoney: UILabel!
    @IBOutlet weak var lbTotalSumMoney: UILabel!
    @IBOutlet weak var lbPdtPrice: UILabel!
    @IBOutlet weak var lbSendPrice: UILabel!
    @IBOutlet weak var lbSellubUseMoney: UILabel!
    @IBOutlet weak var lbServiceMoney: UILabel!
    @IBOutlet weak var lbPromotion: UILabel!
    
    //최대 적립가능 셀럽머니
    @IBOutlet weak var lbMaxSellubMoney: UILabel!
    @IBOutlet weak var lbMaxSellubMoney1: UILabel!
    @IBOutlet weak var lbBuyMoney: UILabel!
    @IBOutlet weak var lbWriterMoney: UILabel!
    
    //배송정보
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbContact: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    //constraint
    @IBOutlet weak var lcvwSendPopupHeight: NSLayoutConstraint! //53 배송팝업
    @IBOutlet weak var lcvwServiceHeight: NSLayoutConstraint! //147 편의점택배
    @IBOutlet weak var lcvwNormalHeight: NSLayoutConstraint! //120  일반택배
    @IBOutlet weak var lcvwMoneyInfoHeight: NSLayoutConstraint!//53  정산정보
    @IBOutlet weak var lcvwMoneyHeight: NSLayoutConstraint! //45 정산금액
    @IBOutlet weak var lcvwMaxSelluvMoneyHeight: NSLayoutConstraint!//45 최대 적립가능 셀럽머니
    
    var dealUid         : Int!
    var dicDeal         : DealListDto!
    var dicDealDetail   : DealDetailDto!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getDealDetailInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {

    }
    
    func showMainContent() {
        if dicDealDetail.deal.status == 11 { //네고제안
            lbStatus.text = "판매자에게 네고를 제안하였습니다. 3일 이내로 판매자가 네고 제안을 승인하지 않을 경우, 거래는 자동적으로 취소됩니다. 판매자가 네고제안을 수락할 경우, 등록하신 카드로 결제가 진행됩니다."
            lbNegoStatus.text = "[네고제안]"
            lbBottomStatus.isHidden = true
            btnNego.isHidden = true
            htBottom.constant = 15
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            lcvwMoneyInfoHeight.constant = 0  //정산정보
            lcvwMoneyHeight.constant = 0  //정산금액
            lcvwMaxSelluvMoneyHeight.constant = 0 //최대 적립가능 셀럽머니
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.negoTime, type: "yyyy-MM-dd hh:mm")
            
        } else if dicDealDetail.deal.status == 12 {  //카운터 네고
            lbStatus.text = "판매자가 카운터 네고를 통해 새로운 가격을 제안하였습니다. 판매자가 제안한 가격으로 승인하실 경우, 등록하신 카드로 결제가 진행됩니다. 3일 이내로 승인하지 않으실 경우, 거래는 자동으로 취소됩니다."
            lbNegoStatus.text = "[카운터네고]"
            lbBottomStatus.isHidden = true
            btnNego.isHidden = true
            htBottom.constant = 15
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            lcvwMoneyInfoHeight.constant = 0  //정산정보
            lcvwMoneyHeight.constant = 0  //정산금액
            lcvwMaxSelluvMoneyHeight.constant = 0 //최대 적립가능 셀럽머니
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.negoTime, type: "yyyy-MM-dd hh:mm")

        } else if dicDealDetail.deal.status == 1 { //배송준비
            lbStatus.text = "상품의 주문 및 결제가 완료되어, 판매작에게 주문하신 상품 발송을 요청하였습니다. 판매자의 사정으로 상품이 3명업일내 발송하지 않을 경우, 주문이 자동으로 취소됩니다."
            lbNegoStatus.text = "[배송준비]"
            lbBottomStatus.isHidden = true
            btnNego.isHidden = true
            htBottom.constant = 15
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")

        } else if dicDealDetail.deal.status == 2 { //배송진행

            lbNegoStatus.text = "[배송진행]"
            lbBottomStatus.isHidden = false
            btnNego.isHidden = false
            htBottom.constant = 42
            if dicDealDetail.deal.verifyPrice > 0 {
                lbStatus.text = "정품 감정이 완료되었습니다. 상품이 고객님께 발송되어 운송장번호가 업데이트 되었습니다. 상품 배송을 조회하실 수 있습니다."
            } else {
                lbStatus.text = "판매자가 주문하신 상품을 발송하였습니다."
            }
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배

            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if dicDealDetail.deal.status == 3 { //정품인증
            lbStatus.text = "판매자가 주문하신 상품을 셀럽으로 발송하였습니다.\n전문감정 평가단에 의해 정품 감정이 이루어질 예정입니다. 감정이 완료된후, 상품이 발송되면 운송장 번호를 확인하실 수 있으며 운송장 번호 업데이트까지 평균적으로 3-7일 정도 소요됩니다."
            lbNegoStatus.text = "[정품인증]"
            lbBottomStatus.isHidden = false
            btnNego.setBackgroundImage(UIImage.init(named: "btn_nego_long"), for: .normal)
            btnNego.setTitle("정품 감정 서비스 안내", for: .normal)
            btnNego.setTitleColor(.black, for: .normal)
            btnNego.isHidden = false
            htBottom.constant = 42
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
            lbBottomStatus.isHidden = true
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            
        } else if dicDealDetail.deal.status == 4 {  //배송완료
//            lbStatus.text = "주문하신 상품에 대한 배송이 완료되었습니다. 상품을 잘 받으신 겨우, 구매를 확정해주시기 바랍니다. 2017년 4월 10일 17시 자동으로 구매확정이 완료됩니다."
            lbNegoStatus.text = "[배송완료]"
            lbBottomStatus.isHidden = false
            btnNego.isHidden = false
            htBottom.constant = 42
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
            getWorkingDay(_date: CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd"), diff: 3)

            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배

        } else if dicDealDetail.deal.status == 10 {  //주문최소
            
            lbNegoStatus.text = "[주문취소]"
            lbBottomStatus.isHidden = true
            btnNego.isHidden = true
            htBottom.constant = 15
            
            if dicDealDetail.deal.cancelReason.contains("자동") {
                lbStatus.text = "판매자 사정으로 상품이 발송되지 않아 주문이 취소되었습니다."
            } else {
                lbStatus.text = "주문이 취소되었습니다."
            }
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if dicDealDetail.deal.status == 5 {  //거래완료
            lbStatus.text = "주문하신 상품에 대한 구매가 확정되었습니다. 판매자에 대한 거래후기를 남겨 주시면, 셀럽머니 5,000원을 적립해드립니다."
            lbNegoStatus.text = "[거래완료]"
            lbBottomStatus.isHidden = false
            btnNego.isHidden = false
            htBottom.constant = 42
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if dicDealDetail.deal.status == 7 {  //반품신청
            lbStatus.text = "반품신청이 완료되었습니다. 반품 사유가 적합치 않을 경우 판매자가 반품을 거절할수 있습니다."
            lbNegoStatus.text = "[반품신청]"
            lbBottomStatus.isHidden = false
            btnNego.isHidden = false
            htBottom.constant = 42
            
            lcvwSendPopupHeight.constant = 0  //배송팝업
            lcvwServiceHeight.constant = 0   //편의점택배
            lcvwNormalHeight.constant = 0  //일반택배
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if dicDealDetail.deal.status == 8 {  //반품승인
            
            lbNegoStatus.text = "[반품승인]"
            lbBottomStatus.isHidden = false
            btnNego.isHidden = false
            htBottom.constant = 42
            
            //trim
            if dicDealDetail.deal.refundDeliveryNumber != "" || dicDealDetail.deal.refundDeliveryNumber != nil {
                lbStatus.text = "판매자에게 상품을 승인하였습니다. 아래 배송 방법 중 한 가지를 선택하여, 상품을 판매자에게 착불로 발송해주세요."
                lbBottomStatus.text = dicDealDetail.deal.deliveryNumber
                btnNego.setTitle("배송 조회", for: .normal)
                btnNego.setTitle("배송 조회", for: .selected)
            } else {
                lbStatus.text = "판매자에게 상품을 발송하였습니다. 판매자가 상품 수취를 확인하면, 결제취소 절차가 진행됩니다."
                lbBottomStatus.text = dicDealDetail.deal.refundDeliveryNumber
                btnNego.setTitle("반품 배송 조회", for: .normal)
                btnNego.setTitle("반품 배송 조회", for: .selected)
                
                lcvwSendPopupHeight.constant = 0  //배송팝업
                lcvwServiceHeight.constant = 0   //편의점택배
                lcvwNormalHeight.constant = 0  //일반택배
            }
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
            
        } else if dicDealDetail.deal.status == 9 {  //반품완료
            lbStatus.text = "반품이 완료되었습니다. 결제하신 수단으로 결제 취소가 진행되며, 최대 3영업일 이내로 취소를 확인하실 수 있습니다."
            lbNegoStatus.text = "[반품완료]"
            lbBottomStatus.isHidden = false
            btnNego.isHidden = false
            htBottom.constant = 42
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        }
        
        setPdtData()
    }
    
    //상품 정보
    func setPdtData() {
        
        let _uid : String = String(dealUid)
        var securityStr = ""
        for _ in 0..<10 - _uid.count {
            securityStr = securityStr + "0"
        }
        lblOrderNum.text = "주문번호: " + securityStr + _uid
        
        ivPdt.kf.setImage(with: URL(string: dicDealDetail.deal.pdtImg), placeholder: UIImage(named: "ic_logo"), options: [], progressBlock: nil, completionHandler: nil)
        lbBrandEn.text = dicDealDetail.deal.brandEn
        lbPdtContent.text = dicDealDetail.deal.pdtTitle
        lbSize.text = dicDealDetail.deal.pdtSize
        lbPrice.text = CommonUtil.formatNum(dicDealDetail.deal.reqPrice)
        
        lblNickname.text = dicDealDetail.usr.usrNckNm
        
        //배송정보
        lbName.text = dicDealDetail.deal.recipientNm
        lbContact.text = dicDealDetail.deal.recipientPhone
        lbAddress.text = dicDealDetail.deal.recipientAddress
        
        //정산 금액
        lbPdtPrice.text = "+ " + CommonUtil.formatNum(dicDealDetail.deal.pdtPrice)
        lbSendPrice.text = "+ " + CommonUtil.formatNum(dicDealDetail.deal.sendPrice)
        lbSellubUseMoney.text = "- " + CommonUtil.formatNum(dicDealDetail.deal.selluvPrice)
        lbServiceMoney.text = CommonUtil.formatNum(dicDealDetail.deal.verifyPrice)
        lbPromotion.text = "- " + CommonUtil.formatNum(dicDealDetail.deal.payPromotionPrice)
        
        let totalMoney = dicDealDetail.deal.pdtPrice + dicDealDetail.deal.sendPrice - dicDealDetail.deal.selluvPrice + dicDealDetail.deal.verifyPrice - dicDealDetail.deal.payPromotionPrice
        lbSumMoney.text = CommonUtil.formatNum(totalMoney)
        lbTotalSumMoney.text = CommonUtil.formatNum(totalMoney)
        
        //초대 적립가능 셀럽머니
        getSystemSettingInfo(_money: totalMoney)
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickNego(_ sender: Any) {
         BestQualityPopup.show(self)
    }
    
    @IBAction func onClickInfo(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dicDealDetail.deal.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onClickUser(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicDealDetail.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNormal(_ sender: Any) {
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        if btnNormal.isSelected {
            btnNormal.isSelected = false
            htNormal.constant = 0
            vwNormalLine.isHidden = false
            imgNormal.image = UIImage.init(named: "ic_undo_arrow")
        } else {
            btnNormal.isSelected = true
            htNormal.constant = 182
            vwNormalLine.isHidden = true
            imgNormal.image = UIImage.init(named: "ic_undo_arrow_rotate")
            scvList.scrollToBottom(isAnimated: true)
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
    }
    
    @IBAction func onClickMost(_ sender: Any) {
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        if btnMost.isSelected {
            btnMost.isSelected = false
            htMost.constant = 0
            imgMost.image = UIImage.init(named: "ic_undo_arrow")
        } else {
            btnMost.isSelected = true
            htMost.constant = 120
            imgMost.image = UIImage.init(named: "ic_undo_arrow_rotate")
            scvList.scrollToBottom(isAnimated: true)
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
    }
    
    //편의점 택배
    @IBAction func onClickService(_ sender: Any) {
        
        getDealDeliveryInfo()
    }
    
    //일반택배
    @IBAction func onClickNormalService(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_NORMALDELIVERY_VIEW")) as! NormalDeliveryViewController
        vc.dicDealDetail = dicDealDetail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //배송조회
    @IBAction func onBtnDeliveryTracking(_ sender: Any) {
        
        if dicDealDetail.deal.status == 3 {
            BestQualityPopup.show(self)
        } else {
            if dicDealDetail.deal.deliveryNumber == "" || dicDealDetail.deal.deliveryNumber == nil {
                return
            }
            
            let num : String = dicDealDetail.deal.deliveryNumber
            let encodeStr : String = num.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
            let weburl = DELIVERY_URL + encodeStr
            
            guard let url = URL(string: weburl) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 주문상세
    func getDealDetailInfo() {
        
        gProgress.show()
        Net.getDealDetail(
            accessToken     : gMeInfo.token,
            dealUid         : dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getDealDetailResult
                self.getDealDetailResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //주문상세 얻기 결과
    func getDealDetailResult(_ data: Net.getDealDetailResult) {
        
        dicDealDetail  = data.dealDetail
        showMainContent()
        
    }
    
    // 시스템 설정 정보 얻기
    func getSystemSettingInfo(_money : Int){
        
        gProgress.show()
        Net.getSystemSettingInfo(
            accessToken         : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSystemSettingInfoResult
                self.getSystemSettingInfoResult(res, money: _money)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //시스템 설정 정보 얻기결과
    func getSystemSettingInfoResult(_ data: Net.getSystemSettingInfoResult, money : Int) {
        
        let dicSysstemInfo : SystemSettingDto = data.dicInfo
        let buyReviewReward : Int = dicSysstemInfo.buyReviewReward
        let maxSelluvmoney : Int = (money * dicSysstemInfo.dealReward ) / 100
        
        let total = maxSelluvmoney + buyReviewReward
        lbMaxSellubMoney.text = CommonUtil.formatNum(total)
        lbMaxSellubMoney1.text = CommonUtil.formatNum(total)
        lbBuyMoney.text = CommonUtil.formatNum(maxSelluvmoney)
        lbWriterMoney.text = CommonUtil.formatNum(buyReviewReward)
    }
    
    // 영업일 기준 날짜 얻기
    func getWorkingDay(_date : String, diff : Int) {
        
        gProgress.show()
        Net.getWorkingDay(
            accessToken     : gMeInfo.token,
            basisDate       : _date,
            deltaDays       : diff,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.ggetWorkingDayResult
                self.ggetWorkingDayResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //
    func ggetWorkingDayResult(_ data: Net.ggetWorkingDayResult) {
        
        let strDate : String = data.targetDate
        
        let year =  strDate.substring(with: strDate.index(strDate.startIndex, offsetBy: 0)..<strDate.index(strDate.startIndex, offsetBy: 4))
        let month =  strDate.substring(with: strDate.index(strDate.startIndex, offsetBy: 5)..<strDate.index(strDate.startIndex, offsetBy: 7))
        let day =  strDate.substring(with: strDate.index(strDate.startIndex, offsetBy: 8)..<strDate.index(strDate.startIndex, offsetBy: 10))
        
        let makedate = String.init(format: "%d년 %d월 %d일 17시", Int(year)!, Int(month)!, Int(day)!)
        
        lbStatus.text = "주문하신 상품에 대한 배송이 완료되었습니다. 상품을 잘 받으신 겨우, 구매를 확정해주시기 바랍니다. " + makedate + " 자동으로 구매확정이 완료됩니다."
    }
    
    // 택배예약정보 얻기
    func getDealDeliveryInfo() {
        
        gProgress.show()
        Net.getDealDeliveryInfo(
            accessToken     : gMeInfo.token,
            dealUid         : dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getDealDeliveryInfoResult
                self.getDealDeliveryInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == DELIVERY_HISTORY_NOT_EXIST {
                
                let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SERVICEDELIVERY_VIEW")) as! ServiceDeliveryViewController
                vc.dicDealDetail = self.dicDealDetail
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //택배 에약정보 얻기 결과
    func getDealDeliveryInfoResult(_ data: Net.getDealDeliveryInfoResult) {
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_DELIVERYCANCEL_VIEW")) as! DeliveryCancelViewController
        vc.dicDealHis = data.dealHisDao
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
