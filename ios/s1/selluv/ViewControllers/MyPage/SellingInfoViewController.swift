//
//  SellingInfoViewController.swift
//  selluv
//  판매내역
//  Created by Dev on 12/7/17.
//  modified by PJH on 26/03/18.

import UIKit
import DGElasticPullToRefresh

class SellingInfoViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    var header : BuyingInfoHeaderTVC?
    
    var arrSellHisList  : Array<DealListDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!
    var totalCnt        : Int = 0
    var completedCount  : Int = 0
    var negoCount       : Int = 0
    var preparedCount   : Int = 0
    var progressCount   : Int = 0
    var sentCount       : Int = 0
    var cachedCount     : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        _tvList.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getSellHistoryList()
            })
            }, loadingView: loadingView)
        _tvList.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        _tvList.dg_setPullToRefreshBackgroundColor(_tvList.backgroundColor!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSellHistoryList()
    }

    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib(nibName: "BuyingInfoHeaderTVC", bundle: nil), forCellReuseIdentifier: "BuyingInfoHeaderTVC")
        _tvList.register(UINib(nibName: "SellingInfoTVC", bundle: nil), forCellReuseIdentifier: "SellingInfoTVC")
        
        _tvList.estimatedRowHeight = 140
        _tvList.rowHeight = UITableViewAutomaticDimension
        _tvList.estimatedSectionHeaderHeight = 185
        _tvList.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    @objc func onBtnDetail(_ sender: UIButton) {
        let dic : DealListDto = arrSellHisList[sender.tag]
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dic.deal.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : DealListDto = arrSellHisList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.deal.sellerUsrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //주문상세 페이지로..
    @objc func onBtnNego(_ sender: UIButton) {
        
        let dic : DealListDto = arrSellHisList[sender.tag]
        let storyboard = UIStoryboard(name: "Mypage", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MYPAGE_SELLORDERDETAIL_VIEW") as! SellOrderDetailViewController
        vc.dealUid = dic.deal.dealUid
        vc.goPage = DealPage.SELL.rawValue
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //판매주문상세
    @objc func onBtnBg(_ sender: UIButton) {
        let dic : DealListDto = arrSellHisList[sender.tag]
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SELLORDERDETAIL_VIEW")) as! SellOrderDetailViewController
        vc.goPage = DealPage.SELL.rawValue
        vc.dealUid = dic.deal.dealUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //left button
    @objc func onBtnLeft(_ sender: UIButton) {
        let dic : DealListDto = arrSellHisList[sender.tag]
        
        switch dic.deal.status {
        case 11:  //네고제안
            //네고거절
            let alert = UIAlertController(title: "", message: NSLocalizedString("seller_nego_refuse_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealNegoRefuse(_dealUid: dic.deal.dealUid)
        
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break

        default:
            break
        }
    }
    
    //middle button
    @objc func onBtnMiddle(_ sender: UIButton) {
        let dic : DealListDto = arrSellHisList[sender.tag]
        
        switch dic.deal.status {

        case 5:  //거래완료
            //공유리워드 신청
            let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SHAREREWARD_VIEW")) as! ShareRewardViewController
            vc.dicDealInfo = dic
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 7:  //반품접수
            //반품거절
            let alert = UIAlertController(title: "", message: NSLocalizedString("refund_disallow_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqRefundDisallow(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        case 11:  //네고제안
            CNegoPopup.show(self, dicInfo: dic) { (price) in
                self.reqDealNegoCounter(_dealUid: dic.deal.dealUid, price: price)
            }
            break
        default:
            break
        }
    }
    
    //right button
    @objc func onBtnRight(_ sender: UIButton) {
        let dic : DealListDto = arrSellHisList[sender.tag]
        
        switch dic.deal.status {
        case 1:  //배송준비
            //판매거절
            let alert = UIAlertController(title: "", message: NSLocalizedString("sell_stop_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealCancelSeller(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        case 5:  //거래완료
            //거래후기
            let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_BUSINESSLATTER_VIEW")) as! BusinessLatterViewController
            vc.dicDealInfo = dic
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 6:  //정산완료
            //공유리워드 작성
            let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SHAREREWARD_VIEW")) as! ShareRewardViewController
            vc.dicDealInfo = dic
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 7:  //반품접수
            //반품승인
            let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_RETURNSOK_VIEW")) as! ReturnsOkViewController
            vc.dicDealInfo = dic
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 8:  //반품승인
            //반품왁인
            let alert = UIAlertController(title: "", message: NSLocalizedString("seller_nego_refuse_allow_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqRefundComplete(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        case 11:  //네고제안
            //네고승인
            let alert = UIAlertController(title: "", message: NSLocalizedString("seller_nego_refuse_allow_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealNegoAllow(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        default:
            break
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    //////////////////////////////////////////
    // MARK: - Delegate & DataSource
    //////////////////////////////////////////
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSellHisList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SellingInfoTVC", for: indexPath as IndexPath) as! SellingInfoTVC
        
        let dicDeal : DealListDto = arrSellHisList[indexPath.row]
        
        cell.model(dicDeal)
        
        cell.btnDetail.tag = indexPath.row
        cell.btnDetail.addTarget(self, action:#selector(self.onBtnDetail(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNego.tag = indexPath.row
        cell.btnNego.addTarget(self, action:#selector(self.onBtnNego(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNickname.tag = indexPath.row
        cell.btnNickname.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        cell.btnBgUp.tag = indexPath.row
        cell.btnBgUp.addTarget(self, action:#selector(self.onBtnBg(_:)), for: UIControlEvents.touchUpInside)
        cell.btnBgDown.tag = indexPath.row
        cell.btnBgDown.addTarget(self, action:#selector(self.onBtnBg(_:)), for: UIControlEvents.touchUpInside)

        //bottom button
        cell.btnNegoHidden.tag = indexPath.row
        cell.btnNegoHidden.addTarget(self, action:#selector(self.onBtnLeft(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNegoWhite.tag = indexPath.row
        cell.btnNegoWhite.addTarget(self, action:#selector(self.onBtnMiddle(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNegoBlack.tag = indexPath.row
        cell.btnNegoBlack.addTarget(self, action:#selector(self.onBtnRight(_:)), for: UIControlEvents.touchUpInside)

        //더보기
        if indexPath.row == arrSellHisList.count - 1 && !isLast {
            nPage = nPage + 1
            getSellHistoryList()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        header = tableView.dequeueReusableCell(withIdentifier: "BuyingInfoHeaderTVC") as? BuyingInfoHeaderTVC
        header?.lbTitle.text = "판매내역"
        
        header?.lbPrepareNumber.text = String(negoCount)
        header?.lbExitNumber.text = String(completedCount)
        header?.lbExit.text = "정산 완료"
        header?.lbT1Number.text = String(preparedCount)
        header?.lbT2Number.text = String(progressCount)
        header?.lbT3Number.text = String(sentCount)
        header?.lbT4Number.text = String(completedCount)
        header?.lbTitleNumber.text = String(totalCnt)
        
        return header
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 195
        
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 판매내역 얻기
    func getSellHistoryList() {
        
        gProgress.show()
        Net.getSellHistory(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self._tvList.dg_stopLoading()
                let res = result as! Net.getSellHistoryResult
                self.getSellHistoryResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //구매내역 얻기 결과
    func getSellHistoryResult(_ data: Net.getSellHistoryResult) {
        isLast = data.last
        totalCnt = data.totalElements
        
        completedCount = data.completedCount
        negoCount = data.negoCount
        preparedCount = data.preparedCount
        progressCount = data.progressCount
        sentCount = data.sentCount
        cachedCount = data.cachedCount
        
        if nPage == 0 {
            arrSellHisList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : DealListDto = data.list[i]
            arrSellHisList.append(dic)
        }
        _tvList.reloadData()
    }
    
    //네고거절
    func reqDealNegoRefuse(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealNegoRefuse(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getSellHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //네고승인
    func reqDealNegoAllow(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealNegoAllow(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getSellHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //판매거절
    func reqDealCancelSeller(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealCancelSeller(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getSellHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //반품확인
    func reqRefundComplete(_dealUid : Int) {
        
        gProgress.show()
        Net.reqRefundComplete(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getSellHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //반품거절
    func reqRefundDisallow(_dealUid : Int) {
        
        gProgress.show()
        Net.reqRefundDisallow(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getSellHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //판매자 카운터 네고 요청
    func reqDealNegoCounter(_dealUid : Int, price : Int) {
        
        gProgress.show()
        Net.reqDealNegoCounter(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            reqPrice        : price,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getSellHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
