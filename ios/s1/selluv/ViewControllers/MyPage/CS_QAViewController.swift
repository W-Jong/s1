//
//  CS_QAViewController.swift
//  selluv
//  1 : 1문의 목록 얻기 및 작성
//  Created by Dev on 12/8/17.
//  modified by PJH on 03/02/18.

import UIKit
import Toast_Swift

class CS_QAViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    var m_selectedRow : Int = -1
    
    @IBOutlet weak var _tvList: UITableView!
    
    @IBOutlet weak var _btnSave: UIButton!
    @IBOutlet weak var _tvAsk: KMPlaceholderTextView!
    @IBOutlet weak var _tvTitle: DesignableUITextField!
    @IBOutlet weak var lctvQnaListHeight: NSLayoutConstraint!
    
    var arrQnaList : Array<Qna> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initVC()
        getQnaList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        setupTableView()
        
        _tvTitle.delegate = self
        _tvAsk.delegate = self
        
        _tvTitle.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        
        _tvTitle.text = ""
        _tvAsk.text = ""
        _tvAsk.placeholder = "문의 내용"
        
        _btnSave.addTarget(self, action: #selector(onBtnSave(_:)), for: .touchUpInside)
        _btnSave.isEnabled = false
    }
    
    func setupTableView() {
        _tvList.register(UINib(nibName: "CS_QADownTVC", bundle: nil), forCellReuseIdentifier: "CS_QADownTVC")
    }
    
    @objc func onBtnViewContent(_ sender: UIButton) {
        if m_selectedRow == sender.tag {
            m_selectedRow = -1
        } else {
            m_selectedRow = sender.tag
        }
        
        _tvList.reloadData()
    }

    @objc func textChanged(_ sender : Any) {
        if _tvAsk.text == "" || _tvTitle.text == "" {
            _btnSave.isEnabled = false
            _btnSave.backgroundColor = UIColor.lightGray
        } else {
            _btnSave.isEnabled = true
            _btnSave.backgroundColor = UIColor.black
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        _tvTitle.resignFirstResponder()
        _tvAsk.resignFirstResponder()
        
        _tvList.reloadData()
    }
    
    @IBAction func onBtnSave(_ sender: Any) {
        reqQna()
    }
    
    
    //////////////////////////////////////////
    // MARK: - Delegate & DataSource
    //////////////////////////////////////////
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQnaList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        lctvQnaListHeight.constant = tableView.contentSize.height
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CS_QADownTVC", for: indexPath) as! CS_QADownTVC
        
        cell.btnHeader.tag = indexPath.row
        cell.btnHeader.addTarget(self, action: #selector(onBtnViewContent(_:)), for: .touchUpInside)
        
        let dic : Qna = arrQnaList[indexPath.row]
        
        cell.lbTitle.text = dic.title
        cell.lbTitle.textColor = UIColor.black
        
        cell.lbTime.text = CommonUtil.getDateFromStamp(dic.regTime, type: "yyyy-MM-dd")
        cell.lbContent.text = dic.content
        
        if dic.status == 1 { //답변완료
            cell.lbAnswer.text = dic.answer
            cell.lbAnswer.textColor = UIColor.black
            
            if m_selectedRow == indexPath.row {
                cell.vwContentHeight.constant = 118
            } else {
                cell.vwContentHeight.constant = 0
            }
            
            cell.ivArrow.isHidden = false
            cell.ivArrow.image = m_selectedRow == indexPath.row ? #imageLiteral(resourceName: "ic_undo_arrow_rotate") : #imageLiteral(resourceName: "ic_undo_arrow")
            cell.btnHeader.isEnabled = true
        } else {  //답변중
            cell.lbAnswer.text = "답변 대기중입니다."
            cell.lbAnswer.textColor = UIColor(hex: 0x999999)
            cell.ivArrow.isHidden = true
            cell.vwContent.isHidden = true
            cell.vwContentHeight.constant = 0
            cell.btnHeader.isEnabled = false
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dic : Qna = arrQnaList[indexPath.row]
        if dic.status == 1 { //답변완료
            
            if m_selectedRow == indexPath.row {
                return 200
            } else {
                return 90
            }
        } else {
            return 90
        }
    }
    
    // TextView Delegate

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self._tvList.reloadData()
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self._tvList.reloadData()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if _tvAsk.text == "" || _tvTitle.text == "" {
            _btnSave.isEnabled = false
            _btnSave.backgroundColor = UIColor.lightGray
        } else {
            _btnSave.isEnabled = true
            _btnSave.backgroundColor = UIColor.black
        }
        
        return true
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 1 : 1 문의 목록 작성
    func reqQna() {
        
        onBGTouched(self)
        
        gProgress.show()
        Net.reqQna(
            title           : (_tvTitle.text)!,
            content         : (_tvAsk.text)!,
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getQnaListResult
                self.reqQnaResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //1 : 1 문의 목록 작성 결과
    func reqQnaResult(_ data: Net.getQnaListResult) {
        
        CommonUtil.showToast(NSLocalizedString("qna_reg_success", comment:""))
        
        _tvAsk.text = ""
        _tvTitle.text = ""
        
        arrQnaList = data.list
        _tvList.reloadData()
        
    }
    
    // 1 : 1 문의 목록 얻기
    func getQnaList() {
        
        onBGTouched(self)
        
        gProgress.show()
        Net.getQnaList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getQnaListResult
                self.getQnaResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //1 : 1 문의 목록 얻기 결과
    func getQnaResult(_ data: Net.getQnaListResult) {
        
        arrQnaList = data.list
        _tvList.reloadData()
        
    }

}
