//
//  ProfileViewController.swift
//  selluv
//  프로필 수정
//  Created by Dev on 12/7/17.
//  modified by PJH on 07/03/18.

import UIKit
import Toast_Swift
import Photos

class ProfileViewController: BaseViewController, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var _vwEventItem: UIView!
    @IBOutlet weak var _vwEventItemMain: UIView!
    
    @IBOutlet weak var _vwBgPhoto: UIView!
    @IBOutlet weak var _vwProfilePhoto: UIView!
    
    @IBOutlet weak var _htIntroduce: NSLayoutConstraint!
    
    @IBOutlet weak var _imgProfileBg: UIImageView!
    @IBOutlet weak var _imgProfilePhoto: UIImageView!
    
    @IBOutlet weak var _tfNickName: UITextField!
    @IBOutlet weak var _lblid: UILabel!
    @IBOutlet weak var lblSex: UILabel!
    
    @IBOutlet weak var _tfIntroduce: UITextView!
    @IBOutlet weak var _lbIntroduce: UILabel!
    @IBOutlet weak var _btnIntroduce: UIButton!
    @IBOutlet weak var _lbBtns: UILabel!
    @IBOutlet weak var _lbStrings: UILabel!
    @IBOutlet weak var _lbDownStrings: UILabel!
    
    @IBOutlet weak var _imgMenEffect: UIImageView!
    @IBOutlet weak var _imgWomenEffect: UIImageView!
    @IBOutlet weak var _imgKidsEffect: UIImageView!
    
    @IBOutlet weak var _scvMain: UIScrollView!
    @IBOutlet weak var _tpScvHeight: NSLayoutConstraint!
    
    @IBOutlet weak var _btnMen: UIButton!
    @IBOutlet weak var _btnWomen: UIButton!
    @IBOutlet weak var _btnKids: UIButton!
    @IBOutlet weak var _btnNickNameDel: UIButton!
    @IBOutlet weak var wdBtnNickName: NSLayoutConstraint!
    
    var m_activeText : UITextField?
    
    var assetsList = [UIImage]()
    
    var m_bKeyboard     = false
    var m_bProfile      = false
    var tempImg         : UIImage!
    
    var profileImg      : String = ""
    var profileBgImg    : String = ""
    var nickname        : String = ""
    var intro           : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        _scvMain.delegate = self
        
        _vwEventItem.layer.cornerRadius = 10
        _vwEventItemMain.layer.cornerRadius = 10
        
        _vwBgPhoto.layer.cornerRadius = 15
        _vwProfilePhoto.layer.cornerRadius = 15
        
        _tfIntroduce.delegate = self
        _tfNickName.delegate = self
        
        _htIntroduce.constant = 0
        _tfIntroduce.isHidden = true
        _lbIntroduce.isHidden = true
        
        _tfNickName.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        keyboardShow()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onLocalNotificationReceived(_:)), name: nil, object: nil)
    }
    
    func initData(){
        _tfNickName.text = gMeInfo.usrNckNm
        _lblid.text = gMeInfo.usrId
        _tfIntroduce.text = gMeInfo.desc
        
        if gMeInfo.desc == "" {
            _btnIntroduce.isSelected = false
        } else {
            _btnIntroduce.isSelected = true
            
        }
        showIntroArea(status: _btnIntroduce.isSelected)
        
        if gMeInfo.gender == 1 {
            lblSex.text = NSLocalizedString("male", comment:"")
        } else {
            lblSex.text = NSLocalizedString("female", comment:"")
        }
        
        if gMeInfo.likeGroup & 1 == 0 {
            _imgMenEffect.isHidden = true
        } else {
            _imgMenEffect.isHidden = false
        }

        if gMeInfo.likeGroup & 2 == 0 {
            _imgWomenEffect.isHidden = true
        } else {
            _imgWomenEffect.isHidden = false
        }

        if gMeInfo.likeGroup & 4 == 0 {
            _imgKidsEffect.isHidden = true
        } else {
            _imgKidsEffect.isHidden = false
        }
        
        _imgProfilePhoto.kf.setImage(with: URL(string: gMeInfo.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        _imgProfileBg.kf.setImage(with: URL(string: gMeInfo.profileBackImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
    }
    
    func keyboardShow() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func onLocalNotificationReceived(_ notification : Notification) {
        
        switch notification.name {
        case Notification.Name(NOTI_PROFILE_CHANGE as String) :
            let msg = notification.object as? UIImage
            _imgProfilePhoto.image = msg
            tempImg = msg
//            view.makeToast("프로필 이미지 변겅")
            let data = UIImageJPEGRepresentation(msg!, 0.8)
            uploadFile(data: data!)
            break
        case Notification.Name(NOTI_BG_CHANGE as String) :
            let msg = notification.object as? UIImage
            _imgProfileBg.image = msg
//            view.makeToast("프로필 배경이미지 변겅")
            let data = UIImageJPEGRepresentation(msg!, 0.8)
            uploadFile(data: data!)
            tempImg = msg
            break
        default:
            break
        }
    }
    
    func getImageFromAsset(assets : PHAsset) {
        //        var resImg = UIImage()
        
        let option = PHImageRequestOptions()
        option.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
        //        option.isSynchronous = true
        
        let imageSize = CGSize(width: assets.pixelWidth, height: assets.pixelHeight)
        
        PHCachingImageManager.default().requestImage(for: assets, targetSize: imageSize, contentMode: .aspectFill, options: option) { (result, _) in
            if let image = result {
                self.showEdit(image)
                //                resImg = image
            }
        }
        
        //        return resImg
    }
    
    
    func showEdit(_ image : UIImage) {
        let nav : UINavigationController! = self.navigationController
        let vc = PhotoEditMypage(nibName: "PhotoEditMypage", bundle: nil)
        vc.imgList = image
        vc.m_nStatus = m_bProfile == true ? 2 : 3
        vc.m_bFlag = m_bProfile == true ? true : false
        nav.pushViewController(vc, animated: true)
    }
    
    func go2Gallery() {
        PhotoGalleryMypage.show(self) { (photo) in
            
            self.getImageFromAsset(assets : photo)
//            self.assetsList.removeAll()
        }
    }

    func showIntroArea(status : Bool) {
        if status {
            UIView.beginAnimations("menuHoverAnim", context: nil)
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationDelegate(self)
            
            _lbBtns.text = NSLocalizedString("close", comment:"")
            _btnIntroduce.isSelected = false
            _htIntroduce.constant = 85
            _lbStrings.isHidden = false
            _lbDownStrings.isHidden = false
            
            _tfIntroduce.isHidden = false
            if _tfIntroduce.text == "" {
                _lbIntroduce.isHidden = false
            }
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            UIView.commitAnimations()
        } else {
            UIView.beginAnimations("menuHoverAnim", context: nil)
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationDelegate(self)
            
            _lbBtns.text = NSLocalizedString("modify", comment:"")
            _btnIntroduce.isSelected = true
            _htIntroduce.constant = 0
            _lbStrings.isHidden = true
            _lbDownStrings.isHidden = true
            _tfIntroduce.isHidden = true
            _lbIntroduce.isHidden = true
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            UIView.commitAnimations()
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        if _tfNickName.text == "" {
            MsgUtil.showUIAlert(NSLocalizedString("empty_nickname_alert", comment:""))
        }
        
        if _imgMenEffect.isHidden && _imgWomenEffect.isHidden && _imgKidsEffect.isHidden {
            MsgUtil.showUIAlert(NSLocalizedString("select_kind", comment:""))
        }
        
        updateUserInfo()
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        _tfNickName.resignFirstResponder()
        _tfIntroduce.resignFirstResponder()
    }
    
    @IBAction func onClickProfileBg(_ sender: Any) {
        m_bProfile = false
        onBGTouched(sender)
        go2Gallery()
    }
    
    @IBAction func onClickNickNameDel(_ sender: Any) {
        _tfNickName.text = ""
        _btnNickNameDel.isHidden = true
        wdBtnNickName.constant = 15
    }
    
    @IBAction func onClickProfilePhoto(_ sender: Any) {
        m_bProfile = true
        onBGTouched(sender)
        go2Gallery()
    }
    
    @IBAction func onClickIntroduce(_ sender: Any) {
        keyboardShow()
        showIntroArea(status: _btnIntroduce.isSelected)
    }
    
    //로그아웃
    @IBAction func onClickLogout(_ sender: Any) {
        
       
        let alert = UIAlertController(title: NSLocalizedString("alarm", comment:""), message: NSLocalizedString("logout_alert_msg", comment:""), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
            
            let m : UserModel = UserModel()
            m.usrUid = String(gMeInfo.usrUid)
            m.loginType = String(gMeInfo.usrLoginType)
            m.usrId = gMeInfo.usrId
            m.profileImg = gMeInfo.profileImg
            m.usrPass = gMeInfo.usrPass
            m.usrNcknm = gMeInfo.usrNckNm
            DBManager.getSharedInstance().createUser(m)
            
            CommonUtil.showToast(NSLocalizedString("logout_success", comment:""))
            let ud: UserDefaults = UserDefaults.standard
            gMeInfo.usrPass = nil
            ud.set(gMeInfo.usrPass, forKey: "usrPass")
            ud.synchronize()
            
            // 메인페이지의 방문탭인덱스를 1 (인기탭)로 세팅
            gMainPageIndex = 1
            
            self.pushVC("REVISIT_VIEW", storyboard: "Login", animated: true)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
       
    }
    
    @IBAction func onClickMen(_ sender: Any) {
        if _btnMen.isSelected {
            _btnMen.isSelected = false
            _imgMenEffect.isHidden = true
        } else {
            _btnMen.isSelected = true
            _imgMenEffect.isHidden = false
        }
    }
    
    @IBAction func onClickWomen(_ sender: Any) {
        if _btnWomen.isSelected {
            _btnWomen.isSelected = false
            _imgWomenEffect.isHidden = true
        } else {
            _btnWomen.isSelected = true
            _imgWomenEffect.isHidden = false
        }
    }
    
    @IBAction func onClickKids(_ sender: Any) {
        if _btnKids.isSelected {
            _btnKids.isSelected = false
            _imgKidsEffect.isHidden = true
        } else {
            _btnKids.isSelected = true
            _imgKidsEffect.isHidden = false
        }
    }
    
    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {

                self._tpScvHeight.constant = -(1.5 * keyboardSize.height)
                self.view.layoutIfNeeded()
                m_bKeyboard = true
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
            self._tpScvHeight.constant = 0
            m_bKeyboard = false
    }
    
    // TextView Delegat
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newString : NSString = NSString(string: textView.text)
        
        guard let textEdit = textView.text else { return true }
        let newLength = textEdit.count + text.count - range.length
        
        if textView == _tfIntroduce {
            _lbStrings.text = String(newLength)
            if newString.length > 120 {
                return false
            }
        }
        
        newString = newString.replacingCharacters(in: range, with: text) as NSString
        if (newString.length == 0) {
            _lbIntroduce.isHidden = false
        } else {
            _lbIntroduce.isHidden = true
        }
        
        return true
    }
    
    
    @objc func textChanged(_ sender : UITextField) {
        switch sender {
        case _tfNickName:
            if !(_tfNickName.text?.isEmpty)! {
                _btnNickNameDel.isHidden = false
                wdBtnNickName.constant = 35
            } else {
                _btnNickNameDel.isHidden = true
                wdBtnNickName.constant = 15
            }
        default:
            return
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case _tfNickName:
            keyboardShow()
            if !(_tfNickName.text?.isEmpty)! {
                _btnNickNameDel.isHidden = false
                wdBtnNickName.constant = 35
            } else {
                _btnNickNameDel.isHidden = true
                wdBtnNickName.constant = 15
            }
        default:
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
        switch textField {
        case _tfNickName:
            _btnNickNameDel.isHidden = true
            wdBtnNickName.constant = 15
        default:
            return
        }

    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 한개 파일 업로드
    func uploadFile( data : Data) {
        
        gProgress.show()
        Net.uploadFile(
            file            : data,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UploadFileResult
                
                if self.m_bProfile {
                    self.profileImg = res.fileName
                    self._imgProfilePhoto.image = self.tempImg
                } else {
                    self.profileBgImg = res.fileName
                    self._imgProfileBg.image = self.tempImg
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 프로필정보 업데이트
    func updateUserInfo() {
        
        nickname = _tfNickName.text!.trimmingCharacters(in: .whitespaces)
        intro = _tfIntroduce.text
        
        gProgress.show()
        Net.updateUserInfo(
            accessToken     : gMeInfo.token,
            description     : intro,
            isChildren      : _imgKidsEffect.isHidden ? false : true,
            isFemale        : _imgWomenEffect.isHidden ? false : true,
            isMale          : _imgMenEffect.isHidden ? false : true,
            profileBackImg  : profileBgImg,
            profileImg      : profileImg,
            usrNckNm        : nickname,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}
