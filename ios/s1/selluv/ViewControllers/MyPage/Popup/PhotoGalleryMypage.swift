//
//  PhotoGalleryMypage.swift
//  selluv
//
//  Created by Gambler on 12/25/17.
//

import UIKit
import Photos
import Kingfisher

class PhotoGalleryMypage: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var btnTitle: UIButton!
    
    @IBOutlet var clvPhoto: UICollectionView!
    @IBOutlet var tblItem: UITableView!
    
    var cbOk: callback! = nil
    public typealias callback = (_ photo: PHAsset) -> ()
    
    var assetsList = [PHAsset]()
    var selectedAssets = PHAsset()
    var photoTypeList = [PhotoType]()
    
    let typeList = [
        (name: "모든 사진", cnt: 0, type: PHAssetCollectionSubtype.any, thumbnail: UIImage(named: "")),
        (name: "최근 추가된 항목", cnt: 0, type: PHAssetCollectionSubtype.smartAlbumRecentlyAdded, thumbnail: UIImage(named: "")),
        (name: "스크린샷", cnt: 0, type: PHAssetCollectionSubtype.smartAlbumScreenshots, thumbnail: UIImage(named: "")),
        (name: "다운로드", cnt: 0, type: PHAssetCollectionSubtype.albumImported, thumbnail: UIImage(named: "")),
        (name: "셀카", cnt: 0, type: PHAssetCollectionSubtype.smartAlbumSelfPortraits, thumbnail: UIImage(named: "")),
        (name: "고속연사", cnt: 0, type: PHAssetCollectionSubtype.smartAlbumLivePhotos, thumbnail: UIImage(named: ""))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc: UIViewController, okCallback: callback! = nil) {
        let galleryPopup = PhotoGalleryMypage(okCallback: okCallback)
        galleryPopup.modalPresentationStyle = .overCurrentContext
        galleryPopup.modalTransitionStyle = .crossDissolve
        vc.present(galleryPopup, animated: true, completion: nil)
    }
    
    func initVC() {
        
        clvPhoto.register(UINib(nibName: "GalleryGridCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        tblItem.register(UINib(nibName: "GalleryListCell", bundle: nil), forCellReuseIdentifier: "listCell")
        tblItem.rowHeight = 75
        
        authorize(fromViewController: self) { (authorized) -> Void in
            guard authorized == true else {
                return
            }
            
            self.initAlbum()
            self.initList()
        }
        
    }
    
    func initAlbum() {
        let fetchOptions = PHFetchOptions()
        
        // Camera roll fetch result
        let cameraRollResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .smartAlbumUserLibrary, options: fetchOptions)
        
        // Albums fetch result
        let albumResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        let arrCamera = initAlbumItem(cameraRollResult)
        let arrAlbum = initAlbumItem(albumResult)
        
        for obj in [arrCamera, arrAlbum] {
            for item in obj {
                assetsList.append(item)
            }
        }
    }
    
    func initList() {
        
        let fetchOptions = PHFetchOptions()
        let collectionResult = PHCollection.fetchTopLevelUserCollections(with: fetchOptions)
        
        
        for item in typeList {
            
            let type = PhotoType()
            
            let albumResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: item.type, options: fetchOptions)
            let arrAlbum = initAlbumItem(albumResult)
            
            type.name = item.name
            type.count = arrAlbum.count
            type.type = item.type
            
            if arrAlbum.count > 0 {
                let imageSize = CGSize(width: 55, height: 55)
                let imageContentMode: PHImageContentMode = .aspectFill
                
                PHCachingImageManager.default().requestImage(for: arrAlbum[0], targetSize: imageSize, contentMode: imageContentMode, options: nil) { (result, _) in
                    type.thumbnail = result
                }
            }
            
            photoTypeList.append(type)
            
        }
        
        if collectionResult.count > 0 {
            collectionResult.enumerateObjects({ (collection, index, stop) in
                
                let type = PhotoType()
                
                let albumResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [collection.localIdentifier], options: fetchOptions)
                let arrAlbum = self.initAlbumItem(albumResult)
                
                type.name = collection.localizedTitle!
                type.count = arrAlbum.count
                type.identifier = collection.localIdentifier
                
                if arrAlbum.count > 0 {
                    let imageSize = CGSize(width: 55, height: 55)
                    let imageContentMode: PHImageContentMode = .aspectFill
                    
                    PHCachingImageManager.default().requestImage(for: arrAlbum[0], targetSize: imageSize, contentMode: imageContentMode, options: nil) { (result, _) in
                        type.thumbnail = result
                    }
                }
                
                self.photoTypeList.append(type)
                
            })
        }
        
        tblItem.reloadData()
        
    }
    
    func initAlbumItem(_ fetchResult : PHFetchResult<PHAssetCollection>) -> [PHAsset] {
        var arrAsset = [PHAsset]()
        
        let cachingManager = PHCachingImageManager.default() as? PHCachingImageManager
        cachingManager?.allowsCachingHighQualityImages = false
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        
        fetchResult.enumerateObjects({ (object, index, stop) -> Void in
            let result = PHAsset.fetchAssets(in: object, options: fetchOptions)
            result.enumerateObjects({ (asset, idx, stp) in
                arrAsset.append(asset)
            })
        })
        
        return arrAsset
    }
    
    func authorize(_ status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus(), fromViewController: UIViewController, completion: @escaping (_ authorized: Bool) -> Void) {
        switch status {
        case .authorized:
            // We are authorized. Run block
            completion(true)
            
        case .notDetermined:
            // Ask user for permission
            PHPhotoLibrary.requestAuthorization({ (status) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    self.authorize(status, fromViewController: fromViewController, completion: completion)
                })
            })
            
        default:
            DispatchQueue.main.async(execute: { () -> Void in
                completion(false)
            })
        }
    }
    
    func filterPhoto(type : PhotoType) {
        
        let fetchOptions = PHFetchOptions()
        var albumResult : PHFetchResult<PHAssetCollection>?
        
        if type.type != nil {
            albumResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: type.type!, options: fetchOptions)
            
            for item in typeList {
                if item.type == type.type {
                    btnTitle.setTitle(type.name, for: .normal)
                    btnTitle.setTitle(type.name, for: .selected)
                }
            }
        } else {
            albumResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [type.identifier!], options: fetchOptions)
            btnTitle.setTitle(type.name, for: .normal)
            btnTitle.setTitle(type.name, for: .selected)
        }
        
        let arrAlbum = initAlbumItem(albumResult!)
        
        assetsList.removeAll()
        for item in arrAlbum {
            assetsList.append(item)
        }
        
        clvPhoto.reloadData()
        
        btnTitle.isSelected = !btnTitle.isSelected
        tblItem.isHidden = true
        clvPhoto.isHidden = false
        btnClose.isHidden = false
        
    }
    
    func okAction() {
        
        if cbOk != nil {
            cbOk(selectedAssets)
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func titleAction(_ sender: Any) {
        
        btnTitle.isSelected = !btnTitle.isSelected
        tblItem.isHidden = !btnTitle.isSelected
        btnClose.isHidden = !tblItem.isHidden
        
    }
    
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension PhotoGalleryMypage : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assetsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath) as! GalleryGridCell
        
        let w_assets = assetsList[indexPath.row]
        let height = (self.view.frame.size.width - 2) / 3
        let imageSize = CGSize(width: height, height: height)
        let imageContentMode: PHImageContentMode = .aspectFill
        
        PHCachingImageManager.default().requestImage(for: w_assets, targetSize: imageSize, contentMode: imageContentMode, options: nil) { (result, _) in
            cell.ivPhoto.image = result
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! GalleryGridCell
        
        cell.ivTick.isHidden = !cell.ivTick.isHidden
        cell.ivMask.isHidden = !cell.ivMask.isHidden
        
        selectedAssets = assetsList[indexPath.row]
        
        okAction()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 4) / 3
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 1.0, bottom: 0, right: 1.0)
    }
    
}

extension PhotoGalleryMypage : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photoTypeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! GalleryListCell
        
        let ptype = photoTypeList[indexPath.row]
        
        cell.lbName.text = ptype.name
        cell.lbCount.text = String.init(format: "%d", ptype.count)
        
        if ptype.thumbnail != nil {
            cell.ivPhoto.image = ptype.thumbnail
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        filterPhoto(type: photoTypeList[indexPath.row])
        
    }
    
}
