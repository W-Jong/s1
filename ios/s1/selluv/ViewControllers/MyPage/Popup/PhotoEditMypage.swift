//
//  PhotoEditMypage
//  selluv
//
//  Created by Gambler on 1/8/18.
//

import UIKit
import Toast_Swift
import AVFoundation

class PhotoEditMypage: BaseViewController, CropViewDelegate {
    
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    var imgList = UIImage()
    
    var assetsList : [UIImage] = []
    
    var originImageViewFrame: CGRect!
    var cropView: LyEditImageView?
    var finalImage: UIImage!
    var m_bFlag = false
    var m_nStatus : Int = 0
    
    var cbOk: callback! = nil
    
    fileprivate var vwCrop: UIView!
    public typealias callback = (_ img : [UIImage]) -> ()
    
    @IBOutlet var btnClose: UIButton!
    
    @IBOutlet var vwFrame: UIView!
    @IBOutlet var ivScalablePhoto: ScalableImageView!
    fileprivate var overLayView: OverLayView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ivScalablePhoto.setImage(imgList)
        
        overLayView = OverLayView(frame: CGRect(x: 0, y: 0, width: ivScalablePhoto.frame.size.width, height: ivScalablePhoto.frame.size.height))
        
        var length = ivScalablePhoto.m_imageView.frame.size.width > ivScalablePhoto.m_imageView.frame.size.height ? ivScalablePhoto.m_imageView.frame.size.height : ivScalablePhoto.m_imageView.frame.size.width
        vwCrop = UIView(frame: CGRect(x: ivScalablePhoto.frame.size.width / 2 - (length - 30) / 2, y: ivScalablePhoto.frame.size.height / 2 - (length - 30) / 2, width: length - 30, height: length - 30))
        vwCrop.borderWidth = 0.5
        vwCrop.borderColor = UIColor.white
        if m_bFlag {
            vwCrop.cornerRadius = (length - 30) / 2
        } else {
            vwCrop.cornerRadius = 0
        }
        vwCrop.clipsToBounds = true
        vwCrop.isUserInteractionEnabled = false
        vwFrame.addSubview(vwCrop)
        overLayView.setOverLayView(cropRect: vwCrop.frame, flag: m_bFlag)
        overLayView.clipsToBounds = true
        vwFrame.addSubview(overLayView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    convenience init(img : UIImage, okCallback : callback! = nil) {
        self.init()
        
        imgList = img
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, imgList : UIImage, okCallback: callback! = nil) {
        
        let sizePopup = PhotoEditMypage(img: imgList, okCallback : okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
    }
    
    func initVC() {
        
    }
    
    func cropImage(image: UIImage, toRect rect: CGRect) -> UIImage {
        let imageRef = image.cgImage?.cropping(to: rect)
        let croppedImage = UIImage(cgImage: imageRef!)
        return croppedImage
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func finishAction(_ sender: Any?) {
        let rect = vwFrame.convert(vwCrop.frame, to: ivScalablePhoto.m_imageView)
        let imageSize = ivScalablePhoto.m_imageView.image?.size
        let ratio = ivScalablePhoto.frame.size.width / (imageSize?.width)!
        let zoomedRect = CGRect(x: rect.origin.x / ratio, y: rect.origin.y / ratio, width: rect.size.width / ratio, height: rect.size.height / ratio)
        let croppedImage = cropImage(image: ivScalablePhoto.m_imageView.image!, toRect: zoomedRect)
        finalImage = croppedImage
        
        if m_nStatus == 1 {
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_MYPAGE_CHANGE), object: finalImage))
        } else if m_nStatus == 2 {
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_PROFILE_CHANGE), object: finalImage))
        } else if m_nStatus == 3 {
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue:NOTI_BG_CHANGE), object: finalImage))
        }
        popVC()
    }
}

private class OverLayView: UIView {
    var cropRect : CGRect?
    var blurEffectView: UIVisualEffectView!
    var blurEffect: UIBlurEffect!
    var delayTask: DispatchWorkItem!
    var flag = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = false;
        self.backgroundColor = UIColor.clear;
        blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.clipsToBounds = true
        delayTask = DispatchWorkItem { [unowned self] in
            UIView.animate(withDuration: 0.5, animations: {
                self.blurEffectView.alpha = 1
            })
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setOverLayView(cropRect: CGRect, flag: Bool) {
        self.cropRect = cropRect
        self.flag = flag
        self.clipsToBounds = true
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4).set()
        UIRectFill(self.frame)
        
        let intersecitonRect = self.frame.intersection(self.cropRect!)
        UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0).set()
        
        if flag {
            
            let context = UIGraphicsGetCurrentContext()
            context?.setBlendMode(CGBlendMode.clear)
//            context?.setFillColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
            context?.fillEllipse(in: intersecitonRect)
            
        } else {
            UIRectFill(intersecitonRect)
        }
    }
    
}
