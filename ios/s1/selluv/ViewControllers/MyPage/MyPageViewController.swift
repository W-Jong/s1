//
//  MyPageViewController.swift
//  selluv
//   마이페이지
//  Created by Dev on 12/6/17.
//  modified by PJH on 03/03/18.

import UIKit
import Photos
import Toast_Swift
import SwiftyJSON
import ChannelIO

class MyPageViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, ChannelPluginDelegate {

    @IBOutlet weak var _vwAlarm: UIView!
    @IBOutlet weak var _vwComPage: UIView!
    @IBOutlet weak var _vwComPageMain: UIView!
    @IBOutlet weak var _vwEventItem: UIView!
    @IBOutlet weak var _vwEventItemMain: UIView!
    
    @IBOutlet weak var _htDetailEvent: NSLayoutConstraint!
    @IBOutlet weak var _tvEventList: UITableView!
    @IBOutlet weak var _btnProfile: UIButton!
    @IBOutlet weak var _imgProfile: UIImageView!
    @IBOutlet weak var ivPdtLikeBadge: UIImageView!
    
    @IBOutlet weak var lblAlarmCnt: UILabel!
    @IBOutlet weak var btnNickname: UIButton!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lblitemcnt: UILabel!
    @IBOutlet weak var lblFollowerCnt: UILabel!
    @IBOutlet weak var lblFollowingCnt: UILabel!
    @IBOutlet weak var lblBuyCnt: UILabel!
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var lblSellCnt: UILabel!
    @IBOutlet weak var lblNoticeCnt: UILabel!
    
    var profileImg : UIImage!
    var arrEventList : Array<Event> = []
//    var assetsList = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getMyDetailInfo()
    }
    
    func initUI() {
        _vwAlarm.layer.cornerRadius = 7
        _vwAlarm.isHidden = true
        
        _vwComPage.layer.cornerRadius = 15
        _vwComPageMain.layer.cornerRadius = 15
        
        _vwEventItem.layer.cornerRadius = 10
        _vwEventItemMain.layer.cornerRadius = 10
        
        _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2
        
        _tvEventList.delegate = self
        _tvEventList.dataSource = self
        _tvEventList.register(UINib.init(nibName: "MainEventTableViewCell", bundle: nil), forCellReuseIdentifier: "MainEventTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onLocalNotificationReceived(_:)), name: nil, object: nil)
    }

    @objc func onLocalNotificationReceived(_ notification : Notification) {
        
        switch notification.name {
        case Notification.Name(NOTI_MYPAGE_CHANGE as String) :
            let msg = notification.object as? UIImage
            _imgProfile.image = msg
            profileImg = msg
            let data = UIImageJPEGRepresentation(msg!, 0.8)
            uploadFile(data: data!)
            break
        default:
            break
        }
    }
    
    @objc func onBtnGo2Detail(_ sender: UIButton) {
        let dic : Event = arrEventList[sender.tag]
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc : EventDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_EVENTDETAIL_VIEW") as! EventDetailViewController)
        vc.eventUid = dic.eventUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - OnClick
    
    @IBAction func onClick2Brand(_ sender: Any) {
        replaceVC("BRAND_VIEW", storyboard: "Brand", animated: false)
    }
    
    @IBAction func onClick2Sell(_ sender: Any) {
        pushVC("SELL_MAIN_VIEW", storyboard: "Sell", animated: false)
    }
    
    @IBAction func onClick2Shop(_ sender: Any) {
        replaceVC("SHOP_VIEW", storyboard: "Shop", animated: false)
    }
    
    @IBAction func onClick2Main(_ sender: Any) {
        replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
    }
    
    @IBAction func onClickAlarm(_ sender: Any) {
        pushVC("MYPAGE_ALARM_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onClickLike(_ sender: Any) {
        self.pushVC("LIKE_VIEW", storyboard: "Top", animated: true)
    }
    
    //프로필 수정
    @IBAction func onClickProfileSettings(_ sender: Any) {
        pushVC("MYPAGE_PROFILE_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onClickProfilePhoto(_ sender: Any) {
        
        PhotoGalleryMypage.show(self) { (photo) in

            self.getImageFromAsset(assets : photo)
//            self.assetsList.removeAll()
        }
    
    }
    
    func getImageFromAsset(assets : PHAsset) {
//        var resImg = UIImage()
        
        let option = PHImageRequestOptions()
        option.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
//        option.isSynchronous = true
        
        let imageSize = CGSize(width: assets.pixelWidth, height: assets.pixelHeight)
        
        PHCachingImageManager.default().requestImage(for: assets, targetSize: imageSize, contentMode: .aspectFill, options: option) { (result, _) in
            if let image = result {
                self.showEdit(image)
//                resImg = image
                
            }
        }
        
//        return resImg
    }
    
    
    func showEdit(_ image : UIImage) {
        let nav : UINavigationController! = self.navigationController
        let vc = PhotoEditMypage(nibName: "PhotoEditMypage", bundle: nil)
        vc.imgList = image
        vc.m_bFlag = true
        vc.m_nStatus = 1
        nav.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickUserPage(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = gMeInfo.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickPoint(_ sender: Any) {
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserLikeViewController = (storyboard.instantiateViewController(withIdentifier: "USER_LIKE_VIEW") as! UserLikeViewController)
        vc.usrUid = gMeInfo.usrUid
        vc.point = gMeInfo.point
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickItem(_ sender: Any) {
        dic["isScrollDown"] = true
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = gMeInfo.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //팔로워
    @IBAction func onClickWi(_ sender: Any) {

        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
        vc.following = false
        vc.usrUid = gMeInfo.usrUid
        vc.topName = gMeInfo.usrNckNm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //팔로잉
    @IBAction func onClickIng(_ sender: Any) {
       
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
        vc.following = true
        vc.usrUid = gMeInfo.usrUid
        vc.topName = gMeInfo.usrNckNm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //나의 상품 페이지
    @IBAction func onClickComPage(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = gMeInfo.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    //구매내역
    @IBAction func onClickBuy(_ sender: Any) {
        pushVC("MYPAGE_BUYINGINFO_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //셀럽머니
    @IBAction func onClickSelluvMoney(_ sender: Any) {
        pushVC("MYPAGE_SELLUVMONEY_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //판매내역
    @IBAction func onClickSell(_ sender: Any) {
        pushVC("MYPAGE_SELLINGINFO_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //공지사항
    @IBAction func onClickHelp(_ sender: Any) {
        pushVC("MYPAGE_SELLUVNEWS_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //이벤트
    @IBAction func onClickEvent(_ sender: Any) {
        pushVC("MYPAGE_EVENT_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //개인정보 수정
    @IBAction func onClickInfoSettings(_ sender: Any) {
        pushVC("MYPAGE_PRIVATE_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //알림 설정
    @IBAction func onClickAlarmSettings(_ sender: Any) {
        pushVC("MYPAGE_ALARMSETTING_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //찾고 있는 아이템
    @IBAction func onClickFindItem(_ sender: Any) {
        pushVC("MYPAGE_FINDITEM_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //판매자 설정
    @IBAction func onClickSalesSettings(_ sender: Any) {
        pushVC("MYPAGE_SALESMAN_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //카드관리
    @IBAction func onClickCard(_ sender: Any) {
        pushVC("MYPAGE_CARDVIEW_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //계좌관리
    @IBAction func onClickNumber(_ sender: Any) {
        pushVC("MYPAGE_ACCOUNT_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onClickSelluvHelper(_ sender: Any) {
        SelluvServicePopup.show(self)
    }
    
    //판매 및 환불정책
    @IBAction func onClickSellPolicy(_ sender: Any) {
        pushVC("MYPAGE_SALEPOLICY_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //faq
    @IBAction func onClickFAQ(_ sender: Any) {
        pushVC("MYPAGE_FAQ_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //1:1 문의
    @IBAction func onClickOnetoOne(_ sender: Any) {
        TravelPopup.show(self, code: "") { (type) in
            if type == "1" {
                self.pushVC("MYPAGE_CS_QA_VIEW", storyboard: "Mypage", animated: true)
            } else {
                let settings = ChannelPluginSettings(pluginKey: kPluginKey)
                ChannelIO.delegate = self
//                ChannelIO.boot(with: settings) { completion in
//                    ChannelIO.open(animated: true)
//                }
                ChannelIO.boot(with: settings)
            }
        }
    }
    
    //이용약관
    @IBAction func onClickBottomHelp(_ sender: Any) {
        pushVC("MYPAGE_POLICY_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //개인정보 처리방침
    @IBAction func onClickInfoComment(_ sender: Any) {
        pushVC("MYPAGE_PRIVACY_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //회원탈퇴
    @IBAction func onClickExit(_ sender: Any) {
        
        let alertController = UIAlertController(title: NSLocalizedString("alarm", comment:""), message: NSLocalizedString("user_exit_alert_msg", comment:""), preferredStyle: .alert)
        let actOK = UIAlertAction(title: NSLocalizedString("confirm", comment:""), style: .default) { (action:UIAlertAction) in
            self.reqUserExit()
        }
        
        let actCancel = UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .default) { (action:UIAlertAction) in
            
        }
        
        alertController.addAction(actOK)
        alertController.addAction(actCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
// UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainEventTableViewCell", for: indexPath as IndexPath) as! MainEventTableViewCell
        
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnGo2Detail(_:)), for: UIControlEvents.touchUpInside)
        
        let dic : Event = arrEventList[indexPath.row]
        cell._lbTitle.text = dic.title
        
        return cell
    }
    
// UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func willOpenMessenger() {
        print("will show")
    }
    
    func willCloseMessenger() {
        print("will hide")
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 회원탈퇴
    func reqUserExit() {
        
        gProgress.show()
        Net.userExit(
            accessToken: gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
            
            self.userLogout()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func userLogout() {
        CommonUtil.showToast(NSLocalizedString("userexit_success", comment:""))
        gMeInfo.initial()
      
        pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)

    }
    
    // 회원 상세정보 얻기
    func getMyDetailInfo() {
        
        gProgress.show()
        Net.getMyDetailInfo(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.MyDetailInfoResult
                self.getMyInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //회원 상세정보 얻기 결과
    func getMyInfoResult(_ data: Net.MyDetailInfoResult) {
       
        gMeInfo.set(data.usr, pass: gMeInfo.usrPass, accesstoken: gMeInfo.token)
        gUsrAddress = data.addressList
        
        //evnet
        arrEventList = data.myPageEventList
        _htDetailEvent.constant = CGFloat(arrEventList.count * 45)
        _tvEventList.reloadData()

        btnNickname.setTitle(gMeInfo.usrNckNm,for: .normal)
        _imgProfile.kf.setImage(with: URL(string: gMeInfo.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        lblitemcnt.text = String(data.countPdt)
        lblFollowerCnt.text = String(data.usrFollowerCount)
        lblFollowingCnt.text = String(data.usrFollowingCount)
        lblPoint.text = String(gMeInfo.point)
        lblBuyCnt.text = String(data.countBuy)
        lblSellCnt.text = String(data.countSell)
        lblNoticeCnt.text = String(data.countUnreadNotice)
        lblMoney.text = CommonUtil.formatNum(gMeInfo.money)
        
        getAlarmcount()
    }
    
    // 알림 갯수 얻기
    func getAlarmcount() {
        
        gProgress.show()
        Net.getAlarmCnt(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AlarmCountResult
                if res.totalCount == 0 {
                    self._vwAlarm.isHidden = true
                }
                else {
                    self._vwAlarm.isHidden = false
                }
                self.lblAlarmCnt.text = String(res.totalCount)
                
                self.getPdtLikeUpdateYn()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 좋아요 업데이트뱃지 표시여부
    func getPdtLikeUpdateYn() {
        
        gProgress.show()
        Net.getLikeUpdateStatus(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.PdtLikeUpdateStatusResult
                
                if res.status {
                    self.ivPdtLikeBadge.image = UIImage.init(named: "ic_thema_heart")
                } else {
                    self.ivPdtLikeBadge.image = UIImage.init(named: "ic_thema_heart_off")
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 프로필이미지 업데이트
    func updateProfile(filename : String) {
        
        gProgress.show()
        Net.updateProfile(
            accessToken     : gMeInfo.token,
            filename        : filename,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("profile_modify_success", comment:""))
                self._imgProfile.image = self.profileImg
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 한개 파일 업로드
    func uploadFile( data : Data) {
        
        gProgress.show()
        Net.uploadFile(
            file            : data,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UploadFileResult
                self.updateProfile(filename: res.fileName)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
