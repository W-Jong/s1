//
//  CardViewViewController.swift
//  selluv
//  카드관리 목록
//  Created by Dev on 12/10/17.
//  modified by PJH on 06/03/18.

import UIKit
protocol CardViewViewControllerDelegate {
    func onConfirm(datainfo : UsrCardDto)
}

class CardViewViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _vwCardSave: UIView!
    @IBOutlet weak var _vwCardSaveMain: UIView!
    @IBOutlet weak var _vwCardSaveLine: UIView!
    
    @IBOutlet weak var _tvList: UITableView!
    @IBOutlet weak var _btnCardSave: UIButton!
    
    var delegate : CardViewViewControllerDelegate!
    var nCurCardUid = -1
    var pageStatus : String!
    var arrCardList : Array<UsrCardDto> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCardList()
    }
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "CardViewTVC", bundle: nil), forCellReuseIdentifier: "CardViewTVC")
        
        _vwCardSaveMain.layer.cornerRadius = 6
        _vwCardSaveLine.layer.cornerRadius = 6
    }
    
    @IBAction func onClickBg(_ sender: Any) {
    }
    
    @objc func onBtnSelect(_ sender: UIButton) {
        
        let dic : UsrCardDto = arrCardList[sender.tag]
        if pageStatus == "NEGO" {
            popVC()
            delegate?.onConfirm(datainfo: dic)
        } else {
            reqSelCard(_uid: dic.usrCardUid)
        }
        
    }

    @objc func onBtnDelete(_ sender: UIButton) {
        
        let dic : UsrCardDto = arrCardList[sender.tag]
        reqdelCard(_uid: dic.usrCardUid)
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickCardNew(_ sender: Any) {
        pushVC("MYPAGE_CARD_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        pushVC("MYPAGE_CARD_VIEW", storyboard: "Mypage", animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCardList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardViewTVC", for: indexPath as IndexPath) as! CardViewTVC
        
        let dic : UsrCardDto = arrCardList[indexPath.row]
        cell.lblCardNum.text = CommonUtil.convertNum(data: dic.cardNumber)
        cell._cardName.text = dic.cardName
        if dic.usrCardUid == nCurCardUid {
            cell._imgCheckBox.image = UIImage.init(named: "ic_checkbox_on")
        } else {
            cell._imgCheckBox.image = UIImage.init(named: "ic_checkbox_off")
        }
        
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action: #selector(self.onBtnSelect(_:)), for: UIControlEvents.touchUpInside)

        cell._btnClose.tag = indexPath.row
        cell._btnClose.addTarget(self, action: #selector(self.onBtnDelete(_:)), for: UIControlEvents.touchUpInside)

        return cell
    }
    
    //MARK: - UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 카드 목록 얻기
    func getCardList() {
        
        gProgress.show()
        Net.getCardList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UsrCardResult
                self.getCardListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 카드 목록 얻기 결과
    func getCardListResult(_ data: Net.UsrCardResult) {
        
        arrCardList = data.list
        if arrCardList.count == 0 {
            _vwCardSave.isHidden = false
        } else {
            _vwCardSave.isHidden = true
            
            let dic : UsrCardDto = arrCardList[0]
            nCurCardUid = dic.usrCardUid
            
            _tvList.reloadData()
        }
    }
    
    //카드를 선택한다.
    func reqSelCard(_uid : Int) {
        
        gProgress.show()
        Net.selCard(
            accessToken     : gMeInfo.token,
            usrCardUid      : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.nCurCardUid = _uid
                self.getCardList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //카드를 삭제한다.
    func reqdelCard(_uid : Int) {
        
        gProgress.show()
        Net.delCard(
            accessToken     : gMeInfo.token,
            usrCardUid      : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("card_del_success", comment:""))
                self.getCardList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
