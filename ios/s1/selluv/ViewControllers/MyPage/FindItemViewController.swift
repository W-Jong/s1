//
//  FindItemViewController.swift
//  selluv
//  찾고 있는 아이템
//  Created by Dev on 12/10/17.
//  modified by PJH on 05/03/18.

import UIKit
import SwiftyJSON
import DGElasticPullToRefresh

class FindItemViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    @IBOutlet weak var _btnOption: UIButton!
    
    var m_bOption = false
    
    var arrUsrWishList : Array<FindItemDto> = []
    var nPage : Int = 0
    var isLast : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initData()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        _tvList.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.initData()
            })
            }, loadingView: loadingView)
        _tvList.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        _tvList.dg_setPullToRefreshBackgroundColor(_tvList.backgroundColor!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "FindItemTableViewCell", bundle: nil), forCellReuseIdentifier: "FindItemTableViewCell")
    }
    
    func initData() {
        nPage = 0
        
        getUsrWishList()
    }
    
    @objc func onBnItemDelete(_ sender: UIButton) {
        
        let dicFindItem : FindItemDto = arrUsrWishList[sender.tag]

        let dicUsrWish : JSON = dicFindItem.usrWish
        let usrWishUid : Int = dicUsrWish["usrWishUid"].intValue
        
        var arr : Array<String> = []
        arr.append(String(usrWishUid))
        
        delUsrWish(uids: arr)
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickOption(_ sender: Any) {
        if _btnOption.isSelected {
            _btnOption.isSelected = false
            m_bOption = false
        } else {
            _btnOption.isSelected = true
            m_bOption = true
        }
        
        _tvList.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsrWishList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FindItemTableViewCell", for: indexPath as IndexPath) as! FindItemTableViewCell
        
        let dicFindItem : FindItemDto = arrUsrWishList[indexPath.row]
        
        if m_bOption {
            cell._wdBtns.constant = 67
            cell._btnDelect.isHidden = false
        } else {
            cell._wdBtns.constant = 20
            cell._btnDelect.isHidden = true
        }
        
        let dicBrand : JSON = dicFindItem.brand
        let dicCategory : JSON  = dicFindItem.category
        let dicUsrWish : JSON = dicFindItem.usrWish
        
        let brandName : String = dicBrand["nameKo"].stringValue
        var pdtName : String = ""
        let pdtGroup : Int = dicUsrWish["pdtGroup"].intValue
        if pdtGroup == 1 {//남성
            pdtName = NSLocalizedString("male", comment:"")
        } else if pdtGroup == 2 { //여성
            pdtName = NSLocalizedString("female", comment:"")
        } else { //키즈
            pdtName = NSLocalizedString("kids", comment:"")
        }
        
        let categoryName : String = dicCategory["categoryName"].stringValue
        let modelName : String = dicUsrWish["pdtModel"].stringValue
        let pdtSize : String = dicUsrWish["pdtSize"].stringValue
        let colorName : String = dicUsrWish["colorName"].stringValue
        
        cell.lblBrand.text = brandName
        cell.lblPdt.text = pdtName
        cell.lblCategory.text = categoryName
        cell.lblModel.text = modelName
        cell.lblSize.text = pdtSize
        cell.lblcolor.text = colorName

        cell.lblBrandEn.text = dicBrand["nameEn"].stringValue
        cell.lblbrandKo.text = brandName + pdtName + colorName + categoryName + modelName
        
        cell._btnDelect.tag = indexPath.row
        cell._btnDelect.addTarget(self, action: #selector(onBnItemDelete(_:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    //MARK: - UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 찾는 아이템 목록 얻기
    func getUsrWishList() {
        
        gProgress.show()
        Net.getUsrWishList(
            accessToken: gMeInfo.token,
            page: nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self._tvList.dg_stopLoading()
                let res = result as! Net.FindItemResult
                self.getUsrWishListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
    //찾는 아이템 목록 얻기 결과
    func getUsrWishListResult(_ data: Net.FindItemResult) {
        print("%d", data.list.count)
        
        arrUsrWishList = data.list
        isLast = data.last
        
        _tvList.reloadData()
        
    }
    
    // 찾는 아이템 삭제
    func delUsrWish(uids : Array<String>) {
        
        gProgress.show()
        Net.delFindItem(
            uids: uids,
            accessToken: gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("delete_success", comment:""))
                self._tvList.reloadData()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
}
