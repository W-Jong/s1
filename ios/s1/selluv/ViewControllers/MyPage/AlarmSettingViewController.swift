//
//  AlarmSettingViewController.swift
//  selluv
//
//  Created by Dev on 12/10/17.
//  modified by PJH on 04/03/18.

import UIKit

class AlarmSettingViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var _vwMenuBar: UIView!
    @IBOutlet weak var _vwMenuBarTab: UIView!
    @IBOutlet weak var _vwMenuBarTabTrailing: NSLayoutConstraint!
    @IBOutlet weak var _vwMenuBarTabLeading: NSLayoutConstraint!
    @IBOutlet weak var _scvList: UIScrollView!
    
    @IBOutlet weak var _btnTabBusiness: UIButton!
    @IBOutlet weak var _btnTabNews: UIButton!
    @IBOutlet weak var _btnTabComment: UIButton!
    
    @IBOutlet weak var _btnBusinessAlarm: UIButton!
    @IBOutlet weak var _btn1_1: UISwitch!
    @IBOutlet weak var _btn1_2: UISwitch!
    @IBOutlet weak var _btn1_3: UISwitch!
    @IBOutlet weak var _btn1_4: UISwitch!
    @IBOutlet weak var _btn1_5: UISwitch!
    @IBOutlet weak var _btn1_6: UISwitch!
    @IBOutlet weak var _btn1_7: UISwitch!
    @IBOutlet weak var _btn1_8: UISwitch!
    @IBOutlet weak var _btn1_9: UISwitch!
    @IBOutlet weak var _btn1_10: UISwitch!
    
    @IBOutlet weak var _btnNewsAlarm: UIButton!
    @IBOutlet weak var _btn2_1: UISwitch!
    @IBOutlet weak var _btn2_2: UISwitch!
    @IBOutlet weak var _btn2_3: UISwitch!
    @IBOutlet weak var _btn2_4: UISwitch!
    @IBOutlet weak var _btn2_5: UISwitch!
    @IBOutlet weak var _btn2_6: UISwitch!
    @IBOutlet weak var _btn2_7: UISwitch!
    @IBOutlet weak var _btn2_8: UISwitch!
    @IBOutlet weak var _btn2_9: UISwitch!
    @IBOutlet weak var _btn2_10: UISwitch!
    @IBOutlet weak var _btn2_11: UISwitch!
    @IBOutlet weak var _btn2_12: UISwitch!
    
    @IBOutlet weak var _btnCommentAlarm: UIButton!
    @IBOutlet weak var _btn3_1: UISwitch!
    @IBOutlet weak var _btn3_2: UISwitch!
    
    enum EAlarmTab : Int {
        case business = 0
        case news
        case comment
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getUserSettingInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _scvList.delegate = self
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func AlarmBusiness(_ sender: Any) {
        if !_btnBusinessAlarm.isSelected {
            _btnBusinessAlarm.isSelected = true
            _btn1_1.isOn = false
            _btn1_2.isOn = false
            _btn1_3.isOn = false
            _btn1_4.isOn = false
            _btn1_5.isOn = false
            _btn1_6.isOn = false
            _btn1_7.isOn = false
            _btn1_8.isOn = false
            _btn1_9.isOn = false
            _btn1_10.isOn = false
        } else {
            _btnBusinessAlarm.isSelected = false
            _btn1_1.isOn = true
            _btn1_2.isOn = true
            _btn1_3.isOn = true
            _btn1_4.isOn = true
            _btn1_5.isOn = true
            _btn1_6.isOn = true
            _btn1_7.isOn = true
            _btn1_8.isOn = true
            _btn1_9.isOn = true
            _btn1_10.isOn = true
        }
        reqAlarmSetting()
    }
    
    func setAlarmStatus(index : Int, status : Int){
        
        switch index {
        case 0:
            _btn1_1.isOn = status == 1
            break
        case 1:
            _btn1_2.isOn = status == 1
            break
        case 2:
            _btn1_3.isOn = status == 1
            break
        case 3:
            _btn1_4.isOn = status == 1
            break
        case 4:
            _btn1_5.isOn = status == 1
            break
        case 5:
            _btn1_6.isOn = status == 1
            break
        case 6:
            _btn1_7.isOn = status == 1
            break
        case 7:
            _btn1_8.isOn = status == 1
            break
        case 8:
            _btn1_9.isOn = status == 1
            break
        case 9:
            _btn1_10.isOn = status == 1
            break

        default:
            break
        }
        
        
        if !_btn1_1.isOn && !_btn1_2.isOn && !_btn1_3.isOn && !_btn1_4.isOn && !_btn1_5.isOn && !_btn1_6.isOn && !_btn1_7.isOn && !_btn1_8.isOn && !_btn1_9.isOn && !_btn1_10.isOn {
            _btnBusinessAlarm.isSelected = true
        } else {
            _btnBusinessAlarm.isSelected = false
        }
        
    }
    
    func setNewsStatus(index : Int, status : Int){
        
        switch index {
        case 0:
            _btn2_1.isOn = status == 1
            break
        case 1:
            _btn2_2.isOn = status == 1
            break
        case 2:
            _btn2_3.isOn = status == 1
            break
        case 3:
            _btn2_4.isOn = status == 1
            break
        case 4:
            _btn2_5.isOn = status == 1
            break
        case 5:
            _btn2_6.isOn = status == 1
            break
        case 6:
            _btn2_7.isOn = status == 1
            break
        case 7:
            _btn2_8.isOn = status == 1
            break
        case 8:
            _btn2_9.isOn = status == 1
            break
        case 9:
            _btn2_10.isOn = status == 1
            break
        case 10:
            _btn2_11.isOn = status == 1
            break
        case 11:
            _btn2_12.isOn = status == 1
            break
            
        default:
            break
        }
       
        if !_btn2_1.isOn && !_btn2_2.isOn && !_btn2_3.isOn && !_btn2_4.isOn && !_btn2_5.isOn && !_btn2_6.isOn && !_btn2_7.isOn && !_btn2_8.isOn && !_btn2_9.isOn && !_btn2_10.isOn && !_btn2_11.isOn && !_btn2_12.isOn {
            _btnNewsAlarm.isSelected = true
        } else {
            _btnNewsAlarm.isSelected = false
        }
    }
    
    @IBAction func AlarmNews(_ sender: Any) {
        if !_btnNewsAlarm.isSelected {
            _btnNewsAlarm.isSelected = true
            _btn2_1.isOn = false
            _btn2_2.isOn = false
            _btn2_3.isOn = false
            _btn2_4.isOn = false
            _btn2_5.isOn = false
            _btn2_6.isOn = false
            _btn2_7.isOn = false
            _btn2_8.isOn = false
            _btn2_9.isOn = false
            _btn2_10.isOn = false
            _btn2_11.isOn = false
            _btn2_12.isOn = false

        } else {
            _btnNewsAlarm.isSelected = false
            _btn2_1.isOn = true
            _btn2_2.isOn = true
            _btn2_3.isOn = true
            _btn2_4.isOn = true
            _btn2_5.isOn = true
            _btn2_6.isOn = true
            _btn2_7.isOn = true
            _btn2_8.isOn = true
            _btn2_9.isOn = true
            _btn2_10.isOn = true
            _btn2_11.isOn = true
            _btn2_12.isOn = true
        }
        reqAlarmSetting()
    }
    
    @IBAction func AlarmComment(_ sender: Any) {
        if !_btnCommentAlarm.isSelected {
            _btnCommentAlarm.isSelected = true
            _btn3_1.isOn = false
            _btn3_2.isOn = false
            
        } else {
            _btnCommentAlarm.isSelected = false
            _btn3_1.isOn = true
            _btn3_2.isOn = true
        }
        reqAlarmSetting()
    }
    
    @IBAction func AlarmAction(_ sender: Any) {
        
        if !_btn1_1.isOn && !_btn1_2.isOn && !_btn1_3.isOn && !_btn1_4.isOn && !_btn1_5.isOn && !_btn1_6.isOn && !_btn1_7.isOn && !_btn1_8.isOn && !_btn1_9.isOn && !_btn1_10.isOn {
            _btnBusinessAlarm.isSelected = true
        } else {
            _btnBusinessAlarm.isSelected = false
        }
        
        if !_btn2_1.isOn && !_btn2_2.isOn && !_btn2_3.isOn && !_btn2_4.isOn && !_btn2_5.isOn && !_btn2_6.isOn && !_btn2_7.isOn && !_btn2_8.isOn && !_btn2_9.isOn && !_btn2_10.isOn && !_btn2_11.isOn && !_btn2_12.isOn {
            _btnNewsAlarm.isSelected = true
        } else {
            _btnNewsAlarm.isSelected = false
        }
        
        if !_btn3_1.isOn && !_btn3_2.isOn {
            _btnCommentAlarm.isSelected = true
        } else {
            _btnCommentAlarm.isSelected = false
        }
        
        reqAlarmSetting()
    }
    
    func scrollUp() {
        //        if vwTop.frame.origin.y >= 0 {
        //            return
        //        }
        //
        //        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
        //
        //            self.vwTop.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        //            self.vwMenu.frame = CGRect.init(x: 0, y: self.view.frame.size.height - self.vwMenu.frame.size.height, width: self.view.frame.size.width, height: self.vwMenu.frame.size.height)
        //            self.btnScroll2Top.frame = CGRect.init(x: self.view.frame.size.width - self.btnScroll2Top.frame.size.width - 15, y: self.view.frame.size.height, width: self.btnScroll2Top.frame.size.width, height: self.btnScroll2Top.frame.size.height)
        //        })
        
        if scrollDirection == .up {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollDown() {
        
        //        if vwTop.frame.origin.y < 0 {
        //            return
        //        }
        //        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
        //            self.vwTop.frame = CGRect.init(x: 0, y: -124, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        //            self.vwMenu.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height:              self.vwMenu.frame.size.height)
        //            self.btnScroll2Top.frame = CGRect.init(x: self.view.frame.size.width - self.btnScroll2Top.frame.size.width - 15, y: self.view.frame.size.height - 65, width: self.btnScroll2Top.frame.size.width, height: self.btnScroll2Top.frame.size.height)
        //        })
        
        if scrollDirection == .down {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func pageMoved(pageNum: Int) {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        _btnTabBusiness.isSelected = false
        _btnTabNews.isSelected = false
        _btnTabComment.isSelected = false
        
        switch pageNum {
        case EAlarmTab.business.rawValue :
            _btnTabBusiness.isSelected = true
            clickedBtn = _btnTabBusiness
        case EAlarmTab.news.rawValue :
            _btnTabNews.isSelected = true
            clickedBtn = _btnTabNews
        case EAlarmTab.comment.rawValue :
            _btnTabComment.isSelected = true
            clickedBtn = _btnTabComment
        default:
            _btnTabBusiness.isSelected = true
            clickedBtn = _btnTabBusiness
        }
        
        _vwMenuBar.removeConstraint(_vwMenuBarTabLeading)
        _vwMenuBar.removeConstraint(_vwMenuBarTabTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        _vwMenuBarTabLeading = NSLayoutConstraint(item: _vwMenuBarTab, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: 0.0)
        _vwMenuBarTabTrailing = NSLayoutConstraint(item: _vwMenuBarTab, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        
        _vwMenuBar.addConstraint(_vwMenuBarTabLeading)
        _vwMenuBar.addConstraint(_vwMenuBarTabTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
    }
    
    @IBAction func onClickAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case EAlarmTab.business.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 0
        case EAlarmTab.news.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 1
        case EAlarmTab.comment.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 2
        default:
            _scvList.contentOffset.x = _scvList.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved(pageNum: clickedBtn.tag)
    }
    
    //MARK: - UITableViewDataSource protocol functions
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case _scvList:
            
            let newOffset = scrollView.contentOffset.x
            let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            self.pageMoved(pageNum: pageNum)
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
            
        default:
            return
        }
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 알림 설정 얻기
    func getUserSettingInfo() {
        
        gProgress.show()
        Net.getAlarmSettingInfo(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UserAlarmResult
                self.getAlarmSettingInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //회원 상세정보 얻기 결과
    func getAlarmSettingInfoResult(_ data: Net.UserAlarmResult) {
        
        let dealSetting : String = data.dealSetting
        for i in 0..<10 {
            let tempstr = dealSetting.substring(with: dealSetting.index(dealSetting.startIndex, offsetBy: i)..<dealSetting.index(dealSetting.startIndex, offsetBy: i + 1))
            setAlarmStatus(index: i, status: Int(tempstr)!)
        }

        let newsSetting : String = data.newsSetting
        for i in 0..<12{
            let tempstr = newsSetting.substring(with: newsSetting.index(newsSetting.startIndex, offsetBy: i)..<newsSetting.index(newsSetting.startIndex, offsetBy: i + 1))
            setNewsStatus(index: i, status: Int(tempstr)!)
        }
        
        let replySetting : String = data.replySetting
        let tempstr1 = replySetting.substring(with: replySetting.index(replySetting.startIndex, offsetBy: 0)..<replySetting.index(replySetting.startIndex, offsetBy: 1))
        let tempstr2 = replySetting.substring(with: replySetting.index(replySetting.startIndex, offsetBy: 1)..<replySetting.index(replySetting.startIndex, offsetBy: 2))
    
        _btn3_1.isOn = Int(tempstr1) == 1
        _btn3_2.isOn = Int(tempstr2) == 1
        
        if !_btn3_1.isOn && !_btn3_2.isOn {
            _btnCommentAlarm.isSelected = true
        } else {
            _btnCommentAlarm.isSelected = false
        }
    }
    
    // 알림 설정 얻기
    func reqAlarmSetting() {
        
        let businessAlarm = _btnBusinessAlarm.isSelected == true ? 1 : 0
        let newAlarm = _btnNewsAlarm.isSelected == true ? 1 : 0
        let commentAlarm = _btnCommentAlarm.isSelected == true ? 1 : 0
        
        //deal
        let btn1_1 = _btn1_1.isOn == true ? 1 : 0
        let btn1_2 = _btn1_2.isOn == true ? 1 : 0
        let btn1_3 = _btn1_3.isOn == true ? 1 : 0
        let btn1_4 = _btn1_4.isOn == true ? 1 : 0
        let btn1_5 = _btn1_5.isOn == true ? 1 : 0
        let btn1_6 = _btn1_6.isOn == true ? 1 : 0
        let btn1_7 = _btn1_7.isOn == true ? 1 : 0
        let btn1_8 = _btn1_8.isOn == true ? 1 : 0
        let btn1_9 = _btn1_9.isOn == true ? 1 : 0
        let btn1_10 = _btn1_10.isOn == true ? 1 : 0
        let dealString = String(btn1_1) + String(btn1_2) + String(btn1_3) + String(btn1_4) + String(btn1_5) + String(btn1_6) + String(btn1_7) + String(btn1_8) + String(btn1_9) + String(btn1_10)
        
        //new
        let btn2_1 = _btn2_1.isOn == true ? 1 : 0
        let btn2_2 = _btn2_2.isOn == true ? 1 : 0
        let btn2_3 = _btn2_3.isOn == true ? 1 : 0
        let btn2_4 = _btn2_4.isOn == true ? 1 : 0
        let btn2_5 = _btn2_5.isOn == true ? 1 : 0
        let btn2_6 = _btn2_6.isOn == true ? 1 : 0
        let btn2_7 = _btn2_7.isOn == true ? 1 : 0
        let btn2_8 = _btn2_8.isOn == true ? 1 : 0
        let btn2_9 = _btn2_9.isOn == true ? 1 : 0
        let btn2_10 = _btn2_10.isOn == true ? 1 : 0
        let btn2_11 = _btn2_11.isOn == true ? 1 : 0
        let btn2_12 = _btn2_12.isOn == true ? 1 : 0
        let newString = String(btn2_1) + String(btn2_2) + String(btn2_3) + String(btn2_4) + String(btn2_5) + String(btn2_6) + String(btn2_7) + String(btn2_8) + String(btn2_9) + String(btn2_10) + String(btn2_11) + String(btn2_12)

        //comment
        let btn3_1 = _btn3_1.isOn == true ? 1 : 0
        let btn3_2 = _btn3_2.isOn == true ? 1 : 0
        let commentString = String(btn3_1) + String(btn3_2)
        
        gProgress.show()
        Net.reqAlarmSetting(
            accessToken: gMeInfo.token,
            dealSetting: dealString,
            dealYn: String(businessAlarm),
            newsSetting: newString,
            newsYn: String(newAlarm),
            replySetting: commentString,
            replyYn: String(commentAlarm),
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil .showToast(NSLocalizedString("alarm_setting_success", comment:""))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
