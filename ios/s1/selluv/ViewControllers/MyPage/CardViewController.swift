//
//  CardViewController.swift
//  selluv
//  카드 등록
//  Created by Dev on 12/9/17.
//  modified by PJH on 06/03/18.

import UIKit

class CardViewController: BaseViewController, UITextFieldDelegate {

    
    @IBOutlet weak var _btnPrivatePerson: UIButton!
    @IBOutlet weak var _btnBusinessPerson: UIButton!
    @IBOutlet weak var _btnOk: UIButton!
    @IBOutlet weak var _btnViewGuide: UIButton!
    
    @IBOutlet weak var _tfMonth: UITextField!
    @IBOutlet weak var _tfYear: UITextField!
    
    @IBOutlet weak var _tfCardNum1: UITextField!
    @IBOutlet weak var _tfCardNum2: UITextField!
    @IBOutlet weak var _tfCardNum3: UITextField!
    @IBOutlet weak var _tfCardNum4: UITextField!
    
    @IBOutlet weak var _tfBirthday: UITextField!
    @IBOutlet weak var _tfBusinessNum2: UITextField!
    @IBOutlet weak var _tfBusinessNum3: UITextField!
    @IBOutlet weak var _htBirthday: NSLayoutConstraint!
    @IBOutlet weak var _lbBirthday: UILabel!
    
    @IBOutlet weak var _btnSave: UIButton!
    @IBOutlet weak var _tfSecurityNum: UITextField!
    
    var cardNum : String!
    var validDate : String!
    var birthday : String!
    var validNum : String!
    var password : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tfCardNum1.delegate = self
        _tfCardNum2.delegate = self
        _tfCardNum3.delegate = self
        _tfCardNum4.delegate = self
        _tfBirthday.delegate = self
        _tfBusinessNum2.delegate = self
        _tfBusinessNum3.delegate = self
        _tfSecurityNum.delegate = self
        
        _tfCardNum1.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfCardNum2.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfCardNum3.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfCardNum4.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfBirthday.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfBusinessNum2.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfBusinessNum3.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        _tfSecurityNum.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        
        _tfBirthday.placeholder = "YYMMDD"
    }
    
    func setRequestBtn() {
        
        var addrInputed = false
        
        if !_btnPrivatePerson.isSelected {
            if !_tfCardNum4.text!.isEmpty && !_tfCardNum3.text!.isEmpty && !_tfCardNum2.text!.isEmpty && !_tfCardNum1.text!.isEmpty && !_tfMonth.text!.isEmpty && !_tfYear.text!.isEmpty && !_tfBirthday.text!.isEmpty && !_tfBusinessNum2.text!.isEmpty && !_tfBusinessNum3.text!.isEmpty && !_tfSecurityNum.text!.isEmpty && _btnOk.isSelected {
                addrInputed = true
            }
        } else {
            if !_tfCardNum4.text!.isEmpty && !_tfCardNum3.text!.isEmpty && !_tfCardNum2.text!.isEmpty && !_tfCardNum1.text!.isEmpty && !_tfMonth.text!.isEmpty && !_tfYear.text!.isEmpty && !_tfBirthday.text!.isEmpty && !_tfSecurityNum.text!.isEmpty && _btnOk.isSelected {
                addrInputed = true
            }
        }
        
        _btnSave.isEnabled = addrInputed
        _btnSave.backgroundColor = _btnSave.isEnabled ? UIColor.black : UIColor.lightGray
        
    }
    
    func hideKeyboard() {
        _tfBirthday.resignFirstResponder()
        _tfCardNum1.resignFirstResponder()
        _tfCardNum2.resignFirstResponder()
        _tfCardNum3.resignFirstResponder()
        _tfCardNum4.resignFirstResponder()
        _tfSecurityNum.resignFirstResponder()
        _tfBusinessNum2.resignFirstResponder()
        _tfBusinessNum3.resignFirstResponder()
    }
    
    func  convertDateType( date1 : String) -> String! {
        var result : String = ""
        
        let year =  date1.substring(with: date1.index(date1.startIndex, offsetBy: 0)..<date1.index(date1.startIndex, offsetBy: 2))
        let month =  date1.substring(with: date1.index(date1.startIndex, offsetBy: 2)..<date1.index(date1.startIndex, offsetBy: 4))
        let day =  date1.substring(with: date1.index(date1.startIndex, offsetBy: 4)..<date1.index(date1.startIndex, offsetBy: 6))

        result = "20" + year + "-" + month + "-" + day
        return result
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickMonth(_ sender: Any) {
        hideKeyboard()
        DataMonthSelect.show(self){ (size2) in
            self._tfMonth.text = size2
        }
    }
    
    @IBAction func onClickYear(_ sender: Any) {
        hideKeyboard()
        DataYearSelect.show(self){ (size2) in
            self._tfYear.text = size2
        }
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        
        cardNum = String.init(format: "%@-%@-%@-%@", _tfCardNum1.text!, _tfCardNum2.text!, _tfCardNum3.text!, _tfCardNum4.text!)
        validDate = String.init(format: "%@-%@-01", _tfYear.text!, _tfMonth.text!)
        let tempBirth = _tfBirthday.text
        validNum = String.init(format: "%@%@%@", _tfBirthday.text!, _tfBusinessNum2.text!, _tfBusinessNum3.text!)
        password = _tfSecurityNum.text
        
        if cardNum.count != 19 || cardNum.count != 18 {
            MsgUtil.showUIAlert(NSLocalizedString("wrong_card_num", comment:""))
            return
        }

        if _btnPrivatePerson.isSelected {
            
            if tempBirth?.count != 6 {
                MsgUtil.showUIAlert(NSLocalizedString("wrong_birthday_type", comment:""))
                return
            }
            birthday = tempBirth
        } else {
            if validNum.count != 10 {
                MsgUtil.showUIAlert(NSLocalizedString("wrong_valid_num", comment:""))
                return
            }
        }
        
        regCard()
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        hideKeyboard()
    }
    
    @IBAction func onClickViewGuide(_ sender: Any) {
        pushVC("MYPAGE_CARDGUIDE_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onClickOK(_ sender: Any) {
        _btnOk.isSelected = !_btnOk.isSelected
        setRequestBtn()
    }
    
    @IBAction func onClickPrivate(_ sender: Any) {
        _tfBirthday.text = ""
        if _btnBusinessPerson.isSelected {
            _btnBusinessPerson.isSelected = false
            _btnPrivatePerson.isSelected = true
            _tfBusinessNum2.isHidden = true
            _tfBusinessNum3.isHidden = true
            _lbBirthday.text = "생년월일"
            _htBirthday.constant = 75
            _tfBirthday.placeholder = "YYMMDD"
        } else {
            _btnBusinessPerson.isSelected = true
            _btnPrivatePerson.isSelected = false
            _tfBusinessNum2.isHidden = false
            _tfBusinessNum3.isHidden = false
            _lbBirthday.text = "사업자번호"
            _htBirthday.constant = 50
            _tfBirthday.placeholder = ""
        }
    }
    
    @IBAction func onClickBusiness(_ sender: Any) {
        _tfBirthday.text = ""
        _tfBusinessNum2.text = ""
        _tfBusinessNum3.text = ""
        if _btnPrivatePerson.isSelected {
            _btnBusinessPerson.isSelected = true
            _btnPrivatePerson.isSelected = false
            _tfBusinessNum2.isHidden = false
            _tfBusinessNum3.isHidden = false
            _lbBirthday.text = "사업자번호"
            _htBirthday.constant = 50
            _tfBirthday.placeholder = ""
        } else {
            _btnBusinessPerson.isSelected = false
            _btnPrivatePerson.isSelected = true
            _tfBusinessNum2.isHidden = true
            _tfBusinessNum3.isHidden = true
            _lbBirthday.text = "생년월일"
            _htBirthday.constant = 75
            _tfBirthday.placeholder = "YYMMDD"
        }
    }
    
    @objc func textChanged(_ sender : UITextField) {
        let newString : NSString = NSString(string: sender.text!)
        
        if sender == _tfCardNum1 {
            if newString.length > 3 {
                _tfCardNum2.becomeFirstResponder()
            }
        }
        
        if sender == _tfCardNum2 {
            if newString.length > 3{
                _tfCardNum3.becomeFirstResponder()
            }
        }
        
        if sender == _tfCardNum3 {
            if newString.length > 3 {
                _tfCardNum4.becomeFirstResponder()
            }
        }
        
        if sender == _tfBirthday {
            if newString.length > 2 {
                if _btnBusinessPerson.isSelected {
                    _tfBusinessNum2.becomeFirstResponder()
                }                
            }
        }
        
        if sender == _tfBusinessNum2 {
            if newString.length > 1 {
                _tfBusinessNum3.becomeFirstResponder()
            }
        }
        
        if sender == _tfBusinessNum3 {
            if newString.length > 4 {
                return
            }
        }
        
        if sender == _tfCardNum4 {
            if newString.length > 3 {
            }
        }
        
        if sender == _tfSecurityNum {
            if newString.length > 1 {
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setRequestBtn()
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 카드 등록
    func regCard() {
        
        var kind : Int = 1
        var valid : String = ""
        if _btnPrivatePerson.isSelected {
            kind = 1
            valid = birthday
        } else {
            kind = 2
            valid = validNum
        }
        
        gProgress.show()
        Net.regCard(
            accessToken     : gMeInfo.token,
            cardNumber      : cardNum,
            kind            : kind,
            password        : password,
            validDate       : validDate,
            validNum        : valid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("card_reg_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}
