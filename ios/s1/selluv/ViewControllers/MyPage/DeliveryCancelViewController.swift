//
//  DeliveryCancelViewController.swift
//  selluv
//  예약 취소
//  Created by Dev on 12/12/17.
//  modified by PJH on 03/04/18.

import UIKit

class DeliveryCancelViewController: BaseViewController {

    @IBOutlet weak var tfDelivNum: UITextField!
    @IBOutlet weak var lbNum: UILabel!
    
    var dicDealHis   : DeliveryHistoryDao!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initUI() {
        
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickPopup(_ sender: Any) {
        ServicePopup.show(self)
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "예약 취소하겠습니까?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (UIAlertAction) in
            self.delDealDelivery()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 예약 취소
    func delDealDelivery() {
        
        gProgress.show()
        Net.delDealDelivery(
            accessToken     : gMeInfo.token,
            dealUid         : dicDealHis.dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("delivery_cancel", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}
