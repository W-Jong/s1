//
//  ServiceDeliveryViewController.swift
//  selluv
//  편의점 택배
//  Created by Gambler on 1/13/18.
//  modified by PJH on 03/04/18.

import UIKit

class ServiceDeliveryViewController: BaseViewController {

    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var lcContentBottom: NSLayoutConstraint!
    
    @IBOutlet var btnAddrs: [UIButton]!
    @IBOutlet weak var btnAddrSelect: UIButton!
    
    @IBOutlet weak var tfName: DesignableUITextField!
    @IBOutlet weak var tfContact: DesignableUITextField!
    @IBOutlet weak var tfAddr: DesignableUITextField!
    @IBOutlet weak var lbAddr: UILabel!
    
    @IBOutlet weak var btnRequest: UIButton!
    
    var dicDealDetail   : DealDetailDto!
    var addrInfo =
        [(name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : "")]
    var _curListNum : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func initData() {
        _curListNum = 0
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            addrInfo[i].name = dic.addressName
            addrInfo[i].contact = dic.addressPhone
            addrInfo[i].addr = dic.addressDetail
            addrInfo[i].code = dic.addressDetailSub
        }
        
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            if dic.status == 2 {
                _curListNum = i
            }
        }
        selectTabBtn(index: _curListNum)
    }
    
    func selectTabBtn(index : Int) {
        
        for i in 0..<3 {
            btnAddrs[i].isSelected = false
            btnAddrs[i].backgroundColor = UIColor(hex: 0xffffff)
        }
        
        btnAddrs[index].isSelected = true
        btnAddrs[index].backgroundColor = UIColor(hex: 0x42c2fe)
        
        _curListNum = index
        
        tfName.text = addrInfo[_curListNum].name
        tfContact.text = addrInfo[_curListNum].contact
        lbAddr.text = String.init(format: "%@\n[%@]", addrInfo[_curListNum].addr, addrInfo[_curListNum].code)

        setRequestBtn()
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                lcContentBottom.constant = 0
                
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        
        lcContentBottom.constant = 0
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func setRequestBtn() {
        
        var addrInputed = false
        
        if !(addrInfo[_curListNum].name.isEmpty) && !(addrInfo[_curListNum].contact.isEmpty) && !(addrInfo[_curListNum].addr.isEmpty) && !(addrInfo[_curListNum].code.isEmpty) {
            addrInputed = true
        }
        
        btnRequest.isEnabled = addrInputed
        btnRequest.backgroundColor = btnRequest.isEnabled ? UIColor.black : UIColor(hex: 0x999999)
        
        if addrInfo[_curListNum].addr != "" && addrInfo[_curListNum].code != "" {
            lbAddr.isHidden = false
            tfAddr.isHidden = true
        } else {
            lbAddr.isHidden = true
            tfAddr.isHidden = false
        }
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func backAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickPopup(_ sender: Any) {
        ServicePopup.show(self)
    }
    
    @IBAction func addrNumAction(_ sender: UIButton) {

       selectTabBtn(index: sender.tag)
    }
    
    @IBAction func addrSearchAction(_ sender: Any) {
        
        AddrSearch.show(self) { (addr, code) in
            self.lbAddr.text = String.init(format: "%@\n[%@]", addr, code)
            self.addrInfo[self._curListNum].addr = addr
            self.addrInfo[self._curListNum].code = code
  
            self.setRequestBtn()
        }
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        
        tfName.resignFirstResponder()
        tfContact.resignFirstResponder()
        
    }
    
    @IBAction func requestAction(_ sender: Any) {
        reqDealDelivery()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 택배 예약하기
    func reqDealDelivery() {
        let name = addrInfo[_curListNum].name
        let contact = addrInfo[_curListNum].contact
        let address = addrInfo[_curListNum].addr + "\n" + addrInfo[_curListNum].code
        
        if name == "" {
            CommonUtil.showToast(NSLocalizedString("empty_name", comment:""))
            return
        }
        
        if contact == "" {
            CommonUtil.showToast(NSLocalizedString("empty_contact", comment:""))
            return
        }
        
        if address == "" {
            CommonUtil.showToast(NSLocalizedString("empty_address", comment:""))
            return
        }
        
        gProgress.show()
        Net.reqDealDelivery(
            accessToken         : gMeInfo.token,
            dealUid             : dicDealDetail.deal.dealUid,
            recipientAddress    : address,
            recipientNm         : name,
            recipientPhone      : contact,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.reqDealDeliveryResult
                self.reqDealDeliveryResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 결과
    func reqDealDeliveryResult(_ data: Net.reqDealDeliveryResult) {
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_DELIVERYCANCEL_VIEW")) as! DeliveryCancelViewController
        vc.dicDealHis = data.deal
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension ServiceDeliveryViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfName {
            tfContact.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        case tfName :
            addrInfo[_curListNum].name = textField.text!
        case tfContact:
            addrInfo[_curListNum].contact = textField.text!
        default:
            break
        }
        
        setRequestBtn()
        
    }
    
}

