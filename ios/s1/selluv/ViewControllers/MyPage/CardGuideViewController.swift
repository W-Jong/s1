//
//  CardGuideViewController.swift
//  selluv
//
//  Created by Dev on 12/10/17.
//  modified by PJH on 06/03/18.

import UIKit

class CardGuideViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func initUI() {
        
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
}
