//
//  EventViewController.swift
//  selluv
//
//  Created by Dev on 12/11/17.
//

import UIKit
import DGElasticPullToRefresh

class EventViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    var arrEventList : Array<Event> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getEventList()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        _tvList.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.getEventList()
            })
            }, loadingView: loadingView)
        _tvList.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        _tvList.dg_setPullToRefreshBackgroundColor(_tvList.backgroundColor!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
    }
    
    @objc func onBtnGo2Detail(_ sender: UIButton) {
        
        let dic : Event = arrEventList[sender.tag]
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc : EventDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_EVENTDETAIL_VIEW") as! EventDetailViewController)
        vc.eventUid = dic.eventUid
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }    

    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath as IndexPath) as! EventTableViewCell
        
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnGo2Detail(_:)), for: UIControlEvents.touchUpInside)
        
        let dic : Event = arrEventList[indexPath.row]
        
        cell._imgEvent.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default"), options: [], progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 이벤트 목록 얻기
    func getEventList() {
        
        gProgress.show()
        Net.getEventList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                self._tvList.dg_stopLoading()
                let res = result as! Net.getEventListResult
                self.getEventResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //이벤트 목록 얻기 결과
    func getEventResult(_ data: Net.getEventListResult) {
        
        arrEventList = data.list
        _tvList.reloadData()
        
    }
    
}
