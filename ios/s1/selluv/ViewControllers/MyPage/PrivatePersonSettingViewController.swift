//
//  PrivatePersonSettingViewController.swift
//  selluv
//  개인정보 수정
//  Created by Dev on 12/11/17.
//  modified by PJH on 22/03/18.

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AEXML
import TwitterKit
import SwiftInstagram

class PrivatePersonSettingViewController: BaseViewController, UITextViewDelegate {
    
    @IBOutlet weak var btnAddrSelect: UIButton!
    @IBOutlet var btnAddrs: [UIButton]!
    
    @IBOutlet weak var tfName: DesignableUITextField!
    @IBOutlet weak var tfContact: DesignableUITextField!
    @IBOutlet weak var tfAddr: DesignableUITextField!
    
    @IBOutlet weak var _tfEmail: UITextField!
    @IBOutlet weak var tfUsrPhone: UITextField!
    @IBOutlet weak var lblAddr: UILabel!
    
    //sns status
    @IBOutlet weak var _btnFacebook: UIButton!
    @IBOutlet weak var _btnNaver: UIButton!
    @IBOutlet weak var _btnKakao: UIButton!
    @IBOutlet weak var _btnInstagram: UIButton!
    @IBOutlet weak var _btnTwitter: UIButton!
    
    var addrTabIndex : Int = 0
    var addrInfo =
        [(name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : "")]
    
    @IBOutlet weak var _scvMain: UIScrollView!
    @IBOutlet weak var tpScvMain: NSLayoutConstraint!
    
    var m_activeTextField : UITextField?
    var usrPhone        : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getSNSStatusInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tfName.resignFirstResponder()
        tfContact.resignFirstResponder()
        tfAddr.resignFirstResponder()
        _tfEmail.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        tfName.delegate = self
        tfContact.delegate = self
        _tfEmail.delegate = self
        tfUsrPhone.isEnabled = false
        tfAddr.isEnabled = false
        
        usrPhone = gMeInfo.usrPhone
        _tfEmail.text = gMeInfo.usrMail
        self.tfUsrPhone.text = CommonUtil.getDashPhoneNum(num: usrPhone)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        btnAddrs[0].isSelected = true
        btnAddrs[0].backgroundColor = UIColor(hex: 0x42c2fe)
        
        tfName.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        tfContact.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            addrInfo[i].name = dic.addressName
            addrInfo[i].contact = dic.addressPhone
            addrInfo[i].addr = dic.addressDetail
            addrInfo[i].code = dic.addressDetailSub
        }
        
        addrTabIndex = 0
        for i in 0..<3 {
            let dic : UsrAddressDao = gUsrAddress[i]
            if dic.status == 2 {
                addrTabIndex = i
            }
        }
        selectTabBtn(index: addrTabIndex)
    }
    
    func selectTabBtn(index : Int) {
        
        for i in 0..<3 {
            btnAddrs[i].isSelected = false
            btnAddrs[i].backgroundColor = UIColor(hex: 0xffffff)
        }
        
        btnAddrs[index].isSelected = true
        btnAddrs[index].backgroundColor = UIColor(hex: 0x42c2fe)
        
        addrTabIndex = index
        
        tfName.text = addrInfo[index].name
        tfContact.text = addrInfo[index].contact
        if addrInfo[index].addr != "" && addrInfo[index].code != ""{
            lblAddr.text = String.init(format: "%@\n[%@]", addrInfo[index].addr, addrInfo[index].code)
            tfAddr.isHidden = true
        } else {
            lblAddr.text = ""
            tfAddr.isHidden = false
        }
        
//        setRequestBtn()
    }
    
    func setRequestBtn() {
        
        for addr in addrInfo {
            if !addr.name.isEmpty && !addr.contact.isEmpty && !addr.addr.isEmpty {
                break
            }
        }
    }
    
    @objc func textChanged(_ sender : DesignableUITextField) {
        switch sender {
        case tfName :
            addrInfo[addrTabIndex].name = sender.text!
        case tfContact:
            addrInfo[addrTabIndex].contact = sender.text!
        default:
            break
        }
        
//        setRequestBtn()
    }

    @IBAction func onClickBack(_ sender: Any) {
        if !CommonUtil.validEmail(_tfEmail.text!) {
            MsgUtil.showUIAlert("이메일형식이 아닙니다.")
        }
        updateOtherUsrInfo()
    }
    
    @IBAction func addrNumAction(_ sender: UIButton) {
        
       selectTabBtn(index: sender.tag)
    }
    
    @IBAction func certiAction(_ sender: Any) {
        WebviewViewController.show(self, page: Webview.DANAL_SMS.rawValue, url: "") { (name, phone, birth, gender) in
            
            self.usrPhone = phone
            self.tfUsrPhone.text = CommonUtil.getDashPhoneNum(num: phone)
            
            self.updateBasisUsrInfo(name: name, phone: phone, birth: birth, gender: gender)
        }
    }
    
    @IBAction func addrSearchAction(_ sender: Any) {
        
        AddrSearch.show(self) { (addr, code) in
            
            self.lblAddr.text = String.init(format: "%@\n[%@]", addr, code)
            if self.lblAddr.text == "" {
                self.tfAddr.isHidden = false
            } else {
                self.tfAddr.isHidden = true
            }
            
            self.addrInfo[self.addrTabIndex].addr = addr
            self.addrInfo[self.addrTabIndex].code = code
            
//            self.setRequestBtn()
        }
    }
    
    @IBAction func onClickProfileSettings(_ sender: Any) {
        pushVC("MYPAGE_PROFILE_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //비밀번호 변경
    @IBAction func onClickSecuritySettings(_ sender: Any) {
        pushVC("MYPAGE_PASSSSETTING_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        tfName.resignFirstResponder()
        tfContact.resignFirstResponder()
        _tfEmail.resignFirstResponder()
    }

    //차단된 유저
    @IBAction func onClickBlockUser(_ sender: Any) {
        pushVC("MYPAGE_BLOCKUSER_VIEW", storyboard: "Mypage", animated: true)
    }
    
    //팔로우 브렌드
    @IBAction func onClickBrand(_ sender: Any) {
        pushVC("FOLLOW_BRAND_VIEW", storyboard: "Brand", animated: true)
    }
    
   
    //    ///sns
    @IBAction func onBtnfacebook(_ sender: Any) {
        if _btnFacebook.isSelected {
            delUsrSns(_type: "FACEBOOK")
        } else {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            loginManager.loginBehavior = .web
            loginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
                if (error == nil){
                    if(result?.isCancelled)! {
                        MsgUtil.showUIAlert("페이스북 로그인에 실패했습니다.")
                    } else {
                        self.getFacebookInfo()
                    }
                }
            }
        }
    }
    
    @IBAction func onBtnNaver(_ sender: Any) {
        if  _btnNaver.isSelected {
            delUsrSns(_type: "NAVER")
        } else {
            let tlogin : NaverThirdPartyLoginConnection = NaverThirdPartyLoginConnection.getSharedInstance()
            tlogin.delegate = self
            tlogin.consumerKey = kConsumerKey
            tlogin.consumerSecret = kConsumerSecret
            tlogin.serviceUrlScheme = kServiceAppUrlScheme
            tlogin.appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String // 앱이름
            tlogin.requestThirdPartyLogin()
        }
    }
    
    @IBAction func onBtnKakaoTalk(_ sender: Any) {
        if  _btnKakao.isSelected {
            delUsrSns(_type: "KAKAOTALK")
        } else {
            let session: KOSession = KOSession.shared();
            if session.isOpen() {
                session.close()
            }
            session.presentingViewController = self
            session.open { (error) in
                if error != nil{
                    MsgUtil.showUIAlert((error?.localizedDescription)!)
                    print(error?.localizedDescription as Any)
                }else if session.isOpen() == true{
                    KOSessionTask.meTask(completionHandler: { (profile , error) -> Void in
                        if profile != nil{
                            DispatchQueue.main.async(execute: { () -> Void in
                                let kakao : KOUser = profile as! KOUser
                                session.logoutAndClose(completionHandler: nil)
                                let snsId = NumberFormatter().string(from: kakao.id)!  //아이디를 패스워드로 쓰기로 했다.
                                let snsName = (kakao.properties?["nickname"] as? String) ?? ""
                                let snsEmail = kakao.email ?? ""
                                
                                self.setUsrSns(_type: "KAKAOTALK", email: snsEmail, _id: snsId, nickname: snsName, profile: "", token: "")
                                
                            })
                        } else {
                            MsgUtil.showUIAlert("카카오톡 정보를 불러오는데 실패했습니다.")
                        }
                    })
                }else{
                    print("isNotOpen")
                }
            }
        }
    }
    
    @IBAction func onBtnInstagram(_ sender: Any) {
        if  _btnInstagram.isSelected {
            delUsrSns(_type: "INSTAGRAM")
        } else {
          let api = Instagram.shared
            
            // Login
            api.login(from: navigationController!, success: {
                api.user("self", success: { (user) in
                    self.setUsrSns(_type: "INSTAGRAM", email: "", _id: user.id, nickname: user.username, profile: "", token: "")
                }, failure: { (error) in
                    CommonUtil.showToast("인스타그램 로그인정보얻기에 실패했습니다.")
                })
            }, failure: { error in
                CommonUtil.showToast("인스타그램 로그인에 실패했습니다.")
            })
        }
    }
    
    @IBAction func onBtnTwitter(_ sender: Any) {
        if _btnTwitter.isSelected {
            delUsrSns(_type: "TWITTER")
        } else {
        
            TWTRTwitter.sharedInstance().logIn {
                (session, error) -> Void in
                if (session != nil) {
                    
                    print(session!.userID)
                    print(session!.userName)
                    print(session!.authToken)
                    print(session!.authTokenSecret)
                    self.setUsrSns(_type: "TWITTER", email: "", _id: session!.userID, nickname: session!.userName, profile: "", token: session!.authToken)
                }else {
                    CommonUtil.showToast("트위터로그인 실패했습니다.")
                }
            }
        }
    }
    
    @objc func keyboardChange(_ aNotification: Notification) {
        let kbSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        let default_offset : CGFloat = 300
        var aRect = self.view.frame
        aRect.size.height -= (kbSize?.height)!
        var point : CGPoint
        
        if m_activeTextField == nil {
            return
        }
        
        point = m_activeTextField!.frame.origin
        point.y += m_activeTextField!.frame.height + m_activeTextField!.superview!.frame.origin.y
        point.y += default_offset // plus header size
        
        if m_activeTextField != nil {
            point = m_activeTextField!.frame.origin
            
            if m_activeTextField == tfName || m_activeTextField == _tfEmail{
                point.y += m_activeTextField!.frame.height + (m_activeTextField!.superview!.superview?.frame.origin.y)! +  m_activeTextField!.frame.height
                point.y += default_offset // plus header size
            } else if m_activeTextField == tfContact {
                point.y += m_activeTextField!.frame.height + (m_activeTextField!.superview!.superview?.frame.origin.y)! +  m_activeTextField!.frame.height
                point.y += default_offset // plus header size
                point.y += m_activeTextField!.frame.height
            } else {
                point.y += m_activeTextField!.frame.height + m_activeTextField!.superview!.frame.origin.y +  m_activeTextField!.frame.height
                point.y += default_offset // plus header size
            }
            if !aRect.contains(point) {
                let scrollPoint = CGPoint.init(x: 0.0, y: point.y - aRect.size.height)
                _scvMain.setContentOffset(scrollPoint, animated: true)
            }
        }
    }
    
    @objc func keyboardHide(_ aNotification: Notification) {
        _scvMain.setContentOffset(CGPoint.init(x: 0.0, y: 0.0), animated: true)
    }
    
    //set/del sns status
    func setSnsStatus(_type : String, status : Bool){
        switch _type {
        case "FACEBOOK":
            _btnFacebook.isSelected = status
            break
        case "NAVER":
            _btnNaver.isSelected = status
            break
        case "KAKAOTALK":
            _btnKakao.isSelected = status
            break
        case "INSTAGRAM":
            _btnInstagram.isSelected = status
            break
        case "TWITTER":
            _btnTwitter.isSelected = status
            break

        default:
            break
        }
    }
    
    //페이스북 정보 얻기
    func getFacebookInfo() {
        let params = ["fields": "id,name,email,first_name, last_name"]
        let request = FBSDKGraphRequest(graphPath: "me", parameters: params)!
        request.start(completionHandler: { (connection, result, error) in
            if result != nil {
                print(result!)
                let w_result = result as! [String: AnyObject?]
                
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                
                let snsId = (w_result["id"] as? String) ?? ""
                let snsName = (w_result["name"] as? String) ?? ""
                let snsEmail = (w_result["email"] as? String) ?? ""
                
                self.setUsrSns(_type: "FACEBOOK", email: snsEmail, _id: snsId, nickname: snsName, profile: "", token: "")
                
            } else {
                MsgUtil.showUIAlert("페이스북 정보를 불러오는데 실패했습니다.")
            }
        })
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    //sns 연동정보 얻기
    func getSNSStatusInfo() {
        
        gProgress.show()
        Net.getSNSStatusInfo(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.UserSNSStatusResult
                self.getSNSStatusResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //sns 연동정보 얻기 결과
    func getSNSStatusResult(_ data: Net.UserSNSStatusResult) {
        
        _btnFacebook.isSelected = data.facekbook
        _btnNaver.isSelected = data.naver
        _btnKakao.isSelected = data.kakaoTalk
        _btnInstagram.isSelected = data.instagram
        _btnTwitter.isSelected = data.twitter

    }
    
    // 회원기타정보 수정
    func updateOtherUsrInfo() {
        
        gProgress.show()
        Net.updateOtherUsrInfo(
            accessToken              : gMeInfo.token,
            addrDetail1              : addrInfo[0].addr,
            addrDetail2              : addrInfo[1].addr,
            addrDetail3              : addrInfo[2].addr,
            addrDetailSub1           : addrInfo[0].code,
            addrDetailSub2           : addrInfo[1].code,
            addrDetailSub3           : addrInfo[2].code,
            addrName1                : addrInfo[0].name,
            addrName2                : addrInfo[1].name,
            addrName3                : addrInfo[2].name,
            addrPhone1               : addrInfo[0].contact,
            addrPhone2               : addrInfo[1].contact,
            addrPhone3               : addrInfo[2].contact,
            addrSelectionIndex       : addrTabIndex + 1,
            usermail                 : _tfEmail.text!,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 회원기본정보 수정
    func updateBasisUsrInfo(name : String, phone : String, birth : String, gender : String) {
        
        gProgress.show()
        Net.updateBasisUsrInfo(
            accessToken        : gMeInfo.token,
            birthday           : birth,
            gender             : gender,
            usrNm              : name,
            usrPhone           : phone,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //sns 연동삭제
    func delUsrSns(_type : String) {
        
        gProgress.show()
        Net.delUsrSns(
            accessToken     : gMeInfo.token,
            snsType         : _type,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.setSnsStatus(_type: _type, status: false)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //sns 연동하기
    func setUsrSns(_type : String, email : String, _id : String, nickname : String, profile : String, token : String) {
        
        gProgress.show()
        Net.setUsrSns(
            accessToken     : gMeInfo.token,
            snsEmail        : email,
            snsId           : _id,
            snsNckNm        : nickname,
            snsProfileImg   : profile,
            snsToken        : token,
            snsType         : _type,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.setSnsStatus(_type: _type, status: true)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    
}

extension PrivatePersonSettingViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        m_activeTextField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfName {
            tfContact.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
        
    }
    
}

extension PrivatePersonSettingViewController : NaverThirdPartyLoginConnectionDelegate {
    
    func parseXML(_ str: String) {
        do {
            let xmlDoc = try AEXMLDocument(xml: str)
            if (xmlDoc.root["result"]["resultcode"].string == "00") {
                let snsId = xmlDoc.root["response"]["id"].string
                let snsName = xmlDoc.root["response"]["nickname"].string
                let snsProfileImagePath = xmlDoc.root["response"]["profile_image"].string
                let snsEmail = xmlDoc.root["response"]["email"].string

                self.setUsrSns(_type: "NAVER", email: snsEmail, _id: snsId, nickname: snsName, profile: snsProfileImagePath, token: "")
                
            } else {
                MsgUtil.showUIAlert("네이버 정보를 불러오는데 실패했습니다.")
            }
        }
        catch {
            print("\(error)")
        }
    }
    
    func naverSDKDidLoginSuccess() {
        /* 네이버 회원 정보 조회 */
        let loginConn = NaverThirdPartyLoginConnection.getSharedInstance()
        let tokenType = loginConn?.tokenType
        let accessToken = loginConn?.accessToken
        
        // Get User Profile
        if let url = URL(string: "https://apis.naver.com/nidlogin/nid/getUserProfile.xml") {
            if tokenType != nil && accessToken != nil {
                let authorization = "\(tokenType!) \(accessToken!)"
                var request = URLRequest(url: url)
                request.setValue(authorization, forHTTPHeaderField: "Authorization")
                let dataTask = URLSession.shared.dataTask(with: request) {(data, response, error) in
                    if let str = String(data: data!, encoding: .utf8) {
                        print(str)
                        loginConn?.resetToken()
                        
                        DispatchQueue.main.async {
                            self.parseXML(str)
                        }
                    }
                }
                dataTask.resume()
            }
        }
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        // 로그인이 성공했을 경우 호출
        naverSDKDidLoginSuccess()
        //        g_ProgressUtil.hideProgress()
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        /* 로그인 실패시에 호출되며 실패 이유와 메시지 확인 가능합니다. */
        print("oauth20Connection")
    }
    
    func oauth20ConnectionDidOpenInAppBrowser(forOAuth request: URLRequest!) {
        // 네이버 앱이 설치되어있지 않은 경우에 인앱 브라우저로 열리는데 이때 호출되는 함수
        
        let naverInappBrower = NLoginThirdPartyOAuth20InAppBrowserViewController(request: request)
        naverInappBrower?.modalPresentationStyle = .overFullScreen
        self.present(naverInappBrower!, animated: true, completion: nil)
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        // 이미 로그인이 되어있는 경우 access 토큰을 업데이트 하는 경우
        print("oauth20ConnectionDidFinishRequestACTokenWithRefreshToken")
    }
    
    func oauth20ConnectionDidFinishDeleteToken() {
        // 로그아웃이나 토큰이 삭제되는 경우
        print("oauth20ConnectionDidFinishDeleteToken")
    }
}
