//
//  ReturnsOkViewController.swift
//  selluv
//  반품승인
//  Created by Dev on 12/11/17.
//  modified by PJH on 04/04/18.

import UIKit
import Toast_Swift

class ReturnsOkViewController: BaseViewController {

    @IBOutlet var btnAddrs: [UIButton]!
    @IBOutlet weak var btnAddrSelect: UIButton!
    
    @IBOutlet weak var tfName: DesignableUITextField!
    @IBOutlet weak var tfContact: DesignableUITextField!
    @IBOutlet weak var tfAddr: DesignableUITextField!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var lbAddr: UILabel!
    
    var selectIndex = 0
    var dicDealInfo     : DealListDto!
    var postCode        = ""
    var addrInfo =
        [(name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : ""),
         (name : "", contact : "", addr : "", code : "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        tfAddr.isEnabled = false
        tfName.delegate = self
        tfContact.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
//        for i in 0..<3 {
//            let dic : UsrAddressDao = gUsrAddress[i]
//            addrInfo[i].name = dic.addressName
//            addrInfo[i].contact = dic.addressPhone
//            addrInfo[i].addr = dic.addressDetail
//            addrInfo[i].code = dic.addressDetailSub
//        }
//
//        selectIndex = 0
//        for i in 0..<3 {
//            let dic : UsrAddressDao = gUsrAddress[i]
//            if dic.status == 2 {
//                selectIndex = i
//            }
//        }
        selectIndex = 0
        selectTabBtn(index: selectIndex)
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func setRequestBtn() {
        
        var addrInputed = false
        
        if !(addrInfo[selectIndex].name.isEmpty) && !(addrInfo[selectIndex].contact.isEmpty) && !(addrInfo[selectIndex].addr.isEmpty) && !(addrInfo[selectIndex].code.isEmpty) {
            addrInputed = true
        }
       
        btnRequest.isEnabled = addrInputed
        btnRequest.backgroundColor = btnRequest.isEnabled ? UIColor.black : UIColor(hex: 0x999999)
        
        if addrInfo[selectIndex].addr != "" && addrInfo[selectIndex].code != ""{
            lbAddr.isHidden = false
            tfAddr.isHidden = true
        } else {
            lbAddr.isHidden = true
            tfAddr.isHidden = false
        }

    }
    
    func selectTabBtn(index : Int) {
        
        for i in 0..<3 {
            btnAddrs[i].isSelected = false
            btnAddrs[i].backgroundColor = UIColor(hex: 0xffffff)
        }
        
        btnAddrs[index].isSelected = true
        btnAddrs[index].backgroundColor = UIColor(hex: 0x42c2fe)
        
        selectIndex = index

        tfName.text = addrInfo[index].name
        tfContact.text = addrInfo[index].contact
        lbAddr.text = String.init(format: "%@\n[%@]", addrInfo[index].addr, addrInfo[index].code)
        
//        btnAddrSelect.isHidden = !addrInfo[index].addr.isEmpty
        
        setRequestBtn()
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func addrNumAction(_ sender: UIButton) {
        
      selectTabBtn(index: sender.tag)
    }
    
    @IBAction func addrSearchAction(_ sender: Any) {
        
        AddrSearch.show(self) { (addr, code) in

            self.lbAddr.text = String.init(format: "%@\n[%@]", addr, code)
            self.addrInfo[self.selectIndex].addr = addr
            self.addrInfo[self.selectIndex].code = code

            self.setRequestBtn()
        }
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        
        tfName.resignFirstResponder()
        tfContact.resignFirstResponder()
        
    }
    
    @IBAction func requestAction(_ sender: Any) {
        
        reqRefundAllow()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //판품 승인
    func reqRefundAllow() {
        let name = addrInfo[selectIndex].name
        let contact = addrInfo[selectIndex].contact
        let addr = String.init(format: "%@\n[%@]", addrInfo[selectIndex].addr, addrInfo[selectIndex].code)
        
        if name == "" {
            CommonUtil.showToast(NSLocalizedString("empty_name", comment:""))
            return
        }
        
        if contact == "" {
            CommonUtil.showToast(NSLocalizedString("empty_contact", comment:""))
            return
        }
        
        if addr == "" {
            CommonUtil.showToast(NSLocalizedString("empty_address", comment:""))
            return
        }
        
        gProgress.show()
        Net.reqRefundAllow(
            accessToken             : gMeInfo.token,
            dealUid                 : dicDealInfo.deal.dealUid,
            addressDetail           : addrInfo[selectIndex].addr,
            addressDetailSub        : addrInfo[selectIndex].code,
            addressName             : name,
            addressPhone            : contact,
            addressSelectionIndex   : selectIndex + 1,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("deal_refund_req_allow_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension ReturnsOkViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfName {
            tfContact.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        for btn in btnAddrs {
            if btn.isSelected {
                switch textField {
                case tfName :
                    addrInfo[btn.tag].name = textField.text!
                case tfContact:
                    addrInfo[btn.tag].contact = textField.text!
                default:
                    break
                }
            }
        }
        setRequestBtn()
    }
}
