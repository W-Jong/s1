//
//  SellOrderDetailViewController.swift
//  selluv
//  판매주문상세
//  Created by Dev on 12/12/17.
//  modified by PJh on 03/04/18.

import UIKit

class SellOrderDetailViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var scvList: UIScrollView!
    
    @IBOutlet weak var vwNormalLine: UIView!
    @IBOutlet weak var vwMostLine: UIView!
    
    @IBOutlet weak var htNormal: NSLayoutConstraint!
    @IBOutlet weak var htMost: NSLayoutConstraint!
    
    @IBOutlet weak var btnNormal: UIButton!
    @IBOutlet weak var btnMost: UIButton!
    
    @IBOutlet weak var imgNormal: UIImageView!
    @IBOutlet weak var imgMost: UIImageView!
    
    @IBOutlet weak var vwMoneyInfo: UIView!
    @IBOutlet weak var vwInfo: UIView!
    
    @IBOutlet weak var imgWarning: UIImageView!
    @IBOutlet weak internal var lbStatus: UILabel!
    @IBOutlet weak internal var lbNegoStatus: UILabel!
    @IBOutlet weak internal var lbBottomStatus: UILabel! //운송장번호
    @IBOutlet weak internal var btnNego: UIButton!
    @IBOutlet weak internal var htBottom: NSLayoutConstraint!
    @IBOutlet weak var wdLeft: NSLayoutConstraint!
    @IBOutlet weak var lbTopStatus: UILabel!
    
    @IBOutlet weak var lblOrderNum: UILabel!  //주분번호
    @IBOutlet weak var lblOrderDate: UILabel!  //주문일자
    @IBOutlet weak var lbCost: UILabel!//운임비용
    
    //pdt info
    @IBOutlet weak var ivPdt: UIImageView!
    @IBOutlet weak var lbBrandEn: UILabel!
    @IBOutlet weak var lbPdtContent: UILabel!
    @IBOutlet weak var lblNickname: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    //정산금액
    @IBOutlet weak var lbSumMoney: UILabel!
    @IBOutlet weak var lbTotalSumMoney: UILabel!
    @IBOutlet weak var lbPdtPrice: UILabel!
    @IBOutlet weak var lbSendPrice: UILabel!
    @IBOutlet weak var lbSellubUseMoney: UILabel!
    @IBOutlet weak var lbPayFees: UILabel!
    @IBOutlet weak var lbPromotion: UILabel!
    
    //최대 적립가능 셀럽머니
    @IBOutlet weak var lbMaxSellubMoney: UILabel!
    @IBOutlet weak var lbMaxSellubMoney1: UILabel!
    @IBOutlet weak var lbSnsMoney: UILabel!
    @IBOutlet weak var lbWriterMoney: UILabel!
    
    //정산계좌
    @IBOutlet weak var lbAccountNm: UILabel!
    @IBOutlet weak var lbBank: UILabel!
    @IBOutlet weak var lbAccountNum: UILabel!
    
    //반품 주소
    @IBOutlet weak var lbrefundName: UILabel!
    @IBOutlet weak var lbRefundContact: UILabel!
    @IBOutlet weak var lbRefundAddress: UILabel!
    
    //반품 사유 및 사진
    @IBOutlet weak var tvRefundReason: KMPlaceholderTextView!
    @IBOutlet weak var clvRefundPhoto: UICollectionView!
    
    //constraint
    @IBOutlet weak var lcvwSendPopupHeight: NSLayoutConstraint! //53 배송팝업
    @IBOutlet weak var lcvwServiceHeight: NSLayoutConstraint! //147 편의점택배
    @IBOutlet weak var lcvwNormalHeight: NSLayoutConstraint! //120  일반택배
    @IBOutlet weak var lcvwMoneyInfoHeight: NSLayoutConstraint!//53  정산정보
    @IBOutlet weak var lcvwMoneyHeight: NSLayoutConstraint! //45 정산금액
    @IBOutlet weak var lcvwMaxSelluvMoneyHeight: NSLayoutConstraint!//45 최대 적립가능 셀럽머니
    @IBOutlet weak var lcvwPayMenuyHeight: NSLayoutConstraint!//45 결제금액
    @IBOutlet weak var lcvwAccountHeight: NSLayoutConstraint!//188 정산계좌
    @IBOutlet weak var lcvwNoAccountHeight: NSLayoutConstraint!//188 정산계좌가 없을때
    @IBOutlet weak var lcvwRefundReasonHeight: NSLayoutConstraint! //348 반품 사유 및 사진
    @IBOutlet weak var lcvwRefundPhotosHeight: NSLayoutConstraint! //188 반품사진
    
    var dealUid         : Int!
    var dicDealDetail   : DealDetailDto!
    var goPage          : Int!
    var arrRefundPhotos : Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        vwMoneyInfo.isHidden = true
        vwInfo.isHidden = true
        
        tvRefundReason.isEditable = false
        tvRefundReason.textColor = UIColor.init(hex: 333333)
        
        clvRefundPhoto.delegate = self
        clvRefundPhoto.dataSource = self
        clvRefundPhoto.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")
        
        getDealDetailInfo()
    }
    
    func showMainContent() {
        
        if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 12 { //카운터 네고
            lbStatus.text = "구매자에게 카운터 네고를 제안하였습니다. 3일 이내로 구매자가 카운터 네고를 승인하지 않을 경우, 거래는 자동으로 취소됩니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[카운터네고]"
            htBottom.constant = 15
            btnNego.isHidden = true
            lbBottomStatus.isHidden = true
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwMoneyInfoHeight.constant = 0
            lcvwMoneyHeight.constant = 0
            lcvwMaxSelluvMoneyHeight.constant = 0
            lcvwPayMenuyHeight.constant = 0
            lcvwAccountHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.negoTime, type: "yyyy-MM-dd hh:mm")

        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 11 { //네고제안
            lbStatus.text = "구매자가 네고를 제안하였습니다. 네고 금액 조정을 원하실 경우 카운터 네고 기능으로 구매자에게 새로운 금액을 제안하실 수 있습니다. 네고 승인이나 카운터 네고 없이 3일이 경과하게 돠면 거래는 자동으로 취소됩니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[네고제안]"
            htBottom.constant = 15
            btnNego.isHidden = true
            lbBottomStatus.isHidden = true
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwMoneyInfoHeight.constant = 0
            lcvwMoneyHeight.constant = 0
            lcvwMaxSelluvMoneyHeight.constant = 0
            lcvwPayMenuyHeight.constant = 0
            lcvwAccountHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.negoTime, type: "yyyy-MM-dd hh:mm")
            
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 1 {   //배송준비
            
            imgWarning.isHidden = false
            wdLeft.constant = 36
            lbTopStatus.text = "제품을 발송해주세요."
            lbNegoStatus.text = "[발송대기]"
            htBottom.constant = 15
            btnNego.isHidden = true
            lbBottomStatus.isHidden = true
            
            lcvwAccountHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            lbCost.text = "운임 비용: " + CommonUtil.formatNum(dicDealDetail.deal.sendPrice) + " 원~"
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
            getWorkingDay(_date: CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd"), diff: 3)
            
        } else if (goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 2) || (goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 3) {   //배송진행, 정품인증
            lbStatus.text = "구매자의 요청으로 상품이 정품 감정을 위해 셀럽으로 배송 중 입니다. 감정이 완료된 후, 구매자에게 상품이 발송되면 운송장 번호를 확인하실 수 있으며, 운송장 번호 업데이트까지 평균적으로 3-7일 정도 소요됩니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[배송진행]"
            htBottom.constant = 15
            btnNego.isHidden = true
            lbBottomStatus.isHidden = true
            
            lbBottomStatus.text = dicDealDetail.deal.deliveryNumber
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            //계좌관리
            if gMeInfo.accountNm == "" || gMeInfo.accountNum == "" || gMeInfo.bankNm == "" {
                lcvwAccountHeight.constant = 0
            } else {
                lcvwNoAccountHeight.constant = 0
                lbAccountNm.text = gMeInfo.accountNm
                lbBank.text = gMeInfo.bankNm
                lbAccountNum.text = gMeInfo.accountNum
            }
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 4 { //배송완료
            lbStatus.text = "상품 배송이 완료되었습니다.\n 구매자의 구매 확정에는 최대 3일이 소요될 수 있습니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[배송완료]"
            htBottom.constant = 42
            btnNego.isHidden = false
            lbBottomStatus.isHidden = false
            
            lbBottomStatus.text = dicDealDetail.deal.deliveryNumber
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            lbAccountNm.text = gMeInfo.accountNm
            lbBank.text = gMeInfo.bankNm
            lbAccountNum.text = gMeInfo.accountNum
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 5 {  //거래완료
            lbStatus.text = "구매 확정으로 거래가 완료되어 판매 대금이 아래 계좌로 익일 정산될 예정입니다. 정산 계좌를 다시 한 번 정확히 확인해주세요."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[거래완료]"
            htBottom.constant = 42
            btnNego.isHidden = false
            lbBottomStatus.isHidden = false
            
            lbBottomStatus.text = dicDealDetail.deal.deliveryNumber
            
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            lbAccountNm.text = gMeInfo.accountNm
            lbBank.text = gMeInfo.bankNm
            lbAccountNum.text = gMeInfo.accountNum
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 6 { //정산완료
            lbStatus.text = "판매 대금에 대한 정산이 완료되었습니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[정산완료]"
            htBottom.constant = 42
            btnNego.isHidden = false
            lbBottomStatus.isHidden = false
            
            lbBottomStatus.text = dicDealDetail.deal.deliveryNumber
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            lbAccountNm.text = gMeInfo.accountNm
            lbBank.text = gMeInfo.bankNm
            lbAccountNum.text = gMeInfo.accountNum
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 10 {  //주문취소
            lbStatus.text = "주문이 취소되었습니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[주문취소]"
            htBottom.constant = 15
            btnNego.isHidden = true
            lbBottomStatus.isHidden = true
            
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            lcvwNormalHeight.constant = 0
            lcvwMoneyInfoHeight.constant = 0
            lcvwMoneyHeight.constant = 0
            lcvwMaxSelluvMoneyHeight.constant = 0
            lcvwPayMenuyHeight.constant = 0
            lcvwAccountHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundReasonHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
            
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 7 {  //반품신청
            lbStatus.text = "구매자가 아래의 사유로 반품을 신청하였습니다. 반품 사유를 확인하신 후, 반품 승인여부를 결정해주세요. 상품이 가품이거나, 판매자가 사전에 밝히지 않은 큰 하자가 있는 경우에도 불구하고, 합당한 이유없이 반품신청을 거절하실 경우, 셀럽은 양자간 거래에 개입할 수 있습니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[반품신청]"
            htBottom.constant = 42
            btnNego.isHidden = false
            lbBottomStatus.isHidden = false
            
            lcvwAccountHeight.constant = 0
            lcvwNoAccountHeight.constant = 0
            lcvwRefundPhotosHeight.constant = 0
            lcvwSendPopupHeight.constant = 0
            lcvwServiceHeight.constant = 0
            
            tvRefundReason.text = dicDealDetail.deal.refundReason

            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 8 {  //반품승인
            lbStatus.text = "구매자가 반품 상품을 발송하였습니다. 배송이 완료되면 상품을 체크하신 후 '반품확인'버튼을 눌러 반품 절차를 완료해 주시기 바랍니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[반품승인]"
            htBottom.constant = 42
            btnNego.isHidden = false
            lbBottomStatus.isHidden = false
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        } else if goPage == DealPage.SELL.rawValue && dicDealDetail.deal.status == 9 { //반품완료
            lbStatus.text = "반품이 완료되었습니다."
            imgWarning.isHidden = true
            wdLeft.constant = 15
            lbNegoStatus.text = "[반품완료]"
            htBottom.constant = 42
            btnNego.isHidden = false
            lbBottomStatus.isHidden = false
            lblOrderDate.text = CommonUtil.getDateFromStamp(dicDealDetail.deal.payTime, type: "yyyy-MM-dd hh:mm")
        }
        
        setPdtData()
    }
    
    //상품 정보
    func setPdtData() {

        let _uid : String = String(dealUid)
        var securityStr = ""
        for _ in 0..<10 - _uid.count {
            securityStr = securityStr + "0"
        }
        lblOrderNum.text = "주문번호: " + securityStr + _uid
        
        ivPdt.kf.setImage(with: URL(string: dicDealDetail.deal.pdtImg), placeholder: UIImage(named: "ic_logo"), options: [], progressBlock: nil, completionHandler: nil)
        lbBrandEn.text = dicDealDetail.deal.brandEn
        lbPdtContent.text = dicDealDetail.deal.pdtTitle
        lbSize.text = dicDealDetail.deal.pdtSize
        lbPrice.text = CommonUtil.formatNum(dicDealDetail.deal.reqPrice)
        
        lblNickname.text = dicDealDetail.usr.usrNckNm
        
        //정산 금액
        lbPdtPrice.text = "+ " + CommonUtil.formatNum(dicDealDetail.deal.pdtPrice)
        lbSendPrice.text = "+ " + CommonUtil.formatNum(dicDealDetail.deal.sendPrice)
        lbSellubUseMoney.text = "- " + CommonUtil.formatNum(dicDealDetail.deal.selluvPrice)
        lbPayFees.text = "- " + CommonUtil.formatNum(dicDealDetail.deal.payFeePrice)
        lbPromotion.text = "+ " + CommonUtil.formatNum(dicDealDetail.deal.payPromotionPrice)
        
        let totalMoney = dicDealDetail.deal.pdtPrice + dicDealDetail.deal.sendPrice - dicDealDetail.deal.selluvPrice - dicDealDetail.deal.payFeePrice + dicDealDetail.deal.payPromotionPrice
        lbSumMoney.text = CommonUtil.formatNum(totalMoney)
        lbTotalSumMoney.text = CommonUtil.formatNum(totalMoney)
        
        //초대 적립가능 셀럽머니
        getSystemSettingInfo()
    }
    
    @IBAction func onClickInfo(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dicDealDetail.deal.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onClickUser(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicDealDetail.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func onClickPopup(_ sender: Any) {
        ServicePopup.show(self)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickNormal(_ sender: Any) {
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        if btnNormal.isSelected {
            btnNormal.isSelected = false
            htNormal.constant = 0
            vwNormalLine.isHidden = false
            imgNormal.image = UIImage.init(named: "ic_undo_arrow")
        
            vwMoneyInfo.isHidden = true
            vwInfo.isHidden = btnMost.isSelected ? false : true
        } else {
            btnNormal.isSelected = true
            htNormal.constant = 182
            vwNormalLine.isHidden = true
            imgNormal.image = UIImage.init(named: "ic_undo_arrow_rotate")
            scvList.scrollToBottom(isAnimated: true)
            
            vwMoneyInfo.isHidden = false
            vwInfo.isHidden = btnMost.isSelected ? false : true
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
    }
    
    @IBAction func onClickMost(_ sender: Any) {
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        if btnMost.isSelected {
            btnMost.isSelected = false
            htMost.constant = 0
            vwMostLine.isHidden = false
            imgMost.image = UIImage.init(named: "ic_undo_arrow")
            vwMoneyInfo.isHidden = btnNormal.isSelected ? false : true
            vwInfo.isHidden = true
        } else {
            btnMost.isSelected = true
            htMost.constant = 120
            vwMostLine.isHidden = true
            imgMost.image = UIImage.init(named: "ic_undo_arrow_rotate")
            scvList.scrollToBottom(isAnimated: true)
            vwMoneyInfo.isHidden = btnNormal.isSelected ? false : true
            vwInfo.isHidden = false
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
    }
    
    //편의점 택배
    @IBAction func onClickService(_ sender: Any) {
        
        getDealDeliveryInfo()
    }
    
    //일반택배
    @IBAction func onClickNormalService(_ sender: Any) {
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_NORMALDELIVERY_VIEW")) as! NormalDeliveryViewController
        vc.dicDealDetail = dicDealDetail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //배송조회
    @IBAction func onBtnDeliveryTracking(_ sender: Any) {
        
        if dicDealDetail.deal.deliveryNumber == "" || dicDealDetail.deal.deliveryNumber == nil {
            return
        }
        
        let num : String = dicDealDetail.deal.deliveryNumber
        let encodeStr : String = num.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
        let weburl = DELIVERY_URL + encodeStr
        
        guard let url = URL(string: weburl) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    //계좌번호 등록
    @IBAction func onBtnAccountRegister(_ sender: Any) {
        pushVC("MYPAGE_ACCOUNT_VIEW", storyboard: "Mypage", animated: true)
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Collection
    /////////////////////////////////////////////////////
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.size.width) / 3, height: (collectionView.bounds.size.width) / 3 * 120 / 150);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrRefundPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestCVC", for: indexPath as IndexPath) as! RequestCVC
        let img = arrRefundPhotos[indexPath.row]
        cell.ivProfile.kf.setImage(with: URL(string: img), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
    
        cell.btnDel.isHidden = true
        cell.btnSelect.isSelected = false
        return cell
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 주문상세
    func getDealDetailInfo() {
        
        gProgress.show()
        Net.getDealDetail(
            accessToken     : gMeInfo.token,
            dealUid         : dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getDealDetailResult
                self.getDealDetailResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //주문상세 얻기 결과
    func getDealDetailResult(_ data: Net.getDealDetailResult) {
        
        dicDealDetail  = data.dealDetail
        showMainContent()
        
        arrRefundPhotos = dicDealDetail.refundPhotos
        clvRefundPhoto.reloadData()
    }
    
    // 시스템 설정 정보 얻기
    func getSystemSettingInfo(){
        
        gProgress.show()
        Net.getSystemSettingInfo(
            accessToken         : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getSystemSettingInfoResult
                self.getSystemSettingInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //시스템 설정 정보 얻기결과
    func getSystemSettingInfoResult(_ data: Net.getSystemSettingInfoResult) {
        
        let dicSysstemInfo : SystemSettingDto = data.dicInfo
        let buyReviewReward : Int = dicSysstemInfo.buyReviewReward
        let shareRewrad  : Int = dicSysstemInfo.shareRewrad * 5
        
        let total = shareRewrad + buyReviewReward
        lbMaxSellubMoney.text = CommonUtil.formatNum(total)
        lbMaxSellubMoney1.text = CommonUtil.formatNum(total)
        lbSnsMoney.text = CommonUtil.formatNum(shareRewrad)
        lbWriterMoney.text = CommonUtil.formatNum(buyReviewReward)
    }
    
    // 영업일 기준 날짜 얻기
    func getWorkingDay(_date : String, diff : Int) {
        
        gProgress.show()
        Net.getWorkingDay(
            accessToken     : gMeInfo.token,
            basisDate       : _date,
            deltaDays       : diff,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.ggetWorkingDayResult
                self.ggetWorkingDayResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //
    func ggetWorkingDayResult(_ data: Net.ggetWorkingDayResult) {
        
        let strDate : String = data.targetDate
        
        let year =  strDate.substring(with: strDate.index(strDate.startIndex, offsetBy: 0)..<strDate.index(strDate.startIndex, offsetBy: 4))
        let month =  strDate.substring(with: strDate.index(strDate.startIndex, offsetBy: 5)..<strDate.index(strDate.startIndex, offsetBy: 7))
        let day =  strDate.substring(with: strDate.index(strDate.startIndex, offsetBy: 8)..<strDate.index(strDate.startIndex, offsetBy: 10))
        
        let makedate = String.init(format: "%d년 %d월 %d일 20시", Int(year)!, Int(month)!, Int(day)!)
        
        let 분홍색10 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10), NSAttributedStringKey.foregroundColor: UIColor.init(red: 0xff / 255.0, green: 0x3b / 255.0, blue: 0x7e / 255.0, alpha: 1.0)]
        let Default = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10), NSAttributedStringKey.foregroundColor: UIColor(hex: 666666)]
        
        let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  "상품이 판매되었습니다. 아래 배송 방법 중 한 가지를 선택하여 상품을 발송하신 후 운송장 번호를 입력해주세요.\n", attributes: Default))
        contentStr.append(NSAttributedString(string: makedate, attributes: 분홍색10))
        contentStr.append(NSAttributedString(string: "까지 운송장 번호를 입력하지 않으실 경우 거래가 자동으로 취소됩니다. (편의점 택배는 예약번호로 발송하신 후, 운송장 번호가 자동으로 업데이트됩니다.)", attributes: Default))
     
        lbStatus.attributedText = contentStr
    }
    
    // 택배예약정보 얻기
    func getDealDeliveryInfo() {
        
        gProgress.show()
        Net.getDealDeliveryInfo(
            accessToken     : gMeInfo.token,
            dealUid         : dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getDealDeliveryInfoResult
                self.getDealDeliveryInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == DELIVERY_HISTORY_NOT_EXIST {
                
                let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SERVICEDELIVERY_VIEW")) as! ServiceDeliveryViewController
                vc.dicDealDetail = self.dicDealDetail
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //택배 에약정보 얻기 결과
    func getDealDeliveryInfoResult(_ data: Net.getDealDeliveryInfoResult) {
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_DELIVERYCANCEL_VIEW")) as! DeliveryCancelViewController
        vc.dicDealHis = data.dealHisDao
        self.navigationController?.pushViewController(vc, animated: true)

    }
}
