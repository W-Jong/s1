//
//  SelluvNewsDetailViewController.swift
//  selluv
//
//  Created by Dev on 12/11/17.
//

import UIKit

class SelluvNewsDetailViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    var noticeUid : Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getNoticeDetailInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 공지 상세 얻기
    func getNoticeDetailInfo() {
        
        gProgress.show()
        Net.getNoticeDetailInfo(
            accessToken : gMeInfo.token,
            noticeUid   : noticeUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getNoticeDetailInfoResult
                
                self.lblTitle.text = res.dicInfo.title
                self.lblContent.text = res.dicInfo.content
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
