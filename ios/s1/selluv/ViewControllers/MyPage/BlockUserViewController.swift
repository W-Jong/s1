//
//  BlockUserViewController.swift
//  selluv
//  차단된 유저
//  Created by Dev on 12/10/17.
//  modified by PJH on 06/03/18.

import UIKit

class BlockUserViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvMain: UITableView!
    
    var arrBlockList    : Array<BlockUsrDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getBlockList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvMain.delegate = self
        _tvMain.dataSource = self
        _tvMain.register(UINib.init(nibName: "BlockUserTVC", bundle: nil), forCellReuseIdentifier: "BlockUserTVC")
        
    }

    @objc func onBtnGo2User(_ sender: UIButton) {
        let dic : BlockUsrDto = arrBlockList[sender.tag]
        
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usrUid
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBlockList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockUserTVC", for: indexPath as IndexPath) as! BlockUserTVC
        
        let dic : BlockUsrDto = arrBlockList[indexPath.row]
        cell._lbId.text = dic.usrId
        cell._lbNickname.text = dic.usrNckNm
        cell._imgPhoto.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnGo2User(_:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == arrBlockList.count - 1 && !isLast {
            nPage = nPage + 1
            getBlockList()
        }
        
        return cell
    }
    
    //MARK: - UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 차단된 유저 목록 얻기
    func getBlockList() {
        
        gProgress.show()
        Net.getBlockUsrList(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.BlockUsrResult
                self.getBlockListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 차단된 유저 목록 얻기 결과
    func getBlockListResult(_ data: Net.BlockUsrResult) {
        
        isLast = data.last
        
        if nPage == 0 {
            arrBlockList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : BlockUsrDto = data.list[i]
            arrBlockList.append(dic)
        }
        
       _tvMain.reloadData()
    }
    
}
