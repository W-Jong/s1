//
//  AccountViewController.swift
//  selluv
//  게좌관리
//  Created by Dev on 12/9/17.
//  modified by PJH on 05/03/18.

import UIKit

class AccountViewController: BaseViewController {

    @IBOutlet weak var _lbName: UILabel!
    @IBOutlet weak var _lbBank: UILabel!
    @IBOutlet weak var _tfAccount: UITextField!
    
    var banknm : String!
    var accountNum : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
    }
    
    func initData() {
        _lbName.text = gMeInfo.accountNm
        _tfAccount.text = gMeInfo.accountNum
        
        if gMeInfo.bankNm != "" {
            _lbBank.textColor = UIColor.init(hex: 333333)
        }
        _lbBank.text = gMeInfo.bankNm
    }

    @IBAction func onClickBack(_ sender: Any) {
        reqAccountUpdate()
    }
    
    @IBAction func onClickBank(_ sender: Any) {
        BankSelect.show(self){ (size2) in
            self._lbBank.textColor = UIColor.init(hex: 333333)
            self._lbBank.text = size2
        }
    }
    
    @IBAction func onClickBg(_ sender: Any) {
        _tfAccount.resignFirstResponder()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //게좌 설정
    func reqAccountUpdate() {
        
        banknm = _lbBank.text
//        if banknm == "" {
//            CommonUtil.showToast(NSLocalizedString("empty_bank_name", comment:""))
//            return
//        }
        
        accountNum = _tfAccount.text
//        if accountNum == "" {
//            CommonUtil.showToast(NSLocalizedString("empty_account_num", comment:""))
//            return
//        }
        
        if banknm == "" || accountNum == "" {
            popVC()
            return
        }
        
        gProgress.show()
        Net.reqAccountUpdate(
            accessToken     : gMeInfo.token,
            accountNum      : accountNum,
            bankNm          : banknm,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
}
