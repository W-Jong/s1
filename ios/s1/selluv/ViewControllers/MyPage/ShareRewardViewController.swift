//
//  ShareRewardViewController.swift
//  selluv
//  공유리워드 작성
//  Created by Dev on 12/11/17.
//  modified by PJH on 03/04/18.

import UIKit

class ShareRewardViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var clvGram: UICollectionView!
    @IBOutlet weak var clvNeiver: UICollectionView!
    @IBOutlet weak var clvFace: UICollectionView!
    @IBOutlet weak var clvTwiter: UICollectionView!
    @IBOutlet weak var clvKakao: UICollectionView!
    
    @IBOutlet weak var vwGram: UIView!
    @IBOutlet weak var vwNeiver: UIView!
    @IBOutlet weak var vwFace: UIView!
    @IBOutlet weak var vwTwiter: UIView!
    @IBOutlet weak var vwKakao: UIView!
    @IBOutlet weak var btnRequest: UIButton!
    
    var dicDealInfo     : DealListDto!
    var arrFBPhotos     : Array<String> = []
    var arrIGPhotos     : Array<String> = []
    var arrKKPhotos     : Array<String> = []
    var arrNVPhotos     : Array<String> = []
    var arrTWPhotos     : Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        btnRequest.backgroundColor = UIColor.black
        btnRequest.isEnabled = true

        clvGram.delegate = self
        clvGram.dataSource = self
        clvGram.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")

        clvNeiver.delegate = self
        clvNeiver.dataSource = self
        clvNeiver.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")

        clvFace.delegate = self
        clvFace.dataSource = self
        clvFace.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")

        clvTwiter.delegate = self
        clvTwiter.dataSource = self
        clvTwiter.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")

        clvKakao.delegate = self
        clvKakao.dataSource = self
        clvKakao.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")

        showCollection()
    }

    func showCollection() {
        if arrIGPhotos.count > 0 {
            clvGram.isHidden = false
            vwGram.isHidden = true
        } else {
            clvGram.isHidden = true
            vwGram.isHidden = false
        }
        
        if arrNVPhotos.count > 0 {
            clvNeiver.isHidden = false
            vwNeiver.isHidden = true
        } else {
            clvNeiver.isHidden = true
            vwNeiver.isHidden = false
        }
        
        if arrFBPhotos.count > 0 {
            clvFace.isHidden = false
            vwFace.isHidden = true
        } else {
            clvFace.isHidden = true
            vwFace.isHidden = false
        }
        
        if arrTWPhotos.count > 0 {
            clvTwiter.isHidden = false
            vwTwiter.isHidden = true
        } else {
            clvTwiter.isHidden = true
            vwTwiter.isHidden = false
        }
        
        if arrKKPhotos.count > 0 {
            clvKakao.isHidden = false
            vwKakao.isHidden = true
        } else {
            clvKakao.isHidden = true
            vwKakao.isHidden = false
        }
    }
    
    func goCameraPage(_index : Int) {
        
        var arrImgs : [UIImage] = []
        var arrPhotos : Array<String> = []
        
        switch _index {
        case 1:  //인스타그람
            arrPhotos = arrIGPhotos
            break
        case 2:  //네이버
            arrPhotos = arrNVPhotos
            break
        case 3:  //페이스북
            arrPhotos = arrFBPhotos
            break
        case 4:  //트위터
            arrPhotos = arrTWPhotos
            break
        case 5:  //카카오
            arrPhotos = arrKKPhotos
            break
        default:
            break
        }
        
        for i in 0..<arrPhotos.count {
            let img = UIImageView()
            img.kf.setImage(with: URL(string: arrPhotos[i]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            arrImgs.append(img.image!)
        }
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "SNS_SHARE_CAMERA_VIEW")) as! SnsSharelCameraViewController
        vc.assetsList = arrImgs
        vc.delegate = self
        vc.mType = _index
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnDelete(_ sender: UIButton) {
        var index = sender.tag
        if index >= 100000 && index < 200000  {  //인스타그람
            index = index - 100000
            arrIGPhotos.remove(at: index)
            clvGram.reloadData()
        } else if index >= 200000 && index < 300000  { //네이버
            index = index - 200000
            arrNVPhotos.remove(at: index)
            clvNeiver.reloadData()
        } else if index >= 300000 && index < 400000  { //페이스북
            index = index - 300000
            arrFBPhotos.remove(at: index)
            clvFace.reloadData()
        } else if index >= 400000 && index < 500000  { //트위터
            index = index - 400000
            arrTWPhotos.remove(at: index)
            clvTwiter.reloadData()
        } else {  //카카오
            index = index - 500000
            arrKKPhotos.remove(at: index)
            clvKakao.reloadData()
        }
        showCollection()
    }

    @objc func onBtnSelect(_ sender: UIButton) {
        let index = sender.tag
        var type = 0
        if index >= 100000 && index < 200000  {  //인스타그람
            type = 1
        } else if index >= 200000 && index < 300000  { //네이버
            type = 2
        } else if index >= 300000 && index < 400000  { //페이스북
            type = 3
        } else if index >= 400000 && index < 500000  { //트위터
            type = 4
        } else {  //카카오
            type = 5
        }
        goCameraPage(_index: type)
    }

    @IBAction func onClickGram(_ sender: Any) {
        goCameraPage(_index: 1)
    }
    
    @IBAction func onClickNeiver(_ sender: Any) {
        goCameraPage(_index: 2)
    }
    
    @IBAction func onClickFace(_ sender: Any) {
        goCameraPage(_index: 3)
    }
    
    @IBAction func onClickTwiter(_ sender: Any) {
        goCameraPage(_index: 4)
    }
    
    @IBAction func onClickKakao(_ sender: Any) {
        goCameraPage(_index: 5)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickRequest(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "신청하시겠습니까?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (UIAlertAction) in
            self.reqDealShare()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.size.width) / 3, height: (collectionView.bounds.size.width) / 3 * 120 / 150);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clvGram {
            return arrIGPhotos.count
        }else if collectionView == clvNeiver {
            return arrNVPhotos.count
        } else if collectionView == clvFace {
            return arrFBPhotos.count
        } else if collectionView == clvTwiter {
            return arrTWPhotos.count
        }  else {
            return arrKKPhotos.count
        }
    }
    var nTag = -1
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestCVC", for: indexPath as IndexPath) as! RequestCVC
        
        if collectionView == clvGram {
            cell.ivProfile.kf.setImage(with: URL(string: arrIGPhotos[indexPath.row]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            nTag = 100000 + indexPath.row
        } else if collectionView == clvNeiver {
            cell.ivProfile.kf.setImage(with: URL(string: arrNVPhotos[indexPath.row]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            nTag = 200000 + indexPath.row
        } else if collectionView == clvFace {
            cell.ivProfile.kf.setImage(with: URL(string: arrFBPhotos[indexPath.row]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            nTag = 300000 + indexPath.row
        } else if collectionView == clvTwiter {
            cell.ivProfile.kf.setImage(with: URL(string: arrTWPhotos[indexPath.row]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            nTag = 400000 + indexPath.row
        }  else {
            cell.ivProfile.kf.setImage(with: URL(string: arrKKPhotos[indexPath.row]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            nTag = 500000 + indexPath.row
        }
        
        cell.btnSelect.tag = nTag
        cell.btnSelect.addTarget(self, action:#selector(self.onBtnSelect(_:)), for: UIControlEvents.touchUpInside)
        cell.btnDel.tag = nTag
        cell.btnDel.addTarget(self, action:#selector(self.onBtnDelete(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //공유리워드 신청
    func reqDealShare() {
        
        gProgress.show()
        Net.reqDealShare(
            accessToken         : gMeInfo.token,
            dealUid             : dicDealInfo.deal.dealUid,
            facebookPhotos      : arrFBPhotos,
            instagramPhotos     : arrIGPhotos,
            kakaostoryPhotos    : arrKKPhotos,
            naverblogPhotos     : arrNVPhotos,
            twitterPhotos       : arrTWPhotos,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("reg_sns_share_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

extension ShareRewardViewController: SnsSharelCameraViewControllerDelegate {
    func setPhotos( photos : Array<String>, photosFiles : Array<String>, _type : Int){
        switch _type {
        case 1:  //인스타그람
            arrIGPhotos =  photos
            clvGram.reloadData()
            break
        case 2:  //네이버
            arrNVPhotos =  photos
            clvNeiver.reloadData()
            break
        case 3:  //페이스북
            arrFBPhotos =  photos
            clvFace.reloadData()
            break
        case 4:  //트위터
            arrTWPhotos =  photos
            clvTwiter.reloadData()
            break
        case 5:  //카카오
            arrKKPhotos =  photos
            clvKakao.reloadData()
            break
        default:
            break
        }
        self.showCollection()
    }
}
