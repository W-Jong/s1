//
//  NormalDeliveryViewController.swift
//  selluv
//  일반 택배
//  Created by Dev on 12/11/17.
//  modified by PJH on 03/04/18.

import UIKit

class NormalDeliveryViewController: BaseViewController {

    @IBOutlet weak var _tfNumber: UITextField!
    @IBOutlet weak var _lbCompany: UILabel!
    
    @IBOutlet weak var lbAddressTitle: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbContact: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    var dicDealDetail   : DealDetailDto!
    var arrCompnies     : Array<String>!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        if dicDealDetail.deal.verifyPrice > 0 {
            //셀럽 주소
            lbAddressTitle.text = NSLocalizedString("selluv_address", comment:"")
            lbName.text = SELLUV_NAME
            lbContact.text = SELLUV_CONTACT
            lbAddress.text = SELLUV_ADDRESS
        } else {
            //구매자 주소
            lbAddressTitle.text = NSLocalizedString("buyer_address", comment:"")
            lbName.text = dicDealDetail.deal.recipientNm
            lbContact.text = dicDealDetail.deal.recipientPhone
            lbAddress.text = dicDealDetail.deal.recipientAddress
        }
        getDeliveryCompanies()
    }
    
    @IBAction func onClickCompany(_ sender: Any) {
        CompanySelect.show(self, arr: arrCompnies){ (size2) in
            self._lbCompany.textColor = UIColor.init(hex: 333333)
            self._lbCompany.text = size2
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onBgTouched(_ sender: Any) {
        _tfNumber.resignFirstResponder()
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        reqDealDeliveryNum()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 운송장 번호 등록
    func reqDealDeliveryNum() {
        
        _tfNumber.resignFirstResponder()
        
        let company = _lbCompany.text
        let num = _tfNumber.text
        
        if num == "" {
            CommonUtil.showToast(NSLocalizedString("empty_deliver_num", comment:""))
            return
        }
        
        if (num?.count)! < 10 {
            CommonUtil.showToast(NSLocalizedString("worong_deliver_num", comment:""))
            return
        }
        
        gProgress.show()
        Net.reqDealDeliveryNum(
            accessToken         : gMeInfo.token,
            dealUid             : dicDealDetail.deal.dealUid,
            deliveryCompany     : company!,
            deliveryNumber      : num!,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("reg_deliver_num_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 택배사 목록 얻기
    func getDeliveryCompanies() {
        
        gProgress.show()
        Net.getDeliveryCompanies(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.ggetDeliveryCompaniesResult
                self.ggetDeliveryCompaniesResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //
    func ggetDeliveryCompaniesResult(_ data: Net.ggetDeliveryCompaniesResult) {
        arrCompnies = data.companies
    }
}
