//
//  EventDetailViewController.swift
//  selluv
//
//  Created by Dev on 12/11/17.
//

import UIKit

class EventDetailViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivDetailImg: UIImageView!

    var eventUid : Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getEventDetailInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
 
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 이벤트 상세 얻기
    func getEventDetailInfo() {
        
        gProgress.show()
        Net.getEventDetailInfo(
            accessToken : gMeInfo.token,
            eventUid    : eventUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getEventDetailInfoResult
                
                self.ivDetailImg.kf.setImage(with: URL(string: res.dicInfo.detailImg), placeholder: UIImage(named: "img_default"), options: [], progressBlock: nil, completionHandler: nil)
                self.lblTitle.text = res.dicInfo.title

                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
