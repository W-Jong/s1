//
//  PassSettingViewController.swift
//  selluv
//  비밀번호 변경
//  Created by Dev on 12/11/17.
//  modified by PJH on 06/03/18.

import UIKit
import Toast_Swift

class PassSettingViewController: BaseViewController {

    @IBOutlet weak var _tfSecuNum: UITextField!
    @IBOutlet weak var _tfSecuNew: UITextField!
    @IBOutlet weak var _tfSecuCom: UITextField!
    
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btRequest: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tfSecuCom.delegate = self
        _tfSecuNew.delegate = self
        _tfSecuNum.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setRequestBtn() {
        
        var addrInputed = false
        
        if !_tfSecuNum.text!.isEmpty && !_tfSecuNew.text!.isEmpty && !_tfSecuCom.text!.isEmpty {
            addrInputed = true
        }
        
        btnRequest.isEnabled = addrInputed
        btnRequest.backgroundColor = btnRequest.isEnabled ? UIColor.black : UIColor.lightGray
        
    }
    
    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                
                self.btRequest.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        self.btRequest.constant = 0
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        _tfSecuNum.resignFirstResponder()
        _tfSecuNew.resignFirstResponder()
        _tfSecuCom.resignFirstResponder()
    }
    
    @IBAction func onPassChange(_ sender: Any) {

        onBGTouched(self)
        changePass()
    }
    
    ////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////

    // 비리번호 변경
    func changePass() {
        
        if _tfSecuNum.text != gMeInfo.usrPass {
            MsgUtil.showUIAlert(NSLocalizedString("current_password_wrong", comment:""))
            return
        }
        
        if _tfSecuNew.text != _tfSecuCom.text {
            MsgUtil.showUIAlert(NSLocalizedString("new_password_incorrent", comment:""))
            return
        }

        gProgress.show()
        Net.changePass(
            accessToken     : gMeInfo.token,
            oldPassword     : _tfSecuNum.text!,
            newPassword     : _tfSecuNew.text!,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("change_password_success", comment:""))
                
                gMeInfo.usrPass = self._tfSecuNew.text
                let ud: UserDefaults = UserDefaults.standard
                ud.set(self._tfSecuNew.text, forKey: "usrPass")
                ud.synchronize()

        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}

extension PassSettingViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case _tfSecuNum:
            _tfSecuNew.becomeFirstResponder()
        case _tfSecuNew:
            _tfSecuCom.becomeFirstResponder()
        default: return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        default:
            setRequestBtn()
            break
        }
    }
    
}
