//
//  AlarmViewController.swift
//  selluv
//  알림목록
//  Created by Dev on 12/7/17.
//  modified by PJH on 03/03/18.

import UIKit

class AlarmViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    @IBOutlet weak var _tvList: UITableView!
    @IBOutlet weak var _tvListNews: UITableView!
    @IBOutlet weak var _tvListComment: UITableView!
    @IBOutlet weak var _vwMenuBar: UIView!
    @IBOutlet weak var _vwMenuBarTab: UIView!
    @IBOutlet weak var _vwMenuBarTabTrailing: NSLayoutConstraint!
    @IBOutlet weak var _vwMenuBarTabLeading: NSLayoutConstraint!
    @IBOutlet weak var _scvList: UIScrollView!
    
    @IBOutlet weak var _btnTabBusiness: UIButton!
    @IBOutlet weak var _btnTabNews: UIButton!
    @IBOutlet weak var _btnTabComment: UIButton!
    
    @IBOutlet weak var _imgT1: UIImageView!
    @IBOutlet weak var _imgT2: UIImageView!
    @IBOutlet weak var _imgT3: UIImageView!
    
    @IBOutlet weak var vwNoData: UIView!
    
    var arrAlarmList : Array<Alarm> = []
    var nPage : Int = 0
    var isLast : Bool = true
    var nCurPage : Int = 1
    
    enum EAlarmTab : Int {
        case business = 0
        case news
        case comment
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        
        nPage = 0
        nCurPage = 0
        getAlarmList()
        getAlarmcount()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Helper
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "AlarmTableViewCell", bundle: nil), forCellReuseIdentifier: "AlarmTableViewCell")
        
        _tvListNews.delegate = self
        _tvListNews.dataSource = self
        _tvListNews.register(UINib.init(nibName: "AlarmTableViewCell", bundle: nil), forCellReuseIdentifier: "AlarmTableViewCell")
        
        _tvListComment.delegate = self
        _tvListComment.dataSource = self
        _tvListComment.register(UINib.init(nibName: "AlarmTableViewCell", bundle: nil), forCellReuseIdentifier: "AlarmTableViewCell")

        _imgT1.isHidden = true
        _imgT2.isHidden = false
        _imgT3.isHidden = false
        
        _tvList.estimatedRowHeight = 45
        _tvList.rowHeight = UITableViewAutomaticDimension
        
        _tvListNews.estimatedRowHeight = 45
        _tvListNews.rowHeight = UITableViewAutomaticDimension
        
        _tvListComment.estimatedRowHeight = 45
        
        vwNoData.isHidden = true
    }
    
    func scrollUp() {
        if scrollDirection == .up {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollDown() {
        if scrollDirection == .down {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func pageMoved(pageNum: Int) {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        _btnTabBusiness.isSelected = false
        _btnTabNews.isSelected = false
        _btnTabComment.isSelected = false
        
        switch pageNum {
        case EAlarmTab.business.rawValue :
            _btnTabBusiness.isSelected = true
            clickedBtn = _btnTabBusiness
        case EAlarmTab.news.rawValue :
            _btnTabNews.isSelected = true
            clickedBtn = _btnTabNews
        case EAlarmTab.comment.rawValue :
            _btnTabComment.isSelected = true
            clickedBtn = _btnTabComment
        default:
            _btnTabBusiness.isSelected = true
            clickedBtn = _btnTabBusiness
        }
        
        _vwMenuBar.removeConstraint(_vwMenuBarTabLeading)
        _vwMenuBar.removeConstraint(_vwMenuBarTabTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        _vwMenuBarTabLeading = NSLayoutConstraint(item: _vwMenuBarTab, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: 0.0)
        _vwMenuBarTabTrailing = NSLayoutConstraint(item: _vwMenuBarTab, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        
        _vwMenuBar.addConstraint(_vwMenuBarTabLeading)
        _vwMenuBar.addConstraint(_vwMenuBarTabTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        nPage = 0
        nCurPage = pageNum
        getAlarmList()
    }
    
    //MARK: - OnClick
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickSetting(_ sender: Any) {
        pushVC("MYPAGE_ALARMSETTING_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @IBAction func onClickAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case EAlarmTab.business.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 0
        case EAlarmTab.news.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 1
        case EAlarmTab.comment.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 2
        default:
            _scvList.contentOffset.x = _scvList.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved(pageNum: clickedBtn.tag)
    }
    
    //MARK: - UITableViewDataSource protocol functions
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case _scvList:
            
            let newOffset = scrollView.contentOffset.x
            let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            self.pageMoved(pageNum: pageNum)
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
            
        default:
            return
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAlarmList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlarmTableViewCell", for: indexPath as IndexPath) as! AlarmTableViewCell
        
        let dic : Alarm = arrAlarmList[indexPath.row]
        
        cell._imgProfile.kf.setImage(with: URL(string: dic.usrProfileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        
        cell._imgCom.isHidden = false
        cell._vwComBg.isHidden = false
        
        // icon1->parcel, icon2->genuine, icon3->money, icon4->certification, icon5->redraw, icon6->notice, icon7->event, icon8->alarm, icon9->enrollemnt, icon10->discount
        if(dic.subKind == 1) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon1")
        }
        else if(dic.subKind == 2) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon2")
        }
        else if(dic.subKind == 3) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon3")
        }
        else if(dic.subKind == 6) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon4")
        }
        else if(dic.subKind == 11 || dic.subKind == 12) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon5")
        }
        else if(dic.subKind == 17) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon10")
        }
        else if(dic.subKind == 20) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon9")
        }
        else if(dic.subKind == 21) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon8")
        }
        else if(dic.subKind == 22) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon7")
            cell._imgCom.isHidden = true
            cell._vwComBg.isHidden = true
        }
        else if(dic.subKind == 23) {
            cell._imgProfile.image = UIImage(named:"ic_alarm_icon6")
            cell._imgCom.isHidden = true
            cell._vwComBg.isHidden = true
        }
        
        cell._imgCom.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        let 색1 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x999999)]
        let 색2 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor(hex: 0x333333)]

        let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  dic.content, attributes: 색2))
        contentStr.append(NSAttributedString(string: " " + CommonUtil.diffTime1(dic.regTime), attributes: 색1))
        cell._lbDetail.attributedText = contentStr
        
        if tableView == _tvListComment {
            cell._lbDetail.numberOfLines = 3
        } else {
            cell._lbDetail.numberOfLines = 0
        }
        
        if indexPath.row == arrAlarmList.count - 1 && !isLast {
            nPage = nPage + 1
            getAlarmList()
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 알림 목록 얻기
    func getAlarmList() {
        
        var kind = ""

        if nCurPage == 0 {
            kind = "DEAL"
        } else if nCurPage == 1 {
            kind = "NEWS"
        } else {
            kind = "REPLY"
        }
        
        gProgress.show()
        Net.getAlarmList(
            kind: kind,
            page: nPage,
            accessToken: gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AlarmResult
                self.getAlarmListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
    //알림 목록 얻기 결과
    func getAlarmListResult(_ data: Net.AlarmResult) {
        
        isLast = data.last
        if nPage == 0 {
            arrAlarmList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : Alarm = data.list[i]
            arrAlarmList.append(dic)
        }
        
        if arrAlarmList.count == 0 {
            vwNoData.isHidden = false
        } else {
            vwNoData.isHidden = true
            
            if nCurPage == 0 {
                _tvList.reloadData()
            } else if nCurPage == 1 {
                _tvListNews.reloadData()
            } else {
                _tvListComment.reloadData()
            }
        }
    }
    
    // 알림 갯수 얻기
    func getAlarmcount() {
        
        gProgress.show()
        Net.getAlarmCnt(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.AlarmCountResult
                
                self._imgT1.isHidden = true
                self._imgT2.isHidden = true
                self._imgT3.isHidden = true
                
                if res.dealCount == 0 {
                    self._imgT1.isHidden = true
                }
                else {
                    self._imgT1.isHidden = false
                }
                
                if res.newsCount == 0 {
                    self._imgT2.isHidden = true
                }
                else {
                    self._imgT2.isHidden = false
                }
                
                if res.replyCount == 0 {
                    self._imgT3.isHidden = true
                }
                else {
                    self._imgT3.isHidden = false
                }
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}
