//
//  CQTableViewCell.swift
//  selluv
//
//  Created by Dev on 12/8/17.
//

import UIKit
import Material

class CQTableViewCell: UITableViewCell {

    @IBOutlet weak var _btnSave: UIButton!
    @IBOutlet weak var _vwAsk: UIView!
    @IBOutlet weak var _tvAsk: TextView!
    @IBOutlet weak var _tvTitle: DesignableUITextField!    
    @IBOutlet weak var _lbComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initUI() {
        _tvAsk.font = UIFont.systemFont(ofSize: 12.0)
        _tvAsk.placeholderColor = UIColor(hex: 0xb5b5b5)
        _tvTitle.placeholderColor = UIColor(hex: 0xb5b5b5)
    }
    
}
