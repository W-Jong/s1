//
//  FAQTableViewCell.swift
//  selluv
//
//  Created by Dev on 12/9/17.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _vwTopLine: UIView!
    @IBOutlet weak var _vwBottomLine: UIView!
    @IBOutlet weak var _ivArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
