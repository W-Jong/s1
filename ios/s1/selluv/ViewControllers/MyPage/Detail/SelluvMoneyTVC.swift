//
//  SelluvMoneyTVC.swift
//  selluv
//
//  Created by Dev on 12/7/17.
//

import UIKit

class SelluvMoneyTVC: UITableViewCell {

    @IBOutlet weak var _btnNoMoney: UIButton!
    @IBOutlet weak var _btnBank: UIButton!
    @IBOutlet weak var _lbInfo: UILabel!
    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _htTop: NSLayoutConstraint!
    @IBOutlet weak var _htMain: NSLayoutConstraint!
    @IBOutlet weak var _vwTop: UIView!
    
    @IBOutlet weak var lbMoney: UILabel!
    @IBOutlet weak var lbCashBack: UILabel!
    
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbNum: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
