//
//  MainEventTableViewCell.swift
//  selluv
//
//  Created by Dev on 12/14/17.
//

import UIKit

class MainEventTableViewCell: UITableViewCell {

    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
