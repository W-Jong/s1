//
//  TravelPopup.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class TravelPopup: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    
    var code = ""
//    var cbOk: callback! = nil
//    public typealias callback = () -> ()
    var cbOk : callback! = nil
    public typealias callback = ( _ type : String) -> ()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(code: String, okCallback: callback! = nil) {
        self.init()
        
        self.code = code
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, code: String = "", okCallback: callback! = nil) {
        let sizePopup = TravelPopup(code: code, okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        vwDlg.layer.cornerRadius = 6
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func commentAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk("1")
        }

    }
    
    @IBAction func onClickKakao(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk("2")
        }
    }
    
    @IBAction func onClickCall(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        CommonUtil.callPhone(SELLUV_CONTACT)
    }
    
    @IBAction func hideAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////
