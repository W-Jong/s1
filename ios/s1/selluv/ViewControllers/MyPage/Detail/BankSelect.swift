//
//  BankSelect.swift
//  selluv
//
//  Created by Gambler on 12/28/17.
//

import UIKit

class BankSelect: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var picker2: UIPickerView!
    @IBOutlet weak var vwBg: UIView!
    
    var cbOk: callback! = nil
    public typealias callback = (_ size2: String) -> ()
    
    var sizeList2 : [String] = ["신한은행", "제주은행", "국민은행", "농협은행", "우리은행", "keb하나은행", "외환은행", "우체국", "기업은행", "sc은행", "씨티은행", "수협은행", "상호저축은행", "신용협동조합", "새마을금고", "경남은행", "전북은행", "광주은행", "부산은행", "대구은행", "hsbc","도이치", "jp모건", "bnp파리", "산업은행", "Boa"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animateAndChain(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }) { (finish) in
            self.vwBg.alpha = 0.8
            
        }
        
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, okCallback: callback! = nil) {
        let sizePopup = BankSelect(okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .coverVertical
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        btnOk.layer.cornerRadius = 5
    }
    
    func effectVC() {
        UIView.animateAndChain(withDuration: 0.1, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.0
        }, completion: nil).animateAndChain(withDuration: 0.1, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0
        }) { (finish) in
            self.vwBg.alpha = 0
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func okAction(_ sender: Any) {
        
        effectVC()
        
        if cbOk != nil {
            cbOk(String(sizeList2[picker2.selectedRow(inComponent: 0)]))
        }
        
    }
    
    @IBAction func closeAction(_ sender : Any) {
    
        effectVC()
    
    }
    

}

extension BankSelect : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sizeList2.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(sizeList2[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 48
    }
    
    
}
