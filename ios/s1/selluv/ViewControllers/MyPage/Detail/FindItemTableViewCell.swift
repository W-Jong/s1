//
//  FindItemTableViewCell.swift
//  selluv
//
//  Created by Dev on 12/10/17.
//

import UIKit

class FindItemTableViewCell: UITableViewCell {

    @IBOutlet weak var _wdBtns: NSLayoutConstraint!
    @IBOutlet weak var _btnDelect: UIButton!
    
    @IBOutlet weak var lblBrandEn: UILabel!
    @IBOutlet weak var lblbrandKo: UILabel!
    
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblPdt: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblcolor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
