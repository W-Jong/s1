//
//  BuyingInfoTVC.swift
//  selluv
//  구매내역 셀 정보
//  Created by Dev on 12/7/17.
//  modified by PJh on 04/04/18.

import UIKit

class BuyingInfoTVC: UITableViewCell {
    
    //com info
    
    @IBOutlet var ivThumb: UIImageView!
    @IBOutlet var btnDetail: UIButton!
    @IBOutlet var btnNego: UIButton!
    @IBOutlet var btnNickname: UIButton!
    @IBOutlet var btnBgUp: UIButton!
    @IBOutlet var btnBgDown: UIButton!
    
    //com info ui
    
    @IBOutlet var lbEBrand: UILabel!
    @IBOutlet var lbKBrand: UILabel!
    @IBOutlet var lbSize: UILabel!
    
    @IBOutlet var btnSeller: UIButton!
    @IBOutlet var lbSellerMoney: UILabel!
    
    @IBOutlet var lbResult: UILabel!
    @IBOutlet var lbResultMoney: UILabel!
    @IBOutlet var htResult: NSLayoutConstraint!
    
    @IBOutlet var btnNegoWhite: UIButton!
    @IBOutlet var btnNegoBlack: UIButton!
    @IBOutlet var btnNegoHidden: UIButton!
    
    @IBOutlet var htNego: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func model(_ dicInfo : DealListDto) {
        
        let pdtPrice : Int = dicInfo.deal.pdtPrice
        let reqPrice : Int = dicInfo.deal.reqPrice
        
        lbEBrand.text = dicInfo.deal.brandEn
        lbKBrand.text = dicInfo.deal.pdtTitle
        lbSize.text = dicInfo.deal.pdtSize
        btnSeller.setTitle(dicInfo.usr.usrNckNm, for: .normal)
        ivThumb.kf.setImage(with: URL(string: dicInfo.deal.pdtImg), placeholder: UIImage(named: "ic_logo"), options: [], progressBlock: nil, completionHandler: nil)
        
        switch dicInfo.deal.status {
        case 11:   //네고제안
            btnNego.setTitle("네고제안  ", for: .normal)
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 0
            htResult.constant = 25
            
            lbResult.isHidden = false
            lbResultMoney.isHidden = false
            
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            btnNegoHidden.isHidden = true
            
            break
        case 12:  //카운터네고제안
            btnNego.setTitle("카운터네고  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 45
            htResult.constant = 25
            
            lbResult.isHidden = false
            lbResultMoney.isHidden = false
            
            lbResult.isHidden = false
            lbResultMoney.isHidden = false
            
            btnNegoWhite.isHidden = false
            btnNegoBlack.isHidden = false
            btnNegoHidden.isHidden = true
            
            btnNegoWhite.setTitle("네고 거절", for: .normal)
            btnNegoWhite.setTitleColor(.black, for: .normal)
            btnNegoBlack.setTitle("네고 승인", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoWhite.setBackgroundImage(UIImage.init(named: "btn_nego_white"), for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            
            break
        case 1: //배송준비
            
            btnNego.setTitle("배송준비  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoWhite.isHidden = true
            btnNegoHidden.isHidden = true
            btnNegoBlack.isHidden = false
            
            btnNegoBlack.setTitle("주문 취소", for: .normal)
            btnNegoBlack.setTitleColor(.black, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_white"), for: .normal)
            
            if CommonUtil.diffHour(dicInfo.deal.payTime)  > 3 {
                btnNegoBlack.isHidden = true
                htNego.constant = 0
            }
            break
        case 2: //배송진행
            if dicInfo.deal.verifyPrice > 0 {
                btnNego.setTitle("배송진행  ", for: .normal)
                
                lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
                lbResult.text = "감정결과"
                lbResultMoney.text = "정품으로 판정 되었습니다."
                
                htNego.constant = 0
                htResult.constant = 25
                
                lbResult.isHidden = false
                lbResultMoney.isHidden = false
                
                btnNegoWhite.isHidden = true
                btnNegoBlack.isHidden = true
                
            } else {
                btnNego.setTitle("배송진행  ", for: .normal)
                
                lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
                lbResult.text = "네고 제안가"
                lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
                
                htNego.constant = 0
                htResult.constant = 5
                
                lbResult.isHidden = true
                lbResultMoney.isHidden = true
                
                btnNegoWhite.isHidden = true
                btnNegoBlack.isHidden = true
                
            }
            break
        case 3: //정품인증
            btnNego.setTitle("정품인증  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
            
        case 4: //배송완료
            btnNego.setTitle("배송완료  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = false
            btnNegoBlack.isHidden = false
            
            btnNegoWhite.setTitle("반품 신청", for: .normal)
            btnNegoWhite.setTitleColor(.black, for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoWhite.setBackgroundImage(UIImage.init(named: "btn_nego_white"), for: .normal)
            btnNegoBlack.setTitle("구매 확정", for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)

            break
        case 5: //거래완료
            btnNego.setTitle("거래완료  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = false
            
            btnNegoBlack.setTitle("거래 후기", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)

            break
            
        case 6: //정산완료
            btnNego.setTitle("정산완료  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = false
            
            btnNegoBlack.setTitle("거래 후기", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            
            break
        case 10:  //주문취소
            btnNego.setTitle("주문취소  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
        case 7: //반품접수
            btnNego.setTitle("반품신청  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
        case 8:  //반품승인
            btnNego.setTitle("반품승인  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            btnNegoBlack.setTitle("반품 확인", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            
            break
        case 9: //반품완료
            btnNego.setTitle("반품완료  ", for: .normal)
            
            lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
            lbResult.text = "네고 제안가"
            lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
        default:

            break
        }
    }
}

