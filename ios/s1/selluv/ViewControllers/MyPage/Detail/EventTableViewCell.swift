//
//  EventTableViewCell.swift
//  selluv
//
//  Created by Dev on 12/11/17.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _imgEvent: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
