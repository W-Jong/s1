//
//  CS_QADownTVC.swift
//  selluv
//
//  Created by World on 12/28/17.
//

import UIKit

class CS_QADownTVC: UITableViewCell {
    
    @IBOutlet var ivIcon: UIImageView!
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var lbTime: UILabel!
    @IBOutlet var ivArrow: UIImageView!
    @IBOutlet var btnHeader: UIButton!
    @IBOutlet var lbAnswer: UILabel!
    
    @IBOutlet var vwContent: UIView!
    @IBOutlet var vwContentHeight: NSLayoutConstraint!
    @IBOutlet var lbContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
