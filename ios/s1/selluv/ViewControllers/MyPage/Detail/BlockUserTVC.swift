//
//  BlockUserTVC.swift
//  selluv
//
//  Created by Dev on 12/10/17.
//

import UIKit

class BlockUserTVC: UITableViewCell {

    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _imgPhoto: UIImageView!
    @IBOutlet weak var _lbId: UILabel!
    @IBOutlet weak var _lbNickname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
