//
//  SellingInfoTVC.swift
//  selluv
//  판매테이블 셀
//  Created by Dev on 12/7/17.
//

import UIKit

class SellingInfoTVC: UITableViewCell {

    //com info
    
    @IBOutlet weak var ivThumb: UIImageView!
    @IBOutlet var btnDetail: UIButton!
    @IBOutlet var btnNego: UIButton!
    @IBOutlet var btnNickname: UIButton!
    @IBOutlet var btnBgUp: UIButton!
    @IBOutlet var btnBgDown: UIButton!
    
    //com info ui
    
    @IBOutlet var lbEBrand: UILabel!
    @IBOutlet var lbKBrand: UILabel!
    @IBOutlet var lbSize: UILabel!
    
    @IBOutlet var btnSeller: UIButton!
    @IBOutlet var lbSellerMoney: UILabel!
    
    @IBOutlet var lbResult: UILabel!
    @IBOutlet var lbResultMoney: UILabel!
    @IBOutlet var htResult: NSLayoutConstraint!
    
    @IBOutlet var btnNegoWhite: UIButton!
    @IBOutlet var btnNegoBlack: UIButton!
    @IBOutlet var btnNegoHidden: UIButton!
    
    @IBOutlet var htNego: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func model(_ dicInfo : DealListDto) {
        
        let pdtPrice : Int = dicInfo.deal.pdtPrice
        let reqPrice : Int = dicInfo.deal.reqPrice
        
        lbEBrand.text = dicInfo.deal.brandEn
        lbKBrand.text = dicInfo.deal.pdtTitle
        lbSize.text = dicInfo.deal.pdtSize
        btnSeller.setTitle(dicInfo.usr.usrNckNm, for: .normal)
        ivThumb.kf.setImage(with: URL(string: dicInfo.deal.pdtImg), placeholder: UIImage(named: "ic_logo"), options: [], progressBlock: nil, completionHandler: nil)

        btnSeller.setTitle(dicInfo.usr.usrNckNm, for: .normal)
        lbSellerMoney.text = CommonUtil.formatNum(pdtPrice) + " 원"
        lbResult.text = "네고 제안가"
        lbResultMoney.text = CommonUtil.formatNum(reqPrice) + " 원"
        
        switch dicInfo.deal.status {
        case 12: //카운터 네고
            btnNego.setTitle("카운터네고  ", for: .normal)
            
            lbResult.text = "카운터 네고 제안가"
            
            htNego.constant = 0
            htResult.constant = 25
            
            lbResult.isHidden = false
            lbResultMoney.isHidden = false
            
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
        case 11:  //네고제안
            btnNego.setTitle("네고제안  ", for: .normal)
            
            htNego.constant = 45
            htResult.constant = 25
            
            lbResult.isHidden = false
            lbResultMoney.isHidden = false
            
            btnNegoHidden.isHidden = false
            btnNegoWhite.isHidden = false
            btnNegoBlack.isHidden = false
            
            btnNegoWhite.setTitle("카운터 네고", for: .normal)
            btnNegoWhite.setTitleColor(.white, for: .normal)
            btnNegoBlack.setTitle("네고 승인", for: .normal)
            btnNegoHidden.setTitle("네고 거절", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            btnNegoWhite.setBackgroundImage(UIImage.init(named: "btn_nego_grey"), for: .normal)
            
            break
        case 1: //배송준비
            btnNego.setTitle("발송대기  ", for: .normal)
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = false
            
            btnNegoBlack.setTitle("판매 거절", for: .normal)
            btnNegoBlack.setTitleColor(.black, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_white"), for: .normal)
            
            break
        case 2:  //배송진행
            btnNego.setTitle("배송진행  ", for: .normal)
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
        case 3:  //정품인증
            btnNego.setTitle("배송진행  ", for: .normal)
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            
            break
        case 4: //배송완료
            btnNego.setTitle("배송완료  ", for: .normal)
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            btnNego.tag = 15
            break
        case 5:  //거래완료
            btnNego.setTitle("거래완료  ", for: .normal)
            
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            
            btnNegoWhite.setTitle("공유 리워드 신청", for: .normal)
            btnNegoWhite.setTitleColor(.black, for: .normal)
            btnNegoBlack.setTitle("거래 후기", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            btnNegoWhite.setBackgroundImage(UIImage.init(named: "btn_nego_long"), for: .normal)
            btnNego.tag = 16
            
            htNego.constant = 0
            
            if CommonUtil.diffDay(dicInfo.deal.compTime)  > 7 {
                btnNegoWhite.isHidden = true
            } else {
                btnNegoWhite.isHidden = false
                htNego.constant = 45
            }

            if CommonUtil.diffDay(dicInfo.deal.compTime)  > 3 {
                btnNegoBlack.isHidden = true
            } else {
                btnNegoBlack.isHidden = false
                htNego.constant = 45
            }
            
            break
        case 6:  // 정산완료

            btnNego.setTitle("정상완료  ", for: .normal)
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = false
            
            btnNegoBlack.setTitle("공유 리워드 신청", for: .normal)
            btnNegoBlack.setTitleColor(.black, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_long"), for: .normal)
            btnNego.tag = 17
            break
        case 10: //주문취소
            btnNego.setTitle("주문취소  ", for: .normal)
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            btnNego.tag = 18
            break
        case 7: //반품접수
            btnNego.setTitle("반품신청  ", for: .normal)
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = false
            btnNegoBlack.isHidden = false
            
            btnNegoWhite.setTitle("반품 거절", for: .normal)
            btnNegoBlack.setTitle("반품 승인", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            btnNegoWhite.setBackgroundImage(UIImage.init(named: "btn_nego_white"), for: .normal)
            btnNego.tag = 19
            break
        case 8: //반품승인
            btnNego.setTitle("반품승인  ", for: .normal)
            
            htNego.constant = 45
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = false
            
            btnNegoBlack.setTitle("반품 확인", for: .normal)
            btnNegoBlack.setTitleColor(.white, for: .normal)
            btnNegoBlack.setBackgroundImage(UIImage.init(named: "btn_nego_black"), for: .normal)
            btnNego.tag = 20
            break
        case 9: //반품완료
            btnNego.setTitle("반품완료  ", for: .normal)
            
            htNego.constant = 0
            htResult.constant = 5
            
            lbResult.isHidden = true
            lbResultMoney.isHidden = true
            
            btnNegoHidden.isHidden = true
            btnNegoWhite.isHidden = true
            btnNegoBlack.isHidden = true
            btnNego.tag = 21
            break
            
        default:
            break
        }
    }
}
