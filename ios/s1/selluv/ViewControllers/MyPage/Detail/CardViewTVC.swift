//
//  CardViewTVC.swift
//  selluv
//
//  Created by Dev on 12/10/17.
//

import UIKit

class CardViewTVC: UITableViewCell {

    @IBOutlet var _cardName: UILabel!
    @IBOutlet weak var _vwCard: UIView!
    @IBOutlet weak var _btnClose: UIButton!
    @IBOutlet weak var _btnSelect: UIButton!
    
    @IBOutlet weak var _imgCheckBox: UIImageView!
    
    @IBOutlet weak var lblCardNum: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initUI() {
        _vwCard.layer.cornerRadius = 6
    }
    
    @IBAction func onClickSelect(_ sender: Any) {
        if _btnSelect.isSelected {
            _btnSelect.isSelected = false
            _imgCheckBox.image = UIImage.init(named: "ic_checkbox_off")
        } else {
            _btnSelect.isSelected = true
            _imgCheckBox.image = UIImage.init(named: "ic_checkbox_on")
        }
    }
}
