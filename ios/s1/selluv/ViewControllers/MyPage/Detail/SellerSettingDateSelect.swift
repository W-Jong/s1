//
//  SellerSettingDateSelect.swift
//  selluv
//
//  Created by PJH on 05/03/18.
//

import UIKit

class SellerSettingDateSelect: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var picker2: UIPickerView!
    @IBOutlet weak var vwBg: UIView!
    
    var cbOk: callback! = nil
    public typealias callback = (_ nDate: Int) -> ()
    
    var arrDates : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animateAndChain(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.8
        }) { (finish) in
            self.vwBg.alpha = 0.8
            
        }
        
    }
    
    convenience init(okCallback: callback! = nil) {
        self.init()
        
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, okCallback: callback! = nil) {
        let sizePopup = SellerSettingDateSelect(okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .coverVertical
        
        vc.present(sizePopup, animated: true, completion: nil)
        
    }
    
    func initVC() {
        
        btnOk.layer.cornerRadius = 5
        var date : String = ""
        for i in 0..<30 {
            date = String(i + 1) + "일 후"
            arrDates.append(String(date))
        }
    }
    
    func effectVC() {
        UIView.animateAndChain(withDuration: 0.1, delay: 0.0, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0.0
        }, completion: nil).animateAndChain(withDuration: 0.1, delay: 0.1, options: .curveEaseInOut, animations: {
            self.vwBg.alpha = 0
        }) { (finish) in
            self.vwBg.alpha = 0
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func okAction(_ sender: Any) {
        
        effectVC()
        
        if cbOk != nil {
            cbOk(picker2.selectedRow(inComponent: 0) + 1)
        }
        
    }
    
    @IBAction func closeAction(_ sender : Any) {
    
        effectVC()
    
    }
}

extension SellerSettingDateSelect : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrDates.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrDates[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 48
    }
}
