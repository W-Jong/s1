//
//  BuyingInfoHeaderTVC.swift
//  selluv
//
//  Created by World on 2/3/18.
//

import UIKit

class BuyingInfoHeaderTVC: UITableViewCell {

    @IBOutlet weak var lbPrepare: UILabel!
    @IBOutlet weak var lbPrepareNumber: UILabel!
    
    @IBOutlet weak var lbExit: UILabel!
    @IBOutlet weak var lbExitNumber: UILabel!
    
    @IBOutlet weak var lbT1: UILabel!
    @IBOutlet weak var lbT1Number: UILabel!
    
    @IBOutlet weak var lbT2: UILabel!
    @IBOutlet weak var lbT2Number: UILabel!
    
    @IBOutlet weak var lbT3: UILabel!
    @IBOutlet weak var lbT3Number: UILabel!
    
    @IBOutlet weak var lbT4: UILabel!
    @IBOutlet weak var lbT4Number: UILabel!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTitleNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
