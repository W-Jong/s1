//
//  AlarmTableViewCell.swift
//  selluv
//
//  Created by Dev on 12/7/17.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {

    @IBOutlet weak var _lbDetail: UILabel!
    @IBOutlet weak var _vwComBg: UIView!
    @IBOutlet weak var _imgCom: UIImageView!
    @IBOutlet weak var _imgProfile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initUI() {
        _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2
        _imgProfile.clipsToBounds = true
    }
    
}
