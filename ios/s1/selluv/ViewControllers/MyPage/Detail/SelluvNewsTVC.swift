//
//  SelluvNewsTVC.swift
//  selluv
//
//  Created by Dev on 12/11/17.
//

import UIKit

class SelluvNewsTVC: UITableViewCell {

    @IBOutlet weak var _btnSelect: UIButton!
    @IBOutlet weak var _lbContent: UILabel!
    @IBOutlet weak var _lbDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
