//
//  CNegoPopup.swift
//  selluv
//  카운터 네고
//  Created by Gambler on 12/28/17.
//  modified by PJh on 02/04/18.

import UIKit

class CNegoPopup: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet var vwDlg: UIView!
    @IBOutlet var lcContentBottom: NSLayoutConstraint!
    @IBOutlet weak var lbMoney: UILabel!
    @IBOutlet weak var lbSlider: UISlider!
    @IBOutlet weak var lbMinMoney: UILabel!
    @IBOutlet weak var lbMaxMoney: UILabel!
    @IBOutlet weak var vwImage: UIView!
    @IBOutlet weak var _tfMoney: UITextField!
    @IBOutlet weak var _htBody: NSLayoutConstraint!
    
    @IBOutlet weak var ivPdt: UIImageView!
    @IBOutlet weak var lbBrandEn: UILabel!
    @IBOutlet weak var lbPdtName: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    var cbOk: callback! = nil
    var dicDealInfo  : DealListDto!
    var minPrice : Int!
    var maxPrice : Int!

    public typealias callback = (_ price : Int) -> ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initVC()
        setPdtData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(dicInfo: DealListDto, okCallback: callback! = nil) {
        self.init()
        
        self.dicDealInfo = dicInfo
        cbOk = okCallback
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    static func show(_ vc : UIViewController, dicInfo: DealListDto, okCallback: callback! = nil) {
        let sizePopup = CNegoPopup(dicInfo: dicInfo, okCallback: okCallback)
        
        sizePopup.modalPresentationStyle = .overCurrentContext
        sizePopup.modalTransitionStyle = .crossDissolve
        
        vc.present(sizePopup, animated: true, completion: nil)
    }
    
    func initVC() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        vwDlg.layer.cornerRadius = 6
        vwImage.layer.cornerRadius = 6
        
    }
    
    func setPdtData(){
        
        lbPdtName.text = dicDealInfo.deal.pdtTitle
        lbBrandEn.text = dicDealInfo.deal.brandEn
        lbSize.text = dicDealInfo.deal.pdtSize
        lbPrice.text = "￦ " + CommonUtil.formatNum(dicDealInfo.deal.price)
        ivPdt.kf.setImage(with: URL(string: dicDealInfo.deal.pdtImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        lbMinMoney.text = CommonUtil.formatNum(dicDealInfo.deal.reqPrice)
        lbMaxMoney.text = CommonUtil.formatNum(dicDealInfo.deal.price)
        
        minPrice = dicDealInfo.deal.reqPrice
        maxPrice = dicDealInfo.deal.price
        lbSlider.minimumValue = Float(minPrice)
        lbSlider.maximumValue = Float(maxPrice)
        
        _tfMoney.text = CommonUtil.formatNum(dicDealInfo.deal.reqPrice)
    }
    
    @objc func keyboardChange(_ aNotification: Notification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                
                //self._htBody.constant = -200
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardHide(_ aNotification: Notification) {
        self._htBody.constant = 0
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func hideAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func slideAction(_ sender: Any) {
        _tfMoney.text = String(Int(lbSlider.value))
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        _tfMoney.resignFirstResponder()
    }
    
    @IBAction func proposalAction(_ sender: Any) {
        _tfMoney.resignFirstResponder()
        
        if _tfMoney.text == "" {
            self.view.makeToast(NSLocalizedString("empty_proposal_money", comment:""))
            return
        }
        
        let proposalPrice : Int  = Int(_tfMoney.text!)!
        if proposalPrice < minPrice  || proposalPrice > maxPrice {
            self.view.makeToast(NSLocalizedString("reg_money_wrong", comment:""))
            return
        }
        
        dismiss(animated: true, completion: nil)
        
        if cbOk != nil {
            cbOk(proposalPrice)
        }
        
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension CNegoPopup : UITextFieldDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let strNew = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let len = strNew.count
        
        if textView == _tfMoney {
            _tfMoney.text = CommonUtil.formatNum(Int(strNew)!)
            return false
        }
        
        return true
    }
    
}
