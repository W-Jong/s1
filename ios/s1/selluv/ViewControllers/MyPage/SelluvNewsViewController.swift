//
//  SelluvNewsViewController.swift
//  selluv
//  공지사항
//  Created by Dev on 12/11/17.
//  modified by PJH on 03/02/18.

import UIKit

class SelluvNewsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var _tvList: UITableView!
    
    var arrNoticeList : Array<Notice> = []
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getNoticeList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "SelluvNewsTVC", bundle: nil), forCellReuseIdentifier: "SelluvNewsTVC")
    }
    
    @objc func onBtnGo2Detail(_ sender: UIButton) {
        
        let dic : Notice = arrNoticeList[sender.tag]
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc : SelluvNewsDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_SELLUVNEWSDETAIL_VIEW") as! SelluvNewsDetailViewController)
        vc.noticeUid = dic.noticeUid
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNoticeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelluvNewsTVC", for: indexPath as IndexPath) as! SelluvNewsTVC
        
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnGo2Detail(_:)), for: UIControlEvents.touchUpInside)
        
        let dic : Notice = arrNoticeList[indexPath.row]
        cell._lbContent.text = dic.title
        cell._lbDate.text = CommonUtil.getDateFromStamp(dic.regTime, type: "yyyy-MM-dd")
        
        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 공지사항 목록 얻기
    func getNoticeList() {
        
        gProgress.show()
        Net.getNoticeList(
            accessToken     : gMeInfo.token,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getNoticeListResult
                self.getNoticeResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //공지사항 목록 얻기 결과
    func getNoticeResult(_ data: Net.getNoticeListResult) {
        
        arrNoticeList = data.list
        _tvList.reloadData()
        
    }
    
}
