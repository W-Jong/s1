//
//  BuyingInfoViewController.swift
//  selluv
//  구매내역
//  Created by Dev on 12/7/17.
//  modified by PJH on 26/03/18.

import UIKit
import DGElasticPullToRefresh

class BuyingInfoViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    var header : BuyingInfoHeaderTVC?
    
    var arrBuyHisList   : Array<DealListDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!
    var totalCnt        : Int = 0
    var completedCount  : Int = 0
    var negoCount       : Int = 0
    var preparedCount   : Int = 0
    var progressCount   : Int = 0
    var sentCount       : Int = 0
    var verifiedCount   : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        _tvList.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.nPage = 0
                self?.getBuyHistoryList()
            })
            }, loadingView: loadingView)
        _tvList.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        _tvList.dg_setPullToRefreshBackgroundColor(_tvList.backgroundColor!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBuyHistoryList()
    }

    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib(nibName: "BuyingInfoHeaderTVC", bundle: nil), forCellReuseIdentifier: "BuyingInfoHeaderTVC")
        _tvList.register(UINib(nibName: "BuyingInfoTVC", bundle: nil), forCellReuseIdentifier: "BuyingInfoTVC")
        
        _tvList.estimatedRowHeight = 140
        _tvList.rowHeight = UITableViewAutomaticDimension
        _tvList.estimatedSectionHeaderHeight = 185
        _tvList.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    @objc func onBtnNegoWhite(_ sender: UIButton) {
        let dic : DealListDto = arrBuyHisList[sender.tag]
        
        switch dic.deal.status	 {
        case 12:  //카운터 네고
            //네고거절
            let alert = UIAlertController(title: "", message: NSLocalizedString("seller_nego_refuse_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealNegoRefuseCounter(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        case 4:  //배송완료
            //반품신청
          
          let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_HALFREQUEST_VIEW")) as! HalfRequestViewController
          vc.dicDealInfo = dic
          self.navigationController?.pushViewController(vc, animated: true)
            break
       
        default:
            break
        }
    }
    
    @objc func onBtnNegoBlack(_ sender: UIButton) {
        let dic : DealListDto = arrBuyHisList[sender.tag]
        
        switch dic.deal.status {
        case 12:  //카운터 네고
            //네고승인
            let alert = UIAlertController(title: "", message: NSLocalizedString("seller_nego_refuse_allow_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealNegoAllowCounter(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        case 4:  //배송완료
            //구매확정
            let alert = UIAlertController(title: "", message: NSLocalizedString("buy_confirm_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealComplete(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
        case 5:  //거래완료
            //거래후기
            let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_BUSINESSLATTER_VIEW")) as! BusinessLatterViewController
            vc.dicDealInfo = dic
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 1:  //배송준비
            //주문취소
            let alert = UIAlertController(title: "", message: NSLocalizedString("order_cancel_alert", comment:""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { action in
                self.reqDealCancel(_dealUid: dic.deal.dealUid)
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
            self.present(alert, animated: true)
            break
     
        default:
            break
        }
    }
    
    @objc func onBtnDetail(_ sender: UIButton) {
        let dic : DealListDto = arrBuyHisList[sender.tag]
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dic.deal.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : DealListDto = arrBuyHisList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.deal.sellerUsrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnNego(_ sender: UIButton) {
        let dic : DealListDto = arrBuyHisList[sender.tag]
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = dic.deal.dealUid
        nav.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnBg(_ sender: UIButton) {
        let dic : DealListDto = arrBuyHisList[sender.tag]
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = dic.deal.dealUid
        nav.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    //////////////////////////////////////////
    // MARK: - Delegate & DataSource
    //////////////////////////////////////////
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBuyHisList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuyingInfoTVC", for: indexPath as IndexPath) as! BuyingInfoTVC
        
        let dic : DealListDto = arrBuyHisList[indexPath.row]

        cell.model(dic)
        
        cell.btnNego.tag = indexPath.row
        cell.btnNego.addTarget(self, action:#selector(self.onBtnNego(_:)), for: UIControlEvents.touchUpInside)
        cell.btnDetail.tag = indexPath.row
        cell.btnDetail.addTarget(self, action:#selector(self.onBtnDetail(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNickname.tag = indexPath.row
        cell.btnNickname.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        cell.btnBgUp.tag = indexPath.row
        cell.btnBgUp.addTarget(self, action:#selector(self.onBtnBg(_:)), for: UIControlEvents.touchUpInside)
        cell.btnBgDown.tag = indexPath.row
        cell.btnBgDown.addTarget(self, action:#selector(self.onBtnBg(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNegoWhite.tag = indexPath.row
        cell.btnNegoWhite.addTarget(self, action:#selector(self.onBtnNegoWhite(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNegoBlack.tag = indexPath.row
        cell.btnNegoBlack.addTarget(self, action:#selector(self.onBtnNegoBlack(_:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == arrBuyHisList.count - 1 && !isLast {
            nPage = nPage + 1
            getBuyHistoryList()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        header = tableView.dequeueReusableCell(withIdentifier: "BuyingInfoHeaderTVC") as? BuyingInfoHeaderTVC
        header?.lbPrepareNumber.text = String(negoCount)
        header?.lbExitNumber.text = String(completedCount)
        header?.lbT1Number.text = String(preparedCount)
        header?.lbT2Number.text = String(verifiedCount)
        header?.lbT3Number.text = String(progressCount)
        header?.lbT4Number.text = String(sentCount)
        header?.lbTitleNumber.text = String(totalCnt)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 195
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 구매내역 얻기
    func getBuyHistoryList() {
        
        gProgress.show()
        Net.getBuyHistory(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self._tvList.dg_stopLoading()
                let res = result as! Net.getBuyHistoryResult
                self.getBuyHistoryResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //구매내역 얻기 결과
    func getBuyHistoryResult(_ data: Net.getBuyHistoryResult) {
        isLast = data.last
        totalCnt = data.totalElements
        
        completedCount = data.completedCount
        negoCount = data.negoCount
        preparedCount = data.preparedCount
        progressCount = data.progressCount
        sentCount = data.sentCount
        verifiedCount = data.verifiedCount

        if nPage == 0 {
            arrBuyHisList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : DealListDto = data.list[i]
            arrBuyHisList.append(dic)
        }
        _tvList.reloadData()
    }
    
    //구매자  카운터네고승인
    func reqDealNegoAllowCounter(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealNegoAllowCounter(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBuyHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //구매자  카운터네고거절
    func reqDealNegoRefuseCounter(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealNegoRefuseCounter(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBuyHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //구매확정
    func reqDealComplete(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealComplete(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBuyHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //주문최소
    func reqDealCancel(_dealUid : Int) {
        
        gProgress.show()
        Net.reqDealCancel(
            accessToken     : gMeInfo.token,
            dealUid         : _dealUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getBuyHistoryList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }

}
