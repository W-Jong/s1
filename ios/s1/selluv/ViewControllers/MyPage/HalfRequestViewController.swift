//
//  HalfRequestViewController.swift
//  selluv
//  구매자 판품신청
//  Created by Dev on 12/12/17.
//  modified by PJh on 02/04/18

import UIKit

class HalfRequestViewController: BaseViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var tvRequest: KMPlaceholderTextView!
    
    @IBOutlet var _scvMain : UIScrollView!
    
    @IBOutlet var _btnRequest : UIButton!
    @IBOutlet var _cvPhoto : UICollectionView!
    @IBOutlet var _vwPhoto : UIView!
    
    @IBOutlet weak var _tpScvHeight: NSLayoutConstraint!
    
    var dicDealInfo : DealListDto!
    var m_bKeyboard = false
    var arrPhotos : Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        _scvMain.delegate = self
        
        tvRequest.delegate = self
        tvRequest.placeholder = NSLocalizedString("deal_refund_req_hint", comment:"")

        showContent()
        
        _cvPhoto.delegate = self
        _cvPhoto.dataSource = self
        _cvPhoto.register(UINib.init(nibName: "RequestCVC", bundle: nil), forCellWithReuseIdentifier: "RequestCVC")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickPhoto(_ sender: Any) {
       goCameraPage()
    }
    
    @IBAction func onClickRequest(_ sender: Any) {
        if arrPhotos.count == 0 {
            CommonUtil.showToast(NSLocalizedString("upload_photo_alert", comment:""))
            return
        }
        reqDealRefund()
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        tvRequest.resignFirstResponder()
    }
    
    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                
                self._tpScvHeight.constant = -keyboardSize.height
                self.view.layoutIfNeeded()
                m_bKeyboard = true
            }
        }
    }
    
    func showContent() {
        if arrPhotos.count > 0 {
            _cvPhoto.isHidden = false
            _vwPhoto.isHidden = true
        } else {
            _cvPhoto.isHidden = true
            _vwPhoto.isHidden = false
        }
        _cvPhoto.reloadData()
    }
    
    func goCameraPage() {
        var arrImgs : [UIImage] = []
        for i in 0..<arrPhotos.count {
            let img = UIImageView()
            img.kf.setImage(with: URL(string: arrPhotos[i]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
            arrImgs.append(img.image!)
        }
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "SNS_SHARE_CAMERA_VIEW")) as! SnsSharelCameraViewController
        vc.assetsList = arrImgs
        vc.delegate = self
        vc.mType = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        self._tpScvHeight.constant = 0
        m_bKeyboard = false
    }
    
    @objc func onBtnDelete(_ sender: UIButton) {
        arrPhotos.remove(at: sender.tag)
        
        showContent()
    }
    
    @objc func onBtnSelect(_ sender: UIButton) {
        goCameraPage()
    }
    
    // TextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newString : NSString = NSString(string: textView.text)
        
        newString = newString.replacingCharacters(in: range, with: text) as NSString
        if (newString.length == 0) {
            _btnRequest.isEnabled = false
            _btnRequest.backgroundColor = UIColor.lightGray
        } else {
            _btnRequest.isEnabled = true
            _btnRequest.backgroundColor = UIColor.black
        }
        
        return true
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.size.width) / 3, height: (collectionView.bounds.size.width) / 3 * 120 / 150);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = _cvPhoto.dequeueReusableCell(withReuseIdentifier: "RequestCVC", for: indexPath as IndexPath) as! RequestCVC
        
        cell.ivProfile.kf.setImage(with: URL(string: arrPhotos[indexPath.row]), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action:#selector(self.onBtnSelect(_:)), for: UIControlEvents.touchUpInside)
        cell.btnDel.tag = indexPath.row
        cell.btnDel.addTarget(self, action:#selector(self.onBtnDelete(_:)), for: UIControlEvents.touchUpInside)


        return cell
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 구매자 판품 신청
    func reqDealRefund() {
        
        gProgress.show()
        Net.reqDealRefund(
            accessToken     : gMeInfo.token,
            dealUid         : dicDealInfo.deal.dealUid,
            photos          : arrPhotos,
            refundReason    : tvRequest.text,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("deal_refund_req_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}

extension HalfRequestViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case _scvMain:
            
            if m_bKeyboard {
                tvRequest.resignFirstResponder()
            }
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case _scvMain:
            
            if m_bKeyboard {
                tvRequest.resignFirstResponder()
            }
            
        default:
            return
        }
    }
}

extension HalfRequestViewController: SnsSharelCameraViewControllerDelegate {
    func setPhotos( photos : Array<String>, photosFiles : Array<String>, _type : Int){
        
        arrPhotos =  photos
        showContent()
    }
}
