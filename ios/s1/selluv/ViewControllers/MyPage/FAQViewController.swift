//
//  FAQViewController.swift
//  selluv
//
//  Created by Dev on 12/9/17.
//

import UIKit

class FAQViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    @IBOutlet weak var _tvListBuy: UITableView!
    @IBOutlet weak var _tvListAction: UITableView!
    @IBOutlet weak var _vwMenuBar: UIView!
    @IBOutlet weak var _vwMenuBarTab: UIView!
    @IBOutlet weak var _vwMenuBarTabTrailing: NSLayoutConstraint!
    @IBOutlet weak var _vwMenuBarTabLeading: NSLayoutConstraint!
    @IBOutlet weak var _scvList: UIScrollView!
    
    @IBOutlet weak var _btnTabSell: UIButton!
    @IBOutlet weak var _btnTabBuy: UIButton!
    @IBOutlet weak var _btnTabAction: UIButton!
    
    var m_selectedRow : Int = -1
    var nCurPage : Int = 0
    
    var arrFaqList : Array<Faq> = []
    
    enum EFAQTab : Int {
        case sell = 0
        case buy
        case action
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        getFaqList(faqType: nCurPage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvList.register(UINib.init(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQTableViewCell")
        _tvList.register(UINib.init(nibName: "FAQDownTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQDownTableViewCell")

        _tvListBuy.register(UINib.init(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQTableViewCell")
        _tvListBuy.register(UINib.init(nibName: "FAQDownTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQDownTableViewCell")

        _tvListAction.register(UINib.init(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQTableViewCell")
        _tvListAction.register(UINib.init(nibName: "FAQDownTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQDownTableViewCell")
        
        _tvList.estimatedRowHeight = 45
        _tvList.rowHeight = UITableViewAutomaticDimension
        
        _tvListBuy.estimatedRowHeight = 45
        _tvListBuy.rowHeight = UITableViewAutomaticDimension
        
        _tvListAction.estimatedRowHeight = 45
        _tvListAction.rowHeight = UITableViewAutomaticDimension
    }
    
    func scrollUp() {
        //        if vwTop.frame.origin.y >= 0 {
        //            return
        //        }
        //
        //        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
        //
        //            self.vwTop.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        //            self.vwMenu.frame = CGRect.init(x: 0, y: self.view.frame.size.height - self.vwMenu.frame.size.height, width: self.view.frame.size.width, height: self.vwMenu.frame.size.height)
        //            self.btnScroll2Top.frame = CGRect.init(x: self.view.frame.size.width - self.btnScroll2Top.frame.size.width - 15, y: self.view.frame.size.height, width: self.btnScroll2Top.frame.size.width, height: self.btnScroll2Top.frame.size.height)
        //        })
        
        if scrollDirection == .up {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollDown() {
        
        //        if vwTop.frame.origin.y < 0 {
        //            return
        //        }
        //        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
        //            self.vwTop.frame = CGRect.init(x: 0, y: -124, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        //            self.vwMenu.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height:              self.vwMenu.frame.size.height)
        //            self.btnScroll2Top.frame = CGRect.init(x: self.view.frame.size.width - self.btnScroll2Top.frame.size.width - 15, y: self.view.frame.size.height - 65, width: self.btnScroll2Top.frame.size.width, height: self.btnScroll2Top.frame.size.height)
        //        })
        
        if scrollDirection == .down {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func pageMoved(pageNum: Int) {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        _btnTabSell.isSelected = false
        _btnTabBuy.isSelected = false
        _btnTabAction.isSelected = false
        
        switch pageNum {
        case EFAQTab.sell.rawValue :
            _btnTabSell.isSelected = true
            clickedBtn = _btnTabSell
        case EFAQTab.buy.rawValue :
            _btnTabBuy.isSelected = true
            clickedBtn = _btnTabBuy
        case EFAQTab.action.rawValue :
            _btnTabAction.isSelected = true
            clickedBtn = _btnTabAction
        default:
            _btnTabSell.isSelected = true
            clickedBtn = _btnTabSell
        }
        
        _vwMenuBar.removeConstraint(_vwMenuBarTabLeading)
        _vwMenuBar.removeConstraint(_vwMenuBarTabTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        _vwMenuBarTabLeading = NSLayoutConstraint(item: _vwMenuBarTab, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: 0.0)
        _vwMenuBarTabTrailing = NSLayoutConstraint(item: _vwMenuBarTab, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        
        _vwMenuBar.addConstraint(_vwMenuBarTabLeading)
        _vwMenuBar.addConstraint(_vwMenuBarTabTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        nCurPage = pageNum
        getFaqList(faqType: pageNum)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickAsk(_ sender: Any) {	
        TravelPopup.show(self, code: "") { (type) in
            self.pushVC("MYPAGE_CS_QA_VIEW", storyboard: "Mypage", animated: true)
        }
    }
    
    @IBAction func onClickAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case EFAQTab.sell.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 0
        case EFAQTab.buy.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 1
        case EFAQTab.action.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 2
        default:
            _scvList.contentOffset.x = _scvList.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved(pageNum: clickedBtn.tag)
    }
    
    @objc func onBtnViewContent(_ sender: UIButton) {
        if m_selectedRow == sender.tag {
            m_selectedRow = -1
        } else {
            m_selectedRow = sender.tag
        }
        
        if _btnTabSell.isSelected {
            _tvList.reloadData()
        } else if _btnTabBuy.isSelected {
            _tvListBuy.reloadData()
        } else if _btnTabAction.isSelected {
            _tvListAction.reloadData()
        }
    }
    
    //MARK: - UITableViewDataSource protocol functions
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case _scvList:
            
            let newOffset = scrollView.contentOffset.x
            let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            self.pageMoved(pageNum: pageNum)
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
            
        default:
            return
        }
    }
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrFaqList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == m_selectedRow {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQDownTableViewCell", for: indexPath) as! FAQDownTableViewCell
        
        let dic : Faq = arrFaqList[m_selectedRow]
        cell.lblContent.text = dic.content

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell") as! FAQTableViewCell
        cell._btnSelect.tag = section
        cell._btnSelect.addTarget(self, action: #selector(onBtnViewContent(_:)), for: UIControlEvents.touchUpInside)
        
        cell._btnSelect.isSelected = (m_selectedRow == section)
        
        let dic : Faq = arrFaqList[section]
        cell.lblContent.text = dic.title
        
        if cell._btnSelect.isSelected {
            cell._vwTopLine.isHidden = true
            cell._vwBottomLine.isHidden = true
            cell._ivArrow.image = UIImage(named: "ic_undo_arrow_rotate")
        } else {
            cell._vwTopLine.isHidden = true
            cell._vwBottomLine.isHidden = false
            cell._ivArrow.image = UIImage(named: "ic_undo_arrow")
        }
        
        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // FAQ 문의 목록 얻기
    func getFaqList(faqType : Int) {
        
        var type : String = ""
        if faqType == 0 {
            type = "SELL"
        } else  if faqType == 1 {
            type = "BUY"
        } else {
            type = "ACTIVITY"
        }
        
        gProgress.show()
        Net.getFaqList(
            accessToken     : gMeInfo.token,
            faqType         : type,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getFaqListResult
                self.getQnaResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //FAQ 문의 목록 얻기 결과
    func getQnaResult(_ data: Net.getFaqListResult) {
        m_selectedRow = -1
        arrFaqList = data.list
        if _btnTabSell.isSelected {
            _tvList.reloadData()
        } else if _btnTabBuy.isSelected {
            _tvListBuy.reloadData()
        } else if _btnTabAction.isSelected {
            _tvListAction.reloadData()
        }
        
    }
    
}
