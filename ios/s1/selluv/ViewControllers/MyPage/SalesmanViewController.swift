//
//  SalesmanViewController.swift
//  selluv
//  판매자 설정
//  Created by Dev on 12/10/17.
//  modified by PJH on 05/03/18.

import UIKit

class SalesmanViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var _lbNMOff: UILabel!
    @IBOutlet weak var _vwNMOn: UIView!
    
    @IBOutlet weak var _vwKeyboardTop: UIView!
    @IBOutlet weak var _htKeyboardTop: NSLayoutConstraint!
    
    @IBOutlet weak var _lbVCOff: UILabel!
    @IBOutlet weak var _lbVCOn: UILabel!
    
    @IBOutlet weak var _btnNM: UIButton!
    @IBOutlet weak var _btnVC: UIButton!
    
    @IBOutlet weak var _vwMenuItem: UIView!
    @IBOutlet weak var _vwMenu: UIView!
    @IBOutlet weak var _vwMenuBarTabLeading: NSLayoutConstraint!
    @IBOutlet weak var _vwMenuBarTabTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var _btnNMSwitch: UISwitch!
    @IBOutlet weak var _btnVCSwitch: UISwitch!
    
    @IBOutlet weak var _tfMoney: UITextField!
    @IBOutlet weak var _lbTapTitle: UILabel!
    
    @IBOutlet weak var _scvList: UIScrollView!
    
    var nHolidayDate : Int = 0
    var price        : Int = 0
    
    enum ESalesTab : Int {
        case NM = 0
        case VC
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        _scvList.delegate = self
        
        if gMeInfo.freeSendPrice == "-1" {
            _btnNMSwitch.isOn = false
        } else {
            _btnNMSwitch.isOn = true
        }

        if gMeInfo.sleepYn == 0 {
            _btnVCSwitch.isOn = false
        } else {
            _btnVCSwitch.isOn = true
        }
    }
    
    func scrollUp() {
        
        if scrollDirection == .up {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollDown() {
        
        if scrollDirection == .down {
            return
        }
        
        view.setNeedsLayout()
        UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func pageMoved(pageNum: Int) {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        _btnNM.isSelected = false
        _btnVC.isSelected = false
       
        switch pageNum {
        case ESalesTab.NM.rawValue :
            _btnNM.isSelected = true
            clickedBtn = _btnNM
        case ESalesTab.VC.rawValue :
            _btnVC.isSelected = true
            clickedBtn = _btnVC
        default:
            _btnNM.isSelected = true
            clickedBtn = _btnNM
        }
        
        _vwMenu.removeConstraint(_vwMenuBarTabLeading)
        _vwMenu.removeConstraint(_vwMenuBarTabTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        _vwMenuBarTabLeading = NSLayoutConstraint(item: _vwMenuItem, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: 20.0)
        _vwMenuBarTabTrailing = NSLayoutConstraint(item: _vwMenuItem, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: -20.0)
        
        _vwMenu.addConstraint(_vwMenuBarTabLeading)
        _vwMenu.addConstraint(_vwMenuBarTabTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
    }
    
    @IBAction func onClickAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case ESalesTab.NM.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 0
        case ESalesTab.VC.rawValue : _scvList.contentOffset.x = _scvList.frame.size.width * 1
        default:
            _scvList.contentOffset.x = _scvList.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        pageMoved(pageNum: clickedBtn.tag)
    }
    
    @objc func keyboardChange(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0 {
                
                _htKeyboardTop.constant = keyboardSize.height
                _vwKeyboardTop.isHidden = false
                view.setNeedsLayout()
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardHide(_ notification: Notification) {
        
        _htKeyboardTop.constant = 0
        _vwKeyboardTop.isHidden = true
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    //MARK: - UITableViewDataSource protocol functions
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case _scvList:
            
            let newOffset = scrollView.contentOffset.x
            let pageNum = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 2
            
            self.pageMoved(pageNum: pageNum)
            
        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
            
        default:
            return
        }
    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickBg(_ sender: Any) {
        _tfMoney.resignFirstResponder()
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        _tfMoney.resignFirstResponder()
        if (_tfMoney.text?.isEmpty)! {
            CommonUtil.showToast(NSLocalizedString("empty_price_alert", comment:""))
            return
        }
        price = Int(_tfMoney.text!)!
        reqSellerSetting()
    }
    
    @IBAction func onClickNMClick(_ sender: Any) {
        if _btnNMSwitch.isOn {
            _lbNMOff.isHidden = true
            _vwNMOn.isHidden = false
        } else {
            _lbNMOff.isHidden = false
            _vwNMOn.isHidden = true
            
            price = 0
        }
        reqSellerSetting()
    }
    
    @IBAction func onClickVCClick(_ sender: Any) {
        if _btnVCSwitch.isOn {
            _lbVCOff.isHidden = true
            _lbVCOn.isHidden = false
            
            SellerSettingDateSelect.show(self){ (nDate) in
                self.nHolidayDate = nDate
                
                self.reqSellerSetting()
            }
        } else {
            _lbVCOff.isHidden = false
            _lbVCOn.isHidden = true
            
            nHolidayDate = 0
            
            reqSellerSetting()
        }
        
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    //판매자 설정
    func reqSellerSetting() {
        
        gProgress.show()
        Net.reqSellerSetting(
            accessToken     : gMeInfo.token,
            freeSendPrice   : String(price),
            isFreeSend      : _btnNMSwitch.isOn ? true : false,
            timeOutDate     : String(nHolidayDate),
            isSleep         : _btnVCSwitch.isOn ? true : false,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("setting_success", comment:""))
                
//                gMeInfo.sleepTime = nHolidayDate
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_NOT_EXISTS {  //
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
            
        })
    }
    
}
