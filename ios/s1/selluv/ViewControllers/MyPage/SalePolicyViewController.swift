//
//  SalePolicyViewController.swift
//  selluv
//  판매 및 환불정책
//  Created by Dev on 12/8/17.
//  modified by PJH on 03/02/18.

import UIKit

class SalePolicyViewController: BaseViewController {

    @IBOutlet weak var lblContent: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getLincese()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    
    // 약관 얻기
    func getLincese() {
        
        gProgress.show()
        Net.OtherLincese(
            accessToken: gMeInfo.token,
            licenseType: "SELLUV_LICENSE",
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.OtherLineseResult
                self.getLinceseResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ID_DUPLICATED {  //아이디 중복됨
                CommonUtil .showToast(err)
            } else if code == USER_ERROR_ACCESS_TOKEN {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getLinceseResult(_ data: Net.OtherLineseResult) {
        lblContent.text = data.content
        
    }
}
