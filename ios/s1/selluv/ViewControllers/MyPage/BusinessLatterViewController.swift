//
//  BusinessLatterViewController.swift
//  selluv
//  거래후기
//  Created by Dev on 12/11/17.
//  modified by PJh on 02/04/18

import UIKit
import Toast_Swift

class BusinessLatterViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var _imgPhoto: UIButton!
    @IBOutlet weak var _btnNormal: UIButton!
    @IBOutlet weak var _btnOk: UIButton!
    @IBOutlet weak var _btnNo: UIButton!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet var _tvAsk: KMPlaceholderTextView!
    @IBOutlet weak var _tpMainHeight: NSLayoutConstraint!
    @IBOutlet weak var _btnSave: UIButton!
    
    var dicDealInfo : DealListDto!
    var mPoint = 2
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvAsk.delegate = self
        _tvAsk.placeholder = NSLocalizedString("deal_review_write_hint", comment:"")
        
        ivProfile.layer.cornerRadius = ivProfile.frame.size.width / 2
        ivProfile.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setData() {
        ivProfile.kf.setImage(with: URL(string: dicDealInfo.usr.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        
        let type1 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor(hex: 333333)]
        let type2 = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10), NSAttributedStringKey.foregroundColor: UIColor(hex: 666666)]
        
        let contentStr = NSMutableAttributedString(attributedString: NSAttributedString(string:  dicDealInfo.usr.usrNckNm, attributes: type1))
        contentStr.append(NSAttributedString(string: " 님과의 거래\n    어떠셨어요?", attributes: type2))
        lbTitle.attributedText = contentStr

    }

    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @IBAction func onClickPhoto(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicDealInfo.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNickname(_ sender: Any) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dicDealInfo.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickOk(_ sender: Any) {
        mPoint = 2
        _btnOk.isSelected = true
        _btnNormal.isSelected = false
        _btnNo.isSelected = false
    }
    
    @IBAction func onClickNormal(_ sender: Any) {
        mPoint = 1
        _btnOk.isSelected = false
        _btnNormal.isSelected = true
        _btnNo.isSelected = false
    }
    
    @IBAction func onClickNo(_ sender: Any) {
        mPoint = 0
        _btnOk.isSelected = false
        _btnNormal.isSelected = false
        _btnNo.isSelected = true
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        reqDealReview()
    }
    
    @IBAction func onBGTouched(_ sender: Any) {
        _tvAsk.resignFirstResponder()
    }
    
    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height > 0.0 {
                
                self._tpMainHeight.constant = -keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        self._tpMainHeight.constant = 0
       
    }
    
    // TextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newString : NSString = NSString(string: textView.text)
        
        if textView == _tvAsk {
            if newString.length > 120 {
                return false
            }
        }
        
        newString = newString.replacingCharacters(in: range, with: text) as NSString
        if (newString.length == 0) {
            _btnSave.isEnabled = false
            _btnSave.backgroundColor = UIColor.lightGray
        } else {
            if (newString.length > 10) {
                _btnSave.isEnabled = true
                _btnSave.backgroundColor = UIColor.black
            }
        }
        
        return true
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 거래후기 작성
    func reqDealReview() {
        
        let content = _tvAsk.text
        if (content?.count)! < 10 {
            CommonUtil.showToast(NSLocalizedString("worong_writer_letter", comment:""))
            return
        }
        
        gProgress.show()
        Net.reqDealReview(
            accessToken     : gMeInfo.token,
            dealUid         : dicDealInfo.deal.dealUid,
            content         : content!,
            point           : mPoint,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("deal_review_write_success", comment:""))
                self.popVC()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
}
