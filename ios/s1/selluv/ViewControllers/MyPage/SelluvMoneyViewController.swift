//
//  SelluvMoneyViewController.swift
//  selluv
//  셀럽머니
//  Created by Dev on 12/7/17.
//  modified by PJH on 26/03/18.

import UIKit

class SelluvMoneyViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tvList: UITableView!
    
    var arrMoneyHisList : Array<UsrMoneyHisDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!
    var money           : Int!
    var cashbackMoney   : Int!
    var totalCnt        : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getMoneyHistoryList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        _tvList.delegate = self
        _tvList.dataSource = self
        _tvList.register(UINib.init(nibName: "SelluvMoneyTVC", bundle: nil), forCellReuseIdentifier: "SelluvMoneyTVC")
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        popVC()
    }
    
    @objc func onBtnNoMoney(_ sender: UIButton) {
        pushVC("MYPAGE_EVENT_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @objc func onBtnBank(_ sender: UIButton) {
        pushVC("MYPAGE_ACCOUNT_VIEW", storyboard: "Mypage", animated: true)
    }
    
    @objc func onBtnSelect(_ sender: UIButton) {
        if sender.tag == 0 {
            return
        }
        
        let dic : UsrMoneyHisDto = arrMoneyHisList[sender.tag]
        switch dic.kind {
        case 1: //구매주문상세
            goBuyPage(_uid: dic.targetUid)
            break
        case 2:  //판매주문상세
            goSellPage(_uid: dic.targetUid)
            break
        case 3: //판매주문상세
            goSellPage(_uid: dic.targetUid)
            break
        case 4:  //상품상세
            goPdtDetailPage(_uid: dic.targetUid)
            break
        case 5: //유저페이지
            goUserPage(_uid: dic.targetUid)
            break
        case 6: //유저페이지
            goUserPage(_uid: dic.targetUid)
            break
        case 7: //유저페이지
            goUserPage(_uid: dic.targetUid)
            break
        case 8:
            //주문상세페이지를 호출해보고 구매주문상세 혹은 판매주문상세 페이지로 이동시킨다.
            getDealDetailInfo(_uid: dic.targetUid)
            break
        case 9: //구매주문상세
            goBuyPage(_uid: dic.targetUid)
            break
        default:
            break
        }
    }
    
    //유저페이지로
    func goUserPage(_uid : Int) {
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //판매상세페이지로
    func goSellPage(_uid : Int) {
        
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SELLORDERDETAIL_VIEW")) as! SellOrderDetailViewController
        vc.goPage = DealPage.SELL.rawValue
        vc.dealUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //구매상세페이지로
    func goBuyPage(_uid : Int) {
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = _uid
        nav.pushViewController(vc, animated: true)
    }
    
    //상품 상세페이지로
    func goPdtDetailPage(_uid : Int) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = _uid
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMoneyHisList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelluvMoneyTVC", for: indexPath as IndexPath) as! SelluvMoneyTVC
        
        let dic : UsrMoneyHisDto = arrMoneyHisList[indexPath.row]
        if indexPath.row == 0 {
            cell._htTop.constant = 228
            cell._htMain.constant = 0
            cell.lbMoney.text = CommonUtil.formatNum(money)
            cell.lbCashBack.text = CommonUtil.formatNum(cashbackMoney)
            cell._lbInfo.text = "( " + String(arrMoneyHisList.count - 1) + " )"
        } else {
            cell._htTop.constant = 0
            cell._htMain.constant = 48
            cell.lbContent.text = dic.content
            cell.lbNum.text = dic.hintContent
            cell.lbTime.text = CommonUtil.getDateFromStamp(dic.regTime, type: "yyyy.MM.dd")
            cell.lbPrice.text = CommonUtil.formatNum(dic.amount) + " 원"
            if dic.amount >= 0 {
                cell.lbPrice.textColor = UIColor.init(hex: 0x333333)
            } else {
                cell.lbPrice.textColor = UIColor.init(hex: 0xFF3B7E)
            }
        }
        
        cell._btnBank.tag = indexPath.row
        cell._btnBank.addTarget(self, action:#selector(self.onBtnBank(_:)), for: UIControlEvents.touchUpInside)
        cell._btnNoMoney.tag = indexPath.row
        cell._btnNoMoney.addTarget(self, action:#selector(self.onBtnNoMoney(_:)), for: UIControlEvents.touchUpInside)
        cell._btnSelect.tag = indexPath.row
        cell._btnSelect.addTarget(self, action:#selector(self.onBtnSelect(_:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == arrMoneyHisList.count - 1 && !isLast {
            nPage = nPage + 1
            getMoneyHistoryList()
        }
        
        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 228
        } else {
            return 48
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 셀럽머니 변경이력 얻기
    func getMoneyHistoryList() {
        
        gProgress.show()
        Net.getMoneyHistory(
            accessToken     : gMeInfo.token,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getMoneyHistoryResult
                self.getMoneyHistoryResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //셀럽머니 변경이력 얻기 결과
    func getMoneyHistoryResult(_ data: Net.getMoneyHistoryResult) {
        isLast = data.last
        totalCnt = data.totalElements
        money = data.money
        cashbackMoney = data.cashbackMoney
        
        if nPage == 0 {
            arrMoneyHisList.removeAll()
            arrMoneyHisList.append(UsrMoneyHisDto("")) //header
        }
        
        for i in 0..<data.list.count {
            let dic : UsrMoneyHisDto = data.list[i]
            arrMoneyHisList.append(dic)
        }
        _tvList.reloadData()
    }
    
    // 주문상세
    func getDealDetailInfo(_uid : Int) {
        
        gProgress.show()
        Net.getDealDetail(
            accessToken     : gMeInfo.token,
            dealUid         : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getDealDetailResult
                self.getDealDetailResult(res, targetUid: _uid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //주문상세 얻기 결과
    func getDealDetailResult(_ data: Net.getDealDetailResult , targetUid : Int) {
       
        if data.dealDetail.deal.sellerUsrUid == gMeInfo.usrUid {
            //판매주문상세
            goSellPage(_uid: targetUid)
        } else {
            //구매주문상세
            goBuyPage(_uid: targetUid)
        }
    }
}
