//
//  UserFollowTVC.swift
//  selluv
//
//  Created by World on 12/21/17.
//

import UIKit

class UserFollowTVC: UITableViewCell {

    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var btnNickname: UIButton!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var ivPhoto: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
