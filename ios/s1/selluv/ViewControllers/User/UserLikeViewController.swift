//
//  UserLikeViewController.swift
//  selluv
//  매너포인트
//  Created by Gambler on 12/12/17.
//  modified by PJH on 22/03/18.

import UIKit

class UserLikeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    
    @IBOutlet weak var lbTopPoint: UILabel!
    @IBOutlet weak var tvT1: UITableView!
    @IBOutlet weak var tvT2: UITableView!
    @IBOutlet weak var tvT3: UITableView!
    @IBOutlet weak var tvT4: UITableView!
    
    @IBOutlet weak var lbAllCnt: UILabel!
    @IBOutlet weak var vwLikeMain: UIView!
    @IBOutlet weak var vwLike: UIView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnMenuT1: UIButton!
    @IBOutlet weak var btnMenuT2: UIButton!
    @IBOutlet weak var btnMenuT3: UIButton!
    @IBOutlet weak var btnMenuT4: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    
    enum ETab : Int {
        case T1 = 0
        case T2
        case T3
        case T4
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    var usrUid      : Int!
    var point       : Int!
    var nPage       : Int = 0
    var isLast      : Bool!
    var nCurPage    : Int = ETab.T1.rawValue
    var arrReviewList  : Array<ReviewListDto> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        vwLike.layer.cornerRadius = 10
        vwLikeMain.layer.cornerRadius = 10
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        // Collection view attributes
        
        tvT1.delegate = self
        tvT1.dataSource = self
        tvT1.register(UINib.init(nibName: "UserLikeTVC", bundle: nil), forCellReuseIdentifier: "UserLikeTVC")
        
        tvT2.delegate = self
        tvT2.dataSource = self
        tvT2.register(UINib.init(nibName: "UserLikeTVC", bundle: nil), forCellReuseIdentifier: "UserLikeTVC")
        
        tvT3.delegate = self
        tvT3.dataSource = self
        tvT3.register(UINib.init(nibName: "UserLikeTVC", bundle: nil), forCellReuseIdentifier: "UserLikeTVC")
        
        tvT4.delegate = self
        tvT4.dataSource = self
        tvT4.register(UINib.init(nibName: "UserLikeTVC", bundle: nil), forCellReuseIdentifier: "UserLikeTVC")
        
        self.vwTop.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        
        lbTopPoint.text = CommonUtil.formatNum(point)
        getDataList()
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuT1.isSelected = false
        btnMenuT2.isSelected = false
        btnMenuT3.isSelected = false
        btnMenuT4.isSelected = false
        
        switch nCurPage {
        case ETab.T1.rawValue :
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
        case ETab.T2.rawValue :
            btnMenuT2.isSelected = true
            clickedBtn = btnMenuT2
        case ETab.T3.rawValue :
            btnMenuT3.isSelected = true
            clickedBtn = btnMenuT3
        case ETab.T4.rawValue :
            btnMenuT4.isSelected = true
            clickedBtn = btnMenuT4
        default:
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        nPage = 0
        getDataList()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnGo2User(_ sender: UIButton) {
        let dic : ReviewListDto = arrReviewList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.peerUsr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
    }
    
    @IBAction func menuAction(_ sender: Any) {
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case ETab.T1.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 0
        case ETab.T2.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 1
        case ETab.T3.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 2
        case ETab.T4.rawValue :
            scvView.contentOffset.x = scvView.frame.size.width * 3
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        nCurPage = clickedBtn.tag
        pageMoved()
    }
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserLikeTVC", for: indexPath as IndexPath) as! UserLikeTVC
        
        let dic : ReviewListDto = arrReviewList[indexPath.row]
        
        if dic.review.point == 2 {
            cell.imgStatus.image = UIImage.init(named: "ic_ok_activity")
            cell.lbSatisfaction.text = NSLocalizedString("satisfaction", comment:"")
        } else if dic.review.point == 1 {
            cell.imgStatus.image = UIImage.init(named: "ic_normal_activity")
            cell.lbSatisfaction.text = NSLocalizedString("normal", comment:"")
        } else if dic.review.point == 0 {
            cell.imgStatus.image = UIImage.init(named: "ic_no_activity")
            cell.lbSatisfaction.text = NSLocalizedString("unsatisfaction", comment:"")
        }
        
        cell.btnPhoto.kf.setImage(with: URL(string: dic.peerUsr.profileImg), for: .normal, placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell.lbTime.text = CommonUtil.diffTime1(dic.review.regTime)
        
        cell.btnNickname.setTitle(dic.peerUsr.usrNckNm, for: .normal)
        var content = ""
        if dic.review.type == 1 {
            content = "(판매자)" + dic.review.content
        } else {
            content = "(구매자)" + dic.review.content
        }
        cell.lbContent.text = content

        cell.btnPhoto.tag = indexPath.row
        cell.btnPhoto.addTarget(self, action:#selector(self.onBtnGo2User(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNickname.tag = indexPath.row
        cell.btnNickname.addTarget(self, action:#selector(self.onBtnGo2User(_:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == arrReviewList.count - 1 && !isLast {
            nPage = nPage + 1
            getDataList()
        }
        
        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    func getDataList() {
        var nIdx = -1
        switch nCurPage {
        case ETab.T1.rawValue:
            nIdx = -1
            break
        case ETab.T2.rawValue:
            nIdx = 2
            break
        case ETab.T3.rawValue:
            nIdx = 1
            break
        case ETab.T4.rawValue:
            nIdx = 0
            break

        default:
            break
        }
        getUsrWishList(_idx: nIdx)
    }
    
    // 매너포인트 변경이력(거래후기) 얻기 얻기
    func getUsrWishList(_idx : Int) {
        
        gProgress.show()
        Net.getusrRevieList(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            point           : _idx,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getUsrReviewResult
                self.getUsrReviewResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //매너포인트 변경이력(거래후기) 얻기 결과
    func getUsrReviewResult(_ data: Net.getUsrReviewResult) {
        
        isLast = data.last
        lbAllCnt.text = String.init(format: "(%d)", data.totalElements)
        let allCnt = data.countOnePoint + data.countTwoPoint
        let point  = data.countOnePoint*2 + data.countTwoPoint
        lbTopPoint.text = CommonUtil.formatNum(point)
        
        btnMenuT1.setTitle("총 " + String(allCnt + data.countZeroPoint), for: .normal)
        btnMenuT1.setTitle("총 " + String(allCnt + data.countZeroPoint), for: .selected)
        btnMenuT2.setTitle(" " + String(data.countTwoPoint), for: .normal)
        btnMenuT2.setTitle(" " + String(data.countTwoPoint), for: .selected)
        btnMenuT3.setTitle(" " + String(data.countOnePoint), for: .normal)
        btnMenuT3.setTitle(" " + String(data.countOnePoint), for: .selected)
        btnMenuT4.setTitle(" " + String(data.countZeroPoint), for: .normal)
        btnMenuT4.setTitle(" " + String(data.countZeroPoint), for: .selected)
        
        if nPage == 0 {
            arrReviewList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : ReviewListDto = data.list[i]
            arrReviewList.append(dic)
        }
        
        if nCurPage == ETab.T1.rawValue {
            tvT1.reloadData()
        } else if nCurPage == ETab.T2.rawValue {
            tvT2.reloadData()
        } else if nCurPage == ETab.T3.rawValue {
            tvT3.reloadData()
        } else {
            tvT4.reloadData()
        }
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension UserLikeViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            nCurPage = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 4
            
            self.pageMoved()
            
        case tvT1, tvT2, tvT3, tvT4:

            self.scrollDirection = EScrollDirection.none

            //                let curScrollPos = scrollView.contentOffset.y
            //                if curScrollPos == 0 {
            //                    // scroll Down
            //                    self.scrollUp()
            //                } else if curScrollPos >= abs(scrollView.contentSize.height - scrollView.bounds.size.height)  {
            //                    // scroll Up
            //                    self.scrollDown()
            //                }
            //
            //                scrollPos = curScrollPos

            //                return

        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case tvT1, tvT2, tvT3, tvT4:

            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollDirection = EScrollDirection.up
            }

            scrollPos = curScrollPos

        default:
            return
        }
    }
}
