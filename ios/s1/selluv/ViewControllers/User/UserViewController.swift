//
//  UserViewController.swift
//  selluv
//  회원정보 페이지
//  Created by Gambler on 12/12/17.
//  modified by PJH on 22/03/18.

import UIKit
import Toast_Swift

class UserViewController: BaseViewController {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var scvView: UIScrollView!
    @IBOutlet weak var clvT1: UICollectionView!
    @IBOutlet weak var clvT2: UICollectionView!
    @IBOutlet weak var clvT3: UICollectionView!
    
    @IBOutlet weak var vwLine: UIView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnMenuT1: UIButton!
    @IBOutlet weak var btnMenuT2: UIButton!
    @IBOutlet weak var btnMenuT3: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnFindSettings: UIButton!
    @IBOutlet weak var btnNickname: UIButton!
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var lbPoint: UILabel!
    @IBOutlet weak var lbFollowCnt: UILabel!
    @IBOutlet weak var lbfollowingCnt: UILabel!
    @IBOutlet weak var ivBackBG: UIImageView!
    @IBOutlet weak var lblPdtAllCnt: UILabel!
    @IBOutlet weak var lbNickname: UILabel!
    @IBOutlet var lbTabSubTitle: UILabel!
    
    @IBOutlet weak var lcTopTop: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var btnScroll2Top: UIButton!
    @IBOutlet weak var btnScroll2TopTop: NSLayoutConstraint!
    var pStart : CGPoint?;
    var arrClvT : [UICollectionView] = []
    
    let MAXIMUM_TOP_HEIGHT : CGFloat = 455.0
    
    // 고정부분 높이
    let FIX_HEIGHT : CGFloat = 114.0
    
    //zoom효과관련
    weak var selectedImageView : UIImageView?
    var id : Int?
    
    enum ETab : Int {
        case T1 = 0
        case T2
        case T3
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    var usrUid      : Int!  //회원uid
    var dicUsrInfo  : UsrInfoDto!
    var nPage       : Int = 0
    var isLast      : Bool!
    var nCurPage    : Int = ETab.T1.rawValue
    var arrPdtList  : Array<PdtListDto> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidLoad()
        
        btnFindSettings.isSelected = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        ivProfile.layer.cornerRadius = ivProfile.frame.size.width / 2
        
        arrClvT = [clvT1, clvT2, clvT3]
        
        // Collection view attributes
        clvT1.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        clvT1.alwaysBounceVertical = true
        
        for i in 0..<arrClvT.count {
            // Add the waterfall layout to your collection view
            arrClvT[i].collectionViewLayout = layout
            
            let viewNib = UINib(nibName: "MainItemCVC", bundle: nil)
            arrClvT[i].register(viewNib, forCellWithReuseIdentifier: "cell")
            
            arrClvT[i].isUserInteractionEnabled = false;
        }
        
        self.vwTop.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        self.btnScroll2Top.frame = CGRect.init(x: self.view.frame.size.width - self.btnScroll2Top.frame.size.width - 15, y: self.view.frame.size.height - 65, width: self.btnScroll2Top.frame.size.width, height: self.btnScroll2Top.frame.size.height)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let TOP_HEIGHT : CGFloat = MAXIMUM_TOP_HEIGHT
        let TOUCH_LIMIT_Y : CGFloat = -(TOP_HEIGHT - FIX_HEIGHT);
        
        if((dic["isScrollDown"] as? Bool)  == true) {
            UIView.beginAnimations("menuHoverAnim", context: nil)
            UIView.setAnimationDuration(TabAnimationTime)
            UIView.setAnimationDelegate(self)
            
            lcTopTop.constant = TOUCH_LIMIT_Y;
            vwTopTop.alpha = 0
            lbTitle.alpha = 1
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            UIView.commitAnimations()
        } else if((dic["item"] as? Bool)  == true) {
            btnMenuT2.isSelected = true
            nCurPage = 1
            pageMoved()
            scvView.contentOffset.x = scvView.frame.size.width * 1
            
            UIView.commitAnimations()
        }  else if((dic["style"] as? Bool)  == true) {
            btnMenuT3.isSelected = true
            nCurPage = 2
            pageMoved()
            scvView.contentOffset.x = scvView.frame.size.width * 2
            
            UIView.commitAnimations()
        }
        getOtherUsrInfo()
        getDataList()
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuT1.isSelected = false
        btnMenuT2.isSelected = false
        btnMenuT3.isSelected = false
        
        switch nCurPage {
        case ETab.T1.rawValue :
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
            lbTabSubTitle.text = "님의 아이템"
        case ETab.T2.rawValue :
            btnMenuT2.isSelected = true
            clickedBtn = btnMenuT2
            lbTabSubTitle.text = "님의 잇템"
        case ETab.T3.rawValue :
            btnMenuT3.isSelected = true
            clickedBtn = btnMenuT3
            lbTabSubTitle.text = "님의 스타일"
        default:
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
            lbTabSubTitle.text = "님의 아이템"
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
        
        nPage = 0
        getDataList()
    }
    
    func scrollUp() {
        
        if scrollDirection == .up {
            return
        }
        
        if btnMenuT1.isSelected || btnMenuT2.isSelected || btnMenuT3.isSelected {
            UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
                self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: 0))
            })
        }
    }
    
    func scrollDown() {
        
        if scrollDirection == .down {
            return
        }
        
        if btnMenuT1.isSelected || btnMenuT2.isSelected {
            UIView.animate(withDuration: MenuAnimationTime, animations: { () -> Void in
                self.btnScroll2Top.layer.setAffineTransform(CGAffineTransform(translationX: 0, y: -130))
            })
        }
        
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @IBAction func searchAction(_ sender: Any) {
        popVC()
    }
   
    //팔로우 터치
    @IBAction func followAction2(_ sender: Any) {
        if gMeInfo.usrUid == usrUid {
            pushVC("MYPAGE_PROFILE_VIEW", storyboard: "Mypage", animated: true)
        } else {
            if dicUsrInfo.usrLikeStatus {
//                btnFollow.isSelected = false
                self.delFollow(_uid: self.usrUid)
            } else {
                let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                acAlert.title = "[" + btnNickname.title(for: .normal)! + "] " + NSLocalizedString("follow_cancel_alert", comment:"")
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("follow_cancel", comment:""), style: .default, handler: { (action: UIAlertAction!) in
//                    self.btnFollow.isSelected = true
                    
                    self.reqFollow(_uid: self.usrUid)
                }))
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
                present(acAlert, animated: true, completion: nil)
            }
        }
    }

    //오른쪽 상단 메뉴
    @IBAction func favoriteAction(_ sender: Any) {
        if gMeInfo.usrUid != dicUsrInfo.usrUid {
            if dicUsrInfo.usrBlockStatus {
                let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("reporting", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                    Blame.show(self) { (reasson, detail) in
                        self.reqReport(_uid: self.dicUsrInfo.usrUid, kind: 1, type: reasson, content: detail)
                    }
                }))
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("unblocking", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                    self.requsrUnblock()
                }))
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
                present(acAlert, animated: true, completion: nil)
            } else {
                let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("reporting", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                    Blame.show(self) { (reasson, detail) in
                        self.reqReport(_uid: self.dicUsrInfo.usrUid, kind: 1, type: reasson, content: detail)
                    }
                }))
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("blocking", comment:""), style: .default, handler: { (action: UIAlertAction!) in
                    let acAlert = UIAlertController(title: "", message: NSLocalizedString("usr_block_alert", comment:""), preferredStyle: .alert)
                    acAlert.addAction(UIAlertAction(title: NSLocalizedString("yes", comment:""), style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                        
                        self.requsrBlock()
                        
                    }))
                    acAlert.addAction(UIAlertAction(title: NSLocalizedString("no", comment:""), style: .cancel, handler: nil))
                    
                    self.present(acAlert, animated: true, completion: nil)
                    
                }))
                acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
                present(acAlert, animated: true, completion: nil)
            }
        } else {
            let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("profile_modify", comment:""), style: .default, handler: { (action: UIAlertAction!) in self.pushVC("MYPAGE_PROFILE_VIEW", storyboard: "Mypage", animated: true)
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
            present(acAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func search(_ sender: Any) {
        btnFindSettings.isSelected = true
        pushVC("FILTER_VIEW", storyboard: "Top", animated: true)
    }
    
    //매너포인트
    @IBAction func likeAction(_ sender: Any) {
        
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserLikeViewController = (storyboard.instantiateViewController(withIdentifier: "USER_LIKE_VIEW") as! UserLikeViewController)
        vc.usrUid = usrUid
        vc.point = dicUsrInfo.point
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func followAction(_ sender: UIButton) {
        if sender.tag == 0 {
            
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
            let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
            vc.following = false
            vc.usrUid = usrUid
            vc.topName = dicUsrInfo.usrNckNm
            self.navigationController?.pushViewController(vc, animated: true)
        } else if sender.tag == 1 {
//            dic["ing"] = true
//            pushVC("USER_FOLLOW_VIEW", storyboard: "User", animated: true, dic: dic)
            
            let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
            let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
            vc.following = true
            vc.usrUid = usrUid
            vc.topName = dicUsrInfo.usrNckNm
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case ETab.T1.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 0
        case ETab.T2.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 1
        case ETab.T3.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 2
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        nCurPage = clickedBtn.tag
        pageMoved()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuT1.isSelected {
            clvT1.scrollToTop(isAnimated: true)
        } else if btnMenuT2.isSelected {
            clvT2.scrollToTop(isAnimated: true)
        } else if btnMenuT3.isSelected {
            clvT3.scrollToTop(isAnimated: true)
        }
    }
    
    @IBAction func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let clv : UICollectionView!
        if btnMenuT1.isSelected {
            clv = arrClvT[0]
        } else if btnMenuT2.isSelected {
            clv = arrClvT[1]
        } else {
            clv = arrClvT[2]
        }

        let touchPoint : CGPoint = recognizer.location(in: self.view)
        if (recognizer.state == UIGestureRecognizerState.began) {
            pStart = touchPoint;
        } else {
            // 상단부분 높이
            let TOP_HEIGHT : CGFloat = MAXIMUM_TOP_HEIGHT
            // 터치시 상단 리미트 값
            let TOUCH_LIMIT_Y : CGFloat = -(TOP_HEIGHT - FIX_HEIGHT);
            
            let delta : CGFloat = touchPoint.y - pStart!.y
            let nowH : CGFloat = lcTopTop.constant;
            
            if (nowH + delta <= TOUCH_LIMIT_Y) {
                lcTopTop.constant = TOUCH_LIMIT_Y;
                vwTopTop.alpha = 0
                lbTitle.alpha = 1
                clv.isUserInteractionEnabled = true
                NSLog("current pos y = %f", nowH + delta)
                		
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else if (nowH + delta > 0) {
                lcTopTop.constant = 0.0
                vwTopTop.alpha = 1
                lbTitle.alpha = 0
                NSLog("nowH + delta > 0")
                
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            } else {
                lcTopTop.constant = nowH + delta;
                vwTopTop.alpha = 1-(lcTopTop.constant / TOUCH_LIMIT_Y)
                lbTitle.alpha = lcTopTop.constant / TOUCH_LIMIT_Y
                clv.isUserInteractionEnabled = false
                NSLog("else pos y = %f", nowH + delta)
                
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }
            pStart = touchPoint;
        }
    }
    
    @objc func onBtnStyle(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        let dicPdtStyle     : PdtStyleDao        = dic.pdtStyle  //스타일
        getPdtStyleDetailInfo(_pdtStyleUid: dicPdtStyle.pdtStyleUid, pdtUid: dic.pdt.pdtUid)
    }
    
    @objc func onBtnLike(_ sender: UIButton) {
        if let path = sender.withValue {
            let indexPath = path["index"] as! IndexPath
            var cell : MainItemCVC!
            var scrollPos : CGFloat!
            if btnMenuT1.isSelected {
                cell = clvT1.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT1.contentOffset.y
            } else if btnMenuT2.isSelected {
                cell = clvT2.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT2.contentOffset.y
            } else {
                cell = clvT3.cellForItem(at: indexPath) as! MainItemCVC
                scrollPos = clvT3.contentOffset.y
            }
            
            let dic         : PdtListDto   = arrPdtList[indexPath.row]
            let dicPdt      : PdtMiniDto  = dic.pdt  //상품
            var status = -1
            if dic.pdtLikeStatus {
                status = 0
                delPdtLike(_uid: dicPdt.pdtUid)
            } else {
                status = 1
                setPdtLike(_uid: dicPdt.pdtUid)
            }
            showHeart(self, status, cell, scrollPos)
            
        }
    }
    
    @objc func onBtnBrand(_ sender: UIButton) {
        
        let dic : PdtListDto = arrPdtList[sender.tag]
        
        let storyboard = UIStoryboard(name: "Brand", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BRAND_FOLLOW_VIEW") as! BrandFollowViewController
        controller.barndUid = dic.brand.brandUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func onBtnUser(_ sender: UIButton) {
        let dic : PdtListDto = arrPdtList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usr.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnTitle(_ sender: UIButton) {
        let dic  : PdtListDto = arrPdtList[sender.tag]
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        vc.pdtUid = dic.pdt.pdtUid
        self.navigationController?.pushViewController(vc, animated: true)

    }

    
    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    func getDataList() {
        
        switch nCurPage {
        case ETab.T1.rawValue:
            getUsrPdtList()
            break
        case ETab.T2.rawValue:
            getUsrWishList()
            break
        case ETab.T3.rawValue:
            getUsrStyleList()
            break
        default:
            break
        }
    }
    
    // 회원정보 얻기
    func getOtherUsrInfo() {
        
        gProgress.show()
        Net.getOtherUsrDetailInfo(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.OtherUsrDetailInfoResult
                self.getOtherUsrInfoResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                //self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //회원정보 얻기 결과
    func getOtherUsrInfoResult(_ data: Net.OtherUsrDetailInfoResult) {
        
        dicUsrInfo = data.usrDto
        
        //set user data
        ivProfile.kf.setImage(with: URL(string: dicUsrInfo.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        ivBackBG.kf.setImage(with: URL(string: dicUsrInfo.profileBackImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        lbPoint.text = CommonUtil.formatNum(dicUsrInfo.point)
        lbFollowCnt.text = CommonUtil.formatNum(dicUsrInfo.usrFollowerCount)
        lbfollowingCnt.text = CommonUtil.formatNum(dicUsrInfo.usrFollowingCount)
        
        btnNickname.setTitle(dicUsrInfo.usrNckNm, for: .normal)
        lbNickname.text = dicUsrInfo.usrNckNm
        lbTitle.text = dicUsrInfo.usrNckNm
        
        if dicUsrInfo.usrLikeStatus {
            btnFollow.isSelected = true
        } else {
            btnFollow.isSelected = false
        }
        
        //본인인 경우
        if gMeInfo.usrUid == usrUid {
            btnFollow.isSelected = true
            btnFollow.setTitle(NSLocalizedString("profile_modify", comment:""), for: .normal)
        }
    }
    
    // 회원의 아이템 목록 얻기
    func getUsrPdtList() {
        
        gProgress.show()
        Net.getUsrPdtList(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getUsrPdtListResult
                self.getUsrPdtListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                //self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 회원의 스타일 목록 얻기
    func getUsrStyleList() {
        
        gProgress.show()
        Net.getUsrStyleList(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getUsrPdtListResult
                self.getUsrPdtListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                //self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 회원의 잇템 목록 얻기
    func getUsrWishList() {
        
        gProgress.show()
        Net.getUsrWishList(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.getUsrPdtListResult
                self.getUsrPdtListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                //self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    //목록 얻기 결과
    func getUsrPdtListResult(_ data: Net.getUsrPdtListResult) {
        
        isLast = data.last
        lblPdtAllCnt.text = String.init(format: "(%d)", data.totalElements)
        
        if nPage == 0 {
            arrPdtList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : PdtListDto = data.list[i]
            arrPdtList.append(dic)
        }
        
        if nCurPage == ETab.T1.rawValue {
            clvT1.reloadData()
        } else if nCurPage == ETab.T2.rawValue {
            clvT2.reloadData()
        } else {
            clvT3.reloadData()
        }
    }
    
    // 스타일 팝업 상세 정보 얻기
    func getPdtStyleDetailInfo(_pdtStyleUid : Int, pdtUid : Int) {
        
        gProgress.show()
        Net.getPdtStyleDetailInfo(
            accessToken     : gMeInfo.token,
            pdtStyleUid     : _pdtStyleUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                let res = result as! Net.DetailPdtStyleResult
                self.getStyleDetailInfoResult(res, pdtUid: pdtUid)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                //self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    func getStyleDetailInfoResult(_ data: Net.DetailPdtStyleResult, pdtUid : Int) {
        
        var arrImgs : [String] = []
        for i in 0..<data.list.count{
            let dic : PdtStyleDao = data.list[i]
            
            arrImgs.append(dic.styleImg)
        }
        
        MainPopup.show(self, image: arrImgs) {
            
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
            vc.pdtUid = pdtUid
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // 유저 차단하기
    func requsrBlock() {
        
        gProgress.show()
        Net.reqUsrBlock(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("usr_block_success", comment:""))
                self.getOtherUsrInfo()
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 차단 해제하기
    func requsrUnblock() {
        
        gProgress.show()
        Net.reqUsrUnblock(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("usr_unblock_success", comment:""))
                self.getOtherUsrInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 신고 하기
    func reqReport(_uid : Int, kind : Int, type : String, content : String) {
        
        gProgress.show()
        Net.reqReport(
            accessToken     : gMeInfo.token,
            targetUid       : _uid,
            kind            : kind,
            content         : content,
            reportType      : type,
            success: { (result) -> Void in
                gProgress.hide()
                
                CommonUtil.showToast(NSLocalizedString("report_submist_success", comment:""))
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 하기
    func reqFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getOtherUsrInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 해제하기
    func delFollow(_uid : Int) {
        
        gProgress.show()
        Net.delFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getOtherUsrInfo()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 하기
    func setPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.setPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getDataList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 상품 좋아요 취소하기
    func delPdtLike(_uid : Int) {
        
        gProgress.show()
        Net.delPdtLike(
            accessToken     : gMeInfo.token,
            pdtUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.getDataList()
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token, not
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else if code == USER_TEMP_NOT_PERMISSION { // permission error
                LoginSuggestViewController.show(self) { (type) in
                    if type == SIGNUP { // 회원가입으로
                        self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
                    } else { //회원가입없이 둘러보기
                        gMeInfo.token = "temp"
                        self.replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
                    }
                }
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension UserViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            nCurPage = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            self.pageMoved()
            
        default:
            
            return
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case clvT1, clvT2, clvT3:
            // 상단부분 높이
            let TOP_HEIGHT : CGFloat = MAXIMUM_TOP_HEIGHT
            // 터치시 상단 리미트 값
            let TOUCH_LIMIT_Y : CGFloat = -(TOP_HEIGHT - FIX_HEIGHT);
            
            if (scrollView.contentOffset.y < 0.0) {
                let nowH : CGFloat = lcTopTop.constant;
                if (nowH - scrollView.contentOffset.y > 0) {
                    lcTopTop.constant = 0;
                    vwTopTop.alpha = 1
                    lbTitle.alpha = 0
                } else {
                    lcTopTop.constant = nowH - scrollView.contentOffset.y;
                    vwTopTop.alpha = 1-(lcTopTop.constant / TOUCH_LIMIT_Y)
                    lbTitle.alpha = lcTopTop.constant / TOUCH_LIMIT_Y
                }
                scrollView.isUserInteractionEnabled = false;
            }
            
            if lbTitle.alpha < 1 {
                return
            }
            
            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDown()
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollUp()
                self.scrollDirection = EScrollDirection.up
            }

            scrollPos = curScrollPos
            
        default:
            return
        }
    }
}

extension UserViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MainItemCVC
        
        let dic  : PdtListDto = arrPdtList[indexPath.row]
        self.selectedImageView = cell._imgCom
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DETAIL_VIEW") as! DetailViewController
        controller.pdtUid = dic.pdt.pdtUid
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension UserViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPdtList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MainItemCVC
        
        let dic  : PdtListDto = arrPdtList[indexPath.row]
        
        cell._imgCom.kf.setImage(with: URL(string: dic.pdt.profileImg), placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbComName.text = String.init(format: "%@ %@ %@ %@", dic.brand.nameKo, dic.pdt.colorName, dic.pdt.pdtModel, dic.category.categoryName)
        cell._btnStyle.kf.setImage(with: URL(string: dic.pdtStyle.styleImg), for: .normal, placeholder: UIImage(named: "img_default1"), options: [], progressBlock: nil, completionHandler: nil)

        cell._lbBrand.text = dic.brand.nameEn
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        //pdt
        if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
            height = width * ratio
        }
        cell._imgComHeight.constant = height
        
        if dic.pdt.originPrice <= dic.pdt.price {
            cell.hiddenDown(price: dic.pdt.price)
        } else {
            cell.showDown(origin: dic.pdt.originPrice, price: dic.pdt.price)
        }
        cell.lbSize.text = dic.pdt.pdtSize
        cell._lbLike.text = String(dic.pdtLikeCount)
        cell._imgHeart.image = UIImage(named: dic.pdtLikeStatus ? "ic_like_hart" : "ic_dislike_hart")
        cell._imgSoldOut.isHidden = dic.pdt.status == 2 ? false : true //판매 완료 상태체크
        
        //user
        cell._lbName.text = dic.usr.usrNckNm
        cell._imgProfile.kf.setImage(with: URL(string: dic.usr.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        cell._lbTime.text = CommonUtil.diffTime(dic.pdt.edtTime)
        
        cell._btnStyle.tag = indexPath.row
        cell._btnStyle.addTarget(self, action:#selector(self.onBtnStyle(_:)), for: UIControlEvents.touchUpInside)
        cell._btnLike.withValue = ["index" : indexPath]
        cell._btnLike.addTarget(self, action:#selector(self.onBtnLike(_:)), for: UIControlEvents.touchUpInside)
        cell._btnBrand.tag = indexPath.row
        cell._btnBrand.addTarget(self, action:#selector(self.onBtnBrand(_:)), for: UIControlEvents.touchUpInside)
        cell._btnUser.tag = indexPath.row
        cell._btnUser.addTarget(self, action:#selector(self.onBtnUser(_:)), for: UIControlEvents.touchUpInside)
        
        cell._btnProduct.tag = indexPath.row
        cell._btnProduct.addTarget(self, action:#selector(self.onBtnTitle(_:)), for: UIControlEvents.touchUpInside)

        if indexPath.row == arrPdtList.count - 1 && !isLast {
            nPage = nPage + 1
            getDataList()
        }
        
        return cell
    }
}

extension UserViewController : CHTCollectionViewDelegateWaterfallLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
//        let width = (collectionView.bounds.width - 10) / 2
//        let brandImage = UIImage(named: indexPath.row % 2 == 0 ? "bg_style_user_man" : "bg_style_user_woman")
//        let height = width * (brandImage?.size.height)! / (brandImage?.size.width)!
//
//        return CGSize(width: width, height: height + 153)
        let dic : PdtListDto = arrPdtList[indexPath.row]
        
        let width = (collectionView.bounds.width - 30) / 2
        var height : CGFloat = 0
        var ratio : CGFloat = 0
        
        if dic.pdtProfileImgWidth == nil || dic.pdtProfileImgHeight == nil {
            height = width
        } else {
            ratio = CGFloat(dic.pdtProfileImgHeight) / CGFloat(dic.pdtProfileImgWidth)
            height = width * ratio
        }
        
        if dic.pdt.originPrice <= dic.pdt.price {
            height = height + 113
        } else {
            height = height + 133
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 0)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StyleGridHeaderView", for: indexPath as IndexPath)
    }
    
}

