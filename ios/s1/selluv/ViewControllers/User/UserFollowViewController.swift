//
//  UserFollowViewController.swift
//  selluv
//  팔로잉/팔로워
//  Created by Gambler on 12/12/17.
//  modified by PJH on 06/03/18.

import UIKit
import DGElasticPullToRefresh

class UserFollowViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    //////////////////////////////////////////
    // MARK: - Variables
    //////////////////////////////////////////
    
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var scvView: UIScrollView!
    
    @IBOutlet weak var tvT1: UITableView!
    @IBOutlet weak var tvT2: UITableView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopTop: NSLayoutConstraint!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnMenuT1: UIButton!
    @IBOutlet weak var btnMenuT2: UIButton!
    @IBOutlet weak var vwMenuBar: UIView!
    @IBOutlet weak var vwMenuHoverBar: UIView!
    @IBOutlet weak var vwMenuHoverBarLeading: NSLayoutConstraint!
    @IBOutlet weak var vwMenuHoverBarTrailing: NSLayoutConstraint!
    @IBOutlet weak var lbComTitle: UILabel!
    
    @IBOutlet weak var lblFollowCnt: UILabel!
    
    var usrUid          : Int!
    var following       : Bool!
    var topName         : String!
    
    var arrFollowList    : Array<BlockUsrDto> = []
    var nPage           : Int = 0
    var isLast          : Bool!

    var nCurPage        : Int!
    enum ETab : Int {
        case T1 = 0
        case T2
    }
    
    enum EScrollDirection {
        case none
        case up
        case down
    }
    
    var scrollPos : CGFloat = 0
    var scrollDirection = EScrollDirection.none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initVC()
        
        addPullToRefrshh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        lblTitleName.text = topName
        if(following) {
            
            btnMenuT2.isSelected = true
            scvView.contentOffset.x = scvView.frame.size.width * 1
            UIView.commitAnimations()
            
            nCurPage = ETab.T2.rawValue
            
        } else {
            nCurPage = ETab.T1.rawValue
            
        }
        pageMoved()
    }
    
    func addPullToRefrshh() {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        tvT1.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.getFollowerList()
            })
            }, loadingView: loadingView)
        tvT1.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        tvT1.dg_setPullToRefreshBackgroundColor(tvT1.backgroundColor!)

        let loadingView1 = DGElasticPullToRefreshLoadingViewCircle()
        loadingView1.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        tvT2.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.getFollowingList()
            })
            }, loadingView: loadingView1)
        tvT2.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        tvT2.dg_setPullToRefreshBackgroundColor(tvT2.backgroundColor!)
    }
    
    //////////////////////////////////////////
    // MARK: - Helper
    //////////////////////////////////////////
    
    func initVC() {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 15.0
        layout.minimumInteritemSpacing = 30.0
        
        // Collection view attributes
        
        tvT1.delegate = self
        tvT1.dataSource = self
        tvT1.register(UINib.init(nibName: "UserFollowTVC", bundle: nil), forCellReuseIdentifier: "UserFollowTVC")
        
        tvT2.delegate = self
        tvT2.dataSource = self
        tvT2.register(UINib.init(nibName: "UserFollowTVC", bundle: nil), forCellReuseIdentifier: "UserFollowTVC")
        
        self.vwTop.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.vwTop.frame.size.height)
        
    }
    
    func pageMoved() {
        
        scrollPos = 0
        
        var clickedBtn : UIButton!
        
        btnMenuT1.isSelected = false
        btnMenuT2.isSelected = false
        
        switch nCurPage {
        case ETab.T1.rawValue :
            btnMenuT1.isSelected = true
            lbComTitle.text = NSLocalizedString("follower", comment:"")
            clickedBtn = btnMenuT1
            getFollowerList()
        case ETab.T2.rawValue :
            btnMenuT2.isSelected = true
            lbComTitle.text = NSLocalizedString("following", comment:"")
            clickedBtn = btnMenuT2
            getFollowingList()
        default:
            btnMenuT1.isSelected = true
            clickedBtn = btnMenuT1
        }
        
        vwMenuBar.removeConstraint(vwMenuHoverBarLeading)
        vwMenuBar.removeConstraint(vwMenuHoverBarTrailing)
        
        UIView.beginAnimations("menuHoverAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        vwMenuHoverBarLeading = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .leading, relatedBy: .equal, toItem: clickedBtn, attribute: .leading, multiplier: 1.0, constant: -7.0)
        vwMenuHoverBarTrailing = NSLayoutConstraint(item: vwMenuHoverBar, attribute: .trailing, relatedBy: .equal, toItem: clickedBtn, attribute: .trailing, multiplier: 1.0, constant: 7.0)
        
        vwMenuBar.addConstraint(vwMenuHoverBarLeading)
        vwMenuBar.addConstraint(vwMenuHoverBarTrailing)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.commitAnimations()
    }
    
    //////////////////////////////////////////
    // MARK: - Action
    //////////////////////////////////////////
    
    @objc func onBtnGo2User(_ sender: UIButton) {
        let dic : BlockUsrDto = arrFollowList[sender.tag]
        let vc = (UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "USER_VIEW")) as! UserViewController
        vc.usrUid = dic.usrUid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onBtnFollow(_ sender: UIButton) {
        
        let dic : BlockUsrDto = arrFollowList[sender.tag]
        if !dic.usrLikeStatus {
            
            let acAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            acAlert.title = "[" + dic.usrNckNm + "] " + NSLocalizedString("follow_cancel_alert", comment:"")
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("follow_cancel", comment:""), style: .default, handler: { (action: UIAlertAction!) in
//                sender.isSelected = true
                self.reqFollow(_uid: dic.usrUid)
            }))
            acAlert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment:""), style: .cancel, handler: nil))
            present(acAlert, animated: true, completion: nil)
        } else {
//            sender.isSelected = false
            delFollow(_uid: dic.usrUid)
        }
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        popVC()
    }
    
    @IBAction func tabAction(_ sender: Any) {
        
        let clickedBtn = sender as! UIButton
        
        UIView.beginAnimations("pageChangeAnim", context: nil)
        UIView.setAnimationDuration(TabAnimationTime)
        UIView.setAnimationDelegate(self)
        
        switch clickedBtn.tag {
        case ETab.T1.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 0
        case ETab.T2.rawValue : scvView.contentOffset.x = scvView.frame.size.width * 1
        default:
            scvView.contentOffset.x = scvView.frame.size.width * 0
        }
        
        UIView.commitAnimations()
        
        nCurPage = clickedBtn.tag
        pageMoved()
    }
    
    @IBAction func scroll2TopAction(_ sender: Any) {
        if btnMenuT1.isSelected {
            tvT1.scrollToTop(isAnimated: true)
        } else if btnMenuT2.isSelected {
            tvT2.scrollToTop(isAnimated: true)
        }
    }
    
    // UITableViewDataSource protocol functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFollowList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserFollowTVC", for: indexPath as IndexPath) as! UserFollowTVC
        
        let dic : BlockUsrDto = arrFollowList[indexPath.row]
        cell.lblId.text = dic.usrId
        cell.btnNickname.setTitle(dic.usrNckNm, for: .normal)
        cell.ivPhoto.kf.setImage(with: URL(string: dic.profileImg), placeholder: UIImage(named: "ic_user_default"), options: [], progressBlock: nil, completionHandler: nil)
        
        if dic.usrLikeStatus {
            cell.btnFollow.isSelected = false
        } else {
            cell.btnFollow.isSelected = true
        }
        
        //본인이 리스트에 있는 경우 버튼 제거
        if dic.usrUid == usrUid {
            cell.btnFollow.isHidden = true
        } else {
            cell.btnFollow.isHidden = false
        }
        
        cell.btnPhoto.tag = indexPath.row
        cell.btnPhoto.addTarget(self, action:#selector(self.onBtnGo2User(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNickname.tag = indexPath.row
        cell.btnNickname.addTarget(self, action:#selector(self.onBtnGo2User(_:)), for: UIControlEvents.touchUpInside)
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.addTarget(self, action:#selector(self.onBtnFollow(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    // UITableViewDelegate protoal functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    // 팔로워 목록 얻기
    func getFollowerList() {
        
        gProgress.show()
        Net.getFollowerList(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self.tvT1.dg_stopLoading()
                let res = result as! Net.BlockUsrResult
                self.getFollowListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 팔로잉 목록 얻기
    func getFollowingList() {
        
        gProgress.show()
        Net.getFollowingList(
            accessToken     : gMeInfo.token,
            usrUid          : usrUid,
            page            : nPage,
            success: { (result) -> Void in
                gProgress.hide()
                self.tvT2.dg_stopLoading()
                let res = result as! Net.BlockUsrResult
                self.getFollowListResult(res)
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 하기
    func reqFollow(_uid : Int) {
        
        gProgress.show()
        Net.reqFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.nPage = 0
                if self.nCurPage == ETab.T1.rawValue {
                    self.getFollowerList()
                } else {
                    self.getFollowingList()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // 유저 팔로우 해제하기
    func delFollow(_uid : Int) {
        
        gProgress.show()
        Net.delFollow(
            accessToken     : gMeInfo.token,
            usrUid          : _uid,
            success: { (result) -> Void in
                gProgress.hide()
                
                self.nPage = 0
                if self.nCurPage == ETab.T1.rawValue {
                    self.getFollowerList()
                } else {
                    self.getFollowingList()
                }
                
        }, failure: { (code, err) -> Void in
            gProgress.hide()
            
            if code == USER_ERROR_ACCESS_TOKEN || code == USER_NOT_EXISTS {  //token error
                CommonUtil .showToast(err)
                self.pushVC("LAUNCH_VIEW", storyboard: "Login", animated: true)
            } else {
                CommonUtil .showToast(err)
            }
        })
    }
    
    // Follow 목록 얻기 결과
    func getFollowListResult(_ data: Net.BlockUsrResult) {
        
        isLast = data.last
        
        if nPage == 0 {
            arrFollowList.removeAll()
        }
        
        for i in 0..<data.list.count {
            let dic : BlockUsrDto = data.list[i]
            arrFollowList.append(dic)
        }
        
        lblFollowCnt.text = String.init(format: "(%d)", arrFollowList.count)
        
        if nCurPage == ETab.T1.rawValue {
            tvT1.reloadData()
        } else {
            tvT2.reloadData()
        }
        
    }
}

//////////////////////////////////////////
// MARK: - Delegate & DataSource
//////////////////////////////////////////

extension UserFollowViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        switch scrollView {
            
        case scvView:
            
            let newOffset = scrollView.contentOffset.x
            nCurPage = ((newOffset / (scrollView.frame.size.width)) as NSNumber).intValue % 3
            
            vwTopTop.constant = 20
            
            self.pageMoved()
            
        case tvT1, tvT2:

            self.scrollDirection = EScrollDirection.none

            //                let curScrollPos = scrollView.contentOffset.y
            //                if curScrollPos == 0 {
            //                    // scroll Down
            //                    self.scrollUp()
            //                } else if curScrollPos >= abs(scrollView.contentSize.height - scrollView.bounds.size.height)  {
            //                    // scroll Up
            //                    self.scrollDown()
            //                }
            //
            //                scrollPos = curScrollPos

            //                return

        default:
            
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case tvT1, tvT2:

            let curScrollPos = scrollView.contentOffset.y
            if curScrollPos >= scrollPos {
                // scroll Down
                self.scrollDirection = EScrollDirection.down
            } else {
                // scroll Up
                self.scrollDirection = EScrollDirection.up
            }

            scrollPos = curScrollPos

        default:
            return
        }
    }
}
