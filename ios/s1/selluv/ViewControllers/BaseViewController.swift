//
//  BaseViewController.swift
//  selluv
//
//  Created by Dev on 12/4/17.
//

import UIKit
import Toast_Swift

class BaseViewController: UIViewController {

    var dic: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showHeart(_ containerView : UIViewController, _ type : Int = 0) {
        
        let image = UIImageView(image: UIImage(named: type == 0 ? "ic_like_heart" : "ic_like_heart_white"))
        image.layer.frame = CGRect(origin: CGPoint(x: containerView.view.frame.size.width / 2 - (image.image?.size.width)! / 2, y: containerView.view.frame.size.height / 2 - (image.image?.size.width)! / 2), size: (image.image?.size)!)
        
        image.layer.setAffineTransform(CGAffineTransform(scaleX: 0.1, y: 0.1))
        image.alpha = 0
        
        view.addSubview(image)
        
        UIView.animateAndChain(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            image.layer.setAffineTransform(CGAffineTransform(scaleX: 1.0, y: 1.0))
            image.alpha = 1
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            image.layer.setAffineTransform(CGAffineTransform(scaleX: 0.1, y: 0.1))
            image.alpha = 0
        }) { (finish) in
            image.removeFromSuperview()
            
        }
        
    }
    
    func showHeart(_ containerView : UIViewController, _ type : Int = 0, _ cell : MainItemCVC, _ scrollPos : CGFloat, _ shouldAddClvLeading : Bool = true) {
        
        let clvLeading = shouldAddClvLeading ? 15 : 0
        let image = UIImageView(image: UIImage(named: type == 0 ? "ic_like_heart_white" : "ic_like_heart"))
        image.frame = CGRect(x: cell._imgHeart.frame.origin.x + cell._vwLike.frame.origin.x + cell.frame.origin.x + CGFloat(clvLeading), y: cell._imgHeart.frame.origin.y + cell._vwLike.frame.origin.y + cell.frame.origin.y - scrollPos, width: cell._imgHeart.frame.size.width, height: cell._imgHeart.frame.size.height)
        
        view.addSubview(image)
        
        UIView.animateAndChain(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {
            image.frame = CGRect(origin: CGPoint(x: containerView.view.frame.size.width / 2 - (image.image?.size.width)! / 2, y: containerView.view.frame.size.height / 2 - (image.image?.size.width)! / 2), size: (image.image?.size)!)
            image.alpha = 1
        }, completion: nil).animateAndChain(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            image.layer.setAffineTransform(CGAffineTransform(scaleX: 0.1, y: 0.1))
            image.alpha = 0
        }) { (finish) in
            image.removeFromSuperview()
        }
        
    }

    func replaceVC(_ identifier : String, storyboard : String, animated : Bool) {
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: storyboard, bundle: nil)
        
        let vc = (storyboard.instantiateViewController(withIdentifier: identifier))
        
        nav.setViewControllers([vc], animated: animated)
    }
    
    func pushVC(_ identifier : String, storyboard : String, animated : Bool) {
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: storyboard, bundle: nil)
        
        let vc = (storyboard.instantiateViewController(withIdentifier: identifier))
        
        nav.pushViewController(vc, animated: animated)
    }
    
    func pushVC(_ identifier : String, storyboard : String, animated : Bool, dic : [String : Any]) {
        let nav : UINavigationController! = self.navigationController
        let storyboard : UIStoryboard! = UIStoryboard.init(name: storyboard, bundle: nil)
        
        let vc = (storyboard.instantiateViewController(withIdentifier: identifier)) as! BaseViewController
        
        vc.dic = dic
        nav.pushViewController(vc, animated: animated)
    }
    
    func popVC(_ backStep : Int32 = -1, animated : Bool = true) {
        let nav : UINavigationController! = self.navigationController
        
        var viewVCs : [UIViewController] = nav.viewControllers
        for _ in 1...(0 - backStep) {
            viewVCs.removeLast()
        }
        
        nav.setViewControllers(viewVCs, animated: animated)
    }
    
    func presentVC(_ identifier : String, storyboard : String, animated : Bool) {
        let storyboard : UIStoryboard! = UIStoryboard.init(name: storyboard, bundle: nil)
        
        let vc = (storyboard.instantiateViewController(withIdentifier: identifier))
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: animated, completion: nil)
    }
    
    func replaceTab(_ tag : Int) {
        switch tag {
        case 4 : replaceVC("MAIN_VIEW", storyboard: "Main", animated: false)
        case 5 : replaceVC("SHOP_VIEW", storyboard: "Shop", animated: false)
        case 6 : pushVC("SELL_MAIN_VIEW", storyboard: "Sell", animated: false)
        case 7 : replaceVC("BRAND_VIEW", storyboard: "Brand", animated: false)
        case 8 : replaceVC("MYPAGE_VIEW", storyboard: "Mypage", animated: false)
        default:
            break
        }
    }
    
    func goDetailPage() {
        let _type = gPushType
        let _targetUid : Int = Int(gPushUid)!
        gPushUid = ""
        gPushType = ""
        
        switch _type {
        case EPushType.PUSH01_S_PAY_COMPLETED.rawValue:
            
            break
        case EPushType.PUSH02_B_DELIVERY_SENT.rawValue:
            
            break
        case EPushType.PUSH03_D_DEAL_COMPLETED.rawValue:
            
            break
        case EPushType.PUSH04_B_DEAL_CANCELED.rawValue:
            goOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH05_S_DEAL_CANCELED.rawValue:
            goSellOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH06_B_PDT_VERIFICATION.rawValue:
            goOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH07_S_NEGO_SUGGEST.rawValue:
            goSellOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH08_B_NEGO_UPDATED.rawValue:
            goOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH09_S_REFUND_REQUEST.rawValue:
            goSellOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH10_B_REFUND_UPDATED.rawValue:
            goOrderDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH11_REWARD_INVITE.rawValue:
            pushVC("MYPAGE_SELLUVMONEY_VIEW", storyboard: "Mypage", animated: true)
            break
        case EPushType.PUSH12_REWARD_REPORT.rawValue:
            pushVC("MYPAGE_SELLUVMONEY_VIEW", storyboard: "Mypage", animated: true)
            break
        case EPushType.PUSH13.rawValue:
            break
        case EPushType.PUSH14_FOLLOW_ME.rawValue:
            goFollowListPage(_uid: gMeInfo.usrUid, ncknm: gMeInfo.usrNckNm)
            break
        case EPushType.PUSH15_LIKE_MY_PDT.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH16_ITEM_MY_PDT.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH17_LIKE_PDT_SALE.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH18_FOLLOWING_USER_PDT.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH19_FOLLOWING_USER_STYLE.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH20_FOLLOWING_BRAND_PDT.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH21_FINDING_PDT.rawValue:
            goProductDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH22_EVENT.rawValue:
            goEventDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH23_NOTICE.rawValue:
            goNoticeDetailPage(_uid: _targetUid)
            break
        case EPushType.PUSH24_PDT_REPLY.rawValue:
            goReplyPage(_uid: _targetUid)
            break
        case EPushType.PUSH25_REPLY_REPLY.rawValue:
            goReplyPage(_uid: _targetUid)
            break
        case EPushType.PUSH26_MENTIONED_ME.rawValue:
            goReplyPage(_uid: _targetUid)
            break
        default:
            break
        }
    }
    
    func goOrderDetailPage(_uid : Int){
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goSellOrderDetailPage(_uid : Int) {
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_SELLORDERDETAIL_VIEW")) as! SellOrderDetailViewController
        vc.goPage = DealPage.SELL.rawValue
        vc.dealUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goFollowListPage(_uid : Int, ncknm : String) {
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "User", bundle: nil)
        let vc : UserFollowViewController = (storyboard.instantiateViewController(withIdentifier: "USER_FOLLOW_VIEW") as! UserFollowViewController)
        vc.following = false
        vc.usrUid = _uid
        vc.topName = ncknm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goProductDetailPage(_uid : Int) {
        let vc = (UIStoryboard.init(name: "Mypage", bundle: nil).instantiateViewController(withIdentifier: "MYPAGE_ORDERDETAIL_VIEW")) as! OrderDetailViewController
        vc.dealUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goReplyPage(_uid : Int){
        let vc = (UIStoryboard.init(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DETAIL_COMMENT_VIEW")) as! DetailCommentViewController
        vc.pdtUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    func goNoticeDetailPage(_uid : Int){
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc : SelluvNewsDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_SELLUVNEWSDETAIL_VIEW") as! SelluvNewsDetailViewController)
        vc.noticeUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goEventDetailPage(_uid : Int){
        let storyboard : UIStoryboard! = UIStoryboard.init(name: "Mypage", bundle: nil)
        let vc : EventDetailViewController = (storyboard.instantiateViewController(withIdentifier: "MYPAGE_EVENTDETAIL_VIEW") as! EventDetailViewController)
        vc.eventUid = _uid
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
