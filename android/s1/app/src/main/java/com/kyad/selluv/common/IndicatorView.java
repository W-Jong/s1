/**
 * 홈
 */
package com.kyad.selluv.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kyad.selluv.R;

import java.util.ArrayList;

public class IndicatorView extends LinearLayout {

    int count = 0;
    int position = 0;
    int org_pos = 0;

    int clrRef = R.drawable.ic_indicator;

    ArrayList<ImageView> dots = new ArrayList<>();

    public IndicatorView(Context context) {
        super(context);
    }

    public IndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public IndicatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.IndicatorView);
        clrRef = typedArray.getResourceId(R.styleable.IndicatorView_clrRef, clrRef);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void setCount(int count) {
        this.removeAllViews();
        dots.clear();
        position = org_pos = 0;
        this.count = count;
        for (int i = 0; i < count; i++) {
            ImageView iv = new ImageView(getContext());
            iv.setBackgroundResource(clrRef);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(Utils.dpToPixel(getContext(), 7), Utils.dpToPixel(getContext(), 7));
            if (i > 0) {
                lp.setMarginStart(Utils.dpToPixel(getContext(), Utils.dpToPixel(getContext(), 5)));
            }
            iv.setLayoutParams(lp);
            if (i == 0) {
                iv.setActivated(true);
            }
            dots.add(iv);
            this.addView(iv);
        }
    }


    public void setCount(int count, int pos) {
        this.removeAllViews();
        dots.clear();
        position = org_pos = pos;
        this.count = count;
        for (int i = 0; i < count; i++) {
            ImageView iv = new ImageView(getContext());
            iv.setBackgroundResource(clrRef);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(Utils.dpToPixel(getContext(), 7), Utils.dpToPixel(getContext(), 7));
            if (i > 0) {
                lp.setMarginStart(Utils.dpToPixel(getContext(), Utils.dpToPixel(getContext(), 5)));
            }
            iv.setLayoutParams(lp);
            if (i == pos) {
                iv.setActivated(true);
            }
            dots.add(iv);
            this.addView(iv);
        }
    }

    public void setPager(ViewPager pager) {
        if (pager == null)
            return;

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                dots.get(org_pos).setActivated(false);
                dots.get(position).setActivated(true);
                org_pos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
