package com.kyad.selluv.fragment.tips;


import android.view.View;

public interface TipsHelper {

    void showEmpty(String title, String info, int imgID, String buttonTitle, View.OnClickListener listener);

    void hideEmpty();

    void showLoading(boolean firstPage);

    void hideLoading();

    void showError(boolean firstPage, Throwable error);

    void hideError();

    void showHasMore();

    void hideHasMore();
}