package com.kyad.selluv.sell;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.category.SizeRefDto;
import com.kyad.selluv.api.request.PdtCreateReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.sell.fragment.SellAccSizeFragment;
import com.kyad.selluv.sell.fragment.SellBagSizeFragment;
import com.kyad.selluv.sell.fragment.SellConditionFragment;
import com.kyad.selluv.sell.fragment.SellDirectPickerFragment;
import com.kyad.selluv.sell.fragment.SellTagFragment;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class SellInfoActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.frag_size)
    RelativeLayout frag_size;
    @BindView(R.id.frag_condition)
    RelativeLayout frag_condition;
    @BindView(R.id.frag_tag)
    RelativeLayout frag_tag;
    @BindView(R.id.tv_item_size_value)
    TextView tv_item_size_value;
    @BindView(R.id.tv_condition_value)
    TextView tv_condition_value;
    @BindView(R.id.tv_tagadd_value)
    TextView tv_tagadd_value;
    @BindView(R.id.tv_next)
    TextView tv_next;
    @BindView(R.id.btn_more)
    Button bnMore;

    /**
     * 가방사이즈 입력
     */
    SellBagSizeFragment bagSizeFragment = new SellBagSizeFragment();
    /**
     * 기타 입력
     */
    SellAccSizeFragment accSizeFragment = new SellAccSizeFragment();
    /**
     * 의류/신발 사이즈 입력
     */
    SellDirectPickerFragment pickerFragment = new SellDirectPickerFragment();
    /**
     * 컨디션 입력
     */
    SellConditionFragment conditionFragment = new SellConditionFragment();
    /**
     * 태그 추가
     */
    SellTagFragment tagFragment = new SellTagFragment();

    public SizeRefDto sizeRefDto;
    private String thumbFileName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_info_input);
        addForSellActivity();

        thumbFileName = getIntent().getStringExtra(Constants.THUMB_IMAGE_FILE);

        loadLayout();
        loadSizeInf();
        storePdtCreateReq();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();
    }

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        finish();
    }

    @OnClick(R.id.btn_more)
    void onMore(View v) {
        startActivity(SellInfoAddActivity.class, false, 0, 0);
    }

    @OnClick(R.id.tv_next)
    void onNext(View v) {
        Intent i = new Intent(this, SellPriceActivity.class);
        i.putExtra(Constants.THUMB_IMAGE_FILE, thumbFileName);
        startActivity(i);
    }

    @OnClick(R.id.rl_bag_size)
    void onSize(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        bnMore.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.rl_condition)
    void onCondition(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        bnMore.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.rl_tag_add)
    void onTag(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        bnMore.setVisibility(View.INVISIBLE);
    }

    private void loadLayout() {

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkNextable();
            }
        };
        tv_item_size_value.addTextChangedListener(watcher);
        tv_tagadd_value.addTextChangedListener(watcher);
        tv_condition_value.addTextChangedListener(watcher);
    }

    //상품등록 정보
    private void storePdtCreateReq() {
        String strUser = "";
        if (Globals.pdtCreateReq == null) {
            return;
        }

        SharedPreferences pref = getSharedPreferences("createpdt",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        strUser = new Gson().toJson(Globals.pdtCreateReq, PdtCreateReq.class);
        editor.putString("PdtCreateReq", strUser).apply();
    }

    private void checkNextable() {
        if (tv_item_size_value.length() != 0 && tv_condition_value.length() != 0) {
            tv_next.setTextColor(getResources().getColor(R.color.color_42c2fe));
            tv_next.setEnabled(true);
        } else {
            tv_next.setTextColor(getResources().getColor(R.color.color_999999));
            tv_next.setEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (frag_size.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
            bnMore.setVisibility(View.VISIBLE);
        } else if (frag_condition.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
            bnMore.setVisibility(View.VISIBLE);
        } else if (frag_tag.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
            bnMore.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    private void loadSizeInf() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<SizeRefDto>> genRes = restAPI.getCategorySize(Globals.userToken, Globals.pdtCreateReq.getCategoryUid());
        genRes.enqueue(new TokenCallback<SizeRefDto>(this) {
            @Override
            public void onSuccess(SizeRefDto response) {
                closeLoading();

                sizeRefDto = response;

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();


                if (sizeRefDto.getSizeType() == 1) {
                    transaction.replace(R.id.frag_size, bagSizeFragment);
                } else if (sizeRefDto.getSizeType() == 2) {
                    accSizeFragment.resetPage();
                    transaction.replace(R.id.frag_size, accSizeFragment);
                } else {
                    transaction.replace(R.id.frag_size, pickerFragment);
                }
                transaction.replace(R.id.frag_condition, conditionFragment);
                transaction.replace(R.id.frag_tag, tagFragment);
                transaction.commit();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}

