package com.kyad.selluv.fragment.tips;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;

public class DefaultTipsHelper implements TipsHelper {

    protected final WareListFragment mFragment;
    protected final RecyclerView mRecyclerView;
    protected final RecyclerRefreshLayout mRefreshLayout;
    protected final ImageView mLoadingView;
    protected View mEmptyView;

    public DefaultTipsHelper(WareListFragment fragment) {
        mFragment = fragment;
        mRecyclerView = fragment.getRecyclerView();
        mRefreshLayout = fragment.getRecyclerRefreshLayout();

        mLoadingView = new ImageView(fragment.getActivity());
        mLoadingView.setLayoutParams(new RecyclerView.LayoutParams(
                RecyclerRefreshLayout.LayoutParams.MATCH_PARENT,
                (int) Utils.dpToPixel(fragment.getActivity(), 40)));
        mLoadingView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        mLoadingView.setImageResource(R.drawable.spinner);
        mLoadingView.setPadding(0, (int) Utils.dpToPixel(mFragment.getActivity(), 10),
                0, (int) Utils.dpToPixel(mFragment.getActivity(), 10));

        mEmptyView = LayoutInflater.from(mRecyclerView.getContext()).inflate(R.layout.tips_empty, null);
    }

    @Override
    public void showEmpty(String title, String info, int imgID, String buttonTitle, View.OnClickListener listener) {
        hideLoading();

        if (!mFragment.getHeaderAdapter().containsHeaderView(mEmptyView)) {
            mFragment.getHeaderAdapter().addHeaderView(mEmptyView);
        }

        ImageView iv_none = (ImageView) mEmptyView.findViewById(R.id.iv_none);
        TextView tv_none = (TextView) mEmptyView.findViewById(R.id.tv_none);
        TextView tv_none_info = (TextView) mEmptyView.findViewById(R.id.tv_none_info);
        Button btn_none = (Button) mEmptyView.findViewById(R.id.btn_none);

        if (imgID == 0) iv_none.setVisibility(View.INVISIBLE);
        else iv_none.setImageResource(imgID);
        if (title.isEmpty()) tv_none.setVisibility(View.INVISIBLE);
        else tv_none.setText(title);
        if (info.isEmpty()) tv_none_info.setVisibility(View.INVISIBLE);
        else tv_none_info.setText(info);
        if (buttonTitle == null || buttonTitle.isEmpty()) btn_none.setVisibility(View.INVISIBLE);
        else btn_none.setText(buttonTitle);
        if (listener == null) btn_none.setVisibility(View.INVISIBLE);
        else btn_none.setOnClickListener(listener);
    }

    @Override
    public void hideEmpty() {
        if (mFragment.getHeaderAdapter().containsHeaderView(mEmptyView)) {
            mFragment.getHeaderAdapter().removeHeaderView(mEmptyView);
        }
    }

    @Override
    public void showLoading(boolean firstPage) {
        hideEmpty();
        hideError();
//        if (firstPage) {
//            View tipsView = TipsUtils.showTips(mRecyclerView, TipsType.LOADING);
//            AnimationDrawable drawable = (AnimationDrawable) ((ImageView) tipsView).getDrawable();
//            drawable.start();
//            return;
//        }
    }

    @Override
    public void hideLoading() {
        // TipsUtils.hideTips(mRecyclerView, TipsType.LOADING);
    }

    @Override
    public void showError(boolean firstPage, Throwable error) {
        String errorMessage = error.getMessage();
        if (firstPage) {
            View tipsView = TipsUtils.showTips(mRecyclerView, TipsType.LOADING_FAILED);
            tipsView.findViewById(R.id.btn_none).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFragment.refresh();
                }
            });
            if (!TextUtils.isEmpty(errorMessage)) {
                ((TextView) tipsView.findViewById(R.id.tv_none)).setText(errorMessage);
            }
            return;
        }

        Toast.makeText(mLoadingView.getContext(), errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideError() {
        TipsUtils.hideTips(mRecyclerView, TipsType.LOADING_FAILED);
    }

    @Override
    public void showHasMore() {
//        if (!mFragment.getHeaderAdapter().containsFooterView(mLoadingView)) {
//            ((AnimationDrawable) mLoadingView.getDrawable()).start();
//            mFragment.getHeaderAdapter().addFooterView(mLoadingView);
//        }
    }

    @Override
    public void hideHasMore() {
//        if (mFragment.getHeaderAdapter().containsFooterView(mLoadingView)) {
//            mFragment.getHeaderAdapter().removeFooterView(mLoadingView);
//        }
    }
}
