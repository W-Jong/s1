package com.kyad.selluv.api.dao;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Objects;

public class UsrCardDao {
    private int usrCardUid;
//    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
//    @ApiModelProperty("회원UID")
    private int usrUid;
//    @ApiModelProperty("카드종류 1-개인 2-법인")
    private int kind;
//    @ApiModelProperty("카드번호")
    private String cardNumber;
//    @ApiModelProperty("유효기간")
//    @JsonSerialize(using = LocalDateSerializer.class)
//    @JsonDeserialize(using = LocalDateDeserializer.class)
    private String validDate;
//    @ApiModelProperty("유효번호 개인인 경우 생년월일(YYMMDD), 법인일 경우 사업자번호")
    private String validNum;
//    @ApiModelProperty("비밀번호")
    private String password;
//    @ApiModelProperty("카드이름")
    private String cardName = "";


//    @Column(name = "usr_card_uid", nullable = false)
    public int getUsrCardUid() {
        return usrCardUid;
    }

    public void setUsrCardUid(int usrCardUid) {
        this.usrCardUid = usrCardUid;
    }

//    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

//    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

//    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

//    @Column(name = "card_number", nullable = false, length = 20)
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

//    @Column(name = "valid_date", nullable = false)
    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

//    @Column(name = "valid_num", nullable = false, length = 20)
    public String getValidNum() {
        return validNum;
    }

    public void setValidNum(String validNum) {
        this.validNum = validNum;
    }

//    @Column(name = "password", nullable = false, length = 20)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    @Column(name = "card_name", nullable = false, length = 30)
    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrCardDao that = (UsrCardDao) o;
        return usrCardUid == that.usrCardUid &&
                usrUid == that.usrUid &&
                kind == that.kind &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(cardNumber, that.cardNumber) &&
                Objects.equals(validDate, that.validDate) &&
                Objects.equals(validNum, that.validNum) &&
                Objects.equals(password, that.password) &&
                Objects.equals(cardName, that.cardName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrCardUid, regTime, usrUid, kind, cardNumber, validDate, validNum, password, cardName);
    }
}
