package com.kyad.selluv.mypage;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.DeliveryHistoryDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.DealDeliveryReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class PostFinishActivity extends BaseActivity {

    private int dealUid = 0;
    private int deliveryNumber = 0;

    @BindView(R.id.tv_update)
    EditText tv_update;

    //UI Reference
    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_guide)
    void onGuide() {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.mypage_sales_post_guide);
        startActivity(intent);
    }

    @OnClick(R.id.btn_cancel)
    void onCancel() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.book_cancel))
                .setMessage(getString(R.string.delivery_ccancel_alert))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        delDealDelivery();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_post_ok);
        setStatusBarWhite();

        dealUid = getIntent().getIntExtra(Constants.DEAL_UID, 0);
        deliveryNumber = getIntent().getIntExtra(Constants.DELIVERY_NUMBER, 0);
        loadLayout();
    }

    private void loadLayout() {
        tv_update.setText("" + deliveryNumber);
    }

    //예약 취소
    private void delDealDelivery() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.delDealDelivery(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(PostFinishActivity.this, getString(R.string.delivery_cancel));
                finish();
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }

}
