package com.kyad.selluv.shop;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.theme.ThemeListDto;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.top.TopLikeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class ThemaFragment extends BaseFragment {

    //UI Reference
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.rl_thema_detail)
    RelativeLayout rl_thema_detail;
    @BindView(R.id.tv_thema_eng_title)
    TextView tv_thema_eng_title;
    @BindView(R.id.iv_thema_bg)
    ImageView iv_thema_bg;
    @BindView(R.id.ll_thema_line)
    LinearLayout ll_thema_line;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsing_toolbar;
    @BindView(R.id.ib_right)
    ImageButton ib_right;

    //Variables
    View rootView;
    private MainPageAdapter pagerAdapter;
    private ThemeListDto themeListDto;
    WareListFragment fragment_ware_list;

    Fragment mainFragment;
    public int themeUid;

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
//        getFragmentManager().popBackStack();
        if (mainFragment instanceof ShopMainFragment) {
            ((ShopMainFragment) mainFragment).closePopupFragment();
        } else {
            Toast.makeText(getActivity(), "TODO back feature", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    public static ThemaFragment newInstance(Fragment mainFragment, int themeUid) {
        ThemaFragment fragment = new ThemaFragment();
        fragment.mainFragment = mainFragment;
        fragment.themeUid = themeUid;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_thema, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    private void loadLayout() {

        collapsing_toolbar.setTitle("");
        BarUtils.setStatusBarAlpha(getActivity(), 0, true);
        BarUtils.addMarginTopEqualStatusBarHeight(rootView.findViewById(R.id.tb_actionbar));

        AppBarLayout app_bar_layout = (AppBarLayout) rootView.findViewById(R.id.appbar);
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int h = Utils.dpToPixel(getActivity(), rl_thema_detail.getHeight() - 56 - 20);
                int b = Utils.dpToPixel(getActivity(), 18) * (h + verticalOffset) / h;
                if (b < 0) b = 0;
                final Animation animationFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                final Animation animationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
                if (verticalOffset <= -rl_thema_detail.getHeight() / 2) {
                    if (ll_thema_line.getVisibility() == View.VISIBLE) {
                        ll_thema_line.startAnimation(animationFadeOut);
                        ll_thema_line.setVisibility(View.INVISIBLE);
                        tv_thema_eng_title.startAnimation(animationFadeOut);
                        tv_thema_eng_title.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (ll_thema_line.getVisibility() == View.INVISIBLE) {
                        ll_thema_line.startAnimation(animationFadeIn);
                        ll_thema_line.setVisibility(View.VISIBLE);
                        tv_thema_eng_title.startAnimation(animationFadeIn);
                        tv_thema_eng_title.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        fragment_ware_list = new WareListFragment();
        fragment_ware_list.listInfo = getResources().getString(R.string.thema_ware);
        fragment_ware_list.showFilterButton = false;
        fragment_ware_list.showFilter = true;
        fragment_ware_list.parentCollapsable = true;
        fragment_ware_list.setFragType(Constants.WIRE_FRAG_THEME);
        fragment_ware_list.isMiniImageShow = false;

        pagerAdapter = new MainPageAdapter(getChildFragmentManager());
        vp_main.setAdapter(pagerAdapter);

        getThemeDetail();
        updateLikePdtBadge();
    }

    private void refreshArea() {
        if (themeListDto == null)
            return;

        tv_thema_eng_title.setText(themeListDto.getTheme().getTitleEn());
        collapsing_toolbar.setTitle(themeListDto.getTheme().getTitleKo());
        if ("".equals(themeListDto.getTheme().getProfileImg())) {
            rl_thema_detail.setBackgroundColor(Color.WHITE);
        } else {
            Glide.with(getActivity()).load(themeListDto.getTheme().getProfileImg())
                    .apply(new RequestOptions().placeholder(android.R.color.white).error(android.R.color.black))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                            rl_thema_detail.setBackground(resource);
                            iv_thema_bg.setImageDrawable(resource);
//                            iv_thema_bg.setBackground(resource);
                        }
                    });
        }
        rl_thema_detail.setScaleX(1);
        rl_thema_detail.setScaleY(1);

        fragment_ware_list.refreshExternalData(themeListDto.getPdtList(), themeListDto.getPdtList().size());
    }

//    public Model.WareItem[] loadThemaWare(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_6);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(getActivity()) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void getThemeDetail() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<ThemeListDto>> genRes = restAPI.themeDetail(Globals.userToken, themeUid);

        genRes.enqueue(new TokenCallback<ThemeListDto>(getActivity()) {
            @Override
            public void onSuccess(ThemeListDto response) {
                closeLoading();
                themeListDto = response;

                refreshArea();
            }

            @Override
            public void onFailed(Throwable t) {
                themeListDto = null;
                closeLoading();
                onLeft(null);
            }
        });
    }

    private class MainPageAdapter extends FragmentStatePagerAdapter {

        public MainPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = fragment_ware_list;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 1;
        }
    }
}
