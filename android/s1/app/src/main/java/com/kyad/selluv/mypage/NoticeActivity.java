package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.NoticeListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.NoticeDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.login.BrandSelectActivity;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class NoticeActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lv_news)
    ListView lv_news;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    NoticeListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_news);
        setStatusBarWhite();

        loadLayout();
        getNoticeList();
    }

    private void loadLayout() {
        listAdapter = new NoticeListAdapter(this);
        lv_news.setAdapter(listAdapter);
    }

    //get notice list
    private void getNoticeList(){
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<NoticeDao>>> genRes = restAPI.getNoticeList(Globals.userToken);

        genRes.enqueue(new TokenCallback<List<NoticeDao>>(this) {
            @Override
            public void onSuccess(List<NoticeDao> response) {
                closeLoading();

                listAdapter.arrData = response;
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }
}
