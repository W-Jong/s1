package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.other.ReviewListDto;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

public class MannerListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<ReviewListDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public MannerListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<ReviewListDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_detail_comment, parent, false);
            final ReviewListDto anItem = (ReviewListDto) getItem(position);

            if (anItem == null) {
                return convertView;
            }

            CircularImageView iv_profile = (CircularImageView) convertView.findViewById(R.id.iv_profile);
            CircularImageView iv_condition = (CircularImageView) convertView.findViewById(R.id.iv_condition);
            TextView tv_comment = (TextView) convertView.findViewById(R.id.tv_comment);
            TextView tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            TextView tv_condition = (TextView) convertView.findViewById(R.id.tv_response);

            ImageUtils.load(iv_profile.getContext(), anItem.getPeerUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile);
            tv_time.setText(Utils.getDiffTime1(anItem.getReview().getRegTime().getTime()));
            if (anItem.getReview().getPoint() == 2){
                tv_condition.setText("만족");
                iv_condition.setImageResource(R.drawable.review_good_icon_color);
            } else  if (anItem.getReview().getPoint() == 1){
                tv_condition.setText("보통");
                iv_condition.setImageResource(R.drawable.review_normal_icon_color);

            } else {
                tv_condition.setText("불만족");
                iv_condition.setImageResource(R.drawable.review_bad_icon_color);
            }

            String type = "";
            if (anItem.getReview().getType() == 1) {
                type = "판매자";
            } else {
                type = "구매자";
            }
//            tv_comment.setText(String.format("%s(%s) %s", anItem.getPeerUsr().getUsrNckNm(), type, anItem.getReview().getContent()));

            String str_type = String.format("(%s)", type);
            String comment = String.format("%s%s %s", anItem.getPeerUsr().getUsrNckNm(), str_type, anItem.getReview().getContent());

            SpannableStringBuilder builder = new SpannableStringBuilder(comment);
            builder.setSpan(
                    new StyleSpan(Typeface.BOLD),
                    comment.indexOf(anItem.getPeerUsr().getUsrNckNm()),
                    comment.indexOf(anItem.getPeerUsr().getUsrNckNm()) + anItem.getPeerUsr().getUsrNckNm().length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            builder.setSpan(
                    new ForegroundColorSpan(Color.parseColor("#666666")),
                    comment.indexOf(str_type),
                    comment.indexOf(str_type) + str_type.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            tv_comment.setText(builder);

            iv_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getPeerUsr().getUsrUid());
                    activity.startActivity(intent);
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}