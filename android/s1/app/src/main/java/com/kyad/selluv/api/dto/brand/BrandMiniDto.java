package com.kyad.selluv.api.dto.brand;

import lombok.Data;

@Data
public class BrandMiniDto {
    //("브랜드UID")
    private int brandUid;
    //("영문첫글자")
    private String firstEn;
    //("한글초성")
    private String firstKo;
    //("영문이름")
    private String nameEn;
    //("한글이름")
    private String nameKo;
    //("썸네일이미지")
    private String profileImg;


    public int i_followed;

    public BrandMiniDto(BrandMiniDto another) {
        this.brandUid = another.brandUid;
        this.firstEn = another.firstEn;
        this.firstKo = another.firstKo;
        this.nameEn = another.nameEn;
        this.nameKo = another.nameKo;
        this.profileImg = another.profileImg;
        this.i_followed = another.i_followed;
    }
}
