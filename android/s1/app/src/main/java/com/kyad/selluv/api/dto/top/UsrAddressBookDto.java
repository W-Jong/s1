package com.kyad.selluv.api.dto.top;

import com.kyad.selluv.api.dto.usr.UsrFollowListDto;

import java.util.List;

import lombok.Data;

@Data
public class UsrAddressBookDto {
//    @ApiModelProperty("가입한 지인")
    private List<UsrFollowListDto> joinedList;
//    @ApiModelProperty("가입하지 않은 전화번호 목록")
    private List<String> unJoinedPhoneList;
}
