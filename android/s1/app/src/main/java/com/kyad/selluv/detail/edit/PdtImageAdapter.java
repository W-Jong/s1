package com.kyad.selluv.detail.edit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyad.selluv.R;
import com.kyad.selluv.common.ImageUtils;

import java.util.ArrayList;

public class PdtImageAdapter extends RecyclerView.Adapter<PdtImageViewHolder> {

    private ArrayList<String> list;
    private Context mContext;

    public PdtImageAdapter(Context context, ArrayList<String> Data) {
        mContext = context;
        list = Data;
    }

    @NonNull
    @Override
    public PdtImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pdt_img, parent, false);
        PdtImageViewHolder holder = new PdtImageViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PdtImageViewHolder holder, int position) {
        ImageUtils.load(mContext, list.get(position), R.drawable.img_default_square, R.drawable.img_default_square, holder.shareImageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
