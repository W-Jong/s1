package com.kyad.selluv.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.PdtStyleDto;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.IndicatorView;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class DetailStyleAdapter extends RecyclerView.Adapter {

    public List<PdtStyleDto> dataList = new ArrayList<>();
    private List<String> imageUrlList = new ArrayList<>();
    private BaseActivity activity;

    public DetailStyleAdapter(BaseActivity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.detail_style_list, null);
        StyleViewHolder holder = new StyleViewHolder(itemView);
        holder.tv_username = (TextView) itemView.findViewById(R.id.tv_username);
        holder.ib_close = (ImageButton) itemView.findViewById(R.id.btn_close);
        holder.iv_img = (ImageView) itemView.findViewById(R.id.iv_img);
        holder.iv_photo = (ImageView) itemView.findViewById(R.id.iv_photo);
        itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        StyleViewHolder vh = (StyleViewHolder) viewHolder;
        final PdtStyleDto anItem = dataList.get(position);

        ImageUtils.load(activity, anItem.getPdtStyle().getStyleImg(), android.R.color.transparent, R.drawable.img_default, vh.iv_img);
        ImageUtils.load(activity, anItem.getUsr().getProfileImg(), android.R.color.transparent, R.drawable.default_profile, vh.iv_photo);
        vh.tv_username.setText(anItem.getUsr().getUsrNckNm());
        View.OnClickListener toUser = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, UserActivity.class);
                intent.putExtra(UserActivity.USR_UID, anItem.getUsr().getUsrUid());
                activity.startActivity(intent);
            }
        };
        vh.iv_photo.setOnClickListener(toUser);
        vh.tv_username.setOnClickListener(toUser);

        vh.iv_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailActivity) activity).hideSnackBars();

                final PopupDialog popup = new PopupDialog((BaseActivity) activity, R.layout.detail_slide).setBlur().setFullScreen();

                final ViewPager vp_image = (ViewPager) popup.findView(R.id.vp_image);
                IndicatorView indicator = (IndicatorView) popup.findView(R.id.rl_indicator);
                Utils.addTouchableImagesAndIndicator(vp_image, imageUrlList, indicator);

                popup.setOnCancelClickListener(new PopupDialog.bindOnClick() {
                    @Override
                    public void onClick() {
                        popup.dismiss();
                    }
                });

                popup.show();
            }
        });
        if (Globals.isMyUsrUid(anItem.getUsr().getUsrUid())) {
            vh.ib_close.setVisibility(View.VISIBLE);
            vh.ib_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(activity);
                    }
                    builder.setTitle(activity.getString(R.string.alarm))
                            .setMessage(activity.getString(R.string.style_delete_alert))
                            .setPositiveButton(activity.getString(R.string.delete), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    deletePdtStyle(position);
                                }
                            })
                            .setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
            });
        } else {
            vh.ib_close.setVisibility(View.INVISIBLE);
        }
    }

    public void setData(List<PdtStyleDto> dataList) {
        this.dataList = dataList;
        updateImageUrlList();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void updateImageUrlList() {
        imageUrlList.clear();
        for (PdtStyleDto item : dataList)
            imageUrlList.add(item.getPdtStyle().getStyleImg());
    }

    public class StyleViewHolder extends ViewHolder {
        public TextView tv_username;
        public ImageView iv_img;
        public ImageView iv_photo;
        public ImageButton ib_close;

        public StyleViewHolder(View itemView) {
            super(itemView);
        }
    }

    private void deletePdtStyle(final int position) {
        activity.showLoading();

        PdtStyleDto item = dataList.get(position);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.pdtStyleDelete(Globals.userToken, item.getPdtStyle().getPdtStyleUid());
        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {
                activity.closeLoading();

                dataList.remove(position);
                notifyDataSetChanged();
                updateImageUrlList();
            }

            @Override
            public void onFailed(Throwable t) {
                activity.closeLoading();
            }
        });
    }
}
