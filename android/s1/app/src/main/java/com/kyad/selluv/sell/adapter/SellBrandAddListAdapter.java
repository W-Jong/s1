package com.kyad.selluv.sell.adapter;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.AlphabetIndexer.util.HanziToPingyin;
import com.kyad.selluv.common.AlphabetIndexer.util.StringMatcher;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PinnedSectionListView;
import com.kyad.selluv.model.BRAND_LANG;
import com.kyad.selluv.sell.SellValetRequestActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kyad.selluv.common.AlphabetIndexer.util.StringMatcher.isKorean;
import static com.kyad.selluv.sell.SellMainActivity.sellType;

public class SellBrandAddListAdapter extends BaseAdapter implements SectionIndexer, PinnedSectionListView.PinnedSectionListAdapter {

    class BrandItemComparator implements Comparator<BrandMiniDto> {

        private BRAND_LANG lang;

        public BrandItemComparator(BRAND_LANG lang) {
            this.lang = lang;
        }

        @Override
        public int compare(BrandMiniDto brandItem, BrandMiniDto t1) {
            if (lang == BRAND_LANG.ENGLISH) {
                return brandItem.getNameEn().compareToIgnoreCase(t1.getNameEn());
            } else {
                return brandItem.getNameKo().compareToIgnoreCase(t1.getNameKo());
            }
        }
    }

    BaseActivity activity;
    public List<BrandMiniDto> arrData;
    LayoutInflater inflater;
    BRAND_LANG selectedLang;
    ArrayList<String> names = new ArrayList<>();

    private int TYPE_ITEM = 0;
    private int TYPE_SECTION = 1;

    private final int SECTION_POPULAR_CITY = 0;

    private List<String> mSections = new ArrayList<String>();
    private List<Item> mItems = new ArrayList<Item>();
    private Map<String, Character> mCharMap = new HashMap<String, Character>();
    private boolean isFromFilter = false;

    public SellBrandAddListAdapter(BaseActivity activity, BRAND_LANG selectedLang, boolean isFromFilter) {
        this.activity = activity;
        this.selectedLang = selectedLang;
        this.isFromFilter = isFromFilter;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(List<BrandMiniDto> addData) {
        arrData = new ArrayList<>(addData);
    }

    public void reloadData(BRAND_LANG selectedLang) {
        names.clear();
        mSections.clear();
        mItems.clear();
        if (arrData == null) {
            return;
        }
        this.selectedLang = selectedLang;
        BrandItemComparator comparator = new BrandItemComparator(selectedLang);
        Collections.sort(arrData, comparator);
        for (BrandMiniDto item :
                arrData) {
            if (selectedLang == BRAND_LANG.ENGLISH)
                names.add(item.getNameEn().toLowerCase());
            else
                names.add(item.getNameKo());
        }

        mCharMap.clear();
        mCharMap = buildAlphabetMap(names);

        if (selectedLang == BRAND_LANG.ENGLISH) {
            for (char s :
                    Constants.en.toLowerCase().toCharArray()) {
                mSections.add(String.valueOf(s));
            }
        } else {
            for (char s :
                    Constants.kr.toCharArray()) {
                mSections.add(String.valueOf(s));
            }
        }
        char prevChar = '.';
        int offset = 0;
        int orgOffset = -1;
        for (int i = 0; i < names.size(); i++) {
            String name = names.get(i);
            char fc = lookupFirstChar(name);
            if (isKorean(fc)) {
                for (; offset < Constants.krToCompare.length() - 1; offset++) {
                    char startCh = Constants.krToCompare.charAt(offset);
                    char endCh = Constants.krToCompare.charAt(offset + 1);
                    if (startCh <= fc && fc < endCh) {
                        break;
                    }
                }
                if (offset != orgOffset) {
                    Item item = new Item();
                    item.type = TYPE_SECTION;
                    item.text = String.valueOf(Constants.kr.charAt(offset));
                    mItems.add(item);
                    orgOffset = offset;
                }
            } else {
                if (fc != prevChar) {
                    prevChar = fc;

                    Item item = new Item();
                    item.type = TYPE_SECTION;
                    item.text = String.valueOf(fc);
                    mItems.add(item);
                }
            }

            Item item = new SellBrandAddListAdapter.Item();
            item.type = TYPE_ITEM;
            item.text = name;
            item.brandItem = arrData.get(i);
            mItems.add(item);
        }
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        if (mItems == null) {
            return 0;
        } else {
            return mItems.size();
        }
    }

    @Override
    public int getPositionForSection(int section) {

        if (section == SECTION_POPULAR_CITY) {
            return 0;
        }

        // If there is no item for current section, previous section will be selected
        for (int i = section; i >= 1; i--) {
            for (int j = 0; j < getCount(); j++) {
                if (i == 0) {
                    // For numeric section
                    for (int k = 0; k <= 9; k++) {
                        if (StringMatcher.match(mItems.get(j).text, String.valueOf(k))) {
                            return j - 1;
                        }
                    }
                } else {
                    char fc = lookupFirstChar(((SellBrandAddListAdapter.Item) getItem(j)).text);
                    if (StringMatcher.match(String.valueOf(fc), mSections.get(i))) {
                        return j - 1;
                    }
                }
            }
        }
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSections.size(); i++) {
            char fc = lookupFirstChar(mItems.get(position).text);
            if (mSections.get(i).equals(String.valueOf(fc))) {
                return i;
            }
        }

        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        // View view;

        int type = getItemViewType(position);
        final Item anItem = (Item) getItem(position);

        if (isItemViewTypePinned(type)) {
            view = LayoutInflater.from(activity).inflate(
                    R.layout.item_brand_follow_header, parent, false);

            TextView tv_alphabet = view.findViewById(R.id.tv_alphabet);
            tv_alphabet.setText(anItem.text);

        } else {
            view = LayoutInflater.from(activity).inflate(
                    R.layout.item_brand_follow_add, parent, false);

            final CheckBox cb_select = view.findViewById(R.id.cb_select);

            RelativeLayout rly_background = view.findViewById(R.id.rly_background);
            rly_background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cb_select.setChecked(!cb_select.isChecked());
                }
            });

            TextView tv_brand = view.findViewById(R.id.tv_brand);
            tv_brand.setText(selectedLang == BRAND_LANG.ENGLISH ? anItem.brandItem.getNameEn() : anItem.brandItem.getNameKo());

            TextView tv_brand_other = view.findViewById(R.id.tv_brand_other);
            tv_brand_other.setText(selectedLang == BRAND_LANG.ENGLISH ? anItem.brandItem.getNameKo() : anItem.brandItem.getNameEn());


            cb_select.setChecked(anItem.brandItem.i_followed == 1);
            cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    anItem.brandItem.i_followed = b ? 1 : 0;

                    if (isFromFilter) {
                        cb_select.setChecked(anItem.brandItem.i_followed == 1);
                        List<Integer> brandUidList = Globals.searchReq.getBrandUidList();
                        if (b) {
                            brandUidList.add(Integer.valueOf(anItem.brandItem.getBrandUid()));
                        } else {
                            brandUidList.remove(Integer.valueOf(anItem.brandItem.getBrandUid()));
                        }
                        Globals.brandFilterName = "";
                        for (int i = 0; i < brandUidList.size(); i++) {
                            for (int j = 0; j < arrData.size(); j++) {
                                if (brandUidList.get(i) == arrData.get(j).getBrandUid()) {
                                    if (i > 0)
                                        Globals.brandFilterName += ",";
                                    Globals.brandFilterName += arrData.get(j).getNameEn();
                                }
                            }
                        }
                    } else {
                        for (int nInd = 0; nInd < mItems.size(); nInd ++) {
                            if (mItems.get(nInd).brandItem != null) {
                                mItems.get(nInd).brandItem.i_followed = (nInd == position ? 1 : 0);
                            }
                        }
                        notifyDataSetChanged();

                        Globals.selectedBrand = anItem.brandItem;
                        Globals.pdtCreateReq.setBrandUid(Globals.selectedBrand.getBrandUid());
                        Globals.pdtCreateReq.setBrandEN(anItem.brandItem.getNameEn());
                        Globals.pdtCreateReq.setBrandKO(anItem.brandItem.getNameKo());
                        if (b) {
                            if (sellType == 0) {
                                Intent i = new Intent(activity, SellPicActivity.class);
                                i.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_SELL);
                                activity.startActivity(i);
                            } else {
                                activity.startActivity(SellValetRequestActivity.class, false, 0, 0);
                            }
                        } else {
                            if (mItems.get(position).brandItem != null) {
                                mItems.get(position).brandItem.i_followed = 0;
                                notifyDataSetChanged();
                            }
                        }
                    }
                }
            });

        }

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).type;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean isItemViewTypePinned(int type) {
        return type == TYPE_SECTION;
    }


    @Override
    public Object[] getSections() {
        String[] strs = new String[mSections.size()];
        for (int i = 0; i < mSections.size(); i++) {
            strs[i] = mSections.get(i);
        }
        return strs;
    }

    private char lookupFirstChar(String str) {
        if (mCharMap != null) {
            Character co = mCharMap.get(str);
            if (co == null) {
                return '.';
            } else {
                return co;
            }
        } else {
            return '.';
        }
    }

    private char parseFirstChar(String str) {
        char fc = str.charAt(0);
        if (fc <= 128 && fc >= 0) {
            return fc;
        } else {
            return HanziToPingyin.getFirstPinYinChar(str);
        }
    }

    private Map<String, Character> buildAlphabetMap(List<String> strs) {
        Map<String, Character> map = new HashMap<String, Character>();
        for (String str : strs) {
            map.put(str, parseFirstChar(str));
        }
        return map;
    }

    /**
     * 임시 객체.. 표시를 위한
     */
    public static class Item {
        public int type;
        public String text;
        public BrandMiniDto brandItem;
    }
}
