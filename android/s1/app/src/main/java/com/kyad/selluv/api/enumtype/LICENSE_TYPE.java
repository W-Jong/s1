package com.kyad.selluv.api.enumtype;

public enum LICENSE_TYPE {
    LICENSE(1), PERSONAL_POLICY(2), SELLUV_LICENSE(3), GUARANTEE(4), ESCROW(5), DELIVERY(6), CRADLINCESE(7);

    private int code;

    private LICENSE_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
