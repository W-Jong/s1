package com.kyad.selluv.mypage;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.DealRefundAddressReq;
import com.kyad.selluv.api.request.DealShareReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.camera.SellPicActivity;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class SnsRewardActivity extends BaseActivity {

    private int dealUid = 0;
    //UI Reference
    @BindView(R.id.rl_instagram)
    RelativeLayout rl_instagram;
    @BindView(R.id.rl_blog)
    RelativeLayout rl_blog;
    @BindView(R.id.rl_facebook)
    RelativeLayout rl_facebook;
    @BindView(R.id.rl_twitter)
    RelativeLayout rl_twitter;
    @BindView(R.id.rl_kakao)
    RelativeLayout rl_kakao;
    @BindView(R.id.ll_instagram)
    LinearLayout ll_instagram;
    @BindView(R.id.ll_blog)
    LinearLayout ll_blog;
    @BindView(R.id.ll_facebook)
    LinearLayout ll_facebook;
    @BindView(R.id.ll_twitter)
    LinearLayout ll_twitter;
    @BindView(R.id.ll_kakao)
    LinearLayout ll_kakao;
    @BindView(R.id.scv_img_instagram)
    HorizontalScrollView scv_img_instagram;
    @BindView(R.id.scv_img_blog)
    HorizontalScrollView scv_img_blog;
    @BindView(R.id.scv_img_facebook)
    HorizontalScrollView scv_img_facebook;
    @BindView(R.id.scv_img_twitter)
    HorizontalScrollView scv_img_twitter;
    @BindView(R.id.scv_img_kakao)
    HorizontalScrollView scv_img_kakao;
    @BindView(R.id.btn_request)
    Button btn_request;

    //Vars
    ArrayList<ArrayList<String>> imgs = new ArrayList<>();
    ArrayList<ArrayList<String>> imgNamess = new ArrayList<>();

    public static final int INSTAGRAM_IMAGES = 0x01;
    public static final int NAVOR_IMAGES = 0x02;
    public static final int FACEBOOK_IMAGES = 0x03;
    public static final int TWITTER_IMAGES = 0x04;
    public static final int KAKAO_IMAGES = 0x05;

    @OnClick(R.id.rl_instagram)
    void onInstagram() {
        goToCameraActivity(INSTAGRAM_IMAGES);
    }


    @OnClick(R.id.rl_blog)
    void onBlog() {
        goToCameraActivity(NAVOR_IMAGES);
    }

    @OnClick(R.id.rl_facebook)
    void onFacebook() {
        goToCameraActivity(FACEBOOK_IMAGES);
    }

    @OnClick(R.id.rl_twitter)
    void onTwitter() {
        goToCameraActivity(TWITTER_IMAGES);
    }

    @OnClick(R.id.rl_kakao)
    void onKakao() {
        goToCameraActivity(KAKAO_IMAGES);
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_request)
    void onRequest() {
        reqDealShare();
    }

    //Variables
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_snsreward);
        setStatusBarWhite();
        loadLayout();
        dealUid = getIntent().getIntExtra(Constants.DEAL_UID, 0);
        inflater = LayoutInflater.from(this);
    }

    private void loadLayout() {

        imgs.add(new ArrayList<String>());
        imgs.add(new ArrayList<String>());
        imgs.add(new ArrayList<String>());
        imgs.add(new ArrayList<String>());
        imgs.add(new ArrayList<String>());

        imgNamess.add(new ArrayList<String>());
        imgNamess.add(new ArrayList<String>());
        imgNamess.add(new ArrayList<String>());
        imgNamess.add(new ArrayList<String>());
        imgNamess.add(new ArrayList<String>());
    }

    private void goToCameraActivity(int request_code) {
        Intent intent = new Intent(this, SellPicActivity.class);
        intent.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_MYPAGE);
        startActivityForResult(intent, request_code);
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        RelativeLayout rl_input = rl_instagram;
        HorizontalScrollView scrollView = scv_img_instagram;
        LinearLayout ll_scroll = ll_instagram;
        int index = 0;

        if (requestCode == NAVOR_IMAGES) {
            rl_input = rl_blog;
            scrollView = scv_img_blog;
            index = 1;
            ll_scroll = ll_blog;
        }
        if (requestCode == FACEBOOK_IMAGES) {
            rl_input = rl_facebook;
            scrollView = scv_img_facebook;
            index = 2;
            ll_scroll = ll_facebook;
        }
        if (requestCode == TWITTER_IMAGES) {
            rl_input = rl_twitter;
            scrollView = scv_img_twitter;
            index = 3;
            ll_scroll = ll_twitter;
        }
        if (requestCode == KAKAO_IMAGES) {
            rl_input = rl_kakao;
            scrollView = scv_img_kakao;
            index = 4;
            ll_scroll = ll_kakao;
        }
        if (data == null) return;
        imgs.get(index).clear();
        imgNamess.get(index).clear();
        ll_scroll.removeAllViews();
        Collections.addAll(imgs.get(index), data.getStringArrayExtra(Constants.IMAGES));
        Collections.addAll(imgNamess.get(index), data.getStringArrayExtra(Constants.IMAGES_NAMES));

        if (imgs.size() > 10) {
            Utils.showToast(this, "최대 10장의 사진을 선택할수 있습니다.");
            return;
        }

        rl_input.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        for (int i = 0; i < imgs.get(index).size(); i++) {
            final View imgView = inflater.inflate(R.layout.item_img_upload, null);
            final int finalIndex = index;
            final String finalObj = imgs.get(finalIndex).get(i);
            final LinearLayout finalLl_scroll = ll_scroll;
            final RelativeLayout finalRl_input = rl_input;
            final HorizontalScrollView finalScrollView = scrollView;
            (imgView.findViewById(R.id.ib_del)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalLl_scroll.removeView(imgView);
                    imgs.get(finalIndex).remove(finalObj);
                    if (finalLl_scroll.getChildCount() == 0) {
                        finalRl_input.setVisibility(View.VISIBLE);
                        finalScrollView.setVisibility(View.GONE);
                    }
                    isRegisterButtonStatus();
                }
            });
            ((ImageView) imgView.findViewById(R.id.img)).setImageBitmap(
                    ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(imgs.get(index).get(i)),
                            Utils.dpToPixel(this, 102f), Utils.dpToPixel(this, 102f)));
            final int finalIndex1 = index;
            imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SnsRewardActivity.this, SellPicActivity.class);
                    intent.putExtra(Constants.IMAGES, imgs.get(finalIndex1).toArray(new String[]{}));
                    intent.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_MYPAGE);
                    startActivityForResult(intent, requestCode);
                }
            });
            ll_scroll.addView(imgView);
        }
        isRegisterButtonStatus();
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void isRegisterButtonStatus(){
        if (imgs.get(0).size() > 0 || imgs.get(1).size() > 0 || imgs.get(2).size() > 0 || imgs.get(3).size() > 0 || imgs.get(4).size() > 0)
            btn_request.setEnabled(true);
        else
            btn_request.setEnabled(false);

    }

    //공유리워드 신청
    private void reqDealShare() {

        DealShareReq dicInfo = new DealShareReq();
        dicInfo.setInstagramPhotos(imgNamess.get(0));
        dicInfo.setNaverblogPhotos(imgNamess.get(1));
        dicInfo.setFacebookPhotos(imgNamess.get(2));
        dicInfo.setTwitterPhotos(imgNamess.get(3));
        dicInfo.setKakaostoryPhotos(imgNamess.get(4));

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.reqDealShare(Globals.userToken, dealUid, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(SnsRewardActivity.this, getString(R.string.reg_sns_share_success));
                finish();
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }
}
