package com.kyad.selluv.api.dto.pdt;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class PdtStyleDao {
    //"상품스타일UID")
    private int pdtStyleUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"상품UID")
    private int pdtUid;
    //"유저UID")
    private int usrUid;
    //"스타일사진")
    private String styleImg;
    private int status = 1;
}
