/**
 * 홈
 */
package com.kyad.selluv.shop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.BannerDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.IndicatorView;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.mypage.EventDetailActivity;
import com.kyad.selluv.mypage.NoticeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.DATA_STYLE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_STYLE;

public class ShopStyleFragment extends BaseFragment implements View.OnClickListener {

    //UI Refs
    @BindView(R.id.vf_banner)
    ViewPager vf_banner;
    @BindView(R.id.rl_indicator)
    IndicatorView rl_indicator;
    @BindView(R.id.ll_top)
    LinearLayout ll_top;

    //Vars
    View rootView;

    private Handler autoHandler = new Handler();
    private List<BannerDto> bannerDtoList;
    private PagerAdapter pagerAdapter = new PagerAdapter() {
        @Override
        public int getCount() {
            return bannerDtoList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (View) object;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            return vf_banner.getChildAt(position);
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }
    };

    public WareListFragment frag_ware_list = new WareListFragment();

    public ShopStyleFragment() {
        frag_ware_list.setFragType(WIRE_FRAG_STYLE);
        frag_ware_list.setDataType(DATA_STYLE);
        frag_ware_list.isLazyLoading = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_style, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        initBannerList();

        requestBannerList();
        return rootView;
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (frag_ware_list != null) frag_ware_list.setUserVisibleHint(isVisibleToUser);
    }

    private void loadLayout() {
        ViewGroup viewParent = (ViewGroup) ll_top.getParent();
        viewParent.removeView(ll_top);
        frag_ware_list.headerViews.add(ll_top);

        addWareFragment(R.id.frag_shop_style_ware_list, frag_ware_list);
    }

    private void initBannerList() {

        bannerDtoList = new ArrayList<>();

        vf_banner.setAdapter(pagerAdapter);

        final Runnable autoRun = new Runnable() {
            @Override
            public void run() {
                vf_banner.setCurrentItem(vf_banner.getCurrentItem() + 1 < vf_banner.getChildCount() ? vf_banner.getCurrentItem() + 1 : 0, true);
                autoHandler.postDelayed(this, Constants.SHOP_BANNER_INTERVAL);
            }
        };
        autoHandler.postDelayed(autoRun, Constants.SHOP_BANNER_INTERVAL);
    }

    private void refreshBannerArea() {

        Activity activity = getActivity();
        if(activity == null) {
            return;
        }
        for (int i = 0; i < vf_banner.getChildCount(); i++) {
            vf_banner.removeView(vf_banner.getChildAt(i));
        }

        for (int i = 0; i < bannerDtoList.size(); i++) {
            final BannerDto item = bannerDtoList.get(i);
            ImageView iv = new ImageView(activity);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            iv.setLayoutParams(lp);
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);

            ImageUtils.load(getActivity(), item.getBanner().getProfileImg(), iv);

            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (item.getBanner().getType() == 1) {
                        ((BaseActivity) getActivity()).startActivity(NoticeActivity.class, false, 0, 0);
                    } else if (item.getBanner().getType() == 2) {
                        ((ShopMainFragment) getParentFragment().getParentFragment()).gotoThemaFragment(item.getBanner().getTargetUid());
                    } else if (item.getBanner().getType() == 3) {
                        Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                        intent.putExtra(Constants.EVT_UID, item.getBanner().getTargetUid());
                        getActivity().startActivity(intent);
                    }
                }
            });
            vf_banner.addView(iv, i);
        }

        pagerAdapter.notifyDataSetChanged();
        vf_banner.setOffscreenPageLimit(bannerDtoList.size());
        rl_indicator.setCount(bannerDtoList.size());
        rl_indicator.setPager(vf_banner);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    void addWareFragment(int destResID, WareListFragment frag) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(destResID, frag);
        transaction.commit();
    }

    public void requestBannerList() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<BannerDto>>> genRes = restAPI.bannerList(Globals.userToken);

        showLoading();

        genRes.enqueue(new TokenCallback<List<BannerDto>>(getActivity()) {
            @Override
            public void onSuccess(List<BannerDto> response) {
                closeLoading();
                bannerDtoList = response;
                if (bannerDtoList == null)
                    bannerDtoList = new ArrayList<>();

                refreshBannerArea();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });

    }
}
