package com.kyad.selluv.login.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants.Direction;
import com.kyad.selluv.common.Utils;

import static com.kyad.selluv.common.Constants.FRAG_IN_ANIM_DELAY;
import static com.kyad.selluv.common.Constants.VIEW_BOUNCE_ANIM_DURATION;

public class LoginBaseFragment extends BaseFragment {


    public static String PREV_CHILD_CNT = "PREV_CHILD_CNT";
    public static String FROM_DIRECTION = "FROM_DIRECTION";
    public static String FROM_REVISIT = "FROM_REVISIT";

    private View root;
    private int prevChildCnt = 0;
    private Direction from = Direction.BOTTOM;
    private Direction to = Direction.TOP;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        Bundle bundle = getArguments();
        if (bundle != null) {
            prevChildCnt = bundle.getInt(PREV_CHILD_CNT, 0);
            from = Direction.values()[bundle.getInt(FROM_DIRECTION, 0)];
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root = view.findViewById(R.id.activity_main);
        startEnterAnimation(root);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setCloseDirection(Direction to) {
        this.to = to;
    }

    public void startEnterAnimation(View rootView) {
        RelativeLayout view = ((RelativeLayout) rootView);
        int cnt = 0;
        for (int i = 0; i < view.getChildCount(); i++) {
            View v;
            if (from == Direction.TOP)
                v = view.getChildAt(view.getChildCount() - i - 1);
            else
                v = view.getChildAt(i);
            if (v.getVisibility() == View.GONE)
                continue;
            cnt++;
            Animation anim = AnimationUtils.loadAnimation(Utils.getApp().getApplicationContext(),
                    from == Direction.TOP ? R.anim.view_in_from_top :
                            (from == Direction.BOTTOM ? R.anim.view_in_from_bottom :
                                    from == Direction.LEFT ? R.anim.view_in_from_bottom : R.anim.view_in_from_bottom));
            anim.setStartOffset(cnt * VIEW_BOUNCE_ANIM_DURATION + prevChildCnt * VIEW_BOUNCE_ANIM_DURATION + FRAG_IN_ANIM_DELAY);
            v.startAnimation(anim);
        }
    }

    public void startEndAnimation(View rootView) {
        RelativeLayout view = ((RelativeLayout) rootView);
        int cnt = 0;
        for (int i = 0; i < view.getChildCount(); i++) {
            View v;
            if (to == Direction.BOTTOM)
                v = view.getChildAt(view.getChildCount() - i - 1);
            else
                v = view.getChildAt(i);
            if (v.getVisibility() == View.GONE)
                continue;
            cnt++;
            Animation anim = AnimationUtils
                    .loadAnimation(Utils.getApp().getApplicationContext(),
                            to == Direction.TOP ? R.anim.view_out_to_top :
                                    (to == Direction.BOTTOM ? R.anim.view_out_to_bottom :
                                            to == Direction.LEFT ? R.anim.view_in_from_bottom : R.anim.view_in_from_bottom));

            anim.setStartOffset(cnt * VIEW_BOUNCE_ANIM_DURATION);
            v.startAnimation(anim);
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            Animation anim = AnimationUtils.loadAnimation(getActivity(),
                    from == Direction.TOP ? R.anim.frag_slide_in_from_top :
                            (from == Direction.BOTTOM ? R.anim.frag_slide_in_from_bottom :
                                    from == Direction.LEFT ? R.anim.view_in_from_bottom : R.anim.view_in_from_bottom));
            anim.setStartOffset(prevChildCnt * VIEW_BOUNCE_ANIM_DURATION + FRAG_IN_ANIM_DELAY);
            return anim;
        } else {
            startEndAnimation(root);
            Animation anim = AnimationUtils.loadAnimation(getActivity(),
                    to == Direction.TOP ? R.anim.frag_slide_out_to_top :
                            (to == Direction.BOTTOM ? R.anim.frag_slide_out_to_bottom :
                                    to == Direction.LEFT ? R.anim.view_in_from_bottom : R.anim.view_in_from_bottom));
            anim.setStartOffset(((RelativeLayout) root).getChildCount() * VIEW_BOUNCE_ANIM_DURATION);
            return anim;
        }
    }
}
