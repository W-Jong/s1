package com.kyad.selluv.login.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.enumtype.LICENSE_TYPE;
import com.kyad.selluv.api.enumtype.LOGIN_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.mypage.LicenseActivity;
import com.kyad.selluv.webview.CommonWebViewActivity;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Globals.userSignReq;

public class EmailRegisterFragment extends LoginBaseFragment implements View.OnFocusChangeListener, TextWatcher {

    //UI Reference
    @BindView(R.id.edt_email)
    EditText edt_email;

    @BindView(R.id.edt_id)
    EditText edt_id;

    @BindView(R.id.edt_nickname)
    EditText edt_nickname;

    @BindView(R.id.edt_pwd)
    EditText edt_pwd;

    @BindView(R.id.edt_invite_code)
    EditText edt_invite_code;

    @BindView(R.id.iv_check_id)
    ImageView iv_check_id;

    @BindView(R.id.iv_check_mail)
    ImageView iv_check_mail;

    @BindView(R.id.btn_ok)
    Button btn_ok;

    //Variables
    int childCnt = 0;
    boolean fromRevisit = false;
    View rootView;


    public static EmailRegisterFragment newInstance(int prevChildCnt, Constants.Direction direction, boolean fromRevisit) {
        EmailRegisterFragment fragment = new EmailRegisterFragment();
        Bundle args = new Bundle();
        args.putInt(PREV_CHILD_CNT, prevChildCnt);
        args.putInt(FROM_DIRECTION, direction.ordinal());
        args.putBoolean(FROM_REVISIT, fromRevisit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromRevisit = bundle.getBoolean(FROM_REVISIT, false);
            childCnt = bundle.getInt(PREV_CHILD_CNT, 0);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_id_register, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void loadLayout() {
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        edt_email.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        inputMethodManager.showSoftInputFromInputMethod(edt_email.getWindowToken(), InputMethodManager.SHOW_FORCED);

        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.activity_main));

        edt_id.addTextChangedListener(this);
        edt_id.setOnFocusChangeListener(this);
        edt_id.setFilters(new InputFilter[]{new IdInputFilter()});
        edt_id.setPrivateImeOptions("defaultInputmode=english;");

        edt_email.addTextChangedListener(this);
        edt_email.setOnFocusChangeListener(this);
        edt_email.setPrivateImeOptions("defaultInputmode=english;");

        edt_nickname.addTextChangedListener(this);
        edt_nickname.setOnFocusChangeListener(this);

        edt_pwd.addTextChangedListener(this);
        edt_pwd.setOnFocusChangeListener(this);

        edt_invite_code.setOnFocusChangeListener(this);
        if (Constants.IS_TEST) {
            edt_id.setText("ab");
            edt_email.setText("kenjimakuda@gmail.com");
            edt_nickname.setText("겐지");
            edt_pwd.setText("1");
            edt_invite_code.setText("test22");
        }
    }

    @OnClick(R.id.ib_go_back)
    public void onBack(View v) {
        setCloseDirection(Constants.Direction.BOTTOM);
        if (fromRevisit)
            ((LoginStartActivity) getActivity()).gotoRevisitFragment(childCnt, Constants.Direction.TOP);
        else
            ((LoginStartActivity) getActivity()).gotoStartFragment(childCnt, Constants.Direction.TOP);
    }

    @OnClick(R.id.btn_ok)
    void onOk(View v) {
        if (iv_check_id.getVisibility() != View.VISIBLE) {
            checkUserId();
            return;
        }

        if (iv_check_mail.getVisibility() != View.VISIBLE) {
            checkUserMail();
            return;
        }


        if (edt_invite_code.getText().length() > 0) {
            checkInviteCode();
            return;
        }

        cmdRegisterUser();
    }

    @OnClick(R.id.tv_license)
    void OnClickLicense(View v) {
        //이용약관
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.LICENSE.toString());
        startActivity(intent);

    }

    @OnClick(R.id.tv_privacy)
    void OnClickPrivacy(View v) {
        //개인정보취급방침
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.PERSONAL_POLICY.toString());
        startActivity(intent);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!edt_id.isFocused() && hasFocus && iv_check_id.getVisibility() != View.VISIBLE && edt_id.getText().length() > 0) {
            checkUserId();
        }

        if (!edt_email.isFocused() && hasFocus && iv_check_mail.getVisibility() != View.VISIBLE && edt_email.getText().length() > 0) {
            checkUserMail();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (edt_id.isFocused())
            iv_check_id.setVisibility(View.INVISIBLE);
        else if (edt_email.isFocused())
            iv_check_mail.setVisibility(View.INVISIBLE);

        if (edt_id.getText().toString().length() > 0 && edt_email.getText().length() > 0 && edt_nickname.getText().toString().trim().length() > 0 && edt_pwd.getText().length() > 0) {
            btn_ok.setEnabled(true);
        } else {
            btn_ok.setEnabled(false);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    protected class IdInputFilter implements InputFilter {
        @Override
        public CharSequence filter(CharSequence source, int start,
                                   int end, Spanned dest, int dstart, int dend) {
            Pattern ps = Pattern.compile("^[-_a-zA-Z0-9]+$");

            if (source.equals("") || ps.matcher(source).matches()) {
                return source;
            }

            Toast.makeText(getActivity(),
                    "영문, 숫자, _ , -만 입력 가능합니다.", Toast.LENGTH_SHORT).show();

            return "";
        }
    }


    private void checkUserId() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse> genRes = restAPI.checkUserId(edt_id.getText().toString());
        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    iv_check_id.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push));
                    iv_check_id.setVisibility(View.VISIBLE);
                } else {//실패
                    iv_check_id.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push_denied));
                    iv_check_id.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void checkInviteCode() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse> genRes = restAPI.checkInviteCode(edt_invite_code.getText().toString());
        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    cmdRegisterUser();
                } else {//실패
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void checkUserMail() {
        if (!Utils.isValidEmail(edt_email.getText())) {
            iv_check_mail.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push_denied));
            iv_check_mail.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "이메일 형식이 올바르지 않습니다", Toast.LENGTH_SHORT).show();
            return;
        } else {
            iv_check_mail.setVisibility(View.GONE);
        }

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse> genRes = restAPI.checkUserMail(edt_email.getText().toString());
        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    iv_check_mail.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push));
                    iv_check_mail.setVisibility(View.VISIBLE);
                } else {//실패
                    iv_check_mail.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push_denied));
                    iv_check_mail.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cmdRegisterUser() {
        userSignReq.setUsrMail(edt_email.getText().toString());
        userSignReq.setUsrId(edt_id.getText().toString());
        userSignReq.setUsrNckNm(edt_nickname.getText().toString());
        userSignReq.setPassword(edt_pwd.getText().toString());
        userSignReq.setInviteCode(edt_invite_code.getText().toString());
        userSignReq.setLoginType(LOGIN_TYPE.NORMAL);


//        ((LoginStartActivity) getActivity()).register();
        CommonWebViewActivity.showDanalWebviewActivity(getActivity());
    }
}
