package com.kyad.selluv.common;

import android.graphics.Point;

import com.kyad.selluv.api.dao.UsrCardDao;
import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.api.dto.other.PromotionCodeDao;
import com.kyad.selluv.api.dto.other.SystemSettingDto;
import com.kyad.selluv.api.dto.pdt.PdtDetailDto;
import com.kyad.selluv.api.dto.usr.UsrMyInfoDto;
import com.kyad.selluv.api.enumtype.LOGIN_TYPE;
import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.api.enumtype.SEARCH_ORDER_TYPE;
import com.kyad.selluv.api.request.DealBuyReq;
import com.kyad.selluv.api.request.PdtCreateReq;
import com.kyad.selluv.api.request.PdtUpdateReq;
import com.kyad.selluv.api.request.SearchReq;
import com.kyad.selluv.api.request.UserSignupReq;

import java.util.ArrayList;
import java.util.List;

import static com.kyad.selluv.common.Constants.GUEST_TOKEN;
import static com.kyad.selluv.common.Constants.female_category1_icon;
import static com.kyad.selluv.common.Constants.kids_category1_icon;
import static com.kyad.selluv.common.Constants.male_category1_icon;
import static com.kyad.selluv.common.Constants.shopper_type_icon;

public class Globals {
    // Global Variables

    public static UserSignupReq userSignReq = new UserSignupReq();

    public static String userToken = GUEST_TOKEN;

    public static UsrMyInfoDto myInfo = new UsrMyInfoDto();

    public static SystemSettingDto systemSetting = new SystemSettingDto();

    public static List<CategoryDao> subCategories1;
    public static List<CategoryDao> subCategories2;
    public static List<CategoryDao> subCategories3;

    //Variables for category selection
    /**
     * 선택된 상품군  = 남성, 여성, 키즈
     */
    public static int currentShopperTypeIdx = 0;      //Shopper Type for Category Selection
    public static int[] categoryIndex = {1, 1, 1};    //Category 1,2,3 Index for Category Selection
    public static int categoryDepth = 0;

    public static Point screenSize;

    /**
     * 키즈 1차, 2차 카테고리 구성변수
     */
    public static String gKisCategoryName = "";

    /**
     * 카드선택시 선택된 카드데이터
     */
    public static UsrCardDao selectedUsrCardDao = null;

    /**
     * 선택된 상품상세정보 - 네고제안, 상품구매시 이용
     */
    public static PdtDetailDto selectedPdtDetailDto = null;
    public static DealBuyReq selectedDealBuyReq = null;
    public static PromotionCodeDao selectedPromotionCodeDao = null;

    /**
     * 상품 편집중인가??
     */
    public static boolean isEditing = false;
    /**
     * 상품편집 정보 API통신용
     */
    public static PdtUpdateReq pdtUpdateReq = null;
    /**
     * 상품등록 정보 API통신용
     */
    public static PdtCreateReq pdtCreateReq = null;
    /**
     * 상품 등록 할 때... 선택한 브랜드 // 임시 값
     */
    public static BrandMiniDto selectedBrand;
    /**
     * 검색필터
     */
    public static SearchReq searchReq = null;
    /**
     * 브랜드 검색 필터 텍스트
     */
    public static String brandFilterName = "";
    /**
     * 카테고리 검색 필터 패스
     */
    public static String categoryFilterPath = "";

    /**
     * hone, shop, brand페지들에서 마지막으로 본 택을 유지시키기 위한 변수
     */
    public static int selectHomeTab = 1;
    public static int selectShopTab = 0;
    public static int selectBrandTab = 0;

    /**
     * 카테고리 검색 필터 텍스트
     */
    public static String categoryFilterName = "";

    //Retrieve Category Icon for UI Test
    public static int getCategoryIcon() {
        if (categoryDepth == 0) {
            return shopper_type_icon[currentShopperTypeIdx];
        } else {
            return currentShopperTypeIdx == 0 ? male_category1_icon[categoryIndex[0]] : (currentShopperTypeIdx == 1 ? female_category1_icon[categoryIndex[0]] : kids_category1_icon[categoryIndex[0]]);
        }
    }


    public static int getLoginTypeIdx(String login_type) {
        int typeIdx = LOGIN_TYPE.NORMAL.getCode();
        if (login_type.equals(LOGIN_TYPE.FACEBOOK.toString())) {
            typeIdx = LOGIN_TYPE.FACEBOOK.getCode();
        } else if (login_type.equals(LOGIN_TYPE.KAKAOTALK.toString())) {
            typeIdx = LOGIN_TYPE.KAKAOTALK.getCode();
        } else if (login_type.equals(LOGIN_TYPE.NAVER.toString())) {
            typeIdx = LOGIN_TYPE.NAVER.getCode();
        }
        return typeIdx;
    }

    public static String getLoginTypeStr(int idx) {
        String result = LOGIN_TYPE.NORMAL.toString();
        if (idx == LOGIN_TYPE.FACEBOOK.getCode()) {
            result = LOGIN_TYPE.FACEBOOK.toString();
        } else if (idx == LOGIN_TYPE.KAKAOTALK.getCode()) {
            result = LOGIN_TYPE.KAKAOTALK.toString();
        } else if (idx == LOGIN_TYPE.NAVER.getCode()) {
            result = LOGIN_TYPE.NAVER.toString();
        }
        return result;
    }

    /**
     * 서브 카테고리 뎁스 1은 로컬정보로 처리 (서버에 등록되어있는 ID, NAME 일치 되어야함!!!)
     *
     * @return
     */
    public static List<CategoryDao> getSubCategoryDepth_1() {

        int[][] SUBCATEGORY_IDS = {{5, 6, 7, 8, 9}, {10, 11, 12, 13, 14}, {15, 16, 17, 18, 19}};
        String[][] SUBCATEGORY_NAMES = {
                {"의류", "가방", "슈즈", "패션소품", "시계/쥬얼리"},
                {"의류", "핸드백", "슈즈", "패션소품", "시계/쥬얼리"},
                {"베이비", "키즈보이", "키즈걸", "틴보이", "틴걸"}
        };
        List<CategoryDao> result = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            CategoryDao categoryDao = new CategoryDao();
            if (currentShopperTypeIdx == 1) {
                categoryDao.setCategoryUid(SUBCATEGORY_IDS[1][i]);
                categoryDao.setCategoryName(SUBCATEGORY_NAMES[1][i]);
            } else if (currentShopperTypeIdx == 2) {
                categoryDao.setCategoryUid(SUBCATEGORY_IDS[2][i]);
                categoryDao.setCategoryName(SUBCATEGORY_NAMES[2][i]);
            } else { // 0
                categoryDao.setCategoryUid(SUBCATEGORY_IDS[0][i]);
                categoryDao.setCategoryName(SUBCATEGORY_NAMES[0][i]);

            }
            result.add(categoryDao);
        }
        return result;
    }

    /**
     * 셀러가 상품 등록을 진행할 때.. 선택한 카테고리 정보
     *
     * @return
     */
    public static CategoryDao getSelectedCategory() {
        switch (categoryDepth) {
            case 0:
                return subCategories1.get(categoryIndex[0]);
            case 1:
                return subCategories2.get(categoryIndex[1]);
            case 2:
                return subCategories3.get(categoryIndex[2]);
        }
        return null;
    }

    public static boolean isMyUsrUid(int usrUid) {
        return (Globals.myInfo != null &&
                Globals.myInfo.getUsr() != null &&
                Globals.myInfo.getUsr().getUsrUid() == usrUid);
    }

    public static String componentCodeToName(String code) {
        if (code.length() != Constants.COMPONENT_NAMES.length) {
            return "";
        }
        String result = "";
        for (int i = 0; i < code.length(); i++) {
            if (code.substring(i).startsWith("1")) {
                result += (Constants.COMPONENT_NAMES[i] + " ");
            }
        }
        return result.trim();
    }

    /**
     * 정렬 라벨
     *
     * @param _type
     * @return
     */
    public static String getOrderLabel(SEARCH_ORDER_TYPE _type) {
        if (_type.getCode() == SEARCH_ORDER_TYPE.NEW.getCode())
            return "신상품순";
        if (_type.getCode() == SEARCH_ORDER_TYPE.FAME.getCode())
            return "인기순";
        if (_type.getCode() == SEARCH_ORDER_TYPE.PRICELOW.getCode())
            return "가격 낮은순";
        if (_type.getCode() == SEARCH_ORDER_TYPE.PRICEHIGH.getCode())
            return "가격 높은순";

        return "추천순";
    }

    /**
     * 검색 필터 초기화
     */

    public static void initSearchFilters() {
        Globals.brandFilterName = "";
        Globals.categoryFilterPath = "";
        Globals.categoryFilterName = "";

        Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.RECOMMEND);//추천순
        Globals.searchReq.setCategoryUid(0);//전체
        Globals.searchReq.setBrandUidList(new ArrayList<Integer>());//전체
        Globals.searchReq.setPdtSizeList(new ArrayList<String>());//전체
        Globals.searchReq.setPdtConditionList(new ArrayList<PDT_CONDITION_TYPE>());//전체
        Globals.searchReq.setPdtColorList(new ArrayList<String>());//전체
        Globals.searchReq.setPdtPriceMin(-1);//최소 제한 없음
        Globals.searchReq.setPdtPriceMax(-1);//최대 제한 없음
        Globals.searchReq.setSoldOutExpect(false);//품절상품제외 on/off
    }
}