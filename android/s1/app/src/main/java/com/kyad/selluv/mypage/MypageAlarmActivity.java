package com.kyad.selluv.mypage;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.AlarmCountDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.MypageAlarmFragment;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class MypageAlarmActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;

    @BindView(R.id.iv_deal_badge)
    ImageView iv_deal_badge;
    @BindView(R.id.iv_news_badge)
    ImageView iv_news_badge;
    @BindView(R.id.iv_reply_badge)
    ImageView iv_reply_badge;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.ib_setting)
    void onSetting() {
        startActivity(AlarmSettingActivity.class, false, 0, 0);
    }

    //Variables
    int selectedTab = 0;
    MypageAlarmFragment dealFragment, newsFragment, commentFragment;
    private PageAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_alarm);
        setStatusBarWhite();
        initFragment();
        loadLayout();
        selectPage();
        getAlarmcount();
    }

    private void loadLayout() {
        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
//                tl_top_tab.getTabAt(position).select();
                selectPage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);

        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle(0);

    }

    private void initFragment() {
        if (dealFragment == null) {
            dealFragment = new MypageAlarmFragment();
            dealFragment.setLoadInterface(new MypageAlarmFragment.DataLoadInterface() {
                @Override
                public Model.AlarmItem[] loadData() {
                    return loadDealData();
                }
            });
        }
        if (newsFragment == null) {
            newsFragment = new MypageAlarmFragment();
            newsFragment.setLoadInterface(new MypageAlarmFragment.DataLoadInterface() {
                @Override
                public Model.AlarmItem[] loadData() {
                    return loadNewsData();
                }
            });
        }

        if (commentFragment == null) {
            commentFragment = new MypageAlarmFragment();
            commentFragment.setLoadInterface(new MypageAlarmFragment.DataLoadInterface() {
                @Override
                public Model.AlarmItem[] loadData() {
                    return loadCommentData();
                }
            });
        }
    }

    private void selectPage() {
        if (selectedTab == 0) {
            dealFragment.setTab(0);
        } else if (selectedTab == 1) {
            newsFragment.setTab(1);
        } else {
            commentFragment.setTab(2);
        }

        tl_top_tab.getTabAt(selectedTab).select();
    }

    public Model.AlarmItem[] loadDealData() {
        ArrayList<Model.AlarmItem> alarmListArray = new ArrayList<Model.AlarmItem>();
        Collections.addAll(alarmListArray, Constants.alarmList0);
        int cnt = alarmListArray.size();
        return (Model.AlarmItem[]) alarmListArray.toArray(new Model.AlarmItem[cnt]);
    }

    public Model.AlarmItem[] loadNewsData() {
        ArrayList<Model.AlarmItem> alarmListArray = new ArrayList<Model.AlarmItem>();
        Collections.addAll(alarmListArray, Constants.alarmList0);
        int cnt = alarmListArray.size();
        return (Model.AlarmItem[]) alarmListArray.toArray(new Model.AlarmItem[cnt]);
    }

    public Model.AlarmItem[] loadCommentData() {
        ArrayList<Model.AlarmItem> alarmListArray = new ArrayList<Model.AlarmItem>();
        Collections.addAll(alarmListArray, Constants.alarmList0);
        int cnt = alarmListArray.size();
        return (Model.AlarmItem[]) alarmListArray.toArray(new Model.AlarmItem[cnt]);
    }


    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = dealFragment;
                    break;
                case 1:
                    frag = newsFragment;
                    break;
                case 2:
                    frag = commentFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    //get Alarm count
    private void getAlarmcount() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<AlarmCountDto>> genRes = restAPI.getAlarmCnt(Globals.userToken);

        genRes.enqueue(new TokenCallback<AlarmCountDto>(this) {
            @Override
            public void onSuccess(AlarmCountDto response) {
                closeLoading();

                if (response.getDealCount() > 0){
                    iv_deal_badge.setVisibility(View.VISIBLE);
                } else {
                    iv_deal_badge.setVisibility(View.INVISIBLE);
                }

                if (response.getNewsCount() > 0){
                    iv_news_badge.setVisibility(View.VISIBLE);
                } else {
                    iv_news_badge.setVisibility(View.INVISIBLE);
                }

                if (response.getReplyCount() > 0){
                    iv_reply_badge.setVisibility(View.VISIBLE);
                } else {
                    iv_reply_badge.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }
}
