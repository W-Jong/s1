package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import com.kyad.selluv.api.enumtype.DEAL_TYPE;

import lombok.Data;
import lombok.NonNull;

@Data
public class PromotionReq {

    @Size(min = 1)
    //@ApiModelProperty("프로모션코드")
    private String promotionCode;

    @NonNull
    //@ApiModelProperty("코드분류(구매용, 판매용)")
    private DEAL_TYPE dealType;

    //@ApiModelProperty("브랜드UID, 정해지지 않은 경우 0")
    @IntRange(from = 0)
    private int brandUid;

    //@ApiModelProperty("카테고리UID, 정해지지 않은 경우 0")
    @IntRange(from = 0)
    private int categoryUid;

    //@ApiModelProperty("상품UID, 정해지지 않은 경우 0")
    @IntRange(from = 0)
    private int pdtUid;

    public PromotionReq() {

    }
}
