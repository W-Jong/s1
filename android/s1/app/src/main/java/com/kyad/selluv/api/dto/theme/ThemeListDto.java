package com.kyad.selluv.api.dto.theme;

import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.common.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import lombok.Data;

@Data
public class ThemeListDto extends ExpandableGroup<PdtListDto> {
    //("테마")
    private ThemeDao theme;
    //("테마상품갯수")
    private int pdtCount;
    //("테마상품목록")
    private List<PdtListDto> pdtList;

    ThemeListDto(List<PdtListDto> pdtList) {
        super(pdtList);
    }
}
