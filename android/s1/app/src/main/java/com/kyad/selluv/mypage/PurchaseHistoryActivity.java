package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.HistoryListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.BuyHistoryDto;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class PurchaseHistoryActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_nego_count)
    TextView tv_nego_count;
    @BindView(R.id.tv_count_deal)
    TextView tv_count_deal;
    @BindView(R.id.tv_count_prepare)
    TextView tv_count_prepare;
    @BindView(R.id.tv_count_cert)
    TextView tv_count_cert;
    @BindView(R.id.tv_count_run)
    TextView tv_count_run;
    @BindView(R.id.tv_count_finish)
    TextView tv_count_finish;
    @BindView(R.id.tv_history_cnt)
    TextView tv_history_cnt;
    @BindView(R.id.lv_history)
    ListView lv_history;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }


    //Variables
    HistoryListAdapter listAdapter;
    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_history);
        setStatusBarWhite();
        loadLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void loadLayout() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        lv_history.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, dm.heightPixels));

        listAdapter = new HistoryListAdapter(this);
        lv_history.setAdapter(listAdapter);
//        Collections.addAll(listAdapter.arrData, Constants.historyList0);
//        listAdapter.notifyDataSetChanged();
        lv_history.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getBuyHistoryList();
                }
            }
        });
    }

    public void initData(){
        currentPage = 0;
        isLoading = false;
        isLast = false;
        getBuyHistoryList();
    }

    //get buy history list
    private void getBuyHistoryList() {
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();
        Call<GenericResponse<BuyHistoryDto>> genRes = restAPI.getBuyHistoryList(Globals.userToken, currentPage);

        genRes.enqueue(new TokenCallback<BuyHistoryDto>(this) {
            @Override
            public void onSuccess(BuyHistoryDto response) {
                closeLoading();
                isLoading = false;

                if (currentPage == 0) {
                    listAdapter.arrData.clear();
                }

                isLast = response.getHistory().isLast();

                tv_nego_count.setText("" + response.getNegoCount());
                tv_count_deal.setText("" + response.getCompletedCount());
                tv_count_prepare.setText("" + response.getPreparedCount());
                tv_count_cert.setText("" + response.getVerifiedCount());
                tv_count_run.setText("" + response.getProgressCount());
                tv_count_finish.setText("" + response.getSentCount());
                tv_history_cnt.setText("" + response.getHistory().getTotalElements());

                listAdapter.arrData.addAll(response.getHistory().getContent());
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }
}
