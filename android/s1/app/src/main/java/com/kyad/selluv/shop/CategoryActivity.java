package com.kyad.selluv.shop;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandRecommendedListDto;
import com.kyad.selluv.api.dto.category.CategoryDetailDto;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.top.TopLikeActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.ACT_SEARCH_DETAIL;
import static com.kyad.selluv.common.Constants.TAB_BAR_HEIGHT;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_CATEGORY;
import static com.kyad.selluv.common.Constants.female_category1;
import static com.kyad.selluv.common.Constants.kids_category1;
import static com.kyad.selluv.common.Constants.male_category1;
import static com.kyad.selluv.common.Constants.shopper_type;
import static com.kyad.selluv.common.Globals.categoryDepth;
import static com.kyad.selluv.common.Globals.currentShopperTypeIdx;

public class CategoryActivity extends BaseActivity {

    public static final String PDT_GROUP = "PDT_GROUP";
    public static final String CATEGORY_UID = "CATEGORY_UID";
    public static final String COLOR_NAME = "COLOR_NAME";

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.rly_no_data)
    RelativeLayout rly_no_data;

    LinearLayout ll_brands;
    LinearLayout ll_categories;

    WareListFragment fragment_popular, fragment_detail;
    View vPopular, vDetail;

    //Variables
    int selectedTab = 0;    //  0: 피드, 1: 인기, 2: 신상
    ArrayList<View> brandList = new ArrayList<View>();
    ArrayList<ToggleButton> categoryList = new ArrayList<ToggleButton>();
    boolean isFinalCategory = false;
    int selectedBrand = -1, selectedCategory = -1;
    FragmentManager fm;
    FragmentTransaction transaction;
    private int basisCategoryUid = 1;
    private int basicPdtGroup = 1;
    private String basicColorName = "";

    private PagerAdapter pagerAdapter;

    private CategoryDetailDto categoryDetail;
    private int PdtDetailPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        basicPdtGroup = getIntent().getIntExtra(PDT_GROUP, 1);
        basisCategoryUid = getIntent().getIntExtra(CATEGORY_UID, 1);

        basicColorName = getIntent().getStringExtra(COLOR_NAME);
        PdtDetailPage = getIntent().getIntExtra("PDT_DETAIL", 0); //1000이면 상품상세페이지에서 온것
        Globals.searchReq.setCategoryUid(basisCategoryUid);
        if (basicColorName != null) {
            //TODO: 검색설정 컬러명 적용
        }

        loadLayout();

        IntentFilter f = new IntentFilter();
        f.addAction(ACT_SEARCH_DETAIL);
        registerReceiver(mBroadcastReceiver, new IntentFilter(f));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        categoryDepth = 0;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACT_SEARCH_DETAIL)) {
                if (selectedTab == 0) {
                    fragment_popular.setFragType(Constants.WIRE_FRAG_SEARCH);
                    fragment_popular.setLoadInterface("");
                    fragment_popular.setFromWhere(Constants.PAGE_CATEGORY);
                } else {
                    fragment_popular.setFragType(Constants.WIRE_FRAG_SEARCH);
                    fragment_popular.setLoadInterface("");
                    fragment_popular.setFromWhere(Constants.PAGE_CATEGORY);
                }
            }
        }
    };

    private void loadLayout() {
//        BarUtils.addMarginTopEqualStatusBarHeight(findViewById(R.id.appbar));
        findViewById(R.id.appbar).setAlpha(0.95f);

        fm = getSupportFragmentManager();

//        tv_title.setText(basicPdtGroup == PDT_GROUP_TYPE.MALE.getCode() ? "남성" :
//                basicPdtGroup == PDT_GROUP_TYPE.FEMALE.getCode() ? "여성 " : "키즈");

//        isFinalCategory = Constants.categoryItems.length == 0;
//        if (isFinalCategory) {
//            tl_top_tab.setVisibility(View.GONE);
//        }

        initFragment();

        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        vPopular = View.inflate(this, R.layout.fragment_category, null);
        vDetail = View.inflate(this, R.layout.fragment_category, null);
        vPopular.findViewById(R.id.frag_ware_list).setId(R.id.frag_ware_list_popular);
        vDetail.findViewById(R.id.frag_ware_list).setId(R.id.frag_ware_list_detail);
        vDetail.findViewById(R.id.sv_brands).setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPixel(this, 88)));

        ll_brands = (LinearLayout) vPopular.findViewById(R.id.ll_brands);
        ll_categories = (LinearLayout) vDetail.findViewById(R.id.ll_brands);

        View ll_top1 = vPopular.findViewById(R.id.sv_brands);
        ViewGroup viewParent1 = (ViewGroup) ll_top1.getParent();
        viewParent1.removeView(ll_top1);
        fragment_popular.headerViews.add(ll_top1);

        View ll_top2 = vDetail.findViewById(R.id.sv_brands);
        ViewGroup viewParent2 = (ViewGroup) ll_top2.getParent();
        viewParent2.removeView(ll_top2);
        fragment_detail.headerViews.add(ll_top2);

        vp_main.addView(vPopular, 0);
        vp_main.addView(vDetail, 1);

        pagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return isFinalCategory ? 1 : 2;
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == (View) object;
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup collection, int position) {
                return vp_main.getChildAt(position);
            }

            @Override
            public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
                collection.removeView((View) view);
            }
        };
        vp_main.setAdapter(pagerAdapter);

        vp_main.setOffscreenPageLimit(isFinalCategory ? 1 : 2);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();

                refreshList();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle(0);

        updateLikePdtBadge();

        addWareFragment(R.id.frag_ware_list_popular, fragment_popular);
        addWareFragment(R.id.frag_ware_list_detail, fragment_detail);

        getCategoryDetail(basisCategoryUid);
    }

    private void refreshList() {
        if (categoryDetail == null)
            return;
        Globals.initSearchFilters();
        if (selectedTab == 0) {
            int brandUid = (selectedBrand == -1 || categoryDetail.getRecommendedBrand().get(selectedBrand) == null) ? 0 : categoryDetail.getRecommendedBrand().get(selectedBrand).getBrandUid();
            Globals.searchReq.setCategoryUid(basisCategoryUid);
            fragment_popular.setBrandUid(brandUid);
            fragment_popular.setFragType(WIRE_FRAG_CATEGORY);
            fragment_popular.setCategoryUid(basisCategoryUid);
            fragment_popular.setFromWhere(Constants.PAGE_CATEGORY);
            fragment_popular.removeAll();
            fragment_popular.refresh();
        } else {
            int categoryUid = (selectedCategory == -1 || categoryDetail.getChildrenCategories().get(selectedCategory) == null) ? basisCategoryUid : categoryDetail.getChildrenCategories().get(selectedCategory).getCategoryUid();
            Globals.searchReq.setCategoryUid(categoryUid);
            fragment_detail.setBrandUid(0);
            fragment_detail.setFragType(WIRE_FRAG_CATEGORY);
            fragment_detail.setCategoryUid(categoryUid);
            fragment_detail.setFromWhere(Constants.PAGE_CATEGORY);
            fragment_detail.removeAll();
            fragment_detail.refresh();
        }
    }

    private void initFragment() {
        LinearLayout appbar = findViewById(R.id.appbar);

        if (fragment_popular == null) {
            fragment_popular = new WareListFragment();
            fragment_popular.paddintTop = isFinalCategory ? Utils.dpToPixel(this, 50) : Utils.dpToPixel(this, TAB_BAR_HEIGHT);
            fragment_popular.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
            fragment_popular.showFilterButton = true;
            fragment_popular.showFilter = true;
            fragment_popular.isMiniImageShow = false;
            fragment_popular.setFragType(WIRE_FRAG_CATEGORY);
            fragment_popular.setCategoryUid(basisCategoryUid);
            fragment_popular.setFromWhere(Constants.PAGE_CATEGORY);
            fragment_popular.setBrandUid(0);
            fragment_popular.setAppbar(appbar);
        }

        if (fragment_detail == null) {
            fragment_detail = new WareListFragment();
            fragment_detail.paddintTop = isFinalCategory ? Utils.dpToPixel(this, 50) : Utils.dpToPixel(this, TAB_BAR_HEIGHT);
            fragment_detail.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
            fragment_detail.showFilterButton = true;
            fragment_detail.showFilter = true;
            fragment_detail.isMiniImageShow = false;
            fragment_detail.setFragType(WIRE_FRAG_CATEGORY);
            fragment_detail.setCategoryUid(basisCategoryUid);
            fragment_detail.setFromWhere(Constants.PAGE_CATEGORY);
            fragment_detail.setBrandUid(0);
            fragment_detail.setAppbar(appbar);
        }
    }

    @OnClick(R.id.ib_left)
    void onBack(View v) {
        onBackPressed();
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    void addWareFragment(int destResID, WareListFragment frag) {
        transaction = fm.beginTransaction();
        transaction.replace(destResID, frag);
        transaction.commit();
    }

    private void addPopularBrands() {
        if (categoryDetail == null || categoryDetail.getRecommendedBrand() == null)
            return;

        ll_brands.removeAllViews();
        for (int i = 0; i < categoryDetail.getRecommendedBrand().size(); i++) {
            BrandRecommendedListDto item = categoryDetail.getRecommendedBrand().get(i);
            LayoutInflater inflater = LayoutInflater.from(this);
            View v = inflater.inflate(R.layout.item_shop_popular_brand, null);
            if ("".equals(item.getLogoImg())) {
                ((ImageView) v.findViewById(R.id.iv_brand_logo)).setVisibility(View.GONE);
                ((TextView) v.findViewById(R.id.tv_brand_name)).setText(item.getNameEn());
            } else {
                ((TextView) v.findViewById(R.id.tv_brand_name)).setVisibility(View.GONE);
                ImageUtils.load(this, item.getLogoImg(), ((ImageView) v.findViewById(R.id.iv_brand_logo)));
            }
            ImageUtils.load(this, item.getBackImg(), ((ImageView) v.findViewById(R.id.iv_brand_back)));
            ((TextView) v.findViewById(R.id.tv_ware_cnt)).setText(String.valueOf(item.getPdtCount()));
            brandList.add(v);
            final int index = i;
            v.findViewById(R.id.tb_select).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((ToggleButton) view).isChecked())
                        selectedBrand = index;
                    else selectedBrand = -1;
                    updateBrands(selectedBrand);
                }
            });

            ll_brands.addView(v);
        }
    }

    private void addDetailCategories() {
        if (categoryDetail == null || categoryDetail.getChildrenCategories() == null)
            return;

        ll_categories.removeAllViews();
        for (int i = 0; i < categoryDetail.getChildrenCategories().size(); i++) {
            String category = categoryDetail.getChildrenCategories().get(i).getCategoryName();
            LayoutInflater inflater = LayoutInflater.from(this);
            View v = inflater.inflate(R.layout.item_shop_detail_category, null);
            ToggleButton tb_category = ((ToggleButton) v.findViewById(R.id.tb_category));
            tb_category.setText(category);
            categoryList.add((ToggleButton) v.findViewById(R.id.tb_category));
            final int index = i;
            tb_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((ToggleButton) view).isChecked())
                        selectedCategory = index;
                    else selectedCategory = -1;
                    updateCategories(selectedCategory);
                }
            });

            ll_categories.addView(v);
        }
    }

    public void updateBrands(int index) {
        for (int i = 0; i < brandList.size(); i++) {
            if (i != index) {
                ToggleButton tb_select = (ToggleButton) brandList.get(i).findViewById(R.id.tb_select);
                tb_select.setChecked(false);
                ((ImageView) brandList.get(i).findViewById(R.id.iv_brand_back_cover)).setImageDrawable(getResources().getDrawable(R.color.color_000000_80));
            } else {
                ((ImageView) brandList.get(i).findViewById(R.id.iv_brand_back_cover)).setImageDrawable(getResources().getDrawable(R.color.color_000000_30));
            }
        }
        refreshList();
    }

    public void updateCategories(int index) {
        for (int i = 0; i < categoryList.size(); i++) {
            ToggleButton tb_select = (ToggleButton) categoryList.get(i);
            if (i != index) {
                tb_select.setChecked(false);
            } else {
                tb_select.setChecked(true);
            }
        }
        refreshList();
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private String getCategoryTitle() {
        if (categoryDepth == 0) {
            return shopper_type[currentShopperTypeIdx];
        } else if (categoryDepth == 1) {
            String[] titleArray = Globals.currentShopperTypeIdx == 0 ? male_category1 : Globals.currentShopperTypeIdx == 1 ? female_category1 : kids_category1;
            return titleArray[Globals.categoryIndex[0] - 5 - Globals.currentShopperTypeIdx * 5 + 1];
        }
        return "";
    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(this) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void getCategoryDetail(final int selectedCategoryUid) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<CategoryDetailDto>> genRes = restAPI.categoryDetail(Globals.userToken, selectedCategoryUid);

        genRes.enqueue(new TokenCallback<CategoryDetailDto>(this) {
            @Override
            public void onSuccess(CategoryDetailDto response) {
                closeLoading();
                categoryDetail = response;

                if (selectedCategoryUid >= 5) {
                    if (basicPdtGroup == PDT_GROUP_TYPE.KIDS.getCode() && Globals.categoryDepth > 1) {
                        tv_title.setText(Globals.gKisCategoryName);
                        Globals.categoryFilterName = Globals.gKisCategoryName;
                    } else {
                        if (PdtDetailPage == 1000) {//상품상세페이지에서 왔으면
                            tv_title.setText(Globals.gKisCategoryName);
                            Globals.categoryFilterName = Globals.gKisCategoryName;
                        } else {
                            tv_title.setText((basicPdtGroup == PDT_GROUP_TYPE.MALE.getCode() ? "남성 " :
                                    basicPdtGroup == PDT_GROUP_TYPE.FEMALE.getCode() ? "여성 " : "") +
                                    categoryDetail.getCategory().getCategoryName());
                            Globals.categoryFilterName = categoryDetail.getCategory().getCategoryName();
                        }
                    }

                } else {
                    Globals.categoryFilterName = Globals.gKisCategoryName;
                    tv_title.setText(Globals.gKisCategoryName);
                }

                if (response.getChildrenCategories().size() == 0 && response.getRecommendedBrand().size() == 0) {
                    rly_no_data.setVisibility(View.VISIBLE);
                    vp_main.setVisibility(View.GONE);
                } else {
                    rly_no_data.setVisibility(View.GONE);
                    vp_main.setVisibility(View.VISIBLE);
                }

                if (categoryDetail.getChildrenCategories().size() == 0) {
                    isFinalCategory = true;
                    tl_top_tab.setVisibility(View.GONE);
                    vp_main.removeView(vDetail);
                } else {
                    isFinalCategory = false;
                    tl_top_tab.setVisibility(View.VISIBLE);
                }

                pagerAdapter.notifyDataSetChanged();
                vp_main.setOffscreenPageLimit(isFinalCategory ? 1 : 2);

                addPopularBrands();
                addDetailCategories();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
