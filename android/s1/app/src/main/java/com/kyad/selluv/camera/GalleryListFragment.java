/**
 * 홈
 */
package com.kyad.selluv.camera;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.adapter.GalleryListAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.camera.GalleryFragment.galleryList;
import static com.kyad.selluv.camera.GalleryFragment.selectedList;


public class GalleryListFragment extends DialogFragment implements GalleryListAdapter.onDirClickListener {

    //UI Refs
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.rv)
    RecyclerView recyclerView;

    //Vars
    private View rootView;
    public GalleryFragment gallery;
    loadImagesTask loadTask = new loadImagesTask();


    @OnClick(R.id.rl_gallery_list)
    void onGalleryList(View v) {
        dismiss();
    }

    ArrayList<String> all = new ArrayList<>();
    ArrayList<String> cameraA = new ArrayList<>();
    ArrayList<String> screenshot = new ArrayList<>();
    ArrayList<String> download = new ArrayList<>();
    ArrayList<String> sellca = new ArrayList<>();

    ArrayList<GalleryListAdapter.DirItem> listItems = new ArrayList<>();

    GalleryListAdapter.DirItem dirAll;
    GalleryListAdapter.DirItem dirCamera;
    GalleryListAdapter.DirItem dirScreenShot;
    GalleryListAdapter.DirItem dirDownloads;
    GalleryListAdapter.DirItem dirSellca;

    List[] list = {cameraA, all, screenshot, download, sellca};
    Date now = new Date();

    public static int recentThreshold = 1000 * 60 * 60 * 24; //1 day

    public GalleryListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gallery_list, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        loadData();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void loadLayout() {
        tv_title.setText(galleryList[selectedList]);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    public void walkdir(File dir, ArrayList<String> arrayList) {

        File[] listFile;
        listFile = dir.listFiles();

        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    walkdir(listFile[i], arrayList);
                } else {
                    if (listFile[i].getName().toLowerCase().endsWith(".jpg") || listFile[i].getName().toLowerCase().endsWith(".png")) {
                        arrayList.add(listFile[i].getAbsolutePath());
                        if (arrayList == cameraA) {
                            dirCamera.cnt = arrayList.size();
                            dirCamera.thumb = arrayList.get(0);
                        }
                        if (arrayList == screenshot) {
                            dirScreenShot.cnt = arrayList.size();
                            dirScreenShot.thumb = arrayList.get(0);
                        }
                        if (arrayList == download) {
                            dirDownloads.cnt = arrayList.size();
                            dirDownloads.thumb = arrayList.get(0);
                        }
                        if (arrayList == sellca) {
                            dirSellca.cnt = arrayList.size();
                            dirSellca.thumb = arrayList.get(0);
                        }
                        loadTask.doProgress();
                    }
                }
            }
        }
    }

    public void findAll(File dir) {

        File[] listFile;
        listFile = dir.listFiles();

        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    findAll(listFile[i]);
                } else {
                    if (listFile[i].getName().toLowerCase().endsWith(".jpg") || listFile[i].getName().toLowerCase().endsWith(".png")) {

                        if (now.getTime() - listFile[i].lastModified() < recentThreshold) {
                            all.add(listFile[i].getAbsolutePath());

                            dirAll.cnt = all.size();
                            dirAll.thumb = all.get(0);
                        }
                        loadTask.doProgress();
                    }
                }
            }
        }
    }

    public void loadData() {

        loadTask.execute();

        dirAll = all.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[0], 0) :
                new GalleryListAdapter.DirItem(all.get(0), galleryList[0], all.size());
        listItems.add(dirAll);

        dirCamera = cameraA.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[1], 0) :
                new GalleryListAdapter.DirItem(cameraA.get(0), galleryList[1], cameraA.size());
        listItems.add(dirCamera);

        dirScreenShot = screenshot.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[2], 0) :
                new GalleryListAdapter.DirItem(screenshot.get(0), galleryList[2], screenshot.size());
        listItems.add(dirScreenShot);

        dirDownloads = download.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[3], 0) :
                new GalleryListAdapter.DirItem(download.get(0), galleryList[3], download.size());
        listItems.add(dirDownloads);

        dirSellca = sellca.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[4], 0) :
                new GalleryListAdapter.DirItem(sellca.get(0), galleryList[4], sellca.size());
        listItems.add(dirSellca);

        GalleryListAdapter adapter = new GalleryListAdapter(getContext(), listItems);
        adapter.setDirClickListener(this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void ScanFiles() {
        findAll(Environment.getExternalStorageDirectory());
        walkdir(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), cameraA);
        walkdir(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), screenshot);
        walkdir(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), download);
        walkdir(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), sellca);
    }

    @Override
    public void onDirClick(RecyclerView.ViewHolder holder, int position) {
        selectedList = position;
        gallery.reloadData((ArrayList<String>) list[position]);
        loadTask.cancel(true);
        dismiss();
    }

    public class loadImagesTask extends AsyncTask<File, String, Integer> {
        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            recyclerView.getAdapter().notifyDataSetChanged();
        }

        @Override
        protected Integer doInBackground(File... files) {
            ScanFiles();
            return 0;
        }

        public void doProgress() {
            publishProgress();
        }

    }

}
