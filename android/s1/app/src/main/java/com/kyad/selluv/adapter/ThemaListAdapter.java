package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.api.dto.theme.ThemeListDto;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.kyad.selluv.common.expandablerecyclerview.models.ExpandableGroup;
import com.kyad.selluv.common.viewholder.ThemaGroupViewHolder;
import com.kyad.selluv.common.viewholder.ThemaItemViewHolder;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.shop.ShopMainFragment;
import com.kyad.selluv.shop.ShopShopperFragment;

import java.util.List;


public class ThemaListAdapter extends ExpandableRecyclerViewAdapter<ThemaGroupViewHolder, ThemaItemViewHolder> {

    ShopShopperFragment fragment;

    public ThemaListAdapter(ShopShopperFragment fragment, List<? extends ExpandableGroup> groups) {
        super(groups);
        this.fragment = fragment;
    }

    @Override
    public ThemaGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_thema, parent, false);
        return new ThemaGroupViewHolder(view);
    }

    @Override
    public ThemaItemViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_thema_ware, parent, false);
        return new ThemaItemViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(ThemaItemViewHolder holder, int flatPosition,
                                      final ExpandableGroup group, int childIndex) {

        final PdtListDto themaItem = ((ThemeListDto) group).items.get(childIndex);
        holder.tv_ware_name.setText(themaItem.getWareName());
        holder.tv_ware_eng_name.setText(themaItem.getBrand().getNameEn());
        holder.tv_ware_price.setText("\u20a9" + Utils.getAttachCommaFormat((int) themaItem.getPdt().getPrice()));

        ImageUtils.load(holder.iv_ware.getContext(), themaItem.getPdt().getProfileImg(), holder.iv_ware);
        if (flatPosition % 2 == 1) {
            holder.rl_thema_ware.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else
            holder.rl_thema_ware.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        holder.iv_ware.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ViewPosition position = ViewPosition.from(view);
                Intent intent = new Intent(fragment.getActivity(), DetailActivity.class);
                intent.putExtra(DetailActivity.PDT_UID, themaItem.getPdt().getPdtUid());
                fragment.getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(final ThemaGroupViewHolder holder, final int flatPosition,
                                      final ExpandableGroup group) {

        ThemeListDto themeListDto =  (ThemeListDto)group;
        final int themeUid = themeListDto.getTheme().getThemeUid();
        holder.tv_thema_title.setText(themeListDto.getTheme().getTitleKo());
        holder.tv_item_cnt.setText(String.valueOf(themeListDto.getPdtCount()) + " Items");
        ImageUtils.load(holder.iv_thema.getContext(), themeListDto.getTheme().getProfileImg(), holder.iv_thema);

        if (isGroupExpanded(group)) {
            holder.tv_item_cnt.setVisibility(View.GONE);
            holder.ib_thema_close.setVisibility(View.VISIBLE);
            holder.ib_thema_more.setVisibility(View.VISIBLE);
            holder.iv_thema.setColorFilter(Utils.currentActivity.getResources().getColor(R.color.color_000000_30), PorterDuff.Mode.SRC_OVER);
            holder.ib_thema_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ShopMainFragment) fragment.mainFragment).gotoThemaFragment(themeUid);
                }
            });
            holder.ib_thema_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleGroup(flatPosition);
                }
            });
        }
        if (!isGroupExpanded(group)) {
            holder.ib_thema_close.setVisibility(View.GONE);
            holder.ib_thema_more.setVisibility(View.GONE);
            holder.tv_item_cnt.setVisibility(View.VISIBLE);
            holder.iv_thema.setColorFilter(Color.TRANSPARENT);
        }
    }
}
