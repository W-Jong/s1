package com.kyad.selluv.api.dto.category;

import lombok.Data;

@Data
public class CategoryMiniDto {
    //("카테고리UID")
    private int categoryUid;
    //("뎁스")
    private int depth;
    //("카테고리이름")
    private String categoryName;
}
