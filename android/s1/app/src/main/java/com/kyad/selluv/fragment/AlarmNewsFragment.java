/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dao.UsrPushSettingDao;
import com.kyad.selluv.mypage.AlarmSettingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmNewsFragment extends BaseFragment {

    @BindView(R.id.btn_switch)
    ToggleButton btn_switch;

    @BindView(R.id.tb_toggle1)
    Button tb_toggle1;
    @BindView(R.id.tb_toggle2)
    Button tb_toggle2;
    @BindView(R.id.tb_toggle3)
    Button tb_toggle3;
    @BindView(R.id.tb_toggle4)
    Button tb_toggle4;
    @BindView(R.id.tb_toggle5)
    Button tb_toggle5;
    @BindView(R.id.tb_toggle6)
    Button tb_toggle6;
    @BindView(R.id.tb_toggle7)
    Button tb_toggle7;
    @BindView(R.id.tb_toggle8)
    Button tb_toggle8;
    @BindView(R.id.tb_toggle9)
    Button tb_toggle9;
    @BindView(R.id.tb_toggle10)
    Button tb_toggle10;
    @BindView(R.id.tb_toggle11)
    Button tb_toggle11;
    @BindView(R.id.tb_toggle12)
    Button tb_toggle12;

    public AlarmNewsFragment() {
    }

    //UI Referencs
    private UsrPushSettingDao dicSettingInfo;

    public void setAlarmInfo(UsrPushSettingDao dicInfo) {
        dicSettingInfo = dicInfo;
        String dealSetting = dicSettingInfo.getNewsSetting();
        for (int i = 0 ; i < 12 ; i ++){
            String value = dealSetting.substring(i, i + 1);
            setAlarmStatus(i, Integer.valueOf(value));
        }
    }
    //Vars
    View rootView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_alarmset_news, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;

    }

    private void loadLayout() {


    }
    private void setAlarmStatus(int index, int status){
        if (index == 0){
            if (status == 1) {
                tb_toggle1.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[0] = 1;
            } else {
                tb_toggle1.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[0] = 0;
            }
        } else if (index == 1){
            if (status == 1) {
                tb_toggle2.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[1] = 1;
            } else {
                tb_toggle2.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[1] = 0;
            }
        } else if (index == 2){
            if (status == 1) {
                tb_toggle3.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[2] = 1;
            } else {
                tb_toggle3.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[2] = 0;
            }
        } else if (index == 3){
            if (status == 1) {
                tb_toggle4.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[3] = 1;
            } else {
                tb_toggle4.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[3] = 0;
            }
        } else if (index == 4){
            if (status == 1) {
                tb_toggle5.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[4] = 1;
            } else {
                tb_toggle5.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[4] = 0;
            }
        } else if (index == 5){
            if (status == 1) {
                tb_toggle6.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[5] = 1;
            } else {
                tb_toggle6.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[5] = 0;
            }
        } else if (index == 6){
            if (status == 1) {
                tb_toggle7.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[6] = 1;
            } else {
                tb_toggle7.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[6] = 0;
            }
        } else if (index == 7){
            if (status == 1) {
                tb_toggle8.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[7] = 1;
            } else {
                tb_toggle8.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[7] = 0;
            }
        } else if (index == 8){
            if (status == 1) {
                tb_toggle9.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[8] = 1;
            } else {
                tb_toggle9.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[8] = 1;
            }
        } else if (index == 9){
            if (status == 1) {
                tb_toggle10.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[9] = 1;
            } else {
                tb_toggle10.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[9] = 0;
            }
        } else if (index == 10){
            if (status == 1) {
                tb_toggle11.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[8] = 1;
            } else {
                tb_toggle11.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[8] = 1;
            }
        } else if (index == 11){
            if (status == 1) {
                tb_toggle12.setSelected(true);
                ((AlarmSettingActivity)getActivity()).newsAlarm[9] = 1;
            } else {
                tb_toggle12.setSelected(false);
                ((AlarmSettingActivity)getActivity()).newsAlarm[9] = 0;
            }
        }


        if (tb_toggle1.isSelected() && tb_toggle2.isSelected() && tb_toggle3.isSelected() &&
                tb_toggle4.isSelected() && tb_toggle5.isSelected() && tb_toggle6.isSelected() &&
                tb_toggle7.isSelected() && tb_toggle8.isSelected() && tb_toggle9.isSelected() && tb_toggle10.isSelected()
                && tb_toggle11.isSelected() && tb_toggle12.isSelected()){
            btn_switch.setChecked(true);
            ((AlarmSettingActivity)getActivity()).newsYn = 1;
        } else {
            btn_switch.setChecked(false);
            ((AlarmSettingActivity)getActivity()).newsYn = 0;
        }

        //click
        btn_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0 ; i < 10 ; i++){
                    if (btn_switch.isSelected()) {
                        ((AlarmSettingActivity) getActivity()).newsAlarm[i] = 0;
                    } else {
                        ((AlarmSettingActivity) getActivity()).newsAlarm[i] = 1;
                    }
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle1.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[0] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[0] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle2.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[1] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[1] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle3.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[2] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[2] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle4.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[3] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[3] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle5.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[4] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[4] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle6.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[5] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[5] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle7.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[6] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[6] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle8.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[7] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[7] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle9.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[8] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[8] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle10.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[9] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[9] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle11.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[10] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[10] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle12.isSelected()){
                    ((AlarmSettingActivity)getActivity()).newsAlarm[11] = 0;
                } else {
                    ((AlarmSettingActivity)getActivity()).newsAlarm[11] = 1;
                }
                ((AlarmSettingActivity)getActivity()).reqAlarmSetting();
            }
        });
    }

}
