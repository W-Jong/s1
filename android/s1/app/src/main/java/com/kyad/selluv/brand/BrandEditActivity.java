package com.kyad.selluv.brand;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.BrandFollowEditListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.api.request.MultiUidsReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.DragableListView;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class BrandEditActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lv_filter)
    DragableListView lv_filter;

    //Variables
    BrandFollowEditListAdapter adapter = null;
    boolean editable = false;
    private List<BrandListDto> brandListDtoList;

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
//        if (!editable)
//            saveBrand();
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.btn_add)
    void onAdd(View v) {
        startActivity(BrandAddActivity.class, false, 0, 0);
    }

    @OnClick(R.id.tv_edit)
    void onEdit(View v) {
        editable = !editable;
        adapter.setEditable(editable);
        if (!editable) {
            saveBrand();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_edit);
        setStatusBarWhite();
        loadLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFollowBrandList();
    }

    private void loadLayout() {
        adapter = new BrandFollowEditListAdapter(this);
        lv_filter.setAdapter(adapter);
        lv_filter.setClickable(true);
        lv_filter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(BrandEditActivity.this, BrandDetailActivity.class);
                intent.putExtra(BrandDetailActivity.BRAND_UID, brandListDtoList.get(i).getBrandUid());
                startActivity(intent);
            }
        });
    }

    void loadData() {
        adapter.setData(brandListDtoList);
        lv_filter.setDragableList(brandListDtoList);
        adapter.notifyDataSetChanged();
    }

    void saveBrand() {
        if (adapter != null)
            unLikeBrandList(adapter.deletedIndex);
    }


    private void getFollowBrandList() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<BrandListDto>>> genRes = restAPI.brandFollowList(Globals.userToken);

        genRes.enqueue(new TokenCallback<List<BrandListDto>>(this) {
            @Override
            public void onSuccess(List<BrandListDto> response) {
                closeLoading();
                brandListDtoList = response;
                loadData();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void unLikeBrandList(List<Integer> unLikeBrandUidList) {

        if (unLikeBrandUidList.size() < 1) {
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        MultiUidsReq multiUidsReq = new MultiUidsReq(unLikeBrandUidList);
        Call<GenericResponse<Void>> genRes = restAPI.brandUnLikes(Globals.userToken, multiUidsReq);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                onResume();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
