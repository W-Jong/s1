package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.QaListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.FaqDao;
import com.kyad.selluv.api.dao.QnaDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.ContentReq;
import com.kyad.selluv.api.request.QnaReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class QaActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lv_my_qa)
    ListView lv_my_qa;

    private EditText edt_title;
    private EditText edt_content;
    private Button btn_register;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    QaListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_qa);
        setStatusBarWhite();
        loadLayout();
        getQnaList();
    }

    private void loadLayout() {
        LayoutInflater inflater = LayoutInflater.from(this);
        listAdapter = new QaListAdapter(this);
        lv_my_qa.addHeaderView(inflater.inflate(R.layout.header_mypage_qa, null));
        lv_my_qa.setAdapter(listAdapter);
//        Collections.addAll(listAdapter.arrData, Constants.qaItems);
//        listAdapter.notifyDataSetChanged();
        edt_title = (EditText)lv_my_qa.findViewById(R.id.edt_title);
        edt_content = (EditText)lv_my_qa.findViewById(R.id.edt_content);
        btn_register = (Button)lv_my_qa.findViewById(R.id.btn_register);
        btn_register.setEnabled(false);

        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        edt_title.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") ) {
                    //do your work here
                    if (!edt_title.getText().toString().equals("") && !edt_content.getText().toString().equals("")){
                        btn_register.setEnabled(true);
                    } else {
                        btn_register.setEnabled(false);
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        edt_content.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.equals("") ) {
                    //do your work here
                    if (!edt_title.getText().toString().equals("") && !edt_content.getText().toString().equals("")){
                        btn_register.setEnabled(true);
                    } else {
                        btn_register.setEnabled(false);
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqQna();
            }
        });
    }

    //1:1문의작성
    private void reqQna(){
        QnaReq dicInfo = new QnaReq();
        dicInfo.setContent(edt_content.getText().toString());
        dicInfo.setTitle(edt_title.getText().toString());

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.reqQna(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {

                edt_title.setText("");
                edt_content.setText("");
                Utils.showToast(QaActivity.this, getString(R.string.qna_reg_success));

                getQnaList();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //1:1문의 목록 얻기
    private void getQnaList(){
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<QnaDao>>> genRes = restAPI.getQnaList(Globals.userToken);

        genRes.enqueue(new TokenCallback<List<QnaDao>>(this) {
            @Override
            public void onSuccess(List<QnaDao> response) {
                closeLoading();

                listAdapter.arrData = response;
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }
}
