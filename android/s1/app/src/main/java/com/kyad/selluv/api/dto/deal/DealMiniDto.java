package com.kyad.selluv.api.dto.deal;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class DealMiniDto {
    //("거래UID")
    private int dealUid;
    //("추가된 시간")
    private Timestamp regTime;
    //("상품UID")
    private int pdtUid;
    //("구매자 회원UID")
    private int usrUid;
    //("판매자 회원UID")
    private int sellerUsrUid;
    //("상품 영문브랜드")
    private String brandEn;
    //("상품 타이틀")
    private String pdtTitle;
    //("상품 사이즈")
    private String pdtSize;
    //("상품이미지")
    private String pdtImg;
    //("상품가격")
    private long pdtPrice;
    //("배송비")
    private long sendPrice;
    //("정품인증가격, 0이면 정품인증요청안함")
    private long verifyPrice;
    //("결제금액")
    private long price;
    //("정산금액")
    private long cashPrice;
    //("결제시간")
    private Timestamp payTime;
    //("배송완료시간")
    private Timestamp deliveryTime;
    //("거래완료시간")
    private Timestamp compTime;
    //("정산완료시간")
    private Timestamp cashCompTime;
    //("네고 및 카운터네고 제안금액")
    private long reqPrice;
    //("거래상태 0-삭제 1-배송준비 2-배송진행 3-정품인증 4-배송완료 5-거래완료 " +
    //"6-정산완료 7-반품접수 8-반품승인 9-반품완료 10-주문취소 11-네고제안 12-카운터네고제안")
    private int status;
}
