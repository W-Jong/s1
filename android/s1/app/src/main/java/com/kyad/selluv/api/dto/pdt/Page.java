package com.kyad.selluv.api.dto.pdt;

import com.kyad.selluv.api.dto.deal.Sort;

import java.util.List;

import lombok.Data;

@Data
public class Page<T> {
    private List<T> content;
    private boolean first;
    private boolean last;
    private int number;
    private int numberOfElements;
    private int size;
    private Sort sort;
    private int totalElements;
    private int totalPages;
}
