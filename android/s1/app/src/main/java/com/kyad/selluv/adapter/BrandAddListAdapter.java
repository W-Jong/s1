package com.kyad.selluv.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.brand.BrandDetailDto;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.common.AlphabetIndexer.util.HanziToPingyin;
import com.kyad.selluv.common.AlphabetIndexer.util.StringMatcher;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.PinnedSectionListView;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kyad.selluv.common.AlphabetIndexer.util.StringMatcher.isKorean;

public class BrandAddListAdapter extends BaseAdapter implements SectionIndexer, PinnedSectionListView.PinnedSectionListAdapter {

    LayoutInflater inflater;
    public List<BrandListDto> arrData = null;
    BaseActivity activity = null;
    int selectedLang = 0;
    int selectedPos = 0;
    ArrayList<String> names = new ArrayList<>();

    private int TYPE_ITEM = 0;
    private int TYPE_SECTION = 1;

    private final int SECTION_POPULAR_CITY = 0;

    private List<String> mSections = new ArrayList<String>();
    private List<Item> mItems = new ArrayList<Item>();
    private Map<String, Character> mCharMap = new HashMap<String, Character>();
    public Map<Integer, Boolean> modifiedIndex = new HashMap<>();

    public BrandAddListAdapter(BaseActivity activity, int selectedLang) {
        this.activity = activity;
        this.selectedLang = selectedLang;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(List<BrandListDto> addData) {
        arrData = addData;
        modifiedIndex.clear();
        reloadData(selectedLang);
    }

    public void reloadData(int selectedLang) {
        names.clear();
        mSections.clear();
        mItems.clear();
        if (arrData == null) {
            return;
        }
        this.selectedLang = selectedLang;
//        Model.BrandItemComparator comparator = new Model.BrandItemComparator(selectedLang);
//        Collections.sort(arrData, comparator);
        for (BrandListDto item : arrData) {
            if (selectedLang == 0)
                names.add(item.getNameEn());
            else
                names.add(item.getNameKo());
        }

        mCharMap.clear();
        mCharMap = buildAlphabetMap(arrData);

        if (selectedLang == 0) {
            for (char s :
                    Constants.en.toUpperCase().toCharArray()) {
                mSections.add(String.valueOf(s));
            }
        } else {
            for (char s :
                    Constants.kr.toCharArray()) {
                mSections.add(String.valueOf(s));
            }
        }
        char prevChar = '.';
        int offset = 0;
        int orgOffset = -1;
        for (int i = 0; i < names.size(); i++) {
            String name = names.get(i);
            char fc = lookupFirstChar(name);
            if (isKorean(fc)) {
                for (; offset < Constants.krToCompare.length() - 1; offset++) {
                    char startCh = Constants.krToCompare.charAt(offset);
                    char endCh = Constants.krToCompare.charAt(offset + 1);
                    if (startCh <= fc && fc < endCh) {
                        break;
                    }
                }
                if (offset != orgOffset) {
                    Item item = new Item();
                    item.type = TYPE_SECTION;
                    item.text = String.valueOf(Constants.kr.charAt(offset));
                    mItems.add(item);
                    orgOffset = offset;
                }
            } else {
                if (fc != prevChar) {
                    prevChar = fc;

                    Item item = new Item();
                    item.type = TYPE_SECTION;
                    item.text = String.valueOf(fc);
                    mItems.add(item);
                }
            }

            Item item = new BrandAddListAdapter.Item();
            item.type = TYPE_ITEM;
            item.text = name;
            item.brandItem = arrData.get(i);
            mItems.add(item);
        }
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        if (mItems == null) {
            return 0;
        } else {
            return mItems.size();
        }
    }

    @Override
    public int getPositionForSection(int section) {

        if (section == SECTION_POPULAR_CITY) {
            return 0;
        }

        // If there is no item for current section, previous section will be selected
        for (int i = section; i >= 1; i--) {
            for (int j = 0; j < getCount(); j++) {
                if (i == 0) {
                    // For numeric section
                    for (int k = 0; k <= 9; k++) {
                        if (StringMatcher.match(mItems.get(j).text, String.valueOf(k))) {
                            return j - 1;
                        }
                    }
                } else {
                    char fc = lookupFirstChar(((BrandAddListAdapter.Item) getItem(j)).text);
                    if (StringMatcher.match(String.valueOf(fc), mSections.get(i))) {
                        return j - 1;
                    }
                }
            }
        }
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSections.size(); i++) {
            char fc = lookupFirstChar(mItems.get(position).text);
            if (mSections.get(i).equals(String.valueOf(fc))) {
                return i;
            }
        }

        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;

        int type = getItemViewType(position);
        final Item anItem = (Item) getItem(position);

        if (isItemViewTypePinned(type)) {
            view = LayoutInflater.from(activity).inflate(
                    R.layout.item_brand_follow_header, parent, false);

            TextView tv_alphabet = (TextView) view.findViewById(R.id.tv_alphabet);
            tv_alphabet.setText(anItem.text);

        } else {
            view = LayoutInflater.from(activity).inflate(
                    R.layout.item_brand_follow_add, parent, false);

            TextView tv_brand = (TextView) view.findViewById(R.id.tv_brand);
            tv_brand.setText(selectedLang == 0 ? anItem.brandItem.getNameEn() : anItem.brandItem.getNameKo());
            TextView tv_brand_other = (TextView) view.findViewById(R.id.tv_brand_other);
            tv_brand_other.setText(selectedLang == 0 ? anItem.brandItem.getNameKo() : anItem.brandItem.getNameEn());
            CheckBox cb_select = (CheckBox) view.findViewById(R.id.cb_select);
            cb_select.setChecked(anItem.brandItem.getBrandLikeStatus());
            cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    anItem.brandItem.setBrandLikeStatus(b);
                    if (modifiedIndex.get(anItem.brandItem.getBrandUid()) != null) {
                        modifiedIndex.remove(anItem.brandItem.getBrandUid());
                    } else {
                        modifiedIndex.put(anItem.brandItem.getBrandUid(), b);
                    }

                    Button btn_ok = (Button) activity.findViewById(R.id.btn_ok);
                    if (modifiedIndex.size() > 0) {
                        btn_ok.setVisibility(View.VISIBLE);
                    } else {
                        btn_ok.setVisibility(View.GONE);
                    }
                }
            });

        }

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).type;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean isItemViewTypePinned(int type) {
        return type == TYPE_SECTION;
    }

    public boolean isPinned(int pos) {
        return getItemViewType(pos) == TYPE_SECTION;
    }

    @Override
    public Object[] getSections() {
        String[] strs = new String[mSections.size()];
        for (int i = 0; i < mSections.size(); i++) {
            strs[i] = mSections.get(i);
        }
        return strs;
    }

    private char lookupFirstChar(String str) {
        if (mCharMap != null) {
            Character co = mCharMap.get(str);
            if (co == null) {
                return '.';
            } else {
                return co;
            }
        } else {
            return '.';
        }
    }

    private char parseFirstChar(String str) {
        char fc = str.charAt(0);
        if (fc <= 128 && fc >= 0) {
            return fc;
        } else {
            return HanziToPingyin.getFirstPinYinChar(str);
        }
    }

    private Map<String, Character> buildAlphabetMap(List<BrandListDto> listDtos) {
        Map<String, Character> map = new HashMap<String, Character>();
        for (BrandListDto item : listDtos) {
            if (selectedLang == 0)
                map.put(item.getNameEn(), item.getFirstEn().toUpperCase().charAt(0));
            else
                map.put(item.getNameKo(), Character.toUpperCase(item.getFirstKo().charAt(0)));
        }
        return map;
    }

    public static class Item {
        public int type;
        public String text;
        public BrandListDto brandItem;
    }
}
