/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.TagSearchAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SellTagFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.txt_tag_input)
    EditText et_tag_input;
    @BindView(R.id.lv_filter)
    ListView lv_filter;
    @BindView(R.id.iv_edit)
    ImageButton iv_edit;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;
    TagSearchAdapter adapter;
    List<String> tagItems = new ArrayList<>();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_info_tag, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onDone(View v) {
        String tags = et_tag_input.getText().toString().trim();
        if (tags.length() < 2)
            tags = "";
        else if (tags.substring(tags.length() - 2).equals(" #"))
            tags = tags.substring(0, tags.length() - 2);

        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setTag(tags);
        } else {
            Globals.pdtCreateReq.setTag(tags);
        }

        ((TextView) getActivity().findViewById(R.id.tv_tagadd_value)).setText(tags);
        getActivity().onBackPressed();
    }

    private void loadLayout() {
        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.frag_main));

        String s_tag = "";
        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            iv_edit.setVisibility(View.GONE);
            s_tag = Globals.pdtUpdateReq.getTag();
        } else {
            tv_finish.setVisibility(View.GONE);
            iv_edit.setVisibility(View.VISIBLE);
            s_tag = Globals.pdtCreateReq.getTag();
        }
        if (s_tag.length() > 0) {
            et_tag_input.setText(s_tag);
        }

        adapter = new TagSearchAdapter(this);
        lv_filter.setAdapter(adapter);
        et_tag_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (et_tag_input.getText().toString().length() < 1) {
                        et_tag_input.setText("#");
                    }
                }
            }
        });
        et_tag_input.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    return true;
                } else if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    String ss = et_tag_input.getText().toString();
//                    if (ss.length() < 2) {
//                        return true;
//                    }
                    if (ss.length() > 2 && ss.substring(ss.length() - 1).equals("#")) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            et_tag_input.setText(ss.substring(0, ss.length() - 2));
                            et_tag_input.setSelection(ss.length() - 2);
                        }
                        return true;
                    }
                }
                return false;
            }
        });
//        et_tag_input.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
//
//            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//                return false;
//            }
//
//            public void onDestroyActionMode(ActionMode mode) {
//            }
//
//            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//                return false;
//            }
//
//            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//                return false;
//            }
//        });
        et_tag_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("onTextChanged", charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String ss = editable.toString();
                if (ss.length() > 1 && ss.substring(ss.length() - 1).equals("#") && ss.substring(ss.length() - 2, ss.length() - 1).equals("#")) {
                    et_tag_input.setText(ss.substring(0, ss.length() - 1));
                    et_tag_input.setSelection(ss.length() - 1);
                } else if (ss.length() > 1 && ss.substring(ss.length() - 1).equals(" ") && ss.substring(ss.length() - 2, ss.length() - 1).equals("#")) {
                    et_tag_input.setText(ss.substring(0, ss.length() - 1));
                    et_tag_input.setSelection(ss.length() - 1);
                } else if (ss.length() > 0 && ss.substring(ss.length() - 1).equals(" ")) {
                    et_tag_input.getText().append("#");
//                    search();
                } else {
                    search();
                }
            }
        });
    }

    private void search() {

        ((BaseActivity) getActivity()).showLoading();

        String[] tags = et_tag_input.getText().toString().split(" ");
        if (tags == null || tags.length < 1) {
            tagItems.clear();
            adapter.setData(tagItems);
            adapter.notifyDataSetChanged();
            ((BaseActivity) getActivity()).closeLoading();
            return;
        }

        String keyword = tags[tags.length - 1].replace("#", "");
        if (keyword.isEmpty()) {
            tagItems.clear();
            adapter.setData(tagItems);
            adapter.notifyDataSetChanged();
            ((BaseActivity) getActivity()).closeLoading();
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<String>>> genRes = restAPI.searchTag(Globals.userToken, keyword);
        genRes.enqueue(new Callback<GenericResponse<List<String>>>() {

            @Override
            public void onResponse(Call<GenericResponse<List<String>>> call, Response<GenericResponse<List<String>>> response) {
                ((BaseActivity) getActivity()).closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    tagItems.clear();
                    tagItems = response.body().getData();
                    adapter.setData(tagItems);
                    adapter.notifyDataSetChanged();
                } else {//실패
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<List<String>>> call, Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onClickTag(String tag) {
        String str = et_tag_input.getText().toString();
        int lastShap = str.lastIndexOf("#");
        String newStr = "#" + tag;

        if (lastShap > 0) {
            newStr = str.substring(0, lastShap) + "#" + tag;
        }

        et_tag_input.setText(newStr);
        et_tag_input.setSelection(newStr.length());
    }
}
