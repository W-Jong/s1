package com.kyad.selluv.api.dto.pdt;

import lombok.Data;

@Data
public class PdtFakeInfoDto {
    //("가품신고 누적수")
    private int totalCount;
    //("가품신고 리워드 금액")
    private long reward;
}
