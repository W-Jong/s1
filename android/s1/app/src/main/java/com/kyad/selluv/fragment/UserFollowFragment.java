/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.FollowListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class UserFollowFragment extends BaseFragment {

    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;
    private int usrUid = 0;


    public UserFollowFragment() {
    }

    public void setTitle(int title) {
        this.title = title;
    }

    protected boolean isAllowAnimation() {
        return false;
    }

    public void setTab(int index, int _uid) {
        usrUid = _uid;
        currentPage = 0;
        isLast = false;
        isLoading = false;

        if (index == 0) {
            getFollowerList();
        } else {
            getFollowintList();
        }
    }

    //UI Referencs
    @BindView(R.id.tv_deal)
    TextView tv_deal;
    @BindView(R.id.tv_count)
    TextView tv_count;
    @BindView(R.id.lv_list)
    ListView lv_list;


    //Vars
    View rootView;
    int title = 0;
    private FollowListAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, rootView);
        adapter = new FollowListAdapter((BaseActivity) getActivity());
        if (title != 0)
            tv_deal.setText(title);
        loadLayout();
        loadData();
        return rootView;

    }

    private void loadLayout() {
        lv_list.setAdapter(adapter);
        lv_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    //getUsrWishList(currentPage);
                }
            }
        });
    }

    private void loadData() {
        if (adapter != null) {
            //TODO: LikeActiviy 참고 할것
//            Collections.addAll(adapter.arrData, loadInterface.loadData());
            adapter.notifyDataSetChanged();
        }

    }

//    public void setLoadInterface(DataLoadInterface loadInterface) {
//        this.loadInterface = loadInterface;
//    }

    private void getFollowerList() {

        if (isLoading || isLast)
            return;

        isLoading = true;

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Page<UsrFollowListDto>>> genRes = restAPI.getFollowerList(Globals.userToken, usrUid, currentPage);

        genRes.enqueue(new TokenCallback<Page<UsrFollowListDto>>(getActivity()) {
            @Override
            public void onSuccess(Page<UsrFollowListDto> response) {
                closeLoading();

                isLoading = false;

                if (currentPage == 0) {
                    adapter.arrData.clear();
                }

                tv_count.setText("(" + response.getTotalElements() + ")");

                isLast = response.isLast();
                adapter.arrData.addAll(response.getContent());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }

    private void getFollowintList() {

        if (isLoading || isLast)
            return;

        isLoading = true;

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Page<UsrFollowListDto>>> genRes = restAPI.getFollowingList(Globals.userToken, usrUid, currentPage);

        genRes.enqueue(new TokenCallback<Page<UsrFollowListDto>>(getActivity()) {
            @Override
            public void onSuccess(Page<UsrFollowListDto> response) {
                closeLoading();

                isLoading = false;

                if (currentPage == 0) {
                    adapter.arrData.clear();
                }
                tv_count.setText("(" + response.getTotalElements() + ")");

                isLast = response.isLast();
                adapter.arrData.addAll(response.getContent());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }

}
