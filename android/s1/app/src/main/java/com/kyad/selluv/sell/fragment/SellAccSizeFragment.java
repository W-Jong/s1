/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SellAccSizeFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.txt_direct_size_value)
    EditText txt_direct_size_value;
    @BindView(R.id.btn_free_size)
    ImageButton btn_free_size;
    @BindView(R.id.btn_direct_input)
    ImageButton btn_direct_input;
    @BindView(R.id.iv_edit)
    ImageButton iv_edit;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;

    private String s_size = "";


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_info_accsize, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onDone(View v) {

        s_size = "";
        if (btn_free_size.isSelected()) {
            s_size = getString(R.string.free_en);
        }
        if (btn_direct_input.isSelected()) {
            s_size = txt_direct_size_value.getText().toString();
        }

        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtSize(s_size);
        } else {
            Globals.pdtCreateReq.setPdtSize(s_size);
        }
        ((TextView) getActivity().findViewById(R.id.tv_item_size_value)).setText(s_size);

        goBack();
    }

    @OnClick(R.id.btn_free_size)
    void onFreeSize(View v) {
        btn_free_size.setBackgroundResource(R.drawable.sell_direct_free_size_on);
        btn_free_size.setSelected(true);
        btn_direct_input.setBackgroundResource(R.drawable.sell_direct_size_input_off);
        btn_direct_input.setSelected(false);

        txt_direct_size_value.clearFocus();

        s_size = getString(R.string.free_en);
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtSize(s_size);
        } else {
            Globals.pdtCreateReq.setPdtSize(s_size);
        }
        ((TextView) getActivity().findViewById(R.id.tv_item_size_value)).setText(s_size);

        goBack();
    }

    @OnClick(R.id.btn_direct_input)
    void onDirectInput(View v) {
        txt_direct_size_value.requestFocus();
//        btn_free_size.setBackgroundResource(R.drawable.sell_direct_free_size_off);
//        btn_direct_input.setBackgroundResource(R.drawable.sell_direct_size_input_on);

    }

    private void loadLayout() {
        txt_direct_size_value.clearFocus();
        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.frag_main));

        txt_direct_size_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txt_direct_size_value.getText().toString().isEmpty()) {
                    btn_direct_input.setBackgroundResource(R.drawable.sell_direct_size_input_off);
                    btn_direct_input.setSelected(false);
                } else {
                    btn_free_size.setBackgroundResource(R.drawable.sell_direct_free_size_off);
                    btn_free_size.setSelected(false);
                    btn_direct_input.setBackgroundResource(R.drawable.sell_direct_size_input_on);
                    btn_direct_input.setSelected(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            iv_edit.setVisibility(View.GONE);
            s_size = Globals.pdtUpdateReq.getPdtSize();
        } else {
            tv_finish.setVisibility(View.GONE);
            iv_edit.setVisibility(View.VISIBLE);
            s_size = Globals.pdtCreateReq.getPdtSize();
        }

        if (s_size.equals("FREE")) {
            btn_free_size.setBackgroundResource(R.drawable.sell_direct_free_size_on);
            btn_free_size.setSelected(true);
            btn_direct_input.setBackgroundResource(R.drawable.sell_direct_size_input_off);
            btn_direct_input.setSelected(false);
        } else {
            btn_free_size.setBackgroundResource(R.drawable.sell_direct_free_size_off);
            btn_free_size.setSelected(false);

            if (Globals.pdtUpdateReq != null) {
                s_size = Globals.pdtUpdateReq.getPdtSize();
            }
        }
    }

    void goBack() {
        getActivity().onBackPressed();
    }

    public void resetPage() {
        if (btn_direct_input != null && !btn_direct_input.isSelected()) {
            txt_direct_size_value.clearFocus();
        }
    }
}
