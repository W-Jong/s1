package com.kyad.selluv.api.dto.category;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class CategoryDao {
    //"카테고리UID")
    private int categoryUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"상위 카테고리UID")
    private int parentCategoryUid;
    //"카테고리 뎁스")
    private int depth;
    //"카테고리명")
    private String categoryName;
    //"카테고리 순서")
    private int categoryOrder;
    //"카테고리에 해당한 상품수(하위 카테고리 포함)")
    private int pdtCount;
    //"카테고리에 해당한 누적 거래건수(하위 카테고리 포함)")
    private long totalCount;
    //"카테고리에 해당한 누적 거래대금(하위 카테고리 포함)")
    private long totalPrice;

    private int status;
}
