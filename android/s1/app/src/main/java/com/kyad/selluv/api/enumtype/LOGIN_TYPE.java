package com.kyad.selluv.api.enumtype;

public enum LOGIN_TYPE {
    NORMAL(0), FACEBOOK(1), NAVER(2), KAKAOTALK(3);

    private int code;

    LOGIN_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
