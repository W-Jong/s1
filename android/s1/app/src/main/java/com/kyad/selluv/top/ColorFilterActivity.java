package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ColorFilterActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.iv_check_black)
    ImageView iv_check_black;
    @BindView(R.id.iv_check_gray)
    ImageView iv_check_gray;
    @BindView(R.id.iv_check_white)
    ImageView iv_check_white;
    @BindView(R.id.iv_check_beige)
    ImageView iv_check_beige;
    @BindView(R.id.iv_check_red)
    ImageView iv_check_red;
    @BindView(R.id.iv_check_yellow)
    ImageView iv_check_yellow;
    @BindView(R.id.iv_check_brown)
    ImageView iv_check_brown;
    @BindView(R.id.iv_check_green)
    ImageView iv_check_green;
    @BindView(R.id.iv_check_blue)
    ImageView iv_check_blue;
    @BindView(R.id.iv_check_purple)
    ImageView iv_check_purple;
    @BindView(R.id.iv_check_pink)
    ImageView iv_check_pink;
    @BindView(R.id.iv_check_orange)
    ImageView iv_check_orange;
    @BindView(R.id.iv_check_gold)
    ImageView iv_check_gold;
    @BindView(R.id.iv_check_silver)
    ImageView iv_check_silver;
    @BindView(R.id.iv_check_multi)
    ImageView iv_check_multi;


    private List<String> colorFilters;
    /**
     * 컬러 순서는 Constants.COLOR_NAMES 와 동일해야한다.
     */
    private ImageView[] checkIVs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_color);

        checkIVs = new ImageView[]{
                iv_check_black,
                iv_check_gray,
                iv_check_white,
                iv_check_beige,
                iv_check_red,
                iv_check_pink,
                iv_check_purple,
                iv_check_blue,
                iv_check_green,
                iv_check_yellow,
                iv_check_orange,
                iv_check_brown,
                iv_check_gold,
                iv_check_silver,
                iv_check_multi
        };

        colorFilters = Globals.searchReq.getPdtColorList();
        loadLayout();
    }

    @OnClick(R.id.rl_black)
    void onBlack() {
        onClickColor(0);
    }


    @OnClick(R.id.rl_gray)
    void onGray() {
        onClickColor(1);
    }

    @OnClick(R.id.rl_white)
    void onWhite() {
        onClickColor(2);

    }

    @OnClick(R.id.rl_beige)
    void onBeige() {
        onClickColor(3);
    }

    @OnClick(R.id.rl_red)
    void onRed() {
        onClickColor(4);
    }

    @OnClick(R.id.rl_pink)
    void onPink() {
        onClickColor(5);
    }

    @OnClick(R.id.rl_purple)
    void onPurple() {
        onClickColor(6);
    }

    @OnClick(R.id.rl_blue)
    void onBlue() {
        onClickColor(7);
    }

    @OnClick(R.id.rl_green)
    void onGreen() {
        onClickColor(8);
    }

    @OnClick(R.id.rl_yellow)
    void onYellow() {
        onClickColor(9);
    }

    @OnClick(R.id.rl_orange)
    void onOrange() {
        onClickColor(10);
    }

    @OnClick(R.id.rl_brown)
    void onBrown() {
        onClickColor(11);
    }

    @OnClick(R.id.rl_gold)
    void onGold() {
        onClickColor(12);
    }

    @OnClick(R.id.rl_silver)
    void onSilver() {
        onClickColor(13);
    }

    @OnClick(R.id.rl_multi)
    void onMulti() {
        onClickColor(14);
    }

    /**
     * 검색결과 보기
     */
    @OnClick(R.id.btn_ok)
    void onOk() {
        sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.tv_init)
    void onInit() {
        for (int i = 0; i < checkIVs.length; i++) {
            checkIVs[i].setVisibility(View.INVISIBLE);
        }

        colorFilters.clear();
    }

    private void loadLayout() {
        for (int i = 0; i < Constants.COLOR_NAMES.length; i++) {
            checkIVs[i].setVisibility(View.INVISIBLE);
            for (int j = 0; j < colorFilters.size(); j++) {
                if (colorFilters.get(j).equals(Constants.COLOR_NAMES[i])) {
                    checkIVs[i].setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    private void onClickColor(int idx) {

        if (checkIVs[idx].getVisibility() == View.INVISIBLE) {
            checkIVs[idx].setVisibility(View.VISIBLE);
            colorFilters.add(Constants.COLOR_NAMES[idx]);
        } else {
            checkIVs[idx].setVisibility(View.INVISIBLE);
            colorFilters.remove(Constants.COLOR_NAMES[idx]);
        }
    }
}
