package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.deal.DealDetailDto;
import com.kyad.selluv.api.dto.other.DeliveryCompaniesDto;
import com.kyad.selluv.api.dto.other.WorkingDayDto;
import com.kyad.selluv.api.request.DealDeliveryNumReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class ShipActivity extends BaseActivity {

    private int dealUid = 0;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_contact)
    TextView tv_contact;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_buyer_address)
    TextView tv_buyer_address;
    @BindView(R.id.edt_ticket)
    EditText edt_ticket;
    @BindView(R.id.spinner)
    CustomSpinner spinner_company;

    //UI Reference
    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_register)
    void onRegister() {
        reqDealDeliveryNum();
    }

    //Variables
    private String[] arrCompanyList ;
    private String strCompany = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_ship);
        setStatusBarWhite();

        loadLayout();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        String name = getIntent().getStringExtra("name");
        String phone = getIntent().getStringExtra("phone");
        String address = getIntent().getStringExtra("address");
        dealUid = getIntent().getIntExtra("uid", 0);
        long verifyPrice = getIntent().getLongExtra("verifyPrice", 0);

        if (verifyPrice > 0) {
            //셀럽주소
            tv_name.setText(Constants.SELLUV_NAME);
            tv_contact.setText(Constants.SELLUV_CONTACT);
            tv_address.setText(Constants.SELLUV_ADDRESS);
            tv_buyer_address.setText(getString(R.string.selluv_address));
        } else {
            //구매자 주소
            tv_name.setText(name);
            tv_contact.setText(phone);
            tv_address.setText(address);
            tv_buyer_address.setText(getString(R.string.buyer_address));
        }

        getDeliveryCompanies();
    }

    private void setCompanyData(){
        spinner_company.initializeStringValues(this, arrCompanyList, getResources().getString(R.string.post_company_select), null, null, 12.5f);
        spinner_company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position != 0)
                    strCompany = arrCompanyList[position - 1];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    //택배사 목록 얻기
    private void getDeliveryCompanies() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<DeliveryCompaniesDto>> genRes = restAPI.getDeliveryCompanies(Globals.userToken);

        genRes.enqueue(new TokenCallback<DeliveryCompaniesDto>(this) {
            @Override
            public void onSuccess(DeliveryCompaniesDto response) {
                closeLoading();

                arrCompanyList = new String[response.getCompanies().size()];
                arrCompanyList = response.getCompanies().toArray(arrCompanyList);

                setCompanyData();

            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    // 운송장 번호 등록
    private void reqDealDeliveryNum() {

        String num = edt_ticket.getText().toString();
        if (num.equals("")) {
            Utils.showToast(this, getString(R.string.empty_deliver_num));
            return;
        }

        if (strCompany.equals("")) {
            Utils.showToast(this, getString(R.string.post_company_select_msg));
            return;
        }

        DealDeliveryNumReq dicInfo = new DealDeliveryNumReq();
        dicInfo.setDeliveryCompany(strCompany);
        dicInfo.setDeliveryNumber(num);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<DealDao>> genRes = restAPI.reqDealDeliveryNum(Globals.userToken, dealUid, dicInfo);

        genRes.enqueue(new TokenCallback<DealDao>(this) {
            @Override
            public void onSuccess(DealDao response) {
                closeLoading();

                Utils.showToast(ShipActivity.this, getString(R.string.reg_deliver_num_success));
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
