package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.DbManager;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.BrandDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.api.dto.search.RecommendSearchDto;
import com.kyad.selluv.api.dto.search.SearchModelDto;
import com.kyad.selluv.api.dto.search.SearchWordDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.top.adapter.SearchListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class TopSearchActivity extends BaseActivity {

    //UI Reference


    @BindView(R.id.tv_search_title)
    TextView tv_search_title;
    @BindView(R.id.lv_search)
    ListView lv_search;
    @BindView(R.id.edt_keyword)
    EditText edt_keyword;
    @BindView(R.id.tv_delete)
    TextView tv_delete;
    @BindView(R.id.ib_close)
    ImageButton ib_close;
    @BindView(R.id.rl_popular_search)
    RelativeLayout rl_popular_search;


    //Variables
    SearchListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_search);

        loadLayout();
        searchPopular();
    }


    @OnClick(R.id.tv_cancel)
    void onCancel() {
        finish();
    }

    @OnClick(R.id.ib_close)
    void onClose() {
        edt_keyword.setText("");
        searchPopular();
    }

    @OnClick(R.id.tv_delete)
    void onDelete() {
        listAdapter.removeAllItems();
        DbManager.deleteSearchWord();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        listAdapter = new SearchListAdapter(this);
        listAdapter.nType = 0;
        lv_search.setAdapter(listAdapter);

        edt_keyword.clearFocus();
        edt_keyword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    tv_search_title.setText(R.string.search_recent);
                    tv_delete.setVisibility(View.VISIBLE);
                    rl_popular_search.setVisibility(View.VISIBLE);
                    ib_close.setVisibility(View.INVISIBLE);

                    listAdapter.arrData = DbManager.loadSearchWord();
                    listAdapter.nType = 2;//최근검색
                    listAdapter.notifyDataSetChanged();
                }
            }
        });

        edt_keyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edt_keyword.getText().length() > 0) {
                    ib_close.setVisibility(View.VISIBLE);
                    rl_popular_search.setVisibility(View.GONE);

                    searchRecommend(edt_keyword.getText().toString());
                }
            }
        });

        edt_keyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Intent intent = new Intent(TopSearchActivity.this, SearchResultActivity.class);
                    intent.putExtra(Constants.SEARCH_WORD, edt_keyword.getText().toString());
                    intent.putExtra(Constants.FROM_PAGE, Constants.WIRE_FRAG_SEARCH);
                    TopSearchActivity.this.startActivity(intent);

                    handled = true;
                }

                return handled;
            }
        });
    }

    private void searchPopular() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<SearchWordDto>>> genRes = restAPI.searchFame(Globals.userToken);
        genRes.enqueue(new TokenCallback<List<SearchWordDto>>(this) {
            @Override
            public void onSuccess(List<SearchWordDto> response) {
                closeLoading();

                for (int i = 0; i < response.size(); i++)
                    listAdapter.arrData.add(response.get(i));
                listAdapter.nType = 1;//인기검색
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void searchRecommend(final String keyword) {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<RecommendSearchDto>> genRes = restAPI.searchRecommend(Globals.userToken, keyword);
        genRes.enqueue(new TokenCallback<RecommendSearchDto>(this) {
            @Override
            public void onSuccess(RecommendSearchDto response) {
                closeLoading();

                listAdapter.removeAllItems();

                int total_cnt = 0;

                if (response.getBrandList().size() > 0) {
                    listAdapter.arrData.add(new Model.SearchItem("브랜드", "", 0, 0, 0));
                    for (int i = 0; i < response.getBrandList().size(); i++) {
                        BrandDao item =  response.getBrandList().get(i);
                        listAdapter.arrData.add(new Model.SearchItem(item.getNameEn(), item.getFirstKo(), item.getPdtCount(), 1, i));
                    }
                    total_cnt += response.getBrandList().size();
                }

                if (response.getCategoryDaoList().size() > 0) {
                    listAdapter.arrData.add(new Model.SearchItem("카테고리", "", 0, 0, 0));
                    for (int i = 0; i < response.getCategoryDaoList().size(); i++) {
                        List<CategoryDao> item =  response.getCategoryDaoList().get(i);
                        String ss = "";
                        if (i < Constants.shopper_type.length)
                            ss = Constants.shopper_type[i];
                        for (int j = 0; j < item.size(); j++) {
                            CategoryDao cd = item.get(j);
                            listAdapter.arrData.add(new Model.SearchItem(cd.getCategoryName(), ss, cd.getPdtCount(), 2, i* 10000 + j));
                        }
                    }
                    total_cnt += response.getCategoryDaoList().size();
                }

                if (response.getModelList().size() > 0) {
                    listAdapter.arrData.add(new Model.SearchItem("모델명", "", 0, 0, 0));
                    for (int i = 0; i < response.getModelList().size(); i++) {
                        SearchModelDto item =  response.getModelList().get(i);
                        listAdapter.arrData.add(new Model.SearchItem(item.getModelName(), item.getBrandName(), 0, 3, i));
                    }
                    total_cnt += response.getModelList().size();
                }

                if (response.getTagWordList().size() > 0) {
                    listAdapter.arrData.add(new Model.SearchItem("태그", "", 0, 0, 0));
                    for (int i = 0; i < response.getTagWordList().size(); i++) {
                        String item =  response.getTagWordList().get(i);
                        listAdapter.arrData.add(new Model.SearchItem(item, "", 0, 4, i));
                    }
                    total_cnt += response.getTagWordList().size();
                }

                if (total_cnt == 0) {
                    listAdapter.arrData.add(keyword);
                    listAdapter.nType = 4;//추천검색 없음
                } else {
                    listAdapter.nType = 3;//추천검색
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
