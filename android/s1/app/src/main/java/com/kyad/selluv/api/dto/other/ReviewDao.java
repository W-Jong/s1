package com.kyad.selluv.api.dto.other;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class ReviewDao {
    private int reviewUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"회원UID")
    private int usrUid;
    //"거래UID")
    private int dealUid;
    //"대상회원UID")
    private int peerUsrUid;
    //"후기분류 1-판매자후기 2-구매자후기")
    private int type;
    //"후기내용")
    private String content = "";
    //"점수 2-만족 1-보통 0-불만족")
    private int point = 0;

}
