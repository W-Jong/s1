package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.WishalarmAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrWishDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.request.MultiUidsReq;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class WishAlarmActivity extends BaseActivity {

    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;

    //UI Reference
    @BindView(R.id.lv_alarm)
    ListView lv_alarm;
    @BindView(R.id.tv_edit)
    TextView tv_edit;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.tv_edit)
    void onEdit() {
        editable = !editable;
        if (editable) {
            tv_edit.setText(R.string.finish);
            listAdapter.setEditable(true);
        } else {
            tv_edit.setText(R.string.edit);
            listAdapter.setEditable(false);
        }
    }

    //Variables
    boolean editable;
    WishalarmAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_wishalarm);
        setStatusBarWhite();

        loadLayout();
        getUsrWishList(currentPage);
    }

    private void loadLayout() {
        listAdapter = new WishalarmAdapter(this);
        lv_alarm.setAdapter(listAdapter);
        lv_alarm.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getUsrWishList(currentPage);
                }
            }
        });
    }


    //get notice list
    private void getUsrWishList(final int page){
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<UsrWishDto>>> genRes = restAPI.usrWishList(Globals.userToken, page);

        genRes.enqueue(new TokenCallback<Page<UsrWishDto>>(this) {
            @Override
            public void onSuccess(Page<UsrWishDto> response) {
                closeLoading();
                isLoading = false;

                if (page == 0) {
                    listAdapter.arrData.clear();
                }
                List<UsrWishDto> responseData = response.getContent();
                isLast = response.isLast();

                listAdapter.arrData = responseData;
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }

    //delete usr wish
    public void deleteUsrWish(List<Integer> uids) {

        if (uids.size() < 1) {
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        MultiUidsReq multiUidsReq = new MultiUidsReq(uids);
        Call<GenericResponse<Void>> genRes = restAPI.deleteUsrWishs(Globals.userToken, multiUidsReq);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(WishAlarmActivity.this, getString(R.string.delete_success));
                currentPage = 0;
                isLast = false;
                isLoading = false;

                getUsrWishList(currentPage);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

}
