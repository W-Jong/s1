package com.kyad.selluv.api.dto.deal;

import com.kyad.selluv.api.dto.pdt.Page;

import java.util.List;

import lombok.Data;

@Data
public class BuyHistoryDto {
    //("네고제안수")
    private long negoCount;
    //("거래완료수")
    private long completedCount;
    //("배송준비수")
    private long preparedCount;
    //("정품인증수")
    private long verifiedCount;
    //("배송진행수")
    private long progressCount;
    //("배송완료수")
    private long sentCount;
    //("구매내역")
    private Page<DealListDto> history;//kkk - Page
}
