package com.kyad.selluv.api.dto.top;

import com.kyad.selluv.api.dto.pdt.PdtMiniDto;

import java.util.List;

import lombok.Data;

@Data
public class UsrRecommendDto {
    //@ApiModelProperty("회원UID")
    private int usrUid;
    //@ApiModelProperty("아이디")
    private String usrId;
    //@ApiModelProperty("닉네임")
    private String usrNckNm;
    //@ApiModelProperty("프로필이미지")
    private String profileImg;
    //@ApiModelProperty("아이템수")
    private long pdtCount;
    //@ApiModelProperty("팔로워수")
    private long usrFollowerCount;
    //@ApiModelProperty("회원 팔로우 여부")
    private boolean usrLikeStatus;
    //@ApiModelProperty("인기상품목록")
    private List<PdtMiniDto> pdtList;
}
