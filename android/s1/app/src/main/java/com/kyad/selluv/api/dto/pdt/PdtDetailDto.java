package com.kyad.selluv.api.dto.pdt;

import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.api.dto.category.CategoryMiniDto;
import com.kyad.selluv.api.dto.usr.UsrDetailDto;

import java.util.List;

import lombok.Data;

@Data
public class PdtDetailDto {
    //("상품")
    private PdtDto pdt;
    //("판매유저")
    private UsrDetailDto seller;
    //("브랜드")
    private BrandMiniDto brand;
    //("상세카테고리목록(상품군/1단계/2단계/3단계 카테고리)")
    private List<CategoryMiniDto> categoryList;
    //("스타일리스트")
    private List<PdtStyleDto> pdtStyleList;
    //("상품좋아요 갯수")
    private int pdtLikeCount;
    //("유저 상품좋아요 상태")
    private Boolean pdtLikeStatus;
    //("상품잇템 유저수")
    private int pdtWishCount;
    //("유저 상품잇템 상태")
    private Boolean pdtWishStatus;
    //("상품 댓글수")
    private int pdtReplyCount;
    //("등록유저 아이템 갯수")
    private int sellerItemCount;
    //("등록유저 상품리스트")
    private List<PdtInfoDto> sellerPdtList;
    //("등록유저 팔로워 유저수")
    private int sellerFollowerCount;
    //("연관 스타일 갯수")
    private int relationStyleCount;
    //("공유URL")
    private String shareUrl;
    //edited
    private Boolean edited;


    public String getPdtTitle() {
        String pdtTitle = "";
        try {
            pdtTitle = getBrand().getNameKo() + " " + getCategoryList().get(0).getCategoryName() + " " + (getPdt().getColorName().isEmpty() ? "" : getPdt().getColorName() + " ") + (getPdt().getPdtModel().isEmpty() ? "" : getPdt().getPdtModel() + " ") + getCategoryList().get(getCategoryList().size() - 1).getCategoryName();
        } catch (Exception e) {
            e.printStackTrace();
            pdtTitle = "";
        }

        return pdtTitle;
    }

    public int getSalePercent() {
        return (int) ((getPdt().getOriginPrice() - getPdt().getPrice()) * 100.0f / getPdt().getOriginPrice());
    }

    public boolean isFreeSend() {
        return getSeller().getFreeSendPrice() > 0 && getPdt().getPrice() > getSeller().getFreeSendPrice();
    }

    public long getFreeSendPrice() {
        return getPdt().getSendPrice();
    }
}
