package com.kyad.selluv.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.Utils;

import java.util.ArrayList;

/**
 * Created by etiennelawlor on 8/20/15.
 */
public class GalleryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // region Member Variables
    private ArrayList<DirItem> dirs;
    Context context;
    onDirClickListener mDirClickListener = null;

    // region Constructors
    public GalleryListAdapter(Context context, ArrayList<DirItem> dirs) {
        this.context = context;
        this.dirs = dirs;
    }
    // endregion

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gallery_list, viewGroup, false);
        return new DirItemVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final DirItemVH holder = (DirItemVH) viewHolder;

        DirItem dirItem = dirs.get(position);

        if (dirItem.thumb != null)
            holder.ivThumb.setImageBitmap(ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(dirItem.thumb),
                    Utils.dpToPixel(context, 55),
                    Utils.dpToPixel(context, 55)));
        holder.tvCnt.setText(String.valueOf(dirItem.cnt));
        holder.tvDir.setText(String.valueOf(dirItem.dir));
        holder.rlDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPos = holder.getAdapterPosition();
                if (adapterPos != RecyclerView.NO_POSITION) {
                    if (mDirClickListener != null) {
                        mDirClickListener.onDirClick(holder, adapterPos);
                    }
                }
            }
        });
    }

    public void setDirClickListener(onDirClickListener listener) {
        mDirClickListener = listener;
    }

    @Override
    public int getItemCount() {
        if (dirs != null) {
            return dirs.size();
        } else {
            return 0;
        }
    }

    public static class DirItem {
        public String thumb;
        public String dir;
        public int cnt;

        public DirItem(String thumb, String dir, int cnt) {
            this.thumb = thumb;
            this.dir = dir;
            this.cnt = cnt;
        }
    }

    public static class DirItemVH extends RecyclerView.ViewHolder {

        // region Views
        private final ImageView ivThumb;
        private final TextView tvDir;
        private final TextView tvCnt;
        private final FrameLayout rlDir;
        // endregion

        // region Constructors
        public DirItemVH(final View view) {
            super(view);

            ivThumb = view.findViewById(R.id.iv_thumb);
            tvDir = view.findViewById(R.id.tv_dir);
            tvCnt = view.findViewById(R.id.tv_cnt);
            rlDir = view.findViewById(R.id.fl_gallery_list);
        }
        // endregion
    }

    public interface onDirClickListener {
        void onDirClick(RecyclerView.ViewHolder holder, int position);
    }
}
