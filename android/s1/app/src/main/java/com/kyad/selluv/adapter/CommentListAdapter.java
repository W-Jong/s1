package com.kyad.selluv.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.pdt.ReplyDto;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.CommentActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;

import static com.kyad.selluv.detail.DetailActivity.showEtcReport;

public class CommentListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public ArrayList<ReplyDto> arrData = new ArrayList<>();
    CommentActivity activity = null;
    public boolean isEditing;

    public CommentListAdapter(CommentActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<ReplyDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_detail_comment, parent, false);
            final ReplyDto anItem = (ReplyDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }

            ImageView iv_profile = convertView.findViewById(R.id.iv_profile);
            ImageUtils.load(activity, anItem.getUsr().getProfileImg(), android.R.color.transparent, R.drawable.ic_user_default, iv_profile);
            iv_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsr().getUsrUid());
                    activity.startActivity(intent);
                }
            });

            SpannableString text = new SpannableString(anItem.getUsr().getUsrNckNm() + " " + anItem.getReply().getContent());
            TextView tv_comment = (TextView) convertView.findViewById(R.id.tv_comment);
            text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, anItem.getUsr().getUsrNckNm().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new RelativeSizeSpan(1.1f), 0, anItem.getUsr().getUsrNckNm().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_comment.setText(text);

            String comment = tv_comment.getText().toString();
            String[] splits = comment.split(" ");
            String[] userUids = anItem.getReply().getTargetUids().split(",");

            if (splits.length > 2 && splits[1].startsWith("@")) {
                // 닉네임 처리
                InternalURLSpan span = new InternalURLSpan();
                span.userId = userUids.length > 0 ? Integer.parseInt(userUids[0]) : 0;
                text.setSpan(span, comment.indexOf(splits[1]), comment.indexOf(splits[1]) + splits[1].length(), android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_comment.setText(text);
                tv_comment.setLinkTextColor(Color.parseColor("#133465"));
                tv_comment.setLinksClickable(false);
                tv_comment.setMovementMethod(LinkMovementMethod.getInstance());
                tv_comment.setFocusable(false);
            }


            TextView tv_response = (TextView) convertView.findViewById(R.id.tv_response);
            if (Globals.isMyUsrUid(anItem.getUsr().getUsrUid())) {
                tv_response.setVisibility(View.GONE);
            } else {
                tv_response.setVisibility(View.VISIBLE);
                tv_response.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.addComment(anItem.getUsr().getUsrNckNm());
                    }
                });
            }

            TextView tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            tv_time.setText(Utils.getDiffTime(anItem.getReply().getRegTime().getTime()));
            convertView.findViewById(R.id.rl_comment).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideSoftKeyboard(activity);

                    final Snackbar snackbar = Snackbar.make(activity.getWindow().getDecorView(), "", Snackbar.LENGTH_INDEFINITE);
                    Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                    snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
                    layout.setPadding(0, 0, 0, 0);//set padding to 0
                    View view = inflater.inflate(R.layout.action_sheet2, null);
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    layout.addView(view, params);
                    view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackbar.dismiss();
                        }
                    });
                    view.findViewById(R.id.btn_add_reply).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            activity.addComment(anItem.getUsr().getUsrNckNm());
                            snackbar.dismiss();
                        }
                    });
                    view.findViewById(R.id.btn_report).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showEtcReport(activity);
                            snackbar.dismiss();
                        }
                    });

                    if (Globals.isMyUsrUid(anItem.getUsr().getUsrUid())) {
                        view.findViewById(R.id.btn_add_reply).setVisibility(View.GONE);
                        view.findViewById(R.id.btn_report).setVisibility(View.GONE);
                        view.findViewById(R.id.btn_delete).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                                ((CommentActivity) activity).deletePdtReply(anItem);
                            }
                        });
                    }

                    snackbar.show();
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    class InternalURLSpan extends android.text.style.ClickableSpan {
        public int userId;

        @Override
        public void onClick(View widget) {
            if (userId == 0) {
                return;
            }

            Intent intent = new Intent(activity, UserActivity.class);
            intent.putExtra(UserActivity.USR_UID, userId);
            activity.startActivity(intent);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ds.linkColor);
            ds.setUnderlineText(false);
        }
    }
}