package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;

import lombok.Data;
import lombok.NonNull;

@Data
public class ValetCreateReq {

    //@ApiModelProperty("브랜드UID")
    @IntRange(from = 1)
    private int brandUid;

    //@ApiModelProperty("카테고리UID")
    @IntRange(from = 5)
    private int categoryUid;

    @IntRange(from = 0)
    //@ApiModelProperty("판매가, 추천가격의 경우 0, 직접설정의 경우 0이상값")
    private long reqPrice;

    @NonNull
    //@ApiModelProperty("성함")
    private String reqName;

    @NonNull
    //@ApiModelProperty("연락처")
    private String reqPhone;

    @NonNull
    //@ApiModelProperty("주소")
    private String reqAddress;

    @IntRange(from = 1, to = 2)
    //@ApiModelProperty("발송방법, 1-배송키트 이용, 2-직접발송")
    private int sendType = 1;

    @NonNull
    //@ApiModelProperty("사인이미지")
    private String signImg;
}
