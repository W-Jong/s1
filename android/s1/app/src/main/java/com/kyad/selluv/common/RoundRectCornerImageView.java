package com.kyad.selluv.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.kyad.selluv.R;

/**
 * Author: Star_Man
 * Date: 2018-04-23 10:02:34
 */
public class RoundRectCornerImageView extends android.support.v7.widget.AppCompatImageView {

    private float radius = 36.0f;
    private Path path;
    private RectF rect;

    public RoundRectCornerImageView(Context context) {
        super(context);
        init();
    }

    public RoundRectCornerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        radius = context.obtainStyledAttributes(attrs, R.styleable.RoundRectCornerImageView)
                .getDimensionPixelSize(R.styleable.RoundRectCornerImageView_cornerRadius, 36);
        init();
    }

    public RoundRectCornerImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        radius = context.obtainStyledAttributes(attrs, R.styleable.RoundRectCornerImageView)
                .getDimensionPixelSize(R.styleable.RoundRectCornerImageView_cornerRadius, 36);
        init();
    }

    private void init() {
        path = new Path();
        rect = new RectF(0, 0, 0, 0);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        rect.left = 0;
        rect.top = 0;
        rect.right = this.getWidth();
        rect.bottom = this.getHeight();
        path.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(path);
        super.onDraw(canvas);
    }
}
