package com.kyad.selluv.api;

public class APIException extends Exception {
    private String code;

    public APIException(String msg) {
        super(msg);
    }

    public APIException(String msg, String code) {
        super(msg);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
