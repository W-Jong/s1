package com.kyad.selluv.api.dto.other;

import lombok.Data;

@Data
public class MinMaxDto {
    //("최소값")
    private long min;
    //("최대값")
    private long max;
}
