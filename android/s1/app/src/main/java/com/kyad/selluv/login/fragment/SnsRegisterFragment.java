package com.kyad.selluv.login.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.enumtype.LICENSE_TYPE;
import com.kyad.selluv.common.Constants.Direction;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.mypage.LicenseActivity;
import com.kyad.selluv.webview.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Globals.userSignReq;

public class SnsRegisterFragment extends LoginBaseFragment implements TextWatcher, View.OnFocusChangeListener {

    //UI Reference
    @BindView(R.id.edt_sns_id)
    EditText edt_sns_id;

    @BindView(R.id.edt_sns_nickkname)
    EditText edt_sns_nickkname;

    @BindView(R.id.edt_invite_code)
    EditText edt_invite_code;

    @BindView(R.id.iv_check)
    ImageView iv_check;

    @BindView(R.id.btn_ok)
    Button btn_ok;

    //Vars
    int childCnt = 0;
    View rootView;
    boolean fromRevisit = false;


    public static SnsRegisterFragment newInstance(int prevChildCnt, Direction direction, boolean fromRevisit) {
        SnsRegisterFragment fragment = new SnsRegisterFragment();
        Bundle args = new Bundle();
        args.putInt(PREV_CHILD_CNT, prevChildCnt);
        args.putInt(FROM_DIRECTION, direction.ordinal());
        args.putBoolean(FROM_REVISIT, fromRevisit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromRevisit = bundle.getBoolean(FROM_REVISIT, false);
            childCnt = bundle.getInt(PREV_CHILD_CNT, 0);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sns_register, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();

        edt_sns_id.setText(userSignReq.getUsrMail().split("@")[0]);
        edt_sns_nickkname.setText(userSignReq.getUsrNckNm());
        iv_check.setVisibility(View.INVISIBLE);
        checkUserId();
        return rootView;

    }

    private void loadLayout() {

        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.activity_main));

        //childCnt = ((RelativeLayout) rootView).getChildCount();

        edt_sns_id.addTextChangedListener(this);
        edt_sns_id.setOnFocusChangeListener(this);
        //edt_sns_id.setFilters(new InputFilter[]{new IdInputFilter()});
        edt_sns_id.setPrivateImeOptions("defaultInputmode=english;");

        edt_sns_nickkname.addTextChangedListener(this);
        edt_sns_nickkname.setOnFocusChangeListener(this);

        edt_invite_code.setOnFocusChangeListener(this);
    }


    @OnClick(R.id.ib_go_back)
    public void onBack(View v) {
        setCloseDirection(Direction.BOTTOM);
        if (fromRevisit)
            ((LoginStartActivity) getActivity()).gotoRevisitFragment(childCnt, Direction.TOP);
        else
            ((LoginStartActivity) getActivity()).gotoStartFragment(childCnt, Direction.TOP);
    }

    @OnClick(R.id.btn_ok)
    void onOk(View v) {
        if (iv_check.getVisibility() != View.VISIBLE) {
            checkUserId();
            return;
        }

        userSignReq.setUsrId(edt_sns_id.getText().toString());
        userSignReq.setUsrNckNm(edt_sns_nickkname.getText().toString());
        userSignReq.setInviteCode(edt_invite_code.getText().toString());

        CommonWebViewActivity.showDanalWebviewActivity(getActivity());
//        ((LoginStartActivity) getActivity()).register();
    }

    @OnClick(R.id.tv_license)
    void OnClickLicense(View v) {
        //이용약관
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.LICENSE.toString());
        startActivity(intent);

    }

    @OnClick(R.id.tv_privacy)
    void OnClickPrivacy(View v) {
        //개인정보취급방침
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.PERSONAL_POLICY.toString());
        startActivity(intent);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edt_sns_id.isFocused())
            iv_check.setVisibility(View.INVISIBLE);

        if (edt_sns_id.getText().toString().length() > 0 && edt_sns_nickkname.getText().toString().trim().length() > 0) {
            btn_ok.setEnabled(true);
        } else {
            btn_ok.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!edt_sns_id.isFocused() && hasFocus && iv_check.getVisibility() != View.VISIBLE && edt_sns_id.getText().length() > 0) {
            checkUserId();
        }
    }

    private void checkUserId() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse> genRes = restAPI.checkUserId(edt_sns_id.getText().toString());
        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    iv_check.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push));
                    iv_check.setVisibility(View.VISIBLE);
                } else {//실패
                    iv_check.setImageDrawable(getResources().getDrawable(R.drawable.list_ic_select_push_denied));
                    iv_check.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
