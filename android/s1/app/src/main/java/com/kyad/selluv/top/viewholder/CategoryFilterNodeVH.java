package com.kyad.selluv.top.viewholder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.model.Model;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Bogdan Melnychuk on 2/12/15.
 */
public class CategoryFilterNodeVH extends TreeNode.BaseNodeViewHolder<CategoryDao> {
    TextView tv_category;
    TextView tv_wareCnt;
    ImageView iv_check;
    RelativeLayout rl_main;

    public CategoryFilterNodeVH(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, final CategoryDao anItem) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View convertView = inflater.inflate(R.layout.item_search_category, null, false);

        tv_category = convertView.findViewById(R.id.tv_category);
        tv_wareCnt = convertView.findViewById(R.id.tv_ware_cnt);
        rl_main = convertView.findViewById(R.id.rl_item_main);
        iv_check = convertView.findViewById(R.id.iv_check);
        tv_category.setText(anItem.getCategoryName());
        tv_wareCnt.setText(String.valueOf(anItem.getPdtCount()));
        tv_wareCnt.setVisibility(View.INVISIBLE);
        rl_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                getTreeView().deselectAll();
                node.setSelected(true);
                tv_wareCnt.setTextColor(context.getResources().getColor(R.color.color_42c2fe));
                tv_category.setTextColor(context.getResources().getColor(R.color.color_42c2fe));
                iv_check.setVisibility(View.VISIBLE);
                getTreeView().selectNode(node, true);
                return false;
            }
        });
        return convertView;
    }


    @Override
    public void toggleSelectionMode(boolean editModeEnabled) {
        if (mNode.isSelected()) {
            tv_wareCnt.setTextColor(context.getResources().getColor(R.color.color_42c2fe));
            tv_category.setTextColor(context.getResources().getColor(R.color.color_42c2fe));
            iv_check.setVisibility(View.VISIBLE);
        } else {
            tv_wareCnt.setTextColor(Color.BLACK);
            tv_category.setTextColor(Color.BLACK);
            iv_check.setVisibility(View.INVISIBLE);
        }
    }
}
