/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.detail.WareEditActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SellConditionFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.btn_new)
    ImageButton btn_new;
    @BindView(R.id.btn_best)
    ImageButton btn_best;
    @BindView(R.id.btn_good)
    ImageButton btn_good;
    @BindView(R.id.btn_middle)
    ImageButton btn_middle;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_info_condition, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onDone(View v) {
        goBack();
    }

    /**
     * 신상품
     *
     * @param v
     */
    @OnClick(R.id.btn_new)
    void onNew(View v) {
        updateUI(PDT_CONDITION_TYPE.NEW.getCode());

        String s_size = getString(R.string.condition_new);
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtCondition(PDT_CONDITION_TYPE.NEW);
        } else {
            Globals.pdtCreateReq.setPdtCondition(PDT_CONDITION_TYPE.NEW);
        }
        ((TextView) getActivity().findViewById(R.id.tv_condition_value)).setText(s_size);

        goBack();
    }

    /**
     * 최상
     *
     * @param v
     */
    @OnClick(R.id.btn_best)
    void onBest(View v) {
        updateUI(PDT_CONDITION_TYPE.BEST.getCode());

        String s_size = getString(R.string.condition_excellent);
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtCondition(PDT_CONDITION_TYPE.BEST);
        } else {
            Globals.pdtCreateReq.setPdtCondition(PDT_CONDITION_TYPE.BEST);
        }
        ((TextView) getActivity().findViewById(R.id.tv_condition_value)).setText(s_size);

        goBack();
    }

    /**
     * 상
     *
     * @param v
     */
    @OnClick(R.id.btn_good)
    void onGood(View v) {
        updateUI(PDT_CONDITION_TYPE.GOOD.getCode());

        String s_size = getString(R.string.condition_good);
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtCondition(PDT_CONDITION_TYPE.GOOD);
        } else {
            Globals.pdtCreateReq.setPdtCondition(PDT_CONDITION_TYPE.GOOD);
        }
        ((TextView) getActivity().findViewById(R.id.tv_condition_value)).setText(s_size);

        goBack();
    }

    /**
     * 중상
     *
     * @param v
     */
    @OnClick(R.id.btn_middle)
    void onMiddle(View v) {
        updateUI(PDT_CONDITION_TYPE.MIDDLE.getCode());

        String s_size = getString(R.string.condition_ok);
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtCondition(PDT_CONDITION_TYPE.MIDDLE);
        } else {
            Globals.pdtCreateReq.setPdtCondition(PDT_CONDITION_TYPE.MIDDLE);
        }
        ((TextView) getActivity().findViewById(R.id.tv_condition_value)).setText(s_size);

        goBack();
    }

    /**
     * 가이드
     *
     * @param v
     */
    @OnClick(R.id.btn_guide)
    void onGuide(View v) {
        Intent intent = new Intent(getActivity(), GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.sell_direct_info_condition_guide);
        startActivity(intent);
    }

    void goBack() {
        getActivity().onBackPressed();
    }

    private void loadLayout() {
        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            ib_right.setVisibility(View.GONE);

            updateUI(Globals.pdtUpdateReq.getPdtCondition().getCode());

        } else {
            tv_finish.setVisibility(View.GONE);
            ib_right.setVisibility(View.VISIBLE);
        }
    }

    private void updateUI(int statuscode) {
        if (statuscode == (PDT_CONDITION_TYPE.NEW.getCode())) {
            btn_new.setBackgroundResource(R.drawable.ic_sell_condition_new_over);
            btn_best.setBackgroundResource(R.drawable.ic_sell_condition_best);
            btn_good.setBackgroundResource(R.drawable.ic_sell_condition_good);
            btn_middle.setBackgroundResource(R.drawable.ic_sell_condition_middle);
        } else if (statuscode == (PDT_CONDITION_TYPE.BEST.getCode())) {
            btn_new.setBackgroundResource(R.drawable.ic_sell_condition_new);
            btn_best.setBackgroundResource(R.drawable.ic_sell_condition_best_over);
            btn_good.setBackgroundResource(R.drawable.ic_sell_condition_good);
            btn_middle.setBackgroundResource(R.drawable.ic_sell_condition_middle);
        } else if (statuscode == (PDT_CONDITION_TYPE.MIDDLE.getCode())) {
            btn_new.setBackgroundResource(R.drawable.ic_sell_condition_new);
            btn_best.setBackgroundResource(R.drawable.ic_sell_condition_best);
            btn_good.setBackgroundResource(R.drawable.ic_sell_condition_good);
            btn_middle.setBackgroundResource(R.drawable.ic_sell_condition_middle_over);
        } else if (statuscode == (PDT_CONDITION_TYPE.GOOD.getCode())) {
            btn_new.setBackgroundResource(R.drawable.ic_sell_condition_new);
            btn_best.setBackgroundResource(R.drawable.ic_sell_condition_best);
            btn_good.setBackgroundResource(R.drawable.ic_sell_condition_good_over);
            btn_middle.setBackgroundResource(R.drawable.ic_sell_condition_middle);
        }
    }

}
