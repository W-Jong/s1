package com.kyad.selluv.api.dto;

/**
 * Created by KSH on 3/8/2018.
 */

public class GenericResponse<T> {
    private ResponseMeta meta;
    private T data;

    public ResponseMeta getMeta() {
        return this.meta;
    }

    public T getData() {
        return this.data;
    }

    public void setMeta(ResponseMeta meta) {
        this.meta = meta;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GenericResponse)) {
            return false;
        } else {
            GenericResponse other = (GenericResponse) o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                ResponseMeta this$meta = this.getMeta();
                ResponseMeta other$meta = other.getMeta();
                if (this$meta == null) {
                    if (other$meta != null) {
                        return false;
                    }
                } else if (!this$meta.equals(other$meta)) {
                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof GenericResponse;
    }

    public int hashCode() {
        boolean PRIME = true;
        byte result = 1;
        ResponseMeta $meta = this.getMeta();
        int result1 = result * 59 + ($meta == null ? 0 : $meta.hashCode());
        Object $data = this.getData();
        result1 = result1 * 59 + ($data == null ? 0 : $data.hashCode());
        return result1;
    }

    public String toString() {
        return "GenericResponse(meta=" + this.getMeta() + ", data=" + this.getData() + ")";
    }

    public GenericResponse(ResponseMeta meta, T data) {
        this.meta = meta;
        this.data = data;
    }

    public GenericResponse() {
    }
}
