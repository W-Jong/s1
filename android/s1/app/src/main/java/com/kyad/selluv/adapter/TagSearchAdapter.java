package com.kyad.selluv.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.sell.fragment.SellTagFragment;

import java.util.List;

public class TagSearchAdapter extends BaseAdapter {

    LayoutInflater inflater;
    List<String> arrData = null;
    SellTagFragment fragment;
    int selectedPos = 0;

    public TagSearchAdapter(SellTagFragment fragment) {
        this.fragment = fragment;
        inflater = LayoutInflater.from(fragment.getContext());
    }

    public void setData(List<String> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public String getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_search_tag, parent, false);
            final String anItem = getItem(position);
            if (anItem == null) {
                return convertView;
            }
            TextView tv_tag = convertView.findViewById(R.id.tv_tag);
            TextView tv_ware_cnt = convertView.findViewById(R.id.tv_ware_cnt);
            tv_tag.setText("#" + anItem);
            tv_ware_cnt.setText(" ");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.onClickTag(anItem);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return convertView;
    }

}