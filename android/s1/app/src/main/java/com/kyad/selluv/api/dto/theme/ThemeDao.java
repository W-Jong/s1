package com.kyad.selluv.api.dto.theme;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class ThemeDao {
    //"테마UID")
    private int themeUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"테마분류 1-남성 2-여성 4-키즈")
    private int kind;
    //"테마타이틀 - 영어")
    private String titleEn;
    //"테마타이틀 - 한국어")
    private String titleKo;
    //"테마이미지")
    private String profileImg;
    //"시작시간")
    private Timestamp startTime;
    //"종료시간")
    private Timestamp endTime;
    private int status;
}
