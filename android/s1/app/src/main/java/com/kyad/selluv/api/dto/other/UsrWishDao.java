package com.kyad.selluv.api.dto.other;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class UsrWishDao {
    private int usrWishUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"유저UID")
    private int usrUid;
    //"브랜드UID")
    private int brandUid;
    //"상품군 1-남성, 2-여성, 4-키즈")
    private int pdtGroup;
    //"카테고리UID")
    private int categoryUid;
    //"모델명")
    private String pdtModel = "";
    //"사이즈")
    private String pdtSize = "";
    //"컬러")
    private String colorName = "";
    private int status = 1;
}
