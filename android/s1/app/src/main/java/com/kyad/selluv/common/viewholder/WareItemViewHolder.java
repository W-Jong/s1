package com.kyad.selluv.common.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.RoundRectCornerImageView;

import me.grantland.widget.AutofitTextView;

public class WareItemViewHolder extends ViewHolder {
    public FrameLayout padding_start;
    public FrameLayout padding_end;
    public RoundRectCornerImageView iv_ware_img;
    public CircularImageView iv_photo;
    public ImageView iv_ministyle;
    public RelativeLayout rl_mini_style;
    public ImageView iv_heart;
    public ImageView iv_ware_stat;
    public AutofitTextView tv_ware_eng_name;
    public AutofitTextView tv_ware_name;
    public TextView tv_ware_price;
    public TextView tv_sale_price;
    public TextView tv_sale_rate;
    public LinearLayout ll_sale_info;
    public TextView tv_size;
    public TextView tv_like_cnt;
    public TextView tv_nickname;
    public TextView tv_update_time;
    public RelativeLayout rl_like;
    public ImageView iv_price_down;

    public WareItemViewHolder(View itemView) {
        super(itemView);
    }
}
