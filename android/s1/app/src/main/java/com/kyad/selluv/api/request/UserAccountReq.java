package com.kyad.selluv.api.request;


import android.support.annotation.Size;

import lombok.Data;


@Data
public class UserAccountReq {

    @Size(min = 1)
    //@ApiModelProperty("은행명")
    private String bankNm;
    @Size(min = 1)
    //@ApiModelProperty("계좌번호")
    private String accountNum;
}
