package com.kyad.selluv.api.dto.other;

import com.kyad.selluv.api.dto.usr.UsrMiniDto;

import lombok.Data;

@Data
public class ReviewListDto {
    //("후기")
    private ReviewDao review;
    //("유저")
    private UsrMiniDto peerUsr;
}
