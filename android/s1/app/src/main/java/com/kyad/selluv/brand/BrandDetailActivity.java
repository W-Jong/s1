package com.kyad.selluv.brand;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandDetailDto;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.top.TopLikeActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static android.support.design.widget.TabLayout.OnTabSelectedListener;
import static android.support.design.widget.TabLayout.Tab;

public class BrandDetailActivity extends BaseActivity {
    public static final String BRAND_UID = "BRAND_UID";
    public static final String BRAND_PRODUCT_GROUP = "BRAND_PRODUCT_GROUP";

    //UI Reference
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.tb_follow)
    ToggleButton tb_follow;
    @BindView(R.id.tv_brand_title)
    TextView tv_brand_title;
    @BindView(R.id.iv_brand_logo)
    ImageView iv_brand_logo;
    @BindView(R.id.rl_top)
    RelativeLayout rl_top;
    @BindView(R.id.rl_bg)
    RelativeLayout rl_bg;
    @BindView(R.id.rl_tab)
    RelativeLayout rl_tab;
    @BindView(R.id.ib_more)
    ImageButton ib_more;
    @BindView(R.id.ib_right)
    ImageButton ib_right;

    //Variables
    private PageAdapter pagerAdapter;
    WareListFragment frag_all, frag_male, frag_female, frag_kids, frag_style;
    public int selectedTab = 0;

    private int brandUid = 0;
    private int brandUGroup = -1;
    ArrayList<Fragment> arrPages = new ArrayList<Fragment>();
    private BrandDetailDto brandDetail = null;

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
//        BrandDetailActivity.this.getSupportFragmentManager().popBackStack();
        finish();
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    @OnClick(R.id.tb_follow)
    void onFollow(View v) {

    }

    @OnClick(R.id.ib_more)
    void onMore(View v) {
        if (brandDetail != null)
            Utils.showToast(this, brandDetail.getLicenseUrl());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_page);
        BarUtils.setStatusBarAlpha(this, 0, true);
        BarUtils.addMarginTopEqualStatusBarHeight(findViewById(R.id.tb_actionbar));
        brandUid = getIntent().getIntExtra(BRAND_UID, 0);
        brandUGroup = getIntent().getIntExtra(BRAND_PRODUCT_GROUP, -1);

        initAppBar();
//        initFragment();

        getBrandDetail();

        updateLikePdtBadge();
    }

    void initAppBar() {
        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) tv_brand_title.getLayoutParams();
        p.bottomMargin = 36;
        tv_brand_title.setLayoutParams(p);

        AppBarLayout app_bar_layout = (AppBarLayout) findViewById(R.id.appbar);
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) tv_brand_title.getLayoutParams();
                int h = Utils.dpToPixel(BrandDetailActivity.this, rl_bg.getHeight() - 56 - 20);
                int b = Utils.dpToPixel(BrandDetailActivity.this, 18) * (h + verticalOffset) / h;
                if (b < 0) b = 0;
                p.bottomMargin = b;
                tv_brand_title.setLayoutParams(p);
                final Animation animationFadeIn = AnimationUtils.loadAnimation(BrandDetailActivity.this, R.anim.fade_in);
                final Animation animationFadeOut = AnimationUtils.loadAnimation(BrandDetailActivity.this, R.anim.fade_out);
                if (verticalOffset <= -rl_bg.getHeight() / 3 * 2) {
                    tb_follow.startAnimation(animationFadeIn);
                    tb_follow.setVisibility(View.VISIBLE);
                    ib_more.startAnimation(animationFadeIn);
                    ib_more.setVisibility(View.VISIBLE);

                    tv_title.setVisibility(View.VISIBLE);
                    if (brandDetail == null || "".equals(brandDetail.getLogoImg())) {
                        tv_brand_title.startAnimation(animationFadeIn);
                        tv_brand_title.setVisibility(View.VISIBLE);
                    } else {
                        iv_brand_logo.startAnimation(animationFadeIn);
                        iv_brand_logo.setVisibility(View.VISIBLE);
                    }
                } else {
                    tb_follow.startAnimation(animationFadeOut);
                    tb_follow.setVisibility(View.VISIBLE);
                    ib_more.startAnimation(animationFadeOut);
                    ib_more.setVisibility(View.VISIBLE);

                    tv_title.setVisibility(View.INVISIBLE);
                    if (brandDetail == null || "".equals(brandDetail.getLogoImg())) {
                        tv_brand_title.startAnimation(animationFadeOut);
                        tv_brand_title.setVisibility(View.VISIBLE);
                    } else {
                        iv_brand_logo.startAnimation(animationFadeOut);
                        iv_brand_logo.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void initFragment() {
        LinearLayout appbar = findViewById(R.id.appbar);

        int allPstCnt = brandDetail.getMalePdtCount() + brandDetail.getFemalePdtCount() + brandDetail.getKidsPdtCount() + brandDetail.getStylePdtCount();
        if (allPstCnt > 0) {

            if (frag_all == null) {
                frag_all = new WareListFragment();
                frag_all.parentCollapsable = true;
                frag_all.showFilter = true;
                frag_all.showFilterButton = true;
                frag_all.isMiniImageShow = false;
                frag_all.isLazyLoading = false;
                frag_all.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
                frag_all.setFragType(Constants.WIRE_FRAG_BRAND);
                frag_all.setBrandUid(brandUid);
                frag_all.setTabString("ALL");
                frag_all.setAppbar(appbar);

                arrPages.add(frag_all);
            }
        } else {
            for (int i = 0; i < tl_top_tab.getTabCount(); i++){
                if (tl_top_tab.getTabAt(i).getText().equals("전체")){
                    tl_top_tab.removeTabAt(i);
                    break;
                }
            }
        }

        if (brandDetail.getMalePdtCount() > 0) {
            if (brandUGroup == 1) {
                selectedTab = arrPages.size();
            }

            if (frag_male == null) {
                frag_male = new WareListFragment();
                frag_male.parentCollapsable = true;
                frag_male.showFilter = true;
                frag_male.showFilterButton = true;
                frag_male.isMiniImageShow = false;
                frag_male.isLazyLoading = true;
                frag_male.listInfo = "남성 상품";
                frag_male.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
                frag_male.setFragType(Constants.WIRE_FRAG_BRAND);
                frag_male.setBrandUid(brandUid);
                frag_male.setTabString("MALE");
                frag_male.setAppbar(appbar);

                arrPages.add(frag_male);
            }
        } else {
            for (int i = 0; i < tl_top_tab.getTabCount(); i++){
                if (tl_top_tab.getTabAt(i).getText().equals("남성")){
                    tl_top_tab.removeTabAt(i);
                    break;
                }
            }
        }

        if (brandDetail.getFemalePdtCount() > 0) {
            if (brandUGroup == 2) {
                selectedTab = arrPages.size();
            }

            if (frag_female == null) {
                frag_female = new WareListFragment();
                frag_female.parentCollapsable = true;
                frag_female.showFilter = true;
                frag_female.showFilterButton = true;
                frag_female.isMiniImageShow = false;
                frag_female.isLazyLoading = true;
                frag_female.listInfo = "여성 상품";
                frag_female.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
                frag_female.setFragType(Constants.WIRE_FRAG_BRAND);
                frag_female.setBrandUid(brandUid);
                frag_female.setTabString("FEMALE");
                frag_female.setAppbar(appbar);

                arrPages.add(frag_female);
            }
        } else {
            for (int i = 0; i < tl_top_tab.getTabCount(); i++){
                if (tl_top_tab.getTabAt(i).getText().equals("여성")){
                    tl_top_tab.removeTabAt(i);
                    break;
                }
            }
        }

        if (brandDetail.getKidsPdtCount() > 0) {
            if (brandUGroup == 3) {
                selectedTab = arrPages.size();
            }

            if (frag_kids == null) {
                frag_kids = new WareListFragment();
                frag_kids.parentCollapsable = true;
                frag_kids.showFilter = true;
                frag_kids.showFilterButton = true;
                frag_kids.isMiniImageShow = false;
                frag_kids.isLazyLoading = true;
                frag_kids.listInfo = "키즈 상품";
                frag_kids.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
                frag_kids.setFragType(Constants.WIRE_FRAG_BRAND);
                frag_kids.setBrandUid(brandUid);
                frag_kids.setTabString("KIDS");
                frag_kids.setAppbar(appbar);

                arrPages.add(frag_kids);
            }
        } else {
            for (int i = 0; i < tl_top_tab.getTabCount(); i++){
                if (tl_top_tab.getTabAt(i).getText().equals("키즈")){
                    tl_top_tab.removeTabAt(i);
                    break;
                }
            }
        }

        if (brandDetail.getMalePdtCount() > 0) {

            if (frag_style == null) {
                frag_style = new WareListFragment();
                frag_style.parentCollapsable = true;
                frag_style.showFilter = true;
                frag_style.showFilterButton = true;
                frag_style.isLazyLoading = true;
                frag_style.listInfo = "전체 스타일";
                frag_style.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.none_style_img), "", "", null);
                frag_style.setDataType(Constants.DATA_STYLE);
                frag_style.setFragType(Constants.WIRE_FRAG_BRAND);
                frag_style.setBrandUid(brandUid);
                frag_style.setTabString("STYLE");
                frag_style.setAppbar(appbar);

                arrPages.add(frag_style);
            }
        } else {
            for (int i = 0; i < tl_top_tab.getTabCount(); i++){
                if (tl_top_tab.getTabAt(i).getText().equals("스타일")){
                    tl_top_tab.removeTabAt(i);
                    break;
                }
            }
        }

        tl_top_tab.setVisibility(View.VISIBLE);


        tl_top_tab.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(Tab tab) {
            }

            @Override
            public void onTabReselected(Tab tab) {
            }
        });
        pagerAdapter = new PageAdapter(BrandDetailActivity.this.getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
                WareListFragment targetFragment = ((WareListFragment) pagerAdapter.getItem(position));
                targetFragment.removeAll();
                targetFragment.refresh();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        vp_main.setOffscreenPageLimit(5);
        vp_main.setCurrentItem(selectedTab);

        if (arrPages.size() > 0) {
            tl_top_tab.getTabAt(selectedTab).select();
            setSelectedTabStyle(0);
        }
    }

    private void loadLayout() {
        if (brandDetail == null)
            return;

        if (brandDetail.getBrandLikeStatus())
            tb_follow.setText(getString(R.string.following) + " " + String.valueOf(brandDetail.getBrandLikeCount()));
        else
            tb_follow.setText(getString(R.string.follow) + " " + String.valueOf(brandDetail.getBrandLikeCount()));

        if (!"".equals(brandDetail.getLogoImg())) {
            ImageUtils.load(this, brandDetail.getLogoImg(), android.R.color.transparent, R.drawable.img_default, iv_brand_logo);
            tv_brand_title.setVisibility(View.INVISIBLE);
            tv_brand_title.setText(brandDetail.getNameEn());
        } else {
            iv_brand_logo.setVisibility(View.INVISIBLE);
            tv_brand_title.setVisibility(View.VISIBLE);
            tv_brand_title.setText(brandDetail.getNameEn());
        }
        tv_title.setText(brandDetail.getNameEn());

        tb_follow.setChecked(brandDetail.getBrandLikeStatus());
        tb_follow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                brandLike();
            }
        });

        if (!"".equals(brandDetail.getBackImg())) {
            Glide.with(this).load(brandDetail.getBackImg())
                    .apply(new RequestOptions().placeholder(android.R.color.transparent).error(R.drawable.img_default))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            rl_bg.setBackground(resource);
                        }
                    });
        } else {
            rl_bg.setBackgroundResource(R.drawable.img_default);
        }
    }

    private void setSelectedTabStyle(int index) {
//        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
//        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
//        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
//        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
//        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, 4, null, 15, Typeface.NORMAL);
//        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, index, null, 15, Typeface.BOLD);

        for (int i = 0; i < arrPages.size(); i++) {
            Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, i, null, 15, Typeface.NORMAL);
        }
        Utils.setTabItemFontAndSize(BrandDetailActivity.this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(this) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void getBrandDetail() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<BrandDetailDto>> genRes = restAPI.brandDetail(Globals.userToken, brandUid);

        genRes.enqueue(new TokenCallback<BrandDetailDto>(this) {
            @Override
            public void onSuccess(BrandDetailDto response) {
                closeLoading();
                brandDetail = response;

                loadLayout();
                initFragment();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                finish();
            }
        });
    }

    private void brandLike() {
        if (brandDetail == null)
            return;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        Call<GenericResponse<Void>> genRes = !brandDetail.getBrandLikeStatus() ? restAPI.likeBrand(Globals.userToken, brandDetail.getBrandUid()) : restAPI.unLikeBrand(Globals.userToken, brandDetail.getBrandUid());
        genRes.enqueue(new TokenCallback<Void>(this) {

            @Override
            public void onSuccess(Void response) {
                if (brandDetail.getBrandLikeStatus()) {
                    brandDetail.setBrandLikeStatus(false);
                    brandDetail.setBrandLikeCount(brandDetail.getBrandLikeCount() - 1);
                } else {
                    brandDetail.setBrandLikeStatus(true);
                    brandDetail.setBrandLikeCount(brandDetail.getBrandLikeCount() + 1);
                }
                tb_follow.setText(getString(R.string.following) + " " + String.valueOf(brandDetail.getBrandLikeCount()));
                tb_follow.setChecked(brandDetail.getBrandLikeStatus());
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return arrPages.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return arrPages.size();
        }
    }
}
