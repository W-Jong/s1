package com.kyad.selluv.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.DbManager;
import com.kyad.selluv.MainActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.ShareManager;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.ResponseMeta;
import com.kyad.selluv.api.dto.usr.UsrLoginDto;
import com.kyad.selluv.api.enumtype.LOGIN_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Constants.Direction;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.fragment.EmailRegisterFragment;
import com.kyad.selluv.login.fragment.LoginRevisitFragment;
import com.kyad.selluv.login.fragment.LoginStartFragment;
import com.kyad.selluv.login.fragment.SnsRegisterFragment;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.webview.CommonWebViewActivity;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.data.OAuthLoginState;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.ShareManager.SM_AUTO_LOGIN;
import static com.kyad.selluv.ShareManager.SM_LAST_LOGIN_ID;
import static com.kyad.selluv.common.Constants.IS_FIRST;
import static com.kyad.selluv.common.Globals.userSignReq;

/**
 * 로그인 선택 화면 157p
 */
public class LoginStartActivity extends BaseActivity {

    private final int PAGE_LOGIN_SELECT = 0;
    private final int PAGE_ID_LOGIN = 1;
    private final int PAGE_EMAIL_REG = 2;
    private final int PAGE_LOGIN_REVISIT = 3;

    //Variables
    private FragmentManager fm;
    private FragmentTransaction transaction;
    /**
     * 로그인 선택 Page
     */
    private LoginStartFragment loginStartFragment;
    /**
     * SNS 회원가입 Page
     */
    private SnsRegisterFragment snsRegisterFragment;
    /**
     * 이메일 회원가입 Page
     */
    private EmailRegisterFragment emailRegisterFragment;
    /**
     * 로그아웃 이후 로그인
     */
    private LoginRevisitFragment loginRevisitFragment;
    /**
     * 현재 Page 여부
     */
    private int currentPage = PAGE_LOGIN_SELECT;
    /**
     * 첫 기동인지 아니면 앱이용중에 로그인하려고 하는건지
     */
    private boolean isStartApp = true;

    /**
     * 페북콜백
     */
    private CallbackManager callbackManager;
    /**
     * 카톡콜백
     */
    private SessionCallback callback;
    /**
     * 네이버 콜백
     */
    private OAuthLogin mOAuthLoginModule;
    /**
     * SNS토큰 임시저장
     */
    private String strSnsToken;
    /**
     * SNS아이디 임시저장
     */
    private String strSnsID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        isStartApp = getIntent().getBooleanExtra(IS_FIRST, true);

        //facebook
        callbackManager = CallbackManager.Factory.create();
        //kakaotalk
        callback = new SessionCallback();                  // 이 두개의 함수 중요함
        Session.getCurrentSession().addCallback(callback);
        //naver
        mOAuthLoginModule = OAuthLogin.getInstance();
        mOAuthLoginModule.init(
                LoginStartActivity.this
                , "Khgw7OI4mtNP47Hv9Jwq"
                , "K94EMljc5Q"
                , getString(R.string.app_name)
                //,OAUTH_CALLBACK_INTENT
                // SDK 4.1.4 버전부터는 OAUTH_CALLBACK_INTENT변수를 사용하지 않습니다.
        );

        loadLayout();

        checkPermisson();
    }

    private void checkPermisson() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RxPermissions rxPermissions = new RxPermissions(LoginStartActivity.this);
                rxPermissions
                        .request(Manifest.permission.GET_ACCOUNTS, Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS)
                        .subscribe(granted -> {
                            if (granted) {
                                // cache directory empty

//                                try {
//                                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//                                    Log.d("Firbase id login", "Refreshed token: " + refreshedToken);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }

                                autoLogin();
                            } else {
                                // At least one permission is denied
                                Utils.showToast(LoginStartActivity.this, getString(R.string.grand_permisson));
                                finish();
                            }
                        });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (callback != null) {
            Session.getCurrentSession().removeCallback(callback);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }
        if (requestCode == CommonWebViewActivity.WEBVIEW_DANAL_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_DANAL_SUCCESS, false);
            if (isSuccess) {
                userSignReq.setBirthday(data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_BIRTH));
                userSignReq.setGender(data.getIntExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_GENDER, 1));
                userSignReq.setUsrNm(data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_NAME));
                userSignReq.setUsrPhone(data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_PHONE));

                register();
            } else {
                Utils.showToast(this, "본인인증에 실패했습니다.");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        //해당 프레그먼트에 백버튼 사건을 넘긴다.
        if (currentPage == PAGE_LOGIN_SELECT) {
            if (loginStartFragment.loginStartTabIndex == 1) {
                loginStartFragment.onBackToStart(null);
                return;
            }
        } else if (currentPage == PAGE_LOGIN_REVISIT) {
            if (loginRevisitFragment.loginRevisitTabIndex == 1) {
                loginRevisitFragment.onBackToRevisit(null);
                return;
            }
        } else if (currentPage == PAGE_EMAIL_REG) {
            emailRegisterFragment.onBack(null);
            return;
        } else if (currentPage == PAGE_ID_LOGIN) {
            snsRegisterFragment.onBack(null);
            return;
        }

        super.onBackPressed();
    }


    private void loadLayout() {
        fm = getSupportFragmentManager();

        //현재 화면의 크기 얻기
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Globals.screenSize = new Point();
        display.getSize(Globals.screenSize);
    }

    //자동로그인 체크
    private void autoLogin() {

        String userid = ShareManager.manager().getValue(SM_LAST_LOGIN_ID, "");
        Model.UserLoginInfo lastLoginInfo = null;
        if (!userid.isEmpty()) {
            lastLoginInfo = DbManager.loadInfo(userid);
        }
        if (ShareManager.manager().getValue(SM_AUTO_LOGIN, false) && lastLoginInfo != null) {
            login(lastLoginInfo.userid, lastLoginInfo.password, lastLoginInfo.login_type, true);
        } else {
            List<Model.UserLoginInfo> loginUsers = DbManager.loadInfo();
            //로그아웃했다고 다시 로그인 했을 때 이전 로그인 정보가 있는지 체크....
            if (loginUsers.size() > 0) {
                gotoRevisitFragment(0, Direction.BOTTOM);
            } else {
                gotoStartFragment(0, Direction.BOTTOM);
            }
        }
    }

    private void goToBrancSelect() {
        Intent intent = new Intent(LoginStartActivity.this, BrandSelectActivity.class);
        intent.putExtra(IS_FIRST, true);
        startActivity(intent);
        finish();
    }

    public void gotoRevisitFragment(int prevChildCnt, Constants.Direction from) {
        currentPage = PAGE_LOGIN_REVISIT;
        loginRevisitFragment = LoginRevisitFragment.newInstance(prevChildCnt, from);
        transaction = fm.beginTransaction();
        transaction.replace(R.id.rl_fragment, loginRevisitFragment);
        transaction.commit();
    }

    public void gotoStartFragment(int prevChildCnt, Direction from) {
        currentPage = PAGE_LOGIN_SELECT;
        loginStartFragment = LoginStartFragment.newInstance(prevChildCnt, from);
        transaction = fm.beginTransaction();
        transaction.replace(R.id.rl_fragment, loginStartFragment);
        transaction.commit();
    }

    public void gotoSnsRegisterFragment(int prevChildCnt, Direction from, boolean isRevisit) {
        currentPage = PAGE_ID_LOGIN;
        snsRegisterFragment = SnsRegisterFragment.newInstance(prevChildCnt, from, isRevisit);
        transaction = fm.beginTransaction();
        transaction.replace(R.id.rl_fragment, snsRegisterFragment);
        transaction.commit();
    }

    public void gotoEmailRegisterFragment(int prevChildCnt, Direction from, boolean isRevisit) {
        currentPage = PAGE_EMAIL_REG;
        emailRegisterFragment = EmailRegisterFragment.newInstance(prevChildCnt, from, isRevisit);
        transaction = fm.beginTransaction();
        transaction.replace(R.id.rl_fragment, emailRegisterFragment);
        transaction.commit();
    }

    public void facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("kyad-log", "Success");
                        strSnsToken = loginResult.getAccessToken().getToken();
                        strSnsID = loginResult.getAccessToken().getUserId();

                        getFacebookUserInfo(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("kyad-log", "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("kyad-log", "Error");
                        Toast.makeText(LoginStartActivity.this, "페이스북 로그인에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void kakaotalkLogin() {
        Session.getCurrentSession().open(AuthType.KAKAO_TALK, LoginStartActivity.this);
    }

    public void naverLogin() {
        if (OAuthLoginState.OK.equals(OAuthLogin.getInstance().getState(this))) {
            // access token 이 있는 상태로 로그인 버튼 안보여 줌
            strSnsToken = OAuthLogin.getInstance().getAccessToken(this);
            getNaverUserInfo();
        } else {
            mOAuthLoginModule.startOauthLoginActivity(LoginStartActivity.this, mOAuthLoginHandler);
        }

    }


    public void cmdLoginSuccess() {
        if (isStartApp) {
            startActivity(MainActivity.class, true, 0, 0);
        } else {
            finish();
        }
    }

    private void getKakaoUserInfo() {
        showLoading();
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                closeLoading();
                int ErrorCode = errorResult.getErrorCode();
                int ClientErrorCode = -777;

                if (ErrorCode == ClientErrorCode) {
                    Toast.makeText(getApplicationContext(), "카카오톡 서버의 네트워크가 불안정합니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("TAG", "오류로 카카오로그인 실패 ");
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                closeLoading();
                Log.d("TAG", "오류로 카카오로그인 실패 ");
            }

            @Override
            public void onSuccess(UserProfile userProfile) {
                String snsEmail = userProfile.getEmail();
                String snsNickname = userProfile.getNickname();
                strSnsID = "" + userProfile.getId();
                strSnsToken = strSnsID;

                if (snsEmail == null || snsEmail.isEmpty()) {
                    snsEmail = strSnsID + "@kakao.selluv";
//                    Toast.makeText(getApplicationContext(), "이메일주소얻기 권한을 설정해주세요.", Toast.LENGTH_SHORT).show();
//                    return;
                } else if (snsNickname == null) {
                    snsNickname = strSnsID;
                }
                userSignReq.setSnsId(strSnsID);
                userSignReq.setPassword(strSnsToken);
                userSignReq.setUsrMail(snsEmail);
                userSignReq.setUsrNckNm(snsNickname);
                userSignReq.setLoginType(LOGIN_TYPE.KAKAOTALK);

                login(strSnsID, strSnsToken, LOGIN_TYPE.KAKAOTALK.toString());
            }

            @Override
            public void onNotSignedUp() {
                closeLoading();
                Toast.makeText(getBaseContext(), R.string.kakao_login_not_signup, Toast.LENGTH_SHORT).show();
                // 자동가입이 아닐경우 동의창
            }
        }/*, propertyKeys, false*/);
    }

    /**
     * 페이스북 유저 정보 얻기
     *
     * @param accessToken
     */
    private void getFacebookUserInfo(AccessToken accessToken) {
        showLoading();

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                closeLoading();

                try {
                    String snsEmail = jsonObject.getString("email");
                    String snsNickname = jsonObject.getString("name");

                    if (snsEmail == null || snsEmail.isEmpty()) {
                        snsEmail = strSnsID + "@facebook.selluv";
                        //Toast.makeText(getApplicationContext(), "이메일주소얻기 권한을 설정해주세요.", Toast.LENGTH_SHORT).show();
                        //return;
                    } else if (snsNickname == null) {
                        snsNickname = strSnsID;
                    }

                    userSignReq.setSnsId(strSnsID);
                    userSignReq.setPassword(strSnsID);
                    userSignReq.setUsrMail(snsEmail);
                    userSignReq.setUsrNckNm(snsNickname);
                    userSignReq.setLoginType(LOGIN_TYPE.FACEBOOK);

                    login(strSnsID, strSnsID, LOGIN_TYPE.FACEBOOK.toString());

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "페이스북정보를 얻는데 실패했습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,age_range,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getNaverUserInfo() {
        showLoading();
        RequestApiTask task = new RequestApiTask();
        task.execute(strSnsToken);
    }

    public void login(String userId, String password, String loginType) {
        login(userId, password, loginType, false);
    }

    private void login(String userId, final String password, final String loginType, final boolean isAutoLogin) {
        showLoading();

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("loginType", loginType);
            paramObject.put("osTp", "ANDROID");
            paramObject.put("password", password);
            paramObject.put("usrId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<UsrLoginDto>> genRes = restAPI.login(RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), paramObject.toString()));
        genRes.enqueue(new Callback<GenericResponse<UsrLoginDto>>() {

            @Override
            public void onResponse(Call<GenericResponse<UsrLoginDto>> call, Response<GenericResponse<UsrLoginDto>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    UsrLoginDto loginDto = response.body().getData();
                    Globals.userToken = loginDto.getAccessToken();
                    DbManager.saveInfo(loginDto.getUsr().getUsrId(), password, loginDto.getUsr().getUsrNckNm(), loginDto.getUsr().getProfileImg(), Globals.getLoginTypeStr(loginDto.getUsr().getUsrLoginType()));
                    ShareManager.manager().put(SM_LAST_LOGIN_ID, loginDto.getUsr().getUsrId());
                    cmdLoginSuccess();
                } else if (!loginType.equals(LOGIN_TYPE.NORMAL.toString()) && response.body().getMeta().getErrCode() == ResponseMeta.USER_NOT_EXISTS.getErrCode()) {
                    if (isAutoLogin) {
                        gotoStartFragment(0, Direction.BOTTOM);
                    } else {
                        if (currentPage == PAGE_LOGIN_SELECT) {
                            loginStartFragment.setCloseDirection(Direction.TOP);
                            gotoSnsRegisterFragment(loginStartFragment.childCnt, Direction.BOTTOM, false);
                        } else {
                            loginRevisitFragment.setCloseDirection(Direction.TOP);
                            gotoSnsRegisterFragment(0, Direction.BOTTOM, true);
                        }
                    }
                } else {//실패
                    Toast.makeText(LoginStartActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                    if (isAutoLogin)
                        gotoStartFragment(0, Direction.BOTTOM);
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<UsrLoginDto>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(LoginStartActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                if (isAutoLogin)
                    gotoStartFragment(0, Direction.BOTTOM);
            }
        });
    }

    public void register() {
//        if (Constants.IS_TEST) {
//            //kkk - 실명인증 후 처리
//            userSignReq.setBirthday("1988-07-21");
//            userSignReq.setGender(1);
//            userSignReq.setUsrNm("겐지1");
//            userSignReq.setUsrPhone("01012345628");
//        }
        showLoading();
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<UsrLoginDto>> genRes = restAPI.registerUser(userSignReq);
        genRes.enqueue(new Callback<GenericResponse<UsrLoginDto>>() {

            @Override
            public void onResponse(Call<GenericResponse<UsrLoginDto>> call, Response<GenericResponse<UsrLoginDto>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    UsrLoginDto loginDto = response.body().getData();
                    Globals.userToken = loginDto.getAccessToken();
                    DbManager.saveInfo(loginDto.getUsr().getUsrId(), userSignReq.getPassword(), loginDto.getUsr().getUsrNckNm(), loginDto.getUsr().getProfileImg(), Globals.getLoginTypeStr(loginDto.getUsr().getUsrLoginType()));
                    ShareManager.manager().put(SM_LAST_LOGIN_ID, loginDto.getUsr().getUsrId());
                    goToBrancSelect();
                } else {//실패
                    Toast.makeText(LoginStartActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<UsrLoginDto>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(LoginStartActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * 카카오 session
     */
    private class SessionCallback implements ISessionCallback {

        @Override
        public void onSessionOpened() {
            getKakaoUserInfo(); // 세션 연결성공 시 redirectSignupActivity() 호출
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if (exception != null) {
                Logger.e(exception);
                Toast.makeText(LoginStartActivity.this, "카카오톡로그인에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * 네이버 로그인
     */
    private OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            if (success) {
                strSnsToken = mOAuthLoginModule.getAccessToken(LoginStartActivity.this);

                getNaverUserInfo();
            } else {
                String errorCode = mOAuthLoginModule.getLastErrorCode(LoginStartActivity.this).getCode();
                String errorDesc = mOAuthLoginModule.getLastErrorDesc(LoginStartActivity.this);
                if (!errorCode.equals("user_cancel")) {
//                    Toast.makeText(LoginStartActivity.this, "errorCode:" + errorCode
//                            + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
                    Toast.makeText(LoginStartActivity.this, "네이버로그인에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        }

        ;
    };

    /**
     * 네이버 유저정보 얻기
     */
    private class RequestApiTask extends AsyncTask<String, Void, String> {

        String result;

        @Override
        protected String doInBackground(String... params) {
            String token = params[0];// 네이버 로그인 접근 토큰;
            String header = "Bearer " + token; // Bearer 다음에 공백 추가
            try {
                String apiURL = "https://openapi.naver.com/v1/nid/me";
                URL url = new URL(apiURL);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Authorization", header);
                int responseCode = con.getResponseCode();
                BufferedReader br;
                if (responseCode == 200) { // 정상 호출
                    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                } else {  // 에러 발생
                    br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                }
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    response.append(inputLine);
                }
                result = response.toString();
                br.close();
                System.out.println(response.toString());
            } catch (Exception e) {
                System.out.println(e);
            }
            //result 값은 JSONObject 형태로 넘어옵니다.
            return result;

        }

        protected void onPostExecute(String content) {
            super.onPostExecute(content);
            closeLoading();
            try {
                //넘어온 result 값을 JSONObject 로 변환해주고, 값을 가져오면 되는데요.
                // result 를 Log에 찍어보면 어떻게 가져와야할 지 감이 오실거에요.
                JSONObject object = new JSONObject(result);
                if (object.getString("resultcode").equals("00")) {
                    JSONObject jsonObject = new JSONObject(object.getString("response"));
                    Log.d("jsonObject", jsonObject.toString());

                    strSnsID = jsonObject.getString("id");
                    String snsEmail = jsonObject.getString("email");
                    String snsNickname = jsonObject.getString("nickname");

                    if (snsEmail == null || snsEmail.isEmpty()) {
                        snsEmail = strSnsID + "@naver.selluv";
                    } else if (snsNickname == null) {
                        snsNickname = strSnsID;
                    }
                    userSignReq.setSnsId(strSnsID);
                    userSignReq.setPassword(strSnsToken);
                    userSignReq.setUsrMail(snsEmail);
                    userSignReq.setUsrNckNm(snsNickname);
                    userSignReq.setLoginType(LOGIN_TYPE.NAVER);

                    login(strSnsID, strSnsID, LOGIN_TYPE.NAVER.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
            closeLoading();
        }
    }
}
