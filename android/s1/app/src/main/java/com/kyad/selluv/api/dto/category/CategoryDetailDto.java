package com.kyad.selluv.api.dto.category;

import com.kyad.selluv.api.dto.brand.BrandRecommendedListDto;

import java.util.List;

import lombok.Data;

@Data
public class CategoryDetailDto {
    private CategoryDao category;
    private List<BrandRecommendedListDto> recommendedBrand;
    private List<CategoryDao> childrenCategories;
}
