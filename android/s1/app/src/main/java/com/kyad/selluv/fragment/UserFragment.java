/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.MannerListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.mypage.ReviewContentDto;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.model.Model;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class UserFragment extends BaseFragment {

    private int usrUid = 0;
    private int pageIdx = -1;
    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;



    public UserFragment() {
    }

    //UI Referencs
    @BindView(R.id.tv_deal)
    TextView tv_deal;
    @BindView(R.id.tv_count)
    TextView tv_count;
    @BindView(R.id.lv_list)
    ListView lv_list;


    //Vars
    View rootView;
    private MannerListAdapter adapter;


    public void setTab(int index, int _uid) {
        usrUid = _uid;
        currentPage = 0;
        isLast = false;
        isLoading = false;

        if (index == 0){
            pageIdx = -1;
            tv_deal.setText(getString(R.string.deal));
        } else if (index == 1){
            pageIdx = 2;
            tv_deal.setText(getString(R.string.evaluation_good));
        } else if (index == 2){
            pageIdx = 1;
            tv_deal.setText(getString(R.string.evaluation_normal));
        } else {
            pageIdx = 0;
            tv_deal.setText(getString(R.string.evaluation_bad));
        }
        getMannerPoint();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, rootView);
        adapter = new MannerListAdapter((BaseActivity) getActivity());
        loadLayout();
        loadData();
        return rootView;

    }

    private void loadLayout() {
        lv_list.setAdapter(adapter);
        lv_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getMannerPoint();
                }
            }
        });
    }

    private void loadData() {
        if (adapter != null) {
//            Collections.addAll(adapter.arrData, loadInterface.loadData());
            adapter.notifyDataSetChanged();
        }

    }


    //get notice list
    private void getMannerPoint(){
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<ReviewContentDto>> genRes = restAPI.getMannerPoint(Globals.userToken, usrUid, pageIdx, currentPage);

        genRes.enqueue(new TokenCallback<ReviewContentDto>(getActivity()) {
            @Override
            public void onSuccess(ReviewContentDto response) {
                closeLoading();
                isLoading = false;

                if (currentPage == 0) {
                    adapter.arrData.clear();
                }
                isLast = response.getReviews().isLast();

                adapter.arrData.addAll(response.getReviews().getContent());
                adapter.notifyDataSetChanged();

                tv_count.setText("(" + response.getReviews().getTotalElements() + ")");
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }

}
