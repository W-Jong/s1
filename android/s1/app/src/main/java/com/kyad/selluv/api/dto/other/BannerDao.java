package com.kyad.selluv.api.dto.other;

import lombok.Data;

@Data
public class BannerDao {
    private int bannerUid;
    private long endTime; // 종료 시간 ,
    private String profileImg; // 배너이미지 시간 ,
    private long regTime; //(integer, optional): 추가된 시간 ,
    private long startTime; // (integer, optional): 시작 시간 ,
    private int status; // (integer, optional),
    private int targetUid; // (integer, optional): 분류에 따르는 타겟 UID ,
    private String title; // (string, optional): 배너타이틀 ,
    private int type; // (integer, optional): 분류 1-공지사항 2-테마 3-이벤트
}
