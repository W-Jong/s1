package com.kyad.selluv;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.preference.PrefConst;
import com.kyad.selluv.common.preference.Preference;

/**
 * Created by pjh on 2018. 05. 16..
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    String refreshedToken;

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        sendRegistrationToServer(refreshedToken);
    }


    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

        Preference.getInstance().putSharedPreference(getApplicationContext(), PrefConst.PREFCONST_PUSH_TOKEN, token);
    }
}
