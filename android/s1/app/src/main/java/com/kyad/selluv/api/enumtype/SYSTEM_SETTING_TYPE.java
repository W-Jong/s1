package com.kyad.selluv.api.enumtype;

public enum SYSTEM_SETTING_TYPE {
    DEAL_REWARD(1), BUY_REVIEW_REWARD(2), SELL_REVIEW_REWARD(3),
    FAKE_REWARD(4), SHARE_REWRAD(5), INVITER_REWRAD(6),
    INVITED_USER_REWARD(7), INVITED_USER_FIRST_DEAL_REWARD(8);

    private int code;

    private SYSTEM_SETTING_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
