package com.kyad.selluv.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.mypage.EventDetailActivity;
import com.kyad.selluv.mypage.NoticeDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class EventListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<EventDao> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public EventListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<EventDao> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_news, parent, false);
            final EventDao anItem = (EventDao) getItem(position);
            if (anItem == null) {
                return convertView;
            }

            ImageView img = convertView.findViewById(R.id.iv_img);
            ImageUtils.load(img.getContext(), anItem.getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, img);

            final int index = position;
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, EventDetailActivity.class);
                    intent.putExtra(Constants.EVT_UID, arrData.get(index).getEventUid());
                    activity.startActivity(intent);
                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}