package com.kyad.selluv.api.dto.usr;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class EventDao {
    private int eventUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"마이페이지 노출여부 1-노출, 0-노출안함")
    private int mypageYn;
    //"타이틀")
    private String title;
    //"썸네일이미지")
    private String profileImg;
    //"상세이미지")
    private String detailImg;
    private int status;
}
