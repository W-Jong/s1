package com.kyad.selluv.mypage;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.DeliveryHistoryDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDetailDto;
import com.kyad.selluv.api.dto.other.SystemSettingDto;
import com.kyad.selluv.api.dto.other.WorkingDayDto;
import com.kyad.selluv.api.request.WorkingDayReq;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.DockAnimation;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesDetailActivity extends BaseActivity {

    public static final String DEAL_UID = "DEAL_UID";

    private int dealUid = 0;
    private DealDetailDto dealDetail;

    //UI Reference
    @BindView(R.id.tv_order_number)
    TextView tv_order_number;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_status_text)
    TextView tv_status_text;
    @BindView(R.id.tv_company_num)
    TextView tv_company_num;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.btn_status1)
    Button btn_status1;
    @BindView(R.id.btn_status2)
    Button btn_status2;
    @BindView(R.id.iv_alert)
    ImageView iv_alert;
    @BindView(R.id.tv_order_state)
    TextView tv_order_state;
    @BindView(R.id.tv_post_price)
    TextView tv_post_price;


    @BindView(R.id.ib_drop_pay)
    ImageButton ib_drop_pay;
    @BindView(R.id.ib_drop_max)
    ImageButton ib_drop_max;
    @BindView(R.id.rl_pay_detail)
    RelativeLayout rl_pay_detail;
    @BindView(R.id.rl_max_detail)
    RelativeLayout rl_max_detail;
    @BindView(R.id.rl_pay_info)
    RelativeLayout rl_pay_info;
    @BindView(R.id.rl_max_money)
    RelativeLayout rl_max_money;
    @BindView(R.id.rly_gita)
    RelativeLayout rly_gita;
    @BindView(R.id.ll_pay)
    LinearLayout ll_pay;
    @BindView(R.id.rl_post)
    RelativeLayout rl_post;
    @BindView(R.id.rl_ware)
    RelativeLayout rl_ware;
    @BindView(R.id.rly_post_method)
    RelativeLayout rly_post_method;
    @BindView(R.id.rl_account)
    RelativeLayout rl_account;
    @BindView(R.id.ll_account_nodata)
    LinearLayout ll_account_nodata;
    @BindView(R.id.rl_account_data)
    RelativeLayout rl_account_data;
    @BindView(R.id.lly_refund)
    LinearLayout lly_refund;

    //pdt
    @BindView(R.id.iv_ware)
    ImageView iv_ware;
    @BindView(R.id.tv_brand_name)
    TextView tv_brand_name;
    @BindView(R.id.tv_ware_name)
    TextView tv_pdt_content;
    @BindView(R.id.tv_size)
    TextView tv_size;
    @BindView(R.id.tv_seller_name)
    TextView tv_seller_name;
    @BindView(R.id.tv_shipping_charge)
    TextView tv_shipping_charge;
    @BindView(R.id.tv_shipping_title)
    TextView tv_shipping_title;


    //배송정보
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_contact)
    TextView tv_contact;
    @BindView(R.id.tv_address)
    TextView tv_address;

    //정산계좌
    @BindView(R.id.tv_account_name)
    TextView tv_account_name;
    @BindView(R.id.tv_bank)
    TextView tv_bank;
    @BindView(R.id.tv_num_address)
    TextView tv_num_address;

    //최대적립가능 셀럽머니
    @BindView(R.id.txt_buy_price_value)
    TextView txt_buy_price_value;
    @BindView(R.id.txt_after_price_value)
    TextView txt_after_price_value;
    @BindView(R.id.txt_max_total_price_value)
    TextView txt_max_total_price_value;
    @BindView(R.id.tv_max_amount)
    TextView tv_max_amount;

    //결제금액
    @BindView(R.id.txt_item_price_value)
    TextView txt_item_price_value;
    @BindView(R.id.txt_transport_price_value)
    TextView txt_transport_price_value;
    @BindView(R.id.txt_item_office_test_value)
    TextView txt_item_office_test_value;
    @BindView(R.id.txt_promotion_verify_value)
    TextView txt_promotion_verify_value;
    @BindView(R.id.txt_selluvmoney_use_value)
    TextView txt_selluvmoney_use_value;
    @BindView(R.id.tv_pay_amount)
    TextView tv_pay_amount;
    @BindView(R.id.txt_total_price_value)
    TextView txt_total_price_value;

    //반품 사유
    @BindView(R.id.tv_reason)
    TextView tv_reason;
    @BindView(R.id.ll_img_list)
    LinearLayout ll_img_list;
    @BindView(R.id.scv_img)
    HorizontalScrollView scv_img;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.rl_pay_info)
    void onDropPay() {
        showPayDetail();
    }

    @OnClick(R.id.rl_max_money)
    void onDropMax() {
        showMaxDetail();
    }

    @OnClick(R.id.btn_post_guide)
    void onPostGuide() {  //편의점 택배이용안내
        Intent intent = new Intent(SalesDetailActivity.this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.mypage_sales_post_guide);
        startActivity(intent);
    }

    @OnClick(R.id.rl_method1)
    void onConvenienceDelivery() {  //편의점 택배
        getDealDeliveryInfo();
    }

    @OnClick(R.id.rl_method2)
    void onNormalDelivery() {  //일반 택배

        Intent intent = new Intent(SalesDetailActivity.this, ShipActivity.class);
        intent.putExtra("uid", dealDetail.getDeal().getDealUid());
        intent.putExtra("verifyPrice", dealDetail.getDeal().getVerifyPrice());
        intent.putExtra("name", dealDetail.getDeal().getRecipientNm());
        intent.putExtra("phone", dealDetail.getDeal().getRecipientPhone());
        intent.putExtra("address", dealDetail.getDeal().getRecipientAddress());
        startActivity(intent);
    }

    @OnClick(R.id.btn_account_reg)
    void onAccountManage() {
        //계좌번호 등록
        startActivity(AccountActivity.class, false, 0, 0);
    }

    boolean payDropped;
    boolean maxDropped;

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_detail);
        setStatusBarWhite();

        dealUid = getIntent().getIntExtra(DEAL_UID, 0);
        if (Constants.IS_TEST) {
            Utils.showToast(this, "거래 UID: " + dealUid);
        }

        getDealDetailInfo();
    }

    private void loadLayout() {

        if (dealDetail == null)
            return;

        switch (dealDetail.getDeal().getStatus()) {
            case 11: //네고제안
                tv_status.setText(getString(R.string.order_status_title1));
                tv_status_text.setText(getString(R.string.order_status_title18_msg));
                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getNegoTime().getTime(), "yyyy-MM-dd hh:mm"));

                ll_pay.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                break;
            case 12: //카운터네고
                tv_status.setText(getString(R.string.order_status_title2));
                tv_status_text.setText(getString(R.string.order_status_title17_msg));
                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getNegoTime().getTime(), "yyyy-MM-dd hh:mm"));

                ll_pay.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);

                break;
            case 1: //배송준비
                tv_status.setText(getString(R.string.order_status_title3));
                tv_status_text.setText(getString(R.string.order_status_title3_msg));
                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));

                iv_alert.setVisibility(View.VISIBLE);
                tv_order_state.setText("제목을 발송해주세요.");
                tv_post_price.setText("운임 비용: " + Utils.getAttachCommaFormat((int) dealDetail.getDeal().getSendPrice()) + " 원");
                rl_post.setVisibility(View.GONE);
                getWorkingDay(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd"), 3);

                break;
            case 2: //배송진행
                tv_status.setText(getString(R.string.order_status_title4));
                tv_status_text.setText(getString(R.string.order_status_title19_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                rl_account.setVisibility(View.VISIBLE);
                if (Globals.myInfo.getUsr().getAccountNm().equals("") || Globals.myInfo.getUsr().getAccountNum().equals("") || Globals.myInfo.getUsr().getBankNm().equals("")) {
                    ll_account_nodata.setVisibility(View.VISIBLE);
                    rl_account_data.setVisibility(View.GONE);
                } else {
                    ll_account_nodata.setVisibility(View.GONE);
                    rl_account_data.setVisibility(View.VISIBLE);
                    tv_account_name.setText(Globals.myInfo.getUsr().getAccountNm());
                    tv_num_address.setText(Globals.myInfo.getUsr().getAccountNum());
                    tv_bank.setText(Globals.myInfo.getUsr().getBankNm());
                }
                break;

            case 3: //정품인증
                tv_status.setText(getString(R.string.order_status_title4));
                tv_status_text.setText(getString(R.string.order_status_title19_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.VISIBLE);
                rl_post.setVisibility(View.GONE);
                rl_account.setVisibility(View.VISIBLE);
                if (Globals.myInfo.getUsr().getAccountNm().equals("") || Globals.myInfo.getUsr().getAccountNum().equals("") || Globals.myInfo.getUsr().getBankNm().equals("")) {
                    ll_account_nodata.setVisibility(View.VISIBLE);
                    rl_account_data.setVisibility(View.GONE);
                } else {
                    ll_account_nodata.setVisibility(View.GONE);
                    rl_account_data.setVisibility(View.VISIBLE);
                    tv_account_name.setText(Globals.myInfo.getUsr().getAccountNm());
                    tv_num_address.setText(Globals.myInfo.getUsr().getAccountNum());
                    tv_bank.setText(Globals.myInfo.getUsr().getBankNm());
                }

                break;

            case 4: //배송완료
                tv_status.setText(getString(R.string.order_status_title6));
                tv_status_text.setText(getString(R.string.order_status_title20_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.VISIBLE);
                rl_post.setVisibility(View.GONE);
                rl_account.setVisibility(View.VISIBLE);
                if (Globals.myInfo.getUsr().getAccountNm().equals("") || Globals.myInfo.getUsr().getAccountNum().equals("") || Globals.myInfo.getUsr().getBankNm().equals("")) {
                    ll_account_nodata.setVisibility(View.VISIBLE);
                    rl_account_data.setVisibility(View.GONE);
                } else {
                    ll_account_nodata.setVisibility(View.GONE);
                    rl_account_data.setVisibility(View.VISIBLE);
                    tv_account_name.setText(Globals.myInfo.getUsr().getAccountNm());
                    tv_num_address.setText(Globals.myInfo.getUsr().getAccountNum());
                    tv_bank.setText(Globals.myInfo.getUsr().getBankNm());
                }

                break;

            case 10: //주문최소
                tv_status.setText(getString(R.string.order_status_title7));

                if (Utils.getDiffTime2(dealDetail.getDeal().getPayTime().getTime()) > 3) {
                    tv_status_text.setText(getString(R.string.order_status_title24_msg));
                } else {
                    tv_status_text.setText(getString(R.string.order_status_title9_msg));
                }

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.GONE);
                rl_account.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                ll_pay.setVisibility(View.GONE);

                break;

            case 5: //거래완료
                tv_status.setText(getString(R.string.order_status_title12));
                tv_status_text.setText(getString(R.string.order_status_title21_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.VISIBLE);
                rl_post.setVisibility(View.GONE);
                rl_account.setVisibility(View.VISIBLE);
                if (Globals.myInfo.getUsr().getAccountNm().equals("") || Globals.myInfo.getUsr().getAccountNum().equals("") || Globals.myInfo.getUsr().getBankNm().equals("")) {
                    ll_account_nodata.setVisibility(View.VISIBLE);
                    rl_account_data.setVisibility(View.GONE);
                } else {
                    ll_account_nodata.setVisibility(View.GONE);
                    rl_account_data.setVisibility(View.VISIBLE);
                    tv_account_name.setText(Globals.myInfo.getUsr().getAccountNm());
                    tv_num_address.setText(Globals.myInfo.getUsr().getAccountNum());
                    tv_bank.setText(Globals.myInfo.getUsr().getBankNm());
                }

                break;
            case 6: //정산완료
                tv_status.setText(getString(R.string.calculate_finish));
                tv_status_text.setText(getString(R.string.order_status_title22_msg));
                rly_gita.setVisibility(View.VISIBLE);
                btn_status1.setVisibility(View.VISIBLE);
                btn_status2.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                rl_account.setVisibility(View.VISIBLE);
                if (Globals.myInfo.getUsr().getAccountNm().equals("") || Globals.myInfo.getUsr().getAccountNum().equals("") || Globals.myInfo.getUsr().getBankNm().equals("")) {
                    ll_account_nodata.setVisibility(View.VISIBLE);
                    rl_account_data.setVisibility(View.GONE);
                } else {
                    ll_account_nodata.setVisibility(View.GONE);
                    rl_account_data.setVisibility(View.VISIBLE);
                    tv_account_name.setText(Globals.myInfo.getUsr().getAccountNm());
                    tv_num_address.setText(Globals.myInfo.getUsr().getAccountNum());
                    tv_bank.setText(Globals.myInfo.getUsr().getBankNm());
                }

                break;
            case 7: //반품신청
                tv_status.setText(getString(R.string.order_status_title9));
                tv_status_text.setText(getString(R.string.order_status_title23_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.VISIBLE);
                btn_status2.setVisibility(View.GONE);
                btn_status1.setVisibility(View.VISIBLE);
                rl_account.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                lly_refund.setVisibility(View.VISIBLE);

                showRefundData();

                break;

            case 8: //반품승인
                tv_status.setText(getString(R.string.order_status_title10));
                tv_status_text.setText(getString(R.string.order_status_title25_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.VISIBLE);
                btn_status2.setVisibility(View.GONE);
                btn_status1.setVisibility(View.VISIBLE);
                rl_account.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                lly_refund.setVisibility(View.VISIBLE);

                showRefundData();

                break;

            case 9: //반품완료
                tv_status.setText(getString(R.string.order_status_title11));
                tv_status_text.setText(getString(R.string.order_status_title26_msg));

                tv_date.setText(Utils.getStringDate(dealDetail.getDeal().getPayTime().getTime(), "yyyy-MM-dd hh:mm"));
                rly_gita.setVisibility(View.VISIBLE);
                btn_status2.setVisibility(View.GONE);
                btn_status1.setVisibility(View.VISIBLE);
                rl_account.setVisibility(View.GONE);
                rl_post.setVisibility(View.GONE);
                lly_refund.setVisibility(View.VISIBLE);

                tv_company_num.setText(dealDetail.getDeal().getDeliveryNumber());
                showRefundData();
                break;
        }

        setPdtData();

        btn_status1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //배송조회
                if (dealDetail.getDeal().getDeliveryNumber().equals(""))
                    return;

                String _url = Constants.DELIVERY_URL + dealDetail.getDeal().getDeliveryNumber();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
                startActivity(browserIntent);
            }
        });

        btn_status2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //정품감정서비스 안내
                Intent intent = new Intent(SalesDetailActivity.this, GuidImagePopup.class);
                intent.putExtra(Constants.POPUP_IMAGE, R.drawable.detail_buy_auth);
                startActivity(intent);

            }
        });

        rl_ware.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalesDetailActivity.this, DetailActivity.class);
                intent.putExtra(DetailActivity.PDT_UID, dealDetail.getDeal().getPdtUid());
                startActivity(intent);
            }
        });
    }

    private void setPdtData() {
        String _uid = String.valueOf(dealDetail.getDeal().getDealUid());
        String securityStr = "";
        for (int i = 0; i < 10 - _uid.length(); i++) {
            securityStr = securityStr + "0";
        }
        tv_order_number.setText("주문번호 " + securityStr + _uid);

        ImageUtils.load(iv_ware.getContext(), dealDetail.getDeal().getPdtImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_ware);
        tv_brand_name.setText(dealDetail.getDeal().getBrandEn());
        tv_pdt_content.setText(dealDetail.getDeal().getPdtTitle());
        tv_size.setText(dealDetail.getDeal().getPdtSize());
        tv_seller_name.setText(dealDetail.getUsr().getUsrNckNm());

        if (dealDetail.getDeal().getStatus() == 11) {
            tv_shipping_title.setVisibility(View.VISIBLE);
            tv_shipping_title.setText(getString(R.string.counter_nego2));
        } else if (dealDetail.getDeal().getStatus() == 12) {
            tv_shipping_title.setVisibility(View.VISIBLE);
            tv_shipping_title.setText(getString(R.string.counter_nego3));
        } else {
            tv_shipping_title.setVisibility(View.GONE);
        }
        tv_shipping_charge.setText(Utils.getAttachCommaFormat((int) dealDetail.getDeal().getReqPrice()) + " 원");

        //배송정보
        tv_name.setText(dealDetail.getDeal().getRecipientNm());
        tv_contact.setText(dealDetail.getDeal().getRecipientPhone());
        tv_address.setText(dealDetail.getDeal().getRecipientAddress());

        //정산금액
        txt_item_price_value.setText(Utils.getAttachCommaFormat((int) dealDetail.getDeal().getPdtPrice()));
        txt_transport_price_value.setText(Utils.getAttachCommaFormat((int) dealDetail.getDeal().getSendPrice()));
        txt_item_office_test_value.setText(Utils.getAttachCommaFormat((int) dealDetail.getDeal().getVerifyPrice()));
        if (dealDetail.getDeal().getPayPromotionPrice() == 0)
            txt_promotion_verify_value.setText(Utils.getAttachCommaFormat((int) dealDetail.getDeal().getPayPromotionPrice()));
        else
            txt_promotion_verify_value.setText("-" + Utils.getAttachCommaFormat((int) dealDetail.getDeal().getPayPromotionPrice()));
        if (dealDetail.getDeal().getSelluvPrice() == 0)
            txt_selluvmoney_use_value.setText(Utils.getAttachCommaFormat((int) dealDetail.getDeal().getSelluvPrice()));
        else
            txt_selluvmoney_use_value.setText("-" + Utils.getAttachCommaFormat((int) dealDetail.getDeal().getSelluvPrice()));

        long totalMoney = dealDetail.getDeal().getPdtPrice() + dealDetail.getDeal().getSendPrice() - dealDetail.getDeal().getSelluvPrice() + dealDetail.getDeal().getVerifyPrice() - dealDetail.getDeal().getPayPromotionPrice();

        tv_pay_amount.setText(Utils.getAttachCommaFormat((int) totalMoney));
        txt_total_price_value.setText(Utils.getAttachCommaFormat((int) totalMoney));

        //최대 적립가능 셀럽머니
        getSystemSettingInfo((int) totalMoney);
    }

    void showPayDetail() {
        rl_pay_info.setClickable(false);
        DockAnimation m_aniSlideShow = new DockAnimation(rl_pay_detail, 300,
                DockAnimation.FRAME_LAYOUT);

        rl_pay_detail.setVisibility(View.VISIBLE);

        int from = !payDropped ? 0 : Utils.dpToPixel(getApplicationContext(), 200);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(getApplicationContext(), 200) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rl_pay_info.setClickable(true);
                payDropped = !payDropped;
                if (payDropped) {
                    ib_drop_pay.setBackgroundResource(R.drawable.selectbox_arrow_on);
                } else {
                    ib_drop_pay.setBackgroundResource(R.drawable.selectbox_arrow_off);
                }
            }
        });
        m_aniSlideShow.start();
    }

    void showMaxDetail() {
        rl_max_money.setClickable(false);
        DockAnimation m_aniSlideShow = new DockAnimation(rl_max_detail, 300,
                DockAnimation.FRAME_LAYOUT);

        rl_max_detail.setVisibility(View.VISIBLE);

        int from = !maxDropped ? 0 : Utils.dpToPixel(getApplicationContext(), 130);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(getApplicationContext(), 130) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rl_max_money.setClickable(true);
                maxDropped = !maxDropped;
                if (maxDropped) {
                    ib_drop_max.setBackgroundResource(R.drawable.selectbox_arrow_on);
                } else {
                    ib_drop_max.setBackgroundResource(R.drawable.selectbox_arrow_off);
                }
            }
        });
        m_aniSlideShow.start();
    }

    private void showRefundData() {
        tv_reason.setText(dealDetail.getDeal().getRefundReason());

        LayoutInflater inflater;
        inflater = LayoutInflater.from(this);
        List<String> imgRefunds = dealDetail.getRefundPhotos();
        for (int i = 0; i < imgRefunds.size(); i++) {
            final View imgView = inflater.inflate(R.layout.item_img_upload, null);
            ImageView ivRefound = ((ImageView) imgView.findViewById(R.id.img));
            ImageUtils.load(ivRefound.getContext(), imgRefunds.get(i), R.drawable.img_default_square, R.drawable.img_default_square, ivRefound);

            ImageButton btnDel = ((ImageButton) imgView.findViewById(R.id.ib_del));
            btnDel.setVisibility(View.INVISIBLE);
            ll_img_list.addView(imgView);

        }
    }

    /////////////////////////////////////////////////////
    // MARK: - Network apis
    /////////////////////////////////////////////////////
    // 주문상세
    private void getDealDetailInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<DealDetailDto>> genRes = restAPI.getDealDetail(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDetailDto>(this) {
            @Override
            public void onSuccess(DealDetailDto response) {
                closeLoading();
                dealDetail = response;

                loadLayout();
            }

            @Override
            public void onFailed(Throwable t) {
                dealDetail = null;
                closeLoading();
                finish();
            }
        });
    }

    // 시스템 설정 정보 얻기
    private void getSystemSettingInfo(final int money) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<SystemSettingDto>> genRes = restAPI.systemSetting(Globals.userToken);

        genRes.enqueue(new TokenCallback<SystemSettingDto>(this) {
            @Override
            public void onSuccess(SystemSettingDto response) {
                closeLoading();

                long buyReviewReward = response.getBuyReviewReward();
                long maxSelluvmoney = (money * response.getDealReward()) / 100;
                long total = maxSelluvmoney + buyReviewReward;

                txt_buy_price_value.setText(Utils.getAttachCommaFormat((int) maxSelluvmoney));
                txt_after_price_value.setText(Utils.getAttachCommaFormat((int) buyReviewReward));
                txt_max_total_price_value.setText(Utils.getAttachCommaFormat((int) total));
                tv_max_amount.setText(Utils.getAttachCommaFormat((int) total));
            }

            @Override
            public void onFailed(Throwable t) {
                dealDetail = null;
                closeLoading();
                finish();
            }
        });
    }

    // 영업일 기준 날짜 얻기
    private void getWorkingDay(String _date, int diff) {

        WorkingDayReq dicInfo = new WorkingDayReq();
        dicInfo.setBasisDate(_date);
        dicInfo.setDeltaDays(diff);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<WorkingDayDto>> genRes = restAPI.getWorkingDay(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<WorkingDayDto>(this) {
            @Override
            public void onSuccess(WorkingDayDto response) {
                closeLoading();

                String strDate = response.getTargetDate().toString();
                int year = Integer.valueOf(strDate.substring(0, 4));
                int month = Integer.valueOf(strDate.substring(5, 7));
                int day = Integer.valueOf(strDate.substring(8, 10));
                String makedate = String.format("%d년 %d월 %d일 17시", year, month, day);
                tv_status_text.setText("상품이 판매되었습니다. 아래 배송 방법 중 한 가지를 선택하여 상품을 발송하신 후 운송장 번호를 입력해주세요." + makedate + "까지 운송장 번호를 입력하지 않으실 경우 거래가 자동으로 취소됩니다. (편의점 택배는 예약번호로 발송하신 후, 운송장 번호가 자동으로 업데이트됩니다.");
            }

            @Override
            public void onFailed(Throwable t) {
                dealDetail = null;
                closeLoading();
                finish();
            }
        });
    }

    //택배예약정보 얻기
    private void getDealDeliveryInfo() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<DeliveryHistoryDao>> genRes = restAPI.getDealDeliveryInfo(Globals.userToken, dealUid);
        genRes.enqueue(new Callback<GenericResponse<DeliveryHistoryDao>>() {

            @Override
            public void onResponse(Call<GenericResponse<DeliveryHistoryDao>> call, Response<GenericResponse<DeliveryHistoryDao>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    DeliveryHistoryDao dic = response.body().getData();
                    Intent intent = new Intent(SalesDetailActivity.this, PostFinishActivity.class);
                    intent.putExtra(Constants.DEAL_UID, dealUid);
                    intent.putExtra(Constants.DELIVERY_NUMBER, dic.getDeliveryNumber());
                    startActivity(intent);

                } else if (response.body().getMeta().getErrCode() == 321) { //예약한 정보가 없습니다.

                    DeliveryHistoryDao dic = response.body().getData();
                    Intent intent = new Intent(SalesDetailActivity.this, PostActivity.class);
                    intent.putExtra(Constants.DEAL_UID, dealUid);
                    startActivity(intent);
                } else {//실패
                    dealDetail = null;
                    finish();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<DeliveryHistoryDao>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());


            }
        });


    }
}
