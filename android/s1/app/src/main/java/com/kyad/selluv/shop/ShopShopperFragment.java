/**
 * 홈
 */
package com.kyad.selluv.shop;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.MainActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.ThemaListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.api.dto.theme.ThemeListDto;
import com.kyad.selluv.api.enumtype.SEARCH_ORDER_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.home.WareListFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.DATA_STYLE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_STYLE;

public class ShopShopperFragment extends BaseFragment {

    //UI Refs
    @BindView(R.id.ll_brands)
    LinearLayout ll_brands;
    @BindView(R.id.ll_top)
    LinearLayout ll_top;
    @BindView(R.id.recycler_thema)
    RecyclerView recycler_thema;

    //Vars
    View rootView;
    public WareListFragment frag_ware_list = new WareListFragment();
    public int shopperType = 0; //0: male 1: female 2: kids
    int[] nineImgs = {R.id.iv_new, R.id.iv_popular, R.id.iv_discount, R.id.iv_clothing, R.id.iv_all, R.id.iv_shoes, R.id.iv_fashion, R.id.iv_bag, R.id.iv_watch};
    public Fragment mainFragment;


    public ShopShopperFragment() {
        frag_ware_list.isLazyLoading = true;
        frag_ware_list.setFragType(WIRE_FRAG_STYLE);
        frag_ware_list.setDataType((shopperType + 1));
    }

    public static ShopShopperFragment newInstance(Fragment mainFragment) {
        ShopShopperFragment fragment = new ShopShopperFragment();
        fragment.mainFragment = mainFragment;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);

//        frag_ware_list.isLazyLoading = true;
        frag_ware_list.setFragType(WIRE_FRAG_STYLE);
        frag_ware_list.setDataType(((shopperType + 1) | DATA_STYLE));
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_shopper, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();

//        requestBrandList();
//        requestThemeList();
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (frag_ware_list != null) frag_ware_list.setUserVisibleHint(isVisibleToUser);
    }

    private void loadLayout() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_thema.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator animator = recycler_thema.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        initShopperType();

        if (shopperType == 0) {
            rootView.findViewById(R.id.frag_shop_shopper_ware_list).setId(R.id.frag_shop_male_ware_list);
            addWareFragment(R.id.frag_shop_male_ware_list, frag_ware_list);
        } else if (shopperType == 1) {
            rootView.findViewById(R.id.frag_shop_shopper_ware_list).setId(R.id.frag_shop_female_ware_list);
            addWareFragment(R.id.frag_shop_female_ware_list, frag_ware_list);
        } else {
            rootView.findViewById(R.id.frag_shop_shopper_ware_list).setId(R.id.frag_shop_kids_ware_list);
            addWareFragment(R.id.frag_shop_kids_ware_list, frag_ware_list);
        }

        ViewGroup viewParent = (ViewGroup) ll_top.getParent();
        viewParent.removeView(ll_top);
        frag_ware_list.headerViews.add(ll_top);
    }

    void initShopperType() {
        if (shopperType == 1) {
            ((TextView) rootView.findViewById(R.id.tv_shopping)).setText(getResources().getString(R.string.female_shopping));
            ((TextView) rootView.findViewById(R.id.tv_thema)).setText(getResources().getString(R.string.female_thema));
            ((TextView) rootView.findViewById(R.id.tv_style)).setText(getResources().getString(R.string.female_style));
            ((TextView) rootView.findViewById(R.id.tv_bag)).setText(getResources().getString(R.string.handbag));
            int[] colors = {
                    getResources().getColor(R.color.color_000000_65),
                    getResources().getColor(R.color.color_dcdcdc_60),
                    getResources().getColor(R.color.color_ff6b9e_70),
                    getResources().getColor(R.color.color_b9d6ff_65),
                    getResources().getColor(R.color.color_ff3b7e),
                    getResources().getColor(R.color.color_b9d6ff_65),
                    getResources().getColor(R.color.color_ff6b9e_70),
                    getResources().getColor(R.color.color_dcdcdc_30),
                    getResources().getColor(R.color.colorBlackDim75)
            };
            int[] imgs = {
                    R.drawable.search_female_category_new_item,
                    R.drawable.search_female_category_popular,
                    R.drawable.search_female_category_sale,
                    R.drawable.search_female_category_clothes,
                    R.drawable.search_male_category_ful_textl,
                    R.drawable.search_female_category_shoes,
                    R.drawable.search_female_category_acc,
                    R.drawable.search_female_category_bag,
                    R.drawable.search_female_category_watch_jewelry
            };
            for (int i = 0; i < nineImgs.length; i++) {
                ((ImageView) rootView.findViewById(nineImgs[i])).setImageResource(imgs[i]);
                if (i != 4) {
                    ((ImageView) rootView.findViewById(nineImgs[i])).setColorFilter(colors[i], PorterDuff.Mode.SRC_OVER);
                } else {
                    ((ImageView) rootView.findViewById(nineImgs[i])).setColorFilter(colors[i], PorterDuff.Mode.SCREEN);
                }
            }
        }

        if (shopperType == 2) {
            ((TextView) rootView.findViewById(R.id.tv_shopping)).setText(getResources().getString(R.string.kids_shopping));
            ((TextView) rootView.findViewById(R.id.tv_thema)).setText(getResources().getString(R.string.kids_thema));
            ((TextView) rootView.findViewById(R.id.tv_style)).setText(getResources().getString(R.string.kids_style));

            ((TextView) rootView.findViewById(R.id.tv_clothing)).setText(getResources().getString(R.string.kidsboy));
            ((TextView) rootView.findViewById(R.id.tv_shoes)).setText(getResources().getString(R.string.kidsgirl));
            ((TextView) rootView.findViewById(R.id.tv_fashion)).setText(getResources().getString(R.string.teenboy));
            ((TextView) rootView.findViewById(R.id.tv_bag)).setText(getResources().getString(R.string.baby));
            ((TextView) rootView.findViewById(R.id.tv_watch)).setText(getResources().getString(R.string.teengirl));
            int[] colors = {
                    getResources().getColor(R.color.color_000000_60),
                    getResources().getColor(R.color.color_dcdcdc_70),
                    getResources().getColor(R.color.color_3a0d7d_80),
                    getResources().getColor(R.color.color_faec55_70),
                    getResources().getColor(R.color.color_3a0d7d),
                    getResources().getColor(R.color.color_faec55_70),
                    getResources().getColor(R.color.color_3a0d7d_80),
                    getResources().getColor(R.color.color_dcdcdc_70),
                    getResources().getColor(R.color.color_000000_70)
            };
            int[] imgs = {
                    R.drawable.search_kids_category_new_item,
                    R.drawable.search_kids_category_popular,
                    R.drawable.search_kids_category_sale,
                    R.drawable.search_kids_category_boy,
                    R.drawable.search_male_category_ful_textl,
                    R.drawable.search_kids_category_girl,
                    R.drawable.search_kids_category_teenboy,
                    R.drawable.search_kids_category_baby,
                    R.drawable.search_kids_category_teengirl
            };

            for (int i = 0; i < nineImgs.length; i++) {
                ((ImageView) rootView.findViewById(nineImgs[i])).setImageResource(imgs[i]);
                if (i != 4) {
                    ((ImageView) rootView.findViewById(nineImgs[i])).setColorFilter(colors[i], PorterDuff.Mode.SRC_OVER);
                } else {
                    ((ImageView) rootView.findViewById(nineImgs[i])).setColorFilter(colors[i], PorterDuff.Mode.SCREEN);
                }
            }
        }
    }

    @OnClick(R.id.rl_new)
    void onNew(View v) {
        gotoSearchResult(0);
    }

    @OnClick(R.id.rl_popular)
    void onPopular(View v) {
        gotoSearchResult(1);
    }

    @OnClick(R.id.rl_discount)
    void onDiscount(View v) {
        gotoSearchResult(2);
    }

    @OnClick(R.id.rl_clothing)
    void onClothing(View v) {
        gotoCategory(0);
    }

    @OnClick(R.id.rl_all)
    void onAll(View v) {
        Globals.categoryDepth = 0;
        Globals.currentShopperTypeIdx = shopperType;
        Globals.categoryIndex[0] = shopperType;
        Globals.gKisCategoryName = "";
        ((ShopMainFragment) mainFragment).gotoCategorySelectActivity();
    }

    @OnClick(R.id.rl_shoes)
    void onShoes(View v) {
        gotoCategory(2);
    }

    @OnClick(R.id.rl_fashion)
    void onFashion(View v) {
        gotoCategory(3);
    }

    @OnClick(R.id.rl_bag)
    void onBag(View v) {
        gotoCategory(1);
    }

    @OnClick(R.id.rl_watch)
    void onWatch(View v) {
        gotoCategory(4);
    }


    void addBrands(LinearLayout ll, List<BrandMiniDto> brandList) {
        if (mainFragment == null || mainFragment.getActivity() == null) {
            return;
        }
        ll.removeAllViews();
        for (int i = 0; i < brandList.size(); i++) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View v = inflater.inflate(R.layout.item_shop_shopper_brand, null);

            ImageUtils.load(getActivity(), brandList.get(i).getProfileImg(), ((ImageView) v.findViewById(R.id.iv_brand)));

            final int uidTag = brandList.get(i).getBrandUid();
            ((ImageView) v.findViewById(R.id.iv_brand)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ((MainActivity) getActivity()).gotoBrandPage(shopperType + 1);
                    ((MainActivity) getActivity()).gotoBrandPage(uidTag);
                }
            });
            ll.addView(v);
        }
    }

    void addWareFragment(int destResID, WareListFragment frag) {
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(destResID, frag);
        transaction.commit();
    }

    void gotoCategory(int index) {
        Globals.categoryDepth = 1;
        Globals.currentShopperTypeIdx = shopperType;

        if (shopperType == 2) {
            // 키즈인 경우 키즈보이와 베이비의 순서가 바뀌였다.
            if (index == 0 || index == 1) {
                Globals.categoryIndex[0] = 5 + (1 - index) + Globals.currentShopperTypeIdx * 5;
            } else  {
                Globals.categoryIndex[0] = 5 + index + Globals.currentShopperTypeIdx * 5;
            }
        } else {
            Globals.categoryIndex[0] = 5 + index + Globals.currentShopperTypeIdx * 5;
        }

        ((ShopMainFragment) mainFragment).gotoCategoryFragment();    }

    void gotoSearchResult(int index) {
//        Globals.categoryDepth = 1;
        Globals.currentShopperTypeIdx = shopperType;
//        Globals.categoryIndex[0] = index;
        String title = Constants.shopper_type[shopperType] + " " + Constants.shopper_condition[index];
        if (index == 0) {
            Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.NEW);
        } else if (index == 1) {
            Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.FAME);
        } else {
            Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.PRICELOW);
        }
        Globals.searchReq.setKeyword(Constants.shopper_type[shopperType]);
        Globals.searchReq.setCategoryUid(shopperType == 0 ? 1 : shopperType == 1 ? 2 : 4);
        ((ShopMainFragment) mainFragment).gotoSearchResultActivity(title);
    }

    public void requestBrandList() {
        if (mainFragment == null || mainFragment.getActivity() == null) {
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        String type = "MALE";
        if (shopperType == 1) {
            type = "FEMALE";
        } else if (shopperType == 2) {
            type = "KIDS";
        }

        Call<GenericResponse<List<BrandMiniDto>>> genRes = restAPI.brandRecommendedList(Globals.userToken, type);

        showLoading();

        genRes.enqueue(new TokenCallback<List<BrandMiniDto>>(getActivity()) {
            @Override
            public void onSuccess(List<BrandMiniDto> response) {
                closeLoading();

                addBrands(ll_brands, response);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    public void requestThemeList() {
        if (mainFragment == null || mainFragment.getActivity() == null) {
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        String type = "MALE";
        if (shopperType == 1) {
            type = "FEMALE";
        } else if (shopperType == 2) {
            type = "KIDS";
        }

        Call<GenericResponse<List<ThemeListDto>>> genRes = restAPI.themeRecommendedList(Globals.userToken, type);

        showLoading();

        genRes.enqueue(new TokenCallback<List<ThemeListDto>>(getActivity()) {
            @Override
            public void onSuccess(List<ThemeListDto> response) {
                closeLoading();

                ThemaListAdapter adapter;

                for (int i = 0; i < response.size(); i++) {
                    ThemeListDto listDto = response.get(i);
                    response.get(i).setItems(listDto.getPdtList());
                }

                adapter = new ThemaListAdapter(ShopShopperFragment.this, response);
                recycler_thema.setAdapter(adapter);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
