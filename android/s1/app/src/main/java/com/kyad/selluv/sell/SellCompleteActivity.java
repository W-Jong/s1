package com.kyad.selluv.sell;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import butterknife.OnClick;

public class SellCompleteActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_complete);
        addForSellActivity();

        loadLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * 확인 버튼
     *
     * @param v
     */
    @OnClick(R.id.btn_ok)
    void onOk(View v) {
        Globals.pdtCreateReq.init();
        finishForSellActivities();
    }

    @OnClick(R.id.rl_soc02)
    void onSelluvItem(View v) {

    }

    @OnClick(R.id.rl_soc01)
    void onSoc1(View v) {

    }

    @OnClick(R.id.rl_soc03)
    void onSoc3(View v) {

    }

    @OnClick(R.id.rl_soc04)
    void onSoc4(View v) {

    }

    @OnClick(R.id.rl_soc05)
    void onSoc5(View v) {

    }

    @OnClick(R.id.rl_soc06)
    void onSoc6(View v) {

    }

    @OnClick(R.id.tv_sns_reward)
    void onSNSReward(View v) {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.sell_direct_complete_snsreward);
        startActivity(intent);
    }

    @OnClick(R.id.btn_confirm)
    void onConfirm(View v) {
        Globals.pdtCreateReq.init();
        finishForSellActivities();
        //startActivity(DetailActivity.class, false, 0, 0);
    }

    private void loadLayout() {

    }

}
