package com.kyad.selluv.api.dto.usr;

import java.util.List;

import lombok.Data;

@Data
public class UsrMyInfoDto {
    //("회원정보 - 로그인시 회원정보와 동일")
    UsrDetailDto usr;
    //("주소정보")
    private List<UsrAddressDao> addressList;
    //("팔로워수")
    private long usrFollowerCount;
    //("팔로잉수")
    private long usrFollowingCount;
    //("아이템수")
    private long countPdt;
    //("구매수")
    private long countBuy;
    //("판매수")
    private long countSell;
    //("읽지 않는 공지수")
    private long countUnreadNotice;
    //("마이페이지 노출이벤트")
    private List<EventDao> myPageEventList;
}
