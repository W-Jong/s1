package com.kyad.selluv.api.request;


import android.support.annotation.IntRange;

import com.kyad.selluv.api.enumtype.LOGIN_TYPE;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserSignupReq {

    //@ApiModelProperty("아이디")
    @NonNull
    private String usrId = "";

    //@ApiModelProperty("닉네임")
    @NonNull
    private String usrNckNm = "";

    //@ApiModelProperty("이메일")
    @NonNull
    private String usrMail = "";

    //@ApiModelProperty("sns 아이디, 이메일회원가입 인 경우 빈문자열")
    @NonNull
    private String snsId = "";

    //@ApiModelProperty("비번, sns로그인의 경우 sns토큰")
    @NonNull
    private String password = "";

    //@ApiModelProperty("sns 프로필이미지 URL, 이메일회원가입 인 경우 빈문자열")
    @NonNull
    private String snsProfileImg = "";

    //@ApiModelProperty("회원가입 유형")
    @NonNull
    private LOGIN_TYPE loginType = LOGIN_TYPE.NORMAL;

    //@ApiModelProperty("추천인 코드")
    @NonNull
    private String inviteCode = "";

    //@ApiModelProperty("본인인증 실명")
    @NonNull
    private String usrNm = "";

    //@ApiModelProperty("본인인증 생년월일 (형식:1990-01-01)")
    @NonNull
    private String birthday = "";

    //@ApiModelProperty("본인인증 성별 1-남, 2-여")
    @IntRange(from = 1, to = 2)
    private int gender = 1;

    //@ApiModelProperty("본인인증 폰번호")
    @NonNull
    private String usrPhone = "";

    //@ApiModelProperty("관심브랜드 UID 리스트")
    @NonNull
    private List<Integer> likeBrands = new ArrayList<>();


}
