package com.kyad.selluv.common.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BrandListViewHolder extends ViewHolder {
    public TextView tv_brand_logo;
    public TextView tv_follow_cnt;
    public ImageView iv_bg;
    public ImageView iv_logo;
    public ImageView iv_heart;
    public RelativeLayout rl_brand_logo;

    public BrandListViewHolder(View itemView) {
        super(itemView);
    }
}
