package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.enumtype.SEARCH_ORDER_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.OnClick;

public class SortFilterActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.ib_check_recommend)
    ImageButton ib_check_recommend;

    @BindView(R.id.ib_check_new)
    ImageButton ib_check_new;

    @BindView(R.id.ib_check_popular)
    ImageButton ib_check_popular;

    @BindView(R.id.ib_check_ascend)
    ImageButton ib_check_ascend;

    @BindView(R.id.ib_check_descend)
    ImageButton ib_check_descend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_sort);

        loadLayout();
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.rl_recommend)
    void onRecommend() {
        ib_check_recommend.setVisibility(View.VISIBLE);
        ib_check_new.setVisibility(View.INVISIBLE);
        ib_check_popular.setVisibility(View.INVISIBLE);
        ib_check_ascend.setVisibility(View.INVISIBLE);
        ib_check_descend.setVisibility(View.INVISIBLE);
        Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.RECOMMEND);
    }

    @OnClick(R.id.rl_new)
    void onNew() {
        ib_check_recommend.setVisibility(View.INVISIBLE);
        ib_check_new.setVisibility(View.VISIBLE);
        ib_check_popular.setVisibility(View.INVISIBLE);
        ib_check_ascend.setVisibility(View.INVISIBLE);
        ib_check_descend.setVisibility(View.INVISIBLE);
        Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.NEW);
    }

    @OnClick(R.id.rl_popular)
    void onPopular() {
        ib_check_recommend.setVisibility(View.INVISIBLE);
        ib_check_new.setVisibility(View.INVISIBLE);
        ib_check_popular.setVisibility(View.VISIBLE);
        ib_check_ascend.setVisibility(View.INVISIBLE);
        ib_check_descend.setVisibility(View.INVISIBLE);
        Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.FAME);
    }

    @OnClick(R.id.rl_ascend)
    void onAscend() {
        ib_check_recommend.setVisibility(View.INVISIBLE);
        ib_check_new.setVisibility(View.INVISIBLE);
        ib_check_popular.setVisibility(View.INVISIBLE);
        ib_check_ascend.setVisibility(View.VISIBLE);
        ib_check_descend.setVisibility(View.INVISIBLE);
        Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.PRICELOW);
    }

    @OnClick(R.id.rl_descend)
    void onDescend() {
        ib_check_recommend.setVisibility(View.INVISIBLE);
        ib_check_new.setVisibility(View.INVISIBLE);
        ib_check_popular.setVisibility(View.INVISIBLE);
        ib_check_ascend.setVisibility(View.INVISIBLE);
        ib_check_descend.setVisibility(View.VISIBLE);
        Globals.searchReq.setSearchOrderType(SEARCH_ORDER_TYPE.PRICEHIGH);
    }

    /**
     * 검색결과 보기
     */
    @OnClick(R.id.btn_ok)
    void onOk() {
        sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
        setResult(RESULT_OK);
        finish();
    }


    private void loadLayout() {
        if (Globals.searchReq.getSearchOrderType() == SEARCH_ORDER_TYPE.RECOMMEND) {
            onRecommend();
        } else if (Globals.searchReq.getSearchOrderType() == SEARCH_ORDER_TYPE.NEW) {
            onNew();
        } else if (Globals.searchReq.getSearchOrderType() == SEARCH_ORDER_TYPE.FAME) {
            onPopular();
        } else if (Globals.searchReq.getSearchOrderType() == SEARCH_ORDER_TYPE.PRICELOW) {
            onAscend();
        } else if (Globals.searchReq.getSearchOrderType() == SEARCH_ORDER_TYPE.PRICEHIGH) {
            onDescend();
        }
    }

}
