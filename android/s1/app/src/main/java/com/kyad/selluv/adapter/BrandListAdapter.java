package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.PluralsRes;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.brand.BrandDetailActivity;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.viewholder.BrandListViewHolder;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;

public class BrandListAdapter extends RecyclerView.Adapter implements SectionIndexer {

    public List<BrandListDto> dataList = new ArrayList<>();
    private BaseActivity activity;
    public int selectedLang = 0;
    private ArrayList<Integer> mSectionPositions = new ArrayList<>();
    List<String> sections = new ArrayList<>();

    public BrandListAdapter(BaseActivity activity, int selectedLang) {
        this.activity = activity;
        this.selectedLang = selectedLang;
    }

    public void setData(List<BrandListDto> data) {
        this.dataList = data;
        mSectionPositions.clear();
        sections.clear();
        //서버에서 정렬함 ㅋ
//        Model.BrandItemComparator comparator = new Model.BrandItemComparator(selectedLang);
//        Collections.sort(dataList, comparator);
        String alphabets = selectedLang == 0 ? Constants.en : Constants.kr;
        String alphabetsToCompare = selectedLang == 0 ? Constants.en : Constants.krToCompare;

        int pos = 0;
        for (int i = 0; i < alphabets.length(); i++) {
            char c = alphabets.charAt(i);

            for (int j = 0; j < dataList.size(); j++) {
                String name = selectedLang == 0 ? dataList.get(j).getNameEn() : dataList.get(j).getNameKo();
                if (name == null || name.trim().isEmpty())
                    continue;

                if (alphabetsToCompare.charAt(i) <= name.charAt(0) &&
                        ((i + 2 >= alphabets.length()) || alphabetsToCompare.charAt(i + 1) >= name.charAt(0))) {
                    pos = j;
                    break;
                }
            }
            sections.add(String.valueOf(c));
            mSectionPositions.add(pos);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.item_brand_list, null);
        BrandListViewHolder holder = new BrandListViewHolder(itemView);
        holder.tv_brand_logo = (TextView) itemView.findViewById(R.id.tv_brand_logo);
        holder.tv_follow_cnt = (TextView) itemView.findViewById(R.id.tv_follow_cnt);
        holder.iv_bg = (ImageView) itemView.findViewById(R.id.iv_bg);
        holder.iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
        holder.iv_heart = (ImageView) itemView.findViewById(R.id.iv_heart);
        holder.rl_brand_logo = (RelativeLayout) itemView.findViewById(R.id.rl_brand_logo);
        itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        BrandListViewHolder vh = (BrandListViewHolder) viewHolder;
        final BrandListDto item = dataList.get(position);

        vh.tv_brand_logo.setText(selectedLang == 0 ? item.getNameEn() : item.getNameKo());
        vh.tv_follow_cnt.setText(String.valueOf(item.getBrandLikeCount()));
        if ("".equals(item.getLogoImg()) || selectedLang == 1) {
            vh.iv_logo.setVisibility(View.INVISIBLE);
            vh.tv_brand_logo.setVisibility(View.VISIBLE);
        } else {
            ImageUtils.load(activity, item.getLogoImg(), android.R.color.transparent, R.drawable.img_default_square, vh.iv_logo);
            vh.iv_logo.setVisibility(View.VISIBLE);
            vh.tv_brand_logo.setVisibility(View.INVISIBLE);
        }
        vh.iv_heart.setImageResource(item.getBrandLikeStatus() ? R.drawable.ic_brand_follow_active : R.drawable.ic_brand_follow_inactive);
        vh.iv_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brandLike(item);
            }
        });
        final RelativeLayout final_rl_brand_logo = vh.rl_brand_logo;
        final ImageView final_iv_bg = vh.iv_bg;
        if (!"".equals(item.getBackImg())) {
            Glide.with(activity).load(item.getBackImg())
                    .apply(new RequestOptions().placeholder(android.R.color.transparent).error(R.drawable.img_default))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                            final_rl_brand_logo.setBackground(resource);
                            final_iv_bg.setImageDrawable(resource);
                        }
                    });
        } else {
            vh.rl_brand_logo.setBackgroundResource(R.drawable.img_default);
        }

        vh.rl_brand_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBrandDetailPage(v, item.getBrandUid());
            }
        });
    }

    private void goBrandDetailPage(View view, int brandUid){
        if (brandUid == 0)
            return;

        Intent intent = new Intent(activity, BrandDetailActivity.class);
        intent.putExtra(BrandDetailActivity.BRAND_UID, brandUid);
        activity.startActivity(intent);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    private void brandLike(final BrandListDto item) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        Call<GenericResponse<Void>> genRes = !item.getBrandLikeStatus() ? restAPI.likeBrand(Globals.userToken, item.getBrandUid()) : restAPI.unLikeBrand(Globals.userToken, item.getBrandUid());
        genRes.enqueue(new TokenCallback<Void>(activity) {

            @Override
            public void onSuccess(Void response) {
                if (item.getBrandLikeStatus()) {
                    item.setBrandLikeStatus(false);
                    item.setBrandLikeCount(item.getBrandLikeCount() - 1);
                } else {
                    item.setBrandLikeStatus(true);
                    item.setBrandLikeCount(item.getBrandLikeCount() + 1);
                }
                notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                if (t instanceof com.kyad.selluv.api.APIException) {
                    if (((com.kyad.selluv.api.APIException) t).getCode().equals("150")) {
                        ((BaseActivity) activity).showLoginSuggestionPopup();
                    }
                }
            }
        });
    }
}
