package com.kyad.selluv.mypage;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.UsrSnsStatusDto;
import com.kyad.selluv.api.enumtype.SNS_TYPE;
import com.kyad.selluv.api.request.UserOtherInfoReq;
import com.kyad.selluv.api.request.UserSnsReq;
import com.kyad.selluv.brand.BrandEditActivity;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.webview.CommonWebViewActivity;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.data.OAuthLoginState;
import com.squareup.picasso.Picasso;
import com.steelkiwi.instagramhelper.InstagramHelper;
import com.steelkiwi.instagramhelper.InstagramHelperConstants;
import com.steelkiwi.instagramhelper.model.InstagramUser;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

public class MypageEditActivity extends BaseActivity {

    @BindView(R.id.tv_email_show)
    TextView tv_email_show;
    @BindView(R.id.tv_phone_show)
    TextView tv_phone_show;

    @BindView(R.id.tb_facebook)
    Button tb_facebook;
    @BindView(R.id.tb_naver)
    Button tb_naver;
    @BindView(R.id.tb_kakao)
    Button tb_kakao;
    @BindView(R.id.tb_instagram)
    Button tb_instagram;
    @BindView(R.id.tb_twitter)
    Button tb_twitter;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;
    @BindView(R.id.edt_name)
    EditText edt_name;
    @BindView(R.id.edt_contact)
    EditText edt_contact;
    @BindView(R.id.edt_address)
    TextView edt_address;
    @BindView(R.id.login_button)
    TwitterLoginButton btnTwitterLogin;

    //UI Reference
    @OnClick(R.id.ib_left)
    void onLeft() {

        if (!Utils.isValidEmail(tv_email_show.getText())) {
            Toast.makeText(this, "이메일 형식이 올바르지 않습니다", Toast.LENGTH_SHORT).show();
            return;
        }

        updateOtherUsrInfo();
    }

    @OnClick(R.id.rl_profile)
    void onProfile() {
        startActivity(MypageProfileActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_password)
    void onPassword() {
        startActivity(PasswordChangeActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_block)
    void onBlock() {
        startActivity(UserBlockActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_follow)
    void onFolllow() {
        startActivity(BrandEditActivity.class, false, 0, 0);
    }

    @OnClick(R.id.btn_search)
    void onClickSearch(View v) {
        CommonWebViewActivity.showAddressWebviewActivity(this);
    }

    @OnClick(R.id.tb_facebook)
    void onClickFacebook(View v) {
        if (tb_facebook.isSelected()) {
            if (Globals.myInfo.getUsr().getUsrLoginType() == 1)
                return;

            delUsrSns(SNS_TYPE.FACEBOOK);
        } else {
            OnFaceBook();
        }
    }

    @OnClick(R.id.tb_naver)
    void onClickNaver(View v) {
        if (tb_naver.isSelected()) {
            if (Globals.myInfo.getUsr().getUsrLoginType() == 2)
                return;

            delUsrSns(SNS_TYPE.NAVER);
        } else {
            if (OAuthLoginState.OK.equals(OAuthLogin.getInstance().getState(this))) {
                // access token 이 있는 상태로 로그인 버튼 안보여 줌
                strSnsToken = OAuthLogin.getInstance().getAccessToken(this);
                getNaverUserInfo();
            } else {
                mOAuthLoginModule.startOauthLoginActivity(MypageEditActivity.this, mOAuthLoginHandler);
            }

        }
    }

    @OnClick(R.id.tb_kakao)
    void onClickKakao(View v) {
        if (tb_kakao.isSelected()) {
            if (Globals.myInfo.getUsr().getUsrLoginType() == 3)
                return;

            delUsrSns(SNS_TYPE.KAKAOTALK);
        } else {
            Session.getCurrentSession().open(AuthType.KAKAO_TALK, MypageEditActivity.this);
        }
    }

    @OnClick(R.id.tb_instagram)
    void onClickInstagram(View v) {
        if (tb_instagram.isSelected()) {
            delUsrSns(SNS_TYPE.INSTAGRAM);
        } else {
            instagramHelper.loginFromActivity(this);
        }
    }

    @OnClick(R.id.tb_twitter)
    void onClickTwitter(View v) {
        if (tb_twitter.isSelected()) {
            delUsrSns(SNS_TYPE.TWITTER);
        } else {
            if (isInstalledPackage("com.twitter.android")) {
                bTwitter = true;
                btnTwitterLogin.performClick();
            } else {
                Utils.showToast(this, getString(R.string.install_twitter));
            }

        }
    }

    //Variables
    /**
     * 페북콜백
     */
    private CallbackManager callbackManager;
    /**
     * 카톡콜백
     */
    private SessionCallback callback;
    /**
     * 네이버 콜백
     */
    private OAuthLogin mOAuthLoginModule;
    /**
     * SNS토큰 임시저장
     */
    private String strSnsToken;
    /**
     * SNS아이디 임시저장
     */

    /**
     * 인스터그람
     */
    InstagramHelper instagramHelper;

    private String strSnsID;
    String[][] myAddressInfo = new String[3][4];
    int addrTabIndex = 0;
    private boolean bTwitter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_privacy);
        setStatusBarWhite();

        //facebook
        callbackManager = CallbackManager.Factory.create();
        //kakaotalk
        callback = new SessionCallback();                  // 이 두개의 함수 중요함
        Session.getCurrentSession().addCallback(callback);
        //naver
        mOAuthLoginModule = OAuthLogin.getInstance();
        mOAuthLoginModule.init(
                MypageEditActivity.this
                , "Khgw7OI4mtNP47Hv9Jwq"
                , "K94EMljc5Q"
                , getString(R.string.app_name)
                //,OAUTH_CALLBACK_INTENT
                // SDK 4.1.4 버전부터는 OAUTH_CALLBACK_INTENT변수를 사용하지 않습니다.
        );
        loadLayout();
        getSNSStatusInfo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonWebViewActivity.WEBVIEW_ADDRESS_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);
            if (isSuccess) {
                String addressDetail = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS);
                String addressDetailSub = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_POSTCODE);
                edt_address.setText(addressDetail + "\n" + addressDetailSub);

                myAddressInfo[addrTabIndex][2] = addressDetail;
                myAddressInfo[addrTabIndex][3] = addressDetailSub;

            }
        }

        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        } else if (requestCode == InstagramHelperConstants.INSTA_LOGIN && resultCode == RESULT_OK) {
            InstagramUser user = instagramHelper.getInstagramUser(this);

//            Picasso.with(this).load(user.getData().getProfilePicture()).into(userPhoto);
//            userTextInfo.setText(user.getData().getUsername() + "\n"
//                    + user.getData().getFullName() + "\n"
//                    + user.getData().getWebsite()
//            );
            String userName = user.getData().getUsername();
            String profile = user.getData().getProfilePicture();
            //String userFullName = user.getData().getFullName();
            String userId = user.getData().getId();
            String email = userId + "@gmail.com";

            reqUsrSns(SNS_TYPE.INSTAGRAM, email, userId, userName, profile, "");

        } else if (bTwitter) {
            bTwitter = false;
            btnTwitterLogin.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        tv_email_show.setText(Globals.myInfo.getUsr().getUsrMail());
        if (Globals.myInfo.getUsr().getUsrPhone().length() == 11)
            tv_phone_show.setText(Utils.convertPhoneNum(Globals.myInfo.getUsr().getUsrPhone()));
        else
            tv_phone_show.setText(Globals.myInfo.getUsr().getUsrPhone());
        tb_segment.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
//                setEditValueFromDao(Globals.myInfo.getAddressList().get(toggleIntValue));
                addrTabIndex = toggleIntValue;
                selectTabBtn(toggleIntValue);
            }
        });
        setAddressData();

        edt_name.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    //do your work here
                    myAddressInfo[addrTabIndex][0] = edt_name.getText().toString();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        edt_contact.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    //do your work here
                    myAddressInfo[addrTabIndex][1] = edt_contact.getText().toString();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        loginToTwitter();
        loginInstagram();
    }

    private void loginInstagram() {
        String scope = "basic+public_content+follower_list+comments+relationships+likes";
        //scope is for the permissions
        instagramHelper = new InstagramHelper.Builder()
                .withClientId(Constants.kInstagramID)
                .withRedirectUrl(Constants.kInstagramRedirectUrl)
                .withScope(scope)
                .build();
    }

    private void loginToTwitter() {

        btnTwitterLogin.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls

                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                String userID = String.valueOf(session.getUserId());
                String userName = session.getUserName();
                String email = userID + "@gmail.com";
                String token = session.getAuthToken().toString();

                String firstName = "", lastName = "";
//                try {
//                    firstName = session.getUserName().split(" ")[0];
//                    lastName = session.getUserName().split(" ")[1];
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    firstName = session.getUserName();
//                    lastName = "";
//                }
                //snsLogin(userID + "_twitter@pwi.com", firstName, lastName);

                //reqUsrSns(final SNS_TYPE type, String email, String _id, String nickname, String profile, String token) {
                reqUsrSns(SNS_TYPE.TWITTER, email, userID, userName, "", token);
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }

        });
    }

    private void setSnsStatus(SNS_TYPE _type, boolean status) {

        if (_type == SNS_TYPE.FACEBOOK) {
            tb_facebook.setSelected(status);
        } else if (_type == SNS_TYPE.NAVER) {
            tb_naver.setSelected(status);
        } else if (_type == SNS_TYPE.KAKAOTALK) {
            tb_kakao.setSelected(status);
        } else if (_type == SNS_TYPE.INSTAGRAM) {
            tb_instagram.setSelected(status);
        } else {
            tb_twitter.setSelected(status);
        }
    }

    private void setAddressData() {

        for (int i = 0; i < 3; i++) {
            myAddressInfo[i][0] = Globals.myInfo.getAddressList().get(i).getAddressName();
            myAddressInfo[i][1] = Globals.myInfo.getAddressList().get(i).getAddressPhone();
            myAddressInfo[i][2] = Globals.myInfo.getAddressList().get(i).getAddressDetail();
            myAddressInfo[i][3] = Globals.myInfo.getAddressList().get(i).getAddressDetailSub();
        }
        addrTabIndex = 0;
        for (int i = 0; i < 3; i++) {
            if (Globals.myInfo.getAddressList().get(i).getStatus() == 2) {
                addrTabIndex = i;
            }
        }
        selectTabBtn(addrTabIndex);
    }

    private void selectTabBtn(int index) {
        tb_segment.setToggleStatus(index);
        addrTabIndex = index;
        edt_name.setText(myAddressInfo[index][0]);
        edt_contact.setText(myAddressInfo[index][1]);

        if (!myAddressInfo[index][2].equals("") && !myAddressInfo[index][3].equals("")) {
            edt_address.setText(myAddressInfo[index][2] + "\n" + myAddressInfo[index][3]);
        } else {
            edt_address.setText("");
        }
    }

    private void OnFaceBook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("kyad-log", "Success");
                        strSnsToken = loginResult.getAccessToken().getToken();
                        strSnsID = loginResult.getAccessToken().getUserId();

                        getFacebookUserInfo(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("kyad-log", "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("kyad-log", "Error");
                    }
                });
    }

    private void getFacebookUserInfo(AccessToken accessToken) {
        showLoading();

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                closeLoading();

                try {
                    String snsEmail = jsonObject.getString("email");
                    String snsNickname = jsonObject.getString("name");
                    String profile = ((jsonObject.getJSONObject("picture")).getJSONObject("data")).getString("url");

                    if (snsEmail == null || snsEmail.isEmpty()) {
                        snsEmail = strSnsID + "@facebook.selluv";
                        //Toast.makeText(getApplicationContext(), "이메일주소얻기 권한을 설정해주세요.", Toast.LENGTH_SHORT).show();
                        //return;
                    }

                    reqUsrSns(SNS_TYPE.FACEBOOK, snsEmail, strSnsID, snsNickname, profile, strSnsToken);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "페이스북정보를 얻는데 실패했습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,age_range,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getNaverUserInfo() {
        showLoading();
        RequestApiTask task = new RequestApiTask();
        task.execute(strSnsToken);
    }

    /**
     * 카카오 session
     */
    private class SessionCallback implements ISessionCallback {

        @Override
        public void onSessionOpened() {
            getKakaoUserInfo(); // 세션 연결성공 시 redirectSignupActivity() 호출
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if (exception != null) {
                Logger.e(exception);
            }
        }
    }

    private OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            if (success) {
                strSnsToken = mOAuthLoginModule.getAccessToken(MypageEditActivity.this);

                getNaverUserInfo();
            } else {
                String errorCode = mOAuthLoginModule.getLastErrorCode(MypageEditActivity.this).getCode();
                String errorDesc = mOAuthLoginModule.getLastErrorDesc(MypageEditActivity.this);
                if (!errorCode.equals("user_cancel"))
                    Toast.makeText(MypageEditActivity.this, "errorCode:" + errorCode
                            + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
            }
        }

        ;
    };

    private void getKakaoUserInfo() {
        showLoading();
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                closeLoading();
                int ErrorCode = errorResult.getErrorCode();
                int ClientErrorCode = -777;

                if (ErrorCode == ClientErrorCode) {
                    Toast.makeText(getApplicationContext(), "카카오톡 서버의 네트워크가 불안정합니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("TAG", "오류로 카카오로그인 실패 ");
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                closeLoading();
                Log.d("TAG", "오류로 카카오로그인 실패 ");
            }

            @Override
            public void onSuccess(UserProfile userProfile) {
                String snsEmail = userProfile.getEmail();
                String snsNickname = userProfile.getNickname();
                strSnsID = "" + userProfile.getId();
                strSnsToken = strSnsID;

                if (snsEmail == null || snsEmail.isEmpty()) {
                    snsEmail = strSnsID + "@kakao.selluv";
//                    Toast.makeText(getApplicationContext(), "이메일주소얻기 권한을 설정해주세요.", Toast.LENGTH_SHORT).show();
//                    return;
                }

                reqUsrSns(SNS_TYPE.KAKAOTALK, snsEmail, strSnsID, snsNickname, "", strSnsToken);
            }

            @Override
            public void onNotSignedUp() {
                closeLoading();
                Toast.makeText(getBaseContext(), R.string.kakao_login_not_signup, Toast.LENGTH_SHORT).show();
                // 자동가입이 아닐경우 동의창
            }
        }/*, propertyKeys, false*/);
    }

    private class RequestApiTask extends AsyncTask<String, Void, String> {

        String result;

        @Override
        protected String doInBackground(String... params) {
            String token = params[0];// 네이버 로그인 접근 토큰;
            String header = "Bearer " + token; // Bearer 다음에 공백 추가
            try {
                String apiURL = "https://openapi.naver.com/v1/nid/me";
                URL url = new URL(apiURL);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Authorization", header);
                int responseCode = con.getResponseCode();
                BufferedReader br;
                if (responseCode == 200) { // 정상 호출
                    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                } else {  // 에러 발생
                    br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                }
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    response.append(inputLine);
                }
                result = response.toString();
                br.close();
                System.out.println(response.toString());
            } catch (Exception e) {
                System.out.println(e);
            }
            //result 값은 JSONObject 형태로 넘어옵니다.
            return result;

        }

        protected void onPostExecute(String content) {
            super.onPostExecute(content);
            closeLoading();
            try {
                //넘어온 result 값을 JSONObject 로 변환해주고, 값을 가져오면 되는데요.
                // result 를 Log에 찍어보면 어떻게 가져와야할 지 감이 오실거에요.
                JSONObject object = new JSONObject(result);
                if (object.getString("resultcode").equals("00")) {
                    JSONObject jsonObject = new JSONObject(object.getString("response"));
                    Log.d("jsonObject", jsonObject.toString());

                    strSnsID = jsonObject.getString("id");
                    String snsEmail = jsonObject.getString("email");
                    String snsNickname = jsonObject.getString("nickname");

                    if (snsEmail == null || snsEmail.isEmpty()) {
                        snsEmail = strSnsID + "@naver.selluv";
                    }

                    reqUsrSns(SNS_TYPE.NAVER, snsEmail, strSnsID, snsNickname, "", strSnsToken);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
            closeLoading();
        }
    }

    private boolean isInstalledPackage(String packageName) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");

        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(intent, 0);
        if (resInfo.isEmpty()) {
            return false;
        }

        List<Intent> shareIntentList = new ArrayList<Intent>();
        boolean isShare = false;
        for (ResolveInfo info : resInfo) {
            Intent shareIntent = (Intent) intent.clone();

            if (info.activityInfo.packageName.toLowerCase().equals(packageName)) {
                isShare = true;
                break;
            }
        }

        return isShare;
    }


    //get sns info
    private void getSNSStatusInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<UsrSnsStatusDto>> genRes = restAPI.getSnsInfo(Globals.userToken);

        genRes.enqueue(new TokenCallback<UsrSnsStatusDto>(this) {
            @Override
            public void onSuccess(UsrSnsStatusDto response) {

                setSnsStatus(SNS_TYPE.FACEBOOK, response.isFacekbook());
                setSnsStatus(SNS_TYPE.NAVER, response.isNaver());
                setSnsStatus(SNS_TYPE.KAKAOTALK, response.isKakaoTalk());
                setSnsStatus(SNS_TYPE.INSTAGRAM, response.isInstagram());
                setSnsStatus(SNS_TYPE.TWITTER, response.isTwitter());
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void updateOtherUsrInfo() {

        UserOtherInfoReq dicInfo = new UserOtherInfoReq();
        dicInfo.setAddressDetail1(myAddressInfo[0][2]);
        dicInfo.setAddressDetail2(myAddressInfo[1][2]);
        dicInfo.setAddressDetail3(myAddressInfo[2][2]);
        dicInfo.setAddressDetailSub1(myAddressInfo[0][3]);
        dicInfo.setAddressDetailSub2(myAddressInfo[1][3]);
        dicInfo.setAddressDetailSub3(myAddressInfo[2][3]);
        dicInfo.setAddressName1(myAddressInfo[0][0]);
        dicInfo.setAddressName2(myAddressInfo[1][0]);
        dicInfo.setAddressName3(myAddressInfo[2][0]);
        dicInfo.setAddressPhone1(myAddressInfo[0][1]);
        dicInfo.setAddressPhone2(myAddressInfo[1][1]);
        dicInfo.setAddressPhone3(myAddressInfo[2][1]);
        dicInfo.setUsrMail(tv_email_show.getText().toString());
        dicInfo.setAddressSelectionIndex(addrTabIndex + 1);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.updateOtherUsrInfo(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //sns 연동삭제
    private void delUsrSns(final SNS_TYPE snstype) {

        String _type = "";
        if (snstype == SNS_TYPE.FACEBOOK) {
            _type = "FACEBOOK";
        } else if (snstype == SNS_TYPE.NAVER) {
            _type = "NAVER";
        } else if (snstype == SNS_TYPE.KAKAOTALK) {
            _type = "KAKAOTALK";
        } else if (snstype == SNS_TYPE.INSTAGRAM) {
            _type = "INSTAGRAM";
        } else {
            _type = "TWITTER";
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.deleteSns(Globals.userToken, _type);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                setSnsStatus(snstype, false);
            }

            @Override
            public void onFailed(Throwable t) {

            }
        });
    }

    //sns 연동하기
    private void reqUsrSns(final SNS_TYPE type, String email, String _id, String nickname, String profile, String token) {

        UserSnsReq dicInfo = new UserSnsReq();
        dicInfo.setSnsEmail(email);
        dicInfo.setSnsId(_id);
        dicInfo.setSnsNckNm(nickname);
        dicInfo.setSnsProfileImg(profile);
        dicInfo.setSnsToken(token);
        dicInfo.setSnsType(type);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.registerSns(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                setSnsStatus(type, true);
            }

            @Override
            public void onFailed(Throwable t) {

            }
        });
    }

}
