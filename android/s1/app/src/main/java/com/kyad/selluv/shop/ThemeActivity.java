package com.kyad.selluv.shop;

import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.top.TopLikeActivity;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.LOAD_MORE_CNT;

public class ThemeActivity extends BaseActivity {
    //UI Reference
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.rl_thema_detail)
    RelativeLayout rl_thema_detail;
    @BindView(R.id.tv_thema_eng_title)
    TextView tv_thema_eng_title;
    @BindView(R.id.ll_thema_line)
    LinearLayout ll_thema_line;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsing_toolbar;


    private MainPageAdapter pagerAdapter;
    WareListFragment fragment_ware_list;
    int totalWareCnt = 0;
    Fragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);

        loadLayout();
    }

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    private void loadLayout() {

        collapsing_toolbar.setTitle("데일리 포멀 오피스룩");
        BarUtils.setStatusBarAlpha(this, 0, true);
        BarUtils.addMarginTopEqualStatusBarHeight(findViewById(R.id.tb_actionbar));

        AppBarLayout app_bar_layout = (AppBarLayout) findViewById(R.id.appbar);
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int h = Utils.dpToPixel(ThemeActivity.this, rl_thema_detail.getHeight() - 56 - 20);
                int b = Utils.dpToPixel(ThemeActivity.this, 18) * (h + verticalOffset) / h;
                if (b < 0) b = 0;
                final Animation animationFadeIn = AnimationUtils.loadAnimation(ThemeActivity.this, R.anim.fade_in);
                final Animation animationFadeOut = AnimationUtils.loadAnimation(ThemeActivity.this, R.anim.fade_out);
                if (verticalOffset <= -rl_thema_detail.getHeight() / 2) {
                    if (ll_thema_line.getVisibility() == View.VISIBLE) {
                        ll_thema_line.startAnimation(animationFadeOut);
                        ll_thema_line.setVisibility(View.INVISIBLE);
                        tv_thema_eng_title.startAnimation(animationFadeOut);
                        tv_thema_eng_title.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (ll_thema_line.getVisibility() == View.INVISIBLE) {
                        ll_thema_line.startAnimation(animationFadeIn);
                        ll_thema_line.setVisibility(View.VISIBLE);
                        tv_thema_eng_title.startAnimation(animationFadeIn);
                        tv_thema_eng_title.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        fragment_ware_list = new WareListFragment();
        fragment_ware_list.listInfo = getResources().getString(R.string.thema_ware);
        fragment_ware_list.setLoadInterface(null);
        fragment_ware_list.setTotalCnt(loadTotolCnt());
        fragment_ware_list.showFilterButton = true;
        fragment_ware_list.showFilter = true;
        fragment_ware_list.parentCollapsable = true;
        pagerAdapter = new MainPageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
    }

    public int loadTotolCnt() {
        return 20;
    }

//    public Model.WareItem[] loadThemaWare(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_6);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }

    private class MainPageAdapter extends FragmentStatePagerAdapter {

        public MainPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = fragment_ware_list;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 1;
        }
    }
}
