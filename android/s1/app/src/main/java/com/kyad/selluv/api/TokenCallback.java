package com.kyad.selluv.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.kyad.selluv.api.dto.GenericResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class TokenCallback<T> implements Callback<GenericResponse<T>> {
    private Context context;

    public TokenCallback(Context context) {
        this.context = context;
    }

    @Override
    public void onResponse(Call<GenericResponse<T>> call, Response<GenericResponse<T>> response) {
        if(response.body() == null) {
            onFailed(new APIException("Network Error", "0"));
            return;
        }
        Log.d("kyad-log", response.body().toString());

        if (response.body().getMeta().getErrCode() == 0) {//성공
            onSuccess(response.body().getData());
        } else {
            String errorMsg = response.body().getMeta().getErrMsg();
            int errorCode = response.body().getMeta().getErrCode();

            Toast.makeText(context, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();

            onFailed(new APIException(errorMsg, String.format("%d", errorCode)));
        }
    }

    @Override
    public void onFailure(Call<GenericResponse<T>> call, Throwable t) {
        Log.d("kyad-log", t.getMessage());

        onFailed(t);
    }

    abstract public void onSuccess(T response);

    abstract public void onFailed(Throwable t);
}
