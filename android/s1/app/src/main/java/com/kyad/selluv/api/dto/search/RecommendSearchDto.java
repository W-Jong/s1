package com.kyad.selluv.api.dto.search;

import com.kyad.selluv.api.dao.BrandDao;
import com.kyad.selluv.api.dto.category.CategoryDao;

import java.util.List;

import lombok.Data;

@Data
public class RecommendSearchDto {
    //@ApiModelProperty("추천브랜드")
    private List<BrandDao> brandList;
    //@ApiModelProperty("추천카테고리")
    private List<List<CategoryDao>> categoryDaoList;
    //@ApiModelProperty("추천모델명")
    private List<SearchModelDto> modelList;
    //@ApiModelProperty("추천태그명")
    private List<String> tagWordList;
}