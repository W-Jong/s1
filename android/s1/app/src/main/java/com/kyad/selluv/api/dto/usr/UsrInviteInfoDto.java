package com.kyad.selluv.api.dto.usr;

import lombok.Data;

@Data
public class UsrInviteInfoDto {
    //("추천인코드")
    private String inviteCode;
    //("가입한 친구수")
    private long invitedCount;
    //("첫 구매한 친구수")
    private long boughtCount;
    //("이달 가입한 친구수")
    private long invitedMonthCount;
}
