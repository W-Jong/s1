package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.top.UsrRecommendDto;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.mypage.EventDetailActivity;
import com.kyad.selluv.mypage.NoticeDetailActivity;
import com.kyad.selluv.mypage.PurchaseHistoryActivity;
import com.kyad.selluv.top.TopFriendActivity;
import com.kyad.selluv.user.UserActivity;
import com.kyad.selluv.user.UserFollowActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class RecommendUsrListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrRecommendDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public RecommendUsrListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrRecommendDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_selluver_recommend, parent, false);
            final UsrRecommendDto anItem = (UsrRecommendDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }

            CircularImageView iv_profile_img = convertView.findViewById(R.id.iv_profile_img);
            ImageUtils.load(iv_profile_img.getContext(), anItem.getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile_img);

            TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            tv_name.setText(anItem.getUsrNckNm());

            TextView tv_item = (TextView) convertView.findViewById(R.id.tv_item);
            tv_item.setText("아이템 " + anItem.getPdtCount());

            TextView tv_follow = (TextView) convertView.findViewById(R.id.tv_follow);
            tv_follow.setText("팔로워 " + anItem.getUsrFollowerCount());

            ImageView iv_ware1 = convertView.findViewById(R.id.iv_ware1);
            ImageView iv_ware2 = convertView.findViewById(R.id.iv_ware2);
            ImageView iv_ware3 = convertView.findViewById(R.id.iv_ware3);
            if (anItem.getPdtList().size() > 0) {
                ImageUtils.load(iv_ware1.getContext(), anItem.getPdtList().get(0).getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_ware1);
            }

            if (anItem.getPdtList().size() > 1) {
                ImageUtils.load(iv_ware1.getContext(), anItem.getPdtList().get(1).getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_ware1);
            }

            if (anItem.getPdtList().size() > 2) {
                ImageUtils.load(iv_ware1.getContext(), anItem.getPdtList().get(2).getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_ware1);
            }

            Button ib_hide = convertView.findViewById(R.id.ib_hide);
            Button ib_follow = convertView.findViewById(R.id.ib_follow);
            if (anItem.isUsrLikeStatus()) {
                ib_follow.setBackground(activity.getResources().getDrawable(R.drawable.btn_following));
                ib_hide.setVisibility(View.GONE);
            } else {
                ib_follow.setBackground(activity.getResources().getDrawable(R.drawable.btn_follow));
                ib_hide.setVisibility(View.VISIBLE);
            }
            iv_profile_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //유저페이지로
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    activity.startActivity(intent);
                }
            });

            tv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //잇템
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    activity.startActivity(intent);
                }
            });

            tv_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //팔로워
                    Intent intent = new Intent(activity, UserFollowActivity.class);
                    intent.putExtra(Constants.USER_UID, anItem.getUsrUid());
                    intent.putExtra(Constants.FOLLOW_PAGE_TITLE, anItem.getUsrNckNm());
                    intent.putExtra(Constants.FOLLOW_PAGE_IDX, 0);
                    activity.startActivity(intent);
                }
            });

            ib_hide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideRecommendUsr(anItem.getUsrUid());
                }
            });

            ib_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (anItem.isUsrLikeStatus()) {
                        final Snackbar snackbar = Snackbar.make(activity.getWindow().getDecorView(), "", Snackbar.LENGTH_INDEFINITE);
                        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                        snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
                        layout.setPadding(0, 0, 0, 0);//set padding to 0
                        View view = inflater.inflate(R.layout.action_sheet1, null);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        layout.addView(view, params);

                        ((TextView) view.findViewById(R.id.tv_name)).setText(anItem.getUsrNckNm());
                        ImageUtils.load(activity, anItem.getProfileImg(), android.R.color.transparent, R.drawable.ic_user_default, ((ImageView) view.findViewById(R.id.iv_photo)));
                        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();

                            }
                        });
                        view.findViewById(R.id.btn_follow_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestUserFollow(anItem.getUsrUid());
                                snackbar.dismiss();
                            }
                        });
                        snackbar.show();
                    } else {
                        requestUserunFollow(anItem.getUsrUid());
                    }
                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    //유저 숨기기
    private void hideRecommendUsr(int usrUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.usrHide(Globals.userToken, usrUid);

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {

                ((TopFriendActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //req user follow/unfollow
    private void requestUserFollow(int uid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        activity.showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.unFollowUsr(Globals.userToken, uid);

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {
                activity.closeLoading();

                ((TopFriendActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
                activity.closeLoading();
            }
        });
    }

    //req user follow/unfollow
    private void requestUserunFollow(int uid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        activity.showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.followUsr(Globals.userToken, uid);

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {
                activity.closeLoading();

                ((TopFriendActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
                activity.closeLoading();
            }
        });
    }

}