/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.BrandListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import io.ghyeok.stickyswitch.widget.StickySwitch;
import retrofit2.Call;

public class BrandListFragment extends BaseFragment {

    //UI Refs
    @BindView(R.id.lv_filter)
    IndexFastScrollRecyclerView mRecyclerView;
    @BindView(R.id.tv_sort)
    TextView tv_sort;

    //Vars
    View rootView;
    BrandListAdapter adapter;
    int selectedLang = 0;   //0 : eng 1: kor
    LinearLayout mTabMenu = null;
    LinearLayout appbar = null;

    public BrandListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_brand_list, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        loadData();
        return rootView;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (appbar != null) {
                appbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        appbar.setVisibility(View.VISIBLE);
                    }
                }).start();
            }

            if (mTabMenu != null) {
                mTabMenu.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        mTabMenu.setVisibility(View.VISIBLE);
                    }
                }).start();
            }
        }
    }

    private void loadLayout() {

        mTabMenu = (LinearLayout) getActivity().findViewById(R.id.ll_bottom);
        appbar = getActivity().findViewById(R.id.appbar);

        tv_sort.setText(String.format(getString(R.string.brand_sort), selectedLang == 0 ? getString(R.string.en) : getString(R.string.kr)));
        StickySwitch stickySwitch = (StickySwitch) rootView.findViewById(R.id.sw_lang);
        stickySwitch.onSelectedChangeListener = new StickySwitch.OnSelectedChangeListener() {
            @Override
            public void onSelectedChange(StickySwitch.Direction direction, String rightTextAlpha) {
                if (direction == StickySwitch.Direction.LEFT)
                    selectedLang = 0;
                else selectedLang = 1;
                updateLang();
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BrandListAdapter((BaseActivity) getActivity(), selectedLang);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setIndexTextSize(12);
        mRecyclerView.setIndexBarColor("#ffffff");
        mRecyclerView.setIndexBarCornerRadius(0);
        mRecyclerView.setIndexBarTransparentValue((float) 0.0);
        mRecyclerView.setIndexbarMargin(60);
        mRecyclerView.setIndexbarWidth(Utils.dpToPixel(getActivity(), 20));
        mRecyclerView.setPreviewPadding(0);
        mRecyclerView.setIndexBarTextColor("#000000");

        mRecyclerView.setIndexBarVisibility(true);
        mRecyclerView.setIndexbarHighLateTextColor("#42c2fe");
        mRecyclerView.setIndexBarHighLateTextVisibility(true);
    }

    private void loadData() {
        if (adapter == null)
            adapter = new BrandListAdapter((BaseActivity) getActivity(), selectedLang);

        updateUI();
    }


    protected void updateUI() {
        adapter.selectedLang = selectedLang;
        getAllDetailBrandList();

        adapter.notifyDataSetChanged();
    }

    void updateLang() {
        tv_sort.setText(String.format(getString(R.string.brand_sort), selectedLang == 0 ? getString(R.string.en) : getString(R.string.kr)));
        updateUI();
    }

    private void getAllDetailBrandList() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<BrandListDto>>> genRes = restAPI.brandAllDetailList(Globals.userToken, selectedLang + 1);

        genRes.enqueue(new TokenCallback<List<BrandListDto>>(getActivity()) {
            @Override
            public void onSuccess(List<BrandListDto> response) {
                closeLoading();
                adapter.dataList.clear();
                adapter.setData(response);
                adapter.notifyDataSetChanged();
//                requestComplete();
//                showHasMore();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
