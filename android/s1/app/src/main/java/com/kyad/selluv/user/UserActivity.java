package com.kyad.selluv.user;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.UsrInfoDto;
import com.kyad.selluv.api.enumtype.REPORT_TYPE;
import com.kyad.selluv.api.request.ReportReq;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.mypage.MypageProfileActivity;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;
import static com.kyad.selluv.common.Constants.DATA_STYLE;
import static com.kyad.selluv.common.Constants.DATA_WARE;
import static com.kyad.selluv.common.Constants.INITIAL_TAB_INDEX;
import static com.kyad.selluv.common.Constants.LOAD_MORE_CNT;

public class UserActivity extends BaseActivity {

    public static final String USR_UID = "USR_UID";

    private int usrUid = 0;
    private UsrInfoDto dicUsrInfo;
    public static int keyboardHeight;
    LayoutInflater inflater;

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.rl_profile)
    RelativeLayout rl_bg;
    @BindView(R.id.rl_user)
    RelativeLayout rl_user;
    @BindView(R.id.tv_introduce)
    TextView tv_introduce;

    //user info
    @BindView(R.id.iv_profile_img)
    CircularImageView iv_profile_img;
    @BindView(R.id.iv_profile_bg)
    ImageView iv_profile_bg;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_point)
    TextView tv_point;
    @BindView(R.id.tv_follower)
    TextView tv_follower;
    @BindView(R.id.tv_following)
    TextView tv_following;
    @BindView(R.id.btn_follow)
    ToggleButton btn_follow;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        final PopupMenu popup = new PopupMenu(this, v);
        final Activity activity = this;
        MenuInflater inflater = popup.getMenuInflater();

        if (dicUsrInfo.getUsrUid() == Globals.myInfo.getUsr().getUsrUid()) {
            inflater.inflate(R.menu.user_self_menu, popup.getMenu());
        } else {
            inflater.inflate(R.menu.user_menu, popup.getMenu());
            if (!dicUsrInfo.getUsrBlockStatus()) {
                popup.getMenu().getItem(1).setTitle(getString(R.string.user_block));
            } else {
                popup.getMenu().getItem(1).setTitle(getString(R.string.user_unblock));
            }
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.modify:
                        startActivity(MypageProfileActivity.class, false, 0, 0);
                        break;
                    case R.id.reporting:
//                        PopupDialog popupDialog = new PopupDialog((BaseActivity) activity, R.layout.detail_more_report_etc).setFullScreen().setBackGroundCancelable(true);
////                        popupDialog.show();
//                        reportUsr(popupDialog);
                        final PopupDialog opopup = new PopupDialog((BaseActivity) activity, R.layout.detail_more_report_etc).setFullScreen().setCancelable(true);
                        final View rootView1 = opopup.findView(R.id.ll_style_add_background);
                        String types[] = {"광고/스팸", "휴먼 판매자", "직거래 유도", "기타", "취소"};
                        ((CustomSpinner) opopup.findView(R.id.spinner2)).initializeStringValues(activity, types, activity.getResources().getString(R.string.notify_reason_select), null, null, 12.5f);

                        opopup.setOnClickListener(R.id.btn_report, new PopupDialog.bindOnClick() {
                            @Override
                            public void onClick() {
                                ((UserActivity) activity).reportUsr(opopup);

                            }
                        });

                        opopup.setOnClickListener(R.id.btn_close, new PopupDialog.bindOnClick() {
                            @Override
                            public void onClick() {
                                opopup.dismiss();
                                Utils.hideSoftKeyboard(activity);
                            }
                        });
                        opopup.setOnClickListener(R.id.ll_style_add_background, new PopupDialog.bindOnClick() {
                            @Override
                            public void onClick() {
                                Utils.hideSoftKeyboard(activity);
                            }
                        });

                        opopup.show();
                        rootView1.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                Rect r = new Rect();
                                rootView1.getWindowVisibleDisplayFrame(r);
                                int screenHeight = rootView1.getRootView().getHeight();
                                keyboardHeight = screenHeight - (r.bottom);
                                if (rootView1.getRootView().getHeight() - (r.bottom - r.top) > Utils.dpToPixel(rootView1.getContext(), 100)) { // if more than 100 pixels, its probably a keyboard...
                                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rootView1.getLayoutParams();
                                    lp.topMargin = -keyboardHeight + Utils.dpToPixel(activity, 50);
                                    rootView1.setLayoutParams(lp);
                                } else {
                                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rootView1.getLayoutParams();
                                    lp.topMargin = 0;
                                    rootView1.setLayoutParams(lp);
                                }
                            }
                        });
                        break;
                    case R.id.stopping:
                        if (dicUsrInfo.getUsrBlockStatus()) {
                            requestUsrUnBlock();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(UserActivity.this);
                            builder.setTitle(getString(R.string.block))
                                    .setMessage(getString(R.string.usr_block_alert))
                                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            requestUsrBlock();
                                        }
                                    })
                                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                        break;
                    case R.id.cancel:
                        popup.dismiss();
                        break;
                }
                return false;
            }
        });
        popup.show();
    }


    @OnClick(R.id.rl_point)
    void onPoint() {
        Intent intent = new Intent(this, UserMannerActivity.class);
        intent.putExtra(Constants.USER_UID, usrUid);
        startActivity(intent);
    }

    @OnClick(R.id.ll_follower)
    void onFollower() {
        Intent intent = new Intent(this, UserFollowActivity.class);
        intent.putExtra(Constants.USER_UID, usrUid);
        intent.putExtra(Constants.FOLLOW_PAGE_TITLE, dicUsrInfo.getUsrNckNm());
        intent.putExtra(Constants.FOLLOW_PAGE_IDX, 0);
        startActivity(intent);
    }

    @OnClick(R.id.ll_following)
    void onFollowing() {
        Intent intent = new Intent(this, UserFollowActivity.class);
        intent.putExtra(Constants.USER_UID, usrUid);
        intent.putExtra(Constants.FOLLOW_PAGE_TITLE, dicUsrInfo.getUsrNckNm());
        intent.putExtra(Constants.FOLLOW_PAGE_IDX, 1);
        startActivity(intent);
    }

    //Variables

    int selectedTab = 1;
    WareListFragment itemFragment, iitemFragment, styleFragment;
    private PageAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        BarUtils.setStatusBarAlpha(this, 0);

        usrUid = getIntent().getIntExtra(USR_UID, 0);
        if (Constants.IS_TEST) {
            Utils.showToast(this, "유저UID: " + usrUid);
        }
        inflater = LayoutInflater.from(this);
        initFragment();
        loadLayout();
        getOtherUsrInfo();
    }

    private void loadLayout() {

        BarUtils.addMarginTopEqualStatusBarHeight(findViewById(R.id.tb_actionbar));
        btn_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View buttonView) {
//                btn_follow.setChecked(!btn_follow.isChecked());
                if (Globals.isMyUsrUid(usrUid)) {
                    startActivity(MypageProfileActivity.class, false, 0, 0);
                } else {
                    if (!dicUsrInfo.getUsrLikeStatus()) {
                        final Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "", Snackbar.LENGTH_INDEFINITE);
                        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                        snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
                        layout.setPadding(0, 0, 0, 0);//set padding to 0
                        View view = inflater.inflate(R.layout.action_sheet1, null);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        layout.addView(view, params);

                        ((TextView) view.findViewById(R.id.tv_name)).setText(dicUsrInfo.getUsrNckNm());
                        ImageUtils.load(UserActivity.this, dicUsrInfo.getProfileImg(), android.R.color.transparent, R.drawable.ic_user_default, ((ImageView) view.findViewById(R.id.iv_photo)));
                        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                                btn_follow.setChecked(true);
                            }
                        });
                        view.findViewById(R.id.btn_follow_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestUserFollow();
                                snackbar.dismiss();
                            }
                        });
                        snackbar.show();
                    } else {
                        requestUserFollow();
                    }
                }
            }
        });

        int initialTab = getIntent().getIntExtra(INITIAL_TAB_INDEX, 0);

        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
                refresh();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);

        tl_top_tab.getTabAt(initialTab).select();
        setSelectedTabStyle(0);

        AppBarLayout app_bar_layout = (AppBarLayout) findViewById(R.id.appbar);
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                // RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) tv_title.getLayoutParams();
                int h = Utils.dpToPixel(UserActivity.this, rl_bg.getHeight() - 56 - 20);
                int b = Utils.dpToPixel(UserActivity.this, 18) * (h + verticalOffset) / h;
                if (b < 0) b = 0;
                final Animation animationFadeIn = AnimationUtils.loadAnimation(UserActivity.this, R.anim.fade_in);
                final Animation animationFadeOut = AnimationUtils.loadAnimation(UserActivity.this, R.anim.fade_out);
                if (verticalOffset <= -rl_bg.getHeight() / 2) {
                    if (tv_title.getVisibility() == View.INVISIBLE) {
                        rl_user.startAnimation(animationFadeOut);
                        rl_user.setVisibility(View.INVISIBLE);
                        tv_introduce.startAnimation(animationFadeOut);
                        tv_introduce.setVisibility(View.INVISIBLE);
                        tv_title.startAnimation(animationFadeIn);
                        tv_title.setVisibility(View.VISIBLE);
                    }

                } else {
                    if (tv_title.getVisibility() == View.VISIBLE) {
                        rl_user.startAnimation(animationFadeIn);
                        rl_user.setVisibility(View.VISIBLE);
                        tv_introduce.startAnimation(animationFadeIn);
                        tv_introduce.setVisibility(View.VISIBLE);
                        tv_title.startAnimation(animationFadeOut);
                        tv_title.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

    }

    private void initFragment() {
        if (itemFragment == null) {
            itemFragment = new WareListFragment();
            itemFragment.parentCollapsable = true;
            itemFragment.setUsrUid(usrUid);
            itemFragment.setFragType(Constants.WIRE_FRAG_USR_PDT);
            itemFragment.showFilter = true;
            itemFragment.showFilterButton = true;
        }
        if (iitemFragment == null) {
            iitemFragment = new WareListFragment();
            iitemFragment.parentCollapsable = true;
            iitemFragment.setUsrUid(usrUid);
            iitemFragment.setFragType(Constants.WIRE_FRAG_USR_WISH);
            iitemFragment.showFilter = true;
            iitemFragment.isLazyLoading = true;
            iitemFragment.showFilterButton = true;
        }

        if (styleFragment == null) {
            styleFragment = new WareListFragment();
            styleFragment.parentCollapsable = true;
            styleFragment.setUsrUid(usrUid);
            styleFragment.setFragType(Constants.WIRE_FRAG_USR_STYLE);
            styleFragment.setDataType(DATA_WARE | DATA_STYLE);
            styleFragment.showFilter = true;
            styleFragment.isLazyLoading = true;
            styleFragment.showFilterButton = true;
        }
    }

    public void refresh() {
        if (vp_main.getCurrentItem() == 0 && itemFragment != null) {
            if (itemFragment.getRecyclerView() != null)
                itemFragment.getRecyclerView().scrollToPosition(0);
            itemFragment.removeAll();
            itemFragment.refresh();
            itemFragment.setListTitle(dicUsrInfo.getUsrNckNm() + "님 아이템");
        }

        if (vp_main.getCurrentItem() == 1 && iitemFragment != null) {
            if (iitemFragment.getRecyclerView() != null)
                iitemFragment.getRecyclerView().scrollToPosition(0);
            iitemFragment.removeAll();
            iitemFragment.refresh();
            iitemFragment.setListTitle(dicUsrInfo.getUsrNckNm() + "님 잇템");
        }

        if (vp_main.getCurrentItem() == 2 && styleFragment != null) {
            if (styleFragment.getRecyclerView() != null)
                styleFragment.getRecyclerView().scrollToPosition(0);
            styleFragment.removeAll();
            styleFragment.refresh();
            styleFragment.setListTitle(dicUsrInfo.getUsrNckNm() + "님 스타일");
        }
    }

    private void setUserData(UsrInfoDto dicUser) {
        ImageUtils.load(iv_profile_img.getContext(), dicUser.getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile_img);
        ImageUtils.load(iv_profile_bg.getContext(), dicUser.getProfileBackImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_profile_bg);
        tv_point.setText(Utils.getAttachCommaFormat((int) dicUser.getPoint()));
        tv_follower.setText(Utils.getAttachCommaFormat((int) dicUser.getUsrFollowerCount()));
        tv_following.setText(Utils.getAttachCommaFormat((int) dicUser.getUsrFollowingCount()));
        tv_name.setText(dicUser.getUsrNckNm());
        tv_title.setText(dicUser.getUsrNckNm());

        if (dicUser.getUsrLikeStatus()){
            btn_follow.setChecked(false);
        } else {
            btn_follow.setChecked(true);
        }

        //본인인 경우
        if (Globals.isMyUsrUid(usrUid)) {
            btn_follow.setCompoundDrawables(getResources().getDrawable(R.drawable.brand_ic_follow_add_check), null, null, null);
            btn_follow.setText(getString(R.string.profile_modify));
        }

        itemFragment.setListTitle(dicUsrInfo.getUsrNckNm() + "님 아이템");
    }

//    public Model.WareItem[] loadItemWareData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_6);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadIitemWareData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_6);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadStyleWareData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_15);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = itemFragment;
                    break;
                case 1:
                    frag = iitemFragment;
                    break;
                case 2:
                    frag = styleFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    //get user info
    private void getOtherUsrInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrInfoDto>> genRes = restAPI.usrDetail(Globals.userToken, usrUid);

        genRes.enqueue(new TokenCallback<UsrInfoDto>(this) {
            @Override
            public void onSuccess(UsrInfoDto response) {
                closeLoading();

                dicUsrInfo = response;
                setUserData(dicUsrInfo);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                finish();
            }
        });
    }

    //user block
    private void requestUsrBlock() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.usrBlock(Globals.userToken, usrUid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(UserActivity.this, getString(R.string.usr_block_success));
                getOtherUsrInfo();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //user unblock
    private void requestUsrUnBlock() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.usrUnBlock(Globals.userToken, usrUid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(UserActivity.this, getString(R.string.usr_unblock_success));
                getOtherUsrInfo();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //req report
    public void reportUsr(final PopupDialog popup) {

        CustomSpinner spinner2 = ((CustomSpinner) popup.findView(R.id.spinner2));
        if (spinner2.getSelectedItemPosition() == 0) {
            Utils.showToast(UserActivity.this, "신고 사유를 선택해 주세요.");
            return;
        }

        ReportReq reportReq = new ReportReq();
        reportReq.setKind(2);
        reportReq.setTargetUid(usrUid);
        reportReq.setReportType(REPORT_TYPE.values()[spinner2.getSelectedItemPosition() - 1]);
        reportReq.setContent(((EditText) popup.findView(R.id.txt_report_detail)).getText().toString());

        if (TextUtils.isEmpty(reportReq.getContent())) {
            Utils.showToast(UserActivity.this, "신고 사유 내용을 작성해 주세요.");
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.reportOther(Globals.userToken, reportReq);
        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(UserActivity.this, "신고하였습니다.");
                popup.dismiss();
                Utils.hideSoftKeyboard(UserActivity.this);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //req user follow/unfollow
    private void requestUserFollow() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = dicUsrInfo.getUsrLikeStatus() ?
                restAPI.unFollowUsr(Globals.userToken, usrUid) :
                restAPI.followUsr(Globals.userToken, usrUid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                getOtherUsrInfo();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
