/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.PdtDetailDto;
import com.kyad.selluv.api.request.PdtCreateReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.sell.SellCompleteActivity;
import com.kyad.selluv.sell.SellPolicyActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


public class SellSignFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.sign_pad)
    SignaturePad mSignaturePad;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.tv_ware_name_eng)
    TextView tv_ware_name_eng;
    @BindView(R.id.tv_ware_name_ko)
    TextView tv_ware_name_ko;
    @BindView(R.id.tv_ware_size)
    TextView tv_ware_size;
    @BindView(R.id.tv_ware_price)
    TextView tv_ware_price;
    @BindView(R.id.iv_nego_ware)
    ImageView iv_nego_ware;
    @BindView(R.id.tv_nego_commit_description)
    TextView tv_nego_commit_description;


    //Vars
    private View rootView;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public String thumbFileName = "";
    private String signImageName = "";

    private String strpdt = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_sign, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    /**
     * 판매정책확인 버튼
     */
    @OnClick(R.id.tv_sell_policy)
    void onPolicy(View v) {
        Intent i = new Intent(getActivity(), SellPolicyActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btn_close)
    void onClose(View v) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.frag_main)
    void onBackClick(View v) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_ok)
    void onOkk(View v) {

//        Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
        Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
        Bitmap backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sell_direct_sign_pattern);
        Bitmap finalBitmap = overlay(backgroundBitmap, signatureBitmap);

        if (!addJpgSignatureToGallery(finalBitmap)) {
            Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
            return;
        }
//        if (!addSvgSignatureToGallery(mSignaturePad.getSignatureSvg())) {
//            Toast.makeText(getActivity(), "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
//        }

        uploadFile();
    }

    private void loadLayout() {
        verifyStoragePermissions(getActivity());
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                mSignaturePad.setBackgroundResource(R.drawable.sell_direct_signing);
            }

            @Override
            public void onSigned() {
                btn_ok.setBackgroundColor(Color.BLACK);
                btn_ok.setEnabled(true);
            }

            @Override
            public void onClear() {
                btn_ok.setBackgroundColor(getResources().getColor(R.color.color_999999));
                btn_ok.setEnabled(false);
            }
        });

        tv_ware_name_eng.setText(Globals.pdtCreateReq.getBrandEN());
        // 한글브랜드 + 상품군
        int likegroup = Globals.pdtCreateReq.getPdtGroup().getCode() == 1 ? 0 : Globals.pdtCreateReq.getPdtGroup().getCode() == 2 ? 1 : 2;
        String detail_info = Globals.pdtCreateReq.getBrandKO() + " " + Constants.shopper_type[likegroup];
        // + 컬러
        if (Globals.pdtCreateReq.getPdtColor().length() > 0)
            detail_info = detail_info + " " + Globals.pdtCreateReq.getPdtColor();
        // + 모델명
        if (Globals.pdtCreateReq.getPdtModel().length() > 0)
            detail_info = detail_info + " " + Globals.pdtCreateReq.getPdtModel();
        // + 상세카테고리
        if (Globals.pdtCreateReq.getCategoryName().length() > 0)
            detail_info = detail_info + " " + Globals.pdtCreateReq.getCategoryName();

        tv_ware_name_ko.setText(detail_info);

        try {
            iv_nego_ware.setImageURI(Uri.fromFile(new File(Globals.pdtCreateReq.getPhotoPaths().get(0))));
        } catch (Exception e) {

        }

        SpannableString spannablecontent = new SpannableString(getString(R.string.policy_guide));
        spannablecontent.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 9, 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannablecontent.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 29, 35, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_nego_commit_description.setText(spannablecontent);
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, 0, null);
        return bmOverlay;
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    private void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            signImageName = photo.getPath();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

//    public boolean addSvgSignatureToGallery(String signatureSvg) {
//        boolean result = false;
//        try {
//            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
//            OutputStream stream = new FileOutputStream(svgFile);
//            OutputStreamWriter writer = new OutputStreamWriter(stream);
//            writer.write(signatureSvg);
//            writer.close();
//            stream.flush();
//            stream.close();
//            scanMediaFile(svgFile);
//            result = true;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }

    /**
     * Checks if the app has permission to write to device storage
     * <p/>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity the activity from which permissions are checked
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void cmdRegPdt() {

        ((BaseActivity) getActivity()).showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<PdtDetailDto>> genRes = restAPI.registerPdt(Globals.userToken, Globals.pdtCreateReq);
        genRes.enqueue(new TokenCallback<PdtDetailDto>(getActivity()) {
            @Override
            public void onSuccess(PdtDetailDto response) {
                ((BaseActivity) getActivity()).closeLoading();

                Globals.pdtCreateReq.init();
                //상품 등록성공후 초기화한다.

                if (Globals.pdtCreateReq == null) {
                    return;
                }

                SharedPreferences pref = getActivity().getSharedPreferences("createpdt",
                        Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                strpdt = new Gson().toJson(Globals.pdtCreateReq, PdtCreateReq.class);
                editor.putString("PdtCreateReq", strpdt).apply();


                Intent intent = new Intent(getActivity(), SellCompleteActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onFailed(Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
            }
        });
    }

    private void uploadFile() {
        ((BaseActivity) getActivity()).showLoading();

        File file = new File(signImageName);
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("uploadfile", file.getName(), surveyBody);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<HashMap<String, String>>> genRes = restAPI.uploadImage(multipartBody);
        genRes.enqueue(new TokenCallback<HashMap<String, String>>(getActivity()) {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                ((BaseActivity) getActivity()).closeLoading();
                //fileName, fileURL
                Globals.pdtCreateReq.setSignImg(response.get("fileName"));

                cmdRegPdt();
            }

            @Override
            public void onFailed(Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
            }
        });
    }

}
