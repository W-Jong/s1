package com.kyad.selluv.api.dto.usr;

import com.kyad.selluv.api.dto.pdt.PdtListDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UsrFeedPdtListDto extends PdtListDto {
    //("피드데이터")
    UsrFeedDao usrFeed;
    //("팔로우유저")
    UsrMiniDto peerUsr;
}
