package com.kyad.selluv.api.dto.mypage;

import com.kyad.selluv.api.dto.other.ReviewListDto;
import com.kyad.selluv.api.dto.pdt.Page;

import lombok.Data;

@Data
public class ReviewContentDto {
//    @ApiModelProperty("후기내용")
    private Page<ReviewListDto> reviews;
//    @ApiModelProperty("만족갯수")
    private long countTwoPoint;
//    @ApiModelProperty("보통갯수")
    private long countOnePoint;
//    @ApiModelProperty("불만족갯수")
    private long countZeroPoint;
}
