/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.mypage.SellerSettingActivity;
import com.kyad.selluv.user.UserActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SellerVacationFragment extends BaseFragment {

    public SellerVacationFragment() {
    }

    public void setSwitchStatus() {
        if (Globals.myInfo.getUsr().getSleepYn() == 0) {
            tb_switch.setSelected(false);
            ((SellerSettingActivity) getActivity()).setIsSleep(false);
            setOffText();
        } else {
            tb_switch.setSelected(true);
            ((SellerSettingActivity) getActivity()).setIsSleep(true);
            setOnText();
        }
    }

    //UI Referencs
    @BindView(R.id.tv_guide)
    TextView tv_guide;
    @BindView(R.id.tb_switch)
    Button tb_switch;

    //Vars
    View rootView;
    private Snackbar snackbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_vacation_mode, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;

    }

    private void setOnText() {
        SpannableString guide = new SpannableString(getString(R.string.vacation_mode_text));
        guide.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 0, 13, 0);
        guide.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 33, 35, 0);
        guide.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 43, 45, 0);
        tv_guide.setText(guide);
    }

    private void setOffText() {
        SpannableString guide = new SpannableString(getString(R.string.vacation_mode_guide));
        guide.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 19, 21, 0);
        guide.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 29, 31, 0);
        tv_guide.setText(guide);
    }

    private void loadLayout() {

        tb_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_switch.isSelected()) {
                    ((SellerSettingActivity) getActivity()).setIsSleep(false);
                    ((SellerSettingActivity) getActivity()).setTimeOutDate(0);
                    ((SellerSettingActivity) getActivity()).reqSellerSetting();
                } else {
//                    ((SellerSettingActivity) getActivity()).setIsSleep(false);
//                    ((SellerSettingActivity) getActivity()).reqSellerSetting();
                    showPickerDate();
                }
            }
        });

        setSwitchStatus();
    }

    private void showPickerDate() {
        final PopupDialog opopup = new PopupDialog((BaseActivity) getActivity(), R.layout.action_sheet5).setFullScreen().setCancelable(true);
        final View rootView1 = opopup.findView(R.id.ll_style_add_background);
        final CustomSpinner spinner2 = ((CustomSpinner) opopup.findView(R.id.spinner_date));
        String[] dates = new String[30];
        for (int i = 0; i < 30; i++) {
            dates[i] = String.format("%d%s", i + 1, "일 후");
        }
        spinner2.initializeStringValues(getActivity(), dates, getActivity().getResources().getString(R.string.select_date1), null, null, 12.5f);

        opopup.setOnClickListener(R.id.btn_cancel, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                opopup.dismiss();
                setOffText();
            }
        });

        opopup.setOnClickListener(R.id.btn_confirm, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                opopup.dismiss();
                int dateIndex = spinner2.getSelectedItemPosition();
                ((SellerSettingActivity) getActivity()).setTimeOutDate(dateIndex);
                ((SellerSettingActivity) getActivity()).setIsSleep(true);
                ((SellerSettingActivity) getActivity()).reqSellerSetting();
            }
        });
        opopup.show();
    }

}
