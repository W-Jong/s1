package com.kyad.selluv.api.request;


import android.support.annotation.IntRange;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserSellerSettingReq {


    //@ApiModelProperty("무료배송여부")
    private Boolean isFreeSend;

    @IntRange(from = 0)
    //@ApiModelProperty("무료배송가격, isFreeSend가 true일때만 유효")
    private long freeSendPrice;

    //@ApiModelProperty("휴가모두여부")
    private Boolean isSleep;
    @IntRange(from = 0)
    //@ApiModelProperty("타임아웃 날짜, isSleep이 true일때만 유효")
    private int timeOutDate;
}
