package com.kyad.selluv.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDetailDto;
import com.kyad.selluv.api.dto.other.UsrMoneyHisContentDto;
import com.kyad.selluv.api.dto.other.UsrMoneyHisDto;
import com.kyad.selluv.brand.BrandDetailActivity;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.mypage.PurchaseDetailActivity;
import com.kyad.selluv.mypage.SalesDetailActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class MoneyListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrMoneyHisDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public MoneyListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrMoneyHisDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_money_history, parent, false);
            final UsrMoneyHisDto anItem = (UsrMoneyHisDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (anItem.getKind()) {
                        case 1: //구매주문상세
                            goBuyPage(anItem.getTargetUid());
                            break;
                        case 2: //판매주문상세
                            goSellPage(anItem.getTargetUid());
                            break;
                        case 3: //판매주문상세
                            goSellPage(anItem.getTargetUid());
                            break;
                        case 4: //상품상세
                            goPdtDetailPage(anItem.getTargetUid());
                            break;
                        case 5: //유저페이지
                            goUserPage(anItem.getTargetUid());
                            break;
                        case 6: //유저페이지
                            goUserPage(anItem.getTargetUid());
                            break;
                        case 7: //유저페이지
                            goUserPage(anItem.getTargetUid());
                            break;
                        case 8: //주문상세페이지를 호출해보고 구매주문상세 혹은 판매주문상세 페이지로 이동시킨다.
                            getDealDetailInfo(anItem.getTargetUid());
                            break;
                        case 9: //구매주문상세
                            goBuyPage(anItem.getTargetUid());
                            break;

                    }
                }
            });

            TextView tv_action = (TextView) convertView.findViewById(R.id.tv_action);
            TextView tv_number = (TextView) convertView.findViewById(R.id.tv_number);
            TextView tv_date = (TextView) convertView.findViewById(R.id.tv_date);
            TextView tv_money = (TextView) convertView.findViewById(R.id.tv_money);
            tv_action.setText(anItem.getContent());
            tv_number.setText(anItem.getHintContent());
            tv_date.setText(Utils.getStringDate(anItem.getRegTime().getTime(), "yyyy.MM.dd"));
            tv_money.setText(Utils.getAttachCommaFormat((int) anItem.getAmount()) + " 원");
            if (anItem.getAmount() > 0)
                tv_money.setTextColor(activity.getResources().getColor(R.color.color_333333));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    //구매상세페이지로
    private void goBuyPage(int _uid){
        Intent intent = new Intent(activity, PurchaseDetailActivity.class);
        intent.putExtra(Constants.DEAL_UID, _uid);
        activity.startActivity(intent);
    }

    //상품 상세페이지로
    private void goPdtDetailPage(int _uid){
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, _uid);
        activity.startActivity(intent);
    }

    //판매상세페이지로
    private void goSellPage(int _uid){
        Intent intent = new Intent(activity, SalesDetailActivity.class);
        intent.putExtra(UserActivity.USR_UID, _uid);
        activity.startActivity(intent);
    }

    //유저페이지로
    private void goUserPage(int _uid){
        Intent intent = new Intent(activity, UserActivity.class);
        intent.putExtra(UserActivity.USR_UID, _uid);
        activity.startActivity(intent);
    }

    private void getDealDetailInfo(final int _uid){

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        activity.showLoading();

        Call<GenericResponse<DealDetailDto>> genRes = restAPI.getDealDetail(Globals.userToken, _uid);

        genRes.enqueue(new TokenCallback<DealDetailDto>(activity) {
            @Override
            public void onSuccess(DealDetailDto response) {
                activity.closeLoading();

                if (response.getDeal().getSellerUsrUid() == Globals.myInfo.getUsr().getUsrUid()){
                    goSellPage(_uid);
                } else {
                    goUserPage(_uid);
                }
            }

            @Override
            public void onFailed(Throwable t) {
                activity.closeLoading();
            }
        });
    }
}