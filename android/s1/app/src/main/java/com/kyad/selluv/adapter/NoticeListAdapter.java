package com.kyad.selluv.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dao.NoticeDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.mypage.EventDetailActivity;
import com.kyad.selluv.mypage.NoticeDetailActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class NoticeListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<NoticeDao> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public NoticeListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<NoticeDao> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_notice, parent, false);
            final NoticeDao anItem = (NoticeDao) getItem(position);
            if (anItem == null) {
                return convertView;
            }
            final int index = position;
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, NoticeDetailActivity.class);
                    intent.putExtra(Constants.NOTICE_UID, arrData.get(index).getNoticeUid());
                    activity.startActivity(intent);
                }
            });
            TextView txt_title = (TextView) convertView.findViewById(R.id.tv_action);
            txt_title.setText(arrData.get(position).getTitle());

            TextView txt_date = (TextView) convertView.findViewById(R.id.tv_date);
            txt_date.setText(Utils.getStringDate(arrData.get(position).getRegTime().getTime(), "yyyy.MM.dd"));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}