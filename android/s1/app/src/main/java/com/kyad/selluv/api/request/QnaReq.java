package com.kyad.selluv.api.request;


import lombok.Data;

@Data
public class QnaReq {
    //@ApiModelProperty("타이틀")
    //@Size(min = 1)
    private String title;
    //@ApiModelProperty("내용")
    //@Size(min = 1)
    private String content;
}
