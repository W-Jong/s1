package com.kyad.selluv.detail;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.top.TopLikeActivity;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class DetailRecStyleActivity extends BaseActivity {

    public static final String FRAG_TYPE = "FRAG_TYPE";
    public static final String PDT_UID = "PDT_UID";
    public static final String KEYWORD = "KEYWORD";

    private int pdtUid;
    private int fragType = 0; //7 연관스타일 8: 동일한 모델명 9:동일한 태그
    private String keyword = "";

    //UI Reference
    WareListFragment fg_ware_list;

    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.tv_title)
    TextView tv_title;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.ib_right)
    void onRight() {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }


    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_style);

        pdtUid = getIntent().getIntExtra(PDT_UID, 0);
        fragType = getIntent().getIntExtra(FRAG_TYPE, Constants.WIRE_FRAG_PDT_RELATION_STYLE);
        keyword = getIntent().getStringExtra(KEYWORD);

        if (TextUtils.isEmpty(keyword)) {
            keyword = "";
        }

        if (fragType == Constants.WIRE_FRAG_PDT_RELATION_STYLE)
            tv_title.setText(R.string.recommend_style);
        else
            tv_title.setText("#" + keyword);

        loadLayout();
        updateLikePdtBadge();
    }

    private void loadLayout() {
        fg_ware_list = new WareListFragment();
        fg_ware_list.showFilter = true;
        fg_ware_list.showFilterButton = true;
        fg_ware_list.isMiniImageShow = false;
//        fg_ware_list.parentCollapsable = true;
        fg_ware_list.setPdtUid(pdtUid);
        fg_ware_list.setDataType(fragType == Constants.WIRE_FRAG_PDT_RELATION_STYLE ? Constants.DATA_STYLE : Constants.DATA_WARE);
        fg_ware_list.listInfo = fragType == Constants.WIRE_FRAG_PDT_RELATION_STYLE ? "전체 스타일" : "검색결과";
        fg_ware_list.setFragType(fragType);
        fg_ware_list.setNonePage(R.drawable.category_none_ic_normal, getString(fragType == Constants.WIRE_FRAG_PDT_RELATION_STYLE ? R.string.none_style_img : R.string.no_matching_ware), "", "", null);
        fg_ware_list.setPdtTag(keyword);
        fg_ware_list.setAppbar((ViewGroup) findViewById(R.id.appbar));

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fg_ware_list, fg_ware_list);
        transaction.commit();
    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(this) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }
}
