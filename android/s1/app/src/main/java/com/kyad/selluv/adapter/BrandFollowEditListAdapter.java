package com.kyad.selluv.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kgj on 22/12/2016.
 */

public class BrandFollowEditListAdapter extends BaseAdapter {

    private final int INVALID_ID = -1;

    private HashMap<Integer, Integer> mIDHashMap = new HashMap<>();

    LayoutInflater inflater;
    private List<BrandListDto> brandData = null;
    public ArrayList<Integer> deletedIndex = new ArrayList<>();
    BaseActivity activity = null;
    public boolean editable = false;
    int selectedPos = 0;

    public BrandFollowEditListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(List<BrandListDto> addData) {
        brandData = addData;
        for (int i = 0; i < addData.size(); ++i) {
            mIDHashMap.put(addData.get(i).getBrandUid(), i);
        }
        deletedIndex.clear();
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (brandData == null)
            return 0;

        return brandData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (brandData == null)
            return null;
        return brandData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
        if (position < 0 || position >= brandData.size()) {
            return INVALID_ID;
        }

        BrandListDto person = brandData.get(position);
        return mIDHashMap.get(person.getBrandUid());
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            final BrandListDto brand = (BrandListDto) getItem(position);

            if (convertView == null)
                convertView = inflater.inflate(R.layout.item_brand_follow_edit, parent, false);

            if (brand == null)
                return convertView;

            TextView tv_brand = (TextView) convertView.findViewById(R.id.tv_brand);
            tv_brand.setText(brand.getNameEn());
            TextView tv_brand_other = (TextView) convertView.findViewById(R.id.tv_brand_other);
            tv_brand_other.setText(brand.getNameKo());

            ImageButton ib_delete = (ImageButton) convertView.findViewById(R.id.ib_delete);
            ImageView ib_drag = (ImageView) convertView.findViewById(R.id.ib_drag);
            ImageButton ib_go = (ImageButton) convertView.findViewById(R.id.ib_go);

            if (!editable) {
                ib_go.setVisibility(View.VISIBLE);
                ib_drag.setVisibility(View.GONE);
                ib_delete.setVisibility(View.GONE);
//                convertView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        activity.startActivity(BrandPageActivity.class, false, 0, 0);
//                    }
//                });
                ib_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        brand.setBrandLikeStatus(false);
                        brandData.remove(position);
                        notifyDataSetChanged();
                        deletedIndex.add(brand.getBrandUid());
                    }
                });
            } else {
                ib_delete.setVisibility(View.VISIBLE);
                ib_go.setVisibility(View.GONE);
                ib_drag.setVisibility(View.VISIBLE);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        notifyDataSetChanged();
    }


}
