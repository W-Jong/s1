package com.kyad.selluv.api.request;

import android.support.annotation.Size;

import com.kyad.selluv.api.enumtype.SNS_TYPE;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserSnsReq {

    //@ApiModelProperty("SNS유형")
    //@NonNull
    private SNS_TYPE snsType;

    //@ApiModelProperty("SNS아이디")
//    @Size(min = 1)
    private String snsId;

    //@ApiModelProperty("SNS이메일")
//    @NonNull
    private String snsEmail;

    //@ApiModelProperty("SNS닉네임")
//    @NonNull
    private String snsNckNm;

    //@ApiModelProperty("SNS프로필이미지")
//    @NonNull
    private String snsProfileImg;

    //@ApiModelProperty("SNS토큰")
//    @NonNull
    private String snsToken;
}
