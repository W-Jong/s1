package com.kyad.selluv.common;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kyad.selluv.R;

/**
 * @author piotr on 31.07.16.
 */

public class CustomSpinner extends android.support.v7.widget.AppCompatSpinner {
    private static final String TAG = "CustomSpinner";
    private OnSpinnerEventsListener mListener;
    private boolean mOpenInitiated = false;
    private ArrayAdapter spinnerArrayAdapter;

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSpinner(Context context, int mode) {
        super(context, mode);
    }

    public CustomSpinner(Context context) {
        super(context);
    }

    public interface OnSpinnerEventsListener {

        void onSpinnerOpened();

        void onSpinnerClosed();

    }

    @Override
    public boolean performClick() {
        // register that the Spinner was opened so we have a status
        // indicator for the activity(which may lose focus for some other
        // reasons)
        mOpenInitiated = true;
        if (mListener != null) {
            mListener.onSpinnerOpened();
        }
        return super.performClick();
    }

    public void setSpinnerEventsListener(OnSpinnerEventsListener onSpinnerEventsListener) {
        mListener = onSpinnerEventsListener;
    }

    /**
     * Propagate the closed Spinner event to the listener from outside.
     */
    public void performClosedEvent() {
        mOpenInitiated = false;
        if (mListener != null) {
            mListener.onSpinnerClosed();
        }
    }

    /**
     * A boolean flag indicating that the Spinner triggered an open event.
     *
     * @return true for opened Spinner
     */
    public boolean hasBeenOpened() {
        return mOpenInitiated;
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        android.util.Log.d(TAG, "onWindowFocusChanged");
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasBeenOpened() && hasWindowFocus) {
            android.util.Log.i(TAG, "closing popup");
            performClosedEvent();
        }
    }

    public void initializeStringValues(String[] values) {
        spinnerArrayAdapter = new ArrayAdapter<>(this.getContext
                (), android.R.layout.simple_spinner_dropdown_item);
        spinnerArrayAdapter.addAll(values);
        setAdapter(spinnerArrayAdapter);
    }

    public void initializeStringValues(final Context context, String[] values, String promptText, final Typeface defalutFont, final Typeface hintFont, final float fontSize) {
        spinnerArrayAdapter = new ArrayAdapter<String>(this.getContext(), R.layout.simple_combo_item) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                if (position == 0) {
                    tv.setTextColor(Color.rgb(212, 212, 212));
                    tv.setTypeface(hintFont);
                } else {
                    tv.setTextColor(Color.rgb(0, 0, 0));
                    tv.setTypeface(defalutFont);
                }
                tv.setTextSize(fontSize);
                return tv;
            }

            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                if (position == 0) {
                    tv.setTextColor(Color.rgb(212, 212, 212));
                    tv.setTypeface(hintFont);
                } else {
                    tv.setTextColor(Color.rgb(153, 153, 153));
                    tv.setTypeface(defalutFont);
                }
                tv.setTextSize(fontSize);
                return tv;
            }
        };
        spinnerArrayAdapter.add(promptText);
        spinnerArrayAdapter.addAll(values);
        setAdapter(spinnerArrayAdapter);
    }

    public
    @Nullable
    Object getItem(int position) {
        if (spinnerArrayAdapter != null && position < spinnerArrayAdapter.getCount()) {
            return spinnerArrayAdapter.getItem(position);
        } else {
            return null;
        }
    }
}