package com.kyad.selluv.login.fragment;

import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.DbManager;
import com.kyad.selluv.R;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Constants.Direction;
import com.kyad.selluv.common.IndicatorView;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.login.PasswordResetActivity;
import com.kyad.selluv.model.Model;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.VIEW_BOUNCE_ANIM_DURATION;
import static com.kyad.selluv.common.Globals.getLoginTypeIdx;

public class LoginRevisitFragment extends LoginBaseFragment implements TextWatcher {

    int accountMarks[] = {
            R.drawable.login_ic_mail01,
            R.drawable.login_ic_facebook03,
            R.drawable.login_ic_naver03,
            R.drawable.login_ic_kakao03
    };

    //UI Reference
    @BindView(R.id.ib_setting)
    ImageButton ib_setting;
    @BindView(R.id.tv_finish)
    TextView tv_finish;
    @BindView(R.id.ib_back)
    ImageButton ib_back;
    @BindView(R.id.edt_id)
    EditText edt_id;
    @BindView(R.id.edt_pwd)
    EditText edt_pwd;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.vp_user_list)
    ViewPager vp_user_list;
    @BindView(R.id.rl_indicator)
    IndicatorView rl_indicator;

    public int loginRevisitTabIndex = 0;   // 0 : login revisit page 1: login_id page 2: revisit register page

    //Variables
    private View rootView;
    private boolean isInEditMode = false;
    private int childCnt = 0;
    private int startViewIDs[] = {R.id.vp_user_list, R.id.rl_indicator, R.id.btn_signup, R.id.tv_login_other, R.id.ib_setting, R.id.tv_finish};
    private int loginViewIDs[] = {R.id.ib_back, R.id.edt_id, R.id.edt_pwd, R.id.btn_login, R.id.tv_pwd_forget, R.id.tv_or, R.id.ll_sns};
    private int registerViewIDs[] = {R.id.rl_facebook, R.id.rl_kakao, R.id.rl_naver, R.id.tv_back, R.id.tv_email};
    private List<Model.UserLoginInfo> accounts = new ArrayList<>();
    private ArrayList<View> views = new ArrayList<View>();
    private int selectedPage = 0;

    public static LoginRevisitFragment newInstance(int prevChildCnt, Direction from) {
        LoginRevisitFragment fragment = new LoginRevisitFragment();
        Bundle args = new Bundle();
        args.putInt(PREV_CHILD_CNT, prevChildCnt);
        args.putInt(FROM_DIRECTION, from.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login_revisit, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    if (loginRevisitTabIndex == 1) {
//                        toggleLogin(true);
//                        return true;
//                    } else if (loginRevisitTabIndex == 2) {
//                        toggleRegister(true);
//                        return true;
//                    } else
//                        return false;
//                }
//                return false;
//            }
//        });
    }

    //Events for revisit page
    @OnClick(R.id.btn_signup)
    void onSingup() {
        toggleRegister(false);
    }

    @OnClick(R.id.tv_login_other)
    void onLoginOther(View v) {
        toggleLogin(false);
    }

    @OnClick(R.id.ib_setting)
    void onSetting() {
        ib_setting.setVisibility(View.GONE);
        tv_finish.setVisibility(View.VISIBLE);
        ib_setting.setClickable(false);
        tv_finish.setClickable(true);
        isInEditMode = true;
        toggleEditMode();
    }

    @OnClick(R.id.tv_finish)
    void onFinish() {
        ib_setting.setVisibility(View.VISIBLE);
        tv_finish.setVisibility(View.GONE);
        ib_setting.setClickable(true);
        tv_finish.setClickable(false);
        isInEditMode = false;
        toggleEditMode();
    }

    //Events for login_id page
    @OnClick(R.id.ib_back)
    public void onBackToRevisit(View v) {
        toggleLogin(true);
    }

    @OnClick({R.id.rl_facebook, R.id.ib_facebook})
    void onFacebookStart(View v) {
        ((LoginStartActivity) getActivity()).facebookLogin();
    }

    @OnClick({R.id.rl_kakao, R.id.ib_kakao})
    void onKakaoStart(View v) {
        ((LoginStartActivity) getActivity()).kakaotalkLogin();
    }

    @OnClick({R.id.rl_naver, R.id.ib_naver})
    void onNaverStart(View v) {
        ((LoginStartActivity) getActivity()).naverLogin();
    }

    @OnClick(R.id.tv_pwd_forget)
    void onForgetPassword(View v) {
        ((LoginStartActivity) getActivity()).startActivity(PasswordResetActivity.class, false, 0, 0);
    }


    @OnClick(R.id.btn_login)
    void onEmailLogin(View v) {
        ((LoginStartActivity) getActivity()).login(edt_id.getText().toString(), edt_pwd.getText().toString(), "NORMAL");
    }

    //Events for register page

    @OnClick(R.id.tv_back)
    void onBack() {
        toggleRegister(true);
    }

    @OnClick({R.id.tv_email, R.id.ib_email})
    void onEmailReg(View v) {
        ((LoginStartActivity) getActivity()).gotoEmailRegisterFragment(childCnt, Direction.BOTTOM, true);
    }

    void onKeyboardShow() {
        View v = rootView.findViewById(R.id.activity_main);
        if (v != null) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
            lp.topMargin = -Utils.dpToPixel(getActivity(), 140);
            v.setLayoutParams(lp);
        }
    }

    void onKeyboardHidden() {
        View v = rootView.findViewById(R.id.activity_main);
        if (v != null) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
            lp.topMargin = 0;
            v.setLayoutParams(lp);
        }
    }

    private void loadLayout() {

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                if (rootView.getRootView().getHeight() - (r.bottom - r.top) > Utils.dpToPixel(rootView.getContext(), 100)) { // if more than 100 pixels, its probably a keyboard...
                    onKeyboardShow();
                } else {
                    onKeyboardHidden();
                }
            }
        });

        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.activity_main));

        accounts = DbManager.loadInfo();
        if (loginRevisitTabIndex == 0) {
            childCnt = startViewIDs.length + 3;
            for (int i = loginViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(loginViewIDs[i]);
                v.setVisibility(View.GONE);
            }
            for (int i = registerViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(registerViewIDs[i]);
                v.setVisibility(View.GONE);
            }
            for (int i = 0; i < startViewIDs.length; i++) {
                View v = rootView.findViewById(startViewIDs[i]);
                v.setVisibility(View.VISIBLE);
            }

            isInEditMode = false;
            ib_setting.setVisibility(View.VISIBLE);
            tv_finish.setVisibility(View.GONE);

            addAccounts();
        } else if (loginRevisitTabIndex == 1) {
            childCnt = 10;
            for (int i = startViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(startViewIDs[i]);
                v.setVisibility(View.GONE);
            }
            for (int i = registerViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(registerViewIDs[i]);
                v.setVisibility(View.GONE);
            }
            for (int i = 0; i < loginViewIDs.length; i++) {
                View v = rootView.findViewById(loginViewIDs[i]);
                v.setVisibility(View.VISIBLE);
            }
        } else {
            childCnt = 10;
            for (int i = startViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(startViewIDs[i]);
                v.setVisibility(View.GONE);
            }
            for (int i = loginViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(loginViewIDs[i]);
                v.setVisibility(View.GONE);
            }
            for (int i = 0; i < registerViewIDs.length; i++) {
                View v = rootView.findViewById(registerViewIDs[i]);
                v.setVisibility(View.VISIBLE);
            }
        }

        edt_id.addTextChangedListener(this);

        edt_pwd.addTextChangedListener(this);
    }

    private void updateItemInfo(View vAccount, final int idx) {
        final Model.UserLoginInfo loginInfo = accounts.get(idx);

        CircularImageView rightImg = (idx % 2 == 0) ? (CircularImageView) vAccount.findViewById(R.id.iv_left_user) : (CircularImageView) vAccount.findViewById(R.id.iv_right_user);
        ImageButton rightMark = (idx % 2 == 0) ? (ImageButton) vAccount.findViewById(R.id.ib_left_mark) : (ImageButton) vAccount.findViewById(R.id.ib_right_mark);
        TextView rightName = (idx % 2 == 0) ? (TextView) vAccount.findViewById(R.id.tv_left_nickname) : (TextView) vAccount.findViewById(R.id.tv_right_nickname);

        rightName.setText(loginInfo.nickname);
        Picasso.with(getActivity()).load(Uri.parse(loginInfo.profile_url))
                .fit()
                .centerCrop()
                .placeholder(R.drawable.default_profile)
                .into(rightImg);
        rightMark.setImageResource(isInEditMode ? R.drawable.login_ic_edit_del : accountMarks[getLoginTypeIdx(loginInfo.login_type)]);

        rightMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInEditMode) {
                    cmdDeleteAccount(idx);
                } else {
                    ((LoginStartActivity) getActivity()).login(loginInfo.userid, loginInfo.password, loginInfo.login_type);
                }
            }
        });

        rightImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isInEditMode) {
                    ((LoginStartActivity) getActivity()).login(loginInfo.userid, loginInfo.password, loginInfo.login_type);
                }
            }
        });
    }

    private void cmdDeleteAccount(int idx) {
        Model.UserLoginInfo loginInfo = accounts.get(idx);
        DbManager.deleteInfo(loginInfo.userid);
        accounts.remove(idx);
        Utils.showToast(getActivity(), loginInfo.nickname + "계정이 삭제되었습니다.");
        addAccounts();
    }

    void toggleLogin(final boolean fromLogin) {

        if (fromLogin) loginRevisitTabIndex = 0;
        else loginRevisitTabIndex = 1;

        if (fromLogin) childCnt = startViewIDs.length + 3;
        else childCnt = loginViewIDs.length + 3;
        edt_id.clearFocus();
        edt_pwd.clearFocus();

        toggleStartView(fromLogin);

        for (int i = 0; i < loginViewIDs.length; i++) {
            View v = rootView.findViewById(loginViewIDs[i]);
            if (!fromLogin) {
                v.setVisibility(View.VISIBLE);
                v.setClickable(true);
            }
            Animation anim = AnimationUtils
                    .loadAnimation(Utils.getApp().getApplicationContext(), fromLogin ? R.anim.view_out_to_right : R.anim.view_in_from_right);
            anim.setStartOffset(i * VIEW_BOUNCE_ANIM_DURATION + (fromLogin ? 0 : 500));
            anim.setFillAfter(true);
            if (fromLogin && i == loginViewIDs.length - 1) {
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        for (int i = 0; i < loginViewIDs.length; i++) {
                            View v = rootView.findViewById(loginViewIDs[i]);
                            v.setVisibility(View.GONE);
                            v.setClickable(false);
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
            v.startAnimation(anim);
        }
    }

    private void toggleStartView(final boolean fromV) {

        for (int i = 0; i < startViewIDs.length; i++) {
            View v = rootView.findViewById(startViewIDs[i]);
            if (fromV) {
                if ((isInEditMode && i == startViewIDs.length - 2) || (!isInEditMode && i == startViewIDs.length - 1)) {
                    v.setVisibility(View.GONE);
                    v.setClickable(false);
                    continue;
                } else {
                    v.setVisibility(View.VISIBLE);
                    v.setClickable(true);
                }
            }

            Animation anim = AnimationUtils
                    .loadAnimation(Utils.getApp().getApplicationContext(), fromV ? R.anim.view_in_from_left : R.anim.view_out_to_left);
            anim.setFillAfter(true);
            anim.setStartOffset(i * VIEW_BOUNCE_ANIM_DURATION + (fromV ? 500 : 0));
            //마지막 뷰가 애니매이션 된 후에 모든 뷰는 숨겨라!!!
            if (!fromV && i == startViewIDs.length - 1) {
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        for (int i = 0; i < startViewIDs.length; i++) {
                            final View v = rootView.findViewById(startViewIDs[i]);
                            v.setVisibility(View.GONE);
                            v.setClickable(false);
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }
            v.startAnimation(anim);
        }
    }

    void toggleRegister(final boolean fromRegister) {

        if (fromRegister) loginRevisitTabIndex = 0;
        else loginRevisitTabIndex = 2;

        if (fromRegister) childCnt = startViewIDs.length + 3;
        else childCnt = registerViewIDs.length + 3;
        edt_id.clearFocus();
        edt_pwd.clearFocus();

        toggleStartView(fromRegister);

        for (int i = 0; i < registerViewIDs.length; i++) {
            View v = rootView.findViewById(registerViewIDs[i]);
            if (!fromRegister) {
                v.setVisibility(View.VISIBLE);
                v.setClickable(true);
            }
            Animation anim = AnimationUtils
                    .loadAnimation(Utils.getApp().getApplicationContext(), fromRegister ? R.anim.view_out_to_right : R.anim.view_in_from_right);
            anim.setStartOffset(i * VIEW_BOUNCE_ANIM_DURATION + (fromRegister ? 0 : 500));
            anim.setFillAfter(true);
            if (i == registerViewIDs.length - 1) {
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (fromRegister) {
                            for (int i = 0; i < registerViewIDs.length; i++) {
                                View v = rootView.findViewById(registerViewIDs[i]);
                                v.setVisibility(View.GONE);
                                v.setClickable(false);
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }
            v.startAnimation(anim);
        }
    }

    void addAccounts() {
        views.clear();
        vp_user_list.setAdapter(null);
        for (int i = 0; i < accounts.size(); i += 2) {
            View vAccount = View.inflate(getActivity(), R.layout.item_login_account, null);

            updateItemInfo(vAccount, i);

            if (i + 1 < accounts.size()) {
                updateItemInfo(vAccount, i + 1);
            } else {
                if (i == 0) {
                    vAccount.findViewById(R.id.rl_right).setVisibility(View.GONE);
                } else {
                    vAccount.findViewById(R.id.rl_right).setVisibility(View.INVISIBLE);
                }
            }
            views.add(vAccount);
        }

        PagerAdapter mPagerAdapter = new PagerAdapter() {

            @Override
            public int getCount() {
                return views.size();
            }

            @Override
            public int getItemPosition(@NonNull Object object) {
                int index = views.indexOf(object);
                if (index == -1)
                    return POSITION_NONE;
                else
                    return index;
                //return super.getItemPosition(object);
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            @NonNull
            @Override
            public Object instantiateItem(ViewGroup collection, int position) {
                View v = views.get(position);
                collection.addView(v);
                return v;
            }

            @Override
            public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
                collection.removeView((View) view);
            }
        };
        vp_user_list.setAdapter(mPagerAdapter);
        vp_user_list.setOffscreenPageLimit(views.size());
        mPagerAdapter.notifyDataSetChanged();

        vp_user_list.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        vp_user_list.setCurrentItem(Math.min(selectedPage, views.size() - 1));
        rl_indicator.setCount(views.size(), Math.min(selectedPage, views.size() - 1));
        rl_indicator.setPager(vp_user_list);
    }

    void toggleEditMode() {
        for (int i = 0; i < vp_user_list.getChildCount(); i++) {
            View vAccount = vp_user_list.getChildAt(i);
            Model.UserLoginInfo leftAccount = accounts.get(i * 2);
            ImageButton leftMark = vAccount.findViewById(R.id.ib_left_mark);

            leftMark.setImageResource(isInEditMode ? R.drawable.login_ic_edit_del : accountMarks[getLoginTypeIdx(leftAccount.login_type)]);
            if (i * 2 + 1 < accounts.size()) {
                Model.UserLoginInfo rightAccount = accounts.get(i + 1);
                ImageButton rightMark = vAccount.findViewById(R.id.ib_right_mark);
                rightMark.setImageResource(isInEditMode ? R.drawable.login_ic_edit_del : accountMarks[getLoginTypeIdx(rightAccount.login_type)]);
            } else {
                if (i == 0) {
                    vAccount.findViewById(R.id.rl_right).setVisibility(View.GONE);
                } else {
                    vAccount.findViewById(R.id.rl_right).setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (!edt_id.getText().toString().isEmpty() && !edt_pwd.getText().toString().isEmpty())
            btn_login.setEnabled(true);
        else
            btn_login.setEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}