package com.kyad.selluv.api.request;


import lombok.Data;
import lombok.NonNull;

@Data
public class DealDeliveryNumReq {
//    @NonNull
    //@ApiModelProperty("택배사")
    private String deliveryCompany;

//    @NonNull
    //@ApiModelProperty("운송장번호")
    private String deliveryNumber;
}
