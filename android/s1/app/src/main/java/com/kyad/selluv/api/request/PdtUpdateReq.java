package com.kyad.selluv.api.request;


import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;

import java.util.List;

import lombok.Data;

@Data
public class PdtUpdateReq {

    //    @Size(min = 1)
    //@ApiModelProperty("상품이미지 리스트")
    private List<String> photos;

    //    @IntRange(from = 50000)
    //@ApiModelProperty("가격")
    private long price;

    //    @IntRange(from = 0)
    //@ApiModelProperty("배송비")
    private long sendPrice;

    //    @Size(min = 1)
    //@ApiModelProperty("상품사이즈")
    private String pdtSize;


    //@ApiModelProperty("컨디션")
    private PDT_CONDITION_TYPE pdtCondition;


    //@ApiModelProperty("태그")
    private String tag;

    //    @Size(min = 1)
    //@Pattern(value = "^[01]{6}$")
    //@ApiModelProperty("부속품 예:110101 6자리문자열(0-없음, 1-있음)  첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백")
    private String component;


    //@ApiModelProperty("기타")
    private String etc;


    //@ApiModelProperty("컬러")
    private String pdtColor;


    //@ApiModelProperty("모델명")
    private String pdtModel;


    //@ApiModelProperty("상세설명")
    private String content;

    //    @IntRange(from = 1)
    //@ApiModelProperty("네고허용여부 1-네고허용 2-네고 받지 않음")
    private int negoYn;


    //@ApiModelProperty("프로모션코드")
    private String promotionCode;


    //@ApiModelProperty("판매여부")
    private boolean isSell;

    public PdtUpdateReq() {
        init();
    }

    public void init() {
        this.photos = null;
        this.price = 50000;
        this.sendPrice = 0;
        this.pdtSize = "FREE";
        this.pdtCondition = PDT_CONDITION_TYPE.GOOD;
        this.tag = "";
        this.component = "000000";
        this.etc = "";
        this.pdtColor = "멀티";
        this.pdtModel = "기타";
        this.content = "";
        this.negoYn = 2;
        this.promotionCode = "";
        this.isSell = true;
    }

    public boolean getIsSell() {
        return this.isSell;
    }

    public void setIsSell(boolean isSell) {
        this.isSell = isSell;
    }

}
