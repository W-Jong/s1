package com.kyad.selluv;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by KSH on 3/28/2018.
 */

public class ShareManager {
    public static final String SM_AUTO_LOGIN = "SM_AUTO_LOGIN";
    public static final String SM_LAST_LOGIN_ID = "SM_LAST_LOGIN_ID";

    private final String PREF_NAME = ShareManager.class.getCanonicalName();
    private static Context mContext;
    private static ShareManager manager = null;
    int mMode = Activity.MODE_PRIVATE;

    public static ShareManager manager() {
        if (manager == null) {
            manager = new ShareManager();
        }
        mContext = BaseApplication.getInstance();
        return manager;
    }

    public String getValue(String key, String defValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, mMode);
        try {
            return pref.getString(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    public int getValue(String key, int defValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, mMode);
        try {
            return pref.getInt(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    public Boolean getValue(String key, Boolean defValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, mMode);
        try {
            return pref.getBoolean(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    public void put(String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, mMode);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void put(String key, Boolean value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, mMode);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void put(String key, int value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, mMode);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }
}
