/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.COLOR_NAMES;


public class SellDirectInfoAddColor extends DialogFragment {


    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;
    final int[] btnIDs = {R.id.rl_black, R.id.rl_gray, R.id.rl_white, R.id.rl_beige, R.id.rl_red, R.id.rl_pink, R.id.rl_purple, R.id.rl_blue, R.id.rl_green, R.id.rl_yellow, R.id.rl_orange, R.id.rl_brown, R.id.rl_gold, R.id.rl_silver, R.id.rl_multi};

    private int selectedPos = -1, orgSelectedPos = -1;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_color, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onClickDone(View v) {
        String s_color = "";
        if (selectedPos >= 0) {
            s_color = COLOR_NAMES[selectedPos];
        }
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtColor(s_color);
        } else {
            Globals.pdtCreateReq.setPdtColor(s_color);
        }

        ((TextView) getActivity().findViewById(R.id.tv_color_inputs)).setText(s_color);
        getView().setVisibility(View.GONE);
    }

    @OnClick({R.id.rl_black, R.id.rl_gray, R.id.rl_white, R.id.rl_beige, R.id.rl_red, R.id.rl_pink, R.id.rl_purple, R.id.rl_blue, R.id.rl_green, R.id.rl_yellow, R.id.rl_orange, R.id.rl_brown, R.id.rl_gold, R.id.rl_silver, R.id.rl_multi})
    void onColor(View v) {
        boolean goBack = true;

        for (int i = 0; i < btnIDs.length; i++) {
            if (v.getId() == btnIDs[i]) {
                if (orgSelectedPos == i) {
                    selectedPos = -1;
                    orgSelectedPos = -1;
                    rootView.findViewById(btnIDs[i]).findViewWithTag("check").setVisibility(View.INVISIBLE);
                    goBack = false;
                } else {
                    selectedPos = i;
                    v.findViewWithTag("check").setVisibility(View.VISIBLE);
                    orgSelectedPos = selectedPos;
                }
            } else {
                rootView.findViewById(btnIDs[i]).findViewWithTag("check").setVisibility(View.INVISIBLE);
            }
        }

        if (goBack) {
            onClickDone(v);
        }
    }

    private void loadLayout() {

        String s_color;
        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            ib_right.setVisibility(View.GONE);
            s_color = Globals.pdtUpdateReq.getPdtColor();
        } else {
            tv_finish.setVisibility(View.GONE);
            ib_right.setVisibility(View.VISIBLE);
            s_color = Globals.pdtCreateReq.getPdtColor();
        }
        for (int i = 0; i < Constants.COLOR_NAMES.length; i++) {
            if (Constants.COLOR_NAMES[i].equals(s_color)) {
                selectedPos = i;
                rootView.findViewById(btnIDs[i]).findViewWithTag("check").setVisibility(View.VISIBLE);
            } else {
                rootView.findViewById(btnIDs[i]).findViewWithTag("check").setVisibility(View.INVISIBLE);
            }
        }
    }

}
