package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import com.kyad.selluv.api.enumtype.REPORT_TYPE;

import lombok.Data;
import lombok.NonNull;

@Data
public class ReportReq {
    //@ApiModelProperty("대상회원 UID, 회원신고인 경우 usrUid, 상품신고인 경우 pdtUid")

    private int targetUid;
    //@ApiModelProperty("신고유형 1-회원신고, 2-상품신고")
    @IntRange(from = 0)
    private int kind;
    //@ApiModelProperty("신고사유")

    private REPORT_TYPE reportType;
    //@ApiModelProperty("신고내용")
    @Size(min = 1)
    private String content;
}
