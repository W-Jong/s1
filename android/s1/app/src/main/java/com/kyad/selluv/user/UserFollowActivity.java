package com.kyad.selluv.user;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.UserFollowFragment;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class UserFollowActivity extends BaseActivity {

    private int usrUid = 0;

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.tv_title)
    TextView txt_title;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    int selectedTab = 1;
    private PageAdapter pagerAdapter;
    UserFollowFragment followFragment, followingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_follow);

        usrUid = getIntent().getIntExtra(Constants.USER_UID, 0);
        selectedTab = getIntent().getIntExtra(Constants.FOLLOW_PAGE_IDX, 0);
        txt_title.setText(getIntent().getStringExtra(Constants.FOLLOW_PAGE_TITLE));

        initFragment();
        //setStatusBarWhite();
        loadLayout();
        selectPage();
    }

    private void initFragment() {

        if (followFragment == null) {
            followFragment = new UserFollowFragment();
            followFragment.setTitle(R.string.follower);
        }

        if (followingFragment == null) {
            followingFragment = new UserFollowFragment();
            followingFragment.setTitle(R.string.following);
        }
    }


    private void loadLayout() {
        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
//                tl_top_tab.getTabAt(position).select();
                selectPage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);

        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle(0);
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private void selectPage() {
        if (selectedTab == 0) {
            followFragment.setTab(0, usrUid);
        } else {
            followingFragment.setTab(1, usrUid);
        }
        tl_top_tab.getTabAt(selectedTab).select();
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = followFragment;
                    break;
                case 1:
                    frag = followingFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
