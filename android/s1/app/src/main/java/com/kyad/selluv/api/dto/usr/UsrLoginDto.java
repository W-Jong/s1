package com.kyad.selluv.api.dto.usr;

import java.util.List;

import lombok.Data;

@Data
public class UsrLoginDto {
    //("액세스 토큰")
    String accessToken;
    //("회원정보")
    UsrDetailDto usr;
    //("주소정보")
    private List<UsrAddressDao> addressList;
}
