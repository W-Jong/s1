/**
 * 홈
 */
package com.kyad.selluv.sell;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;

import butterknife.OnClick;


public class SellValetActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_vallet_guide);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        onClose(null);
    }

    @OnClick(R.id.ib_close)
    void onClose(View v) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @OnClick({R.id.btn_request, R.id.ib_request})
    void onRequest(View v) {
        setResult(Activity.RESULT_OK);
        finish();
    }

}
