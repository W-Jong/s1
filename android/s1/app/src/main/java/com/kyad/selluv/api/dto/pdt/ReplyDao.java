package com.kyad.selluv.api.dto.pdt;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class ReplyDao {
    private int replyUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"회원UID")
    private int usrUid;
    //"상품UID")
    private int pdtUid;
    //"@붙은 회원UID, 콤마로 구분, 0인 경우 해당 유저상세페이지로 이동불가")
    private String targetUids = "";
    //"내용")
    private String content = "";
    private int status = 1;

    public String getTargetUids() {
        return targetUids;
    }
}
