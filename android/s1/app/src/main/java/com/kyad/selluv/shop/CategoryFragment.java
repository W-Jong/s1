package com.kyad.selluv.shop;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandRecommendedListDto;
import com.kyad.selluv.api.dto.category.CategoryDetailDto;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.top.TopLikeActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.TAB_BAR_HEIGHT;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_CATEGORY;
import static com.kyad.selluv.common.Constants.female_category1;
import static com.kyad.selluv.common.Constants.kids_category1;
import static com.kyad.selluv.common.Constants.male_category1;
import static com.kyad.selluv.common.Constants.shopper_type;
import static com.kyad.selluv.common.Globals.categoryDepth;
import static com.kyad.selluv.common.Globals.currentShopperTypeIdx;

public class CategoryFragment extends BaseFragment {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.ib_right)
    ImageButton ib_right;

    LinearLayout ll_brands;
    LinearLayout ll_categories;

    WareListFragment fragment_popular, fragment_detail;
    View vPopular, vDetail;

    //Variables
    View rootView;
    int selectedTab = 0;    //  0: 피드, 1: 인기, 2: 신상
    ArrayList<View> brandList = new ArrayList<View>();
    ArrayList<ToggleButton> categoryList = new ArrayList<ToggleButton>();
    boolean isFinalCategory = false;
    int selectedBrand = -1, selectedCategory = -1;
    FragmentManager fm;
    FragmentTransaction transaction;
    Fragment mainFragment;
    private int basisCategoryUid = 1;


    private CategoryDetailDto categoryDetail;

    public static CategoryFragment newInstance(Fragment mainFragment) {
        CategoryFragment fragment = new CategoryFragment();
        fragment.mainFragment = mainFragment;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_main, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        categoryDepth = 0;
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    private void loadLayout() {

        basisCategoryUid = (Globals.categoryDepth <= 1) ? Globals.categoryIndex[0] : Globals.categoryIndex[Globals.categoryDepth - 1];

//        isFinalCategory = (categoryDetail.getChildrenCategories().size() == 0);

        BarUtils.addMarginTopEqualStatusBarHeight(rootView.findViewById(R.id.appbar));
        rootView.findViewById(R.id.appbar).setAlpha(0.95f);

        fm = getChildFragmentManager();
        if (categoryDepth == 0) {
            tv_title.setText(shopper_type[currentShopperTypeIdx]);
        } else if (categoryDepth == 1) {
            tv_title.setText(shopper_type[currentShopperTypeIdx] + " " + getCategoryTitle());
        } else {
            tv_title.setText(shopper_type[currentShopperTypeIdx] + " " + getCategoryTitle());
        }


        isFinalCategory = true;
        if (isFinalCategory) {
            tl_top_tab.setVisibility(View.GONE);
        }

        initFragment();

        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        vPopular = View.inflate(getActivity(), R.layout.fragment_category, null);
        vDetail = View.inflate(getActivity(), R.layout.fragment_category, null);
        vPopular.findViewById(R.id.frag_ware_list).setId(R.id.frag_ware_list_popular);
        vDetail.findViewById(R.id.frag_ware_list).setId(R.id.frag_ware_list_detail);
        vDetail.findViewById(R.id.sv_brands).setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPixel(getActivity(), 88)));

        ll_brands = vPopular.findViewById(R.id.ll_brands);
        ll_categories = vDetail.findViewById(R.id.ll_brands);

        View ll_top1 = vPopular.findViewById(R.id.sv_brands);
        ViewGroup viewParent1 = (ViewGroup) ll_top1.getParent();
        viewParent1.removeView(ll_top1);
        fragment_popular.headerViews.add(ll_top1);

        View ll_top2 = vDetail.findViewById(R.id.sv_brands);
        ViewGroup viewParent2 = (ViewGroup) ll_top2.getParent();
        viewParent2.removeView(ll_top2);
        fragment_detail.headerViews.add(ll_top2);

        vp_main.addView(vPopular, 0);
        vp_main.addView(vDetail, 1);

        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return isFinalCategory ? 1 : 2;
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == (View) object;
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup collection, int position) {
                return vp_main.getChildAt(position);
            }

            @Override
            public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
                collection.removeView((View) view);
            }
        };
        vp_main.setAdapter(mPagerAdapter);
        vp_main.setOffscreenPageLimit(isFinalCategory ? 1 : 2);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();

                refreshList();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle(0);

        updateLikePdtBadge();

        addWareFragment(R.id.frag_ware_list_popular, fragment_popular);
        addWareFragment(R.id.frag_ware_list_detail, fragment_detail);

        getCategoryDetail(basisCategoryUid);
    }

    private void refreshList() {
        if (categoryDetail == null)
            return;

        if (selectedTab == 0) {
            int brandUid = (selectedBrand == -1 || categoryDetail.getRecommendedBrand().get(selectedBrand) == null) ? 0 : categoryDetail.getRecommendedBrand().get(selectedBrand).getBrandUid();
            fragment_popular.setBrandUid(brandUid);
            fragment_popular.setCategoryUid(basisCategoryUid);
            fragment_popular.removeAll();
            fragment_popular.refresh();
        } else {
            int categoryUid = (selectedCategory == -1 || categoryDetail.getChildrenCategories().get(selectedCategory) == null) ? basisCategoryUid : categoryDetail.getChildrenCategories().get(selectedCategory).getCategoryUid();
            fragment_detail.setBrandUid(0);
            fragment_detail.setCategoryUid(categoryUid);
            fragment_detail.removeAll();
            fragment_detail.refresh();
        }
    }

    private void initFragment() {
        LinearLayout appbar = rootView.findViewById(R.id.appbar);
        if (fragment_popular == null) {
            fragment_popular = new WareListFragment();
            fragment_popular.paddintTop = isFinalCategory ? Utils.dpToPixel(getActivity(), 50) + BarUtils.getStatusBarHeight() : Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
            fragment_popular.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
            fragment_popular.showFilterButton = true;
            fragment_popular.showFilter = true;
            fragment_popular.isMiniImageShow = false;
            fragment_popular.setFragType(WIRE_FRAG_CATEGORY);
            fragment_popular.setCategoryUid(basisCategoryUid);
            fragment_popular.setBrandUid(0);
            fragment_popular.setAppbar(appbar);
        }

        if (fragment_detail == null) {
            fragment_detail = new WareListFragment();
            fragment_detail.paddintTop = isFinalCategory ? Utils.dpToPixel(getActivity(), 50) + BarUtils.getStatusBarHeight() : Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
            fragment_detail.setNonePage(R.drawable.category_none_ic_normal, getString(R.string.no_matching_ware), "", "", null);
            fragment_detail.showFilterButton = true;
            fragment_detail.showFilter = true;
            fragment_detail.isMiniImageShow = false;
            fragment_detail.setFragType(WIRE_FRAG_CATEGORY);
            fragment_detail.setCategoryUid(basisCategoryUid);
            fragment_detail.setBrandUid(0);
            fragment_detail.setAppbar(appbar);
        }
    }

    @OnClick(R.id.ib_left)
    void onBack(View v) {
//        FragmentTransaction transaction = mainFragment.getFragmentManager().beginTransaction();
//        getChildFragmentManager().popBackStackImmediate();
//        transaction.commitAllowingStateLoss();
        if (mainFragment instanceof ShopMainFragment) {
            ((ShopMainFragment) mainFragment).closePopupFragment();
        } else {
            Toast.makeText(getActivity(), "TODO back feature", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    void addWareFragment(int destResID, WareListFragment frag) {
        transaction = fm.beginTransaction();
        transaction.replace(destResID, frag);
        transaction.commit();
    }

    private void addPopularBrands() {
        if (categoryDetail == null || categoryDetail.getRecommendedBrand() == null)
            return;

        ll_brands.removeAllViews();
        for (int i = 0; i < categoryDetail.getRecommendedBrand().size(); i++) {
            BrandRecommendedListDto item = categoryDetail.getRecommendedBrand().get(i);
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View v = inflater.inflate(R.layout.item_shop_popular_brand, null);
            if ("".equals(item.getLogoImg())) {
                ((ImageView) v.findViewById(R.id.iv_brand_logo)).setVisibility(View.GONE);
                ((TextView) v.findViewById(R.id.tv_brand_name)).setText(item.getNameEn());
            } else {
                ((TextView) v.findViewById(R.id.tv_brand_name)).setVisibility(View.GONE);
                ImageUtils.load(getActivity(), item.getLogoImg(), ((ImageView) v.findViewById(R.id.iv_brand_logo)));
            }
            ImageUtils.load(getActivity(), item.getBackImg(), ((ImageView) v.findViewById(R.id.iv_brand_back)));
            ((TextView) v.findViewById(R.id.tv_ware_cnt)).setText(String.valueOf(item.getPdtCount()));
            brandList.add(v);
            final int index = i;
            v.findViewById(R.id.tb_select).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((ToggleButton) view).isChecked())
                        selectedBrand = index;
                    else selectedBrand = -1;
                    updateBrands(selectedBrand);
                }
            });

            ll_brands.addView(v);
        }
    }

    private void addDetailCategories() {
        if (categoryDetail == null || categoryDetail.getChildrenCategories() == null)
            return;

        ll_categories.removeAllViews();
        for (int i = 0; i < categoryDetail.getChildrenCategories().size(); i++) {
            String category = categoryDetail.getChildrenCategories().get(i).getCategoryName();
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View v = inflater.inflate(R.layout.item_shop_detail_category, null);
            ToggleButton tb_category = ((ToggleButton) v.findViewById(R.id.tb_category));
            tb_category.setText(category);
            categoryList.add((ToggleButton) v.findViewById(R.id.tb_category));
            final int index = i;
            tb_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((ToggleButton) view).isChecked())
                        selectedCategory = index;
                    else selectedCategory = -1;
                    updateCategories(selectedCategory);
                }
            });

            ll_categories.addView(v);
        }
    }

    public void updateBrands(int index) {
        for (int i = 0; i < brandList.size(); i++) {
            if (i != index) {
                ToggleButton tb_select = (ToggleButton) brandList.get(i).findViewById(R.id.tb_select);
                tb_select.setChecked(false);
                ((ImageView) brandList.get(i).findViewById(R.id.iv_brand_back_cover)).setImageDrawable(getResources().getDrawable(R.color.color_000000_80));
            } else {
                ((ImageView) brandList.get(i).findViewById(R.id.iv_brand_back_cover)).setImageDrawable(getResources().getDrawable(R.color.color_000000_30));
            }
        }
        refreshList();
    }

    public void updateCategories(int index) {
        for (int i = 0; i < categoryList.size(); i++) {
            ToggleButton tb_select = (ToggleButton) categoryList.get(i);
            if (i != index) {
                tb_select.setChecked(false);
            } else {
                tb_select.setChecked(true);
            }
        }
        refreshList();
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private String getCategoryTitle() {
        if (categoryDepth == 0) {
            return shopper_type[currentShopperTypeIdx];
        } else if (categoryDepth == 1) {
            String[] titleArray = Globals.currentShopperTypeIdx == 0 ? male_category1 : Globals.currentShopperTypeIdx == 1 ? female_category1 : kids_category1;
            return titleArray[Globals.categoryIndex[0] - 5 - Globals.currentShopperTypeIdx * 5 + 1];
        }
        return "";
    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(getActivity()) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void getCategoryDetail(int selectedCategoryUid) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<CategoryDetailDto>> genRes = restAPI.categoryDetail(Globals.userToken, selectedCategoryUid);

        genRes.enqueue(new TokenCallback<CategoryDetailDto>(getActivity()) {
            @Override
            public void onSuccess(CategoryDetailDto response) {
                closeLoading();
                categoryDetail = response;

                if (Globals.categoryDepth > 1) {
                    tv_title.setText(shopper_type[currentShopperTypeIdx] + " " + categoryDetail.getCategory().getCategoryName());
                }

                addPopularBrands();
                addDetailCategories();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
    }
}
