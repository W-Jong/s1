package com.kyad.selluv.api.dto.brand;

import lombok.Data;

@Data
public class BrandDetailDto {
    //("브랜드UID")
    private int brandUid;
    //("영문첫글자")
    private String firstEn;
    //("한글초성")
    private String firstKo;
    //("영문이름")
    private String nameEn;
    //("한글이름")
    private String nameKo;
    //("라이센스")
    private String licenseUrl;
    //("로고이미지")
    private String logoImg;
    //("썸네일이미지")
    private String profileImg;
    //("백그라운드 이미지")
    private String backImg;
    //("팔로우 유저수")
    private int brandLikeCount;
    //("유저 팔로우 상태")
    private Boolean brandLikeStatus;
    //("남성상품군 상품갯수")
    private int malePdtCount;
    //("여성상품군 상품갯수")
    private int femalePdtCount;
    //("키즈상품군 상품갯수")
    private int kidsPdtCount;
    //("스타일 상품갯수")
    private int stylePdtCount;
}
