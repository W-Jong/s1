package com.kyad.selluv.api.enumtype;

public enum FEED_TYPE {
    FEED01_FINDING_PDT(1), FEED02_LIKE_PDT_SALE(2), FEED03_FOLLOWING_BRAND(3),
    FEED04_FOLLOWING_USER_WISH(4), FEED05_FOLLOWING_USER_STYLE(5), FEED06_FOLLOWING_USER_PDT(6);

    private int code;

    private FEED_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
