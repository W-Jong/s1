/**
 * 부속품 등록 페이지
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.COMPONENT_NAMES;


public class SellDirectInfoAddAcc extends DialogFragment {

    //UI Refs
    @BindView(R.id.edt_etc)
    EditText edt_etc;
    @BindView(R.id.iv_etc)
    ImageButton iv_etc;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;
    final int[] btnIDs = {R.id.iv_acc01, R.id.iv_acc02, R.id.iv_acc03, R.id.iv_acc04, R.id.iv_acc05, R.id.iv_acc06, R.id.iv_etc};
    int[] imgIDs = {R.drawable.sell_direct_ic_part01,
            R.drawable.sell_direct_ic_part02,
            R.drawable.sell_direct_ic_part03,
            R.drawable.sell_direct_ic_part04,
            R.drawable.sell_direct_ic_part05,
            R.drawable.sell_direct_ic_part06,
            R.drawable.sell_direct_ic_part07,
    };
    int[] imgOverIDs = {R.drawable.sell_direct_ic_part01_over,
            R.drawable.sell_direct_ic_part02_over,
            R.drawable.sell_direct_ic_part03_over,
            R.drawable.sell_direct_ic_part04_over,
            R.drawable.sell_direct_ic_part05_over,
            R.drawable.sell_direct_ic_part06_over,
            R.drawable.sell_direct_ic_part07_over
    };

    boolean[] selected = {false, false, false, false, false, false};


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_sell_info_add_acc, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onDone(View v) {
        getView().setVisibility(View.GONE);
        String acc = "";
        String component = "";
        for (int i = 0; i < selected.length; i++) {
            if (selected[i]) {
                acc += COMPONENT_NAMES[i] + " ";
                component = component + "1";
            } else {
                component = component + "0";
            }
        }

        if (edt_etc.length() > 0) {
            //ETC
            if (Globals.isEditing) {
                Globals.pdtUpdateReq.setEtc(edt_etc.getText().toString());
            } else {
                Globals.pdtCreateReq.setEtc(edt_etc.getText().toString());
            }
            acc += edt_etc.getText().toString();
        }


        //예:110101 6자리문자열(0-없음, 1-있음) 첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백 ,
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setComponent(component);
        } else {
            Globals.pdtCreateReq.setComponent(component);
        }
        //kkk
        ((TextView) getActivity().findViewById(R.id.tv_belonging_input)).setText(acc);
    }

    @OnClick({R.id.iv_acc01, R.id.iv_acc02, R.id.iv_acc03, R.id.iv_acc04, R.id.iv_acc05, R.id.iv_acc06, R.id.iv_etc})
    void onAcc(View v) {
        updateButtonState(v);
    }

    private void loadLayout() {
        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.frag_main));

        String s_component;
        String s_etc;
        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            ib_right.setVisibility(View.GONE);
            s_component = Globals.pdtUpdateReq.getComponent();
            s_etc = Globals.pdtUpdateReq.getEtc();
        } else {
            tv_finish.setVisibility(View.GONE);
            ib_right.setVisibility(View.VISIBLE);
            s_component = Globals.pdtCreateReq.getComponent();
            s_etc = Globals.pdtCreateReq.getEtc();
        }
        if (s_component.length() == selected.length) {
            for (int i = 0; i < s_component.length(); i++) {
                if (s_component.substring(i).startsWith("1")) {
                    selected[i] = true;
                } else {
                    selected[i] = false;
                }
                rootView.findViewById(btnIDs[i]).setBackgroundResource(selected[i] ? imgOverIDs[i] : imgIDs[i]);
            }
        }
        if (s_etc.length() > 0) {
            //마지막 ETC
            rootView.findViewById(btnIDs[6]).setBackgroundResource(imgOverIDs[6]);
            edt_etc.setText(s_etc);
        }

        edt_etc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    iv_etc.setBackgroundResource(R.drawable.sell_direct_ic_part07_over);
                } else {
                    iv_etc.setBackgroundResource(R.drawable.sell_direct_ic_part07);
                }
            }
        });
    }

    void updateButtonState(View v) {
        for (int i = 0; i < selected.length; i++) {
            if (v.getId() == btnIDs[i]) {
                selected[i] = !selected[i];
                v.setBackgroundResource(selected[i] ? imgOverIDs[i] : imgIDs[i]);
            }
        }
    }
}
