package com.kyad.selluv.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.usr.UsrMiniDto;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

public class BlockListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrMiniDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public BlockListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrMiniDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_block, parent, false);
            final UsrMiniDto anItem = (UsrMiniDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    activity.startActivity(intent);
                }
            });

            CircularImageView iv_profile = (CircularImageView) convertView.findViewById(R.id.iv_ware);
            ImageUtils.load(iv_profile.getContext(), anItem.getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile);

            TextView tv_right_nickname = (TextView) convertView.findViewById(R.id.tv_right_nickname);
            tv_right_nickname.setText(anItem.getUsrNckNm());

            TextView tv_id = (TextView) convertView.findViewById(R.id.tv_id);
            tv_id.setText(anItem.getUsrId());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}