package com.kyad.selluv.api.dao;

import java.sql.Timestamp;
import java.util.Objects;

public class NoticeDao {
    private int noticeUid;
    //@ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //@ApiModelProperty("타이틀")
    private String title;
    //@ApiModelProperty("내용")
    private String content;
    private int status;

    ////@Column(name = "notice_uid", nullable = false)
    public int getNoticeUid() {
        return noticeUid;
    }

    public void setNoticeUid(int noticeUid) {
        this.noticeUid = noticeUid;
    }


    //@Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }


    //@Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    //@Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    //@Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoticeDao noticeDao = (NoticeDao) o;
        return noticeUid == noticeDao.noticeUid &&
                status == noticeDao.status &&
                Objects.equals(regTime, noticeDao.regTime) &&
                Objects.equals(title, noticeDao.title) &&
                Objects.equals(content, noticeDao.content);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noticeUid, regTime, title, content, status);
    }
}
