/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.BrandPopularAdapter;
import com.kyad.selluv.adapter.HeaderViewRecyclerAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.TAB_BAR_HEIGHT;

public class BrandPopularFragment extends BaseFragment {

    //UI Refs
    @BindView(R.id.grid_view)
    RecyclerView grid_view;
    @BindView(R.id.tv_sort)
    TextView tv_sort;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @BindView(R.id.ib_goto_top)
    ImageButton ib_goto_top;
    @BindView(R.id.rl_detail)
    RelativeLayout rl_detail;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;

    //Vars
    View rootView;
    LinearLayout mTabMenu = null;
    LinearLayout appbar = null;
    BrandPopularAdapter adapter;
    private HeaderViewRecyclerAdapter mHeaderAdapter;
    protected ImageView mLoadingView;
    private final AutoLoadEventDetector mAutoLoadEventDetector = new AutoLoadEventDetector();
    public static int SHOW_TAB_THRESHOLD = 10;
    private int mScrolledDistance = 0;
    private boolean mTopBarVisible = true;
    private boolean mGoBtnVisible = true;
    boolean mIsLoading = false;
    int totalScrolledDistance = 0;
    private int selectedTabIndex = 0;
    View tipsView;

    @OnClick(R.id.ib_goto_top)
    void onTop() {
        grid_view.smoothScrollToPosition(0);
    }

    public BrandPopularFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_brand_popular, container, false);
        ButterKnife.bind(this, rootView);
        adapter = new BrandPopularAdapter((BaseActivity) getActivity());
        loadLayout();
        return rootView;
    }

    private void loadLayout() {

        tipsView = LayoutInflater.from(getContext()).inflate(R.layout.tips_loading, new FrameLayout(getContext()));

        mTabMenu = (LinearLayout) getActivity().findViewById(R.id.ll_bottom);
        appbar = getActivity().findViewById(R.id.appbar);

        mLoadingView = new ImageView(getActivity());
        mLoadingView.setLayoutParams(new RecyclerView.LayoutParams(
                RecyclerRefreshLayout.LayoutParams.MATCH_PARENT,
                (int) Utils.dpToPixel(getActivity(), 40)));
        mLoadingView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        mLoadingView.setImageResource(R.drawable.spinner);
        mLoadingView.setPadding((int) Utils.dpToPixel(getActivity(), 15), (int) Utils.dpToPixel(getActivity(), 10),
                0, (int) Utils.dpToPixel(getActivity(), 10));

        mHeaderAdapter = new HeaderViewRecyclerAdapter(adapter);

        tv_sort.setText(String.format(getString(R.string.brand_rank), "남성"));
        grid_view.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        grid_view.setLayoutManager(llm);
        grid_view.setAdapter(mHeaderAdapter);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_container.setRefreshing(false);
            }
        });


        LinearLayout headerView = new LinearLayout(getActivity());
        int paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT + 10) + BarUtils.getStatusBarHeight();
        headerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, paddintTop));
        headerView.setMinimumHeight(paddintTop);
        mHeaderAdapter.addHeaderView(headerView);

        ViewGroup viewParent = (ViewGroup) rl_detail.getParent();
        viewParent.removeView(rl_detail);
        mHeaderAdapter.addHeaderView(rl_detail);

        grid_view.addOnScrollListener(mAutoLoadEventDetector);

        if (Globals.myInfo != null && Globals.myInfo.getUsr() != null && Globals.myInfo.getUsr().getGender() == 2) {
            selectedTabIndex = 1; //여성
            tb_segment.setToggleStatus(TriStateToggleButton.ToggleStatus.mid, true);
            tv_sort.setText(String.format(getString(R.string.brand_rank), "여성"));
        } else {
            selectedTabIndex = 0; //남성
            tb_segment.setToggleStatus(TriStateToggleButton.ToggleStatus.off, true);
            tv_sort.setText(String.format(getString(R.string.brand_rank), "남성"));
        }
        getFameBrandList();

        tb_segment.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
                selectedTabIndex = toggleIntValue;
                if (adapter != null) {
                    adapter.setSelectedTabIndex(selectedTabIndex + 1);
                }
                if (toggleIntValue == 0) {
                    tv_sort.setText(String.format(getString(R.string.brand_rank), "남성"));
                } else if (toggleIntValue == 1) {
                    tv_sort.setText(String.format(getString(R.string.brand_rank), "여성"));
                } else {
                    tv_sort.setText(String.format(getString(R.string.brand_rank), "키즈"));
                }
                getFameBrandList();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (totalScrolledDistance < SHOW_TAB_THRESHOLD) {
                if (appbar != null) {
                    appbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                        @Override
                        public void run() {
                            appbar.setVisibility(View.VISIBLE);
                        }
                    }).start();
                }
            }
            mTopBarVisible = true;
        }
    }

    public void showLoading(boolean firstPage) {
        hideEmpty();
        if (firstPage) {
            AnimationDrawable drawable = (AnimationDrawable) ((ImageView) tipsView.findViewById(R.id.loading_view)).getDrawable();
            drawable.start();
            ViewGroup parent = (ViewGroup) grid_view.getParent().getParent();
            parent.addView(tipsView, new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            tipsView.bringToFront();
        }
    }

    public void hideLoading() {
        ViewGroup parent = (ViewGroup) grid_view.getParent().getParent();
        parent.removeView(tipsView);
    }

    public void showHasMore() {
        if (!getHeaderAdapter().containsFooterView(mLoadingView)) {
            ((AnimationDrawable) mLoadingView.getDrawable()).start();
            getHeaderAdapter().addFooterView(mLoadingView);
        }
    }

    public void hideHasMore() {
        getHeaderAdapter().removeFooterView(mLoadingView);
    }

    public void hideEmpty() {

    }

    public void showEmpty() {

    }

    public void addData(List<BrandListDto> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        adapter.dataList = list;
    }

    public void removeAll() {
        if (adapter == null) return;
        getHeaderAdapter().removeAllFooterView();
        getHeaderAdapter().removeAllHeaderView();
        adapter.dataList.clear();
        getHeaderAdapter().notifyDataSetChanged();
    }

    public void refresh() {
        mIsLoading = false;
        if (adapter == null) return;
        if (isFirstPage()) {
            showLoading(true);
        }

        requestRefresh();
    }

    public void requestRefresh() {
        if (mIsLoading)
            return;
        mIsLoading = true;
        adapter.dataList.clear();
        getFameBrandList();
    }

    public void requestMore() {
        if (mIsLoading)
            return;
        mIsLoading = true;
        getFameBrandList();
    }

    protected void requestComplete() {
        mIsLoading = false;

        hideEmpty();
        hideLoading();
    }

    public HeaderViewRecyclerAdapter getHeaderAdapter() {
        return mHeaderAdapter;
    }

    public boolean isFirstPage() {
        return adapter.getItemCount() <= 0;
    }

    public class AutoLoadEventDetector extends RecyclerView.OnScrollListener {

        private static final int HIDE_THRESHOLD = 10;

        @Override
        public void onScrolled(RecyclerView view, int dx, int dy) {
            totalScrolledDistance += dy;
            /* Hide/Show BottomBar */
            if (mScrolledDistance > HIDE_THRESHOLD) {
                if (mGoBtnVisible) {
                    if (ib_goto_top != null) {
                        ib_goto_top.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                ib_goto_top.setVisibility(View.VISIBLE);
                            }
                        }).start();
                    }
                    if (mTabMenu != null) {
                        mTabMenu.animate().translationY(mTabMenu.getHeight()).setInterpolator(new AccelerateInterpolator(2)).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                mTabMenu.setVisibility(View.GONE);
                            }
                        }).start();
                    }
                    mGoBtnVisible = false;
                }

                if (mTopBarVisible) {
                    if (appbar != null) {
                        appbar.animate().translationY(-appbar.getHeight()).setInterpolator(new AccelerateInterpolator(2)).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                appbar.setVisibility(View.GONE);
                            }
                        }).start();
                    }
                    mTopBarVisible = false;
                }
                mScrolledDistance = 0;
            } else if (mScrolledDistance < -HIDE_THRESHOLD) {
                if (!mGoBtnVisible) {
                    if (ib_goto_top != null) {
                        ib_goto_top.animate().translationY(ib_goto_top.getHeight() + Utils.dpToPixel(getActivity(), 15)).setInterpolator(new DecelerateInterpolator(2)).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                ib_goto_top.setVisibility(View.GONE);
                            }
                        }).start();
                    }
                    if (mTabMenu != null) {
                        mTabMenu.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                mTabMenu.setVisibility(View.VISIBLE);
                            }
                        }).start();
                    }
                    mGoBtnVisible = true;
                }
                if (!mTopBarVisible) {
                    if (appbar != null) {
                        appbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                appbar.setVisibility(View.VISIBLE);
                            }
                        }).start();
                    }
                    mTopBarVisible = true;
                }

                mScrolledDistance = 0;
            }

            if (mTopBarVisible || (mGoBtnVisible && dy > 0) || (!mGoBtnVisible && dy < 0)) {
                mScrolledDistance += dy;
            }

            /* LoadMore */
            RecyclerView.LayoutManager manager = view.getLayoutManager();
            if (manager.getChildCount() > 0) {
                int count = manager.getItemCount();
                int last = ((RecyclerView.LayoutParams) manager
                        .getChildAt(manager.getChildCount() - 1).getLayoutParams()).getViewAdapterPosition();

                if (last == count - 1 && !mIsLoading) {
                    requestMore();
                }
            }
        }
    }

    private void getFameBrandList() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        String pdtGroupType = (selectedTabIndex == 0) ? PDT_GROUP_TYPE.MALE.toString() :
                (selectedTabIndex == 1) ? PDT_GROUP_TYPE.FEMALE.toString() : PDT_GROUP_TYPE.KIDS.toString();

        Call<GenericResponse<List<BrandListDto>>> genRes = restAPI.brandFameList(Globals.userToken, pdtGroupType);

        genRes.enqueue(new TokenCallback<List<BrandListDto>>(getActivity()) {
            @Override
            public void onSuccess(List<BrandListDto> response) {
                closeLoading();
                if (response == null || response.size() == 0) {
                    showEmpty();
                } else {
                    hideEmpty();
                }
                adapter.dataList.clear();
                addData(response);
                adapter.notifyDataSetChanged();
//                requestComplete();
//                showHasMore();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
