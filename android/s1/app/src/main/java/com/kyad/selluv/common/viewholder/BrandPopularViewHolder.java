package com.kyad.selluv.common.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BrandPopularViewHolder extends ViewHolder {
    public RelativeLayout rl_content;
    public TextView tv_rank;
    public TextView tv_brand_en;
    public TextView tv_brand_kr;
    public TextView tv_brand_logo;
    public TextView tv_follow_cnt;
    public ImageView iv_bg;
    public ImageView iv_logo;
    public ImageView iv_heart;
    public ImageView iv_ware1;
    public ImageView iv_ware2;
    public ImageView iv_ware3;
    public RelativeLayout rl_brand_logo;

    public BrandPopularViewHolder(View itemView) {
        super(itemView);
    }
}
