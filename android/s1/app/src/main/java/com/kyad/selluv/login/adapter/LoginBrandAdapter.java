package com.kyad.selluv.login.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.login.BrandSelectActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginBrandAdapter extends RecyclerView.Adapter {

    public List<BrandListDto> dataList = new ArrayList<>();
    public List<Integer> selectedBrands = new ArrayList<>();
    private BaseActivity activity;

    private boolean isFirstReg = false;

    public LoginBrandAdapter(BaseActivity activity) {
        this.activity = activity;
    }

    public void setTempStaus(boolean status) {
        isFirstReg = status;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.item_login_brand, null);
        LoginBrandViewHolder holder = new LoginBrandViewHolder(itemView);
        holder.tv_cnt = itemView.findViewById(R.id.tv_cnt);
        holder.iv_img = itemView.findViewById(R.id.iv_img);
        holder.iv_heart = itemView.findViewById(R.id.iv_heart);
        holder.ib_add = itemView.findViewById(R.id.ib_add);
        holder.iv_logo = itemView.findViewById(R.id.iv_logo);
        itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        if (dataList == null)
            return;
        LoginBrandViewHolder vh = (LoginBrandViewHolder) viewHolder;
        final BrandListDto item = dataList.get(position);

        Picasso.with(activity).load(Uri.parse(item.getBackImg()))
                .fit()
                .centerCrop()
                .placeholder(android.R.color.black)
                .into(vh.iv_img);
        Picasso.with(activity).load(Uri.parse(item.getLogoImg()))
                .fit()
                .centerInside()
                .placeholder(android.R.color.transparent)
                .into(vh.iv_logo);
        if (isFirstReg) {
            if (selectedBrands.contains(Integer.valueOf(item.getBrandUid()))) {
                vh.tv_cnt.setText(String.valueOf(item.getBrandLikeCount()));
                vh.tv_cnt.setVisibility(View.VISIBLE);
                vh.iv_heart.setVisibility(View.VISIBLE);
                vh.ib_add.setVisibility(View.INVISIBLE);
                vh.iv_img.setAlpha(1.0f);
                View.OnClickListener cancelAction = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cmdUnLike(item);
                    }
                };
                vh.tv_cnt.setOnClickListener(cancelAction);
                vh.iv_heart.setOnClickListener(cancelAction);
                vh.iv_img.setOnClickListener(cancelAction);
            } else {
                vh.iv_img.setAlpha(0.3f);
                vh.tv_cnt.setVisibility(View.INVISIBLE);
                vh.iv_heart.setVisibility(View.INVISIBLE);
                vh.ib_add.setVisibility(View.VISIBLE);
                vh.ib_add.setClickable(false);
                vh.iv_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cmdLike(item);
                    }
                });
            }
        } else {
            if (item.getBrandLikeStatus()) {
                vh.tv_cnt.setText(String.valueOf(item.getBrandLikeCount()));
                vh.tv_cnt.setVisibility(View.VISIBLE);
                vh.iv_heart.setVisibility(View.VISIBLE);
                vh.ib_add.setVisibility(View.INVISIBLE);
                vh.iv_img.setAlpha(1.0f);
                View.OnClickListener cancelAction = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cmdUnLike(item);
                    }
                };
                vh.tv_cnt.setOnClickListener(cancelAction);
                vh.iv_heart.setOnClickListener(cancelAction);
                vh.iv_img.setOnClickListener(cancelAction);
            } else {
                vh.iv_img.setAlpha(0.3f);
                vh.tv_cnt.setVisibility(View.INVISIBLE);
                vh.iv_heart.setVisibility(View.INVISIBLE);
                vh.ib_add.setVisibility(View.VISIBLE);
                vh.ib_add.setClickable(false);
                vh.iv_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cmdLike(item);
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        if (dataList == null)
            return 0;
        return dataList.size();
    }

    public class LoginBrandViewHolder extends ViewHolder {
        public TextView tv_cnt;
        public ImageView iv_logo;
        public ImageView iv_img;
        public ImageView iv_heart;
        public ImageButton ib_add;

        public LoginBrandViewHolder(View itemView) {
            super(itemView);
        }
    }

    private void cmdLike(final BrandListDto item) {
        activity.showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.likeBrand(Globals.userToken, item.getBrandUid());
        genRes.enqueue(new Callback<GenericResponse<Void>>() {

            @Override
            public void onResponse(Call<GenericResponse<Void>> call, Response<GenericResponse<Void>> response) {
                activity.closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    if (isFirstReg) {
                        selectedBrands.add(Integer.valueOf(item.getBrandUid()));
                        ((BrandSelectActivity) activity).setBtnEnable(true);
                        notifyDataSetChanged();
                    } else {
                        ((BrandSelectActivity) activity).getBrandAllList();
                    }
                } else {//실패
                    Toast.makeText(activity, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<Void>> call, Throwable t) {
                activity.closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cmdUnLike(final BrandListDto item) {
        activity.showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.unLikeBrand(Globals.userToken, item.getBrandUid());
        genRes.enqueue(new Callback<GenericResponse<Void>>() {

            @Override
            public void onResponse(Call<GenericResponse<Void>> call, Response<GenericResponse<Void>> response) {
                activity.closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    if (isFirstReg) {
                        selectedBrands.remove(Integer.valueOf(item.getBrandUid()));
                        notifyDataSetChanged();
                        if (selectedBrands.size() == 0)
                            ((BrandSelectActivity) activity).setBtnEnable(false);
                    } else {
                        ((BrandSelectActivity) activity).getBrandAllList();
                    }
                } else {//실패
                    Toast.makeText(activity, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<Void>> call, Throwable t) {
                activity.closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
