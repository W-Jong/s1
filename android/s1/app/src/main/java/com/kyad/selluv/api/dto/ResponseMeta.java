package com.kyad.selluv.api.dto;

import java.util.HashMap;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class ResponseMeta {
    public static final ResponseMeta OK = new ResponseMeta(0, "OK");
    public static final ResponseMeta USER_NOT_EXISTS = new ResponseMeta(100, "아이디 또는 비밀번호가 일치하지 않습니다.");
    public static final ResponseMeta USER_ERROR_ACCESS_TOKEN = new ResponseMeta(101, "인증시간이 만료되었거나 다른 기기에서 로그인 되었습니다.");
    public static final ResponseMeta USER_EMAIL_DUPLICATED = new ResponseMeta(102, "이미 사용중인 이메일입니다.");
    public static final ResponseMeta USER_ID_DUPLICATED = new ResponseMeta(103, "이미 사용중인 아이디입니다.");
    public static final ResponseMeta USER_SNSID_DUPLICATED = new ResponseMeta(104, "이미 연결된 SNS 아이디입니다.");
    public static final ResponseMeta USER_PERSON_DUPLICATED = new ResponseMeta(105, "이미 가입되어 있는 회원입니다.");
    public static final ResponseMeta USER_INVITE_CODE_ERROR = new ResponseMeta(106, "잘못된 추천인 코드입니다.");
    public static final ResponseMeta USER_AGE_NOT_ALLOWED = new ResponseMeta(107, "미성년자는 회원가입할 수 없습니다.");
    public static final ResponseMeta USER_ERROR_OLD_PASSWORD = new ResponseMeta(108, "현재 비밀번호가 일치하지 않습니다.");
    public static final ResponseMeta USER_ID_NICKNAME_SPACE_NOT_ALLOWED = new ResponseMeta(109, "아이디 및 닉네임에는 공백을 사용할 수 없습니다.");
    public static final ResponseMeta USER_ID_NICKNAME_SPECIAL_CHAR_NOT_ALLOWED = new ResponseMeta(110, "아이디 및 닉네임에는 특수문자를 사용할 수 없습니다.");
    public static final ResponseMeta USER_EMAIL_NOT_CONNECTED = new ResponseMeta(120, "해당 이메일로 등록된 계정이 존재하지 않습니다.");
    public static final ResponseMeta USER_SIGNUP_WITH_FACEBOOK = new ResponseMeta(121, "해당 이메일로 등록된 계정은 페이스북으로 로그인되어 있습니다.");
    public static final ResponseMeta USER_SIGNUP_WITH_NAVER = new ResponseMeta(122, "해당 이메일로 등록된 계정은 네이버로 로그인되어 있습니다.");
    public static final ResponseMeta USER_SIGNUP_WITH_KAKAOTALK = new ResponseMeta(123, "해당 이메일로 등록된 계정은 카카오톡으로 로그인되어 있습니다.");
    public static final ResponseMeta USER_EMAIL_SENDING_ERROR = new ResponseMeta(124, "해당 이메일로 메일을 전송할 수 없습니다. 잘못된 메일주소일 수 있습니다.");
    public static final ResponseMeta USER_TEMP_NOT_PERMISSION = new ResponseMeta(150, "임시회원은 요청에 대한 권한이 없습니다.");
    public static final ResponseMeta PDT_NOT_EXIST = new ResponseMeta(200, "이미 삭제되었거나 존재하지 않는 상품입니다.");
    public static final ResponseMeta BRAND_NOT_EXIST = new ResponseMeta(201, "이미 삭제되었거나 존재하지 않는 브랜드입니다.");
    public static final ResponseMeta PDT_STYLE_NOT_EXIST = new ResponseMeta(202, "이미 삭제되었거나 존재하지 않는 상품스타일입니다.");
    public static final ResponseMeta CATEGORY_NOT_EXIST = new ResponseMeta(203, "이미 삭제되었거나 존재하지 않는 카테고리입니다.");
    public static final ResponseMeta THEME_NOT_EXIST = new ResponseMeta(204, "이미 삭제되었거나 존재하지 않는 테마입니다.");
    public static final ResponseMeta PROMOTION_NOT_EXIST = new ResponseMeta(205, "이미 삭제되었거나 존재하지 않는 프로모션코드입니다.");
    public static final ResponseMeta CARD_NOT_EXIST = new ResponseMeta(206, "이미 삭제되었거나 존재하지 않는 카드입니다.");
    public static final ResponseMeta CARD_NOT_VALID = new ResponseMeta(207, "유효하지 않은 않는 카드입니다.");
    public static final ResponseMeta DEAL_NOT_FOUND = new ResponseMeta(300, "이미 취소되었거나 존재하지 않는 거래입니다.");
    public static final ResponseMeta PAY_PRICE_OVER_ZERO = new ResponseMeta(301, "결제금액은 0원 이상이여야 합니다.");
    public static final ResponseMeta PAY_PRICE_RANGE_ERROR = new ResponseMeta(302, "허용범위안의 금액을 입력해주세요.");
    public static final ResponseMeta PAY_PDT_ALREADY_SOLD = new ResponseMeta(303, "이미 판매완료된 상품입니다.");
    public static final ResponseMeta PAY_PDT_NOT_NORMAL = new ResponseMeta(304, "현재 판매중인 상품이 아닙니다.");
    public static final ResponseMeta PAY_TIME_RANGE_ERROR = new ResponseMeta(305, "해당 조작의 허용시간을 초과했습니다.");
    public static final ResponseMeta PAY_AUTOPAYED_FAILED = new ResponseMeta(306, "카드자동결제가 실패했습니다. 카드상태를 확인해주세요.");
    public static final ResponseMeta PAY_CANCEL_FAILED = new ResponseMeta(307, "결제취소에 실패했습니다.");
    public static final ResponseMeta PAY_FAILED = new ResponseMeta(308, "결제에 실패했습니다.");
    public static final ResponseMeta PERMITTION_NOT_ALLOWED = new ResponseMeta(500, "요청에 대한 권한이 없습니다.");
    public static final ResponseMeta ALREADY_ADDED = new ResponseMeta(501, "이미 추가되어 있습니다.");
    public static final ResponseMeta ALREADY_DELETED = new ResponseMeta(502, "이미 삭제되었습니다.");
    public static final ResponseMeta PAYMENT_NOT_SUPPORT = new ResponseMeta(503, "지원되지 않는 결제형식입니다.");
    public static final ResponseMeta ALREADY_LIKE_ADDED = new ResponseMeta(504, "이미 좋아요 했습니다.");
    public static final ResponseMeta ALREADY_LIKE_DELETED = new ResponseMeta(505, "이미 좋아요를 취소했습니다.");
    public static final ResponseMeta ERROR_LIKE_GROUP_REQUIRED = new ResponseMeta(506, "관심상품군을 하나이상 선택해주세요.");
    public static final ResponseMeta PDT_ALREADY_SOLD = new ResponseMeta(507, "이미 판매완료된 상품은 수정할 수 없습니다.");
    public static final ResponseMeta CERTIFICATION_FAILED = new ResponseMeta(508, "본인인증에 실패했습니다.");
    public static final ResponseMeta INVALID_NICKNAME = new ResponseMeta(510, "입력하신 닉네임이 정확하지 않습니다.");
    public static final ResponseMeta PAGE_NOT_FOUND = new ResponseMeta(900, "잘못된 요청입니다.");
    public static final ResponseMeta INTENAL_SERVER_ERROR = new ResponseMeta(910, "서버오류입니다.");
    public static final ResponseMeta DATABASE_ERROR = new ResponseMeta(920, "데이터베이스 오류입니다.");
    public static final ResponseMeta FILE_UPLOAD_ERROR = new ResponseMeta(930, "파일 업로드에 실패했습니다.");
    private int errCode;
    private String errMsg;
    private Object data;

    public int getErrCode() {
        return this.errCode;
    }

    public String getErrMsg() {
        return this.errMsg;
    }

    public Object getData() {
        return data;
    }

    public ResponseMeta(int errCode, String errMsg, Object data) {
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.data = data;
    }

    public ResponseMeta(int errCode, String errMsg) {
        this(errCode, errMsg, new HashMap<String, Object>());
    }

    public ResponseMeta() {
    }

}
