package com.kyad.selluv.top;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrWishDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.home.WareListFragment;
import com.kyad.selluv.model.Model;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.LOAD_MORE_CNT;

public class TopLikeActivity extends BaseActivity {

    private boolean bStyle = false;
    private boolean bSale = false;

    //UI Reference
    @BindView(R.id.ib_right)
    Button ib_right;
    @BindView(R.id.rl_btn_sale)
    ImageButton rl_btn_sale;
    @BindView(R.id.tv_count)
    TextView tv_count;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.ib_right)
    void onStyle(View v) {

        reqPdtList(!bStyle, bSale ? 1 : 0);

        if (bStyle) {
            bStyle = false;
            ib_right.setBackground(getResources().getDrawable(R.drawable.selluv_main_wearing_off));
            if (bSale) {
                getPdtLikeList(1);
            } else {
                getPdtLikeList(0);
            }

        } else {
            bStyle = true;
            ib_right.setBackground(getResources().getDrawable(R.drawable.selluv_main_wearing_push));
            if (bSale) {
                getPdtStyleLikeList(1);
            } else {
                getPdtStyleLikeList(0);
            }
        }

    }

    @OnClick(R.id.rl_btn_sale)
    void onSale(View v) {
        if (bSale) {
            bSale = false;
            rl_btn_sale.setImageDrawable(getResources().getDrawable(R.drawable.top_btn_sale));
            if (bStyle) {
                getPdtStyleLikeList(0);
            } else {
                getPdtLikeList(0);
            }
        } else {
            bSale = true;
            rl_btn_sale.setImageDrawable(getResources().getDrawable(R.drawable.top_btn_sale_active));
            if (bStyle) {
                getPdtStyleLikeList(1);
            } else {
                getPdtLikeList(1);
            }
        }
        reqPdtList(bStyle ? true : false, bSale ? 1 : 0);
    }

    //Variables
    WareListFragment fg_ware_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_like);

        loadLayout();

    }

    private void loadLayout() {
        getPdtLikeList(0);
        fg_ware_list = new WareListFragment();
        reqPdtList(false, 0);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fg_ware_list, fg_ware_list);
        transaction.commit();
        fg_ware_list.setLoadInterface(null);
    }

    private void reqPdtList(boolean style, int sale) {

        fg_ware_list.setSale(sale);
        if (style) {
            fg_ware_list.setFragType(Constants.WIRE_FRAG_PDT_STYLE_LIKE);
        } else {
            fg_ware_list.setFragType(Constants.WIRE_FRAG_PDT_LIKE);
        }
        fg_ware_list.setLoadInterface("");
    }

    //get pdt like list
    private void getPdtLikeList(int sale) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<PdtListDto>>> genRes = restAPI.getLikeList(Globals.userToken, sale, 0);

        genRes.enqueue(new TokenCallback<Page<PdtListDto>>(this) {
            @Override
            public void onSuccess(Page<PdtListDto> response) {
                closeLoading();

                tv_count.setText("(" + response.getTotalElements() + ")");
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }

    //get pdt like style list
    private void getPdtStyleLikeList(int sale) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<PdtListDto>>> genRes = restAPI.getLikeStyleList(Globals.userToken, sale, 0);

        genRes.enqueue(new TokenCallback<Page<PdtListDto>>(this) {
            @Override
            public void onSuccess(Page<PdtListDto> response) {
                closeLoading();

                tv_count.setText("(" + response.getTotalElements() + ")");
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }
}
