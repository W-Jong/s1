package com.kyad.selluv.mypage;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.deal.DealListDto;
import com.kyad.selluv.api.request.DealReviewReq;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class FeedbackActivity extends BaseActivity {

    private String nickname = "";
    private String profile = "";
    private int dealUid = 0;
    private int mPoint = 2;

    //UI Reference
    @BindView(R.id.rl_good)
    RelativeLayout rl_good;
    @BindView(R.id.rl_normal)
    RelativeLayout rl_normal;
    @BindView(R.id.rl_bad)
    RelativeLayout rl_bad;
    @BindView(R.id.iv_good)
    ImageView iv_good;
    @BindView(R.id.iv_normal)
    ImageView iv_normal;
    @BindView(R.id.iv_bad)
    ImageView iv_bad;
    @BindView(R.id.tv_good)
    TextView tv_good;
    @BindView(R.id.tv_normal)
    TextView tv_normal;
    @BindView(R.id.tv_bad)
    TextView tv_bad;
    @BindView(R.id.edt_feedback)
    EditText edt_feedback;
    @BindView(R.id.btn_register)
    Button btn_register;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.iv_profile_img)
    CircularImageView iv_profile_img;


    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_register)
    void onLatter() {
        reqDealReview();
    }

    @OnClick(R.id.rl_good)
    void onGood() {
        rl_good.setBackgroundResource(R.drawable.mypage_evaluation_box_good);
        iv_good.setBackgroundResource(R.drawable.mypage_evaluation_ic_good_on);
        tv_good.setTextColor(getResources().getColor(R.color.color_42c2fe));
        rl_normal.setBackgroundColor(Color.WHITE);
        iv_normal.setBackgroundResource(R.drawable.mypage_evaluation_ic_normal_off);
        tv_normal.setTextColor(getResources().getColor(R.color.color_666666));
        rl_bad.setBackgroundColor(Color.WHITE);
        iv_bad.setBackgroundResource(R.drawable.mypage_evaluation_ic_bad_off);
        tv_bad.setTextColor(getResources().getColor(R.color.color_666666));
        mPoint = 2;
    }

    @OnClick(R.id.rl_normal)
    void onNormal() {
        rl_good.setBackgroundColor(Color.WHITE);
        iv_good.setBackgroundResource(R.drawable.mypage_evaluation_ic_good_off);
        tv_good.setTextColor(getResources().getColor(R.color.color_666666));
        rl_normal.setBackgroundResource(R.drawable.mypage_evaluation_box_normal);
        iv_normal.setBackgroundResource(R.drawable.mypage_evaluation_ic_normal_on);
        tv_normal.setTextColor(getResources().getColor(R.color.color_fedc72));
        rl_bad.setBackgroundColor(Color.WHITE);
        iv_bad.setBackgroundResource(R.drawable.mypage_evaluation_ic_bad_off);
        tv_bad.setTextColor(getResources().getColor(R.color.color_666666));
        mPoint = 1;
    }

    @OnClick(R.id.rl_bad)
    void onBad() {
        rl_good.setBackgroundColor(Color.WHITE);
        iv_good.setBackgroundResource(R.drawable.mypage_evaluation_ic_good_off);
        tv_good.setTextColor(getResources().getColor(R.color.color_666666));
        rl_normal.setBackgroundColor(Color.WHITE);
        iv_normal.setBackgroundResource(R.drawable.mypage_evaluation_ic_normal_off);
        tv_normal.setTextColor(getResources().getColor(R.color.color_666666));
        rl_bad.setBackgroundResource(R.drawable.mypage_evaluation_box_bad);
        iv_bad.setBackgroundResource(R.drawable.mypage_evaluation_ic_bad_on);
        tv_bad.setTextColor(getResources().getColor(R.color.color_ff3b7e));
        mPoint = 0;
    }

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_feedback);
        setStatusBarWhite();

        nickname = getIntent().getStringExtra(Constants.NICKNAME);
        profile = getIntent().getStringExtra(Constants.PROFILE_IMG);
        dealUid = getIntent().getIntExtra(Constants.DEAL_UID, 0);

        loadLayout();

    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        edt_feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edt_feedback.getText().length() >= 10)
                    btn_register.setEnabled(true);
                else {
                    btn_register.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_name.setText(nickname);
        ImageUtils.load(iv_profile_img.getContext(), profile, R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile_img);
    }

    //거래후기 작성
    private void reqDealReview() {

        String dealContent = edt_feedback.getText().toString();
        if (dealContent.length() < 10) {
            Utils.showToast(FeedbackActivity.this, getString(R.string.worong_writer_letter));
            return;
        }

        DealReviewReq dicInfo = new DealReviewReq();
        dicInfo.setContent(dealContent);
        dicInfo.setPoint(mPoint);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.reqDealReview(Globals.userToken, dealUid, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {

                Utils.showToast(FeedbackActivity.this, getString(R.string.deal_review_write_success));
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }
}
