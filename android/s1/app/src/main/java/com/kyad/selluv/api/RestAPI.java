package com.kyad.selluv.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.kyad.selluv.api.dao.DeliveryHistoryDao;
import com.kyad.selluv.api.dao.FaqDao;
import com.kyad.selluv.api.dao.LicenseDao;
import com.kyad.selluv.api.dao.NoticeDao;
import com.kyad.selluv.api.dao.QnaDao;
import com.kyad.selluv.api.dao.UsrCardDao;
import com.kyad.selluv.api.dao.UsrPushSettingDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandDetailDto;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.api.dto.category.CategoryDetailDto;
import com.kyad.selluv.api.dto.category.SizeRefDto;
import com.kyad.selluv.api.dto.deal.BuyHistoryDto;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.deal.DealDetailDto;
import com.kyad.selluv.api.dto.deal.DealPaymentDto;
import com.kyad.selluv.api.dto.deal.SellHistoryDto;
import com.kyad.selluv.api.dto.mypage.ReviewContentDto;
import com.kyad.selluv.api.dto.other.AlarmCountDto;
import com.kyad.selluv.api.dto.other.BannerDto;
import com.kyad.selluv.api.dto.other.DeliveryCompaniesDto;
import com.kyad.selluv.api.dto.other.PromotionCodeDao;
import com.kyad.selluv.api.dto.other.SystemSettingDto;
import com.kyad.selluv.api.dto.other.UsrAlarmDto;
import com.kyad.selluv.api.dto.other.UsrMoneyHisContentDto;
import com.kyad.selluv.api.dto.other.UsrWishDao;
import com.kyad.selluv.api.dto.other.UsrWishDto;
import com.kyad.selluv.api.dto.other.WorkingDayDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.pdt.PdtDetailDto;
import com.kyad.selluv.api.dto.pdt.PdtFakeInfoDto;
import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.api.dto.pdt.PdtMiniDto;
import com.kyad.selluv.api.dto.pdt.PdtStyleDao;
import com.kyad.selluv.api.dto.pdt.ReplyDto;
import com.kyad.selluv.api.dto.search.RecommendSearchDto;
import com.kyad.selluv.api.dto.search.SearchWordDto;
import com.kyad.selluv.api.dto.theme.ThemeListDto;
import com.kyad.selluv.api.dto.top.UsrRecommendDto;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.api.dto.usr.UsrAddressBookDto;
import com.kyad.selluv.api.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.api.dto.usr.UsrInfoDto;
import com.kyad.selluv.api.dto.usr.UsrInviteInfoDto;
import com.kyad.selluv.api.dto.usr.UsrLoginDto;
import com.kyad.selluv.api.dto.usr.UsrMiniDto;
import com.kyad.selluv.api.dto.usr.UsrMyInfoDto;
import com.kyad.selluv.api.dto.usr.UsrSnsStatusDto;
import com.kyad.selluv.api.request.ContentReq;
import com.kyad.selluv.api.request.DealBuyReq;
import com.kyad.selluv.api.request.DealDeliveryNumReq;
import com.kyad.selluv.api.request.DealDeliveryReq;
import com.kyad.selluv.api.request.DealNegoReq;
import com.kyad.selluv.api.request.DealRefundAddressReq;
import com.kyad.selluv.api.request.DealRefundReq;
import com.kyad.selluv.api.request.DealReviewReq;
import com.kyad.selluv.api.request.DealShareReq;
import com.kyad.selluv.api.request.MultiStringReq;
import com.kyad.selluv.api.request.MultiUidsReq;
import com.kyad.selluv.api.request.PdtCreateReq;
import com.kyad.selluv.api.request.PdtStyleMultiReq;
import com.kyad.selluv.api.request.PdtUpdateReq;
import com.kyad.selluv.api.request.PromotionReq;
import com.kyad.selluv.api.request.QnaReq;
import com.kyad.selluv.api.request.ReportReq;
import com.kyad.selluv.api.request.SearchReq;
import com.kyad.selluv.api.request.UserCardReq;
import com.kyad.selluv.api.request.UserLikeGroupUpdateReq;
import com.kyad.selluv.api.request.UserOtherInfoReq;
import com.kyad.selluv.api.request.UserPasswordUpdateReq;
import com.kyad.selluv.api.request.UserProfileReq;
import com.kyad.selluv.api.request.UserPushSettingReq;
import com.kyad.selluv.api.request.UserSellerSettingReq;
import com.kyad.selluv.api.request.UserSignupReq;
import com.kyad.selluv.api.request.UserSnsReq;
import com.kyad.selluv.api.request.UserWishReq;
import com.kyad.selluv.api.request.ValetCreateReq;
import com.kyad.selluv.api.request.WorkingDayReq;
import com.kyad.selluv.common.Constants;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by KSH on 3/7/2018.
 */

public interface RestAPI {

    Gson gson = new GsonBuilder()
            .registerTypeAdapter(Timestamp.class, new JsonDeserializer<Timestamp>() {
                @Override
                public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    return new Timestamp(json.getAsLong());
                }
            })
            .create();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.SELLUV_SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    @Headers({"Accept: application/json"})
    @PUT("api/user")
    Call<GenericResponse<UsrLoginDto>> registerUser(@Body UserSignupReq params);

    @Headers({"Accept: application/json"})
    @DELETE("api/user")
    Call<GenericResponse<Void>> deleteUser(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @POST("api/user/login")
    Call<GenericResponse<UsrLoginDto>> login(@Body RequestBody params);

    @Headers({"Accept: application/json"})
    @POST("api/user/password")
    Call<GenericResponse<Void>> changePass(@Header("accessToken") String accessToken, @Body UserPasswordUpdateReq promotionReq);

    @Headers({"Accept: application/json"})
    @POST("api/user/sellerSetting")
    Call<GenericResponse<Void>> sellerSetting(@Header("accessToken") String accessToken, @Body UserSellerSettingReq promotionReq);

    @Headers({"Accept: application/json"})
    @POST("api/user/friend/addressBook")
    Call<GenericResponse<UsrAddressBookDto>> getFriendAddressList(@Header("accessToken") String accessToken, @Body MultiStringReq promotionReq);

    @Headers({"Accept: application/json"})
    @POST("api/user/friend/facebook")
    Call<GenericResponse<List<UsrFollowListDto>>> getFriendFacebookList(@Header("accessToken") String accessToken, @Body MultiStringReq promotionReq);

    @Headers({"Accept: application/json"})
    @POST("api/user/profileImg")
    Call<GenericResponse<Void>> updateProfile(@Header("accessToken") String accessToken, @Body ContentReq promotionReq);

    @Headers({"Accept: application/json"})
    @POST("api/user/profile")
    Call<GenericResponse<Void>> updateUserInfo(@Header("accessToken") String accessToken, @Body UserProfileReq promotionReq);

    @Headers({"Accept: application/json"})
    @GET("api/user/snsStatus")
    Call<GenericResponse<UsrSnsStatusDto>> getSnsInfo(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/like")
    Call<GenericResponse<Page<PdtListDto>>> getLikeList(@Header("accessToken") String accessToken, @Query("sale") int sale, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/like/style")
    Call<GenericResponse<Page<PdtListDto>>> getLikeStyleList(@Header("accessToken") String accessToken, @Query("sale") int sale, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @POST("api/user/other")
    Call<GenericResponse<Void>> updateOtherUsrInfo(@Header("accessToken") String accessToken, @Body UserOtherInfoReq UsrOtherReq);

    @Headers({"Accept: application/json"})
    @GET("api/user/card")
    Call<GenericResponse<List<UsrCardDao>>> getCardList(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/user/block")
    Call<GenericResponse<Page<UsrMiniDto>>> getUsrBlockList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/user/history/buy")
    Call<GenericResponse<BuyHistoryDto>> getBuyHistoryList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/user/history/sell")
    Call<GenericResponse<SellHistoryDto>> getSaleHistoryList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @PUT("api/user/card/{usrCardUid}")
    Call<GenericResponse<Void>> selectCard(@Header("accessToken") String accessToken, @Path("usrCardUid") int usrCardUid);

    @Headers({"Accept: application/json"})
    @PUT("api/user/{usrUid}/hide")
    Call<GenericResponse<Void>> usrHide(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/user/card/{usrCardUid}")
    Call<GenericResponse<Void>> deleteCard(@Header("accessToken") String accessToken, @Path("usrCardUid") int usrCardUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/user/sns")
    Call<GenericResponse<Void>> deleteSns(@Header("accessToken") String accessToken, @Query("snsType") String snsType);

    @Headers({"Accept: application/json"})
    @PUT("api/user/sns")
    Call<GenericResponse<Void>> registerSns(@Header("accessToken") String accessToken, @Body UserSnsReq params);

    @Headers({"Accept: application/json"})
    @PUT("api/user/card")
    Call<GenericResponse<UsrCardDao>> registerCard(@Header("accessToken") String accessToken, @Body UserCardReq params);

    @Headers({"Accept: application/json"})
    @POST("api/user/deviceToken")
    Call<GenericResponse<Void>> updateDeviceToken(@Header("accessToken") String accessToken, @Query("deviceToken") String deviceToken);

    @Headers({"Accept: application/json"})
    @POST("api/user/accountInfo")
    Call<GenericResponse> updateAccountInfo(@Header("accessToken") String accessToken, @Body RequestBody params);

    @Headers({"Accept: application/json"})
    @GET("api/user/alarmSetting")
    Call<GenericResponse<UsrPushSettingDao>> getAlarmSettingInfo(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @POST("api/user/alarmSetting")
    Call<GenericResponse<Void>> reqAlarmSetting(@Header("accessToken") String accessToken, @Body UserPushSettingReq params);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}/review")
    Call<GenericResponse<ReviewContentDto>> getMannerPoint(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid, @Query("point") int point, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/alarm/count")
    Call<GenericResponse<AlarmCountDto>> getAlarmCnt(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/alarm")
    Call<GenericResponse<Page<UsrAlarmDto>>> getAlarmList(@Header("accessToken") String accessToken, @Query("kind") String kind, @Query("page") long page);

    @Headers({"Accept: application/json"})
    @GET("api/user/myInfo")
    Call<GenericResponse<UsrMyInfoDto>> getMyInfo(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/user/history/money")
    Call<GenericResponse<UsrMoneyHisContentDto>> getMoneyHistory(@Header("accessToken") String accessToken, @Query("page") long page);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}")
    Call<GenericResponse<UsrInfoDto>> usrDetail(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}/pdt")
    Call<GenericResponse<Page<PdtListDto>>> getUsrPdtList(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}/wish")
    Call<GenericResponse<Page<PdtListDto>>> getUsrWishList(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}/style")
    Call<GenericResponse<Page<PdtListDto>>> getUsrStyleList(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @POST("api/user/password/find")
    Call<GenericResponse> findPwd(@Body RequestBody params);

    @Headers({"Accept: application/json"})
    @POST("api/user/likeGroup")
    Call<GenericResponse> userLikeGroup(@Header("accessToken") String accessToken, @Body UserLikeGroupUpdateReq likeGroupUpdateReq);

    @Headers({"Accept: application/json"})
    @GET("api/user/check/usrId")
    Call<GenericResponse> checkUserId(@Query("usrId") String userid);

    @Headers({"Accept: application/json"})
    @GET("api/user/check/inviteCode")
    Call<GenericResponse> checkInviteCode(@Query("inviteCode") String userid);

    @Headers({"Accept: application/json"})
    @GET("api/user/check/usrMail")
    Call<GenericResponse> checkUserMail(@Query("usrMail") String userid);

    @Headers({"Accept: application/json"})
    @GET("api/user/myInfo")
    Call<GenericResponse<UsrMyInfoDto>> myInfo(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/user/inviteInfo")
    Call<GenericResponse<UsrInviteInfoDto>> inviteInfo(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @PUT("api/user/{usrUid}/block")
    Call<GenericResponse<Void>> usrBlock(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/user/{usrUid}/block")
    Call<GenericResponse<Void>> usrUnBlock(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid);

    @Headers({"Accept: application/json"})
    @PUT("api/user/{usrUid}/follow")
    Call<GenericResponse<Void>> followUsr(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/user/{usrUid}/follow")
    Call<GenericResponse<Void>> unFollowUsr(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}/follower")
    Call<GenericResponse<Page<UsrFollowListDto>>> getFollowerList(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/user/{usrUid}/following")
    Call<GenericResponse<Page<UsrFollowListDto>>> getFollowingList(@Header("accessToken") String accessToken, @Path("usrUid") int usrUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/brand/fame/all")
    Call<GenericResponse<List<BrandListDto>>> brandFameAll(@Header("accessToken") String accessToken, @Query("pdtGroupType") String pdtGroupType);

    @Headers({"Accept: application/json"})
    @GET("api/brand")
    Call<GenericResponse<List<BrandMiniDto>>> getBrand(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @PUT("api/other/qna")
    Call<GenericResponse<Void>> reqQna(@Header("accessToken") String accessToken, @Body QnaReq promotionReq);

    @Headers({"Accept: application/json"})
    @GET("api/other/qna")
    Call<GenericResponse<List<QnaDao>>> getQnaList(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/other/setting")
    Call<GenericResponse<SystemSettingDto>> systemSetting(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @POST("api/other/delayWorkingDay")
    Call<GenericResponse<WorkingDayDto>> getWorkingDay(@Header("accessToken") String accessToken, @Body WorkingDayReq workingDayReq);

    @Headers({"Accept: application/json"})
    @GET("api/other/license")
    Call<GenericResponse<LicenseDao>> otherLicense(@Header("accessToken") String accessToken, @Query("licenseType") String licenseType);

    @Headers({"Accept: application/json"})
    @GET("api/other/deliveryCompanies")
    Call<GenericResponse<DeliveryCompaniesDto>> getDeliveryCompanies(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/other/faq")
    Call<GenericResponse<List<FaqDao>>> otherFaq(@Header("accessToken") String accessToken, @Query("faqType") String faqType);

    @Headers({"Accept: application/json"})
    @PUT("api/other/usrWish")
    Call<GenericResponse<UsrWishDao>> insertUserWish(@Header("accessToken") String accessToken, @Body UserWishReq userWishReq);

    @Headers({"Accept: application/json"})
    @PUT("api/other/report")
    Call<GenericResponse<Void>> reportOther(@Header("accessToken") String accessToken, @Body ReportReq reportReq);

    @Headers({"Accept: application/json"})
    @GET("api/other/event/{eventUid}")
    Call<GenericResponse<EventDao>> getEventDetail(@Header("accessToken") String accessToken, @Path("eventUid") int eventUid);

    @Headers({"Accept: application/json"})
    @GET("api/other/event")
    Call<GenericResponse<List<EventDao>>> getEventList(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/user/recommend")
    Call<GenericResponse<Page<UsrRecommendDto>>> getRecommendUsrList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/other/notice")
    Call<GenericResponse<List<NoticeDao>>> getNoticeList(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/other/event/{eventUid}")
    Call<GenericResponse<NoticeDao>> getNoticeDetail(@Header("accessToken") String accessToken, @Path("noticeUid") int noticeUid);

    @Headers({"Accept: application/json"})
    @GET("api/category/{categoryUid}/sub")
    Call<GenericResponse<List<CategoryDao>>> getSubCategories(@Header("accessToken") String accessToken, @Path("categoryUid") int categoryUid);

    @Headers({"Accept: application/json"})
    @PUT("api/brand/{brandUid}/like")
    Call<GenericResponse<Void>> likeBrand(@Header("accessToken") String accessToken, @Path("brandUid") int brandUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/brand/{brandUid}/like")
    Call<GenericResponse<Void>> unLikeBrand(@Header("accessToken") String accessToken, @Path("brandUid") int brandUid);

    @Headers({"Accept: application/json"})
    @GET("api/home/fame")
    Call<GenericResponse<Page<PdtListDto>>> homeFameList(@Header("accessToken") String accessToken, @Query("page") int page, @Query("seed") long seed);

    @Headers({"Accept: application/json"})
    @GET("api/home/fame/style")
    Call<GenericResponse<Page<PdtListDto>>> homeFameStyleList(@Header("accessToken") String accessToken, @Query("page") int page, @Query("seed") long seed);

    @Headers({"Accept: application/json"})
    @GET("api/home/feed")
    Call<GenericResponse<Page<UsrFeedPdtListDto>>> homeFeedList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/home/feed/style")
    Call<GenericResponse<Page<UsrFeedPdtListDto>>> homeFeedStyleList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/home/new")
    Call<GenericResponse<Page<PdtListDto>>> homeNewList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/home/new/style")
    Call<GenericResponse<Page<PdtListDto>>> homeNewStyleList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @PUT("api/pdt/{pdtUid}/like")
    Call<GenericResponse<Void>> likePdt(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/pdt/{pdtUid}/like")
    Call<GenericResponse<Void>> unLikePdt(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid);

    @Headers({"Accept: application/json"})
    @GET("api/pdtStyle/{pdtStyleUid}")
    Call<GenericResponse<List<PdtStyleDao>>> pdtStyleDetail(@Header("accessToken") String accessToken, @Path("pdtStyleUid") int pdtStyleUid);

    @Headers({"Accept: application/json"})
    @GET("api/other/banner")
    Call<GenericResponse<List<BannerDto>>> bannerList(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/other/usrWish")
    Call<GenericResponse<Page<UsrWishDto>>> usrWishList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @HTTP(method = "DELETE", path = "api/other/usrWishs", hasBody = true)
    Call<GenericResponse<Void>> deleteUsrWishs(@Header("accessToken") String accessToken, @Body MultiUidsReq params);

    @Headers({"Accept: application/json"})
    @GET("api/pdtStyle")
    Call<GenericResponse<Page<PdtListDto>>> pdtStyleList(@Header("accessToken") String accessToken, @Query("pdtGroup") String pdtGroup, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdtStyle/all")
    Call<GenericResponse<Page<PdtListDto>>> pdtStyleAllList(@Header("accessToken") String accessToken, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @DELETE("api/pdtStyle/{pdtStyleUid}")
    Call<GenericResponse<Void>> pdtStyleDelete(@Header("accessToken") String accessToken, @Path("pdtStyleUid") int pdtStyleUid);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/like/updateStatus")
    Call<GenericResponse<Boolean>> usrLikePdtStatus(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/category/{categoryUid}/detail")
    Call<GenericResponse<CategoryDetailDto>> categoryDetail(@Header("accessToken") String accessToken, @Path("categoryUid") int categoryUid);

    @Headers({"Accept: application/json"})
    @GET("api/category/{categoryUid}/pdt")
    Call<GenericResponse<Page<PdtListDto>>> categoryPdtList(@Header("accessToken") String accessToken, @Path("categoryUid") int categoryUid, @Query("page") int page, @Query("brandUid") int brandUid);

    @Headers({"Accept: application/json"})
    @GET("api/theme/{themeUid}")
    Call<GenericResponse<ThemeListDto>> themeDetail(@Header("accessToken") String accessToken, @Path("themeUid") int themeUid);

    @Headers({"Accept: application/json"})
    @GET("api/brand/recommended")
    Call<GenericResponse<List<BrandMiniDto>>> brandRecommendedList(@Header("accessToken") String accessToken, @Query("pdtGroup") String pdtGroup);

    @Headers({"Accept: application/json"})
    @GET("api/brand/like")
    Call<GenericResponse<List<BrandListDto>>> brandFollowList(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/brand/fame")
    Call<GenericResponse<List<BrandListDto>>> brandFameList(@Header("accessToken") String accessToken, @Query("pdtGroupType") String pdtGroupType);

    @Headers({"Accept: application/json"})
    @GET("api/brand/list")
    Call<GenericResponse<List<BrandListDto>>> brandAllDetailList(@Header("accessToken") String accessToken, @Query("column") int column);

    @Headers({"Accept: application/json"})
    @GET("api/brand/{brandUid}")
    Call<GenericResponse<BrandDetailDto>> brandDetail(@Header("accessToken") String accessToken, @Path("brandUid") int brandUid);

    @Headers({"Accept: application/json"})
    @PUT("api/brand/likes")
    Call<GenericResponse<Void>> brandLikes(@Header("accessToken") String accessToken, @Body MultiUidsReq multiUidsRequest);

    @Headers({"Accept: application/json"})
    @HTTP(method = "DELETE", path = "api/brand/likes", hasBody = true)
    Call<GenericResponse<Void>> brandUnLikes(@Header("accessToken") String accessToken, @Body MultiUidsReq multiUidsRequest);

    @Headers({"Accept: application/json"})
    @GET("api/brand/{brandUid}/list")
    Call<GenericResponse<Page<PdtListDto>>> brandPdtList(@Header("accessToken") String accessToken, @Path("brandUid") int brandUid, @Query("tab") String tab, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}")
    Call<GenericResponse<PdtDetailDto>> pdtDetail(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid);

    @Headers({"Accept: application/json"})
    @DELETE("api/pdt/{pdtUid}")
    Call<GenericResponse<Void>> pdtDelete(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/recommended")
    Call<GenericResponse<Page<PdtListDto>>> pdtRecommendedList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/relationStyle")
    Call<GenericResponse<Page<PdtListDto>>> pdtRelationStyleList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/sameModel")
    Call<GenericResponse<Page<PdtListDto>>> pdtRelationSameModelList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/sameTag")
    Call<GenericResponse<Page<PdtListDto>>> pdtRelationSameTagList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query(value = "tag", encoded = true) String tag, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/like")
    Call<GenericResponse<Page<UsrFollowListDto>>> pdtLikeUserList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/wish")
    Call<GenericResponse<Page<UsrFollowListDto>>> pdtWishUserList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @PUT("api/pdt/{pdtUid}/wish")
    Call<GenericResponse<Void>> putPdtWish(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/reply")
    Call<GenericResponse<Page<ReplyDto>>> getPdtReplyList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/reply/search")
    Call<GenericResponse<List<UsrMiniDto>>> getPdtReplyUserList(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Query("keyword") String keyword);

    @Headers({"Accept: application/json"})
    @PUT("api/pdt/{pdtUid}/reply")
    Call<GenericResponse<ReplyDto>> putPdtReply(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Body ContentReq contentReq);

    @Headers({"Accept: application/json"})
    @DELETE("api/pdt/reply/{replyUid}")
    Call<GenericResponse<Void>> deletePdtReply(@Header("accessToken") String accessToken, @Path("replyUid") int replyUid);

    @Headers({"Accept: application/json"})
    @GET("api/pdt/{pdtUid}/fake")
    Call<GenericResponse<PdtFakeInfoDto>> getPdtFakeInfo(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid);

    @Headers({"Accept: application/json"})
    @PUT("api/pdt/{pdtUid}/fake")
    Call<GenericResponse<Void>> repostPdtFake(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Body ContentReq contentReq);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/nego")
    Call<GenericResponse<DealDao>> dealNegoReq(@Header("accessToken") String accessToken, @Body DealNegoReq negoReq);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/{dealUid}/delivery")
    Call<GenericResponse<DeliveryHistoryDao>> reqDealDelivery(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Body DealDeliveryReq params);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/refund/allow")
    Call<GenericResponse<Void>> reqRefundAllow(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Body DealRefundAddressReq arams);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/{dealUid}/share")
    Call<GenericResponse<Void>> reqDealShare(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Body DealShareReq arams);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/{dealUid}/deliveryNumber")
    Call<GenericResponse<DealDao>> reqDealDeliveryNum(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Body DealDeliveryNumReq reportReq);

    @Headers({"Accept: application/json"})
    @DELETE("api/deal/{dealUid}/delivery")
    Call<GenericResponse<Void>> delDealDelivery(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @GET("api/deal/{dealUid}/delivery")
    Call<GenericResponse<DeliveryHistoryDao>> getDealDeliveryInfo(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/nego/allow/counter")
    Call<GenericResponse<DealDao>> reqDealNegoAllowCounter(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/nego/refuse/counter")
    Call<GenericResponse<Void>> reqDealNegoRefuseCounter(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/complete")
    Call<GenericResponse<DealDao>> reqDealComplete(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/cancel")
    Call<GenericResponse<DealDao>> reqDealCancel(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/nego/refuse")
    Call<GenericResponse<Void>> reqDealNegoRefuse(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/nego/allow")
    Call<GenericResponse<DealDao>> reqDealNegoAllow(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/cancel/seller")
    Call<GenericResponse<DealDao>> reqDealCancelSeller(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/refund/complete")
    Call<GenericResponse<DealDao>> reqRefundComplete(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @POST("api/deal/{dealUid}/refund/disallow")
    Call<GenericResponse<DealDao>> reqRefundDisallow(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/{dealUid}/review")
    Call<GenericResponse<Void>> reqDealReview(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Body DealReviewReq buyReq);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/{dealUid}/nego/counter")
    Call<GenericResponse<Void>> reqDealNegoCounter(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Query("reqPrice") long reqPrice);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/{dealUid}/refund")
    Call<GenericResponse<Void>> reqDealRefund(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid, @Body DealRefundReq buyReq);

    @Headers({"Accept: application/json"})
    @PUT("api/deal/buy")
    Call<GenericResponse<DealPaymentDto>> dealBuyReq(@Header("accessToken") String accessToken, @Body DealBuyReq buyReq);

    @Headers({"Accept: application/json"})
    @GET("api/deal/{dealUid}")
    Call<GenericResponse<DealDetailDto>> getDealDetail(@Header("accessToken") String accessToken, @Path("dealUid") int dealUid);

    @Headers({"Accept: application/json"})
    @GET("api/theme/recommended")
    Call<GenericResponse<List<ThemeListDto>>> themeRecommendedList(@Header("accessToken") String accessToken, @Query("pdtGroup") String pdtGroup);

    @Headers({"Accept: application/json"})
    @GET("api/search/tag")
    Call<GenericResponse<List<String>>> searchTag(@Header("accessToken") String accessToken, @Query("keyword") String keyword);

    @Headers({"Accept: application/json"})
    @PUT("api/brand")
    Call<GenericResponse<BrandMiniDto>> regNewBrand(@Header("accessToken") String accessToken, @Query("brandName") String brandName);

    @Headers({"Accept: application/json"})
    @GET("api/brand/{brandUid}/modelNames")
    Call<GenericResponse<List<String>>> brandModelNames(@Header("accessToken") String accessToken, @Path("brandUid") int brandUid);

    @Headers({"Accept: application/json"})
    @POST("api/other/promotion")
    Call<GenericResponse<PromotionCodeDao>> getPromotionInfo(@Header("accessToken") String accessToken, @Body PromotionReq promotionReq);

    @Headers({"Accept: application/json"})
    @GET("api/category/{categoryUid}/size")
    Call<GenericResponse<SizeRefDto>> getCategorySize(@Header("accessToken") String accessToken, @Path("categoryUid") int categoryUid);

    @Headers({"Accept: application/json"})
    @PUT("api/pdt")
    Call<GenericResponse<PdtDetailDto>> registerPdt(@Header("accessToken") String accessToken, @Body PdtCreateReq pdtCreateReq);

    @Headers({"Accept: application/json"})
    @PUT("api/other/valet")
    Call<GenericResponse<Void>> registerValet(@Header("accessToken") String accessToken, @Body ValetCreateReq createReq);

    @Multipart
    @POST("api/common/uploads")
    Call<GenericResponse<List<HashMap<String, String>>>> uploadImages(@Part MultipartBody.Part[] uploadfile);

    @Multipart
    @POST("api/common/upload")
    Call<GenericResponse<HashMap<String, String>>> uploadImage(@Part MultipartBody.Part uploadfile);

    @Headers({"Accept: application/json"})
    @POST("api/pdt/{pdtUid}")
    Call<GenericResponse<PdtMiniDto>> editPdt(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Body PdtUpdateReq pdtUpdateReq);

    @Headers({"Accept: application/json"})
    @PUT("api/pdtStyle/{pdtUid}")
    Call<GenericResponse<Void>> registerStyles(@Header("accessToken") String accessToken, @Path("pdtUid") int pdtUid, @Body PdtStyleMultiReq pdtStyleMultiReq);

    @Headers({"Accept: application/json"})
    @GET("api/search/fame")
    Call<GenericResponse<List<SearchWordDto>>> searchFame(@Header("accessToken") String accessToken);

    @Headers({"Accept: application/json"})
    @GET("api/search")
    Call<GenericResponse<Page<PdtListDto>>> searchPdt(@Header("accessToken") String accessToken, @Query("keyword") String keyword, @Query("page") int page);

    @Headers({"Accept: application/json"})
    @GET("api/search/recommend")
    Call<GenericResponse<RecommendSearchDto>> searchRecommend(@Header("accessToken") String accessToken, @Query("keyword") String keyword);

    @Headers({"Accept: application/json"})
    @POST("api/search/detail")
    Call<GenericResponse<Page<PdtListDto>>> searchDetail(@Header("accessToken") String accessToken, @Body SearchReq searchReq, @Query("page") int page);

}

