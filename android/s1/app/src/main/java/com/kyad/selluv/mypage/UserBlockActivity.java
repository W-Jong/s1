package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.BlockListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrAlarmDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.usr.UsrMiniDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserBlockActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lv_block_list)
    ListView lv_block_list;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    BlockListAdapter listAdapter;
    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_block);
        setStatusBarWhite();
        loadLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBlockUsrList();
    }

    private void loadLayout() {
        listAdapter = new BlockListAdapter(this);
        lv_block_list.setAdapter(listAdapter);
//        Collections.addAll(listAdapter.arrData, Constants.noticeItems);
//        listAdapter.notifyDataSetChanged();
        lv_block_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getBlockUsrList();
                }
            }
        });
    }

    //get user block list
    private void getBlockUsrList() {


        if (isLoading || isLast)
            return;

        isLoading = true;

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Page<UsrMiniDto>>> genRes = restAPI.getUsrBlockList(Globals.userToken, currentPage);

        genRes.enqueue(new TokenCallback<Page<UsrMiniDto>>(this) {
            @Override
            public void onSuccess(Page<UsrMiniDto> response) {
                closeLoading();

                isLoading = false;

                if (currentPage == 0) {
                    listAdapter.arrData.clear();
                }

                isLast = response.isLast();
                listAdapter.arrData.addAll(response.getContent());
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }
}
