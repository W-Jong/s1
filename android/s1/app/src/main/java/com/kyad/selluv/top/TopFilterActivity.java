package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.enumtype.SEARCH_ORDER_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.sell.SellBrandAddActivity;

import butterknife.BindView;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.WIRE_FRAG_SHOP;

public class TopFilterActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_sort)
    TextView tv_sort;
    @BindView(R.id.tv_category)
    TextView tv_category;
    @BindView(R.id.tv_brand)
    TextView tv_brand;
    @BindView(R.id.tv_size)
    TextView tv_size;
    @BindView(R.id.tv_condition)
    TextView tv_condition;
    @BindView(R.id.tv_price_from)
    TextView tv_price_from;
    @BindView(R.id.tv_price_to)
    TextView tv_price_to;
    @BindView(R.id.tv_price_min)
    TextView tv_price_min;
    @BindView(R.id.tv_price_max)
    TextView tv_price_max;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.ll_color)
    LinearLayout ll_color;
    @BindView(R.id.seekBar)
    CrystalRangeSeekbar seekBar;
    @BindView(R.id.rl_category)
    RelativeLayout rl_category;
    @BindView(R.id.rl_sort)
    RelativeLayout rly_Sort;

    private int fromWhere = 0;

    private static final int PRICE_MIN = 1000;
    private static final int PRICE_MAX = 10000000;
    //Vars
    private int wareMinPrice = PRICE_MIN, wareMaxPrice = PRICE_MAX;

    private int[] colorImageIDs = {R.drawable.filter_small_color_black,
            R.drawable.filter_small_color_gray,
            R.drawable.filter_small_color_white,
            R.drawable.filter_small_color_beige,
            R.drawable.filter_small_color_red,
            R.drawable.filter_small_color_pink,
            R.drawable.filter_small_color_purple,
            R.drawable.filter_small_color_blue,
            R.drawable.filter_small_color_green,
            R.drawable.filter_small_color_yellow,
            R.drawable.filter_small_color_orange,
            R.drawable.filter_small_color_brown,
            R.drawable.filter_small_color_gold,
            R.drawable.filter_small_color_silver,
            R.drawable.filter_small_color_multy};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);

        if (Globals.searchReq.getPdtPriceMin() != -1) {
            wareMinPrice = (int) Globals.searchReq.getPdtPriceMin();
        }
        if (Globals.searchReq.getPdtPriceMax() != -1) {
            wareMaxPrice = (int) Globals.searchReq.getPdtPriceMax();
        }

        fromWhere = getIntent().getIntExtra("page", 0);
        loadLayout();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5000 && resultCode == RESULT_OK) {
            finish();
        } else {
            updateFilterValue();
        }
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    /**
     * 정렬
     */
    @OnClick(R.id.rl_sort)
    void onSort() {
        if (fromWhere == Constants.WIRE_FRAG_SHOP)  //숍페이지에서 인기,신상,가격인하를 눌러왔다면
            return;

        Intent intent = new Intent(this, SortFilterActivity.class);
        startActivityForResult(intent, 5000);
    }

    /**
     * 카테고리
     */
    @OnClick(R.id.rl_category)
    void onCategory() {
        if (fromWhere == 1000)  //카테고리페지에서 왔다면
            return;
        if (fromWhere == Constants.WIRE_FRAG_SHOP)  //숍페이지에서 인기,신상,가격인하를 눌러왔다면
            return;

        Intent intent = new Intent(this, CategoryFilterActivity.class);
        startActivityForResult(intent, 5000);
    }

    /**
     * 브랜드
     */
    @OnClick(R.id.rl_brand)
    void onBrand() {
        Intent intent = new Intent(this, SellBrandAddActivity.class);
        intent.putExtra(Constants.IS_FROM_FILTER, true);
        startActivityForResult(intent, 5000);
    }

    /**
     * 사이즈
     */
    @OnClick(R.id.rl_size)
    void onSize() {
        if (Globals.searchReq.getCategoryUid() > 0) {
            Intent intent = new Intent(this, SizeFilterActivity.class);
            startActivityForResult(intent, 5000);
        } else {
            Toast.makeText(this, "카테고리를 선택해주세요.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 컨디션
     */
    @OnClick(R.id.rl_condition)
    void onCondition() {
        Intent intent = new Intent(this, ConditionFilterActivity.class);
        startActivityForResult(intent, 5000);
    }

    /**
     * 컬러
     */
    @OnClick(R.id.rl_color)
    void onColor() {
        Intent intent = new Intent(this, ColorFilterActivity.class);
        startActivityForResult(intent, 5000);
    }

    /**
     * 초기화
     */
    @OnClick(R.id.tv_init)
    void onInit() {

        tv_category.setText("");
        tv_brand.setText("");
        tv_size.setText("");
        tv_condition.setText("");

        wareMinPrice = PRICE_MIN;
        wareMaxPrice = PRICE_MAX;

        if (fromWhere == 1000) { //카테고리 페지에서 왔다면 카테고리 uid는 삭제하면 안되기때문에 다시 넣는다.
            int categoryUid = Globals.searchReq.getCategoryUid();
            String categoryName = Globals.categoryFilterName;

            Globals.initSearchFilters();

            tv_category.setText(categoryName);
            Globals.searchReq.setCategoryUid(categoryUid);

        } else if (fromWhere == Constants.WIRE_FRAG_SHOP) {
            SEARCH_ORDER_TYPE Sort = Globals.searchReq.getSearchOrderType();
            Globals.initSearchFilters();
            Globals.searchReq.setSearchOrderType(Sort);
            tv_category.setText(Globals.categoryFilterName);
            tv_sort.setText(Globals.getOrderLabel(Sort));
        } else {
            Globals.initSearchFilters();
        }
    }

    /**
     * 검색결과 보기
     */
    @OnClick(R.id.btn_ok)
    void onOk() {
        if (fromWhere == 1000) { //카테고리 페지에서 왔다면 카테고리 uid는 삭제하면 안되기때문에 다시 넣는다.
            sendBroadcast(new Intent(Constants.ACT_SEARCH_DETAIL));
        } else if (fromWhere == Constants.WIRE_FRAG_SHOP) {
            sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
        } else {  //상단 검색단추를 클릭하여 들어온 경우
            sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
        }
        finish();
    }


    private void loadLayout() {

        updateFilterValue();

        tv_price_from.setText(String.valueOf(wareMinPrice));
        tv_price_to.setText(String.valueOf(wareMaxPrice));
        tv_price_min.setText(String.valueOf(wareMinPrice));
        tv_price_max.setText(String.valueOf(wareMaxPrice));
        seekBar.setMinValue(wareMinPrice);
        seekBar.setMaxValue(wareMaxPrice);
        seekBar.setMinStartValue(wareMinPrice);
        seekBar.setMaxStartValue(wareMaxPrice);
        seekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tv_price_from.setText(String.valueOf(minValue));
                tv_price_to.setText(String.valueOf(maxValue));

                if (minValue.intValue() > PRICE_MIN)
                    Globals.searchReq.setPdtPriceMin(minValue.intValue());
                else
                    Globals.searchReq.setPdtPriceMin(-1);

                if (maxValue.intValue() < PRICE_MAX)
                    Globals.searchReq.setPdtPriceMax(maxValue.intValue());
                else
                    Globals.searchReq.setPdtPriceMax(-1);
            }
        });

        if (fromWhere == 1000) { //카테고리페지에서 왔다면
            rl_category.setBackgroundColor(getResources().getColor(R.color.color_f5f5f5_95));
        } else if (fromWhere == Constants.WIRE_FRAG_SHOP) {
            rl_category.setBackgroundColor(getResources().getColor(R.color.color_f5f5f5_95));
            rly_Sort.setBackgroundColor(getResources().getColor(R.color.color_f5f5f5_95));
        } else {
            rl_category.setBackgroundColor(getResources().getColor(R.color.color_ffffff));
        }
    }

    private void updateFilterValue() {
        //정렬
        tv_sort.setText(Globals.getOrderLabel(Globals.searchReq.getSearchOrderType()));
        //카테고리
        tv_category.setText(Globals.categoryFilterName);
        //사이즈
        StringBuilder size = new StringBuilder();
        for (int i = 0; i < Globals.searchReq.getPdtSizeList().size(); i++) {
            if (i > 0)
                size.append(',');
            size.append(Globals.searchReq.getPdtSizeList().get(i));
        }
        tv_size.setText(size.toString());
        //브랜드
        tv_brand.setText(Globals.brandFilterName);
        //컨디션
        StringBuilder condition = new StringBuilder();
        for (int i = 0; i < Globals.searchReq.getPdtConditionList().size(); i++) {
            if (i > 0)
                condition.append(",");
            condition.append(Constants.PDT_CONDITION_LABEL[Globals.searchReq.getPdtConditionList().get(i).getCode() - 1]);
        }
        tv_condition.setText(condition.toString());
        //컬러
        ll_color.removeAllViews();
        for (int j = 0; j < Globals.searchReq.getPdtColorList().size(); j++) {
            for (int i = 0; i < Constants.COLOR_NAMES.length; i++) {
                if (Globals.searchReq.getPdtColorList().get(j).equals(Constants.COLOR_NAMES[i])) {
                    ImageView iv_color = new ImageView(this);
                    iv_color.setBackgroundResource(colorImageIDs[i]);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMarginEnd(10);
                    iv_color.setLayoutParams(params);
                    ll_color.addView(iv_color);
                    break;
                }
            }
        }
    }

}
