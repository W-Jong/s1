package com.kyad.selluv.api.enumtype;

public enum PDT_STATUS_TYPE {
    DELETED(0), NORMAL(1), SOLD(2), STOP(3), SLEEP(4);

    private int code;

    private PDT_STATUS_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
