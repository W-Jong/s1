package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SizeFilterActivity extends BaseActivity {


    @BindView(R.id.lv_size)
    ListView lv_size;

    String[] arraySize = {"XXS", "XS", "S", "M", "L", "XL", "XXL"};
    boolean[] selectedSize = {false, false, false, false, false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_size);

        for (int i = 0; i < Globals.searchReq.getPdtSizeList().size(); i++) {
            for (int j = 0; j < arraySize.length; j++) {
                if (Globals.searchReq.getPdtSizeList().get(i).equals(arraySize[j])) {
                    selectedSize[j] = true;
                    break;
                }
            }
        }

        loadLayout();
    }

    /**
     * 검색결과 보기
     */
    @OnClick(R.id.btn_ok)
    void onOk() {
        sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
        setResult(RESULT_OK);
        finish();
    }

    /**
     * 초기화
     */
    @OnClick(R.id.tv_init)
    void onInit() {
        List<String> sizeList = Globals.searchReq.getPdtSizeList();
        sizeList.clear();
        for (int i = 0; i < selectedSize.length; i++) {
            selectedSize[i] = false;
        }
        SizeListAdapter sizeListAdapter = new SizeListAdapter(this);
        lv_size.setAdapter(sizeListAdapter);
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.rl_guide_btn)
    void onGuide() {
        if (Globals.categoryFilterPath.length() < 1)
            return;
        String[] s_idx = Globals.categoryFilterPath.split(":");

        Intent intent = new Intent(this, GuidImagePopup.class);
        int imgID;
        if (s_idx[s_idx.length - 1].equals("1")) {
            imgID = R.drawable.size_guide_man;
        } else if (s_idx[s_idx.length - 1].equals("2")) {
            imgID = R.drawable.size_guide_woman;
        } else {
            if (s_idx.length > 1 && s_idx[s_idx.length - 2].equals("1")) {
                imgID = R.drawable.size_guide_baby;
            } else if (s_idx.length > 1 && (s_idx[s_idx.length - 2].equals("4") || s_idx[s_idx.length - 2].equals("5"))) {
                imgID = R.drawable.size_guide_teen;
            } else {
                imgID = R.drawable.size_guide_kids;
            }
        }
        intent.putExtra(Constants.POPUP_IMAGE, imgID);
        startActivity(intent);
    }

    private void loadLayout() {
        SizeListAdapter sizeListAdapter = new SizeListAdapter(this);
        lv_size.setAdapter(sizeListAdapter);
    }


    private class SizeListAdapter extends BaseAdapter {

        LayoutInflater inflater;
        public int selectedPos = 0;
        BaseActivity activity;

        public SizeListAdapter(BaseActivity activity) {
            this.activity = activity;
            inflater = LayoutInflater.from(activity);
        }


        @Override
        public int getCount() {
            return arraySize.length;
        }

        @Override
        public Object getItem(int i) {
            return arraySize[i];
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = inflater.inflate(R.layout.item_search_category, viewGroup, false);
            TextView tv_name = view.findViewById(R.id.tv_category);
            TextView tv_cnt = view.findViewById(R.id.tv_ware_cnt);
            RelativeLayout rl_main = view.findViewById(R.id.rl_item_main);
            final ImageView iv_check = view.findViewById(R.id.iv_check);
            tv_name.setText(arraySize[i]);
            tv_cnt.setVisibility(View.INVISIBLE);
            if (selectedSize[i]) {
                iv_check.setVisibility(View.VISIBLE);
            } else {
                iv_check.setVisibility(View.INVISIBLE);
            }
            rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedSize[i] = !selectedSize[i];
                    List<String> sizeList = Globals.searchReq.getPdtSizeList();
                    if (selectedSize[i]) {
                        iv_check.setVisibility(View.VISIBLE);
                        sizeList.add(arraySize[i]);
                    } else {
                        iv_check.setVisibility(View.INVISIBLE);
                        sizeList.remove(arraySize[i]);
                    }

                }
            });
            return view;
        }


    }
}
