/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.DockAnimation;
import com.kyad.selluv.common.Utils;

import butterknife.BindView;
import butterknife.OnClick;


public class SellDirectPriceDetail extends BaseActivity {

    //UI Refs
    @BindView(R.id.tv_earn_price)
    TextView tv_earn_price;
    @BindView(R.id.tv_ware_price)
    TextView tv_ware_price;
    @BindView(R.id.tv_transport_price)
    TextView tv_transport_price;
    @BindView(R.id.tv_selluv_fee_rate)
    TextView tv_selluv_fee_rate;
    @BindView(R.id.tv_selluv_fee)
    TextView tv_selluv_fee;
    @BindView(R.id.tv_pay_fee_rate)
    TextView tv_pay_fee_rate;
    @BindView(R.id.tv_pay_fee)
    TextView tv_pay_fee;
    @BindView(R.id.tv_promotion_payback)
    TextView tv_promotion_payback;
    @BindView(R.id.tv_total_earn)
    TextView tv_total_earn;
    @BindView(R.id.tv_max_selluv_money)
    TextView tv_max_selluv_money;
    @BindView(R.id.tv_sns_reward)
    TextView tv_sns_reward;
    @BindView(R.id.tv_after_sale)
    TextView tv_after_sale;
    @BindView(R.id.tv_total_sulluv_money)
    TextView tv_total_sulluv_money;
    @BindView(R.id.rl_detail1)
    RelativeLayout rl_detail1;
    @BindView(R.id.rl_detail2)
    RelativeLayout rl_detail2;
    @BindView(R.id.btn_paymoney_collapse)
    ImageButton btn_paymoney_collapse;
    @BindView(R.id.btn_max_selluvmoney_collapse)
    ImageButton btn_max_selluvmoney_collapse;
    @BindView(R.id.rly_promotion)
    RelativeLayout rly_promotion;

    //Vars
    boolean isVisible1 = true;
    boolean isVisible2 = true;


    /**
     * 판매가격 X1000
     */
    public int ware_price = 50000;
    /**
     * 배송비
     */
    public int shipping_price = 0;
    /**
     * 프로모션 페이백
     */
    public int promotion_price = 0;
    /**
     * 셀럽이용료
     */
    public int selluv_fee = 0;
    /**
     * 결제수수료
     */
    public int payment_fee = 0;
    /**
     * 정산금액
     */
    int received_price = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sell_direct_earn_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        ware_price = intent.getIntExtra("ware_price", ware_price);
        shipping_price = intent.getIntExtra("shipping_price", shipping_price);
        promotion_price = intent.getIntExtra("promotion_price", promotion_price);
        selluv_fee = intent.getIntExtra("selluv_fee", selluv_fee);
        payment_fee = intent.getIntExtra("payment_fee", payment_fee);
        received_price = intent.getIntExtra("received_price", received_price);

        loadLayout();
    }

    @OnClick(R.id.ib_back)
    void onDone(View v) {
        finish();
    }

    @OnClick(R.id.rl_pay_money)
    void onPayMoney(View v) {
        showDetail1(isVisible1);
    }

    @OnClick(R.id.rl_max_selluvmoney)
    void onMax(View v) {
        showDetail2(isVisible2);
    }


    private void loadLayout() {
        tv_earn_price.setText(Utils.getAttachCommaFormat(received_price));
        tv_ware_price.setText("+" + Utils.getAttachCommaFormat(ware_price));
        tv_transport_price.setText("+" + Utils.getAttachCommaFormat(shipping_price));
        tv_selluv_fee_rate.setText("(" + String.valueOf(Constants.SELLUV_USE_FEE) + "%)");
        tv_selluv_fee.setText("-" + Utils.getAttachCommaFormat(selluv_fee));
        tv_pay_fee_rate.setText("(" + String.valueOf(Constants.PAYMENT_FEE) + "%)");
        tv_pay_fee.setText("-" + Utils.getAttachCommaFormat(payment_fee));
        tv_promotion_payback.setText("+" + Utils.getAttachCommaFormat(promotion_price));
        tv_total_earn.setText(Utils.getAttachCommaFormat(received_price));

        tv_max_selluv_money.setText(Utils.getAttachCommaFormat(15000));
        tv_sns_reward.setText(Utils.getAttachCommaFormat(10000));
        tv_after_sale.setText(Utils.getAttachCommaFormat(5000));
        tv_total_sulluv_money.setText(Utils.getAttachCommaFormat(15000));

        if (promotion_price == 0) {
            rly_promotion.setVisibility(View.GONE);
        } else {
            rly_promotion.setVisibility(View.VISIBLE);
        }

    }


    void showDetail1(boolean isVisible) {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_detail1, 300,
                DockAnimation.FRAME_LAYOUT);

        int from = !isVisible ? 0 : Utils.dpToPixel(this, 195);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(this, 195) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.start();

        isVisible1 = !isVisible1;

        btn_paymoney_collapse.animate().rotationBy(180).setDuration(400).start();
    }


    void showDetail2(boolean isVisible) {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_detail2, 300,
                DockAnimation.FRAME_LAYOUT);

        int from = !isVisible ? 0 : Utils.dpToPixel(this, 135);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(this, 135) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.start();

        isVisible2 = !isVisible2;
        btn_max_selluvmoney_collapse.animate().rotationBy(180).setDuration(400).start();
    }
}
