package com.kyad.selluv.login.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.MainActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants.Direction;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.login.PasswordResetActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.GUEST_TOKEN;
import static com.kyad.selluv.common.Constants.VIEW_BOUNCE_ANIM_DURATION;


/**
 * stroryboard 157p 6.0.0_login
 * stroryboard 161p 6.2.0_login_id
 */
public class LoginStartFragment extends LoginBaseFragment implements TextWatcher {

    //UI Reference
    @BindView(R.id.ib_back)
    ImageButton ib_back;
    @BindView(R.id.edt_id)
    EditText edt_id;
    @BindView(R.id.edt_pwd)
    EditText edt_pwd;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.tv_skip)
    TextView tv_skip;
    @BindView(R.id.tv_login)
    TextView tv_login;
    @BindView(R.id.keypad_space)
    View keypad_space;

    //Variables
    View rootView;
    public int childCnt = 0;
    int startViewIDs[] = {R.id.rl_facebook, R.id.rl_kakao, R.id.rl_naver, R.id.tv_login, R.id.tv_skip};
    int loginViewIDs[] = {R.id.ib_back, R.id.edt_id, R.id.edt_pwd, R.id.btn_login, R.id.tv_pwd_forget, R.id.tv_or, R.id.ll_sns};

    public int loginStartTabIndex = 0;// 0: login select, 1: id login

    public static LoginStartFragment newInstance(int prevChildCnt, Direction from) {
        LoginStartFragment fragment = new LoginStartFragment();
        Bundle args = new Bundle();
        args.putInt(PREV_CHILD_CNT, prevChildCnt);
        args.putInt(FROM_DIRECTION, from.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            childCnt = bundle.getInt(PREV_CHILD_CNT, 0);
            if (childCnt == loginViewIDs.length + 3) {
                loginStartTabIndex = 1;
            } else {
                loginStartTabIndex = 0;
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_start, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

    }

    void onKeyboardShow() {
        View v = rootView.findViewById(R.id.activity_main);
        if (v != null) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
            lp.topMargin = -Utils.dpToPixel(getActivity(), 60);
            v.setLayoutParams(lp);
        }
    }

    void onKeyboardHidden() {
        View v = rootView.findViewById(R.id.activity_main);
        if (v != null) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
            lp.topMargin = 0;
            v.setLayoutParams(lp);
        }
    }

    private void loadLayout() {

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                if (rootView.getRootView().getHeight() - (r.bottom - r.top) > Utils.dpToPixel(rootView.getContext(), 100)) { // if more than 100 pixels, its probably a keyboard...
                    onKeyboardShow();
                } else {
                    onKeyboardHidden();
                }
            }
        });

        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.activity_main));

        if (loginStartTabIndex == 0) {
            childCnt = startViewIDs.length + 3;
            for (int i = loginViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(loginViewIDs[i]);
                v.setClickable(false);
                v.setVisibility(View.GONE);
            }
            for (int i = 0; i < startViewIDs.length; i++) {
                View v = rootView.findViewById(startViewIDs[i]);
                v.setVisibility(View.VISIBLE);
                v.setClickable(true);
            }

        } else {
            childCnt = loginViewIDs.length + 3;
            for (int i = startViewIDs.length - 1; i >= 0; i--) {
                View v = rootView.findViewById(startViewIDs[i]);
                v.setClickable(false);
                v.setVisibility(View.GONE);
            }
            for (int i = 0; i < loginViewIDs.length; i++) {
                View v = rootView.findViewById(loginViewIDs[i]);
                v.setVisibility(View.VISIBLE);
                v.setClickable(true);
            }
        }

        edt_id.addTextChangedListener(this);

        edt_pwd.addTextChangedListener(this);
    }

    void toggleLogin(final boolean fromLogin) {
        if (fromLogin)
            loginStartTabIndex = 0;
        else
            loginStartTabIndex = 1;

        if (fromLogin) childCnt = startViewIDs.length + 3;
        else childCnt = loginViewIDs.length + 3;
        edt_id.clearFocus();
        edt_pwd.clearFocus();

        for (int i = 0; i < startViewIDs.length; i++) {
            View v = rootView.findViewById(startViewIDs[i]);
            if (fromLogin) {
                v.setVisibility(View.VISIBLE);
                v.setClickable(true);
            }

            Animation anim = AnimationUtils
                    .loadAnimation(Utils.getApp().getApplicationContext(), fromLogin ? R.anim.view_in_from_left : R.anim.view_out_to_left);
            anim.setFillAfter(true);
            anim.setStartOffset(i * VIEW_BOUNCE_ANIM_DURATION + (fromLogin ? 500 : 0));
            if (i == startViewIDs.length - 1)
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (!fromLogin) {
                            for (int i = startViewIDs.length - 1; i >= 0; i--) {
                                final View v = rootView.findViewById(startViewIDs[i]);
                                v.setClickable(false);
                                v.setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            v.startAnimation(anim);
        }

        for (int i = 0; i < loginViewIDs.length; i++) {
            View v = rootView.findViewById(loginViewIDs[i]);
            if (!fromLogin) {
                v.setVisibility(View.VISIBLE);
                v.setClickable(true);
            }
            Animation anim = AnimationUtils
                    .loadAnimation(Utils.getApp().getApplicationContext(), fromLogin ? R.anim.view_out_to_right : R.anim.view_in_from_right);
            anim.setStartOffset(i * VIEW_BOUNCE_ANIM_DURATION + (fromLogin ? 0 : 500));
            anim.setFillAfter(true);
            if (i == loginViewIDs.length - 1) {
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (fromLogin) {
                            for (int i = 0; i < loginViewIDs.length; i++) {
                                View v = rootView.findViewById(loginViewIDs[i]);
                                v.setClickable(false);
                                v.setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
            v.startAnimation(anim);
        }
    }

    @OnClick(R.id.btn_login)
    void onClickLoginBtn(View v) {

        ((LoginStartActivity) getActivity()).login(edt_id.getText().toString(), edt_pwd.getText().toString(), "NORMAL");

    }

    @OnClick(R.id.ib_back)
    public void onBackToStart(View v) {
        toggleLogin(true);
    }

    @OnClick(R.id.tv_login)
    void onLogin(View v) {
        toggleLogin(false);
    }

    @OnClick({R.id.rl_facebook, R.id.ib_facebook})
    void onFacebookStart(View v) {
        ((LoginStartActivity) getActivity()).facebookLogin();
    }

    @OnClick({R.id.rl_kakao, R.id.ib_kakao})
    void onKakaoStart(View v) {
        ((LoginStartActivity) getActivity()).kakaotalkLogin();
    }

    @OnClick({R.id.rl_naver, R.id.ib_naver})
    void onNaverStart(View v) {
        ((LoginStartActivity) getActivity()).naverLogin();
    }

    @OnClick(R.id.tv_skip)
    void onSkip(View v) {
        Globals.userToken = GUEST_TOKEN;
        startActivity(MainActivity.class, true, 0, 0);
    }

    @OnClick(R.id.tv_pwd_forget)
    void onForgetPassword(View v) {
        startActivity(PasswordResetActivity.class, false, 0, 0);
    }

    @OnClick(R.id.ib_email)
    void onEmail(View v) {
        ((LoginStartActivity) getActivity()).gotoEmailRegisterFragment(childCnt, Direction.BOTTOM, false);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!edt_id.getText().toString().isEmpty() && !edt_pwd.getText().toString().isEmpty())
            btn_login.setEnabled(true);
        else
            btn_login.setEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}