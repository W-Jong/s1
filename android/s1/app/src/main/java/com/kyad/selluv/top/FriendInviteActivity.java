package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.UsrInviteInfoDto;
import com.kyad.selluv.api.dto.usr.UsrMyInfoDto;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class FriendInviteActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_invite_code)
    TextView tv_invite_code;

    @OnClick(R.id.ib_left)
    void onBack(View v) {
        finish();
    }

    @OnClick(R.id.btn_invite_code_send)
    void onCodeSend(View v) {
        Intent i = new Intent(Intent.ACTION_SEND);

        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "초대");
        i.putExtra(Intent.EXTRA_TEXT, "추천코드 : " + tv_invite_code.getText());

        startActivity(Intent.createChooser(i, "SELLUV앱 공유"));
    }

    @OnClick(R.id.btn_kakao_send)
    void onKakaoSend(View v) {
        Intent i = new Intent(Intent.ACTION_SEND);

        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "초대");
        i.putExtra(Intent.EXTRA_TEXT, "추천코드 : " + tv_invite_code.getText());

        startActivity(Intent.createChooser(i, "SELLUV앱 공유"));
    }

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_invite);
        setStatusBarWhite();
        loadLayout();
    }

    private void loadLayout() {
        getInviteInfo();
    }

    //초대정보 얻기
    private void getInviteInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrInviteInfoDto>> genRes = restAPI.inviteInfo(Globals.userToken);

        genRes.enqueue(new TokenCallback<UsrInviteInfoDto>(this) {
            @Override
            public void onSuccess(UsrInviteInfoDto response) {
                closeLoading();

                tv_invite_code.setText(response.getInviteCode());
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }

}
