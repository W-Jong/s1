package com.kyad.selluv.home;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.preference.PrefConst;
import com.kyad.selluv.common.preference.Preference;
import com.kyad.selluv.login.BrandSelectActivity;
import com.kyad.selluv.top.TopFriendActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.DATA_FEED;
import static com.kyad.selluv.common.Constants.DATA_STYLE;
import static com.kyad.selluv.common.Constants.DATA_WARE;
import static com.kyad.selluv.common.Constants.TAB_BAR_HEIGHT;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_FAME;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_FEED;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_NEW;

public class HomeFragment extends BaseFragment {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.ib_right)
    ToggleButton tb_style;

    //Vars
    View rootView;

    private WareListFragment fragment_feed, fragment_popular, fragment_newest;
    private MainPageAdapter pagerAdapter;

    private int selectedTab = 1;    //  0: 피드, 1: 인기, 2: 신상

    @OnClick(R.id.ib_left)
    void onTopFriend(View v) {
        startActivity(TopFriendActivity.class, false, 0, 0);
    }

    @OnClick(R.id.ib_right)
    void onStyle(View v) {
        setLoadInterface();
        refresh();
//        fragment_feed.refresh();
//        fragment_newest.refresh();
//        fragment_popular.refresh();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        initFragment();
        updatePushToken();
//        setLoadInterface();
        loadLayout();
        return rootView;
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    private void loadLayout() {

        BarUtils.addMarginTopEqualStatusBarHeight(rootView.findViewById(R.id.appbar));
        rootView.findViewById(R.id.appbar).setAlpha(0.95f);

        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new MainPageAdapter(getChildFragmentManager());

        vp_main.setAdapter(pagerAdapter);

        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
                refresh();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        vp_main.setOffscreenPageLimit(3);

        tl_top_tab.getTabAt(1).select();
        setSelectedTabStyle(1);
    }

    private void initFragment() {
        if (fragment_feed == null) {
            fragment_feed = new WareListFragment();
            fragment_feed.setFragType(WIRE_FRAG_FEED);
            fragment_feed.setDataType(DATA_FEED);
            fragment_feed.isLazyLoading = true;
        }
        fragment_feed.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
        fragment_feed.setNonePage(R.drawable.main_none_ic_normal,
                getString(R.string.no_recommended_ware),
                getString(R.string.none_recommend_ware_info),
                getString(R.string.interest_brand_sel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO : goto 관심브랜드선택(6.1.1.1) 화면 팝업
                        startActivity(BrandSelectActivity.class, false, 0, 0);
                    }
                });

        if (fragment_popular == null) {
            fragment_popular = new WareListFragment();
            fragment_popular.isLazyLoading = true;
            fragment_popular.setFragType(WIRE_FRAG_FAME);
        }
        fragment_popular.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
        fragment_popular.setNonePage(R.drawable.main_none_ic_normal,
                getString(R.string.none_style_img),
                getString(R.string.none_style_img_info),
                null, null);

        if (fragment_newest == null) {
            fragment_newest = new WareListFragment();
            fragment_newest.isLazyLoading = true;
            fragment_newest.setFragType(WIRE_FRAG_NEW);
        }
        fragment_newest.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
        fragment_newest.setNonePage(R.drawable.main_none_ic_normal,
                getString(R.string.none_style_img),
                getString(R.string.none_style_img_info),
                null, null);
    }

    private void setLoadInterface() {
        fragment_feed.setDataType(tb_style.isChecked() ? DATA_STYLE : DATA_FEED);
        fragment_popular.setDataType(tb_style.isChecked() ? DATA_STYLE : DATA_WARE);
        fragment_newest.setDataType(tb_style.isChecked() ? DATA_STYLE : DATA_WARE);
//        fragment_feed.setLoadInterface(new WareListFragment.DataLoadInterface() {
//            @Override
//            public Model.WareItem[] loadData(int offset) {
//                return tb_style.isChecked() ? loadFeedStyleData(offset) : loadFeedWareData(offset);
//            }
//        });
//
//        fragment_popular.setLoadInterface(new WareListFragment.DataLoadInterface() {
//            @Override
//            public Model.WareItem[] loadData(int offset) {
//                return tb_style.isChecked() ? loadPopularStyleData(offset) : loadPopularWareData(offset);
//            }
//        });
//
//        fragment_newest.setLoadInterface(new WareListFragment.DataLoadInterface() {
//            @Override
//            public Model.WareItem[] loadData(int offset) {
//                return tb_style.isChecked() ? loadNewestStyleData(offset) : loadNewestWareData(offset);
//            }
//        });
    }

//    public Model.WareItem[] loadFeedWareData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_6);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadPopularWareData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_15);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadNewestWareData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.wareList_10);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadFeedStyleData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.styleList_0);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadPopularStyleData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.styleList_10);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }
//
//    public Model.WareItem[] loadNewestStyleData(int offset) {
//        ArrayList<Model.WareItem> wareItemArrayList = new ArrayList<Model.WareItem>();
//        Collections.addAll(wareItemArrayList, Constants.styleList_0);
//        if (offset >= wareItemArrayList.size())
//            return null;
//        int cnt = Math.min(LOAD_MORE_CNT, wareItemArrayList.size() - offset);
//        return (Model.WareItem[]) wareItemArrayList.subList(offset, offset + cnt).toArray(new Model.WareItem[cnt]);
//    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    public void refresh() {
        if (vp_main.getCurrentItem() == 0 && fragment_feed != null) {
            if (fragment_feed.getRecyclerView() != null)
                fragment_feed.getRecyclerView().scrollToPosition(0);
            fragment_feed.removeAll();
            fragment_feed.refresh();
        }

        if (vp_main.getCurrentItem() == 1 && fragment_popular != null) {
            if (fragment_popular.getRecyclerView() != null)
                fragment_popular.getRecyclerView().scrollToPosition(0);
            fragment_popular.removeAll();
            fragment_popular.refresh();
        }

        if (vp_main.getCurrentItem() == 2 && fragment_newest != null) {
            if (fragment_newest.getRecyclerView() != null)
                fragment_newest.getRecyclerView().scrollToPosition(0);
            fragment_newest.removeAll();
            fragment_newest.refresh();
        }
    }

    private class MainPageAdapter extends android.support.v4.app.FragmentStatePagerAdapter {

        public MainPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    if (fragment_feed != null)
                        frag = fragment_feed;
                    else initFragment();
                    break;
                case 1:
                    if (fragment_popular != null)
                        frag = fragment_popular;
                    else initFragment();
                    break;
                case 2:
                    if (fragment_newest != null)
                        frag = fragment_newest;
                    else initFragment();
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    //update push token
    private void updatePushToken() {

        String token = Preference.getInstance().getSharedPreference(getActivity(), PrefConst.PREFCONST_PUSH_TOKEN, "");

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.updateDeviceToken(Globals.userToken, token);
        genRes.enqueue(new TokenCallback<Void>(getActivity()) {
            @Override
            public void onSuccess(Void response) {

            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }
}
