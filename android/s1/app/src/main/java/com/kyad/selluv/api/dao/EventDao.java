package com.kyad.selluv.api.dao;

import java.sql.Timestamp;
import java.util.Objects;


public class EventDao {
    private int eventUid;
//    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
//    @ApiModelProperty("마이페이지 노출여부 1-노출, 0-노출안함")
    private int mypageYn;
//    @ApiModelProperty("타이틀")
    private String title;
//    @ApiModelProperty("썸네일이미지")
    private String profileImg;

//    @ApiModelProperty("상세이미지")
    private String detailImg;
    private int status;

//    @Column(name = "event_uid", nullable = false)
    public int getEventUid() {
        return eventUid;
    }

    public void setEventUid(int eventUid) {
        this.eventUid = eventUid;
    }

//    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

//    @Column(name = "mypage_yn", nullable = false)
    public int getMypageYn() {
        return mypageYn;
    }

    public void setMypageYn(int mypageYn) {
        this.mypageYn = mypageYn;
    }

//    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

//    @Column(name = "detail_img", nullable = false, length = 255)
    public String getDetailImg() {
        return detailImg;
    }

    public void setDetailImg(String detailImg) {
        this.detailImg = detailImg;
    }

//    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDao eventDao = (EventDao) o;
        return eventUid == eventDao.eventUid &&
                mypageYn == eventDao.mypageYn &&
                status == eventDao.status &&
                Objects.equals(regTime, eventDao.regTime) &&
                Objects.equals(title, eventDao.title) &&
                Objects.equals(profileImg, eventDao.profileImg) &&
                Objects.equals(detailImg, eventDao.detailImg);
    }

    @Override
    public int hashCode() {

        return Objects.hash(eventUid, regTime, mypageYn, title, profileImg, detailImg, status);
    }
}
