/**
 * 홈
 */
package com.kyad.selluv.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.ResponseMeta;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static com.kyad.selluv.common.Constants.ACT_ERROR_ACCESS_TOKEN;
import static com.kyad.selluv.common.Constants.ACT_NOT_PERMISSION;
import static com.kyad.selluv.common.Constants.female_category1;
import static com.kyad.selluv.common.Constants.female_category1_icon;
import static com.kyad.selluv.common.Constants.kids_category1;
import static com.kyad.selluv.common.Constants.kids_category1_icon;
import static com.kyad.selluv.common.Constants.male_category1;
import static com.kyad.selluv.common.Constants.male_category1_icon;
import static com.kyad.selluv.common.Constants.shopper_type_icon;


public class ShopCategorySelectFragment extends BaseFragment {

    //UI Refs
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_title)
    ImageView iv_title;
    @BindView(R.id.popup_category)
    LinearLayout popup_category;
    @BindView(R.id.ib_back)
    ImageButton ib_back;
    @BindView(R.id.ll_menu)
    LinearLayout ll_menu;
    @BindView(R.id.tv_menu_item)
    TextView tv_menu_item;

    //Vars
    public List<CategoryDao> subCategories = new ArrayList<>();
    private String[] categoryArray;
    View rootView;

    public static ShopCategorySelectFragment newInstance() {
        ShopCategorySelectFragment fragment = new ShopCategorySelectFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_select_shop, container, false);
        ButterKnife.bind(this, rootView);
        if (Globals.categoryDepth == 0) {
            refreshLayout();
        } else if (Globals.categoryDepth == 1) {
            reloadSubCategory2();
        } else {
            reloadSubCategory3();
        }

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, false, nextAnim);
    }


    @OnClick({R.id.btn_cancel, R.id.popup_category})
    void onCancel(View v) {
        ((ShopCategorySelectActivity) getActivity()).closeResultCancelActivity();
    }

    @OnClick(R.id.ib_back)
    public void onBack(View v) {
        Globals.categoryDepth--;
        if (Globals.categoryDepth < 0) {
            ((ShopCategorySelectActivity) getActivity()).closeResultCancelActivity();
        } else {
            reloadSubCategory2();
        }
    }

    private void reloadSubCategory2() {
        subCategories = Globals.subCategories2;
        refreshLayout();
    }

    private void reloadSubCategory3() {
        subCategories = Globals.subCategories3;
        refreshLayout();
    }

    private void refreshLayout() {

        if (ll_menu.getChildCount() > 1) {
            //0번째는 전체보기
            int childCount = ll_menu.getChildCount();
            for (int i = childCount - 1; i > 0; i--) {
                ll_menu.removeViewAt(i);
            }
        }
        if (Globals.categoryDepth == 0) {
            ib_back.setVisibility(View.GONE);
            tv_title.setText(Constants.shopper_type[Globals.currentShopperTypeIdx]);
        } else if (Globals.categoryDepth == 1) {
            ib_back.setVisibility(View.VISIBLE);
            tv_title.setText(Constants.SUBCATEGORY_1[Globals.currentShopperTypeIdx][Globals.categoryIndex[0] - 5 - Globals.currentShopperTypeIdx * 5]);
        } else {
            ib_back.setVisibility(View.VISIBLE);
            CategoryDao parent = Globals.subCategories2.get(Globals.categoryIndex[1] - 1);
            tv_title.setText(parent.getCategoryName());
        }
        iv_title.setImageResource(getCategoryIcon());

        categoryArray = male_category1;
        if (Globals.currentShopperTypeIdx == 0) {
            popup_category.setBackgroundColor(getResources().getColor(R.color.color_42c2fe_92));
            categoryArray = male_category1;
        } else if (Globals.currentShopperTypeIdx == 1) {
            popup_category.setBackgroundColor(getResources().getColor(R.color.color_ff3b7e_92));
            categoryArray = female_category1;
        } else if (Globals.currentShopperTypeIdx == 2) {
            popup_category.setBackgroundColor(getResources().getColor(R.color.color_3a0d7d_92));
            categoryArray = kids_category1;
        }

        int categoryCount = (Globals.categoryDepth == 0) ? categoryArray.length : subCategories.size() + 1;

        for (int i = 0; i < categoryCount; i++) {
            TextView tv = new TextView(getActivity());
            TextView template = tv_menu_item;
            if (i == 0) {
                tv.setText(R.string.show_all);
            } else {
                tv.setText((Globals.categoryDepth == 0) ? categoryArray[i] : subCategories.get(i - 1).getCategoryName());
            }
            tv.setLayoutParams(template.getLayoutParams());
            tv.setTextColor(template.getTextColors());
            tv.setTextSize(COMPLEX_UNIT_DIP, Utils.pixelToDp(getActivity(), (int) template.getTextSize()));
            tv.setTypeface(template.getTypeface(), template.getTypeface().getStyle());
            int dp60 = Utils.dpToPixel(getActivity(), 60);
            tv.setPadding(dp60, tv.getPaddingTop(), dp60, tv.getPaddingBottom());
            tv.setClickable(template.isClickable());
            final int category_index = i;
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Globals.categoryDepth == 0) {
                        if (category_index == 0) {
                            Globals.categoryIndex[Globals.categoryDepth] = (Globals.currentShopperTypeIdx != 2) ? Globals.currentShopperTypeIdx + 1 : 4;
                            ((ShopCategorySelectActivity) getActivity()).closeResultOkActivity();
                            Globals.gKisCategoryName = Globals.currentShopperTypeIdx == 0? "남성" : Globals.currentShopperTypeIdx == 1? "여성" : "키즈";
                        } else {
                            Globals.gKisCategoryName = categoryArray[category_index] ;
                            Globals.categoryIndex[Globals.categoryDepth] = 4 + category_index + Globals.currentShopperTypeIdx * 5;
                            getSubCategory2();
                        }
                    } else if (Globals.categoryDepth == 1) {
                        if (category_index == 0) {
//                            Globals.categoryIndex[0] = (Globals.currentShopperTypeIdx != 2) ? Globals.currentShopperTypeIdx + 1 : 4;
                            ((ShopCategorySelectActivity) getActivity()).closeResultOkActivity();
                        } else {
                            Globals.categoryDepth = 2;
                            Globals.gKisCategoryName = Globals.gKisCategoryName + " " + Globals.subCategories2.get(category_index - 1).getCategoryName();
                            Globals.categoryIndex[Globals.categoryDepth - 1] = subCategories.get(category_index - 1).getCategoryUid();
                            ((ShopCategorySelectActivity) getActivity()).closeResultOkActivity();
                        }
                    }
                }
            });
            ll_menu.addView(tv);
        }
    }

    public int getCategoryIcon() {
        if (Globals.categoryDepth == 0) {
            return shopper_type_icon[Globals.currentShopperTypeIdx];
        } else {
            int[] iconArray = Globals.currentShopperTypeIdx == 0 ? male_category1_icon : Globals.currentShopperTypeIdx == 1 ? female_category1_icon : kids_category1_icon;
            return iconArray[Globals.categoryIndex[0] - 5 - Globals.currentShopperTypeIdx * 5 + 1];
        }
    }

    private void getSubCategory2() {
        int idx = Globals.categoryIndex[Globals.categoryDepth] - 1;

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<CategoryDao>>> genRes = restAPI.getSubCategories(Globals.userToken, Globals.categoryIndex[Globals.categoryDepth]);
        genRes.enqueue(new Callback<GenericResponse<List<CategoryDao>>>() {

            @Override
            public void onResponse(Call<GenericResponse<List<CategoryDao>>> call, Response<GenericResponse<List<CategoryDao>>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    List<CategoryDao> categories = response.body().getData();
                    if (categories == null || categories.size() < 1) {
                        Globals.categoryIndex[Globals.categoryDepth] = (Globals.currentShopperTypeIdx != 2) ? Globals.currentShopperTypeIdx + 1 : 4;
                        ((ShopCategorySelectActivity) getActivity()).closeResultOkActivity();
                    } else {
                        Globals.categoryDepth = 1;
                        Globals.subCategories2 = categories;
                        reloadSubCategory2();
                    }
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_ERROR_ACCESS_TOKEN.getErrCode()) {
                    //token이 만료됬을때.. 혹은 다른 기기에서 로그인
                    getActivity().sendBroadcast(new Intent(ACT_ERROR_ACCESS_TOKEN));
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_TEMP_NOT_PERMISSION.getErrCode()) {
                    //임시회원인경우..
                    getActivity().sendBroadcast(new Intent(ACT_NOT_PERMISSION));
                } else {//실패
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<List<CategoryDao>>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}
