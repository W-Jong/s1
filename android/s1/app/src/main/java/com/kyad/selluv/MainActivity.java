package com.kyad.selluv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.ResponseMeta;
import com.kyad.selluv.api.dto.other.SystemSettingDto;
import com.kyad.selluv.api.dto.usr.UsrMyInfoDto;
import com.kyad.selluv.brand.BrandDetailActivity;
import com.kyad.selluv.brand.BrandMainFragment;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.preference.PrefConst;
import com.kyad.selluv.common.preference.Preference;
import com.kyad.selluv.detail.CommentActivity;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.home.HomeFragment;
import com.kyad.selluv.login.InterestSelectActivity;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.mypage.EventDetailActivity;
import com.kyad.selluv.mypage.MypageFragment;
import com.kyad.selluv.mypage.MypageMoneyActivity;
import com.kyad.selluv.mypage.NoticeDetailActivity;
import com.kyad.selluv.mypage.PurchaseDetailActivity;
import com.kyad.selluv.mypage.SalesDetailActivity;
import com.kyad.selluv.sell.SellMainActivity;
import com.kyad.selluv.shop.ShopMainFragment;
import com.kyad.selluv.user.UserActivity;
import com.kyad.selluv.user.UserFollowActivity;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.ShareManager.SM_AUTO_LOGIN;
import static com.kyad.selluv.common.Constants.ACT_ERROR_ACCESS_TOKEN;
import static com.kyad.selluv.common.Constants.ACT_NOT_PERMISSION;
import static com.kyad.selluv.common.Constants.ACT_USER_SIGNUP;
import static com.kyad.selluv.common.Constants.GUEST_TOKEN;
import static com.kyad.selluv.common.Constants.IS_FIRST;
import static com.kyad.selluv.common.Constants.REQ_RESULT_BRAND_ADD_ACTIVITY;
import static com.kyad.selluv.common.Constants.REQ_RESULT_BRAND_EDIT_ACTIVITY;
import static com.kyad.selluv.common.Constants.REQ_RESULT_INTEREST_ACTIVITY;
import static com.kyad.selluv.common.Constants.REQ_RESULT_SHOP_CATEGORY_SELECT_ACTIVITY;

public class MainActivity extends BaseActivity {

    private static MainActivity instance;
    private String pushType = "";
    private int targetUid = 0;

    public static MainActivity getInstance() {
        return instance;
    }

    //UI Reference

    //Variables
    public int curTab = -1;
    public int oldTab = -1;
    public int[] bottomBars = {R.id.ib_home, R.id.ib_shop, R.id.ib_sell, R.id.ib_brand, R.id.ib_mypage};
    public int[] iconOn = {R.drawable.icon_common_tab_home,
            R.drawable.icon_common_tab_shop,
            R.drawable.icon_common_tab_sell,
            R.drawable.icon_common_tab_brand,
            R.drawable.icon_common_tab_mypage
    };
    public int[] iconOff = {R.drawable.icon_common_tab_home_inactive,
            R.drawable.icon_common_tab_shop_inactive,
            R.drawable.icon_common_tab_sell,
            R.drawable.icon_common_tab_brand_inactive,
            R.drawable.icon_common_tab_mypage_inactive
    };


    HomeFragment homeFragment;
    ShopMainFragment shopMainFragment;
    BrandMainFragment brandMainFragment;
    MypageFragment mypageActivity;

    FragmentManager fm;
    FragmentTransaction transaction;

    boolean m_bFinish = false;
    // BACK key handler
    Handler m_hndBackKey = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0)
                m_bFinish = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        BarUtils.setStatusBarColor(this, Color.WHITE, 100);
        BarUtils.setStatusBarAlpha(this, 200);

        IntentFilter f = new IntentFilter();
        f.addAction(ACT_ERROR_ACCESS_TOKEN);
        f.addAction(ACT_NOT_PERMISSION);
        f.addAction(ACT_USER_SIGNUP);
        f.addAction(Constants.ACT_PUSH_DETAIL);
        registerReceiver(mBroadcastReceiver, new IntentFilter(f));
        instance = this;

        //로그인 설정(이후 자동로그인 됨)
        if (!Globals.userToken.equals(GUEST_TOKEN)) {
            ShareManager.manager().put(SM_AUTO_LOGIN, true);
        }

        loadLayout();

        if (!Globals.userToken.equals(GUEST_TOKEN)) {
            loadMyInfo();
        }

        loadSystemSetting();

        //push
        targetUid = Preference.getInstance().getSharedPreference(this, PrefConst.PREFCONST_PUSH_TARGET_UID, 0);
        pushType = Preference.getInstance().getSharedPreference(this, PrefConst.PREFCONST_PUSH_ALARM_TYPE, "");

        if (targetUid > 0 && !pushType.equals("")) {
            goDetailPage();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
//        instance = null;
    }

    @Override
    public void onBackPressed() {
        if (!m_bFinish) {
            m_bFinish = true;
            Utils.showToast(this, getString(R.string.app_finish_message));
            m_hndBackKey.sendEmptyMessageDelayed(0, 2000);
        } else {
            moveTaskToBack(true);
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_RESULT_INTEREST_ACTIVITY && resultCode == RESULT_OK) {
            if (curTab == 0 && homeFragment != null) {
                homeFragment.refresh();
            }
        } else if (requestCode == REQ_RESULT_SHOP_CATEGORY_SELECT_ACTIVITY && resultCode == RESULT_OK) {
            if (curTab == 1 && shopMainFragment != null)
                shopMainFragment.closeCategorySelectActivity();
        } else if ((requestCode == REQ_RESULT_BRAND_ADD_ACTIVITY || requestCode == REQ_RESULT_BRAND_EDIT_ACTIVITY) && resultCode == RESULT_OK) {
            if (curTab == 3 && brandMainFragment != null)
                brandMainFragment.refreshBrandFollowFragment();
        }
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACT_ERROR_ACCESS_TOKEN)) {
                ShareManager.manager().put(SM_AUTO_LOGIN, false);
                Toast.makeText(MainActivity.this, ResponseMeta.USER_ERROR_ACCESS_TOKEN.getErrMsg(), Toast.LENGTH_LONG).show();
                startActivity(LoginStartActivity.class, true, 0, 0);
            } else if (action.equals(ACT_NOT_PERMISSION)) {
                ShareManager.manager().put(SM_AUTO_LOGIN, false);
                Toast.makeText(MainActivity.this, ResponseMeta.USER_TEMP_NOT_PERMISSION.getErrMsg(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(MainActivity.this, LoginStartActivity.class);
                i.putExtra(IS_FIRST, false);
                startActivity(i);
            } else if (action.equals(ACT_USER_SIGNUP)) {
                ShareManager.manager().put(SM_AUTO_LOGIN, false);
                startActivity(LoginStartActivity.class, true, 0, 0);
            } else if (action.equals(Constants.ACT_PUSH_DETAIL)) {

                targetUid = intent.getIntExtra(PrefConst.PREFCONST_PUSH_TARGET_UID, 0);
                pushType = intent.getStringExtra(PrefConst.PREFCONST_PUSH_ALARM_TYPE);

                goDetailPage();
            }
        }
    };

    private void cmdCheckLoginCount() {
        int logincnt = DbManager.loadLoginCount(Globals.myInfo.getUsr().getUsrId());
        logincnt++;
        if (logincnt < 2) {
            DbManager.updateLoginCount(Globals.myInfo.getUsr().getUsrId(), logincnt);
        } else if (logincnt == 2) {
            DbManager.updateLoginCount(Globals.myInfo.getUsr().getUsrId(), 3);
            startInterestActivity();
        }
    }

    private void startInterestActivity() {
        Intent intent = new Intent(this, InterestSelectActivity.class);
        startActivityForResult(intent, Constants.REQ_RESULT_INTEREST_ACTIVITY);
    }

    private void loadMyInfo() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<UsrMyInfoDto>> genRes = restAPI.myInfo(Globals.userToken);
        genRes.enqueue(new Callback<GenericResponse<UsrMyInfoDto>>() {

            @Override
            public void onResponse(Call<GenericResponse<UsrMyInfoDto>> call, Response<GenericResponse<UsrMyInfoDto>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    Globals.myInfo = response.body().getData();
                    cmdCheckLoginCount();
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_ERROR_ACCESS_TOKEN.getErrCode()) {
                    //token이 만료됬을때.. 혹은 다른 기기에서 로그인
                    sendBroadcast(new Intent(ACT_ERROR_ACCESS_TOKEN));
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_TEMP_NOT_PERMISSION.getErrCode()) {
                    //임시회원인경우..
                    sendBroadcast(new Intent(ACT_NOT_PERMISSION));
                } else {//실패
                    Toast.makeText(MainActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<UsrMyInfoDto>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void loadSystemSetting() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        Call<GenericResponse<SystemSettingDto>> genRes = restAPI.systemSetting(Globals.userToken);
        genRes.enqueue(new TokenCallback<SystemSettingDto>(this) {
            @Override
            public void onSuccess(SystemSettingDto response) {
                Globals.systemSetting = response;
            }

            @Override
            public void onFailed(Throwable t) {
                Globals.systemSetting = new SystemSettingDto();
            }
        });
    }

    private void loadLayout() {
        findViewById(R.id.ll_bottom).setAlpha(0.95f);

        fm = getSupportFragmentManager();
        homeFragment = new HomeFragment();
        shopMainFragment = new ShopMainFragment();
        brandMainFragment = new BrandMainFragment();
        mypageActivity = new MypageFragment();

        gotoHome();
    }

    void gotoHome() {
        curTab = 0;
        updateIcon();
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, homeFragment).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    void gotoShop() {
        curTab = 1;
        updateIcon();
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, shopMainFragment).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    void gotoSell() {
        if (Globals.userToken.equals(GUEST_TOKEN)) {
            showLoginSuggestionPopup();
        } else {
            Intent intent = new Intent(this, SellMainActivity.class);
            startActivity(intent);
        }
    }

    void gotoBrand() {
        curTab = 3;
        updateIcon();
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, brandMainFragment).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    void gotoMypage() {

        if (Globals.userToken.equals(GUEST_TOKEN)) {
            showLoginSuggestionPopup();
            return;
        }

        curTab = 4;
        updateIcon();
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, mypageActivity).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    protected void updateIcon() {
        if (oldTab >= 0) {
            ((ImageButton) findViewById(bottomBars[oldTab])).setImageResource(iconOff[oldTab]);
        }
        if (curTab >= 0) {
            ((ImageButton) findViewById(bottomBars[curTab])).setImageResource(iconOn[curTab]);
            oldTab = curTab;
        }
    }

    public void gotoBrandPage(int tabIndex) {
        curTab = 3;
        updateIcon();
//        transaction = fm.beginTransaction();
//        brandMainFragment.toWhere = 1;
//        brandMainFragment.tabindex = tabIndex;
//        transaction.replace(R.id.fl_fragment, brandMainFragment);
//        transaction.addToBackStack(null);
//        transaction.commitAllowingStateLoss();

        Intent intent = new Intent(this, BrandDetailActivity.class);
        intent.putExtra(BrandDetailActivity.BRAND_UID, tabIndex);
        startActivity(intent);
    }

    private void goDetailPage() {

        int _uid = targetUid;
        String _type = pushType;
        targetUid = 0;
        pushType = "";

        Preference.getInstance().putSharedPreference(this, PrefConst.PREFCONST_PUSH_TARGET_UID, targetUid);
        Preference.getInstance().putSharedPreference(this, PrefConst.PREFCONST_PUSH_ALARM_TYPE, pushType);

        if (_type.equals(Constants.PUSH01_S_PAY_COMPLETED)) {

        } else if (_type.equals(Constants.PUSH02_B_DELIVERY_SENT)) {

        } else if (_type.equals(Constants.PUSH03_D_DEAL_COMPLETED)) {

        } else if (_type.equals(Constants.PUSH04_B_DEAL_CANCELED)) {
            goOrderDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH05_S_DEAL_CANCELED)) {
            goSellDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH06_B_PDT_VERIFICATION)) {
            goOrderDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH07_S_NEGO_SUGGEST)) {
            goSellDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH08_B_NEGO_UPDATED)) {
            goOrderDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH09_S_REFUND_REQUEST)) {
            goSellDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH10_B_REFUND_UPDATED)) {
            goOrderDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH11_REWARD_INVITE)) {
            Intent intent = new Intent(this, MypageMoneyActivity.class);
            intent.putExtra(UserActivity.USR_UID, _uid);
            startActivity(intent);
        } else if (_type.equals(Constants.PUSH12_REWARD_REPORT)) {
            Intent intent = new Intent(this, MypageMoneyActivity.class);
            intent.putExtra(UserActivity.USR_UID, _uid);
            startActivity(intent);
        } else if (_type.equals(Constants.PUSH13)) {

        } else if (_type.equals(Constants.PUSH14_FOLLOW_ME)) {
            goFollowListPage(_uid);
        } else if (_type.equals(Constants.PUSH15_LIKE_MY_PDT)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH16_ITEM_MY_PDT)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH17_LIKE_PDT_SALE)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH18_FOLLOWING_USER_PDT)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH19_FOLLOWING_USER_STYLE)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH20_FOLLOWING_BRAND_PDT)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH21_FINDING_PDT)) {
            goProductDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH22_EVENT)) {
            goEventDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH23_NOTICE)) {
            goNoticeDetailPage(_uid);
        } else if (_type.equals(Constants.PUSH24_PDT_REPLY)) {
            goCommentPage(_uid);
        } else if (_type.equals(Constants.PUSH25_REPLY_REPLY)) {
            goCommentPage(_uid);
        } else if (_type.equals(Constants.PUSH26_MENTIONED_ME)) {
            goCommentPage(_uid);
        }
    }

    private void goOrderDetailPage(int _uid) {
        Intent intent = new Intent(this, PurchaseDetailActivity.class);
        intent.putExtra(Constants.DEAL_UID, _uid);
        startActivity(intent);

    }

    private void goSellDetailPage(int _uid) {
        Intent intent = new Intent(this, SalesDetailActivity.class);
        intent.putExtra(UserActivity.USR_UID, _uid);
        startActivity(intent);
    }

    private void goFollowListPage(int _uid) {
        Intent intent = new Intent(this, UserFollowActivity.class);
        intent.putExtra(Constants.USER_UID, Globals.myInfo.getUsr().getUsrUid());
        intent.putExtra(Constants.FOLLOW_PAGE_TITLE, Globals.myInfo.getUsr().getUsrNckNm());
        intent.putExtra(Constants.FOLLOW_PAGE_IDX, 0);
        startActivity(intent);
    }

    private void goProductDetailPage(int _uid) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, _uid);
        startActivity(intent);
    }

    private void goCommentPage(int _uid) {
        Intent intent = new Intent(this, CommentActivity.class);
        intent.putExtra(CommentActivity.PDT_UID, _uid);
        startActivity(intent);
    }

    private void goNoticeDetailPage(int _uid) {
        Intent intent = new Intent(this, NoticeDetailActivity.class);
        intent.putExtra(Constants.NOTICE_UID, _uid);
        startActivity(intent);
    }

    private void goEventDetailPage(int _uid) {
        Intent intent = new Intent(this, EventDetailActivity.class);
        intent.putExtra(Constants.EVT_UID, _uid);
        startActivity(intent);
    }

    @OnClick(R.id.ib_home)
    void onHome(View v) {
        gotoHome();
    }

    @OnClick(R.id.ib_shop)
    void onShop(View v) {
        gotoShop();
    }

    @OnClick(R.id.ib_sell)
    void onSell(View v) {
        gotoSell();
    }

    @OnClick(R.id.ib_brand)
    void onBrand(View v) {
        gotoBrand();
    }

    @OnClick(R.id.ib_mypage)
    void onMyPage(View v) {
        gotoMypage();
    }
}
