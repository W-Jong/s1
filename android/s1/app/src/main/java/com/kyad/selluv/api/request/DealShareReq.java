package com.kyad.selluv.api.request;


import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class DealShareReq {
    //    @NonNull
    //@ApiModelProperty("인스타그램 공유이미지 리스트")
    private List<String> instagramPhotos;
    //    @NonNull
    //@ApiModelProperty("트위터 공유이미지 리스트")
    private List<String> twitterPhotos;
    //    @NonNull
    //@ApiModelProperty("네이버블로그 공유이미지 리스트")
    private List<String> naverblogPhotos;
    //    @NonNull
    //@ApiModelProperty("카카오스토리 공유이미지 리스트")
    private List<String> kakaostoryPhotos;
    //    @NonNull
    //@ApiModelProperty("페이스북 공유이미지 리스트")
    private List<String> facebookPhotos;
}
