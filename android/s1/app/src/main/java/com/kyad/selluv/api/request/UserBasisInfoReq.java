package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import lombok.Data;


@Data
public class UserBasisInfoReq {
    @Size(min = 1)
    //@ApiModelProperty("본인인증 실명")
    private String usrNm;
    //@ApiModelProperty("본인인증 생년월일 (형식:1990-01-01)")
    private String birthday;
    @IntRange(from = 1, to = 2)
    //@ApiModelProperty("본인인증 성별 1-남, 2-여")
    private int gender;
    @Size(min = 1)
    //@ApiModelProperty("본인인증 폰번호")
    private String usrPhone;
}
