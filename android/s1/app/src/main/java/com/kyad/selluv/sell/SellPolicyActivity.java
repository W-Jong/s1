/**
 * 홈
 */
package com.kyad.selluv.sell;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;

import butterknife.BindView;
import butterknife.OnClick;


public class SellPolicyActivity extends BaseActivity {

    //UI Refs
    @BindView(R.id.tv_chapter)
    TextView tv_chapter;
    @BindView(R.id.tv_content)
    TextView tv_content;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_pay_policy);

        loadLayout();
    }


    @OnClick(R.id.ib_left)
    void onDone(View v) {
        finish();
    }


    private void loadLayout() {
        tv_chapter.setText("셀럽 판매 및 환불 약관");
        tv_content.setText("이 약관에서는 고객들이 셀럽에서...");
    }
}
