package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.CardListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.UsrCardDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class CardManageActivity extends BaseActivity {

    public static final int CARD_SELECT_REQUEST = 0x0100;

    public static final String CARD_SELECTION_MODE = "CARD_SELECTION_MODE";

    //UI Reference
    @BindView(R.id.lv_card_list)
    ListView lv_card_list;
    @BindView(R.id.rl_none)
    RelativeLayout rl_none;
    @BindView(R.id.btn_register)
    Button btn_register;

    //Vars
    CardListAdapter listAdapter;
    private boolean isCardSelectionMode = false;


    @OnClick(R.id.ib_left)
    void onLeft() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.rl_none)
    void onNone() {
        rl_none.setVisibility(View.GONE);
        lv_card_list.setVisibility(View.VISIBLE);
        btn_register.setVisibility(View.VISIBLE);
        startActivity(CardRegisterActivity.class, false, 0, 0);
    }

    @OnClick(R.id.btn_register)
    void onRegister() {
        startActivity(CardRegisterActivity.class, false, 0, 0);
    }

    public void setNone() {
        rl_none.setVisibility(View.VISIBLE);
        lv_card_list.setVisibility(View.GONE);
        btn_register.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_card);
        setStatusBarWhite();

        isCardSelectionMode = getIntent().getBooleanExtra(CARD_SELECTION_MODE, false);

        loadLayout();
    }

    private void loadLayout() {
        listAdapter = new CardListAdapter(this);
        lv_card_list.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCardList();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        onLeft();
    }

    //get card list
    private void getCardList() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<UsrCardDao>>> genRes = restAPI.getCardList(Globals.userToken);

        genRes.enqueue(new TokenCallback<List<UsrCardDao>>(this) {
            @Override
            public void onSuccess(List<UsrCardDao> response) {
                closeLoading();

                listAdapter.arrData = response;

                if (listAdapter.arrData.size() == 0) {
                    Globals.selectedUsrCardDao = null;
                    setNone();
                } else {
                    listAdapter.nCurCardUid = listAdapter.arrData.get(0).getUsrCardUid();
                    Globals.selectedUsrCardDao = listAdapter.arrData.get(0);
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                Globals.selectedUsrCardDao = null;
            }
        });
    }

    //selece card
    public void selectCard(final UsrCardDao usrCard) {

        if (isCardSelectionMode) {
            Globals.selectedUsrCardDao = usrCard;
            setResult(RESULT_OK);
            finish();
        }

        if (listAdapter.nCurCardUid == usrCard.getUsrCardUid()) {
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.selectCard(Globals.userToken, usrCard.getUsrCardUid());

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                listAdapter.nCurCardUid = usrCard.getUsrCardUid();
                Globals.selectedUsrCardDao = usrCard;
                getCardList();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //delete card
    public void deleteCard(int usrCardUid) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.deleteCard(Globals.userToken, usrCardUid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(CardManageActivity.this, getString(R.string.card_del_success));
                getCardList();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
