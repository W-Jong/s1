package com.kyad.selluv.api.dto.other;

import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.api.dto.category.CategoryMiniDto;

import lombok.Data;

@Data
public class UsrWishDto {
    //("찾는 제품정보")
    private UsrWishDao usrWish;
    //("브랜드")
    private BrandMiniDto brand;
    //("카테고리")
    private CategoryMiniDto category;
}
