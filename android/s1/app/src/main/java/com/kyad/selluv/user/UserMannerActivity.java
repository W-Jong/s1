package com.kyad.selluv.user;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.mypage.ReviewContentDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.UserFragment;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static android.graphics.Typeface.BOLD;
import static android.graphics.Typeface.NORMAL;
import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class UserMannerActivity extends BaseActivity {

    private int currentPage = 0;
    private int pageIdx = -1;
    private int usrUid = 0;

    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;

    @BindView(R.id.btn_notice_count)
    Button btn_point;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    int selectedTab = 0, originalTab = 0;
    private PageAdapter pagerAdapter;
    UserFragment totalFragment, goodFragment, normalFragment, badFragment;
    int total, good, normal, bad;
    int[] offImgs = {R.drawable.review_good_icon, R.drawable.review_normal_icon, R.drawable.review_bad_icon};
    int[] onImgs = {R.drawable.review_good_icon_color, R.drawable.review_normal_icon_color, R.drawable.review_bad_icon_color};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_manner);

        usrUid = getIntent().getIntExtra(Constants.USER_UID, 0);

        initFragment();
        loadLayout();
        getMannerPoint(currentPage);
    }

    private void initFragment() {
        if (totalFragment == null) {
            totalFragment = new UserFragment();
        }

        if (goodFragment == null) {
            goodFragment = new UserFragment();
        }

        if (normalFragment == null) {
            normalFragment = new UserFragment();
        }

        if (badFragment == null) {
            badFragment = new UserFragment();
        }
    }

    private void selectPage() {
        if (selectedTab == 0) {
            totalFragment.setTab(0, usrUid);
        } else if (selectedTab == 1) {
            goodFragment.setTab(1, usrUid);
        } else if (selectedTab == 2) {
            normalFragment.setTab(2, usrUid);
        } else {
            badFragment.setTab(3, usrUid);
        }
        tl_top_tab.getTabAt(selectedTab).select();
    }

    void loadCnt() {

        tl_top_tab.getTabAt(0).setText(String.format(getResources().getString(R.string.total), total));

        View vGood = LayoutInflater.from(this).inflate(R.layout.item_manner_tab, null);
        ((ImageView) vGood.findViewById(R.id.icon)).setImageResource(R.drawable.review_good_icon);
        ((TextView) vGood.findViewById(R.id.tv_cnt)).setText(String.valueOf(good));
        tl_top_tab.getTabAt(1).setCustomView(vGood);

        View vNormal = LayoutInflater.from(this).inflate(R.layout.item_manner_tab, null);
        ((ImageView) vNormal.findViewById(R.id.icon)).setImageResource(R.drawable.review_normal_icon);
        ((TextView) vNormal.findViewById(R.id.tv_cnt)).setText(String.valueOf(normal));
        tl_top_tab.getTabAt(2).setCustomView(vNormal);

        View vBad = LayoutInflater.from(this).inflate(R.layout.item_manner_tab, null);
        ((ImageView) vBad.findViewById(R.id.icon)).setImageResource(R.drawable.review_bad_icon);
        ((TextView) vBad.findViewById(R.id.tv_cnt)).setText(String.valueOf(bad));
        tl_top_tab.getTabAt(3).setCustomView(vBad);
    }

    private void loadLayout() {

        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
//                tl_top_tab.getTabAt(position).select();
                selectPage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);

        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle();
    }


    private void setSelectedTabStyle() {
        if (originalTab == 0) {
            Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 12.5f, NORMAL);
        } else {
            ((ImageView) (tl_top_tab.getTabAt(originalTab).getCustomView().findViewById(R.id.icon))).setImageResource(offImgs[originalTab - 1]);
            ((TextView) (tl_top_tab.getTabAt(originalTab).getCustomView().findViewById(R.id.tv_cnt))).setTypeface(null, NORMAL);
        }
        if (selectedTab == 0) {
            Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 12.5f, BOLD);
        } else {
            ((ImageView) (tl_top_tab.getTabAt(selectedTab).getCustomView().findViewById(R.id.icon))).setImageResource(onImgs[selectedTab - 1]);
            ((TextView) (tl_top_tab.getTabAt(selectedTab).getCustomView().findViewById(R.id.tv_cnt))).setTypeface(null, BOLD);
        }
        originalTab = selectedTab;
    }

    private class PageAdapter extends android.support.v4.app.FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = totalFragment;
                    break;
                case 1:
                    frag = goodFragment;
                    break;
                case 2:
                    frag = normalFragment;
                    break;
                case 3:
                    frag = badFragment;
            }
            return frag;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    //get notice list
    private void getMannerPoint(final int page) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<ReviewContentDto>> genRes = restAPI.getMannerPoint(Globals.userToken, usrUid, pageIdx, page);

        genRes.enqueue(new TokenCallback<ReviewContentDto>(this) {
            @Override
            public void onSuccess(ReviewContentDto response) {
                closeLoading();

                good = (int) response.getCountTwoPoint();
                normal = (int) response.getCountOnePoint();
                bad = (int) response.getCountZeroPoint();
                total = good + normal + bad;

                btn_point.setText("" + Globals.myInfo.getUsr().getPoint());
                loadCnt();
                selectPage();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
