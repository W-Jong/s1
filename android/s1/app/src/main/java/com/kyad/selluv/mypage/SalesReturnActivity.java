package com.kyad.selluv.mypage;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.DeliveryHistoryDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.DealDeliveryReq;
import com.kyad.selluv.api.request.DealRefundAddressReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.webview.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

public class SalesReturnActivity extends BaseActivity {

    private int dealUid = 0;
    //UI Reference
    @BindView(R.id.edt_name)
    EditText edt_name;
    @BindView(R.id.edt_contact)
    EditText edt_contact;
    @BindView(R.id.edt_address)
    TextView tv_address;
    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_search)
    void onClickSearch(View v) {
        CommonWebViewActivity.showAddressWebviewActivity(this);
    }

    @OnClick(R.id.btn_accept)
    void onConfirm() {
        reqDealDelivery();
    }

    //Variables
    String[][] myAddressInfo = new String[3][4];
    int addrTabIndex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_return);
        setStatusBarWhite();
        dealUid = getIntent().getIntExtra(Constants.DEAL_UID, 0);
        loadLayout();

    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        edt_name.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    //do your work here
                    myAddressInfo[addrTabIndex][0] = edt_name.getText().toString();
                }
                isBookButtonStatus();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        edt_contact.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    //do your work here
                    myAddressInfo[addrTabIndex][1] = edt_contact.getText().toString();
                }
                isBookButtonStatus();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        tb_segment.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
                addrTabIndex = toggleIntValue;
                selectTabBtn(toggleIntValue);
            }
        });

        setAddressData();
    }

    private void isBookButtonStatus() {
        String name = edt_name.getText().toString();
        String contact = edt_contact.getText().toString();
        String address = tv_address.getText().toString();
        if (!name.equals("") && !name.equals("") && !name.equals(""))
            btn_accept.setEnabled(true);
        else
            btn_accept.setEnabled(false);
    }

    private void setAddressData() {

        for (int i = 0; i < 3; i++) {
            myAddressInfo[i][0] = "";
            myAddressInfo[i][1] = "";
            myAddressInfo[i][2] = "";
            myAddressInfo[i][3] = "";
        }
        addrTabIndex = 0;
        selectTabBtn(addrTabIndex);
    }

    private void selectTabBtn(int index) {
        tb_segment.setToggleStatus(index);
        addrTabIndex = index;
        edt_name.setText(myAddressInfo[index][0]);
        edt_contact.setText(myAddressInfo[index][1]);

        if (!myAddressInfo[index][2].equals("") && !myAddressInfo[index][3].equals("")) {
            tv_address.setText(myAddressInfo[index][2] + "\n" + myAddressInfo[index][3]);
        } else {
            tv_address.setText("");
        }

        isBookButtonStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonWebViewActivity.WEBVIEW_ADDRESS_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);
            if (isSuccess) {
                String addressDetail = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS);
                String addressDetailSub = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_POSTCODE);
                tv_address.setText(addressDetail + "\n" + addressDetailSub);

                myAddressInfo[addrTabIndex][2] = addressDetail;
                myAddressInfo[addrTabIndex][3] = addressDetailSub;

                if (!myAddressInfo[addrTabIndex][2].equals("") && !myAddressInfo[addrTabIndex][3].equals("")) {
                    tv_address.setText(myAddressInfo[addrTabIndex][2] + "\n" + myAddressInfo[addrTabIndex][3]);
                } else {
                    tv_address.setText("");
                }

            }
        }

    }

    //반품승인
    private void reqDealDelivery() {

        DealRefundAddressReq dicInfo = new DealRefundAddressReq();
        dicInfo.setAddressDetail(myAddressInfo[addrTabIndex][2]);
        dicInfo.setAddressDetailSub(myAddressInfo[addrTabIndex][3]);
        dicInfo.setAddressName(myAddressInfo[addrTabIndex][0]);
        dicInfo.setAddressPhone(myAddressInfo[addrTabIndex][1]);
        dicInfo.setAddressSelectionIndex(addrTabIndex + 1);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.reqRefundAllow(Globals.userToken, dealUid, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(SalesReturnActivity.this, getString(R.string.deal_refund_req_allow_success));
                finish();

            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }
}
