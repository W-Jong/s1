package com.kyad.selluv.camera;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.kyad.selluv.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class HorizontalImageListAdapter extends BaseAdapter {


    private final int INVALID_ID = -1;
    private HashMap<String, Integer> mIDHashMap = new HashMap<>();
    public static int hiddenPosition = -1;

    LayoutInflater inflater;
    public ArrayList<String> arrData = new ArrayList<>();
    SellPicActivity activity;
    public boolean isEditable = false;
    HorizontalListView listView;

    public HorizontalImageListAdapter(SellPicActivity activity, HorizontalListView listView) {
        this.activity = activity;
        this.listView = listView;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<String> addData) {
        arrData = addData;
        hiddenPosition = -1;
        for (int i = 0; i < addData.size(); ++i) {
            mIDHashMap.put(addData.get(i), i);
        }
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;
        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }


    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
        if (position < 0 || position >= arrData.size()) {
            return INVALID_ID;
        }

        String key = arrData.get(position);
        return mIDHashMap.get(key);
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, final ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            if (convertView == null)
                convertView = inflater.inflate(R.layout.item_captured_image, parent, false);
            String img = (String) getItem(position);

            if (img == null) {
                return convertView;
            }

            ImageView iv_img = convertView.findViewById(R.id.iv_img);
            Picasso.with(activity).load(img).into(iv_img);
            iv_img.setImageURI(Uri.parse(img));
            //iv_img.setImageBitmap(img.thumbnail);

            iv_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!listView.mDisableClick && !isEditable) {
                        PicRotateFragment picRotateFragment = new PicRotateFragment();
                        picRotateFragment.imgFiles = arrData;
                        picRotateFragment.selectedImage = position;
                        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                        picRotateFragment.show(ft, "PicRotateFragment");
                    }
                }
            });

            ImageButton ib_del = convertView.findViewById(R.id.ib_del);
            ib_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrData.remove(position);
                    if (arrData.size() == 0) {
                        ImageButton ib_right = activity.findViewById(R.id.ib_right);
                        ib_right.setVisibility(View.INVISIBLE);
                        activity.findViewById(R.id.tv_upload).setVisibility(View.INVISIBLE);
                    } else {
                        if (activity.fromPage != SellPicActivity.FROM_PAGE_SELL) {
                            activity.findViewById(R.id.tv_upload).setVisibility(View.VISIBLE);
                        } else {
                            activity.findViewById(R.id.ib_right).setVisibility(View.VISIBLE);
                        }
                    }
                    notifyDataSetChanged();
                }
            });
            if (isEditable) {
                ib_del.setVisibility(View.VISIBLE);
            } else {
                ib_del.setVisibility(View.INVISIBLE);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (position == hiddenPosition) {
            convertView.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

}