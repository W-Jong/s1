package com.kyad.selluv.top;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.EventListAdapter;
import com.kyad.selluv.adapter.RecommendUsrListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrWishDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.top.UsrRecommendDto;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.common.Globals;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class TopFriendActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lsv_content)
    ListView lv_content;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    private CallbackManager callbackManager;

    //Variables
    RecommendUsrListAdapter listAdapter;
    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_friend);
        setStatusBarWhite();

        //facebook
        callbackManager = CallbackManager.Factory.create();

        loadLayout();

        initData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void getContactList() {

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER, "data1", "contact_id"}, "has_phone_number=?", new String[]{"1"}, ContactsContract.Contacts.DISPLAY_NAME);
        int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int phoneNumberIndex = cursor.getColumnIndex("data1");

        if (cursor.getCount() > 0) {
            cursor.close();
            startActivity(ContactFriendActivity.class, false, 0, 0);
        }
    }

    private void facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("kyad-log", "Success");
                        startActivity(FacebookFriendActivity.class, false, 0, 0);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("kyad-log", "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("kyad-log", "Error");
                    }
                });
    }

    private void loadLayout() {
        LayoutInflater lf;
        View headerView;
        lf = this.getLayoutInflater();
        headerView = (View) lf.inflate(R.layout.layout_top_friend, null, false);
        headerView.findViewById(R.id.rl_find_facebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookLogin();
            }
        });
        headerView.findViewById(R.id.rl_find_contact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContactList();
            }
        });
        headerView.findViewById(R.id.rl_invite_friend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FriendInviteActivity.class, false, 0, 0);
            }
        });

        lv_content.addHeaderView(headerView, null, false);

        listAdapter = new RecommendUsrListAdapter(this);
        lv_content.setAdapter(listAdapter);
        lv_content.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getRecommendUsrList();
                }
            }
        });
    }

    public void initData(){
        currentPage = 0;
        isLast = false;
        isLoading = false;
        getRecommendUsrList();
    }

    //get recommend usr list
    private void getRecommendUsrList() {
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<UsrRecommendDto>>> genRes = restAPI.getRecommendUsrList(Globals.userToken, currentPage);

        genRes.enqueue(new TokenCallback<Page<UsrRecommendDto>>(this) {
            @Override
            public void onSuccess(Page<UsrRecommendDto> response) {
                closeLoading();
                isLoading = false;

                if (currentPage == 0) {
                    listAdapter.arrData.clear();
                }
                List<UsrRecommendDto> responseData = response.getContent();
                isLast = response.isLast();

                listAdapter.arrData.addAll(responseData);
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }
}
