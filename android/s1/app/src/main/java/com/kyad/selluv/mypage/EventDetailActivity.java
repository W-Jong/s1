package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class EventDetailActivity extends BaseActivity {

    private int evtUid = 0;
    private EventDao dicEventInfo;

    //UI Reference
    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @BindView(R.id.tv_title)
    TextView txt_title;

    @BindView(R.id.imageView5)
    ImageView iv_img;

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        setStatusBarWhite();

        evtUid = getIntent().getIntExtra(Constants.EVT_UID, 0);
        getEventDetailInfo();

    }

    private void loadLayout() {
        ImageUtils.load(iv_img.getContext(), dicEventInfo.getDetailImg(), iv_img);
        txt_title.setText(dicEventInfo.getTitle());
    }

    //get event detail info
    private void getEventDetailInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<EventDao>> genRes = restAPI.getEventDetail(Globals.userToken, evtUid);

        genRes.enqueue(new TokenCallback<EventDao>(this) {
            @Override
            public void onSuccess(EventDao response) {
                closeLoading();

                dicEventInfo = response;
                loadLayout();
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }
}
