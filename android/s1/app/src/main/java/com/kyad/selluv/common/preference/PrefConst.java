package com.kyad.selluv.common.preference;

public class PrefConst {

    public static String PREFCONST_PUSH_TOKEN = "pref_push_token";
    public static String PREFCONST_PUSH_TARGET_UID = "pref_push_target_uid";
    public static String PREFCONST_PUSH_ALARM_TYPE = "pref_push_alarm_type";

}
