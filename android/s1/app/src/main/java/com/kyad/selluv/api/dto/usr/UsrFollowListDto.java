package com.kyad.selluv.api.dto.usr;

import lombok.Data;

@Data
public class UsrFollowListDto {
    //("회원UID")
    private int usrUid;
    //("아이디")
    private String usrId;
    //("닉네임")
    private String usrNckNm;
    //("프로필이미지")
    private String profileImg;
    //("회원 팔로우 여부")
    private boolean usrLikeStatus;
}
