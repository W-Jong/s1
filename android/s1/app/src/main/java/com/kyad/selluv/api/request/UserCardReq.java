package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserCardReq {

    //@ApiModelProperty("카드종류 1-개인, 2-법인")
    @IntRange(from = 1)
    private int kind;

    //@ApiModelProperty("카드번호")
    @Size(min = 1)
    private String cardNumber;

    //@ApiModelProperty("유효기간 (형식:2018-01-01)")
//    @NonNull
    private String validDate;

    //@ApiModelProperty("유효번호 개인일경우 생년월일, 법인일 경우 사업자번호")
    @Size(min = 1)
    private String validNum;

    //@ApiModelProperty("비밀번호")
    @Size(min = 1)
    private String password;
}
