package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class FollowListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrFollowListDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public FollowListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrFollowListDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_friend_follow, parent, false);
            final UsrFollowListDto anItem = (UsrFollowListDto) getItem(position);

            if (anItem == null) {
                return convertView;
            }
            ImageView iv_profile = (ImageView) convertView.findViewById(R.id.iv_profile);
            iv_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    activity.startActivity(intent);
                }
            });


            TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    activity.startActivity(intent);
                }
            });

            TextView tv_en_name = (TextView) convertView.findViewById(R.id.tv_en_name);
            tv_en_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    activity.startActivity(intent);
                }
            });

            final ToggleButton btn_follow = (ToggleButton) convertView.findViewById(R.id.btn_follow);
            ImageUtils.load(activity, anItem.getProfileImg(), android.R.color.transparent, R.drawable.ic_user_default, iv_profile);
            tv_name.setText(anItem.getUsrNckNm());
            tv_en_name.setText(anItem.getUsrId());
            btn_follow.setChecked(anItem.isUsrLikeStatus());

            if (Globals.isMyUsrUid(anItem.getUsrUid())) {
                btn_follow.setVisibility(View.INVISIBLE);
            } else {
                btn_follow.setVisibility(View.VISIBLE);
            }

            btn_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View buttonView) {
                    btn_follow.setChecked(!btn_follow.isChecked());
                    if (btn_follow.isChecked()) {
                        final Snackbar snackbar = Snackbar.make(activity.getWindow().getDecorView(), "", Snackbar.LENGTH_INDEFINITE);
                        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                        snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
                        layout.setPadding(0, 0, 0, 0);//set padding to 0
                        View view = inflater.inflate(R.layout.action_sheet1, null);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        layout.addView(view, params);

                        ((TextView) view.findViewById(R.id.tv_name)).setText(anItem.getUsrNckNm());
                        ImageUtils.load(activity, anItem.getProfileImg(), android.R.color.transparent, R.drawable.ic_user_default, ((ImageView) view.findViewById(R.id.iv_photo)));
                        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                                btn_follow.setChecked(true);
                            }
                        });
                        view.findViewById(R.id.btn_follow_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestUserFollow(anItem);
                                btn_follow.setChecked(false);
                                snackbar.dismiss();
                            }
                        });
                        snackbar.show();
                    } else {
                        requestUserFollow(anItem);
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    private void requestUserFollow(final UsrFollowListDto usrFollow) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        activity.showLoading();

        Call<GenericResponse<Void>> genRes = usrFollow.isUsrLikeStatus() ?
                restAPI.unFollowUsr(Globals.userToken, usrFollow.getUsrUid()) :
                restAPI.followUsr(Globals.userToken, usrFollow.getUsrUid());

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {
                activity.closeLoading();
                usrFollow.setUsrLikeStatus(!usrFollow.isUsrLikeStatus());
                notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                activity.closeLoading();
            }
        });
    }

}