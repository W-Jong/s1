package com.kyad.selluv.webview;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @Author: Star_Man
 * @Date: 2018-05-03
 */

public class CommonWebViewActivity extends BaseActivity {

    //input intent key
    public static final String WEBVIEW_URL = "WEBVIEW_URL";
    public static final String WEBVIEW_TITLE = "WEBVIEW_TITLE";
    private final String APP_SCHEME = "selluv://";

    //activity request code
    public static final int WEBVIEW_PAYMENT_REQUEST = 9011;
    public static final int WEBVIEW_DANAL_REQUEST = 9012;
    public static final int WEBVIEW_ADDRESS_REQUEST = 9013;

    //activity result intent key
    public static final String WEBVIEW_OUTPUT_PAYMENT_SUCCESS = "WEBVIEW_OUTPUT_PAYMENT_SUCCESS";
    public static final String WEBVIEW_OUTPUT_DEALUID = "WEBVIEW_OUTPUT_DEALUID";

    public static final String WEBVIEW_OUTPUT_DANAL_SUCCESS = "WEBVIEW_OUTPUT_DANAL_SUCCESS";
    public static final String WEBVIEW_OUTPUT_NAME = "WEBVIEW_OUTPUT_NAME";
    public static final String WEBVIEW_OUTPUT_PHONE = "WEBVIEW_OUTPUT_PHONE";
    public static final String WEBVIEW_OUTPUT_BIRTH = "WEBVIEW_OUTPUT_BIRTH";
    public static final String WEBVIEW_OUTPUT_GENDER = "WEBVIEW_OUTPUT_GENDER";

    public static final String WEBVIEW_OUTPUT_ADDRESS_SUCCESS = "WEBVIEW_OUTPUT_ADDRESS_SUCCESS";
    public static final String WEBVIEW_OUTPUT_ADDRESS = "WEBVIEW_OUTPUT_ADDRESS";
    public static final String WEBVIEW_OUTPUT_POSTCODE = "WEBVIEW_OUTPUT_POSTCODE";

    public static void showPaymentWebviewActivity(@NotNull Activity activity, @NotNull String paymentUrl) {
        Intent intent = new Intent(activity, CommonWebViewActivity.class);
        intent.putExtra(WEBVIEW_TITLE, "결제진행");
        intent.putExtra(WEBVIEW_URL, paymentUrl);
        activity.startActivityForResult(intent, WEBVIEW_PAYMENT_REQUEST);
    }

    public static void showDanalWebviewActivity(@NotNull Activity activity) {
        Intent intent = new Intent(activity, CommonWebViewActivity.class);
        intent.putExtra(WEBVIEW_TITLE, "본인인증");
        intent.putExtra(WEBVIEW_URL, Constants.SELLUV_SERVER_URL + "/api/web/danalsms");
        activity.startActivityForResult(intent, WEBVIEW_DANAL_REQUEST);
    }

    public static void showAddressWebviewActivity(@NotNull Activity activity) {
        Intent intent = new Intent(activity, CommonWebViewActivity.class);
        intent.putExtra(WEBVIEW_URL, Constants.SELLUV_SERVER_URL + "/api/web/address");
        activity.startActivityForResult(intent, WEBVIEW_ADDRESS_REQUEST);
    }

    private Intent retIntent = new Intent();

    //UI Reference
    @BindView(R.id.rl_actionbar)
    RelativeLayout rl_actionbar;
    @BindView(R.id.ib_left)
    ImageButton ib_left;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.wv_main)
    WebView wv_main;

    private CustomWebViewClient customWebViewClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_web_view);

        String webviewUrl = getIntent().getStringExtra(WEBVIEW_URL);
        if (webviewUrl == null) {
            Utils.showToast(this, "URL을 설정해주세요.");
        }

        String webviewTitle = getIntent().getStringExtra(WEBVIEW_TITLE);
        if (TextUtils.isEmpty(webviewTitle)) {
            webviewTitle = "";
            rl_actionbar.setVisibility(View.GONE);
        } else {
            tv_title.setText(webviewTitle);
            rl_actionbar.setVisibility(View.VISIBLE);
        }

        initWebviewSetting();

        Intent intent = getIntent();
        Uri intentData = intent.getData();

        if (intentData == null) {
            wv_main.loadUrl(webviewUrl);
        } else {
            //isp 인증 후 복귀했을 때 결제 후속조치
            String url = intentData.toString();
            if (url.startsWith(APP_SCHEME)) {
                String redirectURL = url.substring(APP_SCHEME.length() + 3); //"://"가 추가로 더 전달됨
                wv_main.loadUrl(redirectURL);
            }
        }
    }

    private void initWebviewSetting() {

        //Clear Cache
        wv_main.clearHistory();
        wv_main.clearFormData();
        wv_main.clearCache(true);
        wv_main.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            wv_main.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            cookieManager.setAcceptThirdPartyCookies(wv_main, true);
        }

        if (Constants.IS_TEST) {
            //Debugging
            WebView.setWebContentsDebuggingEnabled(true);
        }

        //db usable
        wv_main.getSettings().setDatabaseEnabled(true);
        wv_main.getSettings().setDomStorageEnabled(true);

        wv_main.getSettings().setJavaScriptEnabled(true);
        //can window.open()
        wv_main.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        wv_main.getSettings().setLoadWithOverviewMode(true);
        wv_main.getSettings().setUseWideViewPort(true);
        wv_main.getSettings().setDefaultTextEncodingName("UTF-8");

        wv_main.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        wv_main.setWebChromeClient(new WebChromeClient());
        customWebViewClient = new CustomWebViewClient(this, wv_main);
        wv_main.setWebViewClient(customWebViewClient);
        wv_main.addJavascriptInterface(new CustomJavascriptInterface(), "selluv");
    }

    @OnClick(R.id.ib_left)
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        retIntent.putExtra(WEBVIEW_OUTPUT_PAYMENT_SUCCESS, false);
        retIntent.putExtra(WEBVIEW_OUTPUT_DANAL_SUCCESS, false);
        retIntent.putExtra(WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);

        closeWebviewProcessing();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /* 실시간 계좌이체 인증 후 후속처리 루틴 */
        if (data != null && data.getExtras() != null) {
            String resVal = data.getExtras().getString("bankpay_value");
            String resCode = data.getExtras().getString("bankpay_code");

            String TAG = "iamport";
            if ("000".equals(resCode)) {
                customWebViewClient.bankPayPostProcess(resCode, resVal);
            } else if ("091".equals(resCode)) {//계좌이체 결제를 취소한 경우
                Log.e(TAG, "계좌이체 결제를 취소하였습니다.");
            } else if ("060".equals(resCode)) {
                Log.e(TAG, "타임아웃");
            } else if ("050".equals(resCode)) {
                Log.e(TAG, "전자서명 실패");
            } else if ("040".equals(resCode)) {
                Log.e(TAG, "OTP/보안카드 처리 실패");
            } else if ("030".equals(resCode)) {
                Log.e(TAG, "인증모듈 초기화 오류");
            }
        }
    }

    private void closeWebviewProcessing() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setResult(RESULT_OK, retIntent);
                finish();
            }
        });
    }

    private class CustomJavascriptInterface {

        @JavascriptInterface
        public void closeWebview() {
            onBackPressed();
        }

        @JavascriptInterface
        public void showToast(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                Utils.showToast(CommonWebViewActivity.this, jsonObject.getString("msg"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void paymentSuccess(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                int dealUid = jsonObject.getInt("dealUid");
                if (Constants.IS_TEST)
                    Utils.showToast(CommonWebViewActivity.this, "Success dealUid:" + dealUid);

                //결제 성공 처리
                retIntent.putExtra(WEBVIEW_OUTPUT_PAYMENT_SUCCESS, true);
                retIntent.putExtra(WEBVIEW_OUTPUT_DEALUID, dealUid);

                closeWebviewProcessing();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void paymentFail(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                int dealUid = jsonObject.getInt("dealUid");
                if (Constants.IS_TEST)
                    Utils.showToast(CommonWebViewActivity.this, "Failed dealUid: " + dealUid);

                //결제 실패 처리
                retIntent.putExtra(WEBVIEW_OUTPUT_PAYMENT_SUCCESS, false);
                retIntent.putExtra(WEBVIEW_OUTPUT_DEALUID, dealUid);

                closeWebviewProcessing();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void danalSuccess(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                String name = jsonObject.getString("name");
                String phone = jsonObject.getString("phone");
                String birth = jsonObject.getString("birth");
                int gender = jsonObject.getInt("gender"); //1- male 2- female

                retIntent.putExtra(WEBVIEW_OUTPUT_DANAL_SUCCESS, true);
                retIntent.putExtra(WEBVIEW_OUTPUT_NAME, name);
                retIntent.putExtra(WEBVIEW_OUTPUT_PHONE, phone);
                retIntent.putExtra(WEBVIEW_OUTPUT_BIRTH, birth);
                retIntent.putExtra(WEBVIEW_OUTPUT_GENDER, gender);

                closeWebviewProcessing();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void danalFail() {
            //다날인증 실패 처리
            retIntent.putExtra(WEBVIEW_OUTPUT_DANAL_SUCCESS, false);

            closeWebviewProcessing();
        }

        @JavascriptInterface
        public void findAddress(String jsonString) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                String address = jsonObject.getString("address");
                String postcode = jsonObject.getString("postcode");

                if (Constants.IS_TEST)
                    Utils.showToast(CommonWebViewActivity.this, "address:" + address + "\npostcode:" + postcode);

                retIntent.putExtra(WEBVIEW_OUTPUT_ADDRESS_SUCCESS, true);
                retIntent.putExtra(WEBVIEW_OUTPUT_ADDRESS, address);
                retIntent.putExtra(WEBVIEW_OUTPUT_POSTCODE, postcode);

                closeWebviewProcessing();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class CustomWebViewClient extends WebViewClient {
        private Activity activity;
        private WebView target;
        private String BANK_TID = "";
        private String NICE_BANK_URL = "https://web.nicepay.co.kr/smart/bank/payTrans.jsp";    // 계좌이체 거래 요청 URL(V2부터는 가변적일 수 있음)

        final int RESCODE = 1;
        final String NICE_URL = "https://web.nicepay.co.kr/smart/interfaceURL.jsp";            // NICEPAY SMART 요청 URL
        final String KTFC_PACKAGE = "com.kftc.bankpay.android";

        public CustomWebViewClient(Activity activity, WebView target) {
            this.activity = activity;
            this.target = target;
        }

        public void bankPayPostProcess(String bankpayCode, String bankpayValue) {
            String postData = "callbackparam2=" + BANK_TID + "&bankpay_code=" + bankpayCode + "&bankpay_value=" + bankpayValue;
            target.postUrl(NICE_BANK_URL, EncodingUtils.getBytes(postData, "euc-kr"));
        }

        @Override
        @SuppressWarnings("deprecation")
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("CommonWebview", "url: " + url);
            if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("javascript:")) {
                Intent intent = null;

                try {
                    /* START - BankPay(실시간계좌이체)에 대해서는 예외적으로 처리 */
                    if (url.startsWith(PaymentScheme.BANKPAY)) {
                        String reqParam = null;
                        try {
                            reqParam = makeBankPayDataV2(url);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        intent = new Intent(Intent.ACTION_MAIN);
                        intent.setComponent(new ComponentName("com.kftc.bankpay.android", "com.kftc.bankpay.android.activity.MainActivity"));
                        intent.putExtra("requestInfo", reqParam);
                        activity.startActivityForResult(intent, RESCODE);

                        return true;
                    }
                    /* END - BankPay(실시간계좌이체)에 대해서는 예외적으로 처리 */

                    intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME); //IntentURI처리
                    Uri uri = Uri.parse(intent.getDataString());

                    activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    return true;
                } catch (URISyntaxException ex) {
                    return false;
                } catch (ActivityNotFoundException e) {
                    if (intent == null) return false;

                    String schema = intent.getScheme();
                    if (schema == null || url.startsWith(PaymentScheme.BANKPAY))
                        schema = PaymentScheme.BANKPAY;

                    if (handleNotFoundPaymentScheme(schema))
                        return true;

                    String packageName = intent.getPackage();
                    if (packageName != null) {
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                        return true;
                    }

                    return false;
                }
            }

            return false;
        }

        /**
         * @param scheme
         * @return 해당 scheme에 대해 처리를 직접 하는지 여부
         * <p>
         * 결제를 위한 3rd-party 앱이 아직 설치되어있지 않아 ActivityNotFoundException이 발생하는 경우 처리합니다.
         * 여기서 handler되지않은 scheme에 대해서는 intent로부터 Package정보 추출이 가능하다면 다음에서 packageName으로 market이동합니다.
         */
        protected boolean handleNotFoundPaymentScheme(String scheme) {
            //PG사에서 호출하는 url에 package정보가 없어 ActivityNotFoundException이 난 후 market 실행이 안되는 경우
            if (PaymentScheme.ISP.equalsIgnoreCase(scheme)) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + PaymentScheme.PACKAGE_ISP)));
                return true;
            } else if (PaymentScheme.BANKPAY.equalsIgnoreCase(scheme)) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + PaymentScheme.PACKAGE_BANKPAY)));
                return true;
            }

            return false;
        }

        private String makeBankPayDataV2(String url) throws UnsupportedEncodingException {
            String prefix = PaymentScheme.BANKPAY + "://eftpay?";

            Uri uri = Uri.parse(url);
            BANK_TID = uri.getQueryParameter("user_key");
            NICE_BANK_URL = uri.getQueryParameter("callbackparam1");

            return URLDecoder.decode(url.substring(prefix.length()), "utf-8");
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            final Uri uri = request.getUrl();
            return shouldOverrideUrlLoading(view, uri.toString());
        }

        @Override
        public void onLoadResource(WebView view, String url) {
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
        }
    }
}
