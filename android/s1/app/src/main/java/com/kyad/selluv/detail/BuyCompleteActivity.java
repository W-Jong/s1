package com.kyad.selluv.detail;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.category.CategoryMiniDto;
import com.kyad.selluv.api.dto.other.PromotionCodeDao;
import com.kyad.selluv.api.dto.pdt.PdtDetailDto;
import com.kyad.selluv.api.request.DealBuyReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.DockAnimation;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.mypage.PurchaseDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class BuyCompleteActivity extends BaseActivity {

    public static final String DEAL_UID = "DEAL_UID";

    private int dealUid = 0;
    private PdtDetailDto pdtDetail;
    private DealBuyReq buyReq;
    private PromotionCodeDao promotionCode = null;

    public boolean visible1 = false;
    public boolean visible2 = false;

    //UI Reference
    @BindView(R.id.tv_ware)
    TextView tv_ware;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_contact)
    TextView tv_contact;
    @BindView(R.id.txt_item_price_value)
    TextView txt_item_price_value;
    @BindView(R.id.txt_transport_price_value)
    TextView txt_transport_price_value;
    @BindView(R.id.txt_item_office_test_value)
    TextView txt_item_office_test_value;
    @BindView(R.id.txt_promotion_verify_value)
    TextView txt_promotion_verify_value;
    @BindView(R.id.txt_selluvmoney_use_value)
    TextView txt_selluvmoney_use_value;
    @BindView(R.id.btn_paymoney_collapse)
    ImageButton btn_paymoney_collapse;
    @BindView(R.id.btn_max_selluvmoney_collapse)
    ImageButton btn_max_selluvmoney_collapse;
    @BindView(R.id.rl_paymoney_bar)
    RelativeLayout rl_paymoney_bar;
    @BindView(R.id.rl_max_money_bar)
    RelativeLayout rl_maxmoney_bar;
    @BindView(R.id.txt_total_price_value)
    TextView txt_total_price_value;
    @BindView(R.id.tv_paymoney_value)
    TextView tv_paymoney_value;
    @BindView(R.id.rl_extra_send_price_info_area)
    RelativeLayout rl_extra_send_price_info_area;
    @BindView(R.id.txt_extra_transport)
    TextView txt_extra_transport;
    @BindView(R.id.txt_extra_transport_price_value)
    TextView txt_extra_transport_price_value;
    @BindView(R.id.rl_promotion_info_area)
    RelativeLayout rl_promotion_info_area;
    @BindView(R.id.rl_selluvmoney_info_area)
    RelativeLayout rl_selluvmoney_info_area;
    @BindView(R.id.txt_price_unit2)
    TextView txt_price_unit2;
    @BindView(R.id.txt_buy_price_percent)
    TextView txt_buy_price_percent;
    @BindView(R.id.txt_buy_price_value)
    TextView txt_buy_price_value;
    @BindView(R.id.txt_after_price_value)
    TextView txt_after_price_value;
    @BindView(R.id.tv_max_selluvmoney_value)
    TextView tv_max_selluvmoney_value;
    @BindView(R.id.txt_max_total_price_value)
    TextView txt_max_total_price_value;

    @OnClick(R.id.rl_pay_money)
    void onCollapse1(View v) {
        showDetail1();
    }

    @OnClick(R.id.rl_max_selluvmoney)
    void onCollapse2(View v) {
        showDetail2();
    }

    @OnClick(R.id.btn_verify)
    void onVerify(View v) {
        Intent intent = new Intent(this, PurchaseDetailActivity.class);
        intent.putExtra(DEAL_UID, dealUid);
        startActivity(intent);
        finish();
        Globals.selectedPdtDetailDto = null;
    }

    @OnClick(R.id.btn_continue)
    void onContinue(View v) {
        finish();
        Globals.selectedPdtDetailDto = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_buy_complete);
        setStatusBarWhite();

        dealUid = getIntent().getIntExtra(DEAL_UID, 0);

        if (Globals.selectedPdtDetailDto == null || Globals.selectedDealBuyReq == null) {
            Utils.showToast(this, "잘못된 접근입니다.");
            finish();
        }

        pdtDetail = Globals.selectedPdtDetailDto;
        buyReq = Globals.selectedDealBuyReq;
        promotionCode = Globals.selectedPromotionCodeDao;

        loadLayout();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        onContinue(null);
    }

    private void loadLayout() {
        tv_name.setText(buyReq.getRecipientNm());
        tv_contact.setText(buyReq.getRecipientPhone());
        tv_address.setText(buyReq.getRecipientAddress());

        tv_ware.setText(Globals.selectedPdtDetailDto.getPdtTitle());

        txt_total_price_value.setText(String.valueOf(Constants.itemDetail.total_price));
        tv_paymoney_value.setText(String.valueOf(Constants.itemDetail.total_price));

        txt_item_price_value.setText(Utils.getAttachCommaFormat(pdtDetail.getPdt().getPrice()));
        txt_transport_price_value.setText(Utils.getAttachCommaFormat((pdtDetail.isFreeSend()) ? 0 : pdtDetail.getPdt().getSendPrice()));

        txt_buy_price_percent.setText("(" + (Globals.systemSetting != null ? Globals.systemSetting.getDealReward() : 0) + "%)");
        txt_after_price_value.setText(Utils.getAttachCommaFormat(Globals.systemSetting != null ? Globals.systemSetting.getBuyReviewReward() : 0));

        refreshPriceInfoArea();
    }

    private void refreshPriceInfoArea() {

        long total_pay_price = buyReq.getReqPrice() + ((pdtDetail.isFreeSend()) ? 0 : pdtDetail.getPdt().getSendPrice());

        if (buyReq.getIslandSendPrice() > 0) {
            rl_extra_send_price_info_area.setVisibility(View.VISIBLE);
            txt_extra_transport_price_value.setText(Utils.getAttachCommaFormat(buyReq.getIslandSendPrice()));
            if (buyReq.getIslandSendPrice() == 3000) {
                txt_extra_transport.setText("제주도지역 추가 배송비");
            } else {
                txt_extra_transport.setText("도서지역 추가 배송비");
            }
            total_pay_price += buyReq.getIslandSendPrice();
        } else {
            rl_extra_send_price_info_area.setVisibility(View.GONE);
        }

        if (buyReq.getPdtVerifyEnabled() == 1) {
            txt_price_unit2.setText("원");
            txt_item_office_test_value.setVisibility(View.VISIBLE);
            txt_item_office_test_value.setText(Utils.getAttachCommaFormat(39000));
            total_pay_price += 39000;
        } else {
            txt_price_unit2.setText("무료");
            txt_item_office_test_value.setVisibility(View.GONE);
        }

        if (promotionCode == null || TextUtils.isEmpty(buyReq.getPayPromotion())) {
            rl_promotion_info_area.setVisibility(View.GONE);
        } else {
            long promotionSalePrice = 0;
            if (promotionCode.getPriceUnit() == 1) {
                promotionSalePrice = (long) (buyReq.getReqPrice() * promotionCode.getPrice() / 100.0f);
            } else {
                promotionSalePrice = promotionCode.getPrice();
            }
            if (promotionSalePrice > 0) {
                rl_promotion_info_area.setVisibility(View.VISIBLE);
                txt_promotion_verify_value.setText("- " + Utils.getAttachCommaFormat(promotionSalePrice));
                total_pay_price -= promotionSalePrice;
            } else {
                rl_promotion_info_area.setVisibility(View.GONE);
            }
        }

        if (buyReq.getRewardPrice() > 0) {
            rl_selluvmoney_info_area.setVisibility(View.VISIBLE);
            txt_selluvmoney_use_value.setText("- " + Utils.getAttachCommaFormat(buyReq.getRewardPrice()));
            total_pay_price -= buyReq.getRewardPrice();
        } else {
            rl_selluvmoney_info_area.setVisibility(View.GONE);
        }

        txt_total_price_value.setText(Utils.getAttachCommaFormat(total_pay_price));
        tv_paymoney_value.setText(Utils.getAttachCommaFormat(total_pay_price) + " 원");

        long deal_reward = (long)(total_pay_price * (Globals.systemSetting != null ? Globals.systemSetting.getDealReward() : 0) / 100.0f);
        txt_buy_price_value.setText(Utils.getAttachCommaFormat(deal_reward));
        tv_max_selluvmoney_value.setText(Utils.getAttachCommaFormat(deal_reward + (Globals.systemSetting != null ? Globals.systemSetting.getBuyReviewReward() : 0)));
        txt_max_total_price_value.setText(Utils.getAttachCommaFormat(deal_reward + (Globals.systemSetting != null ? Globals.systemSetting.getBuyReviewReward() : 0)));

    }

    void showDetail1() {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_paymoney_bar, 300,
                DockAnimation.FRAME_LAYOUT);

        int from = !visible1 ? 0 : Utils.dpToPixel(this, 195);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(this, 195) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.start();

        visible1 = !visible1;

        btn_paymoney_collapse.animate().rotationBy(180).setDuration(400).start();
    }

    void showDetail2() {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_maxmoney_bar, 300,
                DockAnimation.FRAME_LAYOUT);

        int from = !visible2 ? 0 : Utils.dpToPixel(this, 135);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(this, 135) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.start();

        visible2 = !visible2;

        btn_max_selluvmoney_collapse.animate().rotationBy(180).setDuration(400).start();
    }

}
