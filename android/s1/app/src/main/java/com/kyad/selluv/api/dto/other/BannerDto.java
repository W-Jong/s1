package com.kyad.selluv.api.dto.other;

import lombok.Data;

@Data
public class BannerDto {
    private BannerDao banner; // (BannerDao, optional): 배너정보 ,
    private Object target; // (object, optional): 타겟오브젝트(null검사필수), 공지사항인 경우 NoticeDao, 이벤트인 경우 EventDao, 테마인 경우 null
}
