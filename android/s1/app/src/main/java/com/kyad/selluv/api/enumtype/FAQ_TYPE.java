package com.kyad.selluv.api.enumtype;

public enum FAQ_TYPE {
    SELL(1), BUY(2), ACTIVITY(3);

    private int code;

    private FAQ_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
