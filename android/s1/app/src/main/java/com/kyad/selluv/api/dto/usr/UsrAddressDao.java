package com.kyad.selluv.api.dto.usr;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class UsrAddressDao {
    int usrAddressUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"회원UID")
    private int usrUid;
    //"주소 순서 1,2,3만 가능")
    private int addressOrder = 1;
    //"성함")
    private String addressName = "";
    //"연락처")
    private String addressPhone = "";
    //"주소")
    private String addressDetail = "";
    //"상세주소")
    private String addressDetailSub = "";
    //"1이면 정상, 2이면 해당 유저가 기본으로 선택한 주소")
    private int status = 1;
}
