package com.kyad.selluv.api.request;


import android.support.annotation.Size;

import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class MultiStringReq {
    //@ApiModelProperty("string 배열")
//    @NonNull
//    @Size(min = 1)
    private List<String> list;
}
