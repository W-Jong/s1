package com.kyad.selluv.api.dto.deal;

import com.kyad.selluv.api.dto.usr.UsrMiniDto;

import lombok.Data;

@Data
public class DealListDto {
    //("거래정보")
    private DealMiniDto deal;
    //("구매자 또는 판매자정보")
    private UsrMiniDto usr;
}