package com.kyad.selluv.detail;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.DetailStyleAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.LicenseDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.category.SizeRefDto;
import com.kyad.selluv.api.dto.other.UsrWishDao;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.pdt.PdtDetailDto;
import com.kyad.selluv.api.dto.pdt.PdtFakeInfoDto;
import com.kyad.selluv.api.dto.pdt.PdtInfoDto;
import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.api.enumtype.LICENSE_TYPE;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.api.enumtype.PDT_STATUS_TYPE;
import com.kyad.selluv.api.enumtype.REPORT_TYPE;
import com.kyad.selluv.api.request.ContentReq;
import com.kyad.selluv.api.request.PdtStyleMultiReq;
import com.kyad.selluv.api.request.ReportReq;
import com.kyad.selluv.api.request.SearchReq;
import com.kyad.selluv.api.request.UserWishReq;
import com.kyad.selluv.brand.BrandDetailActivity;
import com.kyad.selluv.camera.GalleryFragment;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.IndicatorView;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.RoundRectCornerImageView;
import com.kyad.selluv.common.SoftKeyboard;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.WrapStaggeredGridLayoutManager;
import com.kyad.selluv.home.WareListAdapter;
import com.kyad.selluv.shop.CategoryActivity;
import com.kyad.selluv.user.UserActivity;
import com.kyad.selluv.user.UserFollowActivity;
import com.kyad.selluv.user.UserMannerActivity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Constants.COLOR_NAMES;

public class DetailActivity extends BaseActivity {

    public static final String PDT_UID = "PDT_UID";

    SoftKeyboard softKeyboard;

    private int pdtUid;
    private PdtDetailDto pdtDetail;
    private int page = 0;
    private boolean isRecommendListLoading = false;
    private boolean isRecommendListLoadingEnd = false;
    private boolean isStyleAddBtnShowed = false;
    private boolean isBottomBarShowed = true;
    private Snackbar snackbar, subSnackbar;

    private int imgID;
    String categoryName = "";
    String guarantee = "";
    String escrow = "";
    String delivery = "";

    //UI Reference
    @BindView(R.id.iv_photo)
    ImageView iv_photo;
    @BindView(R.id.user_name)
    TextView user_name;
    @BindView(R.id.activetime)
    TextView active_time;
    @BindView(R.id.tv_sleep_status_alert)
    TextView tv_sleep_status_alert;
    @BindView(R.id.item_name)
    TextView item_name;
    @BindView(R.id.item_old_price)
    TextView item_old_price;
    @BindView(R.id.item_new_price)
    TextView item_new_price;
    @BindView(R.id.item_lower_percent)
    TextView item_lower_percent;
    @BindView(R.id.iv_price_lower)
    ImageView iv_price_lower;
    @BindView(R.id.item_size)
    TextView item_size;
    @BindView(R.id.tv_like_cnt)
    TextView tv_like_cnt;
    @BindView(R.id.tv_item_cnt)
    TextView tv_item_cnt;
    @BindView(R.id.tv_article_cnt)
    TextView tv_article_cnt;
    @BindView(R.id.tv_item_group)
    TextView tv_item_group;
    @BindView(R.id.tv_item_category1)
    TextView tv_item_category1;
    @BindView(R.id.tv_item_brand)
    TextView tv_item_brand;
    @BindView(R.id.tv_item_category2)
    TextView tv_item_category2;
    @BindView(R.id.tv_item_category3)
    TextView tv_item_category3;
    @BindView(R.id.tv_item_color)
    TextView tv_item_color;
    @BindView(R.id.tv_item_tag)
    TextView tv_item_tag;
    @BindView(R.id.tv_item_description)
    TextView tv_item_description;
    @BindView(R.id.detail_description_more)
    Button btn_detail_description_more;
    @BindView(R.id.tv_item_model)
    TextView tv_item_model;
    @BindView(R.id.btn_detail_ship)
    Button btn_detail_ship;
    @BindView(R.id.btn_detail_like)
    Button btn_detail_like;
    @BindView(R.id.btn_style)
    Button btn_style;
    @BindView(R.id.btn_detail_itemtag)
    Button btn_detail_itemtag;
    @BindView(R.id.btn_detail_guarantee)
    Button btn_detail_guarantee;
    @BindView(R.id.btn_detail_receipt)
    Button btn_detail_receipt;
    @BindView(R.id.btn_detail_rest)
    Button btn_detail_rest;
    @BindView(R.id.btn_detail_brandbox)
    Button btn_detail_brandbox;
    @BindView(R.id.btn_detail_dustbag)
    Button btn_detail_dustbag;
    @BindView(R.id.btn_detail_etc)
    Button btn_detail_etc;
    @BindView(R.id.rl_other_info_content)
    GridLayout rl_other_info_content;
    @BindView(R.id.rl_style_none)
    RelativeLayout rl_style_none;
    @BindView(R.id.rl_item_indicator)
    IndicatorView rl_item_indicator;
    @BindView(R.id.lv_rec_style)
    RecyclerView lv_rec_style;
    @BindView(R.id.ll_ware_list)
    LinearLayout ll_ware_list;
    @BindView(R.id.ll_info)
    LinearLayout ll_info;
    @BindView(R.id.iv_soldout)
    ImageView iv_soldout;
    @BindView(R.id.btn_rel_style)
    Button btn_rel_style;
    @BindView(R.id.btn_nego)
    Button btn_nego;
    @BindView(R.id.btn_buy)
    Button btn_buy;
    @BindView(R.id.sv_info)
    NestedScrollView sv_info;
    @BindView(R.id.ib_style_reg)
    ImageButton ib_style_reg;
    @BindView(R.id.vp_item_image)
    ViewPager vp_item_image;
    @BindView(R.id.grid_view)
    RecyclerView grid_view;
    @BindView(R.id.iv_heart)
    ImageView iv_heart;
    @BindView(R.id.iv_user_photo)
    ImageView iv_user_photo;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.mannerpoint_cnt)
    TextView mannerpoint_cnt;
    @BindView(R.id.seller_item_cnt)
    TextView seller_item_cnt;
    @BindView(R.id.seller_follow_cnt)
    TextView seller_follow_cnt;
    @BindView(R.id.like_cnt)
    TextView like_cnt;
    @BindView(R.id.share_cnt)
    TextView share_cnt;
    @BindView(R.id.talk_cnt)
    TextView talk_cnt;
    @BindView(R.id.ll_bottombar)
    LinearLayout ll_bottombar;
    @BindView(R.id.iv_share)
    ImageView iv_share;
    @BindView(R.id.iv_info)
    ImageView btnSizeGuide;

    @OnClick({R.id.iv_user_photo, R.id.seller_item, R.id.seller_item_cnt, R.id.iv_photo, R.id.user_name, R.id.ib_seller_brand_more})
    void goUserPage(View v) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(UserActivity.USR_UID, pdtDetail.getSeller().getUsrUid());
        startActivity(intent);
    }

    @OnClick({R.id.iv_manerpoint, R.id.mannerpoint_cnt})
    void goUserMannerPage(View v) {
        Intent intent = new Intent(getActivity(), UserMannerActivity.class);
        intent.putExtra(Constants.USER_UID, pdtDetail.getSeller().getUsrUid());
        startActivity(intent);
    }

    @OnClick({R.id.seller_follow, R.id.seller_follow_cnt})
    void goUserFollowPage(View v) {
        Intent intent = new Intent(this, UserFollowActivity.class);
        intent.putExtra(Constants.USER_UID, pdtDetail.getSeller().getUsrUid());
        intent.putExtra(Constants.FOLLOW_PAGE_TITLE, pdtDetail.getSeller().getUsrNckNm());
        intent.putExtra(Constants.FOLLOW_PAGE_IDX, 0);
        startActivity(intent);
    }

    @OnClick(R.id.btn_detail_ship)
    void viewToastShip(View v) {
        if (!pdtDetail.isFreeSend()) {
            Utils.showToast(getActivity(), String.format(Locale.getDefault(), "￦%s", Utils.getAttachCommaFormat(pdtDetail.getFreeSendPrice())));
        } else {
            Utils.showToast(getActivity(), loadMessage(1));
        }
    }

    @OnClick(R.id.btn_detail_like)
    void viewToastLike(View v) {
        String toastStr = "";
        switch (pdtDetail.getPdt().getPdtCondition()) {
            case 1:
                toastStr = "현재 상품은 새 상품입니다.";
                break;
            case 2:
                toastStr = "현재 상품은 최상 중고상품입니다.";
                break;
            case 3:
                toastStr = "현재 상품은 상 중고상품입니다.";
                break;
            case 4:
                toastStr = "현재 상품은 중상 중고상품입니다.";
                break;
        }
        Utils.showToast(getActivity(), toastStr);
    }

    @OnClick(R.id.btn_detail_itemtag)
    void viewToastTag(View v) {
        Utils.showToast(getActivity(), loadMessage(3));
    }

    @OnClick(R.id.btn_detail_guarantee)
    void viewToastGuarantee(View v) {
        Utils.showToast(getActivity(), loadMessage(4));
    }

    @OnClick(R.id.btn_detail_receipt)
    void viewToastReceipt(View v) {
        Utils.showToast(getActivity(), loadMessage(5));
    }

    @OnClick(R.id.btn_detail_rest)
    void viewToastRest(View v) {
        Utils.showToast(getActivity(), loadMessage(6));
    }

    @OnClick(R.id.btn_detail_brandbox)
    void viewToastBrnadBox(View v) {
        Utils.showToast(getActivity(), loadMessage(7));
    }

    @OnClick(R.id.btn_detail_dustbag)
    void viewToastDustBag(View v) {
        Utils.showToast(getActivity(), loadMessage(8));
    }

    @OnClick(R.id.btn_detail_etc)
    void viewToastETC(View v) {
        if (pdtDetail == null)
            return;
        Utils.showToast(getActivity(), pdtDetail.getPdt().getEtc());
    }

    @OnClick(R.id.tv_item_model)
    void goSameModel(View v) {
        Intent intent = new Intent(this, DetailRecStyleActivity.class);
        intent.putExtra(DetailRecStyleActivity.PDT_UID, pdtUid);
        intent.putExtra(DetailRecStyleActivity.FRAG_TYPE, Constants.WIRE_FRAG_PDT_SAME_MODEL);
        intent.putExtra(DetailRecStyleActivity.KEYWORD, pdtDetail.getPdt().getPdtModel());
        startActivity(intent);
    }

    @OnClick(R.id.tv_item_tag)
    void goSameTag(View v) {
        Intent intent = new Intent(this, DetailRecStyleActivity.class);
        intent.putExtra(DetailRecStyleActivity.PDT_UID, pdtUid);
        intent.putExtra(DetailRecStyleActivity.FRAG_TYPE, Constants.WIRE_FRAG_PDT_SAME_TAG);

        String tags = pdtDetail.getPdt().getTag().replaceAll("#", "");
        String firstTag = tags.substring(0, tags.indexOf(" "));
        intent.putExtra(DetailRecStyleActivity.KEYWORD, firstTag);
        startActivity(intent);
    }

    @OnClick(R.id.tv_item_group)
    void goCategoryPdtGroup(View v) {
        String categoryName = pdtDetail.getPdt().getPdtGroup() == 1 ? "남성" : pdtDetail.getPdt().getPdtGroup() == 2 ? "여성" : "키즈";
        toCategoryPage(pdtDetail.getPdt().getPdtGroup(), pdtDetail.getPdt().getPdtGroup(), categoryName);
    }

    @OnClick(R.id.tv_item_category1)
    void goCategoryStep1(View v) {
        if (pdtDetail.getCategoryList().size() < 2) {
            return;
        }

        categoryName = "";
        if (pdtDetail.getPdt().getPdtGroup() == 4) {
            categoryName = pdtDetail.getCategoryList().get(1).getCategoryName();
        } else {
            String likegroup = pdtDetail.getPdt().getPdtGroup() == 1 ? "남성" : pdtDetail.getPdt().getPdtGroup() == 2 ? "여성" : "키즈";
            categoryName = likegroup + " " + pdtDetail.getCategoryList().get(1).getCategoryName();
        }
        toCategoryPage(pdtDetail.getPdt().getPdtGroup(), pdtDetail.getCategoryList().get(1).getCategoryUid(), categoryName);
    }

    @OnClick(R.id.tv_item_category2)
    void goCategoryStep2(View v) {
        if (pdtDetail.getCategoryList().size() < 3) {
            return;
        }

        categoryName = "";
        if (pdtDetail.getPdt().getPdtGroup() == 4) {
            categoryName = pdtDetail.getCategoryList().get(1).getCategoryName() + " " + pdtDetail.getCategoryList().get(2).getCategoryName();
        } else {
            String likegroup = pdtDetail.getPdt().getPdtGroup() == 1 ? "남성" : pdtDetail.getPdt().getPdtGroup() == 2 ? "여성" : "키즈";
            categoryName = likegroup + " " + pdtDetail.getCategoryList().get(2).getCategoryName();
        }

        toCategoryPage(pdtDetail.getPdt().getPdtGroup(), pdtDetail.getCategoryList().get(2).getCategoryUid(), categoryName);
    }

    @OnClick(R.id.tv_item_category3)
    void goCategoryStep3(View v) {
        if (pdtDetail.getCategoryList().size() < 4) {
            return;
        }
        categoryName = "";
        if (pdtDetail.getPdt().getPdtGroup() == 4) {
            categoryName = pdtDetail.getCategoryList().get(1).getCategoryName() + pdtDetail.getCategoryList().get(2).getCategoryName();
        } else {
            String likegroup = pdtDetail.getPdt().getPdtGroup() == 1 ? "남성" : pdtDetail.getPdt().getPdtGroup() == 2 ? "여성" : "키즈";
            categoryName = likegroup + " " + pdtDetail.getCategoryList().get(3).getCategoryName();
        }

        toCategoryPage(pdtDetail.getPdt().getPdtGroup(), pdtDetail.getCategoryList().get(3).getCategoryUid(), categoryName);
    }

    @OnClick(R.id.tv_item_brand)
    void goBrandPage(View v) {
        Intent intent = new Intent(this, BrandDetailActivity.class);
        intent.putExtra(BrandDetailActivity.BRAND_UID, pdtDetail.getPdt().getBrandUid());
        intent.putExtra(BrandDetailActivity.BRAND_PRODUCT_GROUP, pdtDetail.getPdt().getPdtGroup() == 4 ? 3 : pdtDetail.getPdt().getPdtGroup());
        startActivity(intent);
    }

    @OnClick(R.id.tv_item_color)
    void goCategoryStep2Color(View v) {
        if (pdtDetail.getCategoryList().size() < 3) {
            return;
        }

        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(CategoryActivity.PDT_GROUP, pdtDetail.getPdt().getPdtGroup());
        intent.putExtra(CategoryActivity.CATEGORY_UID, pdtDetail.getCategoryList().get(2).getCategoryUid());
        intent.putExtra(CategoryActivity.COLOR_NAME, pdtDetail.getPdt().getColorName());
        startActivity(intent);
    }

    @OnClick(R.id.btn_rel_style)
    void goRelStyle(View v) {
        Intent intent = new Intent(this, DetailRecStyleActivity.class);
        intent.putExtra(DetailRecStyleActivity.PDT_UID, pdtUid);
        intent.putExtra(DetailRecStyleActivity.FRAG_TYPE, Constants.WIRE_FRAG_PDT_RELATION_STYLE);
        startActivity(intent);
    }

    @OnClick(R.id.vp_item_image)
    void goSlide(View v) {


    }

    @OnClick(R.id.detail_description_more)
    void showMoreDescription(View v) {
        if (pdtDetail == null)
            return;

        btn_detail_description_more.setVisibility(View.GONE);
        tv_item_description.setMaxLines(100);
    }

    @OnClick(R.id.ib_alarm)
    void onAlarm(View v) {
        hideSnackBars();

        final PopupDialog popup = new PopupDialog(getActivity(), R.layout.detail_alarm).setFullScreen().setCancelable(true);

        ((TextView) popup.findView(R.id.tv_brand_value)).setText(pdtDetail.getBrand().getNameKo());
        ((TextView) popup.findView(R.id.tv_waregroup_value)).setText(pdtDetail.getCategoryList().get(0).getCategoryName());
        ((TextView) popup.findView(R.id.tv_category_value)).setText(pdtDetail.getCategoryList().get(pdtDetail.getCategoryList().size() - 1).getCategoryName());

        getBrandModelNames(((CustomSpinner) popup.findView(R.id.spinner_model)));
        getCategorySizeData(((CustomSpinner) popup.findView(R.id.spinner_size)));


        ((CustomSpinner) popup.findView(R.id.spinner_color)).initializeStringValues(this, COLOR_NAMES, getString(R.string.color_select), null, null, 12.5f);

        popup.setOnClickListener(R.id.btn_alarm_register, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                insertUserWishAlarm(popup);
            }
        });

        popup.setOnClickListener(R.id.btn_alarm_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });

        popup.show();
    }

    @OnClick(R.id.btn_nego)
    void onNego(View v) {
        hideSnackBars();

        if (pdtDetail.getPdt().getStatus() == PDT_STATUS_TYPE.SOLD.getCode()) {
            return;
        }

        if (Globals.isMyUsrUid(pdtDetail.getSeller().getUsrUid())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.item_delete))
                    .setMessage(getString(R.string.detail_pdt_delete_hint))
                    .setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deletePdt();
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
//            finish();
        } else {
            if (pdtDetail.getPdt().getNegoYn() != 1) {
                return;
            }

            final PopupDialog popup = new PopupDialog(this, R.layout.detail_nego).setFullScreen().setCancelable(true);
//            Utils.hideKeypad(getActivity(), popup.findView(R.id.ll_nego_background));

            EditText etCommitPrice = ((EditText) popup.findView(R.id.et_commit_price_value));

            RelativeLayout mainLayout = (RelativeLayout) popup.findView(R.id.ll_nego_background);
            InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);

            softKeyboard = new SoftKeyboard(mainLayout, im);
            softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {
                @Override
                public void onSoftKeyboardHide() {
                    String ss = etCommitPrice.getText().toString().replaceAll(",", "");
                    if (!ss.isEmpty()) {
                        etCommitPrice.setText(Utils.getAttachCommaFormat(Float.valueOf(ss)));
                    }
                }

                @Override
                public void onSoftKeyboardShow() {
                    String ss = etCommitPrice.getText().toString().replaceAll(",", "");
                    if (!ss.isEmpty()) {
                        etCommitPrice.setText(ss);
                    }
                }
            });

            ImageUtils.load(this, pdtDetail.getPdt().getProfileImg(), android.R.color.transparent, R.drawable.img_default_square, ((ImageView) popup.findView(R.id.iv_item_ware)));
            ((TextView) popup.findView(R.id.tv_nego_commit_description)).setText(Html.fromHtml(getString(R.string.detail_nego_dialog_text)));
            ((TextView) popup.findView(R.id.tv_ware_brand_eng)).setText(pdtDetail.getBrand().getNameEn());
            ((TextView) popup.findView(R.id.tv_ware_name_ko)).setText(pdtDetail.getPdtTitle());
            ((TextView) popup.findView(R.id.tv_ware_size)).setText("size " + pdtDetail.getPdt().getPdtSize());
            ((TextView) popup.findView(R.id.tv_ware_price)).setText("￦ " + Utils.getAttachCommaFormat((float) pdtDetail.getPdt().getPrice()));

            popup.setOnClickListener(R.id.btn_nego_request, new PopupDialog.bindOnClick() {
                @Override
                public void onClick() {
                    String ss = etCommitPrice.getText().toString().replaceAll(",", "");
                    if (ss.isEmpty()) {
                        return;
                    }

                    float nego_price = Float.valueOf(ss);
                    if (!(nego_price >= (float) pdtDetail.getPdt().getPrice() / 2 && nego_price <= (float) pdtDetail.getPdt().getPrice())) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
                        builder.setTitle(getString(R.string.alarm))
                                .setMessage("원하시는 가격을 정확히 입력해 주세요")
                                .setNegativeButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                        return;
                    }

                    Globals.selectedPdtDetailDto = pdtDetail;
                    Intent intent = new Intent(DetailActivity.this, NegoCheckoutActivity.class);
                    intent.putExtra(NegoCheckoutActivity.PDT_UID, pdtUid);
//                    intent.putExtra(NegoCheckoutActivity.NEGO_PRICE, Utils.getRemoveCommaValue(((TextView) popup.findView(R.id.tv_commit_price_value)).getText().toString()));
                    intent.putExtra(NegoCheckoutActivity.NEGO_PRICE, Utils.getRemoveCommaValue(((EditText) popup.findView(R.id.et_commit_price_value)).getText().toString()));
                    startActivity(intent);
                    softKeyboard.unRegisterSoftKeyboardCallback();
                    popup.dismiss();
                }
            }).setOnClickListener(R.id.btn_close, new PopupDialog.bindOnClick() {
                @Override
                public void onClick() {
                    softKeyboard.unRegisterSoftKeyboardCallback();
                    popup.dismiss();
                }
            });

            ((CrystalSeekbar) popup.findView(R.id.seekBar)).setMinValue((float) pdtDetail.getPdt().getPrice() / 2)
                    .setMaxValue((float) pdtDetail.getPdt().getPrice())
                    .setMinStartValue((float) pdtDetail.getPdt().getPrice())
                    .apply();
            ((CrystalSeekbar) popup.findView(R.id.seekBar)).setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
                @Override
                public void valueChanged(Number value) {
//                    ((TextView) popup.findView(R.id.tv_commit_price_value)).setText(Utils.getAttachCommaFormat(value.floatValue()));
                    ((EditText) popup.findView(R.id.et_commit_price_value)).setText(Utils.getAttachCommaFormat(value.floatValue()));
                }
            });

//            ((TextView) popup.findView(R.id.tv_commit_price_value)).setText(Utils.getAttachCommaFormat((float) pdtDetail.getPdt().getPrice()));
            ((EditText) popup.findView(R.id.et_commit_price_value)).setText(Utils.getAttachCommaFormat((float) pdtDetail.getPdt().getPrice()));
            ((TextView) popup.findView(R.id.tv_price_min_value)).setText(Utils.getAttachCommaFormat((float) pdtDetail.getPdt().getPrice() / 2));
            ((TextView) popup.findView(R.id.tv_price_max_value)).setText(Utils.getAttachCommaFormat((float) pdtDetail.getPdt().getPrice()));

            popup.show();
        }

    }

    @OnClick(R.id.ib_back)
    void onBack() {
        goBack();
    }

    @OnClick(R.id.btn_buy)
    void onBuy(View v) {
        hideSnackBars();

        if (pdtDetail.getPdt().getStatus() == PDT_STATUS_TYPE.SOLD.getCode()) {
            return;
        }
        if (Globals.isMyUsrUid(pdtDetail.getSeller().getUsrUid())) {
            Globals.selectedBrand = pdtDetail.getBrand();
            Globals.selectedPdtDetailDto = pdtDetail;

            Intent intent = new Intent(this, WareEditActivity.class);
            startActivityForResult(intent, 2000);

        } else if (pdtDetail.getPdt().getStatus() == PDT_STATUS_TYPE.NORMAL.getCode()) {
            Globals.selectedPdtDetailDto = pdtDetail;
            startActivity(BuyActivity.class, false, 0, 0);
        }
    }

    @OnClick(R.id.rl_security_bar)
    void onSecurity(View v) {
        hideSnackBars();

        final PopupDialog popup = new PopupDialog(getActivity(), R.layout.detail_scroll_protect).setFullScreen().setBackGroundCancelable(true).setCancelable(true);
        TextView tvGuarantee = (TextView) popup.findView(R.id.tv_content_protection1);
        TextView tvEscrow = (TextView) popup.findView(R.id.tv_content_protection2);
        TextView tvDelivery = (TextView) popup.findView(R.id.tv_content_protection3);

        tvGuarantee.setText(guarantee);
        tvEscrow.setText(escrow);
        tvDelivery.setText(delivery);

        popup.setOnClickListener(R.id.btn_protect_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });

        popup.show();
    }

    /**
     * 스토일 등록
     *
     * @param v
     */
    @OnClick({R.id.btn_style_add, R.id.ib_style_reg})
    void onRegisterStyle(View v) {
        hideSnackBars();

        final PopupDialog popup = new PopupDialog(getActivity(), R.layout.detail_style_add_popup).setFullScreen().setCancelable(true);
        popup.setOnClickListener(R.id.btn_capture, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                // 갤러리 연결처리
                Intent intent = new Intent(getActivity(), SellPicActivity.class);
                intent.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_PDT_DETAIL);
                startActivityForResult(intent, 3000);
                popup.dismiss();
            }
        });

        popup.setOnClickListener(R.id.btn_open_img, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                // 갤러리 연결처리
                GalleryFragment galleryFragment = new GalleryFragment();
                galleryFragment.fromPage = SellPicActivity.FROM_PAGE_PDT_DETAIL;
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                galleryFragment.show(ft, "GalleryFragment");

                popup.dismiss();
            }
        });

        popup.setOnClickListener(R.id.btn_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });

        popup.show();
    }

    @OnClick(R.id.rl_likebar)
    void onLike(View v) {
        requestPdtLike(v);
    }

    @OnClick(R.id.rl_sharebar)
    void onShare(View v) {
        hideSnackBars();

        final PopupDialog popup = new PopupDialog(getActivity(), R.layout.detail_item).setFullScreen().setBackGroundCancelable(true).setCancelable(true);

        int ids[] = {R.id.btn_share_selluv, R.id.btn_share_instagram, R.id.rl_blog, R.id.rl_facebook, R.id.rl_twitter, R.id.rl_kakao};
        for (int i = 0; i < ids.length; i++) {
            View btn = popup.findView(ids[i]);
            Animation anim = AnimationUtils.loadAnimation(Utils.getApp().getApplicationContext(), R.anim.fade_in);
            anim.setStartOffset(i * 100);
            btn.startAnimation(anim);
        }

        popup.setOnClickListener(R.id.btn_share_selluv, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                putPdtWish();
                popup.dismiss();
            }
        });
        popup.setOnClickListener(R.id.btn_share_instagram, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                String[] content = getSNSShareContent();
                sendShareInsta(content[0], content[1]);
            }
        });

        popup.setOnClickListener(R.id.rl_blog, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                String[] content = getSNSShareContent();
                try {
                    sendShareNaver(content[0], content[1]);
                } catch (Exception e) {

                }
            }
        });
        popup.setOnClickListener(R.id.rl_facebook, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                String[] content = getSNSShareContent();
                sendShareFacebook(content[0], content[1]);
            }
        });
        popup.setOnClickListener(R.id.rl_twitter, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                String[] content = getSNSShareContent();
                sendShareTwitter(content[0], content[1]);
            }
        });
        popup.setOnClickListener(R.id.rl_kakao, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                String[] content = getSNSShareContent();
                sendShareKakao(content[0], content[1]);
            }
        });
        popup.setOnClickListener(R.id.rl_style_scroll_protect, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });

        popup.show();
    }

    @OnClick(R.id.rl_like_CountBar)
    void onLikeCountBar(View v) {
        Intent intent = new Intent(this, LikeActivity.class);
        intent.putExtra(LikeActivity.PDT_UID, pdtUid);
        startActivity(intent);
    }

    @OnClick(R.id.rl_share_CountBar)
    void onShareCountBar(View v) {
        Intent intent = new Intent(this, IitemActivity.class);
        intent.putExtra(IitemActivity.PDT_UID, pdtUid);
        startActivity(intent);
    }

    @OnClick({R.id.rl_talk_CountBar, R.id.rl_talkbar})
    void onTalkCountBar(View v) {
        Intent intent = new Intent(this, CommentActivity.class);
        intent.putExtra(CommentActivity.PDT_UID, pdtUid);
        startActivity(intent);
    }

    @OnClick(R.id.btn_more)
    void onComment(View v) {

        snackbar = Snackbar.make(getWindow().getDecorView(), "", Snackbar.LENGTH_INDEFINITE);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
        layout.setPadding(0, 0, 0, 0);//set padding to 0
        View view = LayoutInflater.from(this).inflate(R.layout.action_sheet3, null);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layout.addView(view, params);
        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        view.findViewById(R.id.btn_iitem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putPdtWish();
                snackbar.dismiss();
            }
        });
        view.findViewById(R.id.btn_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, pdtDetail.getShareUrl());
                startActivity(Intent.createChooser(i, "공유"));

                snackbar.dismiss();
            }
        });
        view.findViewById(R.id.btn_report).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
                subSnackbar = Snackbar.make(getWindow().getDecorView(), "", Snackbar.LENGTH_INDEFINITE);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) subSnackbar.getView();
                subSnackbar.getView().setBackgroundColor(Color.TRANSPARENT);
                layout.setPadding(0, 0, 0, 0);//set padding to 0
                View view = LayoutInflater.from(DetailActivity.this).inflate(R.layout.action_sheet4, null);
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                layout.addView(view, params);
                view.findViewById(R.id.btn_imitation).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showLieReport(DetailActivity.this);
                        subSnackbar.dismiss();
                    }
                });
                view.findViewById(R.id.btn_etc).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showEtcReport(DetailActivity.this);
                        subSnackbar.dismiss();
                    }
                });
                view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        subSnackbar.dismiss();
                    }
                });
                subSnackbar.show();
            }
        });

        snackbar.show();
    }


    private boolean isInstalledPackage(String packageName) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");

        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(intent, 0);
        if (resInfo.isEmpty()) {
            return false;
        }

        List<Intent> shareIntentList = new ArrayList<Intent>();
        boolean isShare = false;
        for (ResolveInfo info : resInfo) {
            Intent shareIntent = (Intent) intent.clone();

            if (info.activityInfo.packageName.toLowerCase().equals(packageName)) {
                isShare = true;
                break;
            }
        }

        return isShare;
    }

    private String[] getSNSShareContent() {
        String content = "SELLUV앱을 사용해보세요.";
        String url = pdtDetail.getShareUrl();
        content = String.format(content, url);

        String[] ret = new String[2];
        ret[0] = content;
        ret[1] = url;

        return ret;
    }

    private void sendShareKakao(String text, String url) {
        if (isInstalledPackage("com.kakao.talk") == false) {
            Utils.showToast(this, getString(R.string.install_kakao));
        } else {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setPackage("com.kakao.talk");
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, text);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(intent, "Share via"));
        }
    }

    private void sendShareFacebook(String text, String url) {
        boolean facebookAppFound = false;
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(url));

        PackageManager pm = getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        for (final ResolveInfo app : activityList) {
            if ((app.activityInfo.packageName).contains("com.facebook.katana")) {
                final ActivityInfo activityInfo = app.activityInfo;
                final ComponentName name = new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setComponent(name);
                facebookAppFound = true;
                break;
            }
        }
        if (!facebookAppFound) {
            //String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + url;
            //shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
            Utils.showToast(this, getString(R.string.install_facebook));
            return;
        }

        startActivity(shareIntent);
    }

    private void sendShareInsta(String text, String url) {
        if (isInstalledPackage("com.instagram.android") == false) {
            Utils.showToast(this, getString(R.string.install_instagram));
        } else {
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("video/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, url);
            shareIntent.setPackage("com.instagram.android");
            startActivity(shareIntent);
        }
    }

    private void sendShareNaver(String text, String url) throws UnsupportedEncodingException {

        PackageManager pm = getPackageManager();
        try {

            String naverPackage = "com.nhn.android.search";
            pm.getApplicationIcon(naverPackage).getClass();

            String utf8Url = new String(url.getBytes(), StandardCharsets.UTF_8);
            String utf8text = new String(text.getBytes(), StandardCharsets.UTF_8);

            String strurl = String.format("http://share.naver.com/web/shareView.nhn?url=%s&title=%s", utf8Url, utf8text);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse(strurl));
            startActivity(intent);

        } catch (Exception e) {
            Utils.showToast(this, getString(R.string.install_naver));
        }
    }

    private void sendShareTwitter(String text, String url) {

        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("video/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, url);
        final PackageManager pm = getPackageManager();
        final List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        boolean found = false;
        for (final ResolveInfo app : activityList) {
            if ("com.twitter.composer.SelfThreadComposerActivity".equals(app.activityInfo.name)) {
                found = true;
                final ActivityInfo activity = app.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                this.getApplicationContext().startActivity(shareIntent);
                break;
            }
        }
        if (!found) {
            Utils.showToast(this, getString(R.string.install_twitter));
        }
    }

    public String loadMessage(int index) {
        String retStr = "";
        switch (index) {
            case 1:
                retStr = "배송비가 없습니다.";
                break;
//            case 2:
//                retStr = "컨디션설명 토스트입니다.";
//                break;
            case 3:
                retStr = "상품택 지원합니다..";
                break;
            case 4:
                retStr = "게런티 지원합니다..";
                break;
            case 5:
                retStr = "영수증 지원합니다..";
                break;
            case 6:
                retStr = "여가부분품 지원합니다..";
                break;
            case 7:
                retStr = "브랜드박스 지원합니다..";
                break;
            case 8:
                retStr = "더스트백 지원합니다..";
                break;
//            case 9:
//                retStr = "ETC설명 토스트입니다.";
//                break;
            default:
                break;
        }
        return retStr;
    }

    LayoutInflater layoutInflater;
    //public ViewPosition position;
    public WareListAdapter adapter = null;
    ArrayList<View> pagerViews = new ArrayList<>();
    DetailStyleAdapter styleAdapter;
    public static int keyboardHeight;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        pdtUid = getIntent().getIntExtra(PDT_UID, 0);
        if (Constants.IS_TEST)
            Utils.showToast(this, "상품UID : " + pdtUid);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        BarUtils.setStatusBarAlpha(this, 200);
        layoutInflater = LayoutInflater.from(getActivity());
        getPdtDetail();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2000) {
            //상품수정
            if (resultCode == RESULT_OK) {
                getPdtDetail();
            } else if (resultCode == 999) {
                onNego(null);
            }
        } else if (requestCode == 3000 && data != null) {
            //카메라 호출
            String[] imgFiles = data.getStringArrayExtra(Constants.IMAGES);
            String[] arrPhotos = data.getStringArrayExtra(Constants.IMAGES_NAMES);
            if (imgFiles != null && imgFiles.length > 0) {
                cmdRegStyles(arrPhotos);
            }
        }
    }

    public void refresh() {
        if (pdtDetail == null)
            return;

        if (!TextUtils.isEmpty(pdtDetail.getSeller().getProfileImg())) {
            Glide.with(this).load(pdtDetail.getSeller().getProfileImg())
                    .apply(new RequestOptions().placeholder(android.R.color.transparent).error(R.drawable.ic_user_default).circleCrop()).into(iv_photo);
            Glide.with(this).load(pdtDetail.getSeller().getProfileImg())
                    .apply(new RequestOptions().placeholder(android.R.color.transparent).error(R.drawable.ic_user_default).circleCrop()).into(iv_user_photo);
        } else {
            Glide.with(this).load(R.drawable.ic_user_default).into(iv_photo);
            Glide.with(this).load(R.drawable.ic_user_default).into(iv_user_photo);
        }
        user_name.setText(pdtDetail.getSeller().getUsrNckNm());
        tv_user_name.setText(pdtDetail.getSeller().getUsrNckNm());
        if (pdtDetail.getEdited()) {
            active_time.setText(Utils.getDiffTime(pdtDetail.getPdt().getEdtTime().getTime()) + getString(R.string.modified));
        } else {
            active_time.setText(Utils.getDiffTime(pdtDetail.getPdt().getEdtTime().getTime()));
        }

        item_name.setText(pdtDetail.getPdtTitle());
        if (pdtDetail.getPdtStyleList() != null && pdtDetail.getPdtStyleList().size() > 0) {
            btn_style.setVisibility(View.VISIBLE);
        } else {
            btn_style.setVisibility(View.GONE);
        }
        if (pdtDetail.getPdt().getOriginPrice() > pdtDetail.getPdt().getPrice()) {
            item_old_price.setText("￦" + Utils.getAttachCommaFormat(pdtDetail.getPdt().getOriginPrice()));
            item_old_price.setPaintFlags(Paint.ANTI_ALIAS_FLAG | Paint.STRIKE_THRU_TEXT_FLAG);
            item_old_price.setTextColor(getResources().getColor(R.color.color_b5b5b5));
            item_new_price.setText("￦" + Utils.getAttachCommaFormat(pdtDetail.getPdt().getPrice()));
            item_lower_percent.setText(pdtDetail.getSalePercent() + "%");
        } else {
            item_old_price.setText("￦" + Utils.getAttachCommaFormat(pdtDetail.getPdt().getPrice()));
            item_old_price.setPaintFlags(Paint.ANTI_ALIAS_FLAG);
            item_old_price.setTextColor(getResources().getColor(android.R.color.black));
            item_new_price.setVisibility(View.GONE);
            item_lower_percent.setVisibility(View.GONE);
            iv_price_lower.setVisibility(View.GONE);
        }
        item_size.setText(pdtDetail.getPdt().getPdtSize());

        boolean showSizeGuideBtn = true;
        if (pdtDetail.getPdt().getCategoryUid() < 1)
            showSizeGuideBtn = false;

        String categoryName = "";
        for (int i = 0; i < pdtDetail.getCategoryList().size(); i++) {
            categoryName = categoryName + pdtDetail.getCategoryList().get(i).getCategoryName();
        }

        if ((categoryName.contains("남성") && (categoryName.contains("의류") || categoryName.contains("신발")))
                || (categoryName.contains("여성") && (categoryName.contains("의류") || categoryName.contains("신발")))
                || categoryName.contains("키즈") || categoryName.contains("베이비") || categoryName.contains("틴")) {
            showSizeGuideBtn = true;
        } else {
            showSizeGuideBtn = false;
        }

        btnSizeGuide.setVisibility(showSizeGuideBtn ? View.VISIBLE : View.INVISIBLE);
        btnSizeGuide.setOnClickListener(new View.OnClickListener() { //사이즈 안내
            @Override
            public void onClick(View v) {
                if (pdtDetail.getPdt().getCategoryUid() < 1)
                    return;

                String categoryName = "";
                for (int i = 0; i < pdtDetail.getCategoryList().size(); i++) {
                    categoryName = categoryName + pdtDetail.getCategoryList().get(i).getCategoryName();
                }

                if (categoryName.contains("남성"))
                    imgID = R.drawable.size_guide_man;

                if (categoryName.contains("여성"))
                    imgID = R.drawable.size_guide_woman;

                if (categoryName.contains("키즈"))
                    imgID = R.drawable.size_guide_kids;

                if (categoryName.contains("베이비"))
                    imgID = R.drawable.size_guide_baby;

                if (categoryName.contains("틴"))
                    imgID = R.drawable.size_guide_teen;

                Intent intent = new Intent(DetailActivity.this, GuidImagePopup.class);
                intent.putExtra(Constants.POPUP_IMAGE, imgID);
                startActivity(intent);
            }
        });

        tv_like_cnt.setText(String.valueOf(pdtDetail.getPdtLikeCount()));
        like_cnt.setText(String.valueOf(pdtDetail.getPdtLikeCount()));
        if (pdtDetail.getPdtLikeStatus()) {
            like_cnt.setTextColor(getResources().getColor(R.color.color_fe263b));
        } else {
            like_cnt.setTextColor(getResources().getColor(R.color.color_ffffff_95));
        }

        tv_item_cnt.setText(String.valueOf(pdtDetail.getPdtWishCount()));
        share_cnt.setText(String.valueOf(pdtDetail.getPdtWishCount()));
        tv_article_cnt.setText(String.valueOf(pdtDetail.getPdtReplyCount()));
        talk_cnt.setText(String.valueOf(pdtDetail.getPdtReplyCount()));

        tv_item_group.setText(pdtDetail.getCategoryList().size() > 0 && pdtDetail.getCategoryList().get(0) != null ? "#" + pdtDetail.getCategoryList().get(0).getCategoryName() : "");
        tv_item_category1.setText(pdtDetail.getCategoryList().size() > 1 && pdtDetail.getCategoryList().get(1) != null ? "#" + pdtDetail.getCategoryList().get(1).getCategoryName() : "");
        if (pdtDetail.getCategoryList().size() > 2 && pdtDetail.getCategoryList().get(2) != null) {
            tv_item_category2.setText("#" + pdtDetail.getCategoryList().get(2).getCategoryName());
        } else {
            tv_item_category2.setVisibility(View.GONE);
        }
        if (pdtDetail.getCategoryList().size() > 3 && pdtDetail.getCategoryList().get(3) != null) {
            tv_item_category3.setText("#" + pdtDetail.getCategoryList().get(3).getCategoryName());
        } else {
            tv_item_category3.setVisibility(View.GONE);
        }
        tv_item_brand.setText("#" + pdtDetail.getBrand().getNameKo());
        tv_item_color.setText("#" + pdtDetail.getPdt().getColorName());
        tv_item_model.setText("#" + pdtDetail.getPdt().getPdtModel());
        tv_item_tag.setText(pdtDetail.getPdt().getTag());

        iv_heart.setBackgroundResource(pdtDetail.getPdtLikeStatus() ? R.drawable.icon_detail_like_active : R.drawable.icon_detail_like);
        iv_share.setBackgroundResource(pdtDetail.getPdtWishStatus() ? R.drawable.ic_detail_ittem_on : R.drawable.ic_detail_ittem_off);

        pagerViews.clear();
        for (int i = 0; i < pdtDetail.getPdt().getPhotoList().size(); i++) {
            ImageView iv = new ImageView(getActivity());
            iv.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            iv.setAdjustViewBounds(true);
            ImageUtils.load(this, pdtDetail.getPdt().getPhotoList().get(i), android.R.color.transparent, R.drawable.img_default, iv);
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideSnackBars();

                    final PopupDialog popup = new PopupDialog((BaseActivity) DetailActivity.this, R.layout.detail_slide).setBlur().setFullScreen();

                    final ViewPager vp_image = (ViewPager) popup.findView(R.id.vp_image);
                    IndicatorView indicator = (IndicatorView) popup.findView(R.id.rl_indicator);
                    Utils.addTouchableImagesAndIndicator(vp_image, pdtDetail.getPdt().getPhotoList(), indicator);
                    popup.setOnCancelClickListener(new PopupDialog.bindOnClick() {
                        @Override
                        public void onClick() {
                            popup.dismiss();
                        }
                    });

                    popup.show();
                }
            });
            pagerViews.add(iv);
        }

        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return pdtDetail.getPdt().getPhotoList().size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == (View) object;
            }

            @NonNull
            @Override
            public Object instantiateItem(ViewGroup collection, int position) {
                collection.addView(pagerViews.get(position));
                return pagerViews.get(position);
            }

            @Override
            public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
                collection.removeView((View) view);
            }
        };
        vp_item_image.setAdapter(mPagerAdapter);
        vp_item_image.setOffscreenPageLimit(pdtDetail.getPdt().getPhotoList().size());

        rl_item_indicator.setCount(pdtDetail.getPdt().getPhotoList().size());
        rl_item_indicator.setPager(vp_item_image);

        if (pdtDetail.getPdt().getStatus() != PDT_STATUS_TYPE.SOLD.getCode()) {
            iv_soldout.setVisibility(View.GONE);
            if (pdtDetail.getPdt().getStatus() != PDT_STATUS_TYPE.NORMAL.getCode()) {
                tv_sleep_status_alert.setVisibility(View.VISIBLE);
            } else {
                tv_sleep_status_alert.setVisibility(View.GONE);
            }
        } else {
            iv_soldout.setVisibility(View.VISIBLE);
            btn_nego.setTextColor(getResources().getColor(R.color.color_666666));
            btn_buy.setTextColor(getResources().getColor(R.color.color_666666));
        }

        if (Globals.isMyUsrUid(pdtDetail.getSeller().getUsrUid())) {
            btn_nego.setText(R.string.delete);
            btn_buy.setText(R.string.modify);
        } else {
            if (pdtDetail.getPdt().getStatus() != PDT_STATUS_TYPE.NORMAL.getCode()) {
                btn_buy.setTextColor(getResources().getColor(R.color.color_666666));
            }
            if (pdtDetail.getPdt().getNegoYn() != 1) {
                btn_nego.setTextColor(getResources().getColor(R.color.color_666666));
            }
        }

        if (TextUtils.isEmpty(pdtDetail.getPdt().getContent())) {
            tv_item_description.setText("상품 설명이 없습니다.");
            btn_detail_description_more.setVisibility(View.GONE);
            tv_item_description.setTextColor(getResources().getColor(R.color.color_b5b5b5));
        } else {
            tv_item_description.setText(pdtDetail.getPdt().getContent());
            tv_item_description.setTextColor(getResources().getColor(R.color.color_333333));
        }

        final TextView postTextView = tv_item_description;
        postTextView.post(new Runnable() {
            @Override
            public void run() {
                if (postTextView.getLineCount() < 2) {
                    btn_detail_description_more.setVisibility(View.GONE);
                } else {
                    postTextView.setMaxLines(2);
                    btn_detail_description_more.setVisibility(View.VISIBLE);
                }
            }
        });

        switch (pdtDetail.getPdt().getPdtCondition()) {
            case 1:
                btn_detail_like.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.list_direct_ic_condition01), null, null);
                btn_detail_like.setText(R.string.new_item);
                break;
            case 2:
                btn_detail_like.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.list_direct_ic_condition02), null, null);
                btn_detail_like.setText(R.string.best);
                break;
            case 3:
                btn_detail_like.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.list_direct_ic_condition03), null, null);
                btn_detail_like.setText(R.string.good);
                break;
            case 4:
                btn_detail_like.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.list_direct_ic_condition04), null, null);
                btn_detail_like.setText(R.string.middle);
                break;

            default:
                break;
        }

        if (!pdtDetail.isFreeSend()) {
            btn_detail_ship.setText(String.format(Locale.getDefault(), "￦%s", Utils.getAttachCommaFormat(pdtDetail.getFreeSendPrice())));
//            btn_detail_ship.setVisibility(View.GONE);
//            rl_other_info_content.removeView(btn_detail_ship);
        }/* else {
            btn_detail_ship.setVisibility(View.VISIBLE);
        }*/

        for (int i = 0; i < 6; i++) {
            if ("0".equals(pdtDetail.getPdt().getComponent().substring(i, i + 1))) {
                switch (i) {
                    case 0:
                        rl_other_info_content.removeView(btn_detail_itemtag);
                        btn_detail_itemtag.setVisibility(View.GONE);
                        break;
                    case 1:
                        btn_detail_guarantee.setVisibility(View.GONE);
                        rl_other_info_content.removeView(btn_detail_guarantee);
                        break;
                    case 2:
                        btn_detail_receipt.setVisibility(View.GONE);
                        rl_other_info_content.removeView(btn_detail_receipt);
                        break;
                    case 3:
                        btn_detail_rest.setVisibility(View.GONE);
                        rl_other_info_content.removeView(btn_detail_rest);
                        break;
                    case 4:
                        btn_detail_brandbox.setVisibility(View.GONE);
                        rl_other_info_content.removeView(btn_detail_brandbox);
                        break;
                    case 5:
                        btn_detail_dustbag.setVisibility(View.GONE);
                        rl_other_info_content.removeView(btn_detail_dustbag);
                        break;
                }
            }
        }

        if (TextUtils.isEmpty(pdtDetail.getPdt().getEtc())) {
            btn_detail_etc.setVisibility(View.GONE);
            rl_other_info_content.removeView(btn_detail_etc);
        } else {
            btn_detail_etc.setVisibility(View.VISIBLE);
        }

        if (pdtDetail.getPdtStyleList().size() < 1) {
            lv_rec_style.setVisibility(View.GONE);
            rl_style_none.setVisibility(View.VISIBLE);
        } else {
            lv_rec_style.setVisibility(View.VISIBLE);
            rl_style_none.setVisibility(View.GONE);
        }

        btn_rel_style.setText("연관 스타일 (" + pdtDetail.getRelationStyleCount() + ")");

        mannerpoint_cnt.setText(String.valueOf(pdtDetail.getSeller().getPoint()));
        seller_item_cnt.setText(String.valueOf(pdtDetail.getSellerItemCount()));
        seller_follow_cnt.setText(String.valueOf(pdtDetail.getSellerFollowerCount()));

        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        lv_rec_style.setLayoutManager(llm);
        lv_rec_style.setNestedScrollingEnabled(false);

        styleAdapter.setData(pdtDetail.getPdtStyleList());
        styleAdapter.notifyDataSetChanged();

        ll_ware_list.removeAllViews();
        for (int i = 0; i < pdtDetail.getSellerPdtList().size(); i++) {
            View view = layoutInflater.inflate(R.layout.item_brand_follow_ware, null);
            final PdtInfoDto ware = pdtDetail.getSellerPdtList().get(i);
            RoundRectCornerImageView iv_ware = (RoundRectCornerImageView) view.findViewById(R.id.iv_ware);
            ImageUtils.load(this, ware.getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_ware);
            ((TextView) view.findViewById(R.id.tv_ware_price)).setText(Utils.getAttachCommaFormat((int) ware.getPrice()));
            ((TextView) view.findViewById(R.id.tv_ware_name)).setText(ware.getBrandName() + " " + ware.getCategoryName());
            iv_ware.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toAnotherPdtDetail(ware.getPdtUid());
                }
            });
            ll_ware_list.addView(view);
        }

        getPdtRecommendedList();
    }

    public void hideSnackBars() {
        if (snackbar != null) {
            snackbar.dismiss();
        }

        if (subSnackbar != null) {
            subSnackbar.dismiss();
        }
    }

    private void loadLayout() {

        if (pdtDetail == null)
            return;

        if (Globals.isMyUsrUid(pdtDetail.getPdt().getUsrUid())) {
            btn_buy.setText(R.string.modify);
        }

        BarUtils.addMarginTopEqualStatusBarHeight(findViewById(R.id.rl_actionbar));

//        final Handler autoHandler = new Handler();
//        final Runnable autoRun = new Runnable() {
//            @Override
//            public void run() {
//                vp_item_image.setCurrentItem(vp_item_image.getCurrentItem() + 1 < vp_item_image.getChildCount() ? vp_item_image.getCurrentItem() + 1 : 0, true);
//                autoHandler.postDelayed(this, Constants.SHOP_BANNER_INTERVAL);
//            }
//        };
//        autoHandler.postDelayed(autoRun, Constants.SHOP_BANNER_INTERVAL);

        final WrapStaggeredGridLayoutManager sglm = new WrapStaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        grid_view.setLayoutManager(sglm);
        DefaultItemAnimator animator = new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {
                return true;
            }
        };
        grid_view.setItemAnimator(animator);
        adapter = new WareListAdapter(this, Constants.DATA_WARE);
        grid_view.setAdapter(adapter);
        grid_view.setNestedScrollingEnabled(false);
        sv_info.setOnScrollChangeListener(new AutoLoadEventDetector());

        styleAdapter = new DetailStyleAdapter(this);
        lv_rec_style.setAdapter(styleAdapter);

        refresh();

        loadLicense(LICENSE_TYPE.GUARANTEE.toString());
        loadLicense(LICENSE_TYPE.ESCROW.toString());
        loadLicense(LICENSE_TYPE.DELIVERY.toString());
    }

    private void loadLicense(String license_type) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<LicenseDao>> genRes = restAPI.otherLicense(Constants.GUEST_TOKEN, license_type);
        genRes.enqueue(new Callback<GenericResponse<LicenseDao>>() {

            @Override
            public void onResponse(Call<GenericResponse<LicenseDao>> call, Response<GenericResponse<LicenseDao>> response) {
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    setLicense(response.body().getData().getContent(), license_type);
                } else {//실패
                    Toast.makeText(DetailActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<LicenseDao>> call, Throwable t) {
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(DetailActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void setLicense(String license, String type) {
        if (type.equals(LICENSE_TYPE.GUARANTEE.toString())) {
            guarantee = license;
        } else if (type.equals(LICENSE_TYPE.ESCROW.toString())) {
            escrow = license;
        } else if (type.equals(LICENSE_TYPE.DELIVERY.toString())) {
            delivery = license;
        }
    }

    public void goBack() {
        finish();
    }

    public class AutoLoadEventDetector implements NestedScrollView.OnScrollChangeListener {

        @Override
        public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            int deltaY = scrollY - oldScrollY;
//            if (deltaY > 10 && isBottomBarShowed) {
//                ll_bottombar.animate().translationY(ll_bottombar.getHeight()).setInterpolator(new AccelerateInterpolator(2)).withEndAction(new Runnable() {
//                    @Override
//                    public void run() {
//                        ll_bottombar.setVisibility(View.GONE);
//                    }
//                }).start();
//                isBottomBarShowed = false;
//            } else if (deltaY < -10 && !isBottomBarShowed) {
//                ll_bottombar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
//                    @Override
//                    public void run() {
//                        ll_bottombar.setVisibility(View.VISIBLE);
//                    }
//                }).start();
//                isBottomBarShowed = true;
//            }

            if (scrollY > 800) {
                if (ib_style_reg != null && !isStyleAddBtnShowed) {
                    ib_style_reg.animate().translationY(-(ib_style_reg.getHeight() + Utils.dpToPixel(getActivity(), 15))).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                        @Override
                        public void run() {
                            ib_style_reg.setVisibility(View.VISIBLE);
                        }
                    }).start();
                    isStyleAddBtnShowed = true;
                }
            } else {
                if (ib_style_reg != null && isStyleAddBtnShowed) {
                    ib_style_reg.animate().translationY(ib_style_reg.getHeight() + Utils.dpToPixel(getActivity(), 15)).setInterpolator(new DecelerateInterpolator(2)).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            ib_style_reg.setVisibility(View.GONE);
                        }
                    }).start();
                    isStyleAddBtnShowed = false;
                }
            }

            /* LoadMore */
            View view = (View) v.getChildAt(v.getChildCount() - 1);

            int diff = (view.getBottom() - (v.getHeight() + v
                    .getScrollY()));

            if (diff == 0) {
                // your pagination code
                getPdtRecommendedList();
            }
        }
    }

    BaseActivity getActivity() {
        return this;
    }

    public static void showEtcReport(final Activity activity) {
        final PopupDialog opopup = new PopupDialog((BaseActivity) activity, R.layout.detail_more_report_etc).setFullScreen().setCancelable(true);
        final View rootView1 = opopup.findView(R.id.ll_style_add_background);
        String types[] = {"광고/스팸", "휴먼 판매자", "직거래 유도", "기타", "취소"};
        ((CustomSpinner) opopup.findView(R.id.spinner2)).initializeStringValues(activity, types, activity.getResources().getString(R.string.notify_reason_select), null, null, 12.5f);

        opopup.setOnClickListener(R.id.btn_report, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                if (activity instanceof CommentActivity) {
                    ((CommentActivity) activity).reportPdt(opopup);
                } else {
                    ((DetailActivity) activity).reportPdt(opopup);
                }

            }
        });

        opopup.setOnClickListener(R.id.btn_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                opopup.dismiss();
                Utils.hideSoftKeyboard(activity);
            }
        });
        opopup.setOnClickListener(R.id.ll_style_add_background, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                Utils.hideSoftKeyboard(activity);
            }
        });

        opopup.show();
        rootView1.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView1.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView1.getRootView().getHeight();
                keyboardHeight = screenHeight - (r.bottom);
                if (rootView1.getRootView().getHeight() - (r.bottom - r.top) > Utils.dpToPixel(rootView1.getContext(), 100)) { // if more than 100 pixels, its probably a keyboard...
                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rootView1.getLayoutParams();
                    lp.topMargin = -keyboardHeight + Utils.dpToPixel(activity, 50);
                    rootView1.setLayoutParams(lp);
                } else {
                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rootView1.getLayoutParams();
                    lp.topMargin = 0;
                    rootView1.setLayoutParams(lp);
                }
            }
        });
    }

    public static void showLieReport(final Activity activity) {

        final PopupDialog dpopup = new PopupDialog((BaseActivity) activity, R.layout.detail_more_report_fake).setFullScreen().setCancelable(true);
        final View rootView = dpopup.findView(R.id.ll_style_add_background);

        SpannableString spannableString = new SpannableString("가장 먼저 정확한 가품 사유를 기재해주신 분께\n 상품이 가품으로 판정될 경우,\n 셀럽 머니 0원을 드립니다.");
        spannableString.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.color_42c2fe)), 51, 52, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ((TextView) dpopup.findView(R.id.tv_description)).setText(spannableString);

        ((DetailActivity) activity).getPdtFakeInfo(dpopup);

        dpopup.setOnClickListener(R.id.btn_report, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                ((DetailActivity) activity).reportFakePdt(dpopup);
            }
        });

        dpopup.setOnClickListener(R.id.btn_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                dpopup.dismiss();
                Utils.hideSoftKeyboard(activity);
            }
        });
        dpopup.setOnClickListener(R.id.ll_style_add_background, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                Utils.hideSoftKeyboard(activity);
            }
        });

        dpopup.show();

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();
                keyboardHeight = screenHeight - (r.bottom);
                if (rootView.getRootView().getHeight() - (r.bottom - r.top) > Utils.dpToPixel(rootView.getContext(), 100)) { // if more than 100 pixels, its probably a keyboard...
                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rootView.getLayoutParams();
                    lp.topMargin = -keyboardHeight + Utils.dpToPixel(activity, 40);
                    rootView.setLayoutParams(lp);
                } else {
                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rootView.getLayoutParams();
                    lp.topMargin = 0;
                    rootView.setLayoutParams(lp);
                }
            }
        });
    }

    private void getPdtDetail() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<PdtDetailDto>> genRes = restAPI.pdtDetail(Globals.userToken, pdtUid);

        genRes.enqueue(new TokenCallback<PdtDetailDto>(getActivity()) {
            @Override
            public void onSuccess(PdtDetailDto response) {
                closeLoading();
                pdtDetail = response;

                loadLayout();
            }

            @Override
            public void onFailed(Throwable t) {
                pdtDetail = null;
                closeLoading();
                onBack();
            }
        });
    }

    private void getPdtRecommendedList() {
        if (isRecommendListLoading || isRecommendListLoadingEnd)
            return;

        isRecommendListLoading = true;
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<PdtListDto>>> genRes = restAPI.pdtRecommendedList(Globals.userToken, pdtUid, page);

        genRes.enqueue(new TokenCallback<Page<PdtListDto>>(this) {
            @Override
            public void onSuccess(Page<PdtListDto> response) {
                closeLoading();
                if (page == 0) {
                    adapter.dataList.clear();
                }

                adapter.dataList.addAll(response.getContent());
                adapter.notifyDataSetChanged();

                isRecommendListLoadingEnd = response.isLast();
                isRecommendListLoading = false;
                page++;
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isRecommendListLoading = false;
            }
        });
    }

    private void requestPdtLike(final View hearView) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = pdtDetail.getPdtLikeStatus() ?
                restAPI.unLikePdt(Globals.userToken, pdtUid) :
                restAPI.likePdt(Globals.userToken, pdtUid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {

                Utils.heartAnimation(getActivity(), hearView, !pdtDetail.getPdtLikeStatus());
                pdtDetail.setPdtLikeStatus(!pdtDetail.getPdtLikeStatus());
                if (pdtDetail.getPdtLikeStatus()) {
                    iv_heart.setBackgroundResource(R.drawable.ic_detail_like_active);
                    pdtDetail.setPdtLikeCount(pdtDetail.getPdtLikeCount() + 1);
                    like_cnt.setTextColor(getResources().getColor(R.color.color_fe263b));
                } else {
                    iv_heart.setBackgroundResource(R.drawable.ic_detail_like);
                    pdtDetail.setPdtLikeCount(pdtDetail.getPdtLikeCount() > 0 ? pdtDetail.getPdtLikeCount() - 1 : 0);
                    like_cnt.setTextColor(getResources().getColor(R.color.color_ffffff_95));
                }
                tv_like_cnt.setText(String.valueOf(pdtDetail.getPdtLikeCount()));
                like_cnt.setText(String.valueOf(pdtDetail.getPdtLikeCount()));
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void getBrandModelNames(final CustomSpinner modelSpinner) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();
        Call<GenericResponse<List<String>>> genRes = restAPI.brandModelNames(Globals.userToken, pdtDetail.getBrand().getBrandUid());
        genRes.enqueue(new TokenCallback<List<String>>(getActivity()) {
            @Override
            public void onSuccess(List<String> response) {
                closeLoading();
                modelSpinner.initializeStringValues(DetailActivity.this, response.toArray(new String[response.size()]), getString(R.string.model_select), null, null, 12.5f);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void getCategorySizeData(final CustomSpinner categorySizeSpinner) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();
        Call<GenericResponse<SizeRefDto>> genRes = restAPI.getCategorySize(Globals.userToken, pdtDetail.getPdt().getCategoryUid());
        genRes.enqueue(new TokenCallback<SizeRefDto>(this) {
            @Override
            public void onSuccess(SizeRefDto response) {
                closeLoading();
                String[] sizeArray = new String[0];
                if (response.getSizeType() == 3) {
                    List<String> sizeList = new ArrayList<>();
                    for (int i = 0; i < response.getSizeFirst().size(); i++) {
                        for (int j = 0; j < response.getSizeSecond().get(i).size(); j++) {
                            sizeList.add(response.getSizeFirst().get(i) + " " + response.getSizeSecond().get(i).get(j));
                        }
                    }

                    sizeArray = sizeList.toArray(new String[sizeList.size()]);
                }

                categorySizeSpinner.initializeStringValues(DetailActivity.this, sizeArray, getString(R.string.size_select), null, null, 12.5f);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void insertUserWishAlarm(final PopupDialog popup) {

        CustomSpinner spinner_model = (CustomSpinner) popup.findView(R.id.spinner_model);
        CustomSpinner spinner_size = (CustomSpinner) popup.findView(R.id.spinner_size);
        CustomSpinner spinner_color = (CustomSpinner) popup.findView(R.id.spinner_color);

//        if (spinner_model.getSelectedItemPosition() == 0) {
//            Utils.showToast(this, getString(R.string.select_model));
//            return;
//        }
//
//        if (spinner_size.getSelectedItemPosition() == 0) {
//            Utils.showToast(this, getString(R.string.select_size));
//            return;
//        }
//
//        if (spinner_color.getSelectedItemPosition() == 0) {
//            Utils.showToast(this, getString(R.string.select_color));
//            return;
//        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        UserWishReq userWishReq = new UserWishReq();
        userWishReq.setBrandUid(pdtDetail.getPdt().getBrandUid());
        userWishReq.setCategoryUid(pdtDetail.getPdt().getCategoryUid());
        userWishReq.setPdtGroupType(pdtDetail.getPdt().getPdtGroup() == 1 ? PDT_GROUP_TYPE.MALE :
                pdtDetail.getPdt().getPdtGroup() == 2 ? PDT_GROUP_TYPE.FEMALE : PDT_GROUP_TYPE.KIDS);


        userWishReq.setPdtModel(spinner_model.getSelectedItemPosition() == 0 ? "" : spinner_model.getSelectedItem().toString());
        userWishReq.setPdtSize(spinner_size.getSelectedItemPosition() == 0 ? "" : spinner_size.getSelectedItem().toString());
        userWishReq.setColorName(spinner_color.getSelectedItemPosition() == 0 ? "" : spinner_color.getSelectedItem().toString());

        Call<GenericResponse<UsrWishDao>> genRes = restAPI.insertUserWish(Globals.userToken, userWishReq);
        genRes.enqueue(new TokenCallback<UsrWishDao>(this) {
            @Override
            public void onSuccess(UsrWishDao response) {
                closeLoading();
                Utils.showToast(getActivity(), "알람 등록 완료되었습니다.");
                popup.dismiss();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void putPdtWish() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.putPdtWish(Globals.userToken, pdtUid);
        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(getActivity(), "잇템 완료되었습니다.");

                if (!pdtDetail.getPdtWishStatus()) {
                    pdtDetail.setPdtWishStatus(true);
                    pdtDetail.setPdtWishCount(pdtDetail.getPdtWishCount() + 1);

                    tv_item_cnt.setText(String.valueOf(pdtDetail.getPdtWishCount()));
                    share_cnt.setText(String.valueOf(pdtDetail.getPdtWishCount()));

                    iv_share.setBackgroundResource(pdtDetail.getPdtWishStatus() ? R.drawable.ic_detail_ittem_on : R.drawable.ic_detail_ittem_off);
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    public void reportPdt(final PopupDialog popup) {

        CustomSpinner spinner2 = ((CustomSpinner) popup.findView(R.id.spinner2));
        if (spinner2.getSelectedItemPosition() == 0) {
            Utils.showToast(DetailActivity.this, "신고 사유를 선택해 주세요.");
            return;
        }

        ReportReq reportReq = new ReportReq();
        reportReq.setKind(2);
        reportReq.setTargetUid(pdtUid);
        reportReq.setReportType(REPORT_TYPE.values()[spinner2.getSelectedItemPosition() - 1]);
        reportReq.setContent(((EditText) popup.findView(R.id.txt_report_detail)).getText().toString());

        if (TextUtils.isEmpty(reportReq.getContent())) {
            Utils.showToast(DetailActivity.this, "신고 사유 내용을 작성해 주세요.");
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.reportOther(Globals.userToken, reportReq);
        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(DetailActivity.this, "신고하였습니다.");
                popup.dismiss();
                Utils.hideSoftKeyboard(DetailActivity.this);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    public void reportFakePdt(final PopupDialog popup) {

        ContentReq contentReq = new ContentReq();
        contentReq.setContent(((EditText) popup.findView(R.id.txt_report_detail)).getText().toString());

        if (TextUtils.isEmpty(contentReq.getContent())) {
            Utils.showToast(DetailActivity.this, "가품으로 판단되는 이류룰 기재해 주세요.");
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.repostPdtFake(Globals.userToken, pdtUid, contentReq);
        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(DetailActivity.this, "신고하였습니다.");
                popup.dismiss();
                Utils.hideSoftKeyboard(DetailActivity.this);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    public void getPdtFakeInfo(final PopupDialog popup) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<PdtFakeInfoDto>> genRes = restAPI.getPdtFakeInfo(Globals.userToken, pdtUid);
        genRes.enqueue(new TokenCallback<PdtFakeInfoDto>(this) {
            @Override
            public void onSuccess(PdtFakeInfoDto response) {
                closeLoading();
                ((TextView) popup.findView(R.id.tv_guide1_2)).setText(String.valueOf(response.getTotalCount()));
                SpannableString spannableString = new SpannableString("가장 먼저 정확한 가품 사유를 기재해주신 분께\n 상품이 가품으로 판정될 경우,\n 셀럽 머니 " + Utils.getAttachCommaFormat(response.getReward()) + "원을 드립니다.");
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 51, 51 + Utils.getAttachCommaFormat(response.getReward()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((TextView) popup.findView(R.id.tv_description)).setText(spannableString);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void deletePdt() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.pdtDelete(Globals.userToken, pdtUid);
        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(getActivity(), "삭제되었습니다.");
                goBack();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void toAnotherPdtDetail(int pdtUid) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, pdtUid);
        startActivity(intent);
    }

    private void toCategoryDetail(int pdtGroup, int categoryUid) {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(CategoryActivity.PDT_GROUP, pdtGroup);
        intent.putExtra(CategoryActivity.CATEGORY_UID, categoryUid);
        startActivity(intent);
    }

    public void toCategoryPage(int pdtGroup, int categoryUid, String categoryName) {

        if (Globals.searchReq == null) {
            Globals.searchReq = new SearchReq();
        }
        Globals.initSearchFilters();
        Globals.gKisCategoryName = categoryName;

        Intent intent = new Intent(getActivity(), CategoryActivity.class);
        intent.putExtra(CategoryActivity.PDT_GROUP, pdtGroup);
        intent.putExtra(CategoryActivity.CATEGORY_UID, categoryUid);
        intent.putExtra("PDT_DETAIL", 1000);
        getActivity().startActivity(intent);
    }

    /**
     * 이미지 업로드
     */
    public void uploadImages(ArrayList<String> imageFiles) {
        showLoading();

        MultipartBody.Part[] multipartBody = new MultipartBody.Part[imageFiles.size()];
        for (int i = 0; i < imageFiles.size(); i++) {
            File file = new File(imageFiles.get(i));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("uploadfiles", file.getName(), surveyBody);
            multipartBody[i] = part;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<HashMap<String, String>>>> genRes = restAPI.uploadImages(multipartBody);
        genRes.enqueue(new TokenCallback<List<HashMap<String, String>>>(this) {
            @Override
            public void onSuccess(List<HashMap<String, String>> response) {
                closeLoading();
                //fileName, fileURL
                ArrayList<String> imag_names = new ArrayList<>();
                for (int i = 0; i < response.size(); i++) {
                    imag_names.add(response.get(i).get("fileName"));
                }

                cmdRegStyles(imag_names.toArray(new String[]{}));
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    /**
     * 스타일 이미지 등록..
     *
     * @param photonames
     */
    private void cmdRegStyles(String[] photonames) {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.registerStyles(Globals.userToken, pdtUid, new PdtStyleMultiReq(Arrays.asList(photonames)));
        genRes.enqueue(new TokenCallback<Void>(getActivity()) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(getActivity(), "스타일 등록 되었습니다.");
                getPdtDetail();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
