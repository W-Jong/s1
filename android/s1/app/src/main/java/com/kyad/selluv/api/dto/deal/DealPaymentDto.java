package com.kyad.selluv.api.dto.deal;

import lombok.Data;

@Data
public class DealPaymentDto {
    //("거래정보")
    DealDao deal;
    //("카드간편결제인 경우 빈문자열, 기타 결제URL 웹뷰에서 호출필요함")
    String paymentUrl;
}
