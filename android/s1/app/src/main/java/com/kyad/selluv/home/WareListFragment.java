/**
 * 홈
 */
package com.kyad.selluv.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.DbManager;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.HeaderViewRecyclerAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.api.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.WrapStaggeredGridLayoutManager;
import com.kyad.selluv.fragment.ResistanceDragDistanceConvert;
import com.kyad.selluv.fragment.tips.DefaultTipsHelper;
import com.kyad.selluv.fragment.tips.TipsHelper;
import com.kyad.selluv.top.TopFilterActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.DATA_STYLE;
import static com.kyad.selluv.common.Constants.DATA_WARE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_BRAND;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_CATEGORY;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_FAME;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_FEED;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_NEW;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_PDT_LIKE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_PDT_RELATION_STYLE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_PDT_SAME_MODEL;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_PDT_SAME_TAG;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_PDT_STYLE_LIKE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_SEARCH;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_SHOP;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_STYLE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_THEME;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_USR_PDT;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_USR_STYLE;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_USR_WISH;

public class WareListFragment extends BaseFragment {

    //UI Referencs
    @BindView(R.id.grid_view)
    RecyclerView grid_view;
    @BindView(R.id.swipe_container)
    RecyclerRefreshLayout swipe_container;
    @BindView(R.id.ib_goto_top)
    ImageButton ib_goto_top;
    @BindView(R.id.tv_all_ware_cnt)
    TextView tv_all_ware_cnt;

    //Vars
    View rootView;
    LinearLayout mTabMenu = null;
    ViewGroup appbar = null;
    TextView tv_all_ware;

    private WareListAdapter adapter;
    private HeaderViewRecyclerAdapter mHeaderAdapter;
    private TipsHelper mTipsHelper;

    public String noneTitle = "", noneInfo = "", buttonTitle = "", listInfo = "전체 상품";
    public boolean showFilter = false, showFilterButton = false, mIsLoading = false, parentCollapsable = false;
    int totalCnt = 0, noneImgID = 0;
    int dataType = DATA_WARE;
    int totalScrolledDistance = 0;
    public int paddintTop = -1;
    public static int SHOW_TAB_THRESHOLD = 10;
    private int mScrolledDistance = 0;
    private boolean mTopBarVisible = true;
    private boolean mGoBtnVisible = true;
    public boolean isRefresh = false;
    public boolean isMiniImageShow = true;
    public boolean isLazyLoading = false;

    private int fromWhere = 0;
    private int fragType = 0;
    private long seed = 0;
    private int currentPage = 0;
    private boolean isLast = false;
    private int categoryUid = 1;
    private int brandUid = 0;
    private int pdtUid = 0;
    private int usrUid = 0;
    private int sale = 0;
    private String pdtTag = "";
    private String tabString = "ALL";

    public ArrayList<View> headerViews = new ArrayList<>();
    private View.OnClickListener noneListener = null;
    private final AutoLoadEventDetector mAutoLoadEventDetector = new AutoLoadEventDetector();

    /**
     * 검색 키워드
     */
    private String strKeyword = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ware_list, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        if (!isLazyLoading)
            refresh();
        return rootView;
    }

    @OnClick(R.id.ib_goto_top)
    void onTop() {
        grid_view.smoothScrollToPosition(0);
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (totalScrolledDistance < SHOW_TAB_THRESHOLD) {
                if (appbar != null) {
                    appbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                        @Override
                        public void run() {
                            appbar.setVisibility(View.VISIBLE);
                        }
                    }).start();
                }
            }
            mTopBarVisible = true;
        }
    }

    @Override
    public void onDestroyView() {
        grid_view.removeOnScrollListener(mAutoLoadEventDetector);
        rootView = null;
        super.onDestroyView();
    }

    private void loadLayout() {

        final WrapStaggeredGridLayoutManager sglm = new WrapStaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        grid_view.setLayoutManager(sglm);

//        DefaultItemAnimator animator = new DefaultItemAnimator() {
//            @Override
//            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
//                return true;
//            }
//        };
        grid_view.setItemAnimator(null);


        adapter = new WareListAdapter(this, dataType);
        adapter.isMiniImageShow = isMiniImageShow;
        mHeaderAdapter = new HeaderViewRecyclerAdapter(adapter);
        grid_view.setAdapter(mHeaderAdapter);
        mHeaderAdapter.adjustSpanSize(grid_view);
        if (paddintTop != -1) {
            LinearLayout headerView = new LinearLayout(getActivity());
            headerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, paddintTop));
            headerView.setMinimumHeight(paddintTop);
            mHeaderAdapter.addHeaderView(headerView);
        }

        for (View v : headerViews) {
            mHeaderAdapter.addHeaderView(v);
        }

        if (showFilter || showFilterButton) {
            View vFilter = rootView.findViewById(R.id.rl_filter);
            vFilter.setVisibility(View.VISIBLE);
            ViewGroup vParent = (ViewGroup) vFilter.getParent();
            vParent.removeView(vFilter);
            tv_all_ware = vFilter.findViewById(R.id.tv_all_ware);
            if (showFilterButton) {
                ToggleButton tbFilter = vFilter.findViewById(R.id.btn_filter);
                tbFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        ((BaseActivity) getActivity()).startActivity(TopFilterActivity.class, false, 0, 0);
                        Intent intent = new Intent(((BaseActivity) getActivity()), TopFilterActivity.class);
                        if (fromWhere == Constants.PAGE_CATEGORY)
                            intent.putExtra("page", 1000);
                        else if (fragType == Constants.WIRE_FRAG_SHOP)
                            intent.putExtra("page", Constants.WIRE_FRAG_SHOP);
                        ((BaseActivity) getActivity()).startActivity(intent);
                    }
                });
                tbFilter.setVisibility(View.VISIBLE);
            }
            tv_all_ware.setText(listInfo);

            mHeaderAdapter.addHeaderView(vFilter);
        }

        swipe_container.setNestedScrollingEnabled(true);

        swipe_container.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
//        swipe_container.setAnimateToRefreshDuration(1000);
        if (!parentCollapsable) {
            swipe_container.setRefreshInitialOffset(paddintTop);
            swipe_container.setRefreshTargetOffset(100);
            swipe_container.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    for (View v : headerViews) {
                        v.setVisibility(View.GONE);
                    }
                    isRefresh = true;
                    mTipsHelper.hideHasMore();
                    refresh();
                }
            });
        } else {
            swipe_container.setRefreshing(false);
            swipe_container.stopNestedScroll();
            swipe_container.setRefreshInitialOffset(10000);
            swipe_container.setRefreshTargetOffset(10000);
        }

        mTipsHelper = new DefaultTipsHelper(this);
        grid_view.addOnScrollListener(mAutoLoadEventDetector);

        getRecyclerRefreshLayout().setDragDistanceConverter(new ResistanceDragDistanceConvert(Utils.getScreenHeight(getActivity()), paddintTop));

        getHeaderAdapter().removeAllFooterView();

        mTabMenu = getActivity().findViewById(R.id.ll_bottom);
        if (appbar == null)
            appbar = getActivity().findViewById(R.id.appbar);

        if ("".equals(noneTitle)) {
            if ((dataType & DATA_STYLE) == DATA_STYLE) {
                noneTitle = getString(R.string.none_style_img);
            } else {
                if (fragType == WIRE_FRAG_FEED) {
                    noneTitle = getString(R.string.no_recommended_ware);
                } else if (fragType == WIRE_FRAG_FAME) {
                    noneTitle = getString(R.string.no_popular_ware);
                } else if (fragType == WIRE_FRAG_NEW) {
                    noneTitle = getString(R.string.no_new_ware);
                } else {
                    noneTitle = getString(R.string.no_matching_ware);
                }
            }
        }
    }

    public HeaderViewRecyclerAdapter getHeaderAdapter() {
        return mHeaderAdapter;
    }

    public RecyclerRefreshLayout getRecyclerRefreshLayout() {
        return swipe_container;
    }

    public RecyclerView getRecyclerView() {
        return grid_view;
    }

    public boolean isFirstPage() {
        return adapter.getItemCount() <= 0;
    }

    public void setLoadInterface(String keyword) {
        this.strKeyword = keyword;
        refresh();
    }

    public void setAppbar(ViewGroup appbar) {
        this.appbar = appbar;
    }

    public void setNonePage(int imgID, String title, String info, String buttonTitle, View.OnClickListener listener) {
        noneImgID = imgID;
        noneTitle = title;
        noneInfo = info;
        noneListener = listener;
        this.buttonTitle = buttonTitle;
    }

    public void setCategoryUid(int categoryUid) {
        this.categoryUid = categoryUid;
    }

    public void setBrandUid(int brandUid) {
        this.brandUid = brandUid;
    }

    public void setFromWhere(int where) {
        this.fromWhere = where;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }

    public void setPdtTag(String pdtTag) {
        this.pdtTag = pdtTag;
    }

    public void setTabString(String tabString) {
        this.tabString = tabString;
    }

    public void setTotalCnt(int cnt) {
        totalCnt = cnt;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
        if (adapter != null) adapter.setDataType(dataType);
    }

    public void setFragType(int type) {
        this.fragType = type;
    }

    public void addData(List<PdtListDto> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        adapter.dataList.addAll(list);
    }

    public void addUsrFeedData(List<UsrFeedPdtListDto> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        adapter.dataList.addAll(list);
    }

    public void removeAll() {
        if (adapter == null) return;
        adapter.dataList.clear();
        mHeaderAdapter.notifyDataSetChanged();
    }

    public void refresh() {
        mIsLoading = false;
        if (adapter == null) return;
        if (isFirstPage() && !isRefresh) {
            mTipsHelper.showLoading(true);
        }

        if (appbar != null) {
            appbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                @Override
                public void run() {
                    appbar.setVisibility(View.VISIBLE);
                }
            }).start();
        }

        requestRefresh();
    }

    public void setListTitle(String title) {
        tv_all_ware.setText(title);
    }

    private void requestRefresh() {
        if (mIsLoading)
            return;
        mIsLoading = true;
        adapter.dataList.clear();
        grid_view.getRecycledViewPool().clear();
        isLast = false;

        if (fragType == WIRE_FRAG_THEME) {
            //테마인 경우 직접 데이터 설정함.
            refreshExternalData(new ArrayList<PdtListDto>(), 0);
            return;
        }

        if (fragType == WIRE_FRAG_FAME) {
            this.seed = System.currentTimeMillis();
        }

        this.currentPage = 0;
        requestMore();
    }

    private void requestMore() {

        if (fragType == WIRE_FRAG_SEARCH) {
            requestSearch(currentPage);
        } else if (fragType == WIRE_FRAG_SHOP) {
            requestSearchDetail(currentPage);
        } else if (fragType == WIRE_FRAG_STYLE) {
            requestStyleList(currentPage, dataType);
        } else {
            requestHomeList(currentPage, this.seed);
        }
    }

    protected void requestComplete() {
        mIsLoading = false;

        mTipsHelper.hideEmpty();
        mTipsHelper.hideLoading();
    }

    public void refreshExternalData(List<PdtListDto> pdtListDtos, int count) {
        if (pdtListDtos == null)
            return;
        addData(pdtListDtos);
        if (adapter.dataList.isEmpty()) {
            mTipsHelper.showEmpty(noneTitle, noneInfo, noneImgID, buttonTitle, noneListener);
        } else {
            getHeaderAdapter().notifyDataSetChanged();
            requestComplete();
            mTipsHelper.showHasMore();
        }

        tv_all_ware_cnt.setText("(" + count + ")");
    }

    private void closeProgres() {
        closeLoading();

        if (isRefresh) {
            for (View v : headerViews) {
                v.setVisibility(View.VISIBLE);
            }
        }

        isRefresh = false;

        mTipsHelper.hideLoading();
        mTipsHelper.hideHasMore();

        swipe_container.setRefreshing(false);
    }

    private void requestHomeList(final int page, long seed) {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        Call genRes = null;
        if (fragType == WIRE_FRAG_FEED) {
            adapter.fragType = WIRE_FRAG_FEED;
            genRes = restAPI.homeFeedList(Globals.userToken, page);
            if ((dataType & DATA_STYLE) == DATA_STYLE) {
                genRes = restAPI.homeFeedStyleList(Globals.userToken, page);
            }
        } else if (fragType == WIRE_FRAG_NEW) {
            adapter.fragType = WIRE_FRAG_NEW;
            genRes = restAPI.homeNewList(Globals.userToken, page);
            if ((dataType & DATA_STYLE) == DATA_STYLE) {
                genRes = restAPI.homeNewStyleList(Globals.userToken, page);
            }
        } else if (fragType == WIRE_FRAG_FAME) {
            adapter.fragType = WIRE_FRAG_FAME;
            genRes = restAPI.homeFameList(Globals.userToken, page, seed);
            if ((dataType & DATA_STYLE) == DATA_STYLE) {
                genRes = restAPI.homeFameStyleList(Globals.userToken, page, seed);
            }
        } else if (fragType == WIRE_FRAG_CATEGORY) {
            genRes = restAPI.categoryPdtList(Globals.userToken, categoryUid, page, brandUid);
        } else if (fragType == WIRE_FRAG_BRAND) {
            genRes = restAPI.brandPdtList(Globals.userToken, brandUid, tabString, page);
        } else if (fragType == WIRE_FRAG_PDT_RELATION_STYLE) {
            genRes = restAPI.pdtRelationStyleList(Globals.userToken, pdtUid, page);
        } else if (fragType == WIRE_FRAG_PDT_SAME_MODEL) {
            genRes = restAPI.pdtRelationSameModelList(Globals.userToken, pdtUid, page);
        } else if (fragType == WIRE_FRAG_PDT_SAME_TAG) {
            genRes = restAPI.pdtRelationSameTagList(Globals.userToken, pdtUid, pdtTag, page);
        } else if (fragType == WIRE_FRAG_USR_PDT) {
            genRes = restAPI.getUsrPdtList(Globals.userToken, usrUid, page);
        } else if (fragType == WIRE_FRAG_USR_WISH) {
            genRes = restAPI.getUsrWishList(Globals.userToken, usrUid, page);
        } else if (fragType == WIRE_FRAG_USR_STYLE) {
            genRes = restAPI.getUsrStyleList(Globals.userToken, usrUid, page);
        } else if (fragType == WIRE_FRAG_PDT_LIKE) {
            genRes = restAPI.getLikeList(Globals.userToken, sale, page);
        } else if (fragType == WIRE_FRAG_PDT_STYLE_LIKE) {
            genRes = restAPI.getLikeStyleList(Globals.userToken, sale, page);
        }

        if (genRes == null) {
            closeLoading();
            return;
        }

        if (fragType == WIRE_FRAG_FEED) {
            genRes.enqueue(new TokenCallback<Page<UsrFeedPdtListDto>>(getActivity()) {
                @Override
                public void onSuccess(Page<UsrFeedPdtListDto> response) {
                    closeProgres();

                    if (page == 0) {
                        removeAll();
                    }

                    List<UsrFeedPdtListDto> responseData = response.getContent();
                    isLast = response.isLast();

                    if (responseData.size() == 0 && adapter.dataList.isEmpty()) {
                        mTipsHelper.showEmpty(noneTitle, noneInfo, noneImgID, buttonTitle, noneListener);
                    } else {
                        addUsrFeedData(responseData);
                        getHeaderAdapter().notifyDataSetChanged();
                        requestComplete();
                        mTipsHelper.showHasMore();
                    }
                }

                @Override
                public void onFailed(Throwable t) {
                    closeProgres();
                }
            });
        } else {
            genRes.enqueue(new TokenCallback<Page<PdtListDto>>(getActivity()) {
                @Override
                public void onSuccess(Page<PdtListDto> response) {
                    closeProgres();

                    if (page == 0) {
                        removeAll();
                    }

                    List<PdtListDto> responseData = response.getContent();
                    isLast = response.isLast();

                    if (responseData.size() == 0 && adapter.dataList.isEmpty()) {
                        mTipsHelper.showEmpty(noneTitle, noneInfo, noneImgID, buttonTitle, noneListener);
                    } else {
                        addData(responseData);
                        getHeaderAdapter().notifyDataSetChanged();
                        requestComplete();
                        mTipsHelper.showHasMore();
                    }

                    tv_all_ware_cnt.setText("(" + response.getTotalElements() + ")");
                }

                @Override
                public void onFailed(Throwable t) {
                    closeProgres();
                }
            });
        }
    }

    /**
     * 상세검색
     *
     * @param page
     */
    private void requestSearchDetail(final int page) {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        //Call<GenericResponse<Page<PdtListDto>>> genRes = restAPI.searchPdt(Globals.userToken, strKeyword, page);
        Call<GenericResponse<Page<PdtListDto>>> genRes = restAPI.searchDetail(Globals.userToken, Globals.searchReq, page);
        genRes.enqueue(new TokenCallback<Page<PdtListDto>>(getActivity()) {
            @Override
            public void onSuccess(final Page<PdtListDto> response) {
                closeProgres();

                if (page == 0) {
                    removeAll();
                }

                isLast = response.isLast();
                refreshExternalData(response.getContent(), response.getNumberOfElements());
            }

            @Override
            public void onFailed(Throwable t) {
                closeProgres();
            }
        });

    }

    /**
     * 검색
     *
     * @param page
     */
    private void requestSearch(final int page) {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Page<PdtListDto>>> genRes = restAPI.searchPdt(Globals.userToken, strKeyword, page);

        genRes.enqueue(new TokenCallback<Page<PdtListDto>>(getActivity()) {
            @Override
            public void onSuccess(final Page<PdtListDto> response) {
                closeProgres();

                if (page == 0) {
                    removeAll();
                }
                //최근 검색 등록
                DbManager.saveWord(strKeyword, response.getNumberOfElements());

                isLast = response.isLast();
                refreshExternalData(response.getContent(), response.getNumberOfElements());
            }

            @Override
            public void onFailed(Throwable t) {
                closeProgres();
            }
        });

    }

    private void requestStyleList(final int page, int dataType) {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        String pdtGroup = "";
        if (dataType == 1 || (dataType ^ DATA_STYLE) == 1) {
            pdtGroup = "MALE";
        } else if (dataType == 2 || (dataType ^ DATA_STYLE) == 2) {
            pdtGroup = "FEMALE";
        } else if (dataType == 3 || (dataType ^ DATA_STYLE) == 3) {
            pdtGroup = "KIDS";
        }

        Call genRes;
        if (dataType != DATA_STYLE) {
            genRes = restAPI.pdtStyleList(Globals.userToken, pdtGroup, page);
        } else {
            genRes = restAPI.pdtStyleAllList(Globals.userToken, page);
        }

        genRes.enqueue(new TokenCallback<Page<PdtListDto>>(getActivity()) {
            @Override
            public void onSuccess(Page<PdtListDto> response) {
                closeProgres();

                if (page == 0) {
                    removeAll();
                }

                List<PdtListDto> responseData = response.getContent();
                isLast = response.isLast();

                if (responseData.size() == 0 && adapter.dataList.isEmpty()) {
                    mTipsHelper.showEmpty(noneTitle, noneInfo, noneImgID, buttonTitle, noneListener);
                } else {
                    addData(responseData);
                    getHeaderAdapter().notifyDataSetChanged();
                    requestComplete();
                    mTipsHelper.showHasMore();
                }

                tv_all_ware_cnt.setText("(" + response.getTotalElements() + ")");
            }

            @Override
            public void onFailed(Throwable t) {
                closeProgres();
            }
        });
    }

    public class AutoLoadEventDetector extends RecyclerView.OnScrollListener {

        private static final int HIDE_THRESHOLD = 10;

        @Override
        public void onScrolled(RecyclerView view, int dx, int dy) {
            totalScrolledDistance += dy;
            /* Hide/Show BottomBar */
            if (mScrolledDistance > HIDE_THRESHOLD) {
                if (mGoBtnVisible) {
                    if (ib_goto_top != null) {
                        ib_goto_top.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                ib_goto_top.setVisibility(View.VISIBLE);
                            }
                        }).start();
                    }
                    if (mTabMenu != null) {
                        mTabMenu.animate().translationY(mTabMenu.getHeight()).setInterpolator(new AccelerateInterpolator(2)).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                mTabMenu.setVisibility(View.GONE);
                            }
                        }).start();
                    }
                    mGoBtnVisible = false;
                }

                if (!parentCollapsable) {
                    if (mTopBarVisible) {
                        if (appbar != null) {
                            appbar.animate().translationY(-appbar.getHeight()).setInterpolator(new AccelerateInterpolator(2)).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    appbar.setVisibility(View.GONE);
                                }
                            }).start();
                        }
                        mTopBarVisible = false;
                    }
                }
                mScrolledDistance = 0;
            } else if (mScrolledDistance < -HIDE_THRESHOLD) {
                if (!mGoBtnVisible) {
                    if (ib_goto_top != null) {
                        ib_goto_top.animate().translationY(ib_goto_top.getHeight() + Utils.dpToPixel(getActivity(), 15)).setInterpolator(new DecelerateInterpolator(2)).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                ib_goto_top.setVisibility(View.GONE);
                            }
                        }).start();
                    }
                    if (mTabMenu != null) {
                        mTabMenu.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                mTabMenu.setVisibility(View.VISIBLE);
                            }
                        }).start();
                    }
                    mGoBtnVisible = true;
                }
                if (!parentCollapsable) {
                    if (!mTopBarVisible) {
                        if (appbar != null) {
                            appbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    appbar.setVisibility(View.VISIBLE);
                                }
                            }).start();
                        }
                        mTopBarVisible = true;
                    }
                }

                mScrolledDistance = 0;
            }

            if (mTopBarVisible || (mGoBtnVisible && dy > 0) || (!mGoBtnVisible && dy < 0)) {
                mScrolledDistance += dy;
            }

            /* LoadMore */
            RecyclerView.LayoutManager manager = view.getLayoutManager();
            if (manager.getChildCount() > 0) {
                int count = manager.getItemCount();
                int last = ((RecyclerView.LayoutParams) manager
                        .getChildAt(manager.getChildCount() - 1).getLayoutParams()).getViewAdapterPosition();

                if (last == count - 1 && !mIsLoading) {
                    if (mIsLoading || isLast == true) {
                        return;
                    }

                    mIsLoading = true;
                    currentPage++;

                    requestMore();
                }
            }
        }
    }
}
