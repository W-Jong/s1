package com.kyad.selluv.api.dto.search;


import lombok.Data;

@Data
public class SearchWordDto {
    //@ApiModelProperty("검색어")
    private String word;
    //@ApiModelProperty("조회수")
    private long count;
}
