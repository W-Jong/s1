package com.kyad.selluv.top;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.home.WareListFragment;

import butterknife.BindView;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.ACT_SEARCH_CATEGORY;
import static com.kyad.selluv.common.Constants.ACT_SEARCH_DETAIL;

public class SearchResultActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_title)
    TextView tv_title;

    WareListFragment fg_ware_list;

    //Variables
    private String searchKeyWord = "";
    private String title = "";
    private int searchResCount = 0;
    private int fromWhere = Constants.WIRE_FRAG_SEARCH;  //어느 페지에서 왔는가 (검색, shop)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        fromWhere = getIntent().getIntExtra(Constants.FROM_PAGE, Constants.WIRE_FRAG_SEARCH);

        if (fromWhere == Constants.WIRE_FRAG_SHOP) {
            title = getIntent().getStringExtra(Constants.PAGE_TITLE);
            Globals.categoryFilterName = Globals.currentShopperTypeIdx == 0? "남성" : Globals.currentShopperTypeIdx == 1? "여성" : "키즈";
        } else {
            searchKeyWord = getIntent().getStringExtra(Constants.SEARCH_WORD);
            Globals.searchReq.setKeyword(searchKeyWord);
        }

        ////shop tab과 brand tab을 누르면 검색조건들을 초기화하도록 한다.
        /*
        if (Globals.searchReq == null) {
            Globals.searchReq = new SearchReq();
        }
        Globals.initSearchFilters();
        */


        loadLayout();

        IntentFilter f = new IntentFilter();
        f.addAction(ACT_SEARCH_DETAIL);
        f.addAction(ACT_SEARCH_CATEGORY);
        registerReceiver(mBroadcastReceiver, new IntentFilter(f));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACT_SEARCH_DETAIL)) {
                fg_ware_list.setFragType(Constants.WIRE_FRAG_SEARCH);
                fg_ware_list.setLoadInterface(searchKeyWord);
            } if (action.equals(ACT_SEARCH_CATEGORY)) {
                fg_ware_list.setFragType(Constants.WIRE_FRAG_SHOP);
                fg_ware_list.setLoadInterface("");
            }
        }
    };

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    /**
     * Top Like
     */
    @OnClick(R.id.ib_right)
    void onRight() {
        startActivity(TopLikeActivity.class, true, 0, 0);
    }


    private void loadLayout() {

        fg_ware_list = new WareListFragment();
        fg_ware_list.showFilterButton = true;
        fg_ware_list.showFilter = true;

        fg_ware_list.listInfo = "검색 결과";
        fg_ware_list.setTotalCnt(searchResCount);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fg_ware_list, fg_ware_list);
        transaction.commit();

        if (fromWhere == Constants.WIRE_FRAG_SEARCH) {
            tv_title.setText(searchKeyWord);
            fg_ware_list.setFragType(Constants.WIRE_FRAG_SEARCH);
        } else {
            tv_title.setText(title);
            fg_ware_list.setFragType(Constants.WIRE_FRAG_SHOP);
        }

        fg_ware_list.setLoadInterface(searchKeyWord);
    }
}
