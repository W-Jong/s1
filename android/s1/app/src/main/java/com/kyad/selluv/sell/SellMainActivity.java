/**
 * 홈
 */
package com.kyad.selluv.sell;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.request.PdtCreateReq;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.sell.fragment.SellCategorySelectFragment;
import com.kyad.selluv.sell.fragment.SellShopperFragment;

import butterknife.BindView;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.FRAG_IN_ANIM_DELAY;
import static com.kyad.selluv.common.Constants.VIEW_BOUNCE_ANIM_DURATION;


public class SellMainActivity extends BaseActivity {

    @BindView(R.id.rl_main)
    RelativeLayout rl_main;
    @BindView(R.id.rl_shopper)
    RelativeLayout rl_shopper;
    @BindView(R.id.rl_category)
    RelativeLayout rl_category;

    private FragmentManager fm;
    private FragmentTransaction transaction;

    public static int sellType = 0; //0 : 직접 판매하기 1: 발렛 서비스

    private SellShopperFragment sellShopperFramment = null;
    private SellCategorySelectFragment sellCategorySelectFragment = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sell_main);
        addForSellActivity();

        loadLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200 & resultCode == RESULT_OK) {
            sellType = 1;

            rl_shopper.setVisibility(View.VISIBLE);

            sellShopperFramment = SellShopperFragment.newInstance();
            transaction = fm.beginTransaction();
            transaction.add(R.id.rl_shopper, sellShopperFramment, "SellShopper");
            transaction.commit();
        }
    }

    private void loadLayout() {
        fm = getSupportFragmentManager();
        startEnterAnimation(findViewById(R.id.rl_main));
    }

    @Override
    public void overridePendingTransition(int enterAnim, int exitAnim) {
        enterAnim = R.anim.frag_slide_in_from_top;
        exitAnim = R.anim.frag_slide_out_to_top;
        super.overridePendingTransition(enterAnim, exitAnim);
    }

    private void initPdtRegInfo() {
        if (Globals.pdtCreateReq == null) {
            Globals.pdtCreateReq = new PdtCreateReq();
        } else {
            Globals.pdtCreateReq.init();
        }

        String strUser = "";
        if (Globals.pdtCreateReq == null) {
            return;
        }

        SharedPreferences pref = getSharedPreferences("createpdt",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        strUser = new Gson().toJson(Globals.pdtCreateReq, PdtCreateReq.class);
        editor.putString("PdtCreateReq", strUser).apply();
    }

    /**
     * 직접 판매 버튼
     *
     * @param v
     */
    @OnClick(R.id.btn_sell)
    void onSell(View v) {
        sellType = 0;

        initPdtRegInfo();

        rl_shopper.setVisibility(View.VISIBLE);

        sellShopperFramment = SellShopperFragment.newInstance();
        transaction = fm.beginTransaction();
        transaction.add(R.id.rl_shopper, sellShopperFramment, "SellShopper");
        transaction.commit();

    }

    /**
     * 발렛서비스 이용
     *
     * @param v
     */
    @OnClick(R.id.btn_service)
    void onService(View v) {
        initPdtRegInfo();
        Intent intent = new Intent(this, SellValetActivity.class);
        startActivityForResult(intent, 200);
    }

    @OnClick(R.id.btn_sell_close)
    void onClose(View v) {
        startEndAnimation(findViewById(R.id.rl_main));
    }

    @OnClick(R.id.rl_guide)
    void onGuide(View v) {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.sell_guide);
        startActivity(intent);
    }

    @OnClick(R.id.rl_ahead)
    void onAhead(View v) {

        SharedPreferences pref = getSharedPreferences("createpdt",
                Activity.MODE_PRIVATE);
        String strUser = pref.getString("PdtCreateReq", "");
        if (strUser.isEmpty() == true) {
            return;
        }
        Globals.pdtCreateReq = new Gson().fromJson(strUser, PdtCreateReq.class);


        //포토리스트만 따져도 사진완료단게까지 했다는것을 알수 있음(이어서 등록하기 가능)
        if (Globals.pdtCreateReq.getPhotos().size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.continue_register))
                    .setMessage(getString(R.string.reg_pdt_continue))
                    .setPositiveButton(getString(R.string.str_continue), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // 상품정보부터 계속 등록하기
                            Intent intent = new Intent(SellMainActivity.this, SellInfoActivity.class);
                            intent.putExtra(Constants.THUMB_IMAGE_FILE, Globals.pdtCreateReq.getPhotos().get(0));
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        } else {
            Utils.showToast(this, getString(R.string.empty_reg_working));
        }
    }

    public void startEnterAnimation(View rootView) {
        RelativeLayout view = ((RelativeLayout) rootView);
        int cnt = 0;
        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);
            cnt++;
            Animation anim = AnimationUtils.loadAnimation(Utils.getApp().getApplicationContext(), R.anim.view_in_from_bottom);
            anim.setStartOffset(cnt * VIEW_BOUNCE_ANIM_DURATION + FRAG_IN_ANIM_DELAY);
            v.startAnimation(anim);
        }
    }

    public void goSellCategorySelect() {
        rl_category.setVisibility(View.VISIBLE);

        sellCategorySelectFragment = SellCategorySelectFragment.newInstance();
        transaction = fm.beginTransaction();
        transaction.add(R.id.rl_category, sellCategorySelectFragment, "SellCategorySelect");
        transaction.commit();
    }

    public void startEndAnimation(View rootView) {
        RelativeLayout view = ((RelativeLayout) rootView);
        int cnt = 0;
        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);
            cnt++;
            Animation anim = AnimationUtils.loadAnimation(Utils.getApp().getApplicationContext(), R.anim.view_out_to_top);
            anim.setStartOffset(cnt * VIEW_BOUNCE_ANIM_DURATION + FRAG_IN_ANIM_DELAY);
            v.startAnimation(anim);
            if (i == view.getChildCount() - 1) {
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        finish();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (rl_shopper.getVisibility() != View.VISIBLE) {
            startEndAnimation(findViewById(R.id.rl_main));
        } else if (rl_category.getVisibility() != View.VISIBLE) {
            rl_shopper.setVisibility(View.INVISIBLE);

            transaction = fm.beginTransaction();
            transaction.remove(sellShopperFramment);
            transaction.commit();
        } else {
            sellCategorySelectFragment.onBack(null);
        }
    }

    public void closeSellCategory() {
        rl_category.setVisibility(View.INVISIBLE);

        transaction = fm.beginTransaction();
        transaction.remove(sellCategorySelectFragment);
        transaction.commit();
    }
}
