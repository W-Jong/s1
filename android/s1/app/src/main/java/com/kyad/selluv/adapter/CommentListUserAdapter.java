package com.kyad.selluv.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.usr.UsrMiniDto;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.detail.CommentActivity;

import java.util.ArrayList;

public class CommentListUserAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public ArrayList<UsrMiniDto> arrData = new ArrayList<>();
    CommentActivity activity = null;
    public boolean isEditing;

    public CommentListUserAdapter(CommentActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrMiniDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_detail_comment_user, parent, false);
            final UsrMiniDto anItem = (UsrMiniDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }

            ImageView iv_profile = convertView.findViewById(R.id.iv_profile);
            ImageUtils.load(activity, anItem.getProfileImg(), android.R.color.transparent, R.drawable.ic_user_default, iv_profile);

            ((TextView) convertView.findViewById(R.id.tv_user_name)).setText(anItem.getUsrNckNm());
            ((TextView) convertView.findViewById(R.id.tv_user_id)).setText(anItem.getUsrId());

            convertView.findViewById(R.id.rl_comment_user).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.addComment(anItem.getUsrNckNm());
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}