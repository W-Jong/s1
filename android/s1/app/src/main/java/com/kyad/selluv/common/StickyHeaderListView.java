/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kyad.selluv.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

import com.kyad.selluv.R;

public class StickyHeaderListView extends ListView {

    private View mSticky;
    private View mStickyContainer;
    private int mStatusBarInset = 0;
    private RectF mStickyRect = new RectF();

    public StickyHeaderListView(Context context) {
        super(context);
        init(null, android.R.attr.listViewStyle);
    }

    public StickyHeaderListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, android.R.attr.listViewStyle);
    }

    public StickyHeaderListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyleAttr) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs,
                R.styleable.SuwStickyHeaderListView, defStyleAttr, 0);
        int headerResId = a.getResourceId(R.styleable.SuwStickyHeaderListView_suwHeader, 0);
        if (headerResId != 0) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View header = inflater.inflate(headerResId, this, false);
            addHeaderView(header);
        }
        a.recycle();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (mSticky == null) {
            updateStickyView();
        }
    }

    public void updateStickyView() {
        mSticky = findViewWithTag("sticky");
        mStickyContainer = findViewWithTag("stickyContainer");
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mStickyRect.contains(ev.getX(), ev.getY())) {
            ev.offsetLocation(-mStickyRect.left, -mStickyRect.top);
            return mStickyContainer.dispatchTouchEvent(ev);
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (mSticky != null) {
            final int saveCount = canvas.save();
            // The view to draw when sticking to the top
            final View drawTarget = mStickyContainer != null ? mStickyContainer : mSticky;
            // The offset to draw the view at when sticky
            final int drawOffset = mStickyContainer != null ? mSticky.getTop() : 0;
            // Position of the draw target, relative to the outside of the scrollView
            final int drawTop = drawTarget.getTop();
            if (drawTop + drawOffset < mStatusBarInset || !drawTarget.isShown()) {
                // ListView does not translate the canvas, so we can simply draw at the top
                mStickyRect.set(0, -drawOffset, drawTarget.getWidth(),
                        drawTarget.getHeight() - drawOffset);
                canvas.translate(0, mStickyRect.top);
                canvas.clipRect(0, 0, drawTarget.getWidth(), drawTarget.getHeight());
                drawTarget.draw(canvas);
            } else {
                mStickyRect.setEmpty();
            }
            canvas.restoreToCount(saveCount);
        }
    }

}
