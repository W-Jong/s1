package com.kyad.selluv.detail;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.CommentListAdapter;
import com.kyad.selluv.adapter.CommentListUserAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.pdt.ReplyDto;
import com.kyad.selluv.api.dto.usr.UsrMiniDto;
import com.kyad.selluv.api.enumtype.REPORT_TYPE;
import com.kyad.selluv.api.request.ContentReq;
import com.kyad.selluv.api.request.ReportReq;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class CommentActivity extends BaseActivity {

    public static final String PDT_UID = "PDT_UID";

    private int pdtUid;
    private int page = 0;
    private boolean isLoading = false;
    private boolean isLast = false;

    private List<UsrMiniDto> usrMiniDtoList = new ArrayList<>();

    //UI Reference
    @BindView(R.id.lv_list)
    ListView lv_list;
    @BindView(R.id.lv_list_user)
    ListView lv_list_user;
    @BindView(R.id.btn_comment_add)
    Button btn_comment_add;
    @BindView(R.id.comment_input)
    EditText comment_input;
    @BindView(R.id.tv_title)
    TextView tv_no_comment;

    @OnClick(R.id.ib_left)
    void onLeft() {
        Utils.hideSoftKeyboard(this);
        finish();
    }

    @OnClick(R.id.btn_comment_add)
    void onCommentAdd(View view) {
        if (TextUtils.isEmpty(comment_input.getText().toString())) {
            return;
        }

        Utils.hideSoftKeyboard(this);
        putPdtReply();
    }

    //Variables
    CommentListAdapter listAdapter;
    CommentListUserAdapter listUserAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_comment);
        pdtUid = getIntent().getIntExtra(PDT_UID, 0);
        if (pdtUid < 1) {
            finish();
            return;
        }

        loadLayout();

        getPdtReplyList();
    }

    @Override
    public void onBackPressed() {
        onLeft();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.rl_filter));
        listAdapter = new CommentListAdapter(this);
        lv_list.setAdapter(listAdapter);
        lv_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    page++;
                    getPdtReplyList();
                }
            }
        });

        listUserAdapter = new CommentListUserAdapter(this);
        lv_list_user.setAdapter(listUserAdapter);

        comment_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputComment = comment_input.getText().toString();
                if (inputComment.length() > 0) {
                    btn_comment_add.setBackgroundResource(R.drawable.review_btn_on_transparent);
                    btn_comment_add.setTextColor(getResources().getColor(R.color.color_42c2fe));

                    if ("@".equals(inputComment) || inputComment.matches("^\\@(\\w+)$")) {
                        getPdtReplyUserList(inputComment.substring(1));
                    } else {
                        lv_list_user.setVisibility(View.INVISIBLE);
                    }
                } else {
                    lv_list_user.setVisibility(View.INVISIBLE);
                    btn_comment_add.setBackgroundResource(R.drawable.review_btn_off_transparent);
                    btn_comment_add.setTextColor(getResources().getColor(R.color.color_666666));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void addComment(String nickname) {
        lv_list_user.setVisibility(View.INVISIBLE);

        comment_input.setText("@" + nickname + " ");
        comment_input.setSelection(nickname.length() + 2);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(comment_input, InputMethodManager.SHOW_FORCED);
        comment_input.requestFocus();
    }


    private void getPdtReplyList() {
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<ReplyDto>>> genRes = restAPI.getPdtReplyList(Globals.userToken, pdtUid, page);

        genRes.enqueue(new TokenCallback<Page<ReplyDto>>(this) {
            @Override
            public void onSuccess(Page<ReplyDto> response) {
                closeLoading();
                isLoading = false;

                if (page == 0) {
                    listAdapter.arrData.clear();
                }
                isLast = response.isLast();
                listAdapter.arrData.addAll(response.getContent());
                listAdapter.notifyDataSetChanged();

                if (listAdapter.arrData.size() == 0) {
                    tv_no_comment.setVisibility(View.VISIBLE);
                } else {
                    tv_no_comment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void getPdtReplyUserList(String keyword) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        Call<GenericResponse<List<UsrMiniDto>>> genRes = restAPI.getPdtReplyUserList(Globals.userToken, pdtUid, keyword);

        genRes.enqueue(new TokenCallback<List<UsrMiniDto>>(this) {
            @Override
            public void onSuccess(List<UsrMiniDto> response) {
                closeLoading();
                usrMiniDtoList = response;
                if (response.size() < 1) {
                    lv_list_user.setVisibility(View.INVISIBLE);
                } else {
                    lv_list_user.setVisibility(View.VISIBLE);
                    listUserAdapter.arrData.clear();
                    listUserAdapter.arrData.addAll(response);
                }
                listUserAdapter.notifyDataSetChanged();
                if (listUserAdapter.arrData.size() == 0) {
                    tv_no_comment.setVisibility(View.VISIBLE);
                } else {
                    tv_no_comment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                lv_list_user.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void putPdtReply() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        ContentReq contentReq = new ContentReq();
        contentReq.setContent(comment_input.getText().toString());

        Call<GenericResponse<ReplyDto>> genRes = restAPI.putPdtReply(Globals.userToken, pdtUid, contentReq);

        genRes.enqueue(new TokenCallback<ReplyDto>(this) {
            @Override
            public void onSuccess(ReplyDto response) {
                closeLoading();
                listAdapter.arrData.add(response);
                listAdapter.notifyDataSetChanged();
                comment_input.setText("");
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

                if (t instanceof com.kyad.selluv.api.APIException) {
                    if (((com.kyad.selluv.api.APIException)t).getCode().equals("510")){
                        Toast.makeText(CommentActivity.this, "입력하신 닉네임이 정확하지 않습니다.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void deletePdtReply(final ReplyDto replyDto) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.delete))
                .setMessage(getString(R.string.reply_comment_delete_quiz))
                .setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doDeletePdtReply(replyDto);
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    void doDeletePdtReply(ReplyDto replyDto) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        ContentReq contentReq = new ContentReq();
        contentReq.setContent(comment_input.getText().toString());

        Call<GenericResponse<Void>> genRes = restAPI.deletePdtReply(Globals.userToken, replyDto.getReply().getReplyUid());

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                listAdapter.arrData.remove(replyDto);
                listAdapter.notifyDataSetChanged();
                Utils.showToast(CommentActivity.this, "삭제되었습니다.");
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    public void reportPdt(final PopupDialog popup) {

        CustomSpinner spinner2 = ((CustomSpinner) popup.findView(R.id.spinner2));
        if (spinner2.getSelectedItemPosition() == 0) {
            Utils.showToast(CommentActivity.this, "신고 사유를 선택해 주세요.");
            return;
        }

        ReportReq reportReq = new ReportReq();
        reportReq.setKind(2);
        reportReq.setTargetUid(pdtUid);
        reportReq.setReportType(REPORT_TYPE.values()[spinner2.getSelectedItemPosition() - 1]);
        reportReq.setContent(((EditText) popup.findView(R.id.txt_report_detail)).getText().toString());

        if (TextUtils.isEmpty(reportReq.getContent())) {
            Utils.showToast(CommentActivity.this, "신고 사유 내용을 작성해 주세요.");
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.reportOther(Globals.userToken, reportReq);
        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                Utils.showToast(CommentActivity.this, "신고하였습니다.");
                popup.dismiss();
                Utils.hideSoftKeyboard(CommentActivity.this);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
