package com.kyad.selluv.detail;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealPaymentDto;
import com.kyad.selluv.api.dto.other.PromotionCodeDao;
import com.kyad.selluv.api.dto.pdt.PdtDetailDto;
import com.kyad.selluv.api.dto.usr.UsrAddressDao;
import com.kyad.selluv.api.dto.usr.UsrMyInfoDto;
import com.kyad.selluv.api.enumtype.DEAL_TYPE;
import com.kyad.selluv.api.enumtype.PAY_TYPE;
import com.kyad.selluv.api.request.DealBuyReq;
import com.kyad.selluv.api.request.PromotionReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.DockAnimation;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.mypage.CardManageActivity;
import com.kyad.selluv.webview.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

import static com.kyad.selluv.mypage.CardManageActivity.CARD_SELECT_REQUEST;

public class BuyActivity extends BaseActivity {

    private PdtDetailDto pdtDetail;
    private DealBuyReq buyReq = new DealBuyReq();
    private PromotionCodeDao promotionCode = null;
    int nSelectedUsrCardUid = 0;
    private String addressDetail = "";
    private String addressDetailSub = "";
    private long total_pay_price = 0;

    @BindView(R.id.iv_item_ware)
    ImageView iv_item_ware;

    @BindView(R.id.tv_ware_name_eng)
    TextView tv_ware_name_eng;
    @BindView(R.id.tv_ware_name_ko)
    TextView tv_ware_name_ko;
    @BindView(R.id.tv_ware_size)
    TextView tv_ware_size;
    @BindView(R.id.tv_ware_price)
    TextView tv_ware_price;
    @BindView(R.id.btn_test_free_onoff)
    ToggleButton btn_test_free;
    @BindView(R.id.btn_test_pay_onoff)
    ToggleButton btn_test_pay;
    @BindView(R.id.tv_selluvmoney_value)
    TextView tv_selluvmoney_value;
    @BindView(R.id.txt_item_price_value)
    TextView txt_item_price_value;
    @BindView(R.id.txt_transport_price_value)
    TextView txt_transport_price_value;
    @BindView(R.id.txt_item_office_test_value)
    TextView txt_item_office_test_value;
    @BindView(R.id.txt_promotion_verify_value)
    TextView txt_promotion_verify_value;
    @BindView(R.id.txt_selluvmoney_use_value)
    TextView txt_selluvmoney_use_value;
    @BindView(R.id.tv_selluvmoney_value_input)
    EditText tv_selluvmoney_value_input;
    @BindView(R.id.btn_paymoney_collapse)
    ImageView btn_paymoney_collapse;
    @BindView(R.id.btn_max_selluvmoney_collapse)
    ImageView btn_max_selluvmoney_collapse;
    @BindView(R.id.rl_paymoney_bar)
    RelativeLayout rl_paymoney_bar;
    @BindView(R.id.rl_max_money_bar)
    RelativeLayout rl_maxmoney_bar;
    @BindView(R.id.txt_buy_price_percent)
    TextView txt_buy_price_percent;
    @BindView(R.id.txt_buy_price_value)
    TextView txt_buy_price_value;
    @BindView(R.id.txt_after_price_value)
    TextView txt_after_price_value;
    @BindView(R.id.tv_max_selluvmoney_value)
    TextView tv_max_selluvmoney_value;
    @BindView(R.id.txt_max_total_price_value)
    TextView txt_max_total_price_value;
    @BindView(R.id.txt_total_price_value)
    TextView txt_total_price_value;
    @BindView(R.id.tv_paymoney_value)
    TextView tv_paymoney_value;
    @BindView(R.id.btn_simple_pay_onoff)
    ToggleButton btn_simple_pay_onoff;
    @BindView(R.id.rbtn_checkcard_pay)
    ToggleButton rbtn_checkcard_pay;
    @BindView(R.id.rbtn_neighbour_pay)
    ToggleButton rbtn_neighbour_pay;
    @BindView(R.id.rbtn_timeline_pay)
    ToggleButton rbtn_timeline_pay;
    @BindView(R.id.tv_name)
    EditText edt_name;
    @BindView(R.id.tv_contact)
    EditText edt_contact;
    @BindView(R.id.tv_address)
    EditText edt_address;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;
    @BindView(R.id.rl_card)
    RelativeLayout rl_card;
    @BindView(R.id.rl_card_register)
    RelativeLayout rl_card_register;
    @BindView(R.id.btn_card_change)
    Button btn_card_change;
    @BindView(R.id.txt_description2)
    TextView txt_description2;
    @BindView(R.id.txt_price_unit2)
    TextView txt_price_unit2;
    @BindView(R.id.rl_extra_send_price_info_area)
    RelativeLayout rl_extra_send_price_info_area;
    @BindView(R.id.txt_extra_transport)
    TextView txt_extra_transport;
    @BindView(R.id.txt_extra_transport_price_value)
    TextView txt_extra_transport_price_value;
    @BindView(R.id.rl_promotion_info_area)
    RelativeLayout rl_promotion_info_area;
    @BindView(R.id.rl_selluvmoney_info_area)
    RelativeLayout rl_selluvmoney_info_area;
    @BindView(R.id.tv_promotion)
    TextView tv_promotion;
    @BindView(R.id.btn_promotion_setup)
    Button btn_promotion_setup;
    @BindView(R.id.btn_selluvMoney_use)
    Button btn_selluvMoney_use;


    public boolean visible1 = true;
    public boolean visible2 = true;

    //UI Reference
    @OnClick(R.id.ib_Start)
    void onBack(View v) {
        finish();
    }

    @OnClick(R.id.btn_eval_guide)
    void onEvalGuide(View v) {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.detail_buy_auth);
        startActivity(intent);
    }

    @OnClick(R.id.btn_promotion_setup)
    void onPromotion(View v) {
        checkPromotionCode();
    }

    @OnClick(R.id.rl_pay_money)
    void onCollapse1(View v) {
        showDetail1();
    }

    @OnClick(R.id.rl_max_selluvmoney)
    void onCollapse2(View v) {
        showDetail2();
    }

    @OnClick({R.id.btn_search_address, R.id.tv_address})
    void onSearchAddress() {
        CommonWebViewActivity.showAddressWebviewActivity(this);
    }

    @OnClick({R.id.btn_card_register, R.id.btn_card_change})
    void onRegisterCard(View v) {
        Intent intent = new Intent(this, CardManageActivity.class);
        intent.putExtra(CardManageActivity.CARD_SELECTION_MODE, true);
        startActivityForResult(intent, CardManageActivity.CARD_SELECT_REQUEST);
    }

    @OnClick({R.id.btn_simple_pay_onoff, R.id.rl_pay_card_option})
    void onSimplePay(View v) {
        btn_simple_pay_onoff.setBackgroundResource(R.drawable.list_roundbox_on);
        rbtn_checkcard_pay.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_neighbour_pay.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_timeline_pay.setBackgroundResource(R.drawable.list_roundbox_off);

        buyReq.setPayType(PAY_TYPE.SIMPLECARD);
    }

    @OnClick({R.id.rbtn_checkcard_pay, R.id.rl_checkcard_pay_option})
    void onCheckCard(View v) {
        btn_simple_pay_onoff.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_checkcard_pay.setBackgroundResource(R.drawable.list_roundbox_on);
        rbtn_neighbour_pay.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_timeline_pay.setBackgroundResource(R.drawable.list_roundbox_off);

        buyReq.setPayType(PAY_TYPE.NORMALCARD);
    }
//
//    @OnClick({R.id.rbtn_neighbour_pay, R.id.rl_neighbour_pay_option})
//    void onNeighbourCard(View v) {
//        btn_simple_pay_onoff.setBackgroundResource(R.drawable.list_roundbox_off);
//        rbtn_checkcard_pay.setBackgroundResource(R.drawable.list_roundbox_off);
//        rbtn_neighbour_pay.setBackgroundResource(R.drawable.list_roundbox_on);
//        rbtn_timeline_pay.setBackgroundResource(R.drawable.list_roundbox_off);
//    }

    @OnClick({R.id.rbtn_timeline_pay, R.id.rl_timeline_pay_option})
    void onTimeLine(View v) {
        btn_simple_pay_onoff.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_checkcard_pay.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_neighbour_pay.setBackgroundResource(R.drawable.list_roundbox_off);
        rbtn_timeline_pay.setBackgroundResource(R.drawable.list_roundbox_on);

        buyReq.setPayType(PAY_TYPE.REALTIMEPAY);
    }

    @OnClick({R.id.btn_test_free_onoff, R.id.rl_test_option1})
    void onTestFree(View v) {
        btn_test_free.setBackgroundResource(R.drawable.list_roundbox_on);
        btn_test_pay.setBackgroundResource(R.drawable.list_roundbox_off);
        buyReq.setPdtVerifyEnabled(0);

        refreshPriceInfoArea();
    }

    @OnClick({R.id.btn_test_pay_onoff, R.id.rl_test_option2})
    void onTestPay(View v) {
        btn_test_pay.setBackgroundResource(R.drawable.list_roundbox_on);
        btn_test_free.setBackgroundResource(R.drawable.list_roundbox_off);
        buyReq.setPdtVerifyEnabled(1);

        refreshPriceInfoArea();
    }

    @OnClick(R.id.btn_selluvMoney_use)
    void selluvUse(View v) {
        checkSelluvMoney();
    }

    @OnClick(R.id.btn_pay)
    void onPay(View v) {
        refreshPriceInfoArea();

        if (total_pay_price < 1) {
            Utils.showToast(this, "결제금액은 0원 이상이어야 합니다.");
            return;
        }

        if (edt_name.length() == 0) {
            Utils.showToast(BuyActivity.this, "성함을 입력해주세요.");
            return;
        }
        if (edt_contact.length() == 0) {
            Utils.showToast(BuyActivity.this, "연락처를 입력해주세요.");
            return;
        }
        if (edt_address.length() == 0) {
            Utils.showToast(BuyActivity.this, "주소를 입력해주세요.");
            return;
        }

        buyReq.setRecipientNm(edt_name.getText().toString());
        buyReq.setRecipientPhone(edt_contact.getText().toString());
        buyReq.setRecipientAddress(addressDetail + "\n" + addressDetailSub + "");

        if (buyReq.getPayType().getCode() == PAY_TYPE.SIMPLECARD.getCode() && nSelectedUsrCardUid < 1) {
            Utils.showToast(BuyActivity.this, "결제에 이용될 카드를 선택해주세요.");
            return;
        }

        if (buyReq.getPayType().getCode() == PAY_TYPE.SIMPLECARD.getCode())
            buyReq.setUsrCardUid(nSelectedUsrCardUid);
        else
            buyReq.setUsrCardUid(0);

        buyRequest();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_buy);
        setStatusBarWhite();

        if (Globals.selectedPdtDetailDto == null)
            finish();

        pdtDetail = Globals.selectedPdtDetailDto;

        buyReq.setPdtUid(pdtDetail.getPdt().getPdtUid());
        buyReq.setReqPrice(pdtDetail.getPdt().getPrice());
        buyReq.setPayPromotion("");
        buyReq.setPayType(PAY_TYPE.SIMPLECARD);

        loadLayout();

        getMyDetailInfo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CARD_SELECT_REQUEST && resultCode == RESULT_OK && Globals.selectedUsrCardDao != null) {
            rl_card_register.setVisibility(View.GONE);
            rl_card.setVisibility(View.VISIBLE);
            btn_card_change.setVisibility(View.VISIBLE);

            TextView tv_number = (TextView) findViewById(R.id.tv_number);
            TextView tv_company = (TextView) findViewById(R.id.tv_company);
            tv_number.setText(Utils.convertCardNum(Globals.selectedUsrCardDao.getCardNumber()));
            tv_company.setText(Globals.selectedUsrCardDao.getCardName());
            nSelectedUsrCardUid = Globals.selectedUsrCardDao.getUsrCardUid();

        }

        if (requestCode == CommonWebViewActivity.WEBVIEW_ADDRESS_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);
            if (isSuccess) {
                addressDetail = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS);
                addressDetailSub = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_POSTCODE);
                edt_address.setText(addressDetail + "\n" + addressDetailSub + "");

                refreshPriceInfoArea();
            }
        }

        if (requestCode == CommonWebViewActivity.WEBVIEW_PAYMENT_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_PAYMENT_SUCCESS, false);
            if (isSuccess) {
                Globals.selectedPdtDetailDto = pdtDetail;
                Globals.selectedDealBuyReq = buyReq;
                Globals.selectedPromotionCodeDao = promotionCode;
                Intent intent = new Intent(BuyActivity.this, BuyCompleteActivity.class);
                intent.putExtra(BuyCompleteActivity.DEAL_UID, data.getLongExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_DEALUID, 0));
                startActivity(intent);
                finish();
            } else {
                Utils.showToast(this, "결제에 실패했습니다.");
            }
        }
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        ImageUtils.load(this, pdtDetail.getPdt().getProfileImg(), android.R.color.transparent, R.drawable.img_default_square, iv_item_ware);
        tv_ware_name_eng.setText(pdtDetail.getBrand().getNameEn());
        tv_ware_name_ko.setText(pdtDetail.getPdtTitle());
        tv_ware_size.setText("size " + pdtDetail.getPdt().getPdtSize());
        tv_ware_price.setText("￦ " + Utils.getAttachCommaFormat(pdtDetail.getPdt().getPrice()));

        txt_description2.setText(R.string.simplecard_pay_description);
        txt_item_price_value.setText(Utils.getAttachCommaFormat(pdtDetail.getPdt().getPrice()));
        txt_transport_price_value.setText(Utils.getAttachCommaFormat((pdtDetail.isFreeSend()) ? 0 : pdtDetail.getPdt().getSendPrice()));

        txt_buy_price_percent.setText("(" + (Globals.systemSetting != null ? Globals.systemSetting.getDealReward() : 0) + "%)");
        txt_after_price_value.setText(Utils.getAttachCommaFormat(Globals.systemSetting != null ? Globals.systemSetting.getBuyReviewReward() : 0));

        refreshPriceInfoArea();
    }

    private void loadUserLayout() {
        tv_selluvmoney_value.setText(String.valueOf(Globals.myInfo.getUsr().getMoney()));

        try {
            UsrAddressDao defaultUsrAddress = Globals.myInfo.getAddressList().get(0);
            tb_segment.setToggleStatus(0);
            if (Globals.myInfo.getAddressList().get(1).getStatus() == 2) {
                defaultUsrAddress = Globals.myInfo.getAddressList().get(1);
                tb_segment.setToggleStatus(1);
            }
            if (Globals.myInfo.getAddressList().get(2).getStatus() == 2) {
                defaultUsrAddress = Globals.myInfo.getAddressList().get(2);
                tb_segment.setToggleStatus(2);
            }
            setEditValueFromDao(defaultUsrAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tb_segment.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
                try {
                    setEditValueFromDao(Globals.myInfo.getAddressList().get(toggleIntValue));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setEditValueFromDao(UsrAddressDao usrAddress) {
        edt_name.setText(usrAddress.getAddressName());
        edt_contact.setText(usrAddress.getAddressPhone());

        addressDetail = usrAddress.getAddressDetail();
        addressDetailSub = usrAddress.getAddressDetailSub();
        edt_address.setText(TextUtils.isEmpty(addressDetail) && TextUtils.isEmpty(addressDetailSub) ? "" : addressDetail + "\n" + addressDetailSub + "");
    }

    private void refreshPriceInfoArea() {

        total_pay_price = buyReq.getReqPrice() + ((pdtDetail.isFreeSend()) ? 0 : pdtDetail.getPdt().getSendPrice());

        //TODO: 카드연동 및 도서지역 추가배송비 설정
        final long islandSendPrice = 0;
        buyReq.setIslandSendPrice(islandSendPrice);

        if (buyReq.getIslandSendPrice() > 0) {
            rl_extra_send_price_info_area.setVisibility(View.VISIBLE);
            txt_extra_transport_price_value.setText(Utils.getAttachCommaFormat(buyReq.getIslandSendPrice()));
            if (buyReq.getIslandSendPrice() == 3000) {
                txt_extra_transport.setText("제주도지역 추가 배송비");
            } else {
                txt_extra_transport.setText("도서지역 추가 배송비");
            }
            total_pay_price += buyReq.getIslandSendPrice();
        } else {
            rl_extra_send_price_info_area.setVisibility(View.GONE);
        }

        if (buyReq.getPdtVerifyEnabled() == 1) {
            txt_price_unit2.setText("원");
            txt_item_office_test_value.setVisibility(View.VISIBLE);
            txt_item_office_test_value.setText(Utils.getAttachCommaFormat(39000));
            total_pay_price += 39000;
        } else {
            txt_price_unit2.setText("무료");
            txt_item_office_test_value.setVisibility(View.GONE);
        }

        if (promotionCode == null || TextUtils.isEmpty(buyReq.getPayPromotion())) {
            rl_promotion_info_area.setVisibility(View.GONE);
        } else {
            long promotionSalePrice = 0;
            if (promotionCode.getPriceUnit() == 1) {
                promotionSalePrice = (long) (buyReq.getReqPrice() * promotionCode.getPrice() / 100.0f);
            } else {
                promotionSalePrice = promotionCode.getPrice();
            }
            if (promotionSalePrice > 0) {
                rl_promotion_info_area.setVisibility(View.VISIBLE);
                txt_promotion_verify_value.setText("- " + Utils.getAttachCommaFormat(promotionSalePrice));
                total_pay_price -= promotionSalePrice;
            } else {
                rl_promotion_info_area.setVisibility(View.GONE);
            }
        }

        if (buyReq.getRewardPrice() > 0) {
            rl_selluvmoney_info_area.setVisibility(View.VISIBLE);
            txt_selluvmoney_use_value.setText("- " + Utils.getAttachCommaFormat(buyReq.getRewardPrice()));
            total_pay_price -= buyReq.getRewardPrice();
        } else {
            rl_selluvmoney_info_area.setVisibility(View.GONE);
        }

        txt_total_price_value.setText(Utils.getAttachCommaFormat(total_pay_price));
        tv_paymoney_value.setText(Utils.getAttachCommaFormat(total_pay_price) + " 원");

        long deal_reward = (long) (total_pay_price * (Globals.systemSetting != null ? Globals.systemSetting.getDealReward() : 0) / 100.0f);
        txt_buy_price_value.setText(Utils.getAttachCommaFormat(deal_reward));
        tv_max_selluvmoney_value.setText(Utils.getAttachCommaFormat(deal_reward + (Globals.systemSetting != null ? Globals.systemSetting.getBuyReviewReward() : 0)));
        txt_max_total_price_value.setText(Utils.getAttachCommaFormat(deal_reward + (Globals.systemSetting != null ? Globals.systemSetting.getBuyReviewReward() : 0)));

    }

    void showDetail1() {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_paymoney_bar, 300,
                DockAnimation.FRAME_LAYOUT);

        int from = !visible1 ? 0 : Utils.dpToPixel(this, 195);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(this, 195) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.start();

        visible1 = !visible1;

        btn_paymoney_collapse.animate().rotationBy(180).setDuration(400).start();
    }

    void showDetail2() {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_maxmoney_bar, 300,
                DockAnimation.FRAME_LAYOUT);

        int from = !visible2 ? 0 : Utils.dpToPixel(this, 135);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(this, 135) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.start();

        visible2 = !visible2;

        btn_max_selluvmoney_collapse.animate().rotationBy(180).setDuration(400).start();
    }

    //get user detail info
    private void getMyDetailInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrMyInfoDto>> genRes = restAPI.myInfo(Globals.userToken);

        genRes.enqueue(new TokenCallback<UsrMyInfoDto>(this) {
            @Override
            public void onSuccess(UsrMyInfoDto response) {
                closeLoading();
                Globals.myInfo = response;

                loadUserLayout();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                finish();
            }
        });
    }

    private void buyRequest() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<DealPaymentDto>> genRes = restAPI.dealBuyReq(Globals.userToken, buyReq);

        genRes.enqueue(new TokenCallback<DealPaymentDto>(this) {
            @Override
            public void onSuccess(DealPaymentDto response) {
                closeLoading();

                if (buyReq.getPayType().getCode() == PAY_TYPE.SIMPLECARD.getCode()) {
                    Globals.selectedPdtDetailDto = pdtDetail;
                    Globals.selectedDealBuyReq = buyReq;
                    Globals.selectedPromotionCodeDao = promotionCode;
                    Intent intent = new Intent(BuyActivity.this, BuyCompleteActivity.class);
                    intent.putExtra(BuyCompleteActivity.DEAL_UID, response.getDeal().getDealUid());
                    startActivity(intent);
                    finish();
                } else {
                    CommonWebViewActivity.showPaymentWebviewActivity(BuyActivity.this, response.getPaymentUrl());
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void checkSelluvMoney() {
        if (btn_selluvMoney_use.getText().equals(getString(R.string.selluvmoney_use_text))) {
            String inputSelluvMoney = tv_selluvmoney_value_input.getText().toString();
            if (TextUtils.isEmpty(inputSelluvMoney)) {
                Utils.showToast(this, "사용할 셀럽머니를 입력해주세요.");
                return;
            }

            long useMoney = 0;
            try {
                useMoney = Long.parseLong(inputSelluvMoney);
            } catch (NumberFormatException e) {
                useMoney = 0;
            }

            if (Globals.myInfo.getUsr().getMoney() < useMoney) {
                Utils.showToast(this, "셀럽머니가 부족합니다.");
                return;
            }

            tv_selluvmoney_value_input.setText(String.valueOf(useMoney));
            tv_selluvmoney_value_input.setTextColor(getResources().getColor(R.color.color_42c2fe));
            tv_selluvmoney_value_input.setEnabled(false);
            btn_selluvMoney_use.setText(getString(R.string.cancel_selluvmoney_use_text));
            buyReq.setRewardPrice(useMoney);
        } else {
            tv_selluvmoney_value_input.setText("");
            tv_selluvmoney_value_input.setTextColor(getResources().getColor(R.color.color_333333));
            tv_selluvmoney_value_input.setEnabled(true);
            btn_selluvMoney_use.setText(getString(R.string.selluvmoney_use_text));
            buyReq.setRewardPrice(0);
        }

    }

    private void checkPromotionCode() {
        if (btn_promotion_setup.getText().equals(getString(R.string.setup_text))) {
            final String promosioncode = tv_promotion.getText().toString();

            if (TextUtils.isEmpty(promosioncode)) {
                Utils.showToast(this, "구매용 프로모션코드를 입력해주세요.");
                return;
            }

            showLoading();

            PromotionReq promotionReq = new PromotionReq();
            promotionReq.setBrandUid(pdtDetail.getPdt().getBrandUid());
            promotionReq.setDealType(DEAL_TYPE.BUYER);
            promotionReq.setPdtUid(pdtDetail.getPdt().getPdtUid());
            promotionReq.setPromotionCode(promosioncode);
            promotionReq.setCategoryUid(pdtDetail.getPdt().getCategoryUid());

            RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
            Call<GenericResponse<PromotionCodeDao>> genRes = restAPI.getPromotionInfo(Globals.userToken, promotionReq);
            genRes.enqueue(new TokenCallback<PromotionCodeDao>(this) {

                @Override
                public void onSuccess(PromotionCodeDao response) {
                    closeLoading();
                    promotionCode = response;
                    buyReq.setPayPromotion(promosioncode);
                    tv_promotion.setText(promotionCode.getTitle());
                    tv_promotion.setTextColor(getResources().getColor(R.color.color_42c2fe));
                    tv_promotion.setEnabled(false);

                    btn_promotion_setup.setText(getString(R.string.cancel_setup_text));

                    Utils.showToast(BuyActivity.this, "프로모션코드가 적용되었습니다.");
                    refreshPriceInfoArea();
                }

                @Override
                public void onFailed(Throwable t) {
                    closeLoading();
                    tv_promotion.setText("");
                    tv_promotion.setTextColor(getResources().getColor(R.color.color_333333));
                    tv_promotion.setEnabled(true);
                    btn_promotion_setup.setText(getString(R.string.setup_text));
                }
            });
        } else {
            tv_promotion.setText("");
            tv_promotion.setTextColor(getResources().getColor(R.color.color_333333));
            tv_promotion.setEnabled(true);
            btn_promotion_setup.setText(getString(R.string.setup_text));
            refreshPriceInfoArea();
        }

    }
}
