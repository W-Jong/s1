package com.kyad.selluv.api.dto.other;

import lombok.Data;

@Data
public class AlarmCountDto {
    //("전체 알림갯수")
    private long totalCount;
    //("거래 알림갯수")
    private long dealCount;
    //("소식 알림갯수")
    private long newsCount;
    //("댓글 알림갯수")
    private long replyCount;
}
