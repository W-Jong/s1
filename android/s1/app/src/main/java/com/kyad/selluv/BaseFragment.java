package com.kyad.selluv;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Fragment의 최상위 parent
 * <p>
 * Created by Kris on 2017.07.10
 */
public class BaseFragment extends Fragment {

    private boolean hasFocus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected boolean isAllowAnimation() {
        return true;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        // animation 허용 여부 확인
        if (!isAllowAnimation()) {
            return super.onCreateAnimation(transit, enter, nextAnim);
        }
        if (enter) {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            return animation;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public void showLoading() {
        if(getActivity() == null) {
            return;
        }

        ((BaseActivity) getActivity()).showLoading();
    }

    public void closeLoading() {
        if(getActivity() == null) {
            return;
        }

        ((BaseActivity) getActivity()).closeLoading();
    }

    /**
     * Dialog, DialogFragment, Activity 에 의해 변경
     *
     * @return
     */
    public boolean getFocus() {
        return this.hasFocus;
    }

    public void setFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        //viewTreeObserver.addOnWindowFocusChangeListener(this::setFocus);
        setFocus(true); // Default Focus
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void startActivity(Class cls, boolean finish, int enterAnim, int exitAnim) {
        Intent intent = new Intent(getActivity(), cls);
        startActivity(intent);
        if (finish) getActivity().finish();
    }
}
