package com.kyad.selluv.api.dto.deal;

import com.kyad.selluv.api.dto.usr.UsrMiniDto;

import java.util.List;

import lombok.Data;

@Data
public class DealDetailDto {
    //("거래정보")
    private DealDao deal;
    //("구매자 또는 판매자정보")
    private UsrMiniDto usr;
    //("반품첨부이미지")
    private List<String> refundPhotos;
}