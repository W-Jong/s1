package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.NoticeDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class NoticeDetailActivity extends BaseActivity {

    private int noticeUid = 0;
    private NoticeDao dicNoticeInfo;

    //UI Reference
    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @BindView(R.id.tv_title)
    TextView txt_title;

    @BindView(R.id.tv_news_content)
    TextView txt_contant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        setStatusBarWhite();

        noticeUid = getIntent().getIntExtra(Constants.NOTICE_UID, 0);
        getEventDetailInfo();
    }

    private void loadLayout() {
        txt_title.setText(dicNoticeInfo.getTitle());
        txt_contant.setText(dicNoticeInfo.getContent());
    }

    //get event detail info
    private void getEventDetailInfo(){
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<NoticeDao>> genRes = restAPI.getNoticeDetail(Globals.userToken, noticeUid);

        genRes.enqueue(new TokenCallback<NoticeDao>(this) {
            @Override
            public void onSuccess(NoticeDao response) {
                closeLoading();

                dicNoticeInfo = response;
                loadLayout();
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }
}
