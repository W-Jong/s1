package com.kyad.selluv.api.request;

import com.kyad.selluv.api.enumtype.LOGIN_TYPE;
import com.kyad.selluv.api.enumtype.OS_TYPE;

import lombok.Data;
import lombok.NonNull;


@Data
public class UserLoginReq {

    //@Size(min = 1)
    //@ApiModelProperty("아이디, sns로그인의 경우 sns ID")
    private String usrId;
    @NonNull
    //@ApiModelProperty("비번, sns로그인의 경우 빈 문자열")
    private String password;
    @NonNull
    //@ApiModelProperty("로그인유형")
    private LOGIN_TYPE loginType;
    @NonNull
    //@ApiModelProperty("장치종류")
    private OS_TYPE osTp;
}
