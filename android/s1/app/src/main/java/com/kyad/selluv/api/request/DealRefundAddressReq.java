package com.kyad.selluv.api.request;


import lombok.Data;

@Data
public class DealRefundAddressReq {
//    @Range(min = 1, max = 3)
//    @ApiModelProperty("선택된 주소번호 1~3만 가능")
    private int addressSelectionIndex;
//    @ApiModelProperty("성함")
    private String addressName;
//    @NotNull
//    @ApiModelProperty("연락처")
    private String addressPhone;
//    @NotNull
//    @ApiModelProperty("주소")
    private String addressDetail;
//    @NotNull
//    @ApiModelProperty("상세주소(우편번호)")
    private String addressDetailSub;
}
