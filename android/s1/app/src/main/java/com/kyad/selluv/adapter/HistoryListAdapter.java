package com.kyad.selluv.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.deal.DealListDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.mypage.FeedbackActivity;
import com.kyad.selluv.mypage.PurchaseDetailActivity;
import com.kyad.selluv.mypage.PurchaseHistoryActivity;
import com.kyad.selluv.mypage.PurchaseReturnActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;

import retrofit2.Call;

public class HistoryListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public ArrayList<DealListDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public HistoryListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<DealListDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_purchase_history, parent, false);
            final DealListDto anItem = (DealListDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }
            ImageView iv_ware = (ImageView) convertView.findViewById(R.id.iv_ware);
            RelativeLayout rl_btn_list = (RelativeLayout) convertView.findViewById(R.id.rl_btn_list);
            RelativeLayout rly_main = (RelativeLayout) convertView.findViewById(R.id.rly_main);
            Button btn_white = (Button) convertView.findViewById(R.id.btn_white);
            Button btn_black = (Button) convertView.findViewById(R.id.btn_black);
            Button btn_gray = (Button) convertView.findViewById(R.id.btn_gray);
            TextView tv_brand_name = (TextView) convertView.findViewById(R.id.tv_brand_name);
            TextView tv_ware_name = (TextView) convertView.findViewById(R.id.tv_ware_name);
            TextView tv_size = (TextView) convertView.findViewById(R.id.tv_size);
            TextView tv_seller_name = (TextView) convertView.findViewById(R.id.tv_seller_name);
            TextView tv_price_nego = (TextView) convertView.findViewById(R.id.tv_price_nego);
            TextView tv_shipping_charge = (TextView) convertView.findViewById(R.id.tv_shipping_charge);
            TextView tv_nego_price = (TextView) convertView.findViewById(R.id.tv_nego_price);
            TextView tv_status = (TextView) convertView.findViewById(R.id.tv_status);

            String pdtPrice = Utils.getAttachCommaFormat((float) anItem.getDeal().getPdtPrice());
            String reqPrice = Utils.getAttachCommaFormat((float) anItem.getDeal().getReqPrice());
            tv_brand_name.setText(anItem.getDeal().getBrandEn());
            tv_ware_name.setText(anItem.getDeal().getPdtTitle());
            tv_size.setText(anItem.getDeal().getPdtSize());
            tv_seller_name.setText(anItem.getUsr().getUsrNckNm());
            ImageUtils.load(iv_ware.getContext(), anItem.getDeal().getPdtImg(), R.drawable.ic_logo, R.drawable.ic_logo, iv_ware);
            tv_shipping_charge.setText(pdtPrice + " 원");
            tv_price_nego.setText(reqPrice + " 원");
            tv_nego_price.setText(activity.getString(R.string.nego_price));

            rly_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toDetail(anItem.getDeal().getPdtUid());
                }
            });

            tv_seller_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getDeal().getSellerUsrUid());
                    activity.startActivity(intent);
                }
            });

            switch (anItem.getDeal().getStatus()) {
                case 11: //네고제안
                    rl_btn_list.setVisibility(View.GONE);
                    tv_status.setText(activity.getString(R.string.detail_nego_title));
                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    break;
                case 12:  //카운터 네고
                    tv_status.setText(activity.getString(R.string.counter_nego1));
                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle(activity.getString(R.string.nego_accept))
                                    .setMessage(activity.getString(R.string.seller_nego_refuse_allow_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqDealNegoAllowCounter(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });

                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle(activity.getString(R.string.nego_reject))
                                    .setMessage(activity.getString(R.string.seller_nego_refuse_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqDealNegoRefuseCounter(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });

                    btn_gray.setVisibility(View.GONE);
                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
                case 1: //배송준비
                    tv_status.setText(activity.getString(R.string.delivery_prepare));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_black.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);
                    btn_white.setText(R.string.order_cancel);
                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //주문최소 api
                            reqDealCancel(anItem.getDeal().getDealUid());
                        }
                    });
                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    if (Utils.getDiffTime2(anItem.getDeal().getPayTime().getTime()) > 3){
                        rl_btn_list.setVisibility(View.GONE);
                    }

                    break;
                case 2: //배송진행
                    if (anItem.getDeal().getVerifyPrice() > 0) {
                        tv_price_nego.setText("정품으로 판정 되었습니다.");
                        tv_nego_price.setText(activity.getString(R.string.result1));
                        rl_btn_list.setVisibility(View.GONE);
                        tv_status.setText(activity.getString(R.string.delivery_success));
                        tv_status.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toBuyDetail(anItem.getDeal().getDealUid());
                            }
                        });
                    } else {
                        rl_btn_list.setVisibility(View.GONE);
                        tv_nego_price.setVisibility(View.GONE);
                        tv_price_nego.setVisibility(View.GONE);
                        tv_status.setText(activity.getString(R.string.delivery_success));
                        tv_status.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toBuyDetail(anItem.getDeal().getDealUid());
                            }
                        });
                    }

                    break;

                case 3:  //정품인증
                    rl_btn_list.setVisibility(View.GONE);
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    tv_status.setText(activity.getString(R.string.activation));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    break;
                case 4: //배송완료

                    tv_status.setText(activity.getString(R.string.delivery_finishh));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);
                    btn_black.setText(R.string.buy_confirm);
                    btn_white.setText(R.string.refrund_req);
                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });


                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // 구매확정
                            reqDealComplete(anItem.getDeal().getDealUid());
                        }
                    });

                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //반품 신청
                            Intent intent = new Intent(activity, PurchaseReturnActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            activity.startActivity(intent);
                        }
                    });
                    break;

                case 5: //거래완료

                    tv_status.setText(activity.getString(R.string.deal_finish1));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);
                    btn_white.setVisibility(View.GONE);
                    btn_black.setText(activity.getString(R.string.deal_feedback));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });


                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //거래후기
                            Intent intent = new Intent(activity, FeedbackActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            intent.putExtra(Constants.NICKNAME, anItem.getUsr().getUsrNckNm());
                            intent.putExtra(Constants.PROFILE_IMG, anItem.getUsr().getProfileImg());
                            activity.startActivity(intent);
                        }
                    });
                    break;
                case 6: //정산완료
                    tv_status.setText(activity.getString(R.string.calculate_finish));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);
                    btn_white.setVisibility(View.GONE);
                    btn_black.setText(activity.getString(R.string.deal_feedback));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });


                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //거래후기
                            Intent intent = new Intent(activity, FeedbackActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            intent.putExtra(Constants.NICKNAME, anItem.getUsr().getUsrNckNm());
                            intent.putExtra(Constants.PROFILE_IMG, anItem.getUsr().getProfileImg());
                            activity.startActivity(intent);
                        }
                    });

                    break;
                case 10: //주문취소
                    tv_status.setText(activity.getString(R.string.order_cancel1));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    rl_btn_list.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    break;
                case 7: //반품접수

                    tv_status.setText(activity.getString(R.string.refrund_req1));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    rl_btn_list.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
                case 8: //반품승인
                    tv_status.setText(activity.getString(R.string.refrund_accept));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    rl_btn_list.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
                case 9: //반품완료

                    tv_status.setText(activity.getString(R.string.refrund_finish));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    rl_btn_list.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toBuyDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    public void toDetail(int pdtUid) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, pdtUid);
        activity.startActivity(intent);
    }

    public void toBuyDetail(int dealUid) {
        Intent intent = new Intent(activity, PurchaseDetailActivity.class);
        intent.putExtra(Constants.DEAL_UID, dealUid);
        activity.startActivity(intent);
    }

    //구매자  카운터네고승인
    private void reqDealNegoAllowCounter(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqDealNegoAllowCounter(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((PurchaseHistoryActivity)activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //구매자  카운터네고거절
    private void reqDealNegoRefuseCounter(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.reqDealNegoRefuseCounter(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {

                ((PurchaseHistoryActivity)activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //구매확정
    private void reqDealComplete(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqDealComplete(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((PurchaseHistoryActivity)activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //주문최소
    private void reqDealCancel(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqDealCancel(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((PurchaseHistoryActivity)activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }
}