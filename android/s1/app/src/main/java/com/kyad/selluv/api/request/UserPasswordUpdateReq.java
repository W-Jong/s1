package com.kyad.selluv.api.request;


import lombok.Data;

@Data
public class UserPasswordUpdateReq {

    //@Size(min = 1)
    //@ApiModelProperty("이전 비번")
    private String oldPassword;
    //@Size(min = 1)
    //@ApiModelProperty("새 비번")
    private String newPassword;
}
