/**
 * 홈
 */
package com.kyad.selluv.camera;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView;
import com.yalantis.ucrop.view.UCropView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.yalantis.ucrop.UCropActivity.DEFAULT_COMPRESS_FORMAT;
import static com.yalantis.ucrop.UCropActivity.DEFAULT_COMPRESS_QUALITY;
import static com.yalantis.ucrop.view.OverlayView.FREESTYLE_CROP_MODE_DISABLE;
import static com.yalantis.ucrop.view.OverlayView.FREESTYLE_CROP_MODE_ENABLE;

public class PicCropFragment extends DialogFragment {


    //UI Referencs
    @BindView(R.id.tv_order)
    TextView tv_order;
    @BindView(R.id.ucrop)
    UCropView mUCropView;
    @BindView(R.id.ib_left)
    ImageButton ib_left;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.rl_bottombar)
    RelativeLayout rl_bottombar;
    @BindView(R.id.ib_close)
    ImageButton ib_close;
    @BindView(R.id.btn_cancel)
    Button btn_cancel;


    //Vars
    View rootView;
    public String title = "";
    public Bitmap image = null;
    public String path = null;
    public boolean fromMypage = false;
    public GalleryFragment parent = null;

    private GestureCropImageView mGestureCropImageView;
    private OverlayView mOverlayView;
    private View mBlockingView;

    public GalleryFragment.ImageSelectInterface imageSelectInterface = null;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_pic_edit, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @OnClick({R.id.btn_cancel, R.id.ib_close})
    void onCancel(View v) {
        dismiss();
    }

    @OnClick(R.id.btn_finish)
    void onFinish(View v) {
        cropAndSaveImage();
    }


    private void loadLayout() {

        if (fromMypage) {
            ib_close.setVisibility(View.VISIBLE);
            ib_close.setBackgroundResource(R.drawable.header_ic_white_arrow);
            btn_cancel.setVisibility(View.INVISIBLE);
        } else {
            ib_close.setVisibility(View.INVISIBLE);
            btn_cancel.setVisibility(View.VISIBLE);
        }

        rl_bottombar.setVisibility(View.INVISIBLE);
        ib_left.setVisibility(View.INVISIBLE);
        ib_right.setVisibility(View.INVISIBLE);
        tv_order.setText(title);

        mGestureCropImageView = mUCropView.getCropImageView();
        mOverlayView = mUCropView.getOverlayView();
        mOverlayView.setCropGridColumnCount(0);
        mOverlayView.setCropGridRowCount(0);
        mGestureCropImageView.setTransformImageListener(mImageListener);

        addBlockingView();
        setCropOptions();
        setImageData();

        if (parent != null && parent.frameType == 1) {
            mOverlayView.setCircleDimmedLayer(true);
            mOverlayView.setFreestyleCropMode(FREESTYLE_CROP_MODE_DISABLE);
            mOverlayView.setShowCropFrame(false);
        } else if (parent != null && parent.frameType == 2) {
            mOverlayView.setFreestyleCropMode(FREESTYLE_CROP_MODE_DISABLE);
            mGestureCropImageView.setTargetAspectRatio(1);
        }

    }

    private void setCropOptions() {
        mGestureCropImageView.setRotateEnabled(false);
        mOverlayView.setFreestyleCropMode(FREESTYLE_CROP_MODE_ENABLE);
    }

    /**
     * This method extracts all data from the incoming intent and setups views properly.
     */
    private void setImageData() {
        if (path.length() != 0 && path != null) {
            try {
                mGestureCropImageView.setImageUri(Uri.fromFile(new File(path)), Uri.fromFile(new File(path)));
            } catch (Exception e) {
                mGestureCropImageView.setImageBitmap(image != null ? image : BitmapFactory.decodeFile(path));
                e.printStackTrace();
            }
        } else {
            mGestureCropImageView.setImageBitmap(image != null ? image : BitmapFactory.decodeFile(path));
        }
    }

    protected void cropAndSaveImage() {
        if (path.length() == 0 && path == null) {
            return;
        }
        mBlockingView.setClickable(true);
        try {
            mGestureCropImageView.cropAndSaveImage(DEFAULT_COMPRESS_FORMAT, DEFAULT_COMPRESS_QUALITY, new BitmapCropCallback() {
                @Override
                public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
                    imageSelectInterface.onImageSelect(BitmapFactory.decodeFile(resultUri.getPath()));

                    if (!fromMypage) {
                        ((SellPicActivity) getActivity()).reloadThumbnails();
                    }
                    if (parent != null)
                        parent.dismiss();
                    dismiss();
                    if (parent != null && parent.fromPage == SellPicActivity.FROM_PAGE_MYPAGE && getActivity() != null)
                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }

                @Override
                public void onCropFailure(@NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    /**
     * Adds view that covers everything below the Toolbar.
     * When it's clickable - user won't be able to click/touch anything below the Toolbar.
     * Need to block user input while loading and cropping an image.
     */
    private void addBlockingView() {
        if (mBlockingView == null) {
            mBlockingView = new View(getActivity());
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            lp.addRule(RelativeLayout.BELOW, R.id.rl_actionbar);
            lp.addRule(RelativeLayout.ABOVE, R.id.rl_bottombar);
            mBlockingView.setLayoutParams(lp);
            mBlockingView.setClickable(true);
        }

        ((RelativeLayout) rootView.findViewById(R.id.rl_main)).addView(mBlockingView);
    }

    private TransformImageView.TransformImageListener mImageListener = new TransformImageView.TransformImageListener() {
        @Override
        public void onRotate(float currentAngle) {

        }

        @Override
        public void onScale(float currentScale) {

        }

        @Override
        public void onLoadComplete() {
            mUCropView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());
            mBlockingView.setClickable(false);
        }

        @Override
        public void onLoadFailure(@NonNull Exception e) {
        }
    };
}
