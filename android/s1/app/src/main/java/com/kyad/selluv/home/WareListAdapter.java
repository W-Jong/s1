package com.kyad.selluv.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.MainActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.PdtListDto;
import com.kyad.selluv.api.dto.pdt.PdtStyleDao;
import com.kyad.selluv.api.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluv.api.enumtype.FEED_TYPE;
import com.kyad.selluv.brand.BrandDetailActivity;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.IndicatorView;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.RoundRectCornerImageView;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.viewholder.WareItemViewHolder;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.mypage.WishAlarmActivity;
import com.kyad.selluv.top.TopLikeActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import static android.graphics.Paint.STRIKE_THRU_TEXT_FLAG;
import static com.kyad.selluv.common.Constants.DATA_FEED;
import static com.kyad.selluv.common.Constants.DATA_STYLE;
import static com.kyad.selluv.common.Constants.DATA_WARE;
import static com.kyad.selluv.common.Constants.INITIAL_TAB_INDEX;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_FAME;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_FEED;
import static com.kyad.selluv.common.Constants.WIRE_FRAG_NEW;

public class WareListAdapter extends RecyclerView.Adapter {
    public List<PdtListDto> dataList = new ArrayList<>(); //Temporary image ids, for UI test
    private WareListFragment fragment = null;
    private BaseActivity activity = null;
    private int dataType = DATA_WARE;
    public boolean isMiniImageShow = true;
    public int fragType = 0;

    public WareListAdapter(WareListFragment fragment, int dataType) {
        this.fragment = fragment;
        this.dataType = dataType;
    }

    public WareListAdapter(BaseActivity activity, int dataType) {
        this.activity = activity;
        this.dataType = dataType;
    }

    private BaseActivity getActivity() {
        return fragment != null ? (BaseActivity) fragment.getActivity() : activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(fragment != null ? fragment.getActivity() : activity).inflate(R.layout.item_ware, null);

        WareItemViewHolder holder = new WareItemViewHolder(itemView);
        holder.padding_start = itemView.findViewById(R.id.padding_start);
        holder.padding_end = itemView.findViewById(R.id.padding_end);
        holder.iv_ware_img = (RoundRectCornerImageView) itemView.findViewById(R.id.iv_ware_img);
        holder.iv_photo = (CircularImageView) itemView.findViewById(R.id.iv_photo);
        holder.iv_ministyle = (ImageView) itemView.findViewById(R.id.iv_ministyle);
        holder.rl_mini_style = (RelativeLayout) itemView.findViewById(R.id.rl_mini_style);
        holder.iv_heart = (ImageView) itemView.findViewById(R.id.iv_like);
        holder.iv_ware_stat = (ImageView) itemView.findViewById(R.id.iv_ware_stat);
        holder.tv_ware_eng_name = itemView.findViewById(R.id.tv_ware_eng_name);
        holder.tv_ware_name = itemView.findViewById(R.id.tv_ware_name);
        holder.tv_ware_price = (TextView) itemView.findViewById(R.id.tv_ware_price);
        holder.ll_sale_info = (LinearLayout) itemView.findViewById(R.id.ll_sale_info);
        holder.tv_sale_price = (TextView) itemView.findViewById(R.id.tv_sale_price);
        holder.tv_sale_rate = (TextView) itemView.findViewById(R.id.tv_sale_rate);
        holder.tv_size = (TextView) itemView.findViewById(R.id.tv_size);
        holder.tv_like_cnt = (TextView) itemView.findViewById(R.id.tv_follow_cnt);
        holder.tv_nickname = (TextView) itemView.findViewById(R.id.tv_right_nickname);
        holder.tv_update_time = (TextView) itemView.findViewById(R.id.tv_update_time);
        holder.rl_like = (RelativeLayout) itemView.findViewById(R.id.rl_like);
        holder.iv_price_down = itemView.findViewById(R.id.iv_price_down);
        itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        final WareItemViewHolder vh = (WareItemViewHolder) viewHolder;

//        if (position % 2 == 0) {
//            vh.padding_start.setVisibility(View.VISIBLE);
//            vh.padding_end.setVisibility(View.GONE);
//        } else {
//            vh.padding_start.setVisibility(View.GONE);
//            vh.padding_end.setVisibility(View.VISIBLE);
//        }

        final PdtListDto item = dataList.get(position);

        String url = item.getPdt().getProfileImg();
        if (isStyle()) {
            url = item.getPdtStyle().getStyleImg();
        }
        /*
        Glide.with(vh.iv_ware_img.getContext()).load(url).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                int width = resource.getIntrinsicWidth();
                int height = resource.getIntrinsicHeight();
                final float ratio = (float) height / width;

                vh.iv_ware_img.setRatio(ratio);
                vh.iv_ware_img.setImageDrawable(resource);
            }
        });*/

        int width = (Globals.screenSize.x - 30) / 2;
        float ratio = 0;
        if (isStyle()) {
            ratio = (float) item.getStyleProfileImgHeight() / item.getStyleProfileImgWidth();
        } else {
            ratio = (float) item.getPdtProfileImgHeight() / item.getPdtProfileImgWidth();
        }

        float height = width * ratio;

//        int tempImageHeight = ((position % 2 == 0) ? 7 : 8) * 26;
//        vh.iv_ware_img.getLayoutParams().height = (int) height;//Utils.dpToPixel(vh.iv_ware_img.getContext(), height);
//        vh.iv_ware_img.requestLayout();
        ImageUtils.load(vh.iv_ware_img.getContext(), url, vh.iv_ware_img);

        vh.iv_ware_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((dataType & DATA_STYLE) != 0) {
                    showImagePopup(item);
                } else {
                    toDetail(view, item, position);
                }
            }
        });

        if ((dataType & DATA_STYLE) != 0 || !isMiniImageShow) {
            vh.rl_mini_style.setVisibility(View.INVISIBLE);
        } else {
            vh.rl_mini_style.setVisibility(View.VISIBLE);
            if (isStyle()) {
                url = item.getPdt().getProfileImg();
            } else {
                if (item.getPdtStyle() != null)
                    url = item.getPdtStyle().getStyleImg();
                else {
                    url = null;
                }
            }

            if (url != null) {
                ImageUtils.load(vh.iv_ministyle.getContext(), url, vh.iv_ministyle);
                vh.iv_ministyle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showImagePopup(item);
                    }
                });
            } else {
                vh.rl_mini_style.setVisibility(View.INVISIBLE);
            }
        }

        vh.tv_like_cnt.setText(String.valueOf(item.getPdtLikeCount()));
        if (item.getPdtLikeStatus() == false) {
            vh.iv_heart.setImageResource(R.drawable.btn_list_like_none);
            vh.rl_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestPdtLike(item, vh.rl_like);
                }
            });
        } else {
            vh.iv_heart.setImageResource(R.drawable.btn_list_like_push);
            vh.rl_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestPdtUnLike(item, vh.rl_like);
                }
            });
        }

        vh.tv_ware_eng_name.setText(item.getBrand().getNameEn());
        vh.tv_ware_eng_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Utils.currentActivity, BrandDetailActivity.class);
                intent.putExtra(BrandDetailActivity.BRAND_UID, item.getPdt().getBrandUid());
                Utils.currentActivity.startActivity(intent);
            }
        });

        // 한글브랜드명_컬러_모델명_마지막카테고리
        String wareName = item.getWareName();
        vh.tv_ware_name.setText(wareName);
        vh.tv_ware_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item, position);
            }
        });

        vh.tv_ware_price.setText("₩" + Utils.getAttachCommaFormat((int) item.getPdt().getPrice()));
        vh.tv_ware_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item, position);
            }
        });

        vh.tv_size.setText("Size. " + item.getPdt().getPdtSize());
        vh.tv_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item, position);
            }
        });

        if ((dataType & DATA_FEED) != 0) {
            vh.tv_update_time.setText(Utils.getDiffTime(((UsrFeedPdtListDto) item).getUsrFeed().getRegTime().getTime()));

            FEED_TYPE feedType = null;
            int type = ((UsrFeedPdtListDto) item).getUsrFeed().getType();
            try {
                feedType = FEED_TYPE.values()[((UsrFeedPdtListDto) item).getUsrFeed().getType() - 1];
            } catch (Exception e) {
                feedType = FEED_TYPE.values()[0];
            }
            if (feedType.getCode() < FEED_TYPE.FEED04_FOLLOWING_USER_WISH.getCode()) {
                vh.iv_photo.setImageResource(Constants.feedTypeImg[feedType.getCode() - 1]);
                vh.tv_nickname.setText(Constants.feedTypeTitle[feedType.getCode() - 1]);
            } else {
                ImageUtils.load(vh.iv_photo.getContext(), item.getUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, vh.iv_photo);
            }
            switch (feedType) {
                case FEED01_FINDING_PDT:
                    setClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((BaseActivity) (fragment != null ? fragment.getActivity() : activity)).startActivity(WishAlarmActivity.class, false, 0, 0);
                        }
                    }, vh.iv_photo, vh.tv_nickname, vh.tv_update_time);
                    break;

                case FEED02_LIKE_PDT_SALE:
                    setClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((BaseActivity) (fragment != null ? fragment.getActivity() : activity)).startActivity(TopLikeActivity.class, false, 0, 0);
                        }
                    }, vh.iv_photo, vh.tv_nickname, vh.tv_update_time);
                    break;

                case FEED03_FOLLOWING_BRAND:
                    setClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MainActivity) (fragment != null ? fragment.getActivity() : activity)).gotoBrandPage(item.getBrand().getBrandUid() + 1);
                        }
                    }, vh.iv_photo, vh.tv_nickname, vh.tv_update_time);
                    break;

                case FEED04_FOLLOWING_USER_WISH:
                    vh.tv_nickname.setText(((UsrFeedPdtListDto) item).getPeerUsr().getUsrNckNm() + " " + (fragment != null ? fragment.getActivity() : activity).getString(R.string.detail_item_title));
                    setClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), UserActivity.class);
                            intent.putExtra(UserActivity.USR_UID, item.getUsr().getUsrUid());
                            intent.putExtra(INITIAL_TAB_INDEX, 1);
                            (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                        }
                    }, vh.iv_photo, vh.tv_nickname, vh.tv_update_time);
                    ImageUtils.load(vh.iv_photo.getContext(), ((UsrFeedPdtListDto) item).getPeerUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, vh.iv_photo);
                    break;

                case FEED05_FOLLOWING_USER_STYLE:
                    vh.tv_nickname.setText(((UsrFeedPdtListDto) item).getPeerUsr().getUsrNckNm() + " " + (fragment != null ? fragment.getActivity() : activity).getString(R.string.detail_style_title));
                    setClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), UserActivity.class);
                            intent.putExtra(UserActivity.USR_UID, item.getUsr().getUsrUid());
                            intent.putExtra(INITIAL_TAB_INDEX, 2);
                            (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                        }
                    }, vh.iv_photo, vh.tv_nickname, vh.tv_update_time);
                    ImageUtils.load(vh.iv_photo.getContext(), ((UsrFeedPdtListDto) item).getPeerUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, vh.iv_photo);
                    break;

                case FEED06_FOLLOWING_USER_PDT:
                    vh.tv_nickname.setText(((UsrFeedPdtListDto) item).getPeerUsr().getUsrNckNm());
                    setClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), UserActivity.class);
                            intent.putExtra(UserActivity.USR_UID, item.getUsr().getUsrUid());
                            (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                        }
                    }, vh.iv_photo, vh.tv_nickname, vh.tv_update_time);
                    ImageUtils.load(vh.iv_photo.getContext(), ((UsrFeedPdtListDto) item).getPeerUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, vh.iv_photo);
                    break;

            }
        } else {
            vh.tv_update_time.setText(Utils.getDiffTime(item.getPdt().getEdtTime().getTime()));
            ImageUtils.load(vh.iv_photo.getContext(), item.getUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, vh.iv_photo);
            vh.iv_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, item.getUsr().getUsrUid());
                    (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                }
            });
            vh.tv_nickname.setText(item.getUsr().getUsrNckNm());
            vh.tv_nickname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, item.getUsr().getUsrUid());
                    (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                }
            });

            vh.tv_update_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, item.getUsr().getUsrUid());
                    (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                }
            });
        }

        vh.ll_sale_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item, position);
            }
        });

        if (item.getPdt().getStatus() == 2)    //sold out
        {
            vh.iv_ware_stat.setImageResource(R.drawable.mark_sold_out);
            vh.iv_ware_stat.setVisibility(View.VISIBLE);
        } else {
            vh.iv_ware_stat.setVisibility(View.GONE);
        }

        if (item.getPdt().getPrice() >= item.getPdt().getOriginPrice()) {
            vh.tv_sale_price.setVisibility(View.GONE);
            vh.tv_sale_rate.setVisibility(View.GONE);
            vh.iv_price_down.setVisibility(View.GONE);
            vh.ll_sale_info.setVisibility(View.GONE);

            vh.tv_ware_price.setText("₩" + Utils.getAttachCommaFormat((int) item.getPdt().getPrice()));
            vh.tv_ware_price.setPaintFlags(ANTI_ALIAS_FLAG);
        } else {
            vh.tv_sale_price.setVisibility(View.VISIBLE);
            vh.tv_sale_rate.setVisibility(View.VISIBLE);
            vh.iv_price_down.setVisibility(View.VISIBLE);
            vh.ll_sale_info.setVisibility(View.VISIBLE);

            vh.tv_sale_price.setText("₩" + Utils.getAttachCommaFormat((int) item.getPdt().getPrice()));
            vh.tv_ware_price.setText("₩" + Utils.getAttachCommaFormat((int) item.getPdt().getOriginPrice()));
            vh.tv_ware_price.setPaintFlags(STRIKE_THRU_TEXT_FLAG | ANTI_ALIAS_FLAG);
            vh.tv_ware_price.setTextColor((fragment != null ? fragment.getActivity() : activity).getResources().getColor(R.color.color_999999));
            int percent = (int) ((item.getPdt().getOriginPrice() - item.getPdt().getPrice()) * 100.0f / item.getPdt().getOriginPrice());
            vh.iv_price_down.setImageResource(R.drawable.mark_price_down);
            vh.tv_sale_rate.setText(percent + "%");
        }


        //메인의 피드/인기/신상이면서 수정했던 상품이면 수정된시간옆에 "수정"이라는 글자를 넣는 조작
        if ((fragType == WIRE_FRAG_FEED || fragType == WIRE_FRAG_NEW || fragType == WIRE_FRAG_FAME) && item.getPdt().isEdited()) {
            vh.tv_update_time.setText(Utils.getDiffTime(item.getPdt().getEdtTime().getTime()) + " 수정");
        }
    }

    public void toDetail(View view, PdtListDto item, int itemPos) {

        //ViewPosition position = ViewPosition.from(view);
        Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, item.getPdt().getPdtUid());
        (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
    }

    public void showImagePopup(PdtListDto item) {

        final BaseActivity finalActivity = getActivity();
        finalActivity.showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<PdtStyleDao>>> genRes = restAPI.pdtStyleDetail(Globals.userToken, item.getPdtStyle().getPdtStyleUid());

        final int pdtUid = item.getPdt().getPdtUid();
        genRes.enqueue(new TokenCallback<List<PdtStyleDao>>(finalActivity) {
            @Override
            public void onSuccess(List<PdtStyleDao> response) {
                finalActivity.closeLoading();

                final PopupDialog popup = new PopupDialog((BaseActivity) (fragment != null ? fragment.getActivity() : activity), R.layout.popup_main_style).setBlur().setFullScreen();

                final ViewPager vp_image = (ViewPager) popup.findView(R.id.vp_image);
                IndicatorView indicator = (IndicatorView) popup.findView(R.id.rl_indicator);
                List<String> imageUrlList = new ArrayList<>();
                for (PdtStyleDao item : response)
                    imageUrlList.add(item.getStyleImg());
                Utils.addTouchableImagesAndIndicator(vp_image, imageUrlList, indicator);

                popup.setOnCancelClickListener(new PopupDialog.bindOnClick() {
                    @Override
                    public void onClick() {
                        popup.dismiss();
                    }
                });
                popup.setOnOkClickListener(new PopupDialog.bindOnClick() {
                    @Override
                    public void onClick() {
                        Intent intent = new Intent((fragment != null ? fragment.getActivity() : activity), DetailActivity.class);
                        intent.putExtra(DetailActivity.PDT_UID, pdtUid);
                        (fragment != null ? fragment.getActivity() : activity).startActivity(intent);
                    }
                });
                popup.show();
            }

            @Override
            public void onFailed(Throwable t) {
                finalActivity.closeLoading();
            }
        });
    }

    public void setClickListener(View.OnClickListener listener, View... views) {
        for (View v :
                views) {
            v.setOnClickListener(listener);
        }
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    private boolean isStyle() {
        if ((dataType & DATA_STYLE) == DATA_STYLE) {
            return true;
        }

        return false;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public PdtListDto getItem(int position) {
        return dataList.get(position);
    }

    private void requestPdtLike(final PdtListDto item, final View hearView) {
        final BaseActivity finalActivity = getActivity();
        finalActivity.showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.likePdt(Globals.userToken, item.getPdt().getPdtUid());
        genRes.enqueue(new TokenCallback<Void>(finalActivity) {
            @Override
            public void onSuccess(Void response) {
                finalActivity.closeLoading();

                Utils.heartAnimation(finalActivity, hearView, true);
                if ((dataType & DATA_STYLE) != 0) {
                    for (int i = 0; i < dataList.size(); i++) {
                        PdtListDto info = dataList.get(i);
                        if (item.getPdt().getPdtUid() == info.getPdt().getPdtUid()) {
                            int lineCnt = info.getPdtLikeCount() + 1;
                            info.setPdtLikeCount(lineCnt);
                            info.setPdtLikeStatus(true);
                        }
                    }
                } else {
                    int lineCnt = item.getPdtLikeCount() + 1;
                    item.setPdtLikeCount(lineCnt);
                    item.setPdtLikeStatus(true);
                }

                notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                finalActivity.closeLoading();

                if (t instanceof com.kyad.selluv.api.APIException) {
                    if (((com.kyad.selluv.api.APIException)t).getCode().equals("150")){
                        finalActivity.showLoginSuggestionPopup();
                    }
                }

            }
        });
    }

    private void requestPdtUnLike(final PdtListDto item, final View hearView) {
        final BaseActivity finalActivity = getActivity();
        finalActivity.showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.unLikePdt(Globals.userToken, item.getPdt().getPdtUid());

        genRes.enqueue(new TokenCallback<Void>(finalActivity) {
            @Override
            public void onSuccess(Void response) {
                finalActivity.closeLoading();

                Utils.heartAnimation(getActivity(), hearView, false);

                if ((dataType & DATA_STYLE) != 0) {
                    for (int i = 0; i < dataList.size(); i++) {
                        PdtListDto info = dataList.get(i);
                        if (item.getPdt().getPdtUid() == info.getPdt().getPdtUid()) {
                            int lineCnt = info.getPdtLikeCount() - 1;
                            info.setPdtLikeCount(lineCnt);
                            info.setPdtLikeStatus(false);
                        }
                    }
                } else {
                    int lineCnt = item.getPdtLikeCount() - 1;
                    item.setPdtLikeCount(lineCnt);
                    item.setPdtLikeStatus(false);
                }
                notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                finalActivity.closeLoading();
                if (t instanceof com.kyad.selluv.api.APIException) {
                    if (((com.kyad.selluv.api.APIException)t).getCode().equals("150")){
                        finalActivity.showLoginSuggestionPopup();
                    }
                }
            }
        });
    }
}
