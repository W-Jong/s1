package com.kyad.selluv.sell;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.AlphabetIndexer.widget.IndexableListView;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.BRAND_LANG;
import com.kyad.selluv.sell.adapter.SellBrandAddListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.ghyeok.stickyswitch.widget.StickySwitch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.RelativeLayout.CENTER_IN_PARENT;
import static com.kyad.selluv.sell.SellMainActivity.sellType;

public class SellBrandAddActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.rl_hint)
    RelativeLayout rl_hint;
    @BindView(R.id.edt_search)
    EditText edt_search;
    @BindView(R.id.lv_filter)
    IndexableListView lv_filter;
    @BindView(R.id.iv_search)
    ImageView iv_search;
    @BindView(R.id.tv_hint)
    TextView tv_hint;
    @BindView(R.id.ib_clear)
    ImageButton ib_clear;
    @BindView(R.id.tv_cancel)
    TextView tv_cancel;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.sw_lang)
    StickySwitch sw_lang;
    @BindView(R.id.tv_right)
    TextView tv_rignt;
    @BindView(R.id.tv_title)
    TextView tv_title;


    //Variables
    BRAND_LANG selectedLang = BRAND_LANG.ENGLISH;

    String filterText = "";
    List<BrandMiniDto> brandItems;


    private boolean isFromFilter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_add);

        isFromFilter = getIntent().getBooleanExtra(Constants.IS_FROM_FILTER, false);

        if (!isFromFilter)
            addForSellActivity();

        loadLayout();
        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isFromFilter)
            removeForSellActivity();
    }

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        finish();
    }

    /**
     * 검색결과 보기 or 상품등록하기
     * @param v
     */
    @OnClick(R.id.btn_ok)
    void onOK(View v) {
        if (isFromFilter) {
            sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
            setResult(RESULT_OK);
            finish();
        } else {
            showBrandAddPopup();
        }
    }

    @OnClick(R.id.ib_clear)
    void onClear(View v) {
        edt_search.setText("");
    }

    @OnClick(R.id.tv_cancel)
    void onCancel(View v) {
        edt_search.setText("");
        edt_search.clearFocus();
    }


    private void loadLayout() {

        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        tv_title.setText(getString(R.string.brand));

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterText = charSequence.toString();
                filterBrand();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edt_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    tv_hint.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) rl_hint.getLayoutParams();
                    rlp.removeRule(CENTER_IN_PARENT);
                    tv_cancel.setVisibility(View.VISIBLE);
                    ib_clear.setVisibility(View.VISIBLE);
                } else {
                    CharSequence charSequence = edt_search.getText();
                    if (charSequence.length() == 0) {
                        tv_hint.setVisibility(View.VISIBLE);
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) rl_hint.getLayoutParams();
                        rlp.addRule(CENTER_IN_PARENT);
                        tv_cancel.setVisibility(View.GONE);
                        ib_clear.setVisibility(View.GONE);
                    } else {
                        tv_hint.setVisibility(View.GONE);
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) rl_hint.getLayoutParams();
                        rlp.removeRule(CENTER_IN_PARENT);
                        tv_cancel.setVisibility(View.VISIBLE);
                        ib_clear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        edt_search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    Utils.hideSoftKeyboard(SellBrandAddActivity.this);
                    filterBrand();
                }
                return false;
            }
        });

        if (isFromFilter) {
            tv_rignt.setVisibility(View.VISIBLE);
            sw_lang.setVisibility(View.INVISIBLE);
        } else {
            tv_rignt.setVisibility(View.INVISIBLE);
            sw_lang.onSelectedChangeListener = new StickySwitch.OnSelectedChangeListener() {
                @Override
                public void onSelectedChange(StickySwitch.Direction direction, String rightTextAlpha) {
                    if (direction == StickySwitch.Direction.LEFT)
                        selectedLang = BRAND_LANG.ENGLISH;
                    else
                        selectedLang = BRAND_LANG.KOREAN;

                    filterBrand();
                }
            };

        }
    }

    /**
     * 브랜드 리스트
     */
    private void loadData() {

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<BrandMiniDto>>> genRes = restAPI.getBrand(Globals.userToken);
        genRes.enqueue(new Callback<GenericResponse<List<BrandMiniDto>>>() {

            @Override
            public void onResponse(Call<GenericResponse<List<BrandMiniDto>>> call, Response<GenericResponse<List<BrandMiniDto>>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    brandItems = response.body().getData();

                    updateLang(brandItems);
                } else {//실패
                    Toast.makeText(SellBrandAddActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<List<BrandMiniDto>>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(SellBrandAddActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

    void filterBrand() {

        final List<BrandMiniDto> filterResult = new ArrayList<>();
        for (BrandMiniDto brand : brandItems) {
            if (filterText.length() == 0)
                filterResult.add(new BrandMiniDto(brand));
            else {
                if (brand.getNameEn().toLowerCase().contains(filterText.toLowerCase())) {
                    filterResult.add(new BrandMiniDto(brand));
                } else if (brand.getNameKo().contains(filterText)) {
                    filterResult.add(new BrandMiniDto(brand));
                }
            }
        }

        updateLang(filterResult);

        if (isFromFilter) {
            btn_ok.setText(getString(R.string.search_result));
        } else {
            if (filterResult.size() == 0) {
                btn_ok.setText(String.format(getString(R.string.reg_ware_with_brand), filterText));
                btn_ok.setVisibility(View.VISIBLE);
            } else {
                btn_ok.setVisibility(View.GONE);
            }
        }

    }

    /**
     * 서버에 등록되어있지 않는 알수 없는 브랜드로 등록하는 팝업
     */
    void showBrandAddPopup() {
        final PopupDialog popup = new PopupDialog(SellBrandAddActivity.this, R.layout.sell_direct_brand_add_popup)
                .setFullScreen()
                .setCancelable(true)
                .setBackGroundCancelable(true)
                .setBackGroundColor(getResources().getColor(R.color.color_000000_60));
        final TextView tv_guide1_1 = (TextView) popup.findView(R.id.tv_guide1_1);
        SpannableString text = new SpannableString(filterText + getString(R.string.unregistered_brand));
        text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 0, filterText.length(), 0);
        text.setSpan(new ForegroundColorSpan(Color.BLACK), filterText.length() + 1, text.length(), 0);
        tv_guide1_1.setText(text);
        popup.setOnCancelClickListener(new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });
        popup.setOnOkClickListener(new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {

                cmdRegNewBrand(filterText);
            }
        });
        popup.show();
    }

    private void updateLang(List<BrandMiniDto> brands) {
        if (isFromFilter) {
            for (int i = 0; i < Globals.searchReq.getBrandUidList().size(); i++) {
                for (int j = 0; j < brands.size(); j++)
                    if (Globals.searchReq.getBrandUidList().get(i) == brands.get(j).getBrandUid()) {
                        brands.get(j).setI_followed(1);
                    }
            }
        }
        SellBrandAddListAdapter adapter = new SellBrandAddListAdapter(this, selectedLang, isFromFilter);
        adapter.setData(brands);
        adapter.reloadData(selectedLang);
        lv_filter.setAdapter(adapter);
    }


    private void cmdRegNewBrand(String brand) {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<BrandMiniDto>> genRes = restAPI.regNewBrand(Globals.userToken, brand);
        genRes.enqueue(new TokenCallback<BrandMiniDto>(this) {
            @Override
            public void onSuccess(BrandMiniDto response) {
                closeLoading();

                Globals.selectedBrand = response;
                Globals.pdtCreateReq.setBrandUid(Globals.selectedBrand.getBrandUid());
                if (sellType == 0) {
                    Intent i = new Intent(SellBrandAddActivity.this, SellPicActivity.class);
                    i.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_SELL);
                    startActivity(i);
                } else {
                    startActivity(SellValetRequestActivity.class, false, 0, 0);
                }

            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
