package com.kyad.selluv.mypage;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.ContentReq;
import com.kyad.selluv.api.request.UserSellerSettingReq;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.SellerFreeFragment;
import com.kyad.selluv.fragment.SellerVacationFragment;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class SellerSettingActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    int selectedTab = 1;
    private PageAdapter pagerAdapter;
    SellerFreeFragment freeFragment;
    SellerVacationFragment vacationFragment;

    private Boolean isFreeSend = false;
    private long freeSendPrice = -1;
    private Boolean isSleep = false;
    private int timeOutDate = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_seller);
        setStatusBarWhite();
        initFragment();
        loadLayout();
    }

    private void loadLayout() {
        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);
        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle(0);

        //
        if (Globals.myInfo.getUsr().getFreeSendPrice() == -1) {
            isFreeSend = false;
        } else {
            isFreeSend = true;
        }

        if (Globals.myInfo.getUsr().getSleepYn() == 0) {
            isSleep = false;
        } else {
            isSleep = true;
        }
        freeSendPrice = 0;
        timeOutDate = 1;
    }

    private void initFragment() {
        if (freeFragment == null) {
            freeFragment = new SellerFreeFragment();
        }
        if (vacationFragment == null) {
            vacationFragment = new SellerVacationFragment();
        }
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = freeFragment;
                    break;
                case 1:
                    frag = vacationFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public void setFreeSend(boolean status) {
        isFreeSend = status;
    }

    public void setIsSleep(boolean status) {
        isSleep = status;
    }

    public void setTimeOutDate(int value) {
        timeOutDate = value;
    }

    public void setFreeSendPrice(long value) {
        freeSendPrice = value;
    }

    public void reqSellerSetting() {

        UserSellerSettingReq dicInfo = new UserSellerSettingReq();
        dicInfo.setFreeSendPrice(freeSendPrice);
        dicInfo.setIsFreeSend(isFreeSend);
        dicInfo.setTimeOutDate(timeOutDate);
        dicInfo.setIsSleep(isSleep);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.sellerSetting(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {

                Utils.showToast(SellerSettingActivity.this, getString(R.string.setting_success));

                if (isFreeSend) {
                    Globals.myInfo.getUsr().setFreeSendPrice(freeSendPrice);
                } else {
                    Globals.myInfo.getUsr().setFreeSendPrice(-1);
                }
                freeFragment.showSwtichStatus();

                if (isSleep) {
                    Globals.myInfo.getUsr().setSleepYn(1);
                } else {
                    Globals.myInfo.getUsr().setSleepYn(0);
                }
                vacationFragment.setSwitchStatus();

            }

            @Override
            public void onFailed(Throwable t) {
            }
        });

    }
}
