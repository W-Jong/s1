package com.kyad.selluv.api.dto.other;

import lombok.Data;

@Data
public class SystemSettingDto {
    //("구매적립(%)")
    private long dealReward;
    //("구매후기리워드(원)")
    private long buyReviewReward;
    //("판매후기리워드(원)")
    private long sellReviewReward;
    //("가품리워드(원)")
    private long fakeReward;
    //("공유리워드(원)")
    private long shareRewrad;
    //("초대자에게 지급되는 가입리워드(원)")
    private long inviterRewrad;
    //("가입자에게 지급되는 가입리워드(원)")
    private long invitedUserReward;
    //("초대자에게 가입자의 첫 구매 후 지급되는 리워드(원)")
    private long invitedUserFirstDealReward;
}
