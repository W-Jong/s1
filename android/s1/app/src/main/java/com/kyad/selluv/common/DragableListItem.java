package com.kyad.selluv.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * A custom FrameLayout that intercept touch events and routes them the a JBHorizontalSwipe controller.
 */
public class DragableListItem extends FrameLayout {

    public DragableListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DragableListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        return super.onTouchEvent(ev);
    }
}
