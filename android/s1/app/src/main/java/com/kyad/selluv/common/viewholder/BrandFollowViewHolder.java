package com.kyad.selluv.common.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BrandFollowViewHolder extends ViewHolder {
    public ImageButton ib_more;
    public TextView tv_brand_name;
    public LinearLayout ll_ware_list;
    public RelativeLayout rl_none;

    public BrandFollowViewHolder(View itemView) {
        super(itemView);
    }
}
