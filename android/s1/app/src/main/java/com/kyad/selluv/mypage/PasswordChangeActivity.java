package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.DbManager;
import com.kyad.selluv.R;
import com.kyad.selluv.ShareManager;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.api.request.UserPasswordUpdateReq;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.ShareManager.SM_LAST_LOGIN_ID;

public class PasswordChangeActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.edt_old)
    EditText edt_old;
    @BindView(R.id.edt_new)
    EditText edt_new;
    @BindView(R.id.edt_new_confirm)
    EditText edt_new_confirm;
    @BindView(R.id.btn_change)
    Button btn_change;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_password);
        setStatusBarWhite();
        loadLayout();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edt_old.getText().length() > 0 && edt_new.getText().length() > 0 && edt_new_confirm.getText().length() > 0)
                    btn_change.setEnabled(true);
                else
                    btn_change.setEnabled(false);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        edt_old.addTextChangedListener(watcher);
        edt_new.addTextChangedListener(watcher);
        edt_new_confirm.addTextChangedListener(watcher);

        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });
    }

    //change password
    private void changePassword(){

        String usrPass = "";
        String userid = ShareManager.manager().getValue(SM_LAST_LOGIN_ID, "");
        Model.UserLoginInfo lastLoginInfo = null;
        if (!userid.isEmpty()) {
            lastLoginInfo = DbManager.loadInfo(userid);
            usrPass = lastLoginInfo.password;
        }

        if (!edt_old.getText().toString().equals(usrPass)) {
            Utils.showToast(this, getString(R.string.current_password_wrong));
            return;
        }

        if (!edt_new.getText().toString().equals(edt_new_confirm.getText().toString())) {
            Utils.showToast(this, getString(R.string.new_password_incorrent));
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        UserPasswordUpdateReq dicInfo =  new UserPasswordUpdateReq();
        dicInfo.setOldPassword(edt_old.getText().toString());
        dicInfo.setNewPassword(edt_new.getText().toString());

        Call<GenericResponse<Void>> genRes = restAPI.changePass(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(PasswordChangeActivity.this, getString(R.string.change_password_success));
                DbManager.saveInfo(Globals.myInfo.getUsr().getUsrId(), edt_new.getText().toString(), Globals.myInfo.getUsr().getUsrNckNm(), Globals.myInfo.getUsr().getProfileImg(), Globals.getLoginTypeStr(Globals.myInfo.getUsr().getUsrLoginType()));
                ShareManager.manager().put(SM_LAST_LOGIN_ID, Globals.myInfo.getUsr().getUsrId());
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }

}
