package com.kyad.selluv.api.dto.pdt;

import com.kyad.selluv.api.dto.usr.UsrMiniDto;

import lombok.Data;

@Data
public class ReplyDto {
    //("댓글내용")
    private ReplyDao reply;
    //("등록유저")
    private UsrMiniDto usr;
}
