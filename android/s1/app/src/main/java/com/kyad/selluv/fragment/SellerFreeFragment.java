/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.mypage.SellerSettingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;

public class SellerFreeFragment extends BaseFragment {

    public SellerFreeFragment() {
    }

    public void showSwtichStatus(){
        if (Globals.myInfo.getUsr().getFreeSendPrice() == -1){
            tb_switch.setSelected(false);
            ((SellerSettingActivity)getActivity()).setFreeSend(false);
        } else {
            tb_switch.setSelected(true);
            ((SellerSettingActivity)getActivity()).setFreeSend(true);
            edt_code.setText("" + Globals.myInfo.getUsr().getFreeSendPrice());
        }
        ((SellerSettingActivity)getActivity()).setFreeSendPrice(Globals.myInfo.getUsr().getFreeSendPrice());
    }

    //UI Referencs
    @BindView(R.id.tv_guide)
    TextView tv_guide;
    @BindView(R.id.rl_on)
    RelativeLayout rl_on;
    @BindView(R.id.tb_switch)
    Button tb_switch;
    @BindView(R.id.edt_code)
    EditText edt_code;

    @OnClick(R.id.rl_btn)
    void onSendPrice() {

        ((SellerSettingActivity)getActivity()).setFreeSendPrice(Long.parseLong(edt_code.getText().toString()));
        ((SellerSettingActivity)getActivity()).setFreeSend(true);
        ((SellerSettingActivity)getActivity()).reqSellerSetting();
    }

    //Vars
    View rootView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_free_post, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;

    }

    private void loadLayout() {
        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.activity_main));

        tb_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_switch.isSelected()){
                    rl_on.setVisibility(View.GONE);
                    tv_guide.setVisibility(View.VISIBLE);
//                    ((SellerSettingActivity)getActivity()).setFreeSendPrice(-1);
                    ((SellerSettingActivity)getActivity()).setFreeSend(false);
                    ((SellerSettingActivity)getActivity()).reqSellerSetting();
                } else {
                    rl_on.setVisibility(View.VISIBLE);
                    tv_guide.setVisibility(View.GONE);
//                    ((SellerSettingActivity)getActivity()).setFreeSendPrice(Long.parseLong(edt_code.getText().toString()));
//                    ((SellerSettingActivity) getActivity()).setFreeSend(false);
//                    ((SellerSettingActivity) getActivity()).reqSellerSetting();
                }
            }
        });
        showSwtichStatus();
    }
}
