package com.kyad.selluv.mypage;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.UsrPushSettingDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.MultiUidsReq;
import com.kyad.selluv.api.request.UserPushSettingReq;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.AlarmCommentFragment;
import com.kyad.selluv.fragment.AlarmDealFragment;
import com.kyad.selluv.fragment.AlarmNewsFragment;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class AlarmSettingActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    AlarmDealFragment dealFragment;
    AlarmNewsFragment newsFragment;
    AlarmCommentFragment commentFragmetn;
    int selectedTab = 0;
    private PageAdapter pagerAdapter;

    public Integer[] dealAlarm = new Integer[10];
    public Integer[] newsAlarm = new Integer[12];
    public Integer[] replyAlarm = new Integer[2];
    public int dealYn = 0;
    public int newsYn = 0;
    public int replyYn = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_alarmset);
        setStatusBarWhite();
        initFragment();
        loadLayout();
    }

    private void loadLayout() {
        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
                getUserSettingInfo();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);
        tl_top_tab.getTabAt(0).select();
        setSelectedTabStyle(0);
    }

    private void initFragment() {
        if (dealFragment == null) {
            dealFragment = new AlarmDealFragment();
        }
        if (newsFragment == null) {
            newsFragment = new AlarmNewsFragment();
        }
        if (commentFragmetn == null) {
            commentFragmetn = new AlarmCommentFragment();
        }
        tl_top_tab.getTabAt(selectedTab).select();
        getUserSettingInfo();
    }

    private void selectPage(UsrPushSettingDao dic) {
        if (selectedTab == 0) {
            dealFragment.setAlarmInfo(dic);
        } else if (selectedTab == 1) {
            newsFragment.setAlarmInfo(dic);
        } else {
            commentFragmetn.setAlarmInfo(dic);
        }
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = dealFragment;
                    break;
                case 1:
                    frag = newsFragment;
                    break;
                case 2:
                    frag = commentFragmetn;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    //알림 설정정보 얻기
    private void getUserSettingInfo(){

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrPushSettingDao>> genRes = restAPI.getAlarmSettingInfo(Globals.userToken);

        genRes.enqueue(new TokenCallback<UsrPushSettingDao>(this) {
            @Override
            public void onSuccess(UsrPushSettingDao response) {
                closeLoading();

                dealYn = response.getDealYn();
                newsYn = response.getNewsYn();
                replyYn = response.getReplyYn();

                for (int i = 0 ; i < 10 ; i++){
                    dealAlarm[i] = Integer.valueOf(response.getDealSetting().substring(i, i + 1));
                }

                for (int i = 0 ; i < 12 ; i++){
                    newsAlarm[i] = Integer.valueOf(response.getNewsSetting().substring(i, i + 1));
                }

                for (int i = 0 ; i < 2 ; i++){
                    replyAlarm[i] = Integer.valueOf(response.getReplySetting().substring(i, i + 1));
                }

                selectPage(response);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //알림 설정정보 신청
    public void reqAlarmSetting(){

        String dealSetting = "";
        String newsSetting = "";
        String replySetting = "";

        for (int i = 0 ; i < 10 ; i++){
            dealSetting = dealSetting + String.valueOf(dealAlarm[i]);
        }

        for (int i = 0 ; i < 12 ; i++){
            newsSetting = newsSetting + String.valueOf(newsAlarm[i]);
        }

        for (int i = 0 ; i < 2 ; i++){
            replySetting = replySetting + String.valueOf(replyAlarm[i]);
        }

        UserPushSettingReq dicInfo = new UserPushSettingReq();
        dicInfo.setDealYn(dealYn);
        dicInfo.setNewsYn(newsYn);
        dicInfo.setReplyYn(replyYn);
        dicInfo.setDealSetting(dealSetting.trim());
        dicInfo.setNewsSetting(newsSetting.trim());
        dicInfo.setReplySetting(replySetting.trim());

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.reqAlarmSetting(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(AlarmSettingActivity.this, getString(R.string.alarm_setting_success));
                getUserSettingInfo();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
