package com.kyad.selluv.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrWishDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.mypage.WishAlarmActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class WishalarmAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrWishDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;
    boolean editable;

    public WishalarmAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        notifyDataSetChanged();
    }

    public void setData(ArrayList<UsrWishDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_wishalarm, parent, false);
            final UsrWishDto anItem = (UsrWishDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }

            ImageButton ib_delete = (ImageButton) convertView.findViewById(R.id.ib_delete);
            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<Integer> arr =  new ArrayList<>();
                    arr.add(arrData.get(position).getUsrWish().getUsrWishUid());
                    if(activity != null && activity instanceof WishAlarmActivity) {
                        ((WishAlarmActivity)activity).deleteUsrWish(arr);
                    }
                }
            });
            if (editable) {
                ib_delete.setVisibility(View.VISIBLE);
            } else {
                ib_delete.setVisibility(View.GONE);
            }

            String brandName = anItem.getBrand().getNameKo();
            String pdtName = "";
            int pdtGroup = anItem.getUsrWish().getPdtGroup();
            if (pdtGroup == 1){
                pdtName = activity.getString(R.string.male);
            } else if (pdtGroup == 2){
                pdtName = activity.getString(R.string.female);
            } else {
                pdtName = activity.getString(R.string.kids);
            }

            String categoryName = anItem.getCategory().getCategoryName();
            String modelName = anItem.getUsrWish().getPdtModel();
            String pdtSize = anItem.getUsrWish().getPdtSize();
            String colorName = anItem.getUsrWish().getColorName();

            TextView tv_brand_show = convertView.findViewById(R.id.tv_brand_show);
            tv_brand_show.setText(brandName);

            TextView tv_model_show = convertView.findViewById(R.id.tv_model_show);
            tv_model_show.setText(modelName);

            TextView tv_group_show = convertView.findViewById(R.id.tv_group_show);
            tv_group_show.setText(pdtName);

            TextView tv_size_show = convertView.findViewById(R.id.tv_size_show);
            tv_size_show.setText(pdtSize);

            TextView tv_category_show = convertView.findViewById(R.id.tv_category_show);
            tv_category_show.setText(categoryName);

            TextView tv_color_show = convertView.findViewById(R.id.tv_color_show);
            tv_color_show.setText(colorName);

            TextView tv_brand_name = convertView.findViewById(R.id.tv_brand_name);
            tv_brand_name.setText(anItem.getBrand().getNameEn());

            TextView tv_ware_name = convertView.findViewById(R.id.tv_ware_name);
            tv_ware_name.setText(brandName + pdtName + colorName + categoryName + modelName);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}