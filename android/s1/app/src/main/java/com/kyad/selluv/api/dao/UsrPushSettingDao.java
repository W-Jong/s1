package com.kyad.selluv.api.dao;

import java.sql.Timestamp;
import java.util.Objects;

public class UsrPushSettingDao {
    private int usrUid;
//    @JsonIgnore
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
//    //@ApiModelProperty("거래알림 사용여부 1-사용, 0-사용안함")
    private int dealYn = 1;
    //@ApiModelProperty("소식알림 사용여부 1-사용, 0-사용안함")
    private int newsYn = 1;
    //@ApiModelProperty("댓글알림 사용여부 1-사용, 0-사용안함")
    private int replyYn = 1;
    //@ApiModelProperty("거래알림 - 10자리 숫자배열(1-사용 0-사용안함)")
    private String dealSetting = "1111111111";
    //@ApiModelProperty("소식알림 - 12자리 숫자배열(1-사용 0-사용안함)")
    private String newsSetting = "111111111111";
    //@ApiModelProperty("댓글알림 - 2자리 숫자배열(1-사용 0-사용안함)")
    private String replySetting = "11";

    //@Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    //@Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    //@Column(name = "deal_yn", nullable = false)
    public int getDealYn() {
        return dealYn;
    }

    public void setDealYn(int dealYn) {
        this.dealYn = dealYn;
    }

    //@Column(name = "news_yn", nullable = false)
    public int getNewsYn() {
        return newsYn;
    }

    public void setNewsYn(int newsYn) {
        this.newsYn = newsYn;
    }

    //@Column(name = "reply_yn", nullable = false)
    public int getReplyYn() {
        return replyYn;
    }

    public void setReplyYn(int replyYn) {
        this.replyYn = replyYn;
    }

    //@Column(name = "deal_setting", nullable = false, length = 30)
    public String getDealSetting() {
        return dealSetting;
    }

    public void setDealSetting(String dealSetting) {
        this.dealSetting = dealSetting;
    }

    //@Column(name = "news_setting", nullable = false, length = 30)
    public String getNewsSetting() {
        return newsSetting;
    }

    public void setNewsSetting(String newsSetting) {
        this.newsSetting = newsSetting;
    }

    //@Column(name = "reply_setting", nullable = false, length = 30)
    public String getReplySetting() {
        return replySetting;
    }

    public void setReplySetting(String replySetting) {
        this.replySetting = replySetting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrPushSettingDao that = (UsrPushSettingDao) o;
        return usrUid == that.usrUid &&
                dealYn == that.dealYn &&
                newsYn == that.newsYn &&
                replyYn == that.replyYn &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(dealSetting, that.dealSetting) &&
                Objects.equals(newsSetting, that.newsSetting) &&
                Objects.equals(replySetting, that.replySetting);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrUid, regTime, dealYn, newsYn, replyYn, dealSetting, newsSetting, replySetting);
    }
}
