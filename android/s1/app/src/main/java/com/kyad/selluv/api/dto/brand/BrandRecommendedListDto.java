package com.kyad.selluv.api.dto.brand;

import lombok.Data;

@Data
public class BrandRecommendedListDto {
    //("브랜드UID")
    private int brandUid;
    //("영문이름")
    private String nameEn;
    //("한글이름")
    private String nameKo;
    //("백그라운드 이미지")
    private String backImg;
    //("로고이미지")
    private String logoImg ;
    //("상품갯수")
    private int pdtCount;
}
