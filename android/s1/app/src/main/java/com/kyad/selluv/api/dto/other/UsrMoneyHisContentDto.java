package com.kyad.selluv.api.dto.other;

import com.kyad.selluv.api.dto.pdt.Page;

import java.util.List;

import lombok.Data;

@Data
public class UsrMoneyHisContentDto {
    //("셀럽머니")
    private long money;
    //("판매정산")
    private long cashbackMoney;
    //("상세내역")
    private Page<UsrMoneyHisDto> history;
}
