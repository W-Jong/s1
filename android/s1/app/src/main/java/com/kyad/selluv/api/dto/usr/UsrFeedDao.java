package com.kyad.selluv.api.dto.usr;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class UsrFeedDao {
    private int usrFeedUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"회원UID")
    private int usrUid;
    //"상품UID")
    private int pdtUid;
    //"분류 1-찾았던 상품 2-좋아요 상품 가격인하 3-팔로우 브랜드 4-잇템 5-팔로우 유저 스타일 6-팔로우 유저 신상품")
    private int type;
    //"대상자 UID")
    private int peerUsrUid;
    private int status = 1;
}
