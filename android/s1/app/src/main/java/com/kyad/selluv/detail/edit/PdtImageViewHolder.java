package com.kyad.selluv.detail.edit;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.kyad.selluv.R;

public class PdtImageViewHolder extends RecyclerView.ViewHolder {
    public ImageView shareImageView;

    public PdtImageViewHolder(View v) {
        super(v);

        shareImageView = v.findViewById(R.id.shareImageView);
        shareImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
