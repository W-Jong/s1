package com.kyad.selluv.api.request;


import android.support.annotation.Size;

import lombok.Data;
import lombok.NonNull;


@Data
public class UserProfileReq {

//    @Size(min = 1)
    //@ApiModelProperty("닉네임")
    private String usrNckNm;
//    //@NonNull
    //@ApiModelProperty("남성군 좋아요")
    private Boolean isMale;
    //@NonNull
    //@ApiModelProperty("여성군 좋아요")
    private Boolean isFemale;
    //@NonNull
    //@ApiModelProperty("키즈군 좋아요")
    private Boolean isChildren;
    //@NonNull
    //@ApiModelProperty("자기소개")
    private String description;
    //@NonNull
    //@ApiModelProperty("프로필이미지, 변경하지 않는 경우 빈문자열")
    private String profileImg;
    //@NonNull
    //@ApiModelProperty("배경이미지, 변경하지 않는 경우 빈문자열")
    private String profileBackImg;
}
