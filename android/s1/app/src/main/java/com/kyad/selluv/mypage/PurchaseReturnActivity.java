package com.kyad.selluv.mypage;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.DealRefundReq;
import com.kyad.selluv.api.request.DealReviewReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.sell.SellInfoActivity;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


public class PurchaseReturnActivity extends BaseActivity {

    private int dealUid = 0;
    private ArrayList<String> arrPhotos = new ArrayList<>();

    //UI Reference
    @BindView(R.id.edt_reason)
    EditText edt_reason;
    @BindView(R.id.rl_img_input)
    RelativeLayout rl_img_input;
    @BindView(R.id.ll_img_list)
    LinearLayout ll_img_list;
    @BindView(R.id.scv_img)
    HorizontalScrollView scv_img;
    @BindView(R.id.btn_request)
    Button btn_request;

    @OnClick(R.id.rl_img_input)
    void onImgInput() {
        Intent intent = new Intent(this, SellPicActivity.class);
        intent.putExtra(Constants.IMAGES, imgs.toArray(new String[]{}));
        intent.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_MYPAGE);
        startActivityForResult(intent, 2222);
    }

    @OnClick(R.id.btn_request)
    void onRefundReq() {
        reqDealRefund();
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    LayoutInflater inflater;
    ArrayList<String> imgs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_return);
        setStatusBarWhite();
        loadLayout();
        dealUid = getIntent().getIntExtra(Constants.DEAL_UID, 0);
        inflater = LayoutInflater.from(this);
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        edt_reason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkNextable();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2222) {
            if (data == null) return;
            imgs.clear();
            ll_img_list.removeAllViews();
            Collections.addAll(imgs, data.getStringArrayExtra(Constants.IMAGES));
            Collections.addAll(arrPhotos, data.getStringArrayExtra(Constants.IMAGES_NAMES));

            if (imgs.size() > 10) {
                Utils.showToast(this, "최대 10장의 사진을 선택할수 있습니다.");
                return;
            }
            rl_img_input.setVisibility(View.GONE);
            scv_img.setVisibility(View.VISIBLE);
            for (int i = 0; i < imgs.size(); i++) {
                final View imgView = inflater.inflate(R.layout.item_img_upload, null);
                ((ImageView) imgView.findViewById(R.id.img)).setImageBitmap(
                        ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(imgs.get(i)),
                                Utils.dpToPixel(this, 102f), Utils.dpToPixel(this, 102f)));
                ll_img_list.addView(imgView);
                final String finalObj = imgs.get(i);
                (imgView.findViewById(R.id.ib_del)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imgs.remove(finalObj);
                        ll_img_list.removeView(imgView);
                        if (ll_img_list.getChildCount() == 0) {
                            rl_img_input.setVisibility(View.VISIBLE);
                            scv_img.setVisibility(View.GONE);
                        }
                    }
                });
                imgView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(PurchaseReturnActivity.this, SellPicActivity.class);
                        intent.putExtra(Constants.IMAGES, imgs.toArray(new String[]{}));
                        intent.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_MYPAGE);
                        startActivityForResult(intent, 2222);
                    }
                });
            }
            checkNextable();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    void checkNextable() {
        if (imgs.size() > 0 && edt_reason.length() > 0)
            btn_request.setEnabled(true);
        else
            btn_request.setEnabled(false);
    }

    //반품신청
    private void reqDealRefund() {

        if (arrPhotos.size() == 0) {
            Utils.showToast(this, getString(R.string.upload_photo_alert));
            return;
        }

        DealRefundReq dicInfo = new DealRefundReq();
        dicInfo.setPhotos(arrPhotos);
        dicInfo.setRefundReason(edt_reason.getText().toString());

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.reqDealRefund(Globals.userToken, dealUid, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {

                Utils.showToast(PurchaseReturnActivity.this, getString(R.string.deal_refund_req_success));
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

}
