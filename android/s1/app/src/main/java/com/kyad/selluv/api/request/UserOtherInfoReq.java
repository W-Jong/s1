package com.kyad.selluv.api.request;


import android.support.annotation.IntRange;
import android.support.annotation.Size;

import lombok.Data;
import lombok.NonNull;


@Data
public class UserOtherInfoReq {
    @Size(min = 1)
    //@ApiModelProperty("이메일")
    private String usrMail;
//    //@NonNull
    //@ApiModelProperty("성함1")
    private String addressName1;
//    //@NonNull
    //@ApiModelProperty("연락처1")
    private String addressPhone1;
    //@NonNull
    //@ApiModelProperty("주소1")
    private String addressDetail1;
    //@NonNull
    //@ApiModelProperty("성함2")
    private String addressName2;
    //@NonNull
    //@ApiModelProperty("연락처2")
    private String addressPhone2;
    //@NonNull
    //@ApiModelProperty("주소2")
    private String addressDetail2;
    //@NonNull
    //@ApiModelProperty("성함3")
    private String addressName3;
    //@NonNull
    //@ApiModelProperty("연락처3")
    private String addressPhone3;
    //@NonNull
    //@ApiModelProperty("주소3")
    private String addressDetail3;
    @IntRange(from = 1)
    //@ApiModelProperty("선택된 주소번호 1~3만 가능")
    private int addressSelectionIndex;

    //상세주소1(우편번호) ,
    private String addressDetailSub1;
    //상세주소2(우편번호) ,
    private String addressDetailSub2;
    //상세주소3(우편번호) ,
    private String addressDetailSub3;

}
