package com.kyad.selluv.detail;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.usr.UsrAddressDao;
import com.kyad.selluv.api.request.DealNegoReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.mypage.CardManageActivity;
import com.kyad.selluv.webview.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

import static com.kyad.selluv.mypage.CardManageActivity.CARD_SELECT_REQUEST;

public class NegoCheckoutActivity extends BaseActivity {

    public static final String PDT_UID = "PDT_UID";
    public static final String NEGO_PRICE = "NEGO_PRICE";

    private int pdtUid = 0;
    private long negoPrice = 0;

    //UI Reference
    @BindView(R.id.rl_card)
    RelativeLayout rl_card;
    @BindView(R.id.rl_card_register)
    RelativeLayout rl_card_register;
    @BindView(R.id.rl_card_none)
    RelativeLayout rl_card_none;
    @BindView(R.id.ll_main_bar)
    LinearLayout ll_main_bar;
    @BindView(R.id.tv_name)
    EditText edt_name;
    @BindView(R.id.tv_contact)
    EditText edt_contact;
    @BindView(R.id.tv_address)
    EditText edt_address;
    @BindView(R.id.btn_card_change)
    Button btn_card_change;
    @BindView(R.id.txt_description2)
    TextView txt_description2;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;

    //Vars
    int nSelectedUsrCardUid = 0;
    private String addressDetail = "";
    private String addressDetailSub = "";

    @OnClick(R.id.ib_left)
    void onBack(View v) {
        finish();
    }

    @OnClick({R.id.btn_search_address, R.id.tv_address})
    void onSearchAddress() {
        CommonWebViewActivity.showAddressWebviewActivity(this);
    }

    @OnClick(R.id.btn_finish)
    void onFinish(View v) {
        if (edt_name.length() == 0) {
            Utils.showToast(NegoCheckoutActivity.this, "성함을 입력해주세요.");
            return;
        }
        if (edt_contact.length() == 0) {
            Utils.showToast(NegoCheckoutActivity.this, "연락처를 입력해주세요.");
            return;
        }
        if (TextUtils.isEmpty(addressDetail) || TextUtils.isEmpty(addressDetailSub)) {
            Utils.showToast(NegoCheckoutActivity.this, "주소를 선택해주세요.");
            return;
        }
        if (nSelectedUsrCardUid < 1) {
            Utils.showToast(NegoCheckoutActivity.this, "카드를 선택해주세요.");
            return;
        }

        negoRequest();
    }

    @OnClick({R.id.btn_card_register, R.id.btn_card_change})
    void onRegisterCard(View v) {
        Intent intent = new Intent(this, CardManageActivity.class);
        intent.putExtra(CardManageActivity.CARD_SELECTION_MODE, true);
        startActivityForResult(intent, CardManageActivity.CARD_SELECT_REQUEST);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_nego_checkout);
        pdtUid = getIntent().getIntExtra(PDT_UID, 0);
        negoPrice = getIntent().getLongExtra(NEGO_PRICE, 0);
        setStatusBarWhite();
        loadLayout();
    }

    private void loadLayout() {
        if (Globals.myInfo == null || Globals.myInfo.getUsr() == null) {
            Utils.showToast(this, "로그인 후 이용해주세요.");
            finish();
            return;
        }

        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        if (Constants.cardItemArrayList.size() == 0) {
            rl_card_register.setVisibility(View.VISIBLE);
            rl_card.setVisibility(View.GONE);
            txt_description2.setText(R.string.pay_card_register1_text);
            btn_card_change.setVisibility(View.INVISIBLE);
        } else {
            rl_card_register.setVisibility(View.GONE);
            rl_card.setVisibility(View.VISIBLE);
            txt_description2.setText(R.string.pay_card_input1_text);
            btn_card_change.setVisibility(View.VISIBLE);
        }

        try {
            UsrAddressDao defaultUsrAddress = Globals.myInfo.getAddressList().get(0);
            tb_segment.setToggleStatus(0);
            if (Globals.myInfo.getAddressList().get(1).getStatus() == 2) {
                defaultUsrAddress = Globals.myInfo.getAddressList().get(1);
                tb_segment.setToggleStatus(1);
            }
            if (Globals.myInfo.getAddressList().get(2).getStatus() == 2) {
                defaultUsrAddress = Globals.myInfo.getAddressList().get(2);
                tb_segment.setToggleStatus(2);
            }
            setEditValueFromDao(defaultUsrAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tb_segment.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
                try {
                    setEditValueFromDao(Globals.myInfo.getAddressList().get(toggleIntValue));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setEditValueFromDao(UsrAddressDao usrAddress) {
        edt_name.setText(usrAddress.getAddressName());
        edt_contact.setText(usrAddress.getAddressPhone());

        addressDetail = usrAddress.getAddressDetail();
        addressDetailSub = usrAddress.getAddressDetailSub();
        edt_address.setText(TextUtils.isEmpty(addressDetail) && TextUtils.isEmpty(addressDetailSub) ? "" : addressDetail + "\n" + addressDetailSub + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CARD_SELECT_REQUEST && resultCode == RESULT_OK && Globals.selectedUsrCardDao != null) {
            rl_card_register.setVisibility(View.GONE);
            rl_card.setVisibility(View.VISIBLE);
            txt_description2.setText(R.string.pay_card_input1_text);
            btn_card_change.setVisibility(View.VISIBLE);

            TextView tv_number = (TextView) findViewById(R.id.tv_number);
            TextView tv_company = (TextView) findViewById(R.id.tv_company);
            tv_number.setText(Utils.convertCardNum(Globals.selectedUsrCardDao.getCardNumber()));
            tv_company.setText(Globals.selectedUsrCardDao.getCardName());
            nSelectedUsrCardUid = Globals.selectedUsrCardDao.getUsrCardUid();

        }

        if (requestCode == CommonWebViewActivity.WEBVIEW_ADDRESS_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);
            if (isSuccess) {
                addressDetail = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS);
                addressDetailSub = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_POSTCODE);
                edt_address.setText(addressDetail + "\n" + addressDetailSub + "");
            }
        }
    }

    private void negoRequest() {

        //TODO: 카드연동 및 도서지역 추가배송비 설정
        final long islandSendPrice = 0;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();
        DealNegoReq negoReq = new DealNegoReq();
        negoReq.setPdtUid(pdtUid);
        negoReq.setReqPrice(negoPrice);
        negoReq.setRecipientNm(edt_name.getText().toString());
        negoReq.setRecipientPhone(edt_contact.getText().toString());
        negoReq.setRecipientAddress(addressDetail + "\n" + addressDetailSub);
        negoReq.setUsrCardUid(nSelectedUsrCardUid);
        negoReq.setIslandSendPrice(islandSendPrice);

        Call<GenericResponse<DealDao>> genRes = restAPI.dealNegoReq(Globals.userToken, negoReq);

        genRes.enqueue(new TokenCallback<DealDao>(this) {
            @Override
            public void onSuccess(DealDao response) {
                closeLoading();

                Intent intent = new Intent(NegoCheckoutActivity.this, NegoCompleteActivity.class);
                intent.putExtra(NegoCompleteActivity.DEAL_UID, response.getDealUid());
                intent.putExtra(NegoCompleteActivity.NEGO_PRICE, negoPrice);
                intent.putExtra(NegoCompleteActivity.EXTRA_ISLAND_SEND_PRICE, islandSendPrice);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
