/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.AlarmListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrAlarmDto;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.model.Model;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class MypageAlarmFragment extends BaseFragment {

    public interface DataLoadInterface {
        public Model.AlarmItem[] loadData();
    }

    public MypageAlarmFragment() {
    }

    public void setTab(int index){

        if (index == 0){
            kind = "DEAL";
        } else if (index == 1){
            kind = "NEWS";
        } else {
            kind = "REPLY";
        }

        getAlarmList();
    }

    //UI Referencs
    @BindView(R.id.lv_list)
    ListView lv_list;

    //Vars
    View rootView;
    private AlarmListAdapter adapter;
    private DataLoadInterface loadInterface = null;

    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;
    private String kind = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_alarm, container, false);
        ButterKnife.bind(this, rootView);
        adapter = new AlarmListAdapter((BaseActivity) getActivity());
        loadLayout();
        loadData();
        return rootView;

    }

    private void loadLayout() {
        lv_list.setAdapter(adapter);
        lv_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getAlarmList();
                }
            }
        });
    }

    private void loadData() {
        if (adapter != null) {
//            Collections.addAll(adapter.arrData, loadInterface.loadData());
            adapter.notifyDataSetChanged();
        }

    }

    public void setLoadInterface(DataLoadInterface loadInterface) {
        this.loadInterface = loadInterface;
    }

    //get Alarm list
    private void getAlarmList(){
        if (isLoading || isLast)
            return;

        isLoading = true;

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Page<UsrAlarmDto>>> genRes = restAPI.getAlarmList(Globals.userToken, kind, currentPage);

        genRes.enqueue(new TokenCallback<Page<UsrAlarmDto>>(getActivity()) {
            @Override
            public void onSuccess(Page<UsrAlarmDto> response) {
                closeLoading();

                isLoading = false;

                if (currentPage == 0) {
                    adapter.arrData.clear();
                }

                isLast = response.isLast();
                adapter.arrData.addAll(response.getContent());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }

}
