/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.WareEditActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;


public class SellModelFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.edt_etc)
    EditText edt_etc;
    @BindView(R.id.lv_model)
    ListView lv_model;
    @BindView(R.id.tv_model_recommend)
    TextView tv_model_recommend;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_model, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        loadData();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onDone(View v) {
        String s_model = edt_etc.getText().toString();
        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtModel(s_model);
        } else {
            Globals.pdtCreateReq.setPdtModel(s_model);
        }
        ((TextView) getActivity().findViewById(R.id.tv_model_input)).setText(s_model);
        getView().setVisibility(View.GONE);
    }

    private void loadLayout() {
        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.frag_main));

        tv_model_recommend.setText(String.format(getString(R.string.model_recommend), Globals.selectedBrand.getNameKo()));

        String s_model;
        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            ib_right.setVisibility(View.GONE);
            s_model = Globals.pdtUpdateReq.getPdtModel();
        } else {
            tv_finish.setVisibility(View.GONE);
            ib_right.setVisibility(View.VISIBLE);
            s_model = Globals.pdtCreateReq.getPdtModel();
        }
        edt_etc.setText(s_model);
    }

    private void loadData() {

        ((BaseActivity) getActivity()).showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<String>>> genRes = restAPI.brandModelNames(Globals.userToken, Globals.selectedBrand.getBrandUid());
        genRes.enqueue(new TokenCallback<List<String>>(getActivity()) {
            @Override
            public void onSuccess(List<String> response) {
                ((BaseActivity) getActivity()).closeLoading();

                final List<String> names = response;
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_model_text, names);
                lv_model.setAdapter(adapter);
                lv_model.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        edt_etc.setText(names.get(i));
                    }
                });
            }

            @Override
            public void onFailed(Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
            }
        });
    }
}
