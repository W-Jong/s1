package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import com.kyad.selluv.api.enumtype.PAY_TYPE;

import lombok.Data;
import lombok.NonNull;

@Data
public class DealBuyReq {
    //@ApiModelProperty("상품UID")
    private int pdtUid;
    //@ApiModelProperty("제안가격")
    private long reqPrice;
    //@ApiModelProperty("수령자주소 - 성함")
    @Size(min = 1)
    private String recipientNm;
    //@ApiModelProperty("수령자주소 - 연락처")
    @Size(min = 1)
    private String recipientPhone;
    //@ApiModelProperty("수령자주소 - 주소")
    @Size(min = 1)
    private String recipientAddress;
    //@ApiModelProperty("결제유형")
    private PAY_TYPE payType;
    //@ApiModelProperty("카드UID, 카드결제가 아닐때에는 0")
    private int usrCardUid;
    //@ApiModelProperty("정품감정서비스 사용여부, 0-사용안함, 1-사용")
    private int pdtVerifyEnabled;
    //@ApiModelProperty("구매프로모션코드")
    private String payPromotion;
    //@ApiModelProperty("사용할 셀럽머니")
    @IntRange(from = 0)
    private long rewardPrice;
    //@ApiModelProperty("도서지역 추가배송비")
    @IntRange(from = 0)
    private long islandSendPrice;
}
