package com.kyad.selluv.api.dto.other;

import java.util.List;

import lombok.Data;

@Data
public class DeliveryCompaniesDto {
//    @ApiModelProperty("택배사 목록")
    private List<String> companies;
}
