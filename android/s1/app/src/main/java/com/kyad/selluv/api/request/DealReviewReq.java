package com.kyad.selluv.api.request;


import android.support.annotation.IntRange;
import android.support.annotation.Size;

import lombok.Data;

@Data
public class DealReviewReq {
    //@ApiModelProperty("점수")
    @IntRange(from = 0)
    private int point;
    //@ApiModelProperty("후기내용")
    @Size(min = 1)
    private String content;
}
