package com.kyad.selluv.api.request;


import lombok.Data;
import lombok.NonNull;

@Data
public class UserPasswordFindReq {

    @NonNull
    //@ApiModelProperty("이메일")
    private String email;
    @NonNull
    //@ApiModelProperty("닉네임")
    private String nickname;
}
