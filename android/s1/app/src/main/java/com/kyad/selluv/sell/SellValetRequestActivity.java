package com.kyad.selluv.sell;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.usr.UsrAddressDao;
import com.kyad.selluv.api.request.ValetCreateReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.sell.fragment.SellValetSignFragment;
import com.kyad.selluv.webview.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;

public class SellValetRequestActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.btn_send_kit)
    ImageButton btn_send_kit;
    @BindView(R.id.btn_send_direct)
    ImageButton btn_send_direct;
    @BindView(R.id.btn_auto_price_onoff)
    ImageButton btn_auto_price_onoff;
    @BindView(R.id.btn_direct_input_onoff)
    ImageButton btn_direct_input_onoff;
    @BindView(R.id.edt_name)
    EditText edt_name;
    @BindView(R.id.edt_contact)
    EditText edt_contact;
    @BindView(R.id.edt_address)
    TextView edt_address;
    @BindView(R.id.btn_request)
    Button btn_request;
    @BindView(R.id.txt_input_price)
    TextView tv_input_price;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;
    /**
     * 팝업에 있는 컨트럴
     */
    EditText edt_price = null;
    TextView tv_price_value = null;

    /**
     * 판매가 추천가격
     */
    public boolean isRecommendPrice = true;
    /**
     * 배송방법
     */
    public int sendMethod = 1;
    public double sell_price = 10.0;

    /** ======================================================= **/
    /**
     * + - 버튼을 꾹 누르고 있을때 처리를 위해 필요
     **/
    private final int INTERVAL1 = 300;
    private final int INTERVAL2 = 100;
    private final int INTERVAL3 = 10;
    int interval = INTERVAL1;
    double rate = 0.5;
    Runnable runnablePrice;
    Runnable runnableRate;
    Handler handlerPrice = new Handler();
    Handler handlerRate = new Handler();

    /**
     * =======================================================
     **/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sell_valet_service);
        addForSellActivity();

        loadLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonWebViewActivity.WEBVIEW_ADDRESS_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);
            if (isSuccess) {
                String addressDetail = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS);
                String addressDetailSub = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_POSTCODE);
                edt_address.setText(addressDetail + "\n" + addressDetailSub);
            }
        }
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    /**
     * 배송키트 이용
     */
    @OnClick(R.id.btn_send_kit)
    void onSendKit() {
        btn_send_kit.setBackgroundResource(R.drawable.sell_direct_send_kit_over);
        btn_send_direct.setBackgroundResource(R.drawable.sell_direct_send);
        sendMethod = 1;
    }

    /**
     * 직접 발송
     */
    @OnClick(R.id.btn_send_direct)
    void onSendDirect() {
        btn_send_kit.setBackgroundResource(R.drawable.sell_direct_send_kit);
        btn_send_direct.setBackgroundResource(R.drawable.sell_direct_send_over);
        sendMethod = 2;
    }

    /**
     * 추천 가격
     */
    @OnClick(R.id.rl_recommend)
    void onAutoPrice() {
        isRecommendPrice = true;
        btn_auto_price_onoff.setBackgroundResource(R.drawable.list_roundbox_on);
        btn_direct_input_onoff.setBackgroundResource(R.drawable.list_roundbox_off);
    }

    /**
     * 직접 설정
     */
    @OnClick(R.id.rl_selluvmoney_input)
    void onDirectInput() {
        isRecommendPrice = false;

        btn_auto_price_onoff.setBackgroundResource(R.drawable.list_roundbox_off);
        btn_direct_input_onoff.setBackgroundResource(R.drawable.list_roundbox_on);

        final PopupDialog popup = new PopupDialog(this, R.layout.sell_valet_service_form_amount).setFullScreen();
        Utils.hideKeypad(this, popup.findView(R.id.activity_main));
        edt_price = (EditText) popup.findView(R.id.txt_price);
        tv_price_value = (TextView) popup.findView(R.id.tv_price_value);

        edt_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String ss = edt_price.getText().toString();
                if (ss.length() < 1 || ss.startsWith(".")) {
                    sell_price = 5.0;
                    edt_price.setText(String.format("%.01f", sell_price));
                    updateReceivedMoney();
                } else {
                    sell_price = Double.valueOf(ss);
                    if (sell_price < 5.0) {
                        sell_price = 5.0;
                        edt_price.setText(String.format("%.01f", sell_price));
                    }
                    updateReceivedMoney();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        popup.setOnClickListener(R.id.ib_left, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });
        popup.setOnClickListener(R.id.ib_verify, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                updateReceivedMoney();
                popup.dismiss();
            }
        });
        popup.setOnClickListener(R.id.ib_confirm, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                updateReceivedMoney();
                popup.dismiss();
            }
        });

        popup.findView(R.id.ib_plus).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    rate = 0.5;
                    interval = INTERVAL1;

                    handlerPrice.post(runnablePrice);

                    handlerRate.postDelayed(runnableRate, 3000);
                    handlerRate.postDelayed(runnableRate, 5000);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    handlerPrice.removeCallbacks(runnablePrice);
                    handlerRate.removeCallbacks(runnableRate);
                }
                return false;
            }
        });

        popup.findView(R.id.ib_minus).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    rate = -0.5;
                    interval = INTERVAL1;

                    handlerPrice.post(runnablePrice);

                    handlerRate.postDelayed(runnableRate, 3000);
                    handlerRate.postDelayed(runnableRate, 5000);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    handlerPrice.removeCallbacks(runnablePrice);
                    handlerRate.removeCallbacks(runnableRate);
                }
                return false;
            }
        });
        popup.show();
    }

    @OnClick(R.id.btn_request)
    void onRequest(View v) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        SellValetSignFragment signFragment = new SellValetSignFragment();
        transaction.add(signFragment, "SellValetSignFragment");
        transaction.commit();
    }

    @OnClick(R.id.btn_search)
    void onClickSearch(View v) {
        CommonWebViewActivity.showAddressWebviewActivity(this);
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edt_name.getText().length() > 0 && edt_contact.getText().length() > 0 && edt_address.getText().length() > 0)
                    btn_request.setEnabled(true);
                else
                    btn_request.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        edt_address.addTextChangedListener(watcher);
        edt_contact.addTextChangedListener(watcher);
        edt_name.addTextChangedListener(watcher);

        UsrAddressDao defaultUsrAddress = Globals.myInfo.getAddressList().get(0);
        tb_segment.setToggleStatus(0);
        if (Globals.myInfo.getAddressList().get(1).getStatus() == 2) {
            defaultUsrAddress = Globals.myInfo.getAddressList().get(1);
            tb_segment.setToggleStatus(1);
        }
        if (Globals.myInfo.getAddressList().get(2).getStatus() == 2) {
            defaultUsrAddress = Globals.myInfo.getAddressList().get(2);
            tb_segment.setToggleStatus(2);
        }
        setEditValueFromDao(defaultUsrAddress);

        /** +- 버튼을 누르고 있을 때 처리부분 */
        runnablePrice = new Runnable() {
            @Override
            public void run() {
                sell_price = Double.valueOf(edt_price.getText().toString());
                sell_price = sell_price + rate;
                if (sell_price < 5.0)
                    sell_price = 5.0;
                edt_price.setText(String.format("%.01f", sell_price));

                updateReceivedMoney();

                handlerPrice.postDelayed(runnablePrice, interval);
            }
        };
        /** +- 버튼을 누르고 있을 때 가속 처리부분 */
        runnableRate = new Runnable() {
            @Override
            public void run() {
                if (interval == INTERVAL1) {
                    interval = INTERVAL2;
                } else {
                    interval = INTERVAL3;
                }
            }
        };

        updateReceivedMoney();
    }

    private void setEditValueFromDao(UsrAddressDao usrAddress) {
        edt_name.setText(usrAddress.getAddressName());
        edt_contact.setText(usrAddress.getAddressPhone());

        String addressDetail = usrAddress.getAddressDetail();
        String addressDetailSub = usrAddress.getAddressDetailSub();
        edt_address.setText(TextUtils.isEmpty(addressDetail) && TextUtils.isEmpty(addressDetailSub) ? "" : addressDetail + "\n" + addressDetailSub);

        if (Constants.IS_TEST) {
            edt_name.setText("한권익");
            edt_contact.setText("01012345678");
        }
    }

    public ValetCreateReq getValetCreateReq(String signImage) {
        ValetCreateReq result = new ValetCreateReq(edt_name.getText().toString(), edt_contact.getText().toString(), edt_address.getText().toString(), signImage);
        result.setSendType(sendMethod);
        if (isRecommendPrice)
            result.setReqPrice(0);
        else
            result.setReqPrice((int) (sell_price * 10000));
        return result;
    }

    /**
     * 정산금액 업데이트
     */
    private void updateReceivedMoney() {
        if (edt_price != null)
            sell_price = Double.valueOf(edt_price.getText().toString());
        int desirePrice = (int) (sell_price * 7900);
        tv_input_price.setText(Utils.getAttachCommaFormat(desirePrice) + "원");
        tv_input_price.setVisibility(View.VISIBLE);
        if (tv_price_value != null)
            tv_price_value.setText(Utils.getAttachCommaFormat(desirePrice) + "원");
    }
}
