package com.kyad.selluv.api.request;


import android.support.annotation.IntRange;
import android.support.annotation.Size;

import lombok.Data;
import lombok.NonNull;

@Data
public class DealNegoReq {
    //@ApiModelProperty("상품UID")

    private int pdtUid;
    //@ApiModelProperty("제안가격")

    private long reqPrice;
    //@ApiModelProperty("수령자주소 - 성함")
    @Size(min = 1)
    private String recipientNm;
    //@ApiModelProperty("수령자주소 - 연락처")
    @Size(min = 1)
    private String recipientPhone;
    //@ApiModelProperty("수령자주소 - 주소")
    @Size(min = 1)
    private String recipientAddress;
    //@ApiModelProperty("카드UID")

    private int usrCardUid;
    //@ApiModelProperty("도서지역 추가배송비")

    @IntRange(from = 0)
    private long islandSendPrice;
}
