package com.kyad.selluv.api.request;


import android.support.annotation.Size;

import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class PdtStyleMultiReq {

    @NonNull
    @Size(min = 1)
    //@ApiModelProperty("스타일이미지 배열")
    private List<String> styleImgList;
}
