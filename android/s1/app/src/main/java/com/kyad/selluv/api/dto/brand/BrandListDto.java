package com.kyad.selluv.api.dto.brand;

import com.kyad.selluv.api.dto.pdt.PdtInfoDto;

import java.util.List;

import lombok.Data;

@Data
public class BrandListDto {
    //("브랜드UID")
    private int brandUid;
    //("영문첫글자")
    private String firstEn;
    //("한글초성")
    private String firstKo;
    //("영문이름")
    private String nameEn;
    //("한글이름")
    private String nameKo;
    //("로고이미지")
    private String logoImg;
    //("썸네일이미지")
    private String profileImg;
    //("백그라운드 이미지")
    private String backImg;
    //("팔로우 유저수")
    private int brandLikeCount;
    //("유저 팔로우 상태")
    private Boolean brandLikeStatus;
    //("상품목록")
    private List<PdtInfoDto> pdtList;
}
