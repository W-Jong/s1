package com.kyad.selluv.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.api.request.SearchReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.top.SearchResultActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShopMainFragment extends BaseFragment {

    //UI Reference
    @BindView(R.id.fl_popup_fragment)
    FrameLayout fl_popup_fragment;

    //Vars
    View rootView;
    FragmentManager fm;
    FragmentTransaction transaction;

    ShopFragment shopFragment;
    CategoryFragment categoryFragment;
    ThemaFragment themaFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sub_main, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;

    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (shopFragment != null) shopFragment.setUserVisibleHint(isVisibleToUser);
        if (categoryFragment != null) categoryFragment.setUserVisibleHint(isVisibleToUser);
        if (themaFragment != null) themaFragment.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Globals.searchReq == null) {
            Globals.searchReq = new SearchReq();
        }
        Globals.initSearchFilters();
    }

    private void loadLayout() {
        if (shopFragment != null) {
            shopFragment = ShopFragment.newInstance(this, shopFragment.getSelectedTab());
        } else {
            shopFragment = ShopFragment.newInstance(this, 0);
        }
//        if (categoryFragment == null)
//            categoryFragment = CategoryFragment.newInstance(this);
//        if (themaFragment == null)
//            themaFragment = ThemaFragment.newInstance(this);
        fm = getChildFragmentManager();

        gotoShopFragment();
    }

    public void gotoShopFragment() {
        fl_popup_fragment.setVisibility(View.INVISIBLE);
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, shopFragment).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void gotoCategoryFragment() {
//        categoryFragment = CategoryFragment.newInstance(this);
//        transaction = fm.beginTransaction();
//        transaction.replace(R.id.fl_popup_fragment, categoryFragment).addToBackStack(null);
//        transaction.commitAllowingStateLoss();
//
//        fl_popup_fragment.setVisibility(View.VISIBLE);
        int categoryUid = (Globals.categoryDepth <= 1) ? Globals.categoryIndex[0] : Globals.categoryIndex[Globals.categoryDepth - 1];

        Intent intent = new Intent(getActivity(), CategoryActivity.class);
        intent.putExtra(CategoryActivity.PDT_GROUP, Globals.currentShopperTypeIdx == 0 ? PDT_GROUP_TYPE.MALE.getCode() : Globals.currentShopperTypeIdx == 1 ? PDT_GROUP_TYPE.FEMALE.getCode() : PDT_GROUP_TYPE.KIDS.getCode());
        intent.putExtra(CategoryActivity.CATEGORY_UID, categoryUid);
        getActivity().startActivity(intent);
    }

    public void gotoThemaFragment(int themeUid) {
        themaFragment = ThemaFragment.newInstance(this, themeUid);
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_popup_fragment, themaFragment).addToBackStack(null);
        transaction.commitAllowingStateLoss();

        fl_popup_fragment.setVisibility(View.VISIBLE);

        /*Intent intent = new Intent(getActivity(), ThemeActivity.class);
        getActivity().startActivity(intent);*/
    }

    public void gotoCategorySelectActivity() {
        Intent intent = new Intent(getActivity(), ShopCategorySelectActivity.class);
        getActivity().startActivityForResult(intent, Constants.REQ_RESULT_SHOP_CATEGORY_SELECT_ACTIVITY);
    }

    public void closeCategorySelectActivity() {
//        Globals.categoryDepth = 1;
//        Globals.currentShopperTypeIdx = shopperType;
//        Globals.categoryIndex[0] = index;
        gotoCategoryFragment();
    }

    public void gotoSearchResultActivity(String title) {
        /*transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, categoryFragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();*/

        Intent intent = new Intent(getActivity(), SearchResultActivity.class);
        intent.putExtra(Constants.PAGE_TITLE, title);
        intent.putExtra(Constants.FROM_PAGE, Constants.WIRE_FRAG_SHOP);
        getActivity().startActivity(intent);
    }

    public void closePopupFragment() {
        fl_popup_fragment.setVisibility(View.INVISIBLE);
        categoryFragment = null;
        themaFragment = null;
    }
}
