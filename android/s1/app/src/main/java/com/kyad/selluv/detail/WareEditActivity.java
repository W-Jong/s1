package com.kyad.selluv.detail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.category.SizeRefDto;
import com.kyad.selluv.api.dto.pdt.PdtMiniDto;
import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.api.request.PdtUpdateReq;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.edit.PdtImageAdapter;
import com.kyad.selluv.sell.SellInfoAddActivity;
import com.kyad.selluv.sell.SellPriceActivity;
import com.kyad.selluv.sell.fragment.SellAccSizeFragment;
import com.kyad.selluv.sell.fragment.SellBagSizeFragment;
import com.kyad.selluv.sell.fragment.SellConditionFragment;
import com.kyad.selluv.sell.fragment.SellDirectInfoAddAcc;
import com.kyad.selluv.sell.fragment.SellDirectInfoAddColor;
import com.kyad.selluv.sell.fragment.SellDirectPickerFragment;
import com.kyad.selluv.sell.fragment.SellModelFragment;
import com.kyad.selluv.sell.fragment.SellTagFragment;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.ACT_PDT_EDITED;

public class WareEditActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.rl_frag)
    RelativeLayout rl_frag;
    @BindView(R.id.frag_acc)
    RelativeLayout rl_acc;
    @BindView(R.id.frag_color)
    RelativeLayout rl_color;
    @BindView(R.id.frag_model)
    RelativeLayout rl_model;
    @BindView(R.id.frag_size)
    RelativeLayout frag_size;
    @BindView(R.id.frag_condition)
    RelativeLayout frag_condition;
    @BindView(R.id.frag_tag)
    RelativeLayout frag_tag;
    @BindView(R.id.tv_item_price_value)
    TextView tv_item_price_value;
    @BindView(R.id.tv_transport_price_value)
    TextView tv_transport_price_value;
    //SellBagSizeFragment, SellAccSizeFragment, SellDirectPickerFragment 에서 이용하기 때문에 이 아이디(tv_item_size_value)는 변경하면 안됨
    @BindView(R.id.tv_item_size_value)
    TextView tv_item_size_value;
    //SellConditionFragment 에서 이용하기 때문에 이 아이디(tv_condition_value)는 변경하면 안됨
    @BindView(R.id.tv_condition_value)
    TextView tv_condition_value;
    @BindView(R.id.tv_tagadd_value)
    TextView tv_tagadd_value;
    @BindView(R.id.tv_belonging_input)
    TextView tv_belonging_input;
    @BindView(R.id.tv_color_inputs)
    TextView tv_color_inputs;
    @BindView(R.id.tv_model_input)
    TextView tv_model_input;
    @BindView(R.id.tv_detail_value)
    TextView tv_detail_value;
    @BindView(R.id.tb_nego_allow)
    TriStateToggleButton tb_nego_allow;
    @BindView(R.id.tb_selling)
    TriStateToggleButton tb_selling;
    @BindView(R.id.cardView)
    RecyclerView cardView;

    //Vars
    SellBagSizeFragment bagSizeFragment = new SellBagSizeFragment();
    SellAccSizeFragment accSizeFragment = new SellAccSizeFragment();
    SellDirectPickerFragment pickerFragment = new SellDirectPickerFragment();
    SellConditionFragment conditionFragment = new SellConditionFragment();
    SellTagFragment tagFragment = new SellTagFragment();
    SellDirectInfoAddAcc directInfoAddAcc = new SellDirectInfoAddAcc();
    SellDirectInfoAddColor directInfoAddColor = new SellDirectInfoAddColor();
    SellModelFragment modelFragment = new SellModelFragment();

    /**
     * 카테고리에 따른 사이즈 타입
     */
    public SizeRefDto sizeRefDto;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACT_PDT_EDITED)) {
                updateUI();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdt_edit);

        Globals.isEditing = true;
        if (Globals.pdtUpdateReq == null) {
            Globals.pdtUpdateReq = new PdtUpdateReq();
        } else {
            Globals.pdtUpdateReq.init();
        }

        /** 초기화 */
        Globals.pdtUpdateReq.setComponent(Globals.selectedPdtDetailDto.getPdt().getComponent());
        Globals.pdtUpdateReq.setContent(Globals.selectedPdtDetailDto.getPdt().getContent());
        Globals.pdtUpdateReq.setEtc(Globals.selectedPdtDetailDto.getPdt().getEtc());
        Globals.pdtUpdateReq.setNegoYn(Globals.selectedPdtDetailDto.getPdt().getNegoYn());
        Globals.pdtUpdateReq.setPdtColor(Globals.selectedPdtDetailDto.getPdt().getColorName());
        Globals.pdtUpdateReq.setPdtCondition(PDT_CONDITION_TYPE.values()[Globals.selectedPdtDetailDto.getPdt().getPdtCondition() - 1]);
        Globals.pdtUpdateReq.setPdtModel(Globals.selectedPdtDetailDto.getPdt().getPdtModel());
        Globals.pdtUpdateReq.setPdtSize(Globals.selectedPdtDetailDto.getPdt().getPdtSize());
        Globals.pdtUpdateReq.setPhotos(Globals.selectedPdtDetailDto.getPdt().getPhotoList());
        Globals.pdtUpdateReq.setPrice(Globals.selectedPdtDetailDto.getPdt().getPrice());
        Globals.pdtUpdateReq.setSendPrice(Globals.selectedPdtDetailDto.getPdt().getSendPrice());
        Globals.pdtUpdateReq.setPromotionCode("0");
        Globals.pdtUpdateReq.setIsSell(Globals.selectedPdtDetailDto.getPdt().getStatus() == 3 ? false : true);
        Globals.pdtUpdateReq.setTag(Globals.selectedPdtDetailDto.getPdt().getTag());

        loadLayout();
        loadSizeInf();

        IntentFilter f = new IntentFilter();
        f.addAction(ACT_PDT_EDITED);
        registerReceiver(mBroadcastReceiver, new IntentFilter(f));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
        Globals.isEditing = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3000 && resultCode == RESULT_OK) {
            //카메라
            String[] list = data.getStringArrayExtra(Constants.IMAGES);
            cardView.setAdapter(new PdtImageAdapter(this, new ArrayList<>(Arrays.asList(list))));
            if (list.length > 0) {
                //Globals.pdtUpdateReq.setPhotos();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (frag_size.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (frag_condition.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (frag_tag.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (rl_model.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (rl_color.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (rl_acc.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else {
            setResult(RESULT_CANCELED);
            super.onBackPressed();
        }
    }

    @OnClick(R.id.ib_left)
    void onBack(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * 사진 선택
     *
     * @param v
     */
    @OnClick(R.id.btn_go_photo)
    void goCapture(View v) {
        Intent intent = new Intent(this, SellPicActivity.class);
        intent.putExtra(Constants.FROM_PAGE, SellPicActivity.FROM_PAGE_PDT_EDIT);
        intent.putExtra(Constants.IMAGES, Globals.pdtUpdateReq.getPhotos().toArray(new String[]{}));
        startActivityForResult(intent, 3000);
    }

    /**
     * 상품가격
     *
     * @param v
     */
    @OnClick(R.id.rl_price)
    void goPrice(View v) {
        Intent intent = new Intent(this, SellPriceActivity.class);
        intent.putExtra(Constants.FROM_MYPAGE, true);
        startActivity(intent);
    }

    /**
     * 배송비
     *
     * @param v
     */
    @OnClick(R.id.rl_transport_price)
    void goTransPrice(View v) {
        final PopupDialog popup = new PopupDialog(this, R.layout.popup_post_price).setFullScreen().setBackGroundCancelable(true).setCancelable(true).setOnCancelClickListener(null);
        Utils.hideKeypad(this, popup.findView(R.id.ll_nego_background));
        popup.setOnClickListener(R.id.tv_guide, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                Intent intent = new Intent(WareEditActivity.this, GuidImagePopup.class);
                intent.putExtra(Constants.POPUP_IMAGE, R.drawable.mypage_sales_post_guide);
                startActivity(intent);
            }
        });
        popup.setOnOkClickListener(new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                EditText edt_price = (EditText) popup.findView(R.id.edt_price);
                int val = 0;
                if (edt_price.length() == 0) {
                    tv_transport_price_value.setText(Utils.getAttachCommaFormat(0));
                } else {
                    val = Utils.str2int(edt_price.getText().toString());
                    tv_transport_price_value.setText(Utils.getAttachCommaFormat(val));
                }
                Globals.pdtUpdateReq.setSendPrice(val);
                popup.dismiss();

            }
        });
        popup.show();
    }

    /**
     * 사이즈
     *
     * @param v
     */
    @OnClick(R.id.rl_size)
    void goEditSize(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 컨디션
     *
     * @param v
     */
    @OnClick(R.id.rl_condition)
    void goEditCondition(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 태그
     *
     * @param v
     */
    @OnClick(R.id.rl_tag)
    void goEditTag(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 부속품
     *
     * @param v
     */
    @OnClick(R.id.rl_accessary)
    void goEditAccessary(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 컬러
     *
     * @param v
     */
    @OnClick(R.id.rl_color)
    void goEditColor(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 모델
     *
     * @param v
     */
    @OnClick(R.id.rl_model_name)
    void goEditModel(View v) {
        frag_size.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_condition.findViewById(R.id.frag_main).setVisibility(View.GONE);
        frag_tag.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
    }

    /**
     * 상세설명
     *
     * @param v
     */
    @OnClick(R.id.rl_detail)
    void goEditDetail(View v) {
        Intent intent = new Intent(this, SellInfoAddActivity.class);
        intent.putExtra(Constants.FROM_MYPAGE, true);
        startActivity(intent);

    }

    /**
     * 상품 삭제
     *
     * @param v
     */
    @OnClick(R.id.rl_delete)
    void goDelete(View v) {
        cmdDeletePdt();
    }

    /**
     * 수정하기
     *
     * @param v
     */
    @OnClick(R.id.btn_more)
    void goDone(View v) {

        cmdEditPdt();

    }

    private void loadLayout() {

        cardView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        if (Globals.pdtUpdateReq.getPhotos().size() > 0 & cardView != null) {
            cardView.setAdapter(new PdtImageAdapter(this, new ArrayList<>(Globals.pdtUpdateReq.getPhotos())));
        }
        cardView.setLayoutManager(MyLayoutManager);

        updateUI();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.frag_acc, directInfoAddAcc);
        transaction.replace(R.id.frag_color, directInfoAddColor);
        transaction.replace(R.id.frag_model, modelFragment);

        transaction.commit();
    }

    /**
     * 상세페이지에서 변경된 상품정보로 UI를 업데이트 함
     */
    private void updateUI() {
        tv_item_price_value.setText(Utils.getAttachCommaFormat(Globals.pdtUpdateReq.getPrice()));
        tv_transport_price_value.setText(Utils.getAttachCommaFormat(Globals.pdtUpdateReq.getSendPrice()));
        tv_item_size_value.setText(Globals.pdtUpdateReq.getPdtSize());
        //("컨디션 1-새상품, 2-최상, 3-상, 4-중상")
        tv_condition_value.setText(Constants.PDT_CONDITION_LABEL[Globals.pdtUpdateReq.getPdtCondition().getCode() - 1]);
        tv_tagadd_value.setText(Globals.pdtUpdateReq.getTag());
        //부속품
        String s_component = Globals.componentCodeToName(Globals.pdtUpdateReq.getComponent());
        String s_etc = Globals.pdtUpdateReq.getEtc();
        if (s_etc.length() > 0) {
            s_component += (" " + s_etc);
        }
        tv_belonging_input.setText(s_component.trim());
        tv_color_inputs.setText(Globals.pdtUpdateReq.getPdtColor());
        tv_model_input.setText(Globals.pdtUpdateReq.getPdtModel());
        tv_detail_value.setText(Globals.pdtUpdateReq.getContent());
        //네고허용
        if (Globals.pdtUpdateReq.getNegoYn() == 1)
            tb_nego_allow.setToggleStatus(true);
        else
            tb_nego_allow.setToggleStatus(false);

        tb_nego_allow.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {

                if (booleanToggleStatus) {
                    Globals.pdtUpdateReq.setNegoYn(1);
                } else {
                    Globals.pdtUpdateReq.setNegoYn(2);
                }

            }
        });

        //("상태  0-삭제, 1-판매중 2-판매완료, 3-판매중지, 4-휴가모드")
        if (Globals.pdtUpdateReq.getIsSell())
            tb_selling.setToggleStatus(true);
        else
            tb_selling.setToggleStatus(false);

        tb_selling.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {

                Globals.pdtUpdateReq.setIsSell(booleanToggleStatus);

            }
        });
    }

    private void cmdDeletePdt() {
        setResult(999);
        finish();

//        showLoading();
//
//        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
//        Call<GenericResponse<Void>> genRes = restAPI.pdtDelete(Globals.userToken, Globals.selectedPdtDetailDto.getPdt().getPdtUid());
//        genRes.enqueue(new TokenCallback<Void>(this) {
//            @Override
//            public void onSuccess(Void response) {
//                closeLoading();
//
//                Utils.showToast(WareEditActivity.this, "삭제되었습니다.");
//            }
//
//            @Override
//            public void onFailed(Throwable t) {
//                closeLoading();
//            }
//        });
    }

    private void loadSizeInf() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<SizeRefDto>> genRes = restAPI.getCategorySize(Globals.userToken, Globals.selectedPdtDetailDto.getPdt().getCategoryUid());
        genRes.enqueue(new TokenCallback<SizeRefDto>(this) {
            @Override
            public void onSuccess(SizeRefDto response) {
                closeLoading();

                sizeRefDto = response;

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();


                if (sizeRefDto.getSizeType() == 1)
                    transaction.replace(R.id.frag_size, bagSizeFragment);
                else if (sizeRefDto.getSizeType() == 2)
                    transaction.replace(R.id.frag_size, accSizeFragment);
                else
                    transaction.replace(R.id.frag_size, pickerFragment);

                transaction.replace(R.id.frag_condition, conditionFragment);
                transaction.replace(R.id.frag_tag, tagFragment);
                transaction.commit();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void cmdEditPdt() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<PdtMiniDto>> genRes = restAPI.editPdt(Globals.userToken, Globals.selectedPdtDetailDto.getPdt().getPdtUid(), Globals.pdtUpdateReq);
        genRes.enqueue(new TokenCallback<PdtMiniDto>(this) {
            @Override
            public void onSuccess(PdtMiniDto response) {
                closeLoading();

                Utils.showToast(WareEditActivity.this, "수정 되었습니다.");
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
