package com.kyad.selluv.api.request;


import lombok.Data;
import lombok.NonNull;

@Data
public class UserLikeGroupUpdateReq {

    @NonNull
    //@ApiModelProperty("남성군 좋아요")
    private Boolean isMale;
    @NonNull
    //@ApiModelProperty("여성군 좋아요")
    private Boolean isFemale;
    @NonNull
    //@ApiModelProperty("키즈군 좋아요")
    private Boolean isChildren;

    public UserLikeGroupUpdateReq() {

    }
}
