package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.top.viewholder.CategoryFilterNodeVH;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFilterActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.rl_treeView)
    RelativeLayout rl_treeView;

    AndroidTreeView tView;

    List<CategoryDao> rootCategores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_category);


        CategoryDao cat_1 = new CategoryDao();
        cat_1.setCategoryUid(1);
        cat_1.setCategoryName(Constants.shopper_type[0]);
        rootCategores.add(cat_1);
        CategoryDao cat_2 = new CategoryDao();
        cat_2.setCategoryUid(2);
        cat_2.setCategoryName(Constants.shopper_type[1]);
        rootCategores.add(cat_2);
        CategoryDao cat_4 = new CategoryDao();
        cat_4.setCategoryUid(4);
        cat_4.setCategoryName(Constants.shopper_type[2]);
        rootCategores.add(cat_4);


        loadLayout();
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    /**
     * 초기화
     */
    @OnClick(R.id.tv_init)
    void onInit() {
        Globals.categoryFilterName = "";
        Globals.categoryFilterPath = "";
        Globals.searchReq.setCategoryUid(0);

        tView.deselectAll();
    }

    /**
     * 검색결과 보기
     */
    @OnClick(R.id.btn_ok)
    void onOk() {
        sendBroadcast(new Intent(Constants.ACT_SEARCH_DETAIL));
        setResult(RESULT_OK);
        finish();
    }

    private void loadLayout() {

        TreeNode root = TreeNode.root();
        addToRoot(root, rootCategores);

        tView = new AndroidTreeView(this, root);
        tView.setSelectionModeEnabled(true);
        tView.setDefaultAnimation(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        tView.setDefaultViewHolder(CategoryFilterNodeVH.class);
        tView.setDefaultNodeClickListener(nodeClickListener);

        rl_treeView.addView(tView.getView());
    }


    void addToRoot(TreeNode root, final List<CategoryDao> items) {
        if (root == null || items == null) return;


        for (CategoryDao item :
                items) {
            final TreeNode treeNode = new TreeNode(item).setViewHolder(new CategoryFilterNodeVH(this));
            treeNode.setSelectable(true);
            root.addChild(treeNode);
            //kkk addToRoot(treeNode, item.subItems);

            showLoading();

            RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
            Call<GenericResponse<List<CategoryDao>>> genRes = restAPI.getSubCategories(Globals.userToken, item.getCategoryUid());
            genRes.enqueue(new Callback<GenericResponse<List<CategoryDao>>>() {

                @Override
                public void onResponse(Call<GenericResponse<List<CategoryDao>>> call, Response<GenericResponse<List<CategoryDao>>> response) {
                    closeLoading();

                    Log.d("kyad-log", response.body().toString());
                    if (response.body().getMeta().getErrCode() == 0) {//성공
                        addToRoot(treeNode, response.body().getData());
                    } else {//실패
                        Toast.makeText(CategoryFilterActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<GenericResponse<List<CategoryDao>>> call, Throwable t) {
                    closeLoading();
                    Log.d("kyad-log", t.getMessage());
                    Toast.makeText(CategoryFilterActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private TreeNode.TreeNodeClickListener nodeClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            CategoryDao item = (CategoryDao) value;
            item.setStatus(1);
            //tView.selectNode(node, true);
            Globals.categoryFilterPath = node.getPath();
            Globals.categoryFilterName = item.getCategoryName();
            Globals.searchReq.setCategoryUid(item.getCategoryUid());
            Utils.showToast(CategoryFilterActivity.this, item.getCategoryName());
        }
    };

//    private int getGuideType(TreeNode node) {
//        if (node.get)
//        return 1;
//    }
}
