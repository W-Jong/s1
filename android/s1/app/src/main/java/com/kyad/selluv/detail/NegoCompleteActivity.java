package com.kyad.selluv.detail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.mypage.PurchaseDetailActivity;

import butterknife.BindView;
import butterknife.OnClick;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import static android.graphics.Paint.STRIKE_THRU_TEXT_FLAG;

public class NegoCompleteActivity extends BaseActivity {

    public static final String DEAL_UID = "DEAL_UID";
    public static final String NEGO_PRICE = "NEGO_PRICE";
    public static final String EXTRA_ISLAND_SEND_PRICE = "EXTRA_ISLAND_SEND_PRICE";

    private int dealUid = 0;
    private long negoPrice = 0;
    private long islandSendPrice = 0;

    //UI Reference
    @BindView(R.id.tv_ware)
    TextView tv_ware;

    @BindView(R.id.tv_ware_price)
    TextView tv_ware_price;

    @BindView(R.id.tv_nego_price)
    TextView tv_nego_price;

    @BindView(R.id.tv_expect_price)
    TextView tv_expect_price;

    @BindView(R.id.tv_send_price)
    TextView tv_send_price;

    @OnClick(R.id.btn_verify)
    void onVerify(View v) {
        Intent intent = new Intent(this, PurchaseDetailActivity.class);
        intent.putExtra(DEAL_UID, dealUid);
        startActivity(intent);
        finish();
        Globals.selectedPdtDetailDto = null;
    }

    @OnClick(R.id.btn_continue)
    void onContinue(View v) {
        finish();
        Globals.selectedPdtDetailDto = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_nego_checkout_complete);
        setStatusBarWhite();

        dealUid = getIntent().getIntExtra(DEAL_UID, 0);
        negoPrice = getIntent().getLongExtra(NEGO_PRICE, 0);
        islandSendPrice = getIntent().getLongExtra(EXTRA_ISLAND_SEND_PRICE, 0);

        if (dealUid < 1 || Globals.selectedPdtDetailDto == null) {
            finish();
        }

        loadLayout();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        onContinue(null);
    }

    private void loadLayout() {

        tv_ware.setText(Globals.selectedPdtDetailDto.getPdtTitle());

        tv_ware_price.setPaintFlags(STRIKE_THRU_TEXT_FLAG | ANTI_ALIAS_FLAG);
        tv_ware_price.setText(Utils.getAttachCommaFormat(Globals.selectedPdtDetailDto.getPdt().getPrice()));
        tv_nego_price.setText(Utils.getAttachCommaFormat(negoPrice));
        long finalSendPrice = Globals.selectedPdtDetailDto.getPdt().getSendPrice();
        if (Globals.selectedPdtDetailDto.getSeller().getFreeSendPrice() > 0 &&
                negoPrice > Globals.selectedPdtDetailDto.getSeller().getFreeSendPrice()) {
            finalSendPrice = 0;
        }
        finalSendPrice += islandSendPrice;

        tv_send_price.setText(Utils.getAttachCommaFormat(finalSendPrice));
        tv_expect_price.setText(Utils.getAttachCommaFormat(negoPrice + finalSendPrice));
    }

}
