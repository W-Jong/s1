/**
 * 홈
 */
package com.kyad.selluv.camera;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.GalleryListAdapter;
import com.kyad.selluv.detail.DetailActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class GalleryFragment extends DialogFragment implements GalleryListAdapter.onDirClickListener {


    public interface ImageSelectInterface {
        void onImageSelect(Bitmap bmp);
    }

    //UI Refs
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.gridView)
    GridView gridView;
    @BindView(R.id.ib_close)
    ImageButton ibClose;
    @BindView(R.id.btn_confirm)
    Button btn_confirm;
    @BindView(R.id.rlFolder)
    RelativeLayout rlFolder;
    @BindView(R.id.rvFolder)
    RecyclerView rvFolder;

    //Vars
    private View rootView;
    public loadImagesTask loadTask = new loadImagesTask();
    GalleryAdapter imageGalleryAdapter = null;
    public static String[] galleryList = {"카메라", "최근 사진", "스크린 샷", "다운로드", "셀럽"};
    public static int selectedList = 0;
    public ArrayList<String> images = new ArrayList<>();

    public int fromPage = SellPicActivity.FROM_PAGE_SELL;
    public int frameType = 0;   // 0: default 1: circle 2: square
    public ImageSelectInterface imageSelectInterface = null;


    ArrayList<String> all = new ArrayList<>();
    ArrayList<String> cameraA = new ArrayList<>();
    ArrayList<String> screenshot = new ArrayList<>();
    ArrayList<String> download = new ArrayList<>();
    ArrayList<String> sellca = new ArrayList<>();

    ArrayList<GalleryListAdapter.DirItem> listItems = new ArrayList<>();

    GalleryListAdapter.DirItem dirAll;
    GalleryListAdapter.DirItem dirCamera;
    GalleryListAdapter.DirItem dirScreenShot;
    GalleryListAdapter.DirItem dirDownloads;
    GalleryListAdapter.DirItem dirSellca;

    List[] list = {cameraA, all, screenshot, download, sellca};
    Date now = new Date();

    public static int recentThreshold = 1000 * 60 * 60 * 24; //1 day


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, rootView);

        selectedList = 0;
        loadLayout();
        loadData();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((BaseActivity) getActivity()).showLoading();
        loadTask.execute(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM));
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @OnClick(R.id.ib_close)
    void onCancel(View v) {
        dismiss();
    }

    @OnClick(R.id.rl_gallery_list)
    void onGalleryList(View v) {
        if (rlFolder.getVisibility() == View.VISIBLE) {
            rlFolder.setVisibility(View.INVISIBLE);
            btn_confirm.setVisibility(View.VISIBLE);
            ibClose.setVisibility(View.VISIBLE);
        } else {
            rlFolder.setVisibility(View.VISIBLE);
            btn_confirm.setVisibility(View.INVISIBLE);
            ibClose.setVisibility(View.INVISIBLE);

            rvFolder.getAdapter().notifyDataSetChanged();
        }
    }

    @OnClick(R.id.btn_confirm)
    void onConfirm(View v) {
        if (fromPage == SellPicActivity.FROM_PAGE_SELL)
            ((SellPicActivity) getActivity()).reloadThumbnails(imageGalleryAdapter.selectedImages);
        else if (fromPage == SellPicActivity.FROM_PAGE_PDT_DETAIL) {
            if (imageGalleryAdapter.selectedImages.size() > 0)
                ((DetailActivity) getActivity()).uploadImages(imageGalleryAdapter.selectedImages);
        }
        dismiss();
    }

    @Override
    public void onDirClick(RecyclerView.ViewHolder holder, int position) {
        rlFolder.setVisibility(View.INVISIBLE);
        btn_confirm.setVisibility(View.VISIBLE);
        ibClose.setVisibility(View.VISIBLE);

        selectedList = position;
        reloadData((ArrayList<String>) list[position]);
    }

    private void loadLayout() {
        if (fromPage == SellPicActivity.FROM_PAGE_MYPAGE)
            btn_confirm.setVisibility(View.INVISIBLE);
        tv_title.setText(galleryList[selectedList]);

        imageGalleryAdapter = new GalleryAdapter(this, images);
        gridView.setAdapter(imageGalleryAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvFolder.setLayoutManager(layoutManager);
    }

    private void loadData() {


        dirCamera = cameraA.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[0], 0) :
                new GalleryListAdapter.DirItem(cameraA.get(0), galleryList[1], cameraA.size());
        listItems.add(dirCamera);

        dirAll = all.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[1], 0) :
                new GalleryListAdapter.DirItem(all.get(0), galleryList[0], all.size());
        listItems.add(dirAll);

        dirScreenShot = screenshot.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[2], 0) :
                new GalleryListAdapter.DirItem(screenshot.get(0), galleryList[2], screenshot.size());
        listItems.add(dirScreenShot);

        dirDownloads = download.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[3], 0) :
                new GalleryListAdapter.DirItem(download.get(0), galleryList[3], download.size());
        listItems.add(dirDownloads);

        dirSellca = sellca.size() == 0 ? new GalleryListAdapter.DirItem(null, galleryList[4], 0) :
                new GalleryListAdapter.DirItem(sellca.get(0), galleryList[4], sellca.size());
        listItems.add(dirSellca);

        GalleryListAdapter adapter = new GalleryListAdapter(getContext(), listItems);
        adapter.setDirClickListener(this);
        rvFolder.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void reloadData(ArrayList<String> imgs) {
        this.images = imgs;
        imageGalleryAdapter.setData(images);
        imageGalleryAdapter.notifyDataSetChanged();
        updateTitle();
    }

    public void updateTitle() {
        tv_title.setText(galleryList[selectedList]);
    }

    public void walkdir(File dir, ArrayList<String> arrayList) {

        File[] listFile;
        listFile = dir.listFiles();

        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    walkdir(listFile[i], arrayList);
                } else {
                    if (listFile[i].getName().toLowerCase().endsWith(".jpg") || listFile[i].getName().toLowerCase().endsWith(".png")) {
                        arrayList.add(listFile[i].getAbsolutePath());
                        if (arrayList == cameraA) {
                            dirCamera.cnt = arrayList.size();
                            dirCamera.thumb = arrayList.get(0);
                        }
                        if (arrayList == screenshot) {
                            dirScreenShot.cnt = arrayList.size();
                            dirScreenShot.thumb = arrayList.get(0);
                        }
                        if (arrayList == download) {
                            dirDownloads.cnt = arrayList.size();
                            dirDownloads.thumb = arrayList.get(0);
                        }
                        if (arrayList == sellca) {
                            dirSellca.cnt = arrayList.size();
                            dirSellca.thumb = arrayList.get(0);
                        }
                        //loadTask.doProgress();
                    }
                }
            }
        }
    }

    public void findAll(File dir) {

        File[] listFile;
        listFile = dir.listFiles();

        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    findAll(listFile[i]);
                } else {
                    if (listFile[i].getName().toLowerCase().endsWith(".jpg") || listFile[i].getName().toLowerCase().endsWith(".png")) {

                        if (now.getTime() - listFile[i].lastModified() < recentThreshold) {
                            all.add(listFile[i].getAbsolutePath());

                            dirAll.cnt = all.size();
                            dirAll.thumb = all.get(0);
                        }
                        //loadTask.doProgress();
                    }
                }
            }
        }
    }

    public void ScanFiles() {
        findAll(Environment.getExternalStorageDirectory());
        walkdir(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), cameraA);
        walkdir(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), screenshot);
        walkdir(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), download);
        walkdir(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), sellca);
    }

    public class loadImagesTask extends AsyncTask<File, String, Integer> {
        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            ((BaseActivity) getActivity()).closeLoading();

            selectedList = 0;
            reloadData((ArrayList<String>) list[0]);
        }

        @Override
        protected Integer doInBackground(File... files) {

            ScanFiles();

            loadTask.doProgress();

            return 0;
        }

        public void doProgress() {
            publishProgress();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (fromPage == SellPicActivity.FROM_PAGE_MYPAGE)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
