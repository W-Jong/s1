package com.kyad.selluv.mypage;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.FaqFragment;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class FaqActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_quiz)
    void onQuiz() {
        final PopupDialog popup = new PopupDialog(this, R.layout.popup_mypage_qa).setFullScreen().setBackGroundCancelable(true).setCancelable(true);
        popup.setOnClickListener(R.id.rl_p2p, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                startActivity(QaActivity.class, false, 0, 0);
            }
        });
        popup.setOnClickListener(R.id.ib_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });
        popup.setOnClickListener(R.id.rl_kakao, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
            }
        });
        popup.setOnClickListener(R.id.btn_cs_phone, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
            }
        });
        popup.show();

    }

    int selectedTab = 0;
    FaqFragment salesFragment, purchaseFragment, actionFragment;
    private PageAdapter pagerAdapter;

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_faq);
        setStatusBarWhite();
        initFragment();
        loadLayout();
        selectPage();
    }

    private void initFragment() {
        if (salesFragment == null) {
            salesFragment = new FaqFragment();
            salesFragment.setLoadInterface(new FaqFragment.DataLoadInterface() {
                @Override
                public Model.FaqItem[] loadData() {
                    return loadSalesData();
                }
            });
        }

        if (purchaseFragment == null) {
            purchaseFragment = new FaqFragment();
            purchaseFragment.setLoadInterface(new FaqFragment.DataLoadInterface() {
                @Override
                public Model.FaqItem[] loadData() {
                    return loadPurchaseData();
                }
            });
        }
        if (actionFragment == null) {
            actionFragment = new FaqFragment();
            actionFragment.setLoadInterface(new FaqFragment.DataLoadInterface() {
                @Override
                public Model.FaqItem[] loadData() {
                    return loadActionData();
                }
            });
        }
    }

    private void selectPage() {
        if (selectedTab == 0) {
            salesFragment.setTab(0);
        } else if (selectedTab == 1) {
            purchaseFragment.setTab(1);
        } else {
            actionFragment.setTab(2);
        }
        tl_top_tab.getTabAt(selectedTab).select();
    }

    public Model.FaqItem[] loadSalesData() {
        ArrayList<Model.FaqItem> noticeItemArrayList = new ArrayList<Model.FaqItem>();
        Collections.addAll(noticeItemArrayList, Constants.faqItems);
        int cnt = noticeItemArrayList.size();
        return (Model.FaqItem[]) noticeItemArrayList.toArray(new Model.FaqItem[cnt]);
    }

    public Model.FaqItem[] loadPurchaseData() {
        ArrayList<Model.FaqItem> noticeItemArrayList = new ArrayList<Model.FaqItem>();
        Collections.addAll(noticeItemArrayList, Constants.faqItems);
        int cnt = noticeItemArrayList.size();
        return (Model.FaqItem[]) noticeItemArrayList.toArray(new Model.FaqItem[cnt]);
    }

    public Model.FaqItem[] loadActionData() {
        ArrayList<Model.FaqItem> noticeItemArrayList = new ArrayList<Model.FaqItem>();
        Collections.addAll(noticeItemArrayList, Constants.faqItems);
        int cnt = noticeItemArrayList.size();
        return (Model.FaqItem[]) noticeItemArrayList.toArray(new Model.FaqItem[cnt]);
    }

    private void loadLayout() {
        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
//                tl_top_tab.getTabAt(position).select();
                selectPage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                }
            }
        });
        vp_main.setOffscreenPageLimit(4);

        tl_top_tab.getTabAt(0).select();
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(this, tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(this, tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = salesFragment;
                    break;
                case 1:
                    frag = purchaseFragment;
                    break;
                case 2:
                    frag = actionFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 3;
        }

    }

}
