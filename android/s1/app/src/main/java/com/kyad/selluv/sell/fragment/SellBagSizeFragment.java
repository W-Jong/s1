/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.WareEditActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SellBagSizeFragment extends BaseFragment {

    //UI Refs
    @BindView(R.id.txt_width_input)
    TextView txt_width_input;
    @BindView(R.id.txt_height_input)
    TextView txt_height_input;
    @BindView(R.id.txt_thick_input)
    TextView txt_thick_input;
    @BindView(R.id.iv_edit)
    ImageButton iv_edit;
    @BindView(R.id.tv_finish)
    TextView tv_finish;

    //Vars
    private View rootView;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_info_bagsize, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    private void loadLayout() {
        Utils.hideKeypad(getActivity(), rootView.findViewById(R.id.frag_main));

        if (Globals.isEditing) {
            tv_finish.setVisibility(View.VISIBLE);
            iv_edit.setVisibility(View.GONE);
        } else {
            tv_finish.setVisibility(View.GONE);
            iv_edit.setVisibility(View.VISIBLE);
        }
    }


    @OnClick({R.id.rl_actionbar, R.id.tv_finish})
    void onDone(View v) {
        String s_size = "";

        if (txt_width_input.length() == 0 || txt_height_input.length() == 0 || txt_thick_input.length() == 0) {

        } else {
            int w = Integer.valueOf(txt_width_input.getText().toString());
            int h = Integer.valueOf(txt_height_input.getText().toString());
            int d = Integer.valueOf(txt_thick_input.getText().toString());
            if (w <= 0 || h <= 0 || d <= 0) {

            } else {
                s_size = txt_width_input.getText().toString() + "×" + txt_height_input.getText().toString() + "×" + txt_thick_input.getText().toString();
            }
        }

        if (Globals.isEditing) {
            Globals.pdtUpdateReq.setPdtSize(s_size);
        } else {
            Globals.pdtCreateReq.setPdtSize(s_size);
        }

        TextView tvSize = getActivity().findViewById(R.id.tv_item_size_value);
        tvSize.setText(s_size);


        getActivity().onBackPressed();
    }
}
