package com.kyad.selluv.api.enumtype;

public enum SNS_TYPE {
    FACEBOOK(1), NAVER(2), KAKAOTALK(3), INSTAGRAM(4), TWITTER(5);

    private int code;

    private SNS_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
