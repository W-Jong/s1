package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.api.dto.pdt.PdtInfoDto;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.viewholder.BrandFollowViewHolder;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.List;

import static com.kyad.selluv.brand.BrandMainFragment.brandMainFragment;

public class BrandFollowAdapter extends RecyclerView.Adapter {

    public List<BrandListDto> dataList = new ArrayList<>();
    private BaseActivity activity;

    public BrandFollowAdapter(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.item_brand_follow, null);
        BrandFollowViewHolder holder = new BrandFollowViewHolder(itemView);
        holder.ib_more = (ImageButton) itemView.findViewById(R.id.ib_more);
        holder.tv_brand_name = (TextView) itemView.findViewById(R.id.tv_brand_name);
        holder.ll_ware_list = (LinearLayout) itemView.findViewById(R.id.ll_ware_list);
        holder.rl_none = (RelativeLayout) itemView.findViewById(R.id.rl_none);
        itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        BrandFollowViewHolder vh = (BrandFollowViewHolder) viewHolder;
        final BrandListDto item = dataList.get(position);
        vh.tv_brand_name.setText(item.getNameEn());
        vh.tv_brand_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brandMainFragment.gotoBrandPageFragment(item.getBrandUid());
            }
        });
        vh.ib_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brandMainFragment.gotoBrandPageFragment(item.getBrandUid());
            }
        });
        if (item.getPdtList() != null && item.getPdtList().size() > 0) {
            vh.rl_none.setVisibility(View.GONE);
            for (int i = 0; i < item.getPdtList().size(); i++) {
                final PdtInfoDto ware = item.getPdtList().get(i);
                View wareView = LayoutInflater.from(activity).inflate(R.layout.item_brand_follow_ware, null);
                ImageUtils.load(activity, ware.getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, ((ImageView) wareView.findViewById(R.id.iv_ware)));
                ((ImageView) wareView.findViewById(R.id.iv_ware)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toDetail(v, ware.getPdtUid());
                    }
                });
                ((TextView) wareView.findViewById(R.id.tv_ware_price)).setText(Utils.getAttachCommaFormat((int) ware.getPrice()));
                ((TextView) wareView.findViewById(R.id.tv_ware_name)).setText(ware.getBrandName() + " " + ware.getCategoryName());
                if (i == 0) {
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.setMarginStart(Utils.dpToPixel(activity, 15));
                    wareView.setLayoutParams(lp);
                }
                vh.ll_ware_list.addView(wareView);
            }
        } else {
            vh.rl_none.setVisibility(View.VISIBLE);
        }
    }

    public void toDetail(View view, int pdtUid) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, pdtUid);
        activity.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
