package com.kyad.selluv;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.kyad.selluv.login.LoginSuggestionDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog loadingDlg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }


    public void setStatusBarWhite() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void startActivity(Class cls, boolean finish, int enterAnim, int exitAnim) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
        if (finish) finish();
    }

    public void finish(int enterAnim, int exitAnim) {
        super.finish();
    }

    //show Web Page
    protected void toWeb(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    public void showLoading() {
        if (loadingDlg == null) {
            loadingDlg = new ProgressDialog(this, R.style.LoadingDialogTheme);
            loadingDlg.setTitle(null);
            loadingDlg.setIndeterminate(true);
            loadingDlg.setCancelable(false);

            Drawable drawable = new ProgressBar(this).getIndeterminateDrawable().mutate();
            drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent),
                    PorterDuff.Mode.SRC_IN);
            loadingDlg.setIndeterminateDrawable(drawable);
            loadingDlg.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        }
        loadingDlg.show();
    }

    public void closeLoading() {
        if (loadingDlg != null) {
            loadingDlg.dismiss();
        }
    }

    /**
     * //kkk로그인이 필요할때 띄우는 함수
     */
    public void showLoginSuggestionPopup() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        LoginSuggestionDialog loginSuggestionFragment = new LoginSuggestionDialog();
        loginSuggestionFragment.show(ft, "LoginSuggestion");


    }

    public static List<Context> forSellActivites = new ArrayList<>();

    public void addForSellActivity() {
        forSellActivites.add(this);
    }

    public void removeForSellActivity() {
        forSellActivites.remove(this);
    }

    /**
     * 모든 액티비티를 죽인다.
     */
    public static void finishForSellActivities() {
        for (Context con : BaseActivity.forSellActivites) {
            try {
                ((BaseActivity) con).finish();
            } catch (Exception e) {

            }
        }
    }
}