package com.kyad.selluv.sell.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.ResponseMeta;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.sell.SellBrandAddActivity;
import com.kyad.selluv.sell.SellMainActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static com.kyad.selluv.common.Constants.ACT_ERROR_ACCESS_TOKEN;
import static com.kyad.selluv.common.Constants.ACT_NOT_PERMISSION;


public class SellCategorySelectFragment extends BaseFragment {

    //UI Refs
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_title)
    ImageView iv_title;
    @BindView(R.id.popup_category)
    LinearLayout popup_category;
    @BindView(R.id.ib_back)
    ImageButton ib_back;
    @BindView(R.id.ll_menu)
    LinearLayout ll_menu;
    @BindView(R.id.tv_menu_item)
    TextView tv_menu_item;

    //Vars
    public List<CategoryDao> subCategories;

    View rootView;

    public static SellCategorySelectFragment newInstance() {
        SellCategorySelectFragment fragment = new SellCategorySelectFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_select, container, false);
        ButterKnife.bind(this, rootView);

        if (Globals.categoryDepth == 1) {
            reloadSubCategory2();
        } else {
            reloadSubCategory3();
        }

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, false, nextAnim);
    }


    @OnClick(R.id.btn_cancel)
    void onCancel(View v) {
        onBack(v);
    }

    @OnClick(R.id.ib_back)
    public void onBack(View v) {
        Globals.categoryDepth--;
        if (Globals.categoryDepth < 1) {
            if (getActivity().getClass().equals(SellMainActivity.class)) {
                ((SellMainActivity) getActivity()).closeSellCategory();
            } else {

            }
        } else {
            reloadSubCategory2();
        }
    }

    private void reloadSubCategory2() {
        subCategories = Globals.subCategories2;
        refreshLayout();
    }

    private void reloadSubCategory3() {
        subCategories = Globals.subCategories3;
        refreshLayout();
    }

    private void refreshLayout() {
        popup_category.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        if (ll_menu.getChildCount() > 1) {
            //0번째는 전체보기
            int childCount = ll_menu.getChildCount();
            for (int i = childCount - 1; i > 0; i--) {
                ll_menu.removeViewAt(i);
            }
        }

        if (Globals.categoryDepth == 1) {
            tv_title.setText(Globals.subCategories1.get(Globals.categoryIndex[0] - 1).getCategoryName());
        } else {
            tv_title.setText(Globals.subCategories2.get(Globals.categoryIndex[1] - 1).getCategoryName());
        }
        iv_title.setImageResource(Globals.getCategoryIcon());

        if (Globals.currentShopperTypeIdx == 1)
            popup_category.setBackgroundColor(getResources().getColor(R.color.color_ff3b7e_92));
        else if (Globals.currentShopperTypeIdx == 2)
            popup_category.setBackgroundColor(getResources().getColor(R.color.color_3a0d7d_92));

        ib_back.setVisibility(View.GONE);

        for (int i = 0; i < subCategories.size(); i++) {
            TextView tv = new TextView(getActivity());
            TextView template = tv_menu_item;
            tv.setText(subCategories.get(i).getCategoryName());
            tv.setLayoutParams(template.getLayoutParams());
            tv.setTextColor(template.getTextColors());
            tv.setTextSize(COMPLEX_UNIT_DIP, Utils.pixelToDp(getActivity(), (int) template.getTextSize()));
            tv.setTypeface(template.getTypeface(), template.getTypeface().getStyle());
            tv.setClickable(template.isClickable());
            final int category_index = i;
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO send Server shopper type, category type and get Category List according to depth
                    //The Following process is server action emulation for UI Test
                    if (Globals.categoryDepth == 1) {
                        Globals.categoryIndex[Globals.categoryDepth] = category_index + 1;
                        getSubCategory3();
                    } else {
                        Globals.categoryIndex[Globals.categoryDepth] = category_index + 1;
                        Globals.pdtCreateReq.setCategoryUid(Globals.getSelectedCategory().getCategoryUid());
                        Globals.pdtCreateReq.setCategoryName(Globals.getSelectedCategory().getCategoryName());
                        Intent intent = new Intent(getActivity(), SellBrandAddActivity.class);
                        startActivity(intent);
                    }
                }
            });
            ll_menu.addView(tv);
        }
    }

    private void getSubCategory3() {
        int idx = Globals.categoryIndex[Globals.categoryDepth] - 1;

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<CategoryDao>>> genRes = restAPI.getSubCategories(Globals.userToken, Globals.subCategories2.get(idx).getCategoryUid());
        genRes.enqueue(new Callback<GenericResponse<List<CategoryDao>>>() {

            @Override
            public void onResponse(Call<GenericResponse<List<CategoryDao>>> call, Response<GenericResponse<List<CategoryDao>>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    List<CategoryDao> categories = response.body().getData();
                    if (categories == null || categories.size() < 1) {
                        Globals.pdtCreateReq.setCategoryUid(Globals.subCategories2.get(idx).getCategoryUid());
                        Globals.pdtCreateReq.setCategoryName(Globals.subCategories2.get(idx).getCategoryName());
                        Intent intent = new Intent(getActivity(), SellBrandAddActivity.class);
                        startActivity(intent);
                    } else {
                        Globals.categoryDepth = 2;
                        Globals.subCategories3 = categories;
                        reloadSubCategory3();
                    }
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_ERROR_ACCESS_TOKEN.getErrCode()) {
                    //token이 만료됬을때.. 혹은 다른 기기에서 로그인
                    getActivity().sendBroadcast(new Intent(ACT_ERROR_ACCESS_TOKEN));
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_TEMP_NOT_PERMISSION.getErrCode()) {
                    //임시회원인경우..
                    getActivity().sendBroadcast(new Intent(ACT_NOT_PERMISSION));
                } else {//실패
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<List<CategoryDao>>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}
