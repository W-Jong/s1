package com.kyad.selluv.common;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * an animation for resizing the view.
 */
public class DockAnimation extends Animation {

    private View m_view;
    public View m_observe;
    private int m_base;

    private float m_fromX, m_toX;
    private float m_fromY, m_toY;
    private float m_fromW, m_toW;
    private float m_fromH, m_toH;

    public static final int DIR_NONE = 0x00;
    public static final int DIR_HORIZENTAL = 0x01;
    public static final int DIR_VERTICAL = 0x02;
    private int m_dir = DIR_NONE;

    public static final int ANI_NONE = 0x00;
    public static final int ANI_MOVE = 0x01;
    public static final int ANI_ALPHA = 0x02;
    public static final int ANI_SCALE = 0x04;
    public static final int ANI_ROTATE = 0x08;
    private int m_type = ANI_NONE;

    public static final int FRAME_LAYOUT = 0x00;
    public static final int LINEAR_LAYOUT = 0x01;
    private int m_layout_type = FRAME_LAYOUT;

    /**
     * constructor
     *
     * @param v
     * @param time ms단위의 애니메이션 시간
     */
    public DockAnimation(View v, long time, int layout_type) {

        if (v == null) {
            return;
        }
        m_view = v;
        m_view.clearAnimation();
        m_view.invalidate(); // 요거 않해주면 애니메이션 먹통 된다.

        start();
        setDuration(time);
    }

    @Override
    public void start() {
        super.start();
        m_view.startAnimation(this);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {

        switch (m_type) {

            case ANI_MOVE:
                move(m_dir, interpolatedTime);
                moveObserv();
                break;

            case ANI_ALPHA:
                alpha();
                break;

            case ANI_SCALE:
                size(m_dir, interpolatedTime);
                // scale();
                break;

            case ANI_ROTATE:
                rotate();
                break;
        }
    }

    /****************************************************************************************
     *
     */
    public void setView(View v) {
        m_view = v;
    }

    public void setAniType(int p_dir) {
        m_type = p_dir;
    }

    public int getAniType() {
        return m_type;
    }

    public void setDirection(int p_dir) {
        m_dir = p_dir;
    }

    public int getDirection() {
        return m_dir;
    }

    public void setX(int p_from, int p_to) {

        m_fromX = p_from;
        m_toX = p_to;
    }

    public void setY(int p_from, int p_to) {

        m_fromY = p_from;
        m_toY = p_to;
    }

    public void setWidth(int p_from, int p_to) {

        m_fromW = p_from;
        m_toW = p_to;
    }

    public void setHeight(int p_from, int p_to) {

        m_fromH = p_from;
        m_toH = p_to;
    }

    public void setAniDuration(long time) {
        setDuration(time);
    }

    /**
     * 애니메이션과 함께 움직이는 뷰
     */
    public void setObserver(View p_v, int p_base) {

        m_observe = p_v;
        m_base = p_base;
    }

    /****************************************************************************************
     *
     */
    //
    private void move(int p_direct, float p_time) {

        LinearLayout.LayoutParams lParams = null;
        RelativeLayout.LayoutParams fParams = null;
        if (m_layout_type == FRAME_LAYOUT) {
            fParams = (RelativeLayout.LayoutParams) m_view.getLayoutParams();
        } else if (m_layout_type == LINEAR_LAYOUT) {
            lParams = (LinearLayout.LayoutParams) m_view.getLayoutParams();
        } else {
            return;
        }

        int left, top;

        if ((p_direct & 0x01) == 0x01) {
            left = (int) (m_fromX + (m_toX - m_fromX) * p_time);
            if (m_layout_type == LINEAR_LAYOUT) {
                lParams.leftMargin = left;
            } else { // frame-layout
                fParams.leftMargin = left;
            }
        }

        if ((p_direct & 0x02) == 0x02) {
            top = (int) (m_fromY + (m_toY - m_fromY) * p_time);
            if (m_layout_type == LINEAR_LAYOUT) {
                lParams.topMargin = top;
            } else { // frame-layout
                fParams.topMargin = top;
            }
        }

        m_view.requestLayout();
    }

    private void moveObserv() {

        if (m_observe == null) {
            return;
        }

        int xShow = 0;
        LinearLayout.LayoutParams lParams = null;
        FrameLayout.LayoutParams fParams = null;
        if (m_layout_type == FRAME_LAYOUT) {
            fParams = (FrameLayout.LayoutParams) m_view.getLayoutParams();
            xShow = fParams.leftMargin;
        } else if (m_layout_type == LINEAR_LAYOUT) {
            lParams = (LinearLayout.LayoutParams) m_view.getLayoutParams();
            xShow = lParams.leftMargin;
        } else {
            return;
        }

        int xHide = 0;
        if (xShow < -m_base) {
            xHide = -xShow - m_base;
        }

        RelativeLayout.LayoutParams lParams1 = (RelativeLayout.LayoutParams) m_observe
                .getLayoutParams();
        lParams1.leftMargin = xHide + m_base / 2;
        m_observe.requestLayout();
    }

    //
    private void size(int p_direct, float p_time) {

        LayoutParams lParams = m_view.getLayoutParams();

        if ((p_direct & 0x01) == 0x01) {
            lParams.width = (int) (m_fromW + (m_toW - m_fromW) * p_time);
        }

        if ((p_direct & 0x02) == 0x02) {
            lParams.height = (int) (m_fromH + (m_toH - m_fromH) * p_time);
        }

        m_view.requestLayout();
    }

    //
    // //
    // private void scale() {
    //
    // }

    //
    private void alpha() {

    }

    //
    private void rotate() {

    }

}