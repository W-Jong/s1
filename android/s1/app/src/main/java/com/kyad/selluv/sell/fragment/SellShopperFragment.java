/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.ChangeBounds;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.ResponseMeta;
import com.kyad.selluv.api.dto.category.CategoryDao;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.sell.SellBrandAddActivity;
import com.kyad.selluv.sell.SellMainActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Constants.ACT_ERROR_ACCESS_TOKEN;
import static com.kyad.selluv.common.Constants.ACT_NOT_PERMISSION;


public class SellShopperFragment extends BaseFragment {

    @BindView(R.id.scene_root)
    ViewGroup sceneRoot;

    public int shopperTypeIdx = 0; //0 : man 1 : woman 2 : kids
    public int category1TypeIdx = 0;

    private Scene[] sceneInit = new Scene[3];
    private Scene[] sceneThrough = new Scene[3];
    private Scene[] sceneFinal = new Scene[3];

    boolean selected = false;
    boolean inited = false;
    Transition transitionThrough = new ChangeBounds();
    Transition transitionFinal = new ChangeBounds();
    Transition transitionInit = new ChangeBounds();
    Transition.TransitionListener listenerThrough;
    Transition.TransitionListener listenerFinal;
    Transition.TransitionListener listenerInit;

    View rootView;


    public static SellShopperFragment newInstance() {
        SellShopperFragment fragment = new SellShopperFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sell_shopper, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, false, nextAnim);
    }

    @OnClick(R.id.btn_close)
    void onClose(View v) {
        getActivity().onBackPressed();
    }

    private void loadLayout() {
        setupLayout();
    }


    private void setupLayout() {

        sceneInit[0] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_shopper_select_male, getActivity());
        sceneInit[1] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_shopper_select_female, getActivity());
        sceneInit[2] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_shopper_select_kids, getActivity());
        sceneThrough[0] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_male_category, getActivity());
        sceneThrough[1] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_female_category, getActivity());
        sceneThrough[2] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_kids_category, getActivity());
        sceneFinal[0] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_male_category_open, getActivity());
        sceneFinal[1] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_female_category_open, getActivity());
        sceneFinal[2] = Scene.getSceneForLayout(sceneRoot, R.layout.fragment_sell_kids_category_open, getActivity());

        listenerThrough = new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                if (selected) {
                    TransitionManager.go(sceneInit[shopperTypeIdx], transitionFinal);
                } else {
                    TransitionManager.go(sceneFinal[shopperTypeIdx], transitionFinal);
                }
                selected = !selected;
            }

            @Override
            public void onTransitionCancel(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionPause(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionResume(@NonNull Transition transition) {
            }
        };

        listenerFinal = new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                setListener();
            }

            @Override
            public void onTransitionCancel(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionPause(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionResume(@NonNull Transition transition) {
            }
        };

        listenerInit = new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                if (!inited) {
                    TransitionManager.go(sceneInit[0], transitionInit);
                    inited = true;
                } else setListener();
            }

            @Override
            public void onTransitionCancel(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionPause(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionResume(@NonNull Transition transition) {
            }
        };

        transitionThrough.addListener(listenerThrough);
        transitionFinal.addListener(listenerFinal);
        transitionInit.addListener(listenerInit);

        TransitionManager.go(sceneThrough[0], transitionInit);
        transitionInit.setStartDelay(400);
    }


    void gotoCategory2() {
        if (category1TypeIdx < 1) {
            Toast.makeText(getActivity(), "Please check source", Toast.LENGTH_LONG).show();
            return;
        }
        Globals.currentShopperTypeIdx = shopperTypeIdx;
        Globals.categoryIndex[0] = category1TypeIdx;
        Globals.categoryDepth = 1;
        Globals.subCategories1 = Globals.getSubCategoryDepth_1();


        getSubCategories();
    }

    void setListener() {

        /**남자 선택 */
        rootView.findViewById(R.id.ib_man).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopperTypeIdx = 0;
                Globals.pdtCreateReq.setPdtGroup(PDT_GROUP_TYPE.MALE);
                TransitionManager.go(sceneThrough[shopperTypeIdx], transitionThrough);
            }
        });
        /**여자 선택 */
        rootView.findViewById(R.id.ib_woman).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopperTypeIdx = 1;
                Globals.pdtCreateReq.setPdtGroup(PDT_GROUP_TYPE.FEMALE);
                TransitionManager.go(sceneThrough[shopperTypeIdx], transitionThrough);
            }
        });
        /**어린이 선택 */
        rootView.findViewById(R.id.ib_kids).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopperTypeIdx = 2;
                Globals.pdtCreateReq.setPdtGroup(PDT_GROUP_TYPE.KIDS);
                TransitionManager.go(sceneThrough[shopperTypeIdx], transitionThrough);
            }
        });

        rootView.findViewById(R.id.btn_bag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shopperTypeIdx == 2) {
                    category1TypeIdx = 1;
                } else {
                    category1TypeIdx = 2;
                }
                gotoCategory2();
            }
        });
        rootView.findViewById(R.id.btn_clothing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shopperTypeIdx == 2) {
                    category1TypeIdx = 2;
                } else {
                    category1TypeIdx = 1;
                }
                gotoCategory2();
            }
        });
        rootView.findViewById(R.id.btn_shoes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category1TypeIdx = 3;
                gotoCategory2();
            }
        });
        rootView.findViewById(R.id.btn_fashion).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category1TypeIdx = 4;
                gotoCategory2();
            }
        });
        rootView.findViewById(R.id.btn_watch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category1TypeIdx = 5;
                gotoCategory2();
            }
        });
    }

    private void getSubCategories() {
        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<CategoryDao>>> genRes = restAPI.getSubCategories(Globals.userToken, Globals.subCategories1.get(category1TypeIdx - 1).getCategoryUid());
        genRes.enqueue(new Callback<GenericResponse<List<CategoryDao>>>() {

            @Override
            public void onResponse(Call<GenericResponse<List<CategoryDao>>> call, Response<GenericResponse<List<CategoryDao>>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    List<CategoryDao> categories = response.body().getData();
                    if (categories == null || categories.size() < 1) {
                        Globals.pdtCreateReq.setCategoryUid(Globals.subCategories1.get(category1TypeIdx - 1).getCategoryUid());
                        Globals.pdtCreateReq.setCategoryName(Globals.subCategories1.get(category1TypeIdx - 1).getCategoryName());
                        Intent intent = new Intent(getActivity(), SellBrandAddActivity.class);
                        startActivity(intent);
                    } else {
                        Globals.subCategories2 = categories;
                        ((SellMainActivity) getActivity()).goSellCategorySelect();
                    }

                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_ERROR_ACCESS_TOKEN.getErrCode()) {
                    //token이 만료됬을때.. 혹은 다른 기기에서 로그인
                    getActivity().sendBroadcast(new Intent(ACT_ERROR_ACCESS_TOKEN));
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_TEMP_NOT_PERMISSION.getErrCode()) {
                    //임시회원인경우..
                    getActivity().sendBroadcast(new Intent(ACT_NOT_PERMISSION));
                } else {//실패
                    Toast.makeText(getActivity(), response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<List<CategoryDao>>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
