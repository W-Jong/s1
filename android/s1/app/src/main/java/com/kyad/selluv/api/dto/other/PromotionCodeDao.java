package com.kyad.selluv.api.dto.other;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class PromotionCodeDao {
    //("프로모션코드UID")
    private int promotionCodeUid;
    //("코드분류, 1-구매용 2-판매용")
    private int kind;
    //("프로모션 코드")
    private String title;
    //("혜택가격")
    private long price;
    //("혜택단위 1-퍼센트단위 2-원단위")
    private int priceUnit;
    //("혜택메모")
    private String memo;
    //("프로모션적용 시작날짜")
    private Timestamp startTime;
    //("프로모션적용 끝날짜")
    private Timestamp endTime;
}