package com.kyad.selluv.camera;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.sell.SellInfoActivity;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Flash;
import com.otaliastudios.cameraview.Size;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


/**
 * This demo app saves the taken picture to a constant file.
 * $ adb pull /sdcard/Android/data/com.google.android.cameraview.demo/files/Pictures/picture.jpg
 */
public class SellPicActivity extends BaseActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback {

    public static int thumbSize = 0;
    //UI Refs
    @BindView(R.id.ib_flash)
    ImageButton ib_flash;
    @BindView(R.id.ib_right)
    ImageButton ib_right;
    @BindView(R.id.ib_guide)
    ImageButton ib_guide;
    @BindView(R.id.tv_upload)
    TextView tv_upload;
    @BindView(R.id.hl_image)
    HorizontalListView hl_image;
    //Vars

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";
    //    private static final int[] FLASH_OPTIONS = {
//            CameraView.FLASH_OFF,
//            CameraView.FLASH_TORCH
//    };
    private static final int[] FLASH_ICONS = {
            R.drawable.sell_pic_top_ic01,
            R.drawable.sell_pic_top_ic01_over
    };

    private boolean mCapturingPicture;
    private int mCurrentFlash;
    private CameraView mCameraView;
    private Handler mBackgroundHandler;
    //public boolean fromMyPage = false;

    /**
     * 상품등록페이지에서 호출
     */
    public static final int FROM_PAGE_SELL = 100;
    /**
     * 상품상세페이지에서 호출
     */
    public static final int FROM_PAGE_PDT_DETAIL = 200;
    /**
     * 상품편집페이지에서 호출
     */
    public static final int FROM_PAGE_PDT_EDIT = 300;
    /**
     * 나의페이지에서 호출
     */
    public static final int FROM_PAGE_MYPAGE = 400;
    /**
     * 이전에 어느 페이지에서 왔는지 체크
     */
    public int fromPage = FROM_PAGE_MYPAGE;

    public ArrayList<String> imageFiles = new ArrayList();
    HorizontalImageListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sell_camera_pic);
        addForSellActivity();

        mCameraView = findViewById(R.id.camera);
        if (mCameraView != null) {
            mCameraView.addCameraListener(new CameraListener() {
                public void onCameraOpened(CameraOptions options) {
                }

                public void onPictureTaken(byte[] jpeg) {
                    onPicture(jpeg);
                }

                @Override
                public void onVideoTaken(File video) {
                    super.onVideoTaken(video);
                }
            });
        }

        fromPage = getIntent().getIntExtra(Constants.FROM_PAGE, FROM_PAGE_MYPAGE);
        String[] files = getIntent().getStringArrayExtra(Constants.IMAGES);
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                imageFiles.add(files[i]);
            }
        }
        loadLayout();
    }

    void onPicture(byte[] data) {
        mCapturingPicture = false;
        long callbackTime = System.currentTimeMillis();

        // This can happen if picture was taken with a gesture.
        if (mCaptureTime == 0) mCaptureTime = callbackTime - 300;
        if (mCaptureNativeSize == null) mCaptureNativeSize = mCameraView.getPictureSize();

        Date date = new Date();
        final String filePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/pic" + date.getTime() + ".jpg";

        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

        File file = new File(filePath);
        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            os.write(data);

            os.flush();
            os.close();

            ExifInterface exif = null;

            try {
                exif = new ExifInterface(file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int exifOrientation;
            int exifDegree;

            if (exif != null) {
                exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                exifDegree = exifOrientationToDegrees(exifOrientation);
            } else {
                exifDegree = 0;
            }

            Bitmap photo = rotate(bitmap, exifDegree);

            os = new FileOutputStream(file);

            photo.compress(Bitmap.CompressFormat.JPEG, 60, os); // saving the Bitmap to a file compressed as a JPEG with 70% compression rate

            os.flush();
            os.close();

            imageFiles.add(filePath);
            adapter.setData(imageFiles);
            hl_image.setDragableList(imageFiles);
            adapter.notifyDataSetChanged();
            if (fromPage != FROM_PAGE_SELL) {
                tv_upload.setVisibility(View.VISIBLE);
            } else {
                ib_right.setVisibility(View.VISIBLE);
            }
        } catch (IOException e) {
            Log.w("SellPicActivity", "Cannot write to " + file, e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // Ignore
                }
            }

            mCaptureTime = 0;
            mCaptureNativeSize = null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            if (!mCameraView.isStarted()) {
                mCameraView.start();
            }
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    protected void onPause() {
        mCameraView.stop();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();

        if (mBackgroundHandler != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mBackgroundHandler.getLooper().quitSafely();
            } else {
                mBackgroundHandler.getLooper().quit();
            }
            mBackgroundHandler = null;
        }

        mCameraView.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
                    throw new RuntimeException("Error on requesting camera permission.");
                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, R.string.camera_permission_not_granted,
                            Toast.LENGTH_SHORT).show();
                }
                // No need to start camera here; it is handled by onResume
                break;
        }
    }

    /**
     * < 뒤로
     *
     * @param v
     */
    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * > 다음
     *
     * @param v
     */
    @OnClick(R.id.ib_right)
    void onClickRight(View v) {
        if (imageFiles.size() > 0) {
            uploadFiles();
        }
    }

    /**
     * 업로드
     *
     * @param v
     */
    @OnClick(R.id.tv_upload)
    void onUpload(View v) {

        if (imageFiles.size() > 0) {
            uploadFiles();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

    }

    /**
     * 사진찍기
     *
     * @param v
     */
    private long mCaptureTime;
    private Size mCaptureNativeSize;

    @OnClick(R.id.take_picture)
    void takePicture(View v) {
        if (mCapturingPicture) return;
        mCapturingPicture = true;
        mCaptureTime = System.currentTimeMillis();
        mCaptureNativeSize = mCameraView.getPictureSize();
        mCameraView.capturePicture();
    }

    /**
     * 카메라 앞/뒤 절환
     *
     * @param v
     */
    @OnClick(R.id.ib_other)
    void camOther(View v) {
        if (mCameraView != null) {
            if (mCapturingPicture) return;
            mCameraView.toggleFacing();
        }
    }

    /**
     * 플래시
     *
     * @param v
     */
    private Flash flash = Flash.OFF;

    @OnClick(R.id.ib_flash)
    void onFlash(View v) {
        if (mCameraView != null) {
            if (flash == Flash.OFF) {
                flash = Flash.ON;
                ib_flash.setImageDrawable(getDrawable(FLASH_ICONS[1]));
            } else {
                flash = Flash.OFF;
                ib_flash.setImageDrawable(getDrawable(FLASH_ICONS[0]));
            }
            mCameraView.setFlash(flash);
        }
    }

    /**
     * 배경
     *
     * @param v
     */
    @OnClick(R.id.root)
    void onClickScreen(View v) {
        hl_image.isEditable = false;
        adapter.isEditable = false;
        adapter.notifyDataSetChanged();
    }

    /**
     * 갤러리
     *
     * @param v
     */
    @OnClick(R.id.ib_gallery)
    void onGallery(View v) {
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.fromPage = SellPicActivity.FROM_PAGE_SELL;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        galleryFragment.show(ft, "GalleryFragment");
    }

    /**
     * 가이드
     *
     * @param v
     */
    @OnClick(R.id.ib_guide)
    void onGuide(View v) {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.sell_guide);
        startActivity(intent);
    }

    void loadLayout() {
        if (fromPage == FROM_PAGE_PDT_DETAIL) {
            ib_guide.setVisibility(View.INVISIBLE);
        } else {
            ib_guide.setVisibility(View.VISIBLE);
        }

        if (fromPage == FROM_PAGE_PDT_EDIT) {
            tv_upload.setVisibility(View.VISIBLE);
            tv_upload.setText("완료");
        }

        thumbSize = Utils.dpToPixel(this, 65);
        adapter = new HorizontalImageListAdapter(this, hl_image);
        adapter.setData(imageFiles);
        hl_image.setDragableList(imageFiles);
        hl_image.setAdapter(adapter);

        hl_image.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.isEditable = true;
                hl_image.isEditable = true;
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    public void reloadThumbnails() {

        adapter.notifyDataSetChanged();
    }

    public void reloadThumbnails(ArrayList<String> files) {
//        imageFiles.clear();
        for (String file : files) {
            imageFiles.add(file);
        }
        adapter.setData(imageFiles);
        hl_image.setDragableList(imageFiles);
        adapter.notifyDataSetChanged();
        if (files.size() > 0) {
            if (fromPage != FROM_PAGE_SELL) {
                tv_upload.setVisibility(View.VISIBLE);
            } else {
                ib_right.setVisibility(View.VISIBLE);
            }
        } else {
            ib_right.setVisibility(View.INVISIBLE);
            tv_upload.setVisibility(View.INVISIBLE);
        }
    }

    private Bitmap rotate(Bitmap bitmap, float degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static ConfirmationDialogFragment newInstance(@StringRes int message,
                                                             String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

    }

    private void uploadFiles() {
        showLoading();

        MultipartBody.Part[] multipartBody = new MultipartBody.Part[imageFiles.size()];
        for (int i = 0; i < imageFiles.size(); i++) {
            File file = new File(imageFiles.get(i));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("uploadfiles", file.getName(), surveyBody);
            multipartBody[i] = part;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<HashMap<String, String>>>> genRes = restAPI.uploadImages(multipartBody);
        genRes.enqueue(new TokenCallback<List<HashMap<String, String>>>(this) {
            @Override
            public void onSuccess(List<HashMap<String, String>> response) {
                closeLoading();
                //fileName, fileURL
                ArrayList<String> imag_names = new ArrayList<>();
                for (int i = 0; i < response.size(); i++) {
                    imag_names.add(response.get(i).get("fileName"));
                }

                if (fromPage == FROM_PAGE_SELL) {
                    Globals.pdtCreateReq.setPhotos(imag_names);
                    Globals.pdtCreateReq.setPhotoPaths(imageFiles);
                    Intent intent = new Intent(SellPicActivity.this, SellInfoActivity.class);
                    intent.putExtra(Constants.THUMB_IMAGE_FILE, imageFiles.get(0));
                    startActivity(intent);
//                    finish();

                } else {
                    if (Globals.isEditing) {
                        Globals.pdtUpdateReq.setPhotos(imag_names);
                    }

                    Intent result = new Intent();
                    result.putExtra(Constants.IMAGES, imageFiles.toArray(new String[]{}));
                    result.putExtra(Constants.IMAGES_NAMES, imag_names.toArray(new String[]{}));
                    setResult(RESULT_OK, result);
                    finish();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
