package com.kyad.selluv.api.enumtype;

public enum REPORT_TYPE {
    AD_SPAM(1), SLEEP_SELLER(2), DIRECT_DEAL(3), OTHER(4), CANCEL(5);

    private int code;

    private REPORT_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
