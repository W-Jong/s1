package com.kyad.selluv.api.dto.other;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class UsrAlarmDto {
    private int usrAlarmUid;
    //("추가된 시간")
    private Timestamp regTime;
    //("회원 UID")
    private int usrUid;
    //("알람분류 1-거래, 2-소식 3-댓글")
    private int kind;
    //("부분분류, 앱기획서 208~210페이지 알림리스트유형에 해당")
    private int subKind;
    //("타겟 UID, subKind에 따라 결정됨")
    private int targetUid;
    //("알림내용")
    private String content;
    //("연관된 상품UID")
    private int pdtUid;
    //("연관된 상품이미지")
    private String profileImg;
    //("연관된 유저 프로필이미지 ")
    private String usrProfileImg;
}
