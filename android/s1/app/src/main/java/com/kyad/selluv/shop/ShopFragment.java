package com.kyad.selluv.shop;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.top.TopLikeActivity;
import com.kyad.selluv.top.TopSearchActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.kyad.selluv.common.Constants.TAB_BAR_HEIGHT;

public class ShopFragment extends BaseFragment {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.ib_right)
    ImageButton ib_right;

    //Vars
    View rootView;

    ShopStyleFragment shopStyleFragment;
    ShopShopperFragment shopMaleFragment, shopFemaleFragment, shopKidsFragment;
    private int selectedTab = 0;    //  0: 스타일, 1: 남성, 2: 여성, 3: 키즈
    private PageAdapter pagerAdapter;
    Fragment mainFragment;

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        startActivity(TopSearchActivity.class, false, 0, 0);
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    public static ShopFragment newInstance(Fragment mainFragment, int defaultTab) {
        ShopFragment fragment = new ShopFragment();
        fragment.mainFragment = mainFragment;
        fragment.selectedTab = defaultTab;
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop, container, false);
        ButterKnife.bind(this, rootView);
        initFragment();
        loadLayout();
        return rootView;
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (shopStyleFragment != null) shopStyleFragment.setUserVisibleHint(isVisibleToUser);
        if (shopMaleFragment != null) shopMaleFragment.setUserVisibleHint(isVisibleToUser);
        if (shopFemaleFragment != null) shopFemaleFragment.setUserVisibleHint(isVisibleToUser);
        if (shopKidsFragment != null) shopKidsFragment.setUserVisibleHint(isVisibleToUser);
    }

    private void loadLayout() {

        BarUtils.addMarginTopEqualStatusBarHeight(rootView.findViewById(R.id.appbar));
        rootView.findViewById(R.id.appbar).setAlpha(0.95f);

//        selectedTab = 0;
        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pagerAdapter = new PageAdapter(getChildFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
                refreshFragment();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

        });
        vp_main.setOffscreenPageLimit(4);

        // 수정작업중
//        tl_top_tab.getTabAt(selectedTab).select();
        setSelectedTabStyle(selectedTab);
        vp_main.setCurrentItem(selectedTab);

        updateLikePdtBadge();
    }

    private void initFragment() {
        if (shopStyleFragment == null) {
            shopStyleFragment = new ShopStyleFragment();
            shopStyleFragment.frag_ware_list.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
            shopStyleFragment.frag_ware_list.setNonePage(R.drawable.main_none_ic_normal,
                    getString(R.string.none_style_img),
                    getString(R.string.none_style_img_info),
                    null, null);
            shopStyleFragment.frag_ware_list.isLazyLoading = selectedTab != 0;
        } else {
            shopStyleFragment.frag_ware_list.isLazyLoading = selectedTab != 0;
        }

        if (shopMaleFragment == null) {
            shopMaleFragment = ShopShopperFragment.newInstance(mainFragment);
            shopMaleFragment.shopperType = 0;
            shopMaleFragment.frag_ware_list.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
            shopMaleFragment.frag_ware_list.setNonePage(R.drawable.main_none_ic_normal,
                    getString(R.string.none_style_img),
                    getString(R.string.none_style_img_info),
                    null, null);
            shopMaleFragment.frag_ware_list.isLazyLoading = selectedTab != 1;
        } else {
            shopMaleFragment.frag_ware_list.isLazyLoading = selectedTab != 1;
        }

        if (shopFemaleFragment == null) {
            shopFemaleFragment = ShopShopperFragment.newInstance(mainFragment);
            shopFemaleFragment.shopperType = 1;
            shopFemaleFragment.frag_ware_list.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
            shopFemaleFragment.frag_ware_list.setNonePage(R.drawable.main_none_ic_normal,
                    getString(R.string.none_style_img),
                    getString(R.string.none_style_img_info),
                    null, null);
            shopFemaleFragment.frag_ware_list.isLazyLoading = selectedTab != 2;
        } else {
            shopFemaleFragment.frag_ware_list.isLazyLoading = selectedTab != 2;
        }

        if (shopKidsFragment == null) {
            shopKidsFragment = ShopShopperFragment.newInstance(mainFragment);
            shopKidsFragment.shopperType = 2;
            shopKidsFragment.frag_ware_list.paddintTop = Utils.dpToPixel(getActivity(), TAB_BAR_HEIGHT) + BarUtils.getStatusBarHeight();
            shopKidsFragment.frag_ware_list.setNonePage(R.drawable.main_none_ic_normal,
                    getString(R.string.none_style_img),
                    getString(R.string.none_style_img_info),
                    null, null);
            shopKidsFragment.frag_ware_list.isLazyLoading = selectedTab != 3;
        } else {
            shopKidsFragment.frag_ware_list.isLazyLoading = selectedTab != 3;
        }
    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(getActivity()) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 3, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private void refreshFragment() {
        switch (selectedTab) {
            case 0:
                shopStyleFragment.requestBannerList();
                shopStyleFragment.frag_ware_list.removeAll();
                shopStyleFragment.frag_ware_list.refresh();
                break;
            case 1:
                shopMaleFragment.requestBrandList();
                shopMaleFragment.requestThemeList();
                shopMaleFragment.frag_ware_list.removeAll();
                shopMaleFragment.frag_ware_list.refresh();
                break;
            case 2:
                shopFemaleFragment.requestBrandList();
                shopFemaleFragment.requestThemeList();
                shopFemaleFragment.frag_ware_list.removeAll();
                shopFemaleFragment.frag_ware_list.refresh();
                break;
            case 3:
                shopKidsFragment.requestBrandList();
                shopKidsFragment.requestThemeList();
                shopKidsFragment.frag_ware_list.removeAll();
                shopKidsFragment.frag_ware_list.refresh();
                break;
        }
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = shopStyleFragment;
                    break;
                case 1:
                    frag = shopMaleFragment;
                    break;
                case 2:
                    frag = shopFemaleFragment;
                    break;
                case 3:
                    frag = shopKidsFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    public int getSelectedTab() {
        return selectedTab;
    }
}
