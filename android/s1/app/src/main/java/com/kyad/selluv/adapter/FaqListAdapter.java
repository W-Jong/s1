package com.kyad.selluv.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dao.FaqDao;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.List;

public class FaqListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<FaqDao> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = -1;

    public FaqListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<FaqDao> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_faq, parent, false);
            final FaqDao anItem = (FaqDao) getItem(position);
            if (anItem == null) {
                return convertView;
            }

            TextView tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            tv_title.setText(anItem.getTitle());
            final TextView tv_content = (TextView) convertView.findViewById(R.id.tv_content);
            ImageButton ib_drop = (ImageButton) convertView.findViewById(R.id.ib_drop);
            if (selectedPos == position) {
                tv_content.setVisibility(View.VISIBLE);
                ib_drop.setBackgroundResource(R.drawable.selectbox_arrow_on);
                tv_content.setText(anItem.getContent());
            } else {
                tv_content.setVisibility(View.GONE);
                ib_drop.setBackgroundResource(R.drawable.selectbox_arrow_off);
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPos = position;
                    notifyDataSetChanged();
                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}