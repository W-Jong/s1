package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.api.enumtype.SEARCH_ORDER_TYPE;

import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class SearchReq {
    //@ApiModelProperty("검색키워드")

    private String keyword;

    //@ApiModelProperty("정렬")

    private SEARCH_ORDER_TYPE searchOrderType;

    //@ApiModelProperty("카테고리UID, 전체인 경우 0")

    private int categoryUid;

    //@ApiModelProperty("브랜드UID 리스트, 전체인 경우 빈리스트")

    @Size(min = 0)
    private List<Integer> brandUidList;

    //@ApiModelProperty("사이즈 리스트, 전체인 경우 빈리스트")

    @Size(min = 0)
    private List<String> pdtSizeList;

    //@ApiModelProperty("컨디션 리스트, 전체인 경우 빈리스트, NEW, BEST, GOOD, MIDDLE 만 해당")

    @Size(min = 0)
    private List<PDT_CONDITION_TYPE> pdtConditionList;

    //@ApiModelProperty("컬러 리스트, 전체인 경우 빈리스트")

    @Size(min = 0)
    private List<String> pdtColorList;

    //@ApiModelProperty("가격 낮은 값, -1이면 제한없음")
    @IntRange(from = -1)
    private long pdtPriceMin;

    //@ApiModelProperty("가격 높은 값, -1이면 제한없음")
    @IntRange(from = -1)
    private long pdtPriceMax;

    //@ApiModelProperty("품절 상품 제외여부")
    private boolean isSoldOutExpect;
}
