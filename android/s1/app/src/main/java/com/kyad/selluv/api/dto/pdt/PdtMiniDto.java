package com.kyad.selluv.api.dto.pdt;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class PdtMiniDto {
    //("상품UID")
    private int pdtUid;
    //    //("추가된 시간")
//    private Timestamp regTime;
    //("회원UID")
    private int usrUid;
    //("브랜드UID")
    private int brandUid;
    //("카테고리 UID")
    private int categoryUid;
    //("사이즈")
    private String pdtSize;
    //("대표사진")
    private String profileImg;
    //("변경전 가격")
    private long originPrice;
    //("판매가격")
    private long price;
    //("컬러명")
    private String colorName;
    //("모델명")
    private String pdtModel;
    //("업데이트 시간")
    private Timestamp edtTime;
    //("상태  0-삭제, 1-판매중 2-판매완료, 3-판매중지, 4-휴가모드")
    private int status;
    //
    private boolean edited;
}
