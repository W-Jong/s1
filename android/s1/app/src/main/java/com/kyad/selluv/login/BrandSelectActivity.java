package com.kyad.selluv.login;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.MainActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.login.adapter.LoginBrandAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Globals.userSignReq;

public class BrandSelectActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.rv_brand_list)
    RecyclerView rv_brand_list;
    @BindView(R.id.btn_ok)
    Button btn_ok;

    //Variables
    LoginBrandAdapter adapter;

    private Boolean isFirstReg = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_brand);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        isFirstReg = getIntent().getBooleanExtra(Constants.IS_FIRST, false);

        loadLayout();
        loadData();
    }

    @Override
    public void onBackPressed() {
        if (isFirstReg)
            return;
        super.onBackPressed();
    }

    @OnClick(R.id.btn_ok)
    void onOk(View v) {
        if (adapter == null && adapter.selectedBrands.size() < 1) {
            Toast.makeText(BrandSelectActivity.this, "관심브랜드를 선택해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        cmdResterBrand();
    }


    private void loadLayout() {

        final GridLayoutManager glm = new GridLayoutManager(this, 2);
        rv_brand_list.setLayoutManager(glm);

        adapter = new LoginBrandAdapter(this);
        rv_brand_list.setAdapter(adapter);
    }

    /**
     * 어댑터에서 이용
     *
     * @param enable
     */
    public void setBtnEnable(boolean enable) {
        btn_ok.setEnabled(enable);
    }

    private void loadData() {
        if (adapter == null) return;
        //TODO: Send current loaded count to Server and Load 30 data records

        getBrandAllList();

    }

    public void getBrandAllList() {
        showLoading();

        String pdtGroupType = "MALE";
        if (isFirstReg) {
            if (userSignReq.getGender() != 0)
                pdtGroupType = "FEMALE";
        } else {
            if (Globals.myInfo.getUsr().getGender() == 1) {
                pdtGroupType = "MALE";
            } else {
                pdtGroupType = "FEMALE";
            }
            btn_ok.setEnabled(true);
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<List<BrandListDto>>> genRes = restAPI.brandFameAll(!isFirstReg ? Globals.userToken : "temp", pdtGroupType);
        genRes.enqueue(new Callback<GenericResponse<List<BrandListDto>>>() {

            @Override
            public void onResponse(Call<GenericResponse<List<BrandListDto>>> call, Response<GenericResponse<List<BrandListDto>>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    adapter.dataList = response.body().getData();
                    adapter.setTempStaus(isFirstReg);
                    adapter.notifyDataSetChanged();
                } else {//실패
                    Toast.makeText(BrandSelectActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<List<BrandListDto>>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(BrandSelectActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cmdResterBrand() {
        if (isFirstReg) {
            startActivity(MainActivity.class, true, 0, 0);
        } else {
            setResult(RESULT_OK);
            finish();
        }
    }
}
