package com.kyad.selluv.api.request;


import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class DealRefundReq {
    //@ApiModelProperty("반품사유")
    //@Size(min = 1)
    private String refundReason;
//    @NonNull
    //@Size(min = 1)
    //@ApiModelProperty("반품이미지 리스트")
    private List<String> photos;
}
