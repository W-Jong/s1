package com.kyad.selluv.sell;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.PromotionCodeDao;
import com.kyad.selluv.api.enumtype.DEAL_TYPE;
import com.kyad.selluv.api.request.PromotionReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.SoftKeyboard;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.sell.fragment.SellDirectPriceDetail;
import com.kyad.selluv.sell.fragment.SellSignFragment;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Constants.ACT_PDT_EDITED;

public class SellPriceActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.edt_price)
    EditText edt_price;
    @BindView(R.id.tv_earn_price)
    TextView tv_earn_price;
    @BindView(R.id.ib_plus)
    ImageButton ib_plus;
    @BindView(R.id.ib_minus)
    ImageButton ib_minus;
    @BindView(R.id.tv_register)
    TextView tv_register;
    @BindView(R.id.tv_promotion_title)
    TextView tv_promotion_title;
    @BindView(R.id.iv_promotion_check)
    ImageView iv_promotion_check;
    @BindView(R.id.tv_shipping_charge)
    TextView tv_shipping_charge;
    @BindView(R.id.frag_sign)
    RelativeLayout frag_sign;
    @BindView(R.id.tb_nego)
    TriStateToggleButton tb_nego;
    @BindView(R.id.btn_register)
    Button btn_register;

    SoftKeyboard softKeyboard;

    /**
     * 사인 페이지
     */
    SellSignFragment signFragment = null;

    final int defaultShippingCharge = 3000;
    boolean fromMyPage = false;
    private String thumbFileName;
    /** ======================================================= **/
    /**
     * + - 버튼을 꾹 누르고 있을때 처리를 위해 필요
     **/
    private final int INTERVAL1 = 150;
    private final int INTERVAL2 = 50;
    private final int INTERVAL3 = 10;
    int interval = INTERVAL1;
    double rate = 0.5;
    Runnable runnablePrice;
    Runnable runnableRate;
    Handler handlerPrice = new Handler();
    Handler handlerRate = new Handler();
    /** ======================================================= **/


    /**
     * 판매가격 X1000
     */
    double sell_price = 5.0;
    /**
     * 배송비
     */
    int shipping_price = 0;
    /**
     * 프로모션 페이백
     */
    int promotion_price = 0;
    /**
     * 셀럽이용료
     */
    int selluv_fee = 0;
    /**
     * 결제수수료
     */
    int payment_fee = 0;
    /**
     * 정산금액
     */
    int received_price = 0;


    /**
     * 프로모션 혜택가격
     */
    PromotionCodeDao promotionCodeInfo = null;

    int categoryUid = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_price);
        addForSellActivity();

        fromMyPage = getIntent().getBooleanExtra(Constants.FROM_MYPAGE, false);
        thumbFileName = getIntent().getStringExtra(Constants.THUMB_IMAGE_FILE);

        if (fromMyPage) {
            categoryUid = Globals.selectedPdtDetailDto.getPdt().getCategoryUid();

            sell_price = Globals.pdtUpdateReq.getPrice() / 10000.0;
            shipping_price = (int) Globals.pdtUpdateReq.getSendPrice();

            btn_register.setVisibility(View.INVISIBLE);
        } else {
//            categoryUid = Globals.getSelectedCategory().getCategoryUid();

//            Globals.pdtCreateReq.setBrandUid(Globals.selectedBrand.getBrandUid());
//            Globals.pdtCreateReq.setCategoryUid(Globals.getSelectedCategory().getCategoryUid());
            //프로모션코드
            //가격
            //배송비
            //상품이미지 - 이미지 선택 화면에서 설정
            //사인이미지
            //부속품 선택 화면에서 설정 Globals.pdtCreateReq.setComponent();
            //부속품 선택 화면에서 설정 Globals.pdtCreateReq.setEtc();
            //상품가격 입력 화면에서 설정 Globals.pdtCreateReq.setNegoYn();
            //상품정보입력 화면에서 설정 Globals.pdtCreateReq.setPdtCondition();
            //상품군 선택 화면에서 설정 Globals.pdtCreateReq.setPdtGroup();

            signFragment = new SellSignFragment();
            signFragment.thumbFileName = thumbFileName;
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.add(R.id.frag_sign, signFragment, "SellSignFragment");
            transaction.commit();
        }

        loadLayout();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();
        softKeyboard.unRegisterSoftKeyboardCallback();
    }

    @Override
    public void onBackPressed() {
        if (signFragment != null && frag_sign.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            frag_sign.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }

    }

    /**
     * < 뒤로가기
     */
    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        finish();
    }

    /**
     * 등록
     */
    @OnClick({R.id.tv_register, R.id.btn_register})
    void onRegister(View v) {
        try {
            double cost = Double.valueOf(edt_price.getText().toString());
            edt_price.setText(String.format("%.2f", cost));
            sell_price = Double.valueOf(edt_price.getText().toString());
        } catch (Exception e) {
            sell_price = 5.0;
            e.printStackTrace();
        }

        if (fromMyPage) {
            //수정페이지에서 가격 설정
            Globals.pdtUpdateReq.setPrice((int) (sell_price * 10000));
            Globals.pdtUpdateReq.setSendPrice(shipping_price);
            if (tb_nego.getToggleStatus() == TriStateToggleButton.ToggleStatus.on) {
                Globals.pdtUpdateReq.setNegoYn(1);
            } else {
                Globals.pdtUpdateReq.setNegoYn(2);
            }

            sendBroadcast(new Intent(ACT_PDT_EDITED));
            finish();
        } else {
            Globals.pdtCreateReq.setPrice((int) (sell_price * 10000));
            Globals.pdtCreateReq.setSendPrice(shipping_price);

            frag_sign.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
            ((TextView) frag_sign.findViewById(R.id.tv_ware_size)).setText("size " + Globals.pdtCreateReq.getPdtSize());
            ((TextView) frag_sign.findViewById(R.id.tv_ware_price)).setText("￦" + Utils.getAttachCommaFormat((float) (sell_price * 10000)));

            File imgFile = new File(Globals.pdtCreateReq.getPhotos().get(0));
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ImageView myImage = frag_sign.findViewById(R.id.iv_nego_ware);
                myImage.setImageBitmap(myBitmap);

            }
        }
    }

    /**
     * 배송비 선택
     */
    @OnClick(R.id.rl_shipping_charge)
    void onPostPrice(View v) {
        final PopupDialog popup = new PopupDialog(this, R.layout.popup_post_price).setFullScreen().setBackGroundCancelable(true).setCancelable(true).setOnCancelClickListener(null);
        Utils.hideKeypad(this, popup.findView(R.id.ll_nego_background));
        EditText edt_SendPrice = (EditText) popup.findView(R.id.edt_price);
        edt_SendPrice.setText("" + Utils.str2int(tv_shipping_charge.getText().toString()));
        popup.setOnClickListener(R.id.tv_guide, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                Intent intent = new Intent(SellPriceActivity.this, GuidImagePopup.class);
                intent.putExtra(Constants.POPUP_IMAGE, R.drawable.mypage_sales_post_guide);
                startActivity(intent);
            }
        });
        popup.setOnOkClickListener(new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                EditText edt_price = (EditText) popup.findView(R.id.edt_price);
                if (edt_price.length() == 0) {
                    tv_shipping_charge.setText(Utils.getAttachCommaFormat(0) + "원");
                } else {
                    int val = Utils.str2int(edt_price.getText().toString());
                    tv_shipping_charge.setText(Utils.getAttachCommaFormat(val) + "원");
                }

                updateReceivedMoney();
                popup.dismiss();
            }
        });
        popup.show();
    }

    /**
     * 정산 상세
     */
    @OnClick(R.id.ib_earn_help)
    void onEarnHelp(View v) {
        Intent intent = new Intent(this, SellDirectPriceDetail.class);
        intent.putExtra("ware_price", (int) (sell_price * 10000));
        intent.putExtra("shipping_price", shipping_price);
        intent.putExtra("promotion_price", promotion_price);
        intent.putExtra("selluv_fee", selluv_fee);
        intent.putExtra("payment_fee", payment_fee);
        intent.putExtra("received_price", received_price);
        startActivity(intent);
    }

    /**
     * 프로모션코드 선택
     */
    @OnClick(R.id.rl_promotion)
    void onPromotion(View v) {
        final PopupDialog popup = new PopupDialog(this, R.layout.popup_promotion_code).setFullScreen().setBackGroundCancelable(true).setCancelable(true).setOnCancelClickListener(null);
        Utils.hideKeypad(this, popup.findView(R.id.ll_nego_background));
        popup.setOnOkClickListener(new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                cmdCheckPromotionCode(popup);
            }
        });
        ((EditText) popup.findView(R.id.edt_code)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    popup.findView(R.id.btn_ok).setEnabled(true);
                    popup.findView(R.id.btn_ok).setBackgroundColor(Color.BLACK);
                } else {
                    popup.findView(R.id.btn_ok).setEnabled(false);
                    popup.findView(R.id.btn_ok).setBackgroundColor(getResources().getColor(R.color.color_999999));
                }
            }
        });
        popup.show();
    }


    private void loadLayout() {

//        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        RelativeLayout mainLayout = findViewById(R.id.activity_main);
        InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);

        softKeyboard = new SoftKeyboard(mainLayout, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {
            @Override
            public void onSoftKeyboardHide() {
                String ss = edt_price.getText().toString();
                if (ss.length() < 1 || ss.startsWith(".")) {
                    sell_price = 5.0;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            edt_price.setText(String.format("%.2f", sell_price));
                            updateReceivedMoney();
                        }
                    });
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                double cost = Double.valueOf(edt_price.getText().toString());
                                edt_price.setText(String.format("%.2f", cost));
                                sell_price = Double.valueOf(edt_price.getText().toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onSoftKeyboardShow() {

            }
        });

        if (fromMyPage)
            tv_register.setText(R.string.finish);

        tb_nego.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
                if (fromMyPage) {

                } else {
                    if (booleanToggleStatus) {
                        Globals.pdtCreateReq.setNegoYn(1);
                    } else {
                        Globals.pdtCreateReq.setNegoYn(2);
                    }
                }
            }
        });

        if (fromMyPage) {
            if (Globals.pdtUpdateReq.getNegoYn() == 1)
                tb_nego.setToggleStatus(true);
            else
                tb_nego.setToggleStatus(false);
        } else {
            tb_nego.setToggleStatus(false);
        }

        edt_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String ss = edt_price.getText().toString();
                if (ss.length() < 1 || ss.startsWith(".")) {
//                    sell_price = 5.0;
//                    edt_price.setText(String.format("%.01f", sell_price));
//                    updateReceivedMoney();
                } else {
                    sell_price = Double.valueOf(ss);
                    if (sell_price < 5.0) {
                        sell_price = 5.0;
                        edt_price.setText(String.format("%.2f", sell_price));
                    }
                    updateReceivedMoney();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /** +- 버튼을 누르고 있을 때 처리부분 */
        runnablePrice = new Runnable() {
            @Override
            public void run() {
                if (edt_price.getText().toString().isEmpty()) {
                    return;
                }
                sell_price = Double.valueOf(edt_price.getText().toString());
                sell_price = sell_price + rate;
                if (sell_price < 5.0)
                    sell_price = 5.0;
                edt_price.setText(String.format("%.2f", sell_price));

                updateReceivedMoney();

                handlerPrice.postDelayed(runnablePrice, interval);
            }
        };
        /** +- 버튼을 누르고 있을 때 가속 처리부분 */
        runnableRate = new Runnable() {
            @Override
            public void run() {
                if (interval == INTERVAL1) {
                    interval = INTERVAL2;
                } else {
                    interval = INTERVAL3;
                }
            }
        };

        ib_plus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    rate = 0.5;
                    interval = INTERVAL1;

                    handlerPrice.post(runnablePrice);

                    handlerRate.postDelayed(runnableRate, 3000);
                    handlerRate.postDelayed(runnableRate, 5000);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    handlerPrice.removeCallbacks(runnablePrice);
                    handlerRate.removeCallbacks(runnableRate);
                }
                return false;
            }
        });

        ib_minus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    rate = -0.5;
                    interval = INTERVAL1;

                    handlerPrice.post(runnablePrice);

                    handlerRate.postDelayed(runnableRate, 3000);
                    handlerRate.postDelayed(runnableRate, 5000);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    handlerPrice.removeCallbacks(runnablePrice);
                    handlerRate.removeCallbacks(runnableRate);
                }
                return false;
            }
        });

        edt_price.setText(String.format("%.2f", sell_price));
        tv_shipping_charge.setText(Utils.getAttachCommaFormat(defaultShippingCharge) + "원");
        updateReceivedMoney();
    }

    /**
     * 정산금액 업데이트
     */
    private void updateReceivedMoney() {
        //상품가격
        int pdt_price = (int) (sell_price * 10000);
        //배송비
        shipping_price = Utils.str2int(tv_shipping_charge.getText().toString());
        //셀럽이용료
        selluv_fee = (int) ((pdt_price + shipping_price) * Constants.SELLUV_USE_FEE / 100.0);
        //결제수수료
        payment_fee = (int) ((pdt_price + shipping_price) * Constants.PAYMENT_FEE / 100.0);
        //프로미션코드페이백
        promotion_price = 0;
        if (promotionCodeInfo != null) {
            if (promotionCodeInfo.getPriceUnit() == 1) {
                promotion_price = (int) (selluv_fee * promotionCodeInfo.getPrice() / 100.0);
            } else {
                promotion_price = (int) promotionCodeInfo.getPrice();
            }
        }
        //정산금액
        received_price = pdt_price + shipping_price + promotion_price - selluv_fee - payment_fee;
        if (received_price < 0) {
            received_price = 0;
        }

        tv_earn_price.setText(String.format("%.02f", received_price / 10000.0));
    }

    /**
     * 프로모션 코드 정보 얻기
     *
     * @param popup
     */
    private void cmdCheckPromotionCode(final PopupDialog popup) {
        final String promosioncode = ((EditText) popup.findView(R.id.edt_code)).getText().toString();

        showLoading();

        PromotionReq promotionReq = new PromotionReq();
        promotionReq.setBrandUid(Globals.pdtCreateReq.getBrandUid());
        promotionReq.setDealType(DEAL_TYPE.SELLER);
        promotionReq.setPdtUid(0);
        promotionReq.setPromotionCode(promosioncode);
        promotionReq.setCategoryUid(categoryUid);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<PromotionCodeDao>> genRes = restAPI.getPromotionInfo(Globals.userToken, promotionReq);
        genRes.enqueue(new Callback<GenericResponse<PromotionCodeDao>>() {

            @Override
            public void onResponse(Call<GenericResponse<PromotionCodeDao>> call, Response<GenericResponse<PromotionCodeDao>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    promotionCodeInfo = response.body().getData();

                    // 상품 등록할 때
                    if (!fromMyPage) {
                        Globals.pdtCreateReq.setPromotionCode(promosioncode);
                    }

//                    tv_promotion_title.setText(promotionCodeInfo.getMemo());
                    tv_promotion_title.setText(promosioncode);
                    iv_promotion_check.setVisibility(View.VISIBLE);

                    updateReceivedMoney();

                    popup.dismiss();
                } else {//실패
                    Toast.makeText(SellPriceActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<PromotionCodeDao>> call, Throwable t) {
                closeLoading();

                Log.d("kyad-log", t.getMessage());
                Toast.makeText(SellPriceActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
