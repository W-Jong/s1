package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dao.LicenseDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.enumtype.LICENSE_TYPE;
import com.kyad.selluv.common.Constants;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LicenseActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_sub_title)
    TextView tv_sub_title;
    @BindView(R.id.tv_content)
    TextView tv_content;

    String license_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_license);
        setStatusBarWhite();

        license_type = getIntent().getStringExtra("LICENSE_TYPE");
        tv_content.setText("");
        if (license_type.equals(LICENSE_TYPE.LICENSE.toString())) {
            tv_title.setText(getString(R.string.user_license));
            tv_sub_title.setText("셀럽 서비스 이용약관");
        } else if (license_type.equals(LICENSE_TYPE.PERSONAL_POLICY.toString())) {
            tv_title.setText(getString(R.string.personal_info_terms));
            tv_sub_title.setText("셀럽 개인정보 취급방침 ver1.1");
        } else if (license_type.equals(LICENSE_TYPE.SELLUV_LICENSE.toString())) {
            tv_title.setText(getString(R.string.sell_policy));
            tv_sub_title.setText("셀럽 판매 및 환불정책 ver1.1");
        }  else if (license_type.equals(LICENSE_TYPE.CRADLINCESE.toString())) {
            tv_title.setText(getString(R.string.card_policy_title));
            tv_sub_title.setText(getString(R.string.card_policy_subtitle));
            tv_content.setText(getString(R.string.card_policy_content));
        } else {
            finish();
        }

        if (!license_type.equals(LICENSE_TYPE.CRADLINCESE.toString())) {
            loadLicense();
        }

    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    private void loadLicense() {

        showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<LicenseDao>> genRes = restAPI.otherLicense(Constants.GUEST_TOKEN, license_type);
        genRes.enqueue(new Callback<GenericResponse<LicenseDao>>() {

            @Override
            public void onResponse(Call<GenericResponse<LicenseDao>> call, Response<GenericResponse<LicenseDao>> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    updateData(response.body().getData().getContent());
                } else {//실패
                    Toast.makeText(LicenseActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<LicenseDao>> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(LicenseActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

    private void updateData(String contents) {
        tv_content.setText(contents);
    }
}
