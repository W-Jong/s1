/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dao.UsrPushSettingDao;
import com.kyad.selluv.mypage.AlarmSettingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmCommentFragment extends BaseFragment {

    @BindView(R.id.btn_switch)
    ToggleButton btn_switch;

    @BindView(R.id.tb_toggle1)
    Button tb_toggle1;
    @BindView(R.id.tb_toggle2)
    Button tb_toggle2;

    public AlarmCommentFragment() {
    }

    public void setAlarmInfo(UsrPushSettingDao dicInfo) {
        dicSettingInfo = dicInfo;
        String dealSetting = dicSettingInfo.getReplySetting();
        for (int i = 0; i < 2; i++) {
            String value = dealSetting.substring(i, i + 1);
            setAlarmStatus(i, Integer.valueOf(value));
        }
    }

    //UI Referencs
    private UsrPushSettingDao dicSettingInfo;
    //Vars
    View rootView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_alarmset_comment, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;

    }

    private void loadLayout() {


    }

    private void setAlarmStatus(int index, int status) {
        if (index == 0) {
            if (status == 1) {
                tb_toggle1.setSelected(true);
                ((AlarmSettingActivity) getActivity()).replyAlarm[0] = 1;
            } else {
                tb_toggle1.setSelected(false);
                ((AlarmSettingActivity) getActivity()).replyAlarm[0] = 0;
            }
        } else if (index == 1) {
            if (status == 1) {
                tb_toggle2.setSelected(true);
                ((AlarmSettingActivity) getActivity()).replyAlarm[1] = 1;
            } else {
                tb_toggle2.setSelected(false);
                ((AlarmSettingActivity) getActivity()).replyAlarm[1] = 0;
            }
        }


        if (tb_toggle1.isSelected() && tb_toggle2.isSelected()) {
            btn_switch.setChecked(true);
            ((AlarmSettingActivity) getActivity()).replyYn = 1;
        } else {
            btn_switch.setChecked(false);
            ((AlarmSettingActivity) getActivity()).replyYn = 1;
        }

        //click
        btn_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 2; i++) {
                    if (btn_switch.isSelected()) {
                        ((AlarmSettingActivity) getActivity()).replyAlarm[i] = 0;
                    } else {
                        ((AlarmSettingActivity) getActivity()).replyAlarm[i] = 1;
                    }
                }
                ((AlarmSettingActivity) getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle1.isSelected()) {
                    ((AlarmSettingActivity) getActivity()).replyAlarm[0] = 0;
                } else {
                    ((AlarmSettingActivity) getActivity()).replyAlarm[0] = 1;
                }
                ((AlarmSettingActivity) getActivity()).reqAlarmSetting();
            }
        });

        tb_toggle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb_toggle2.isSelected()) {
                    ((AlarmSettingActivity) getActivity()).replyAlarm[1] = 0;
                } else {
                    ((AlarmSettingActivity) getActivity()).replyAlarm[1] = 1;
                }
                ((AlarmSettingActivity) getActivity()).reqAlarmSetting();
            }
        });
    }

}
