package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.api.request.MultiStringReq;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.user.UserActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class FacebookFriendActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.ll_friend)
    LinearLayout ll_friend;
    @BindView(R.id.tv_friend_count)
    TextView tv_friend_count;
    @BindView(R.id.lly_main)
    LinearLayout lly_main;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }


    //Variables
    LayoutInflater inflater;
    /**
     * 페북콜백
     */
    private CallbackManager callbackManager;
    private ArrayList<String> arrFBList = new ArrayList<String>();
    private List<UsrFollowListDto> arrUsrList = new ArrayList<UsrFollowListDto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_facebook);
        setStatusBarWhite();

        //facebook
        callbackManager = CallbackManager.Factory.create();

//        facebookLogin();
        getFriends();
    }

    private void loadLayout() {
        inflater = LayoutInflater.from(this);
        tv_friend_count.setText(String.format(getString(R.string.facebook_friend_count), arrUsrList.size()));
        int count = arrUsrList.size();
        for (int i = 0; i < count; i++) {
            View item = inflater.inflate(R.layout.item_friend_follow, null);
            final UsrFollowListDto anItem = arrUsrList.get(i);
            ImageView iv_profile = (ImageView) item.findViewById(R.id.iv_profile);
            ImageUtils.load(iv_profile.getContext(), anItem.getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile);
            TextView name = (TextView) item.findViewById(R.id.tv_name);
            TextView en_name = (TextView) item.findViewById(R.id.tv_en_name);
            name.setText(anItem.getUsrNckNm());
            en_name.setText(anItem.getUsrId());
            ToggleButton btn_follow = (ToggleButton) item.findViewById(R.id.btn_follow);
            if (anItem.isUsrLikeStatus()) {
                btn_follow.setChecked(false);
            } else {
                btn_follow.setChecked(true);
            }
            iv_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FacebookFriendActivity.this, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    startActivity(intent);
                }
            });
            btn_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestUserFollow(anItem.isUsrLikeStatus(), anItem.getUsrUid());
                }
            });
            ll_friend.addView(item);
        }

        if (count == 0){
            lly_main.setVisibility(View.GONE);
        } else {
            lly_main.setVisibility(View.VISIBLE);
        }
    }

    private void facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("kyad-log", "Success");
                        getFriends();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("kyad-log", "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("kyad-log", "Error");
                    }
                });
    }

    private void getFriends() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest graphRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                try {
                    JSONArray jsonArrayFriends = jsonObject.getJSONObject("friends").getJSONArray("data");

                    for (int i = 0; i < jsonArrayFriends.length(); i++) {
                        JSONObject friendlistObject = jsonArrayFriends.getJSONObject(i);
                        arrFBList.add(friendlistObject.getString("id"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getFriendFacebookList();
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "friends");
        graphRequest.setParameters(param);
        graphRequest.executeAsync();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //페이스북 친구찾기
    private void getFriendFacebookList() {
        MultiStringReq dicInfo = new MultiStringReq();
        dicInfo.setList(arrFBList);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<UsrFollowListDto>>> genRes = restAPI.getFriendFacebookList(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<List<UsrFollowListDto>>(this) {
            @Override
            public void onSuccess(List<UsrFollowListDto> response) {
                closeLoading();

                arrUsrList = response;
                loadLayout();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }

    //req user follow/unfollow
    private void requestUserFollow(boolean islike, int uid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = islike ?
                restAPI.unFollowUsr(Globals.userToken, uid) :
                restAPI.followUsr(Globals.userToken, uid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                getFriends();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
