package com.kyad.selluv.camera;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView;
import com.yalantis.ucrop.view.UCropView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.yalantis.ucrop.UCropActivity.DEFAULT_COMPRESS_FORMAT;
import static com.yalantis.ucrop.UCropActivity.DEFAULT_COMPRESS_QUALITY;
import static com.yalantis.ucrop.view.OverlayView.FREESTYLE_CROP_MODE_DISABLE;
import static com.yalantis.ucrop.view.OverlayView.FREESTYLE_CROP_MODE_ENABLE;

public class PicRotateFragment extends DialogFragment {


    //UI Referencs
    @BindView(R.id.tv_order)
    TextView tv_order;
    @BindView(R.id.ucrop)
    UCropView mUCropView;
    @BindView(R.id.ib_left)
    ImageButton ibLeft;
    @BindView(R.id.ib_right)
    ImageButton ibRight;
    @BindView(R.id.bottomMenu)
    LinearLayout bottomMenu;
    @BindView(R.id.btn_finish)
    Button bnFinish;
    @BindView(R.id.btn_cancel)
    Button bnCancel;
    @BindView(R.id.ib_close)
    ImageButton ibClose;

    //Vars
    private View rootView;

    private GestureCropImageView mGestureCropImageView;
    private OverlayView mOverlayView;

    public ArrayList<String> imgFiles = new ArrayList<>();
    public int selectedImage = 0;

    private boolean isEdit = false;
    private boolean isRotating = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_pic_edit, container, false);
        ButterKnife.bind(this, rootView);

        isEdit = false;
        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @OnClick(R.id.ib_close)
    void onClose(View v) {
        dismiss();
    }

    @OnClick(R.id.btn_cancel)
    void onCancel(View v) {
        closeEdit();
    }

    @OnClick(R.id.btn_finish)
    void onFinish(View v) {
        if (isEdit) {
            cropAndSaveImage();
        }
    }

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        if (imgFiles.size() < 1)
            return;
        selectedImage--;
        if (selectedImage < 0)
            selectedImage = 0;

        mUCropView.resetCropImageView();
        loadLayout();
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        if (imgFiles.size() < 1)
            return;
        selectedImage++;
        if (selectedImage >= imgFiles.size()) {
            selectedImage = imgFiles.size() - 1;
        }

        mUCropView.resetCropImageView();
        loadLayout();
    }

    @OnClick(R.id.ib_crop)
    void onCrop(View v) {
        if (imgFiles.size() > 0) {

            isEdit = true;

            mOverlayView.setFreestyleCropMode(FREESTYLE_CROP_MODE_ENABLE);
            mOverlayView.setShowCropFrame(true);
            mOverlayView.setShowCropGrid(true);

            bnFinish.setVisibility(View.VISIBLE);
            bnCancel.setVisibility(View.VISIBLE);
            ibClose.setVisibility(View.INVISIBLE);
            bottomMenu.setVisibility(View.INVISIBLE);
            ibLeft.setVisibility(View.INVISIBLE);
            ibRight.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.ib_rotate)
    void onRotate(View v) {
        rotateByAngle(90);
    }

    @OnClick(R.id.ib_del)
    void onDel(View v) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_confirm)
                .setTitle(R.string.delete);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                if (imgFiles.size() > 0) {
                    imgFiles.remove(selectedImage);
                    selectedImage = selectedImage - 1 < 0 ? 0 : selectedImage - 1;
                    if (imgFiles.size() > 0) {
                        setImageData();
                    } else {
                        mGestureCropImageView.setVisibility(View.INVISIBLE);
                        selectedImage = -1;
                    }
                }
                ((SellPicActivity) getActivity()).reloadThumbnails();
                updateCountInfo();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void loadLayout() {

        bnFinish.setVisibility(View.INVISIBLE);
        bnCancel.setVisibility(View.INVISIBLE);
        ibClose.setVisibility(View.VISIBLE);
        bottomMenu.setVisibility(View.VISIBLE);

        mGestureCropImageView = mUCropView.getCropImageView();
        mGestureCropImageView.setTransformImageListener(mImageListener);
        mGestureCropImageView.setRotateEnabled(false);
        mOverlayView = mUCropView.getOverlayView();
        mOverlayView.setPadding(0, 0, 0, 0);

        mUCropView.post(new Runnable() {
            @Override
            public void run() {

                mGestureCropImageView.setCropRect(new RectF(0, 0, mUCropView.getWidth(), mUCropView.getHeight()));

                mOverlayView.setFreestyleCropMode(FREESTYLE_CROP_MODE_DISABLE);
                mOverlayView.setShowCropFrame(false);
                mOverlayView.setShowCropGrid(false);

                setImageData();
            }
        });

        updateCountInfo();
    }


    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    /**
     * This method extracts all data from the incoming intent and setups views properly.
     */
    private void setImageData() {

        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME + ".jpg";

        Uri inputUri = Uri.fromFile(new File(imgFiles.get(selectedImage)));
        Uri outputUri = Uri.fromFile(new File(getActivity().getCacheDir(), destinationFileName));

        if (inputUri != null) {
            try {
                //mGestureCropImageView.setBackgroundColor(Color.WHITE);
                mGestureCropImageView.setImageUri(inputUri, outputUri);
            } catch (Exception e) {
            }
        } else {
        }
    }

    private void cropAndSaveImage() {
        try {
            mGestureCropImageView.cropAndSaveImage(DEFAULT_COMPRESS_FORMAT, DEFAULT_COMPRESS_QUALITY, new BitmapCropCallback() {
                @Override
                public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
                    ((SellPicActivity) getActivity()).reloadThumbnails();

                    imgFiles.remove(selectedImage);
                    imgFiles.add(selectedImage, resultUri.getPath());

                    closeEdit();
                }

                @Override
                public void onCropFailure(@NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    private void closeEdit() {
        isEdit = false;
        mOverlayView.setupCropBounds();

        mUCropView.resetCropImageView();
        loadLayout();
    }

    private void rotateByAngle(int angle) {
        isRotating = true;

        mGestureCropImageView.postRotate(angle);
        mGestureCropImageView.setImageToWrapCropBounds();
    }


    void updateCountInfo() {
        tv_order.setText(String.valueOf(selectedImage + 1) + "/" + String.valueOf(imgFiles.size()));

        if (!isEdit) {
            if (selectedImage > 0) {
                ibLeft.setVisibility(View.VISIBLE);
            } else {
                ibLeft.setVisibility(View.INVISIBLE);
            }

            if (selectedImage < imgFiles.size() - 1) {
                ibRight.setVisibility(View.VISIBLE);
            } else {
                ibRight.setVisibility(View.INVISIBLE);
            }

        }
    }

    private TransformImageView.TransformImageListener mImageListener = new TransformImageView.TransformImageListener() {
        @Override
        public void onRotate(float currentAngle) {
            if (isRotating) {


                Matrix m = new Matrix();
                m.postRotate(currentAngle);
                Bitmap bmp = mGestureCropImageView.getViewBitmap();
                bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), m, true);

                String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME + selectedImage + ".jpg";

                File out = new File(getActivity().getCacheDir(), destinationFileName);
                try {
                    OutputStream stream = new FileOutputStream(out);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    stream.close();

                    imgFiles.remove(selectedImage);
                    imgFiles.add(selectedImage, out.getPath());

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                isRotating = false;
            }
        }

        @Override
        public void onScale(float currentScale) {

        }

        @Override
        public void onLoadComplete() {
            mUCropView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());
        }

        @Override
        public void onLoadFailure(@NonNull Exception e) {
        }

    };

}
