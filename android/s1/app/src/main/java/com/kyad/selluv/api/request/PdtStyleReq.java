package com.kyad.selluv.api.request;


import lombok.Data;
import lombok.NonNull;

@Data
public class PdtStyleReq {


    //@ApiModelProperty("상품UID")
    private int pdtUid;


    //@ApiModelProperty("스타일이미지")
    private String styleImg;
}
