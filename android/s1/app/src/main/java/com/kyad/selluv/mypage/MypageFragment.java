package com.kyad.selluv.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.DbManager;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.ShareManager;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.AlarmCountDto;
import com.kyad.selluv.api.dto.usr.UsrMyInfoDto;
import com.kyad.selluv.api.enumtype.LICENSE_TYPE;
import com.kyad.selluv.api.request.ContentReq;
import com.kyad.selluv.camera.GalleryFragment;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.top.TopLikeActivity;
import com.kyad.selluv.user.UserActivity;
import com.kyad.selluv.user.UserFollowActivity;
import com.kyad.selluv.user.UserMannerActivity;
import com.zoyi.channel.plugin.android.ChannelIO;
import com.zoyi.channel.plugin.android.ChannelPluginCompletionStatus;
import com.zoyi.channel.plugin.android.ChannelPluginSettings;
import com.zoyi.channel.plugin.android.OnBootListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.kyad.selluv.ShareManager.SM_LAST_LOGIN_ID;

public class MypageFragment extends BaseFragment {

    private UsrMyInfoDto myUserInfo;
    private String usrPass = "";
    private Bitmap userProfile;

    //UI Reference
    @BindView(R.id.ll_event_list)
    LinearLayout ll_event_list;
    @BindView(R.id.iv_profile_img)
    CircularImageView iv_profile_img;

    @BindView(R.id.tv_alarm_count)
    TextView txt_alarm_count;

    @BindView(R.id.tv_item)
    TextView txt_item;

    @BindView(R.id.tv_following)
    TextView txt_following;

    @BindView(R.id.tv_follower)
    TextView txt_follower;

    @BindView(R.id.tv_purchase_count)
    TextView txt_purchase_count;

    @BindView(R.id.tv_selluvmoney_count)
    TextView txt_selluvmoney_count;

    @BindView(R.id.tv_sales_count)
    TextView txt_sales_count;

    @BindView(R.id.tv_point)
    TextView txt_point;

    @BindView(R.id.tv_name)
    TextView txt_name;

    @BindView(R.id.btn_notice_count)
    Button btn_notice_count;

    @BindView(R.id.ib_right)
    ImageButton ib_right;


    @OnClick(R.id.ib_left)
    void onLeft() {
        startActivity(MypageAlarmActivity.class, false, 0, 0);
    }

    @OnClick(R.id.ib_right)
    void onRight() {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    @OnClick(R.id.ib_setting)
    void onSetting() {
        startActivity(MypageProfileActivity.class, false, 0, 0);
    }

    @OnClick(R.id.tv_name)
    void onName() {
        Intent intent = new Intent(getActivity(), UserMannerActivity.class);
        intent.putExtra(Constants.USER_UID, Globals.myInfo.getUsr().getUsrUid());
        startActivity(intent);
    }

    @OnClick(R.id.rl_point)
    void onPoint() {
        Intent intent = new Intent(getActivity(), UserMannerActivity.class);
        intent.putExtra(Constants.USER_UID, Globals.myInfo.getUsr().getUsrUid());
        startActivity(intent);
    }

    @OnClick(R.id.ll_item)
    void onItem() {
        Intent intent = new Intent(getActivity(), UserActivity.class);
        intent.putExtra(UserActivity.USR_UID, Globals.myInfo.getUsr().getUsrUid());
        startActivity(intent);
    }

    @OnClick(R.id.ll_follower)
    void onFollower() {
        Intent intent = new Intent(getActivity(), UserFollowActivity.class);
        intent.putExtra(Constants.USER_UID, Globals.myInfo.getUsr().getUsrUid());
        intent.putExtra(Constants.FOLLOW_PAGE_TITLE, Globals.myInfo.getUsr().getUsrNckNm());
        intent.putExtra(Constants.FOLLOW_PAGE_IDX, 0);
        startActivity(intent);
    }

    @OnClick(R.id.ll_following)
    void onFollowing() {
        Intent intent = new Intent(getActivity(), UserFollowActivity.class);
        intent.putExtra(Constants.USER_UID, Globals.myInfo.getUsr().getUsrUid());
        intent.putExtra(Constants.FOLLOW_PAGE_TITLE, Globals.myInfo.getUsr().getUsrNckNm());
        intent.putExtra(Constants.FOLLOW_PAGE_IDX, 1);
        startActivity(intent);
    }

    @OnClick(R.id.btn_myware)
    void onMyware() {
        Intent intent = new Intent(getActivity(), UserActivity.class);
        intent.putExtra(UserActivity.USR_UID, Globals.myInfo.getUsr().getUsrUid());
        startActivity(intent);
    }

    @OnClick(R.id.rl_notice)
    void onNotice() {
        startActivity(NoticeActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_event)
    void onEvent() {
        startActivity(EventActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_privacy)
    void onPrivacy() {
        startActivity(MypageEditActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_alarm)
    void onAlarm() {
        startActivity(AlarmSettingActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_finding_item)
    void onFindingItem() {
        startActivity(WishAlarmActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_seller_setting)
    void onSellerSeting() {
        startActivity(SellerSettingActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_card_manage)
    void onCardManage() {
        startActivity(CardManageActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_account_number)
    void onAccountNumber() {
        startActivity(AccountActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_user_guide)
    void onUserGuide() {
        Intent intent = new Intent(getActivity(), GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.mypage_guide);
        startActivity(intent);
    }

    @OnClick(R.id.rl_sell_policy)
    void onSellPolicy() {
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.SELLUV_LICENSE.toString());
        startActivity(intent);
    }

    @OnClick(R.id.rl_faq)
    void onFaq() {
        startActivity(FaqActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_quiz)
    void onQuiz() {
        final PopupDialog popup = new PopupDialog((BaseActivity) getActivity(), R.layout.popup_mypage_qa).setFullScreen().setBackGroundCancelable(true).setCancelable(true);
        popup.setOnClickListener(R.id.rl_p2p, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                startActivity(QaActivity.class, false, 0, 0);
            }
        });
        popup.setOnClickListener(R.id.ib_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });
        popup.setOnClickListener(R.id.rl_kakao, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                ChannelPluginSettings pluginSettings = new ChannelPluginSettings(Constants.kPluginKey);
                //test key
                //ChannelPluginSettings pluginSettings = new ChannelPluginSettings("4be44efa-59d8-4847-990f-d5cb3e9af40f");
                ChannelIO.boot(pluginSettings, new OnBootListener() {
                    @Override
                    public void onCompletion(ChannelPluginCompletionStatus status) {
                        ChannelIO.open(getActivity());
                    }
                });
            }
        });
        popup.setOnClickListener(R.id.btn_cs_phone, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
                //TODO PJh test phone num
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", Constants.SELLUV_CONTACT, null)));
            }
        });
        popup.show();
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    @OnClick(R.id.tv_license)
    void onLicense() {
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.LICENSE.toString());
        startActivity(intent);
    }

    @OnClick(R.id.tv_privacy_policy)
    void onPrivacyPolicy() {
        Intent intent = new Intent(getActivity(), LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.PERSONAL_POLICY.toString());
        startActivity(intent);
    }

    @OnClick(R.id.tv_sign_out)
    void onSingOut() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.sign_out))
                .setMessage(getString(R.string.userexit_alert))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        usrLogout();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    @OnClick(R.id.rl_purchase)
    void onPurchase() {
        startActivity(PurchaseHistoryActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_selluvmoney)
    void onMoney() {
        startActivity(MypageMoneyActivity.class, false, 0, 0);
    }

    @OnClick(R.id.rl_sales)
    void onSales() {
        startActivity(SalesHistoryActivity.class, false, 0, 0);
    }

    @OnClick(R.id.iv_profile_img)
    void onProfileImg() {
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.fromPage = SellPicActivity.FROM_PAGE_MYPAGE;
        galleryFragment.frameType = 1;
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        galleryFragment.imageSelectInterface = new GalleryFragment.ImageSelectInterface() {
            @Override
            public void onImageSelect(Bitmap bmp) {
                userProfile = bmp;
                addJpgSignatureToGallery(bmp);
            }
        };
        galleryFragment.show(ft, "GalleryFragment");
    }

    //Variables
    LayoutInflater inflater;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mypage, container, false);
        ButterKnife.bind(this, rootView);
        this.inflater = LayoutInflater.from(getActivity());

        BarUtils.addMarginTopEqualStatusBarHeight(rootView.findViewById(R.id.rl_actionbar));
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        String userid = ShareManager.manager().getValue(SM_LAST_LOGIN_ID, "");
        Model.UserLoginInfo lastLoginInfo = null;
        if (!userid.isEmpty()) {
            lastLoginInfo = DbManager.loadInfo(userid);
            usrPass = lastLoginInfo.password;
        }

        getMyDetailInfo();
    }

    private void loadLayout() {

        ll_event_list.removeAllViews();

        //add events to list
        for (int i = 0; i < myUserInfo.getMyPageEventList().size(); i++) {
            View eventView = inflater.inflate(R.layout.item_mypage_event, null);
            TextView txt_event_title = eventView.findViewById(R.id.tv_filter_name);
            txt_event_title.setText(myUserInfo.getMyPageEventList().get(i).getTitle());
            final int index = i;
            eventView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                    intent.putExtra(Constants.EVT_UID, myUserInfo.getMyPageEventList().get(index).getEventUid());
                    startActivity(intent);
                }
            });
            ll_event_list.addView(eventView);
        }

        //set user data
        txt_item.setText(Utils.getAttachCommaFormat((int) myUserInfo.getCountPdt()));
        txt_follower.setText(Utils.getAttachCommaFormat((int) myUserInfo.getUsrFollowerCount()));
        txt_following.setText(Utils.getAttachCommaFormat((int) myUserInfo.getUsrFollowingCount()));
        txt_purchase_count.setText(Utils.getAttachCommaFormat((int) myUserInfo.getCountBuy()));
        txt_selluvmoney_count.setText(Utils.getAttachCommaFormat((int) myUserInfo.getUsr().getMoney()));
        txt_sales_count.setText(Utils.getAttachCommaFormat((int) myUserInfo.getCountSell()));
        txt_point.setText(Utils.getAttachCommaFormat((int) myUserInfo.getUsr().getPoint()));
        txt_name.setText(myUserInfo.getUsr().getUsrNckNm());

        ImageUtils.load(iv_profile_img.getContext(), myUserInfo.getUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile_img);
        btn_notice_count.setText("" + myUserInfo.getCountUnreadNotice());

        Globals.myInfo.setUsr(myUserInfo.getUsr());
        DbManager.saveInfo(Globals.myInfo.getUsr().getUsrId(), usrPass, Globals.myInfo.getUsr().getUsrNckNm(), Globals.myInfo.getUsr().getProfileImg(), Globals.getLoginTypeStr(Globals.myInfo.getUsr().getUsrLoginType()));
        ShareManager.manager().put(SM_LAST_LOGIN_ID, Globals.myInfo.getUsr().getUsrId());

    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    private void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            String temp = String.format("Signature_%d.jpg", System.currentTimeMillis());
            File photo = new File(getAlbumStorageDir("SignaturePad"), temp);
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            uploadFile(photo.getPath());
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * newwork api
     */
    //user logout
    private void usrLogout() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = restAPI.deleteUser(Globals.userToken);

        genRes.enqueue(new TokenCallback<Void>(getActivity()) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                Utils.showToast(getActivity(), getString(R.string.userexit_success));
                startActivity(LoginStartActivity.class, true, 0, 0);
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //get user detail info
    private void getMyDetailInfo() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrMyInfoDto>> genRes = restAPI.myInfo(Globals.userToken);

        genRes.enqueue(new TokenCallback<UsrMyInfoDto>(getActivity()) {
            @Override
            public void onSuccess(UsrMyInfoDto response) {
                closeLoading();
                myUserInfo = response;
                Globals.myInfo = myUserInfo;

                loadLayout();
                getAlarmcount();
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }

    //get Alarm count
    private void getAlarmcount() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<AlarmCountDto>> genRes = restAPI.getAlarmCnt(Globals.userToken);

        genRes.enqueue(new TokenCallback<AlarmCountDto>(getActivity()) {
            @Override
            public void onSuccess(AlarmCountDto response) {
                closeLoading();

                if (response.getTotalCount() == 0) {
                    txt_alarm_count.setVisibility(View.INVISIBLE);
                } else {
                    txt_alarm_count.setVisibility(View.VISIBLE);
                    txt_alarm_count.setText("" + response.getTotalCount());
                }

                updateLikePdtBadge();
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }

    //좋아요 업데이트뱃지 표시여부
    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(getActivity()) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //file upload
    private void uploadFile(String filename) {
        ((BaseActivity) getActivity()).showLoading();

        File file = new File(filename);
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("uploadfile", file.getName(), surveyBody);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<HashMap<String, String>>> genRes = restAPI.uploadImage(multipartBody);
        genRes.enqueue(new TokenCallback<HashMap<String, String>>(getActivity()) {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                ((BaseActivity) getActivity()).closeLoading();
                //fileName, fileURL
                String imgFileName = response.get("fileName");
                updateProfile(imgFileName);
            }

            @Override
            public void onFailed(Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
            }
        });
    }

    //프로필 이미지 업데이트
    private void updateProfile(String filename) {

        ContentReq dicInfo = new ContentReq();
        dicInfo.setContent(filename);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.updateProfile(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(getActivity()) {
            @Override
            public void onSuccess(Void response) {

                Utils.showToast(getActivity(), getString(R.string.profile_modify_success));
                iv_profile_img.setImageBitmap(userProfile);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }
}
