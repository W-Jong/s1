package com.kyad.selluv.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Random;

/**
 * Created by admin on 10/30/15.
 */

public interface Model {
    /**
     * SQLITE로 관리되는 나의 계정 정보들...
     */
    class UserLoginInfo extends Object {
        public String userid = "";
        public String password = "";
        public String nickname = "";
        public String profile_url = "";
        public String login_type = "NORMAL";
        public int login_cnt = 0;
    }

    class WareItem implements Parcelable {
        public static final Creator<WareItem> CREATOR = new Creator<WareItem>() {
            @Override
            public WareItem createFromParcel(Parcel in) {
                return new WareItem(in);
            }

            @Override
            public WareItem[] newArray(int size) {
                return new WareItem[size];
            }
        };
        public int id = 0;
        public int imgID = 0;
        public String wareKrName = "";
        public String wareEngName = "";
        public double warePrice = 0;
        public double wareSalePrice = 0;
        public String size = "";
        public int minStyleImgID = 0;
        public int likeCnt = 0;
        public int profilePhotoID = 0;
        public String nickname = "";
        public String updateTime = "";
        public long time;
        public int state = 0;   // 1: soldout 2: modified
        public int feedType = 0;   /* 0: 찾았던 상품  1: LOVE ITEM 가격인하 2: 팔로우 브랜드
         3: 팔로우 유저의 잇템 4: 팔로우 유저의 스타일 5: 팔로우 유저의 신상품*/
        public int i_liked = 0; //0: false 1: true
        public int shopperType = 0; //상품군 0: Man 1: Woman 2: Kids

        public WareItem(int id, int imgID, String wareEngName, String wareKrName, double warePrice, double wareSalePrice, String size, int minStyleImgID, int likeCnt, int profilePhotoID, String nickname, String updateTime, int state, int i_liked, int feedType, int shopperType) {
            this.id = id;
            this.imgID = imgID;
            this.wareKrName = wareKrName;
            this.wareEngName = wareEngName;
            this.warePrice = warePrice;
            this.wareSalePrice = wareSalePrice;
            this.size = size;
            this.minStyleImgID = minStyleImgID;
            this.likeCnt = likeCnt;
            this.profilePhotoID = profilePhotoID;
            this.nickname = nickname;
            this.updateTime = updateTime;
            time = new Date().getTime() - new Random(new Date().getTime()).nextLong() % (1000 * 60 * 60 * 24 * 12 * 2);
            this.state = state;
            this.i_liked = i_liked;
            this.feedType = feedType;
            this.shopperType = shopperType;
        }

        protected WareItem(Parcel in) {
            id = in.readInt();
            imgID = in.readInt();
            wareKrName = in.readString();
            wareEngName = in.readString();
            warePrice = in.readDouble();
            wareSalePrice = in.readDouble();
            size = in.readString();
            minStyleImgID = in.readInt();
            likeCnt = in.readInt();
            profilePhotoID = in.readInt();
            nickname = in.readString();
            updateTime = in.readString();
            state = in.readInt();
            i_liked = in.readInt();
            feedType = in.readInt();
            shopperType = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeInt(imgID);
            parcel.writeString(wareKrName);
            parcel.writeString(wareEngName);
            parcel.writeDouble(warePrice);
            parcel.writeDouble(wareSalePrice);
            parcel.writeString(size);
            parcel.writeInt(minStyleImgID);
            parcel.writeInt(likeCnt);
            parcel.writeInt(profilePhotoID);
            parcel.writeString(nickname);
            parcel.writeString(updateTime);
            parcel.writeInt(state);
            parcel.writeInt(i_liked);
            parcel.writeInt(feedType);
            parcel.writeInt(shopperType);
        }
    }

    class BrandItem {
        public int id = 0;
        public int imgID = 0;
        public int logoImgID = 0;
        public String brandEngName = "";
        public String brandKrName = "";
        public int brandWareCnt = 0;

        public int i_followed = 0; //0: false 1: true
        public int followerCnt = 0;

        public BrandItem(int id, int imgID, int logoImgID, String brandEngName, String brandKrName, int brandWareCnt, int i_followed, int followerCnt) {
            this.id = id;
            this.imgID = imgID;
            this.logoImgID = logoImgID;
            this.brandEngName = brandEngName;
            this.brandKrName = brandKrName;
            this.brandWareCnt = brandWareCnt;
            this.i_followed = i_followed;
            this.followerCnt = followerCnt;
        }

    }

    class BrandItemComparator implements Comparator<BrandItem> {

        int lang = 0;

        public BrandItemComparator(int lang) {
            this.lang = lang;
        }

        @Override
        public int compare(BrandItem brandItem, BrandItem t1) {
            if (lang == 0) {
                return brandItem.brandEngName.compareToIgnoreCase(t1.brandEngName);
            } else {
                return brandItem.brandKrName.compareToIgnoreCase(t1.brandKrName);
            }
        }
    }


    class ItemDetail {

        public WareItem wareitem;

        public int old_price = 0;
        public String lower_percent = "";

        public int like_cnt = 0;
        public int item_cnt = 0;
        public int comment_cnt = 0;

        public String item_group = "";
        public String category1_name = "";
        public String category2_name = "";
        public String category3_name = "";
        public String brand_name = "";
        public String color_name = "";
        public String model_name = "";
        public ArrayList<String> seller_tag = new ArrayList<>();
        public String item_detail = "";

        public String post_price = "";
        public int condition_state = 1; //1: 새상품, 2: 최상, 3: 상, 4: 중상
        public ArrayList<String> accessory = new ArrayList<>();
        public int isRecStyle = 0; //0: 련관스타일 없음, 1: 있음.

        public int has_card = 0;

        public int total_price = 0;
        public int is_my_ware = 0;

        public ItemDetail(int id, int imgID, String wareEngName, String wareKrName, double warePrice, double wareSalePrice, int oldPrice, String lowerPercent, String size, int minStyleImgID, int likeCnt, int profilePhotoID, String nickname, String updateTime, int state, int i_liked, int like_cnt, int item_cnt,
                          int comment_cnt, String item_group, String category1_name, String category2_name, String category3_name, String brand_name, String color_name, String model_name, String[] seller_tag, String item_detail,
                          String post_price, int condition_state, String[] accessory, int isRecStyle, int has_card, int total_price, int is_my_ware) {

            this.wareitem = new WareItem(id, imgID, wareEngName, wareKrName, warePrice, wareSalePrice, size, minStyleImgID, likeCnt, profilePhotoID, nickname, updateTime, state, i_liked, 0, 0);

            this.old_price = oldPrice;
            this.lower_percent = lowerPercent;
            this.like_cnt = like_cnt;
            this.item_cnt = item_cnt;
            this.comment_cnt = comment_cnt;
            this.item_group = item_group;
            this.category1_name = category1_name;
            this.category2_name = category2_name;
            this.category3_name = category3_name;
            this.brand_name = brand_name;
            this.color_name = color_name;
            this.model_name = model_name;
            Collections.addAll(this.seller_tag, seller_tag);
            this.item_detail = item_detail;
            this.post_price = post_price;
            this.condition_state = condition_state;
            Collections.addAll(this.accessory, accessory);
            this.isRecStyle = isRecStyle;
            this.has_card = has_card;
            this.total_price = total_price;
            this.is_my_ware = is_my_ware;
        }

    }

    class SearchItem {
        public String name;
        public String info;
        public int count;
        /**
         * brand/category/model/tag object idx
         */
        public int idx;
        /**
         * 0: group title
         * 1: brand
         * 2: category
         * 3: model
         * 4: tag
         */
        public int type;

        public SearchItem(String name, String info, int count, int type, int idx) {
            this.name = name;
            this.info = info;
            this.count = count;
            this.type = type;
            this.idx = idx;
        }
    }


    class AlarmItem {
        public int icon;
        public int wareImg;
        public String wareName;
        public String alarmText;
        public String time;

        public AlarmItem(int icon, int wareImg, String alarmText, String wareName, String time) {
            this.icon = icon;
            this.wareImg = wareImg;
            this.alarmText = alarmText;
            this.wareName = wareName;
            this.time = time;
        }
    }


    class FaqItem {

    }

}
