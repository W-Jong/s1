package com.kyad.selluv.mypage;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.UsrCardDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.enumtype.LICENSE_TYPE;
import com.kyad.selluv.api.request.UserCardReq;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class CardRegisterActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.ib_person)
    ImageButton ib_person;
    @BindView(R.id.ib_corporation)
    ImageButton ib_corporation;
    @BindView(R.id.ib_checkbox)
    ImageButton ib_checkbox;
    @BindView(R.id.edt_number1)
    EditText edt_number1;
    @BindView(R.id.edt_number2)
    EditText edt_number2;
    @BindView(R.id.edt_number3)
    EditText edt_number3;
    @BindView(R.id.edt_number4)
    EditText edt_number4;
    @BindView(R.id.edt_birthday)
    EditText edt_birthday;
    @BindView(R.id.edt_passwrod)
    EditText edt_passwrod;
    @BindView(R.id.edt_business_number1)
    EditText edt_business_number1;
    @BindView(R.id.edt_business_number2)
    EditText edt_business_number2;
    @BindView(R.id.edt_business_number3)
    EditText edt_business_number3;
    @BindView(R.id.btn_register)
    Button btn_register;
    @BindView(R.id.rl_business_number)
    RelativeLayout rl_business_number;
    @BindView(R.id.rl_birthday)
    RelativeLayout rl_birthday;
    @BindView(R.id.picker_month)
    CustomSpinner picker_month;
    @BindView(R.id.picker_year)
    CustomSpinner picker_year;

    //Vars
    int currentFocus = 0;
    int cardType = 0;    // 0: 개인 1: 법인
    boolean agreeChecked;

    @OnClick(R.id.ib_person)
    void onPerson() {
        ib_person.setBackgroundResource(R.drawable.list_roundbox_on);
        ib_corporation.setBackgroundResource(R.drawable.list_roundbox_off);
        rl_business_number.setVisibility(View.GONE);
        rl_birthday.setVisibility(View.VISIBLE);
        cardType = 0;
        checkNextable();
    }

    @OnClick(R.id.btn_license_view)
    void onLincese() {
        Intent intent = new Intent(this, LicenseActivity.class);
        intent.putExtra("LICENSE_TYPE", LICENSE_TYPE.CRADLINCESE.toString());
        startActivity(intent);
    }

    @OnClick(R.id.ib_corporation)
    void onCorporation() {
        ib_person.setBackgroundResource(R.drawable.list_roundbox_off);
        ib_corporation.setBackgroundResource(R.drawable.list_roundbox_on);
        rl_business_number.setVisibility(View.VISIBLE);
        rl_birthday.setVisibility(View.GONE);
        cardType = 1;
        checkNextable();

    }

    @OnClick(R.id.ib_checkbox)
    void onCheckbox() {
        agreeChecked = !agreeChecked;
        if (agreeChecked) {
            ib_checkbox.setBackgroundResource(R.drawable.mypage_checkbox_on);
        } else {
            ib_checkbox.setBackgroundResource(R.drawable.mypage_checkbox_off);
        }
        checkNextable();
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_register)
    void onRegister() {
        registerCard();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_register);
        loadLayout();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        Calendar today = Calendar.getInstance();
        int year = today.get(Calendar.YEAR);

        String[] years = new String[50];
        for (int i = 0 ; i < 50 ; i++){
            years[i] = String.valueOf(year + i);
        }

        picker_year.initializeStringValues(this, years, getString(R.string.year), null, null, 12.5f);
        String[] months = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        picker_month.initializeStringValues(this, months, getString(R.string.month), null, null, 12.5f);


        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 4) {
                    if (currentFocus == 0) {
                        edt_number1.setText(edt_number1.getText().subSequence(0, 4));
                        edt_number2.requestFocus();
                        currentFocus = 1;
                    } else if (currentFocus == 1) {
                        edt_number2.setText(edt_number2.getText().subSequence(0, 4));
                        edt_number3.requestFocus();
                        currentFocus = 2;
                    } else if (currentFocus == 2) {
                        edt_number3.setText(edt_number3.getText().subSequence(0, 4));
                        edt_number4.requestFocus();
                        currentFocus = 3;
                    } else {
                        edt_number4.setText(edt_number4.getText().subSequence(0, 4));
                    }
                }
                checkNextable();
            }
        };

        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (v == edt_number1) currentFocus = 0;
                    else if (v == edt_number2) currentFocus = 1;
                    else if (v == edt_number3) currentFocus = 2;
                    else if (v == edt_number4) currentFocus = 3;
                }
            }
        };
        edt_number1.setOnFocusChangeListener(onFocusChangeListener);
        edt_number2.setOnFocusChangeListener(onFocusChangeListener);
        edt_number3.setOnFocusChangeListener(onFocusChangeListener);
        edt_number4.setOnFocusChangeListener(onFocusChangeListener);
        edt_number1.addTextChangedListener(textWatcher);
        edt_number2.addTextChangedListener(textWatcher);
        edt_number3.addTextChangedListener(textWatcher);
        edt_number4.addTextChangedListener(textWatcher);
    }

    void checkNextable() {
        if (cardType == 0 &&
                edt_number1.length() == 4 &&
                edt_number2.length() == 4 &&
                edt_number3.length() == 4 &&
                edt_number4.length() == 4 &&
                picker_month.getSelectedItemPosition() > 0 &&
                picker_year.getSelectedItemPosition() > 0 &&
                edt_birthday.length() > 0 &&
                edt_passwrod.length() > 0 &&
                agreeChecked) {
            btn_register.setEnabled(true);
            return;
        }

        if (cardType == 1 &&
                edt_number1.length() == 4 &&
                edt_number2.length() == 4 &&
                edt_number3.length() == 4 &&
                edt_number4.length() == 4 &&
                edt_business_number1.length() > 0 &&
                edt_business_number2.length() > 0 &&
                edt_business_number3.length() > 0 &&
                picker_month.getSelectedItemPosition() > 0 &&
                picker_year.getSelectedItemPosition() > 0 &&
                edt_birthday.length() > 0 &&
                edt_passwrod.length() > 0 &&
                agreeChecked) {
            btn_register.setEnabled(true);
            return;
        }
        btn_register.setEnabled(false);
    }

    //register card
    private void registerCard() {

        int kind = -1;
        String valid = "";
        String cardNum = String.format("%s-%s-%s-%s",
                edt_number1.getText().toString(),
                edt_number2.getText().toString(),
                edt_number3.getText().toString(),
                edt_number4.getText().toString());

        String validdate = String.format("%s-%02d-01",
                Integer.valueOf(picker_year.getSelectedItem().toString()),
                Integer.valueOf(picker_month.getSelectedItem().toString()));

        String validNum = "";
        if (cardType == 1){
            validNum = String.format("%s%s%s",
                    Integer.valueOf(edt_business_number1.getText().toString()),
                    Integer.valueOf(edt_business_number2.getText().toString()),
                    Integer.valueOf(edt_business_number3.getText().toString()));
        }

        String pass = edt_passwrod.getText().toString();
        String tempBirth = edt_birthday.getText().toString();

        if (cardNum.length() != 19 || cardNum.length() != 18){
            Utils.showToast(this, getString(R.string.wrong_card_num));
            return;
        }

        if (cardType == 0){ //개인
            kind = 1;
            if (tempBirth.length() != 6 ){
                Utils.showToast(this, getString(R.string.wrong_birthday_type));
                return;
            }
            valid = tempBirth;
        } else {
            kind = 2;
            if (validNum.length() != 10 ){
                Utils.showToast(this, getString(R.string.wrong_valid_num));
                return;
            }
            valid = validNum;
        }

        UserCardReq userCardReq = new UserCardReq();
        userCardReq.setCardNumber(cardNum);
        userCardReq.setKind(kind);
        userCardReq.setPassword(pass);
        userCardReq.setValidDate(validdate);
        userCardReq.setValidNum(valid);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrCardDao>> genRes = restAPI.registerCard(Globals.userToken, userCardReq);
        genRes.enqueue(new TokenCallback<UsrCardDao>(this) {
            @Override
            public void onSuccess(UsrCardDao response) {
                closeLoading();
                Utils.showToast(CardRegisterActivity.this, getString(R.string.card_reg_success));
                finish();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });

    }
}
