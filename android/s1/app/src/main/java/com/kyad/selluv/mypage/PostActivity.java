package com.kyad.selluv.mypage;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.DeliveryHistoryDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.DealDeliveryReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.webview.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.OnClick;
import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import retrofit2.Call;

public class PostActivity extends BaseActivity {

    private int dealUid = 0;

    //UI Reference
    @BindView(R.id.edt_name)
    EditText edt_name;
    @BindView(R.id.edt_contact)
    EditText edt_contact;
    @BindView(R.id.tv_address)
    EditText tv_address;
    @BindView(R.id.btn_book)
    Button btn_book;
    @BindView(R.id.tb_segment)
    TriStateToggleButton tb_segment;

    String[][] myAddressInfo = new String[3][4];
    int addrTabIndex = 0;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_guide)
    void onGuide() {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.mypage_sales_post_guide);
        startActivity(intent);
    }

    @OnClick(R.id.btn_book)
    void onBook() {
        reqDealDelivery();
    }

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_post);
        setStatusBarWhite();
        dealUid = getIntent().getIntExtra(Constants.DEAL_UID, 0);
        loadLayout();
    }

    private void loadLayout() {

        tb_segment.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
//                setEditValueFromDao(Globals.myInfo.getAddressList().get(toggleIntValue));
                addrTabIndex = toggleIntValue;
                selectTabBtn(toggleIntValue);
            }
        });

        edt_name.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    //do your work here
                    myAddressInfo[addrTabIndex][0] = edt_name.getText().toString();
                }
                isBookButtonStatus();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        edt_contact.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    //do your work here
                    myAddressInfo[addrTabIndex][1] = edt_contact.getText().toString();
                }
                isBookButtonStatus();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        setAddressData();
    }

    private void isBookButtonStatus(){
        String name = edt_name.getText().toString();
        String contact = edt_contact.getText().toString();
        String address = tv_address.getText().toString();
        if (!name.equals("") && !name.equals("") && !name.equals(""))
            btn_book.setEnabled(true);
        else
            btn_book.setEnabled(false);
    }

    private void setAddressData() {

        for (int i = 0; i < 3; i++) {
            myAddressInfo[i][0] = Globals.myInfo.getAddressList().get(i).getAddressName();
            myAddressInfo[i][1] = Globals.myInfo.getAddressList().get(i).getAddressPhone();
            myAddressInfo[i][2] = Globals.myInfo.getAddressList().get(i).getAddressDetail();
            myAddressInfo[i][3] = Globals.myInfo.getAddressList().get(i).getAddressDetailSub();
        }
        addrTabIndex = 0;
        for (int i = 0; i < 3; i++) {
            if (Globals.myInfo.getAddressList().get(i).getStatus() == 2) {
                addrTabIndex = i;
            }
        }
        selectTabBtn(addrTabIndex);
    }

    private void selectTabBtn(int index) {
        tb_segment.setToggleStatus(index);
        addrTabIndex = index;
        edt_name.setText(myAddressInfo[index][0]);
        edt_contact.setText(myAddressInfo[index][1]);

        if (!myAddressInfo[index][2].equals("") && !myAddressInfo[index][3].equals("")) {
            tv_address.setText(myAddressInfo[index][2] + "\n" + myAddressInfo[index][3]);
        } else {
            tv_address.setText("");
        }

        isBookButtonStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonWebViewActivity.WEBVIEW_ADDRESS_REQUEST) {
            boolean isSuccess = data.getBooleanExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS_SUCCESS, false);
            if (isSuccess) {
                String addressDetail = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_ADDRESS);
                String addressDetailSub = data.getStringExtra(CommonWebViewActivity.WEBVIEW_OUTPUT_POSTCODE);
                tv_address.setText(addressDetail + "\n" + addressDetailSub);

                myAddressInfo[addrTabIndex][2] = addressDetail;
                myAddressInfo[addrTabIndex][3] = addressDetailSub;

                if (!myAddressInfo[addrTabIndex][2].equals("") && !myAddressInfo[addrTabIndex][3].equals("")) {
                    tv_address.setText(myAddressInfo[addrTabIndex][2] + "\n" + myAddressInfo[addrTabIndex][3]);
                } else {
                    tv_address.setText("");
                }

            }
        }

    }

    //예약하기
    private void reqDealDelivery() {

        DealDeliveryReq dicInfo = new DealDeliveryReq();
        dicInfo.setDeliveryType(1);
        dicInfo.setRecipientAddress(myAddressInfo[addrTabIndex][2]);
        dicInfo.setRecipientAddressDetail(myAddressInfo[addrTabIndex][3]);
        dicInfo.setRecipientNm(myAddressInfo[addrTabIndex][0]);
        dicInfo.setRecipientPhone(myAddressInfo[addrTabIndex][1]);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<DeliveryHistoryDao>> genRes = restAPI.reqDealDelivery(Globals.userToken, dealUid, dicInfo);

        genRes.enqueue(new TokenCallback<DeliveryHistoryDao>(this) {
            @Override
            public void onSuccess(DeliveryHistoryDao response) {
                closeLoading();

                Intent intent = new Intent(PostActivity.this, PostFinishActivity.class);
                intent.putExtra(Constants.DEAL_UID, response.getDealUid());
                intent.putExtra(Constants.DELIVERY_NUMBER, response.getDeliveryNumber());
                startActivity(intent);
            }

            @Override
            public void onFailed(Throwable t) {

                closeLoading();

            }
        });
    }

}
