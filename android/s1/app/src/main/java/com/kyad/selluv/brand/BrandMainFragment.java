package com.kyad.selluv.brand;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.request.SearchReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.shop.ShopFragment;

import butterknife.ButterKnife;

public class BrandMainFragment extends BaseFragment {

    //UI Reference

    //Variables
    View rootView;
    FragmentManager fm;
    FragmentTransaction transaction;

    BrandFragment brandFragment;
//    BrandDetailActivity brandPageFragment;

    public int toWhere = 0;    //0 : Brand Fragment 1: Brand Page Fragment
    public int tabindex = 0;    //Index for Tab

    public static BrandMainFragment brandMainFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sub_main, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Globals.searchReq == null) {
            Globals.searchReq = new SearchReq();
        }
        Globals.initSearchFilters();
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    private void loadLayout() {
        brandMainFragment = this;

        if (brandFragment != null) {
            brandFragment = BrandFragment.newInstance(brandFragment.getSelectedTab());
        } else {
            brandFragment = BrandFragment.newInstance();
        }

//        brandPageFragment = BrandDetailActivity.newInstance();
        fm = getActivity().getSupportFragmentManager();

        if (toWhere == 0) {
            gotoBrandFragment(tabindex);
        } else {
            toWhere = 0;
            gotoBrandPageFragment(tabindex);
        }

    }

    public void gotoBrandFragment(int tabindex) {
        transaction = fm.beginTransaction();
        transaction.replace(R.id.fl_fragment, brandFragment);
        transaction.commit();
    }


    public void gotoBrandPageFragment(int brandUid) {
        Intent intent = new Intent(getActivity(), BrandDetailActivity.class);
        intent.putExtra(BrandDetailActivity.BRAND_UID, brandUid);
        intent.putExtra(BrandDetailActivity.BRAND_PRODUCT_GROUP, Globals.myInfo.getUsr().getGender());
        getActivity().startActivity(intent);
//        transaction = fm.beginTransaction();
//        brandPageFragment.selectedTab = tabindex;
//        transaction.replace(R.id.fl_fragment, brandPageFragment);
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.addToBackStack(null);
//        transaction.commit();
    }

    public void refreshBrandFollowFragment() {
        if (brandFragment != null)
            brandFragment.updateBrandFollowFragment();
    }
}
