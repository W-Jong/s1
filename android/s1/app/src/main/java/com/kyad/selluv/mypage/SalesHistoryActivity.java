package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.HistoryListAdapter1;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.SellHistoryDto;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class SalesHistoryActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_count)
    TextView tv_count;
    @BindView(R.id.tv_count_deal)
    TextView tv_count_deal;
    @BindView(R.id.tv_count_prepare)
    TextView tv_count_prepare;
    @BindView(R.id.tv_count_cert)
    TextView tv_count_cert;
    @BindView(R.id.tv_count_run)
    TextView tv_count_run;
    @BindView(R.id.tv_count_finish)
    TextView tv_count_finish;
    @BindView(R.id.tv_history_cnt)
    TextView tv_history_cnt;

    @BindView(R.id.lv_history)
    ListView lv_history;
    HistoryListAdapter1 listAdapter;
    private int currentPage = 0;
    private boolean isLast = false;
    private boolean isLoading = false;


    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_history);
        setStatusBarWhite();
        loadLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void loadLayout() {
        listAdapter = new HistoryListAdapter1(this);
        lv_history.setAdapter(listAdapter);
//        Collections.addAll(listAdapter.arrData, Constants.historyList1);
//        listAdapter.notifyDataSetChanged();
        lv_history.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getSaleHistoryList();
                }
            }
        });

    }

    public void initData(){
        currentPage = 0;
        isLoading = false;
        isLast = false;
        getSaleHistoryList();
    }

    //get sale history list
    private void getSaleHistoryList() {
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();
        Call<GenericResponse<SellHistoryDto>> genRes = restAPI.getSaleHistoryList(Globals.userToken, currentPage);

        genRes.enqueue(new TokenCallback<SellHistoryDto>(this) {
            @Override
            public void onSuccess(SellHistoryDto response) {
                closeLoading();
                isLoading = false;

                if (currentPage == 0) {
                    listAdapter.arrData.clear();
                }

                isLast = response.getHistory().isLast();

                tv_count.setText("" + response.getNegoCount());
                tv_count_deal.setText("" + response.getCachedCount());
                tv_count_prepare.setText("" + response.getPreparedCount());
                tv_count_cert.setText("" + response.getProgressCount());
                tv_count_run.setText("" + response.getSentCount());
                tv_count_finish.setText("" + response.getCompletedCount());
                tv_history_cnt.setText("" + response.getHistory().getTotalElements());

                listAdapter.arrData.addAll(response.getHistory().getContent());
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }
}
