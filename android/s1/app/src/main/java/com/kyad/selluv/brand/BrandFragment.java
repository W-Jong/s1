package com.kyad.selluv.brand;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.BarUtils;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.fragment.BrandFollowFragment;
import com.kyad.selluv.fragment.BrandListFragment;
import com.kyad.selluv.fragment.BrandPopularFragment;
import com.kyad.selluv.top.TopLikeActivity;
import com.kyad.selluv.top.TopSearchActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class BrandFragment extends BaseFragment {

    //UI Reference
    @BindView(R.id.tl_top_tab)
    TabLayout tl_top_tab;
    @BindView(R.id.vp_main)
    ViewPager vp_main;
    @BindView(R.id.ib_right)
    ImageButton ib_right;

    //Variables
    View rootView;
    BrandFollowFragment brandFollowFragment;
    BrandPopularFragment brandPopularFragment;
    BrandListFragment brandListFragment;
    private PageAdapter pagerAdapter;
    int selectedTab = 0;    //  0: 팔로우, 1: 인기, 2: 리스트

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        startActivity(TopSearchActivity.class, false, 0, 0);
    }

    @OnClick(R.id.ib_right)
    void onRight(View v) {
        startActivity(TopLikeActivity.class, false, 0, 0);
    }

    public static BrandFragment newInstance() {
        BrandFragment fragment = new BrandFragment();
        return fragment;
    }

    public static BrandFragment newInstance(int selectedTab) {
        BrandFragment fragment = new BrandFragment();
        fragment.selectedTab = selectedTab;
        return fragment;
    }

    public int getSelectedTab() {
        return selectedTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_brand, container, false);
        ButterKnife.bind(this, rootView);
        initFragment();
        loadLayout();
        return rootView;
    }

    @Override
    protected boolean isAllowAnimation() {
        return false;
    }

    private void loadLayout() {

        BarUtils.addMarginTopEqualStatusBarHeight(rootView.findViewById(R.id.appbar));
        rootView.findViewById(R.id.appbar).setAlpha(0.95f);

        tl_top_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                vp_main.setCurrentItem(selectedTab);
                setSelectedTabStyle(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        pagerAdapter = new PageAdapter(getActivity().getSupportFragmentManager());
        vp_main.setAdapter(pagerAdapter);
        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                tl_top_tab.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        vp_main.setOffscreenPageLimit(3);

        tl_top_tab.getTabAt(selectedTab).select();
        setSelectedTabStyle(selectedTab);

        updateLikePdtBadge();
    }

    private void initFragment() {
        if (brandFollowFragment == null) {
            brandFollowFragment = new BrandFollowFragment();
        }
        if (brandPopularFragment == null) {
            brandPopularFragment = new BrandPopularFragment();
        }
        if (brandListFragment == null) {
            brandListFragment = new BrandListFragment();
        }
    }

    private void setSelectedTabStyle(int index) {
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 0, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 1, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, 2, null, 15, Typeface.NORMAL);
        Utils.setTabItemFontAndSize(getActivity(), tl_top_tab, index, null, 15, Typeface.BOLD);
    }

    private void updateLikePdtBadge() {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Boolean>> genRes = restAPI.usrLikePdtStatus(Globals.userToken);

        genRes.enqueue(new TokenCallback<Boolean>(getActivity()) {
            @Override
            public void onSuccess(Boolean response) {
                ib_right.setActivated(response);
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    public void updateBrandFollowFragment() {
        if (brandFollowFragment ==  null)
            return;

        brandFollowFragment.refresh();
    }

    private class PageAdapter extends FragmentStatePagerAdapter {

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = brandFollowFragment;
                    break;
                case 1:
                    frag = brandPopularFragment;
                    break;
                case 2:
                    frag = brandListFragment;
                    break;
            }
            return frag;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
