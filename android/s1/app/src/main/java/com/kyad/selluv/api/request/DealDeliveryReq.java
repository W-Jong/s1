package com.kyad.selluv.api.request;


import lombok.Data;

@Data
public class DealDeliveryReq {
//    @NotNull
//    @ApiModelProperty("배송유형 1-일반주문, 2-정품인증, 3-반품배송")
    private int deliveryType = 1;
    //@Size(min = 1)
    //@ApiModelProperty("성함")
    private String recipientNm = "";
    //@Size(min = 1)
    //@ApiModelProperty("연락처")
    private String recipientPhone = "";
    //@Size(min = 1)
    //@ApiModelProperty("주소")
    private String recipientAddress = "";
//    @NotEmpty
//    @ApiModelProperty("상세주소 (우편번호)")
    private String recipientAddressDetail = "";
}
