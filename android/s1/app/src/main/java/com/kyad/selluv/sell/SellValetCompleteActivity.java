package com.kyad.selluv.sell;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class SellValetCompleteActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_ware)
    TextView tv_ware;

    @BindView(R.id.tv_send_method)
    TextView tv_send_method;

    @BindView(R.id.tv_desired_price)
    TextView tv_desired_price;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_contact)
    TextView tv_contact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vallet_complete);
        addForSellActivity();

        loadLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeForSellActivity();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @OnClick(R.id.btn_confirm)
    void onConfirm(View v) {
        finishForSellActivities();
    }

    private void loadLayout() {

        int desirePrice = getIntent().getIntExtra(Constants.DESIRED_PRICE, 0);
        int sendMethod = getIntent().getIntExtra(Constants.SEND_METHOD, 0);

        tv_send_method.setText(sendMethod == 1 ? "배송키트 이용" : "직접 발송");
        if (desirePrice == 0)
            tv_desired_price.setText("추천 가격");
        else
            tv_desired_price.setText(Utils.getAttachCommaFormat(desirePrice) + "원");

        String name = getIntent().getStringExtra(Constants.NAME);
        String address = getIntent().getStringExtra(Constants.ADDRESS);
        String contact = getIntent().getStringExtra(Constants.CONTACT);
        String valet_title = getIntent().getStringExtra(Constants.VALETE_COMPLETE_TITLE);

        tv_name.setText(name);
        tv_contact.setText(contact);
        tv_address.setText(address);
        tv_ware.setText(valet_title);

    }

}
