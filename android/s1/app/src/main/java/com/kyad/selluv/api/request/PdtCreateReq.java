package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;
import android.support.annotation.Size;

import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
public class PdtCreateReq {

    //@ApiModelProperty("브랜드UID")
    @IntRange(from = 1)
    private int brandUid;

    @NonNull
    //@ApiModelProperty("상품군")
    private PDT_GROUP_TYPE pdtGroup;

    //@ApiModelProperty("카테고리UID")
    @IntRange(from = 5)
    private int categoryUid;

    @NonNull
    private String categoryName;

    @Size(min = 1)
    //@ApiModelProperty("상품사이즈")
    private String pdtSize;

    @NonNull
    //@ApiModelProperty("상품이미지 리스트")
    private List<String> photos;

    @NonNull
    //@ApiModelProperty("상품이미지 경로리스트")
    private List<String> photoPaths;

    @NonNull
    //@ApiModelProperty("컨디션")
    private PDT_CONDITION_TYPE pdtCondition;

    @NonNull
    //@ApiModelProperty("태그")
    private String tag;

    @Size(min = 1)
    //@ApiModelProperty("부속품 예:110101 6자리문자열(0-없음, 1-있음)  첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백")
    private String component;

    @NonNull
    //@ApiModelProperty("기타")
    private String etc;

    @NonNull
    //@ApiModelProperty("모델명")
    private String pdtModel;

    @NonNull
    //@ApiModelProperty("상세설명")
    private String content;

    @NonNull
    //@ApiModelProperty("컬러")
    private String pdtColor;

    @IntRange(from = 50000)
    //@ApiModelProperty("가격")
    private long price;

    @IntRange(from = 0)
    //@ApiModelProperty("배송비")
    private long sendPrice;

    @NonNull
    private String brandEN;

    @NonNull
    private String brandKO;

    @IntRange(from = 1)
    //@ApiModelProperty("네고허용여부 1-네고허용 2-네고 받지 않음")
    private int negoYn;

    @NonNull
    //@ApiModelProperty("프로모션코드")
    private String promotionCode;

    @Size(min = 1)
    //@ApiModelProperty("사인이미지")
    private String signImg;

    public PdtCreateReq() {
        init();
    }

    public void init() {
        //필수 파라미터
        this.brandUid = 1;
        this.pdtGroup = PDT_GROUP_TYPE.MALE;
        this.categoryUid = 5;
//        this.pdtSize = "FREE";
        this.pdtSize = "";
        this.photos = new ArrayList<>();
        this.pdtCondition = PDT_CONDITION_TYPE.GOOD;
        this.price = 50000;
        this.sendPrice = 0;
        this.negoYn = 2;
        this.signImg = "";
        //옵션 파라미터
        this.tag = "";
        this.component = "000000";
        this.etc = "";
//        this.pdtModel = "기타";
        this.pdtModel = "";
        this.content = "";
//        this.pdtColor = "멀티";
        this.pdtColor = "";
        this.promotionCode = "";
        this.brandEN = "";
        this.brandKO = "";
        this.categoryName = "";
        this.photoPaths = new ArrayList<>();;
    }
}
