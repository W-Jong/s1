package com.kyad.selluv.common.expandablerecyclerview.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kyad.selluv.common.expandablerecyclerview.models.ExpandableGroup;

/**
 * ViewHolder for {@link ExpandableGroup#items}
 */
public class ChildViewHolder extends RecyclerView.ViewHolder {

    public ChildViewHolder(View itemView) {
        super(itemView);
    }
}
