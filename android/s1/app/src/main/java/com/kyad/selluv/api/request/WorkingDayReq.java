package com.kyad.selluv.api.request;

import lombok.Data;

@Data
public class WorkingDayReq {
//    @ApiModelProperty("기준날짜 (형식:2018-01-01)")
//    @NotNull
//    @JsonSerialize(using = LocalDateSerializer.class)
//    @JsonDeserialize(using = LocalDateDeserializer.class)
    private String basisDate;

//    @ApiModelProperty("변경 날짜수 -30~30사이만 허용")
//    @Range(min = -30, max = 30)
    private int deltaDays;
}
