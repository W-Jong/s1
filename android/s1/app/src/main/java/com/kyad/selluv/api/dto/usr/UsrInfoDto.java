package com.kyad.selluv.api.dto.usr;

import lombok.Data;

@Data
public class UsrInfoDto {
    //("회원UID")
    private int usrUid;
    //("아이디")
    private String usrId;
    //("닉네임")
    private String usrNckNm;
    //("프로필이미지")
    private String profileImg;
    //("프로필배경이미지")
    private String profileBackImg;
    //("자기소개")
    private String description;
    //("매너포인트")
    private long point;
    //("팔로워수")
    private long usrFollowerCount;
    //("팔로잉수")
    private long usrFollowingCount;
    //("유저 팔로우 상태")
    private Boolean usrLikeStatus;
    //("유저 차단 상태 ")
    private Boolean usrBlockStatus;

    public int getUsrUid() {
        return usrUid;
    }
}
