package com.kyad.selluv.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;

/**
 * Created by Rbin on 9/22/2017.
 */

public class PopupDialog {

    public interface bindOnClick {
        void onClick();
    }

    BaseActivity activity;
    ViewGroup parentView;
    ViewGroup baseView;
    ViewGroup contentView;
    ViewGroup contentContainer;
    int contentResID;
    boolean cancelable, isDismissing, isShowing, isFullScreen;
    LayoutInflater inflater;
    bindOnClick cancelableBinder = null;
    PopupDialog parentDlg = null;

    public PopupDialog() {
        init();
    }

    public PopupDialog(BaseActivity activity, int contentResID) {
        init();
        this.activity = activity;
        this.contentResID = contentResID;
        isFullScreen = false;
        initView();
    }

    private void init() {
        this.activity = null;
        this.contentResID = 0;
        cancelable = true;
        isDismissing = isShowing = false;
    }

    private void initView() {
        inflater = LayoutInflater.from(activity);
        parentView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        baseView = (ViewGroup) inflater.inflate(R.layout.dialog_base, null);
        contentView = (ViewGroup) inflater.inflate(contentResID, null);

        contentContainer = baseView.findViewById(R.id.content_container);
        contentContainer.addView(contentView);

//        baseView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (!cancelable) return true;
//                dismiss();
//                return true;
//            }
//        });
/*
        if ((Button) baseView.findViewById(R.id.btn_ok) != null) {
            ((Button) baseView.findViewById(R.id.btn_ok)).setTypeface(Constants.tfNotoSansCJKRegular);
        }
        if ((Button) baseView.findViewById(R.id.btn_cancel) != null) {
            ((Button) baseView.findViewById(R.id.btn_cancel)).setTypeface(Constants.tfNotoSansCJKRegular);
        }
        if ((Button) baseView.findViewById(R.id.btn_back) != null) {
            ((Button) baseView.findViewById(R.id.btn_back)).setTypeface(Constants.tfNotoSansCJKRegular);
        }
*/
        contentContainer.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                switch (keyEvent.getAction()) {
                    case KeyEvent.ACTION_UP:
                        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                            if (cancelable) {
                                if (cancelableBinder != null) cancelableBinder.onClick();
                                dismiss();
                            }
                            return true;
                        }
                        break;
                }
                return true;
            }
        });
    }

    public PopupDialog setCancelable(boolean b) {
        cancelable = b;
        return this;
    }

    public PopupDialog setBackGroundCancelable(boolean b) {
        cancelable = b;
        contentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return this;
    }

    public PopupDialog setBackGroundColor(int clr) {
        baseView.findViewById(R.id.outmost_container).setBackgroundColor(clr);
        return this;
    }

    public PopupDialog setFullScreen() {
        isFullScreen = true;
        RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layout.setMargins(0, 0, 0, 0);
        baseView.findViewById(R.id.content_container).setLayoutParams(layout);

        baseView.findViewById(R.id.content_container).requestLayout();

        return this;
    }

    public PopupDialog setBlur() {
        Bitmap map = Utils.takeScreenShot(activity);
        Bitmap fast = ImageUtils.fastBlur(map, 0.1f, 5);
        final Drawable draw = new BitmapDrawable(activity.getResources(), fast);
        baseView.setBackground(draw);
        return this;
    }

    public void setParentDlg(PopupDialog dlg) {
        this.parentDlg = dlg;
    }

    public PopupDialog setText(int layoutResID, String text, Typeface tf) {
        if (text != null && !text.isEmpty())
            ((TextView) baseView.findViewById(layoutResID)).setText(text);
        if (tf != null) ((TextView) baseView.findViewById(layoutResID)).setTypeface(tf);
        return this;
    }

    public PopupDialog setOnOkClickListener(final bindOnClick bind) {
        baseView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bind != null) bind.onClick();
                dismiss();
            }
        });
        return this;
    }

    public PopupDialog setOnCancelClickListener(final bindOnClick bind) {
        baseView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bind != null) bind.onClick();
                dismiss();
            }
        });
        return this;
    }

    public PopupDialog setOnClickListener(int resID, final bindOnClick bind) {
        baseView.findViewById(resID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bind != null) bind.onClick();
            }
        });
        return this;
    }

    public PopupDialog setOnCancelableBinder(final bindOnClick bind) {
        this.cancelableBinder = bind;
        return this;
    }

    /**
     * It adds the dialog view into rootView which is decorView of activity
     */
    public void show() {
        if (isShowing()) {
            //    return;
        }
        parentView.addView(baseView);
        Context context = parentView.getContext();
        Animation inAnim = AnimationUtils.loadAnimation(context, R.anim.fade_in_center);
        contentView.startAnimation(inAnim);

        contentContainer.requestFocus();
        baseView.requestDisallowInterceptTouchEvent(true);
    }

    /**
     * It basically check if the rootView contains the dialog view.
     *
     * @return true if it contains
     */
    public boolean isShowing() {
        View view = parentView.findViewById(R.id.outmost_container);
        return view != null;
    }

    /**
     * It is called when to dismiss the dialog, either by calling dismiss() method or with cancellable
     */
    public void dismiss() {
        if (isDismissing) {
            return;
        }

        Context context = parentView.getContext();
        Animation outAnim = AnimationUtils.loadAnimation(context, R.anim.fade_out_center);
        outAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                parentView.post(new Runnable() {
                    @Override
                    public void run() {
                        parentView.removeView(baseView);
                        isDismissing = false;
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        contentView.startAnimation(outAnim);
        isDismissing = true;
    }

    public View findView(int id) {
        if (baseView != null)
            return baseView.findViewById(id);
        return null;
    }
}
