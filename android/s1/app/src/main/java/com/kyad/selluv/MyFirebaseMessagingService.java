package com.kyad.selluv;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.View;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.preference.PrefConst;
import com.kyad.selluv.common.preference.Preference;
import com.kyad.selluv.login.LoginStartActivity;

import java.util.Map;


/**
 * Created by pjh on 2018. 05. 16..
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String pushType = "";
    int targetUid = 0;

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Map<String, String> data = message.getData();
        String p_type = data.get("type");
        String msg = message.getNotification().getBody();
        pushType = data.get("alarmType");
        targetUid = Integer.valueOf(data.get("targetUid"));

        if (pushType != null) {
            sendNotification(msg, "");
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String linkUrl) {

        PendingIntent pendingIntent = null;
        if (MainActivity.getInstance() != null) {

            // 앱이 실행중이라면
            Intent sendIntent = new Intent(Constants.ACT_PUSH_DETAIL);
            sendIntent.putExtra(PrefConst.PREFCONST_PUSH_TARGET_UID, targetUid);
            sendIntent.putExtra(PrefConst.PREFCONST_PUSH_ALARM_TYPE, pushType);
            sendBroadcast(sendIntent);

            pendingIntent = PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        } else {
            Preference.getInstance().putSharedPreference(this, PrefConst.PREFCONST_PUSH_TARGET_UID, targetUid);
            Preference.getInstance().putSharedPreference(this, PrefConst.PREFCONST_PUSH_ALARM_TYPE, pushType);

            Intent intent = new Intent(this, LoginStartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder;

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{0, 1000})
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setContentIntent(pendingIntent);


        //노티피케이션에 큰 사이즈 아이콘을 현시할때
        // setLargeIcon 을 이용할때..  lage아이콘 우측 하든에 small아이콘이 나오는데 이거 감추는 조작
        Notification notif = notificationBuilder.build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int smallIconViewId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

            if (smallIconViewId != 0) {
                if (notif.contentIntent != null)
                    notif.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (notif.headsUpContentView != null)
                    notif.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (notif.bigContentView != null)
                    notif.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
            }
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(1 /* ID of notification */, notif);
    }

}
