package com.kyad.selluv.common;

import com.kyad.selluv.R;
import com.kyad.selluv.api.dao.UsrCardDao;
import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;

public class Constants {

    public static final Boolean IS_TEST = false;

    /**
     * 셀럽 URL
     */
    public static final String SELLUV_SERVER_URL = "http://13.125.35.211/";

    /**
     * 게스트 토큰값
     */
    public static final String GUEST_TOKEN = "temp";

    /**
     * 브로드캐스트 액션
     */
    public static final String ACT_ERROR_ACCESS_TOKEN = "ACT_ERROR_ACCESS_TOKEN";
    public static final String ACT_NOT_PERMISSION = "ACT_NOT_PERMISSION";
    public static final String ACT_USER_SIGNUP = "ACT_USER_SIGNUP";
    public static final String ACT_PDT_EDITED = "ACT_PDT_EDITED";
    public static final String ACT_SEARCH_DETAIL = "ACT_SEARCH_DETAIL";
    public static final String ACT_SEARCH_CATEGORY = "ACT_SEARCH_CATEGORY";
    public static final String ACT_PUSH_DETAIL = "ACT_PUSH_DETAIL";
    /**
     * 인텐트로 인자 넘길때 이용하는 키값들..
     */
    public static final String IS_FIRST = "IS_FIRST";
    public static final String POPUP_IMAGE = "POPUP_IMAGE";
    public static final String EVT_UID = "EVT_UID";
    public static final String NOTICE_UID = "NOTICE_UID";
    public static final String USER_UID = "USER_UID";
    public static final String DEAL_UID = "DEAL_UID";
    public static final String DELIVERY_NUMBER = "DELIVERY_NUMBER";
    public static final String NICKNAME = "NICKNAME";
    public static final String PROFILE_IMG = "PROFILE_IMG";
    public static final String FOLLOW_PAGE_IDX = "FOLLOW_PAGE_IDX";
    public static final String FOLLOW_PAGE_TITLE = "FOLLOW_PAGE_TITLE";
    public static final String VALETE_COMPLETE_TITLE = "VALETE_COMPLETE_TITLE";
    public static final String THUMB_IMAGE_FILE = "THUMB_IMAGE_FILE";
    public static final String IMAGES = "IMAGES";
    public static final String IMAGES_NAMES = "IMAGES_NAMES";
    public static final String FROM_MYPAGE = "FROM_MYPAGE";
    public static final String FROM_PAGE = "FROM_PAGE";
    public static final String NAME = "NAME";
    public static final String ADDRESS = "ADDRESS";
    public static final String CONTACT = "CONTACT";
    public static final String DESIRED_PRICE = "DESIRED_PRICE";
    public static final String SEND_METHOD = "SEND_METHOD";
    public static final String SEARCH_WORD = "SEARCH_WORD";
    public static final String IS_FROM_FILTER = "IS_FROM_FILTER";
    public static final String PAGE_TITLE = "PAGE_TITLE";

    /**
     * REQ CODE CONSTANTS
     */
    public static final int REQ_RESULT_INTEREST_ACTIVITY = 1001;
    public static final int REQ_RESULT_SHOP_CATEGORY_SELECT_ACTIVITY = 1002;
    public static final int REQ_RESULT_BRAND_ADD_ACTIVITY = 1003;
    public static final int REQ_RESULT_BRAND_EDIT_ACTIVITY = 1004;

    //Enums
    public enum Direction {
        LEFT, TOP, RIGHT, BOTTOM
    }

    //Consts
    public static final int TAB_BAR_HEIGHT = 100;

    public static final int SHOP_BANNER_INTERVAL = 3500;
    public static final int LOAD_MORE_CNT = 6;

    public static final int VIEW_BOUNCE_ANIM_DURATION = 100;
    public static final int FRAG_IN_ANIM_DELAY = 300;

    public static final int WIRE_FRAG_FAME = 0;
    public static final int WIRE_FRAG_FEED = 1;
    public static final int WIRE_FRAG_NEW = 2;
    public static final int WIRE_FRAG_STYLE = 3;
    public static final int WIRE_FRAG_CATEGORY = 4;
    public static final int WIRE_FRAG_THEME = 5;
    public static final int WIRE_FRAG_BRAND = 6;
    public static final int WIRE_FRAG_PDT_RELATION_STYLE = 7;
    public static final int WIRE_FRAG_PDT_SAME_MODEL = 8;
    public static final int WIRE_FRAG_PDT_SAME_TAG = 9;
    public static final int WIRE_FRAG_USR_PDT = 10;
    public static final int WIRE_FRAG_USR_WISH = 11;
    public static final int WIRE_FRAG_USR_STYLE = 12;
    public static final int WIRE_FRAG_SEARCH = 13;
    public static final int WIRE_FRAG_PDT_LIKE = 14;
    public static final int WIRE_FRAG_PDT_STYLE_LIKE = 15;
    public static final int WIRE_FRAG_SHOP = 16;

    //page index
    public static final int PAGE_CATEGORY = 500;

    public static final int DATA_WARE = 0;
    public static final int DATA_STYLE = 1 << 4;
    public static final int DATA_FEED = 1 << 8;

    public static final String en = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String kr = "ㄱㄴㄷㄹㅁㅂㅅㅇㅈㅊㅋㅌㅍㅎ";
    public static final String krToCompare = "가나다라마바사아자차카타파하";


    //push type
    public static final String PUSH01_S_PAY_COMPLETED = "PUSH01_S_PAY_COMPLETED";
    public static final String PUSH02_B_DELIVERY_SENT = "PUSH02_B_DELIVERY_SENT";
    public static final String PUSH03_D_DEAL_COMPLETED = "PUSH03_D_DEAL_COMPLETED";
    public static final String PUSH04_B_DEAL_CANCELED = "PUSH04_B_DEAL_CANCELED";
    public static final String PUSH05_S_DEAL_CANCELED = "PUSH05_S_DEAL_CANCELED";
    public static final String PUSH06_B_PDT_VERIFICATION = "PUSH06_B_PDT_VERIFICATION";
    public static final String PUSH07_S_NEGO_SUGGEST = "PUSH07_S_NEGO_SUGGEST";
    public static final String PUSH08_B_NEGO_UPDATED = "PUSH08_B_NEGO_UPDATED";
    public static final String PUSH09_S_REFUND_REQUEST = "PUSH09_S_REFUND_REQUEST";
    public static final String PUSH10_B_REFUND_UPDATED = "PUSH10_B_REFUND_UPDATED";
    public static final String PUSH11_REWARD_INVITE = "PUSH11_REWARD_INVITE";
    public static final String PUSH12_REWARD_REPORT = "PUSH12_REWARD_REPORT";
    public static final String PUSH13 = "PUSH13";
    public static final String PUSH14_FOLLOW_ME = "PUSH14_FOLLOW_ME";
    public static final String PUSH15_LIKE_MY_PDT = "PUSH15_LIKE_MY_PDT";
    public static final String PUSH16_ITEM_MY_PDT = "PUSH16_ITEM_MY_PDT";
    public static final String PUSH17_LIKE_PDT_SALE = "PUSH17_LIKE_PDT_SALE";
    public static final String PUSH18_FOLLOWING_USER_PDT = "PUSH18_FOLLOWING_USER_PDT";
    public static final String PUSH19_FOLLOWING_USER_STYLE = "PUSH19_FOLLOWING_USER_STYLE";
    public static final String PUSH20_FOLLOWING_BRAND_PDT = "PUSH20_FOLLOWING_BRAND_PDT";
    public static final String PUSH21_FINDING_PDT = "PUSH21_FINDING_PDT";
    public static final String PUSH22_EVENT = "PUSH22_EVENT";
    public static final String PUSH23_NOTICE = "PUSH23_NOTICE";
    public static final String PUSH24_PDT_REPLY = "PUSH24_PDT_REPLY";
    public static final String PUSH25_REPLY_REPLY = "PUSH25_REPLY_REPLY";
    public static final String PUSH26_MENTIONED_ME = "PUSH26_MENTIONED_ME";


    /**
     * 셀럽 이용료
     */
    public static final float SELLUV_USE_FEE = 8.9f;
    /**
     * 결제 수수료
     */
    public static final float PAYMENT_FEE = 3.5f;


    public static final String[] feedTypeTitle = {"찾았던 상품", "LOVE ITEM 가격인하", "팔로우 브랜드",
            "팔로우 유저의 잇템", "팔로우 유저의 스타일", "팔로우 유저의 신상품"};
    public static final int[] feedTypeImg = {R.drawable.list_profile_find_icon, R.drawable.list_profile_lovesale_icon,
            R.drawable.list_profile_follow_icon, R.drawable.list_profile_follow_icon,
            R.drawable.list_profile_follow_icon, R.drawable.list_profile_follow_icon};

    //Tags
    public static final String INITIAL_TAB_INDEX = "INITIAL_TAB_INDEX";


    //==================================================================
    //부속품
    public static final String COMPONENT_NAMES[] = {"상품택", "개런티카드", "영수증", "여분부속품", "브랜드박스", "더스트백"};
    //컬러 네임
    public static final String COLOR_NAMES[] = {"블랙", "그레이", "화이트", "베이지", "레드", "핑크", "퍼플", "블루", "그린", "옐로우", "오렌지", "브라운", "골드", "실버", "멀티"};
    //상품 컨디션 라벨
    public static final String[] PDT_CONDITION_LABEL = {"새상품", "최상", "상", "중상"};
    //상품 컨디션 value
    public static final PDT_CONDITION_TYPE[] PDT_CONDITIONS = {PDT_CONDITION_TYPE.NEW, PDT_CONDITION_TYPE.BEST, PDT_CONDITION_TYPE.GOOD, PDT_CONDITION_TYPE.MIDDLE};
    //상품군
    public static final String[] shopper_type = {"남성", "여성", "키즈"};
    public static final String[] shopper_condition = {"신상품", "인기", "가격인하"};
    public static final String[][] SUBCATEGORY_1 = {
            {"의류", "가방", "슈즈", "패션소품", "시계/쥬얼리"},
            {"의류", "핸드백", "슈즈", "패션소품", "시계/쥬얼리"},
            {"베이비", "키즈보이", "키즈걸", "틴보이", "틴걸"}
    };
    public static final int[] shopper_type_icon = {R.drawable.ic_shop_men_list, R.drawable.ic_shop_women_list, R.drawable.ic_shop_kids_list};

    //Category1

    public static final String[] male_category1 = {"모두 보기", "의류", "가방", "슈즈", "패션소품", "시계/쥬얼리"};
    public static final String[] female_category1 = {"모두 보기", "의류", "핸드백", "슈즈", "패션소품", "시계/쥬얼리"};
    public static final String[] kids_category1 = {"모두 보기", "베이비", "키즈보이", "키즈걸", "틴보이", "틴걸"};

    public static final int[] male_category1_icon = {0, R.drawable.ic_shop_menlist_clothing, R.drawable.ic_shop_menlist_bag, R.drawable.ic_shop_menlist_shoes, R.drawable.ic_shop_menlist_accessories, R.drawable.ic_shop_menlist_watch};
    public static final int[] female_category1_icon = {0, R.drawable.ic_shop_womenlist_clothing, R.drawable.ic_shop_womenlist_handbag, R.drawable.ic_shop_womenlist_shoes, R.drawable.ic_shop_womenlist_accessories, R.drawable.ic_shop_womenlist_jewelry};
    public static final int[] kids_category1_icon = {0, R.drawable.ic_shop_kidslist_baby, R.drawable.ic_shop_kidslist_kidsboy, R.drawable.ic_shop_kidslist_kidsgirl, R.drawable.ic_shop_kidslist_teenboy, R.drawable.ic_shop_kidslist_teengirl};


    public static String[] seller_tag = new String[]{
            "#인생템"
    };

    public static String[] accessory = new String[]{
            "1",
            "2"
    };

    public static Model.ItemDetail itemDetail = new Model.ItemDetail(1, R.drawable.shop_list_sample_04, "HERMES", "에르메스 브라운 토트백", 332000, 233000, 453000, "20%", "Free", R.drawable.brand_sample, 9, R.drawable.list_profile_img02, "덕방이", "5시간 전", 0, 0, 25, 37, 2, "#여성", "#핸드백", "#토트백", "", "#샤넬", "#블랙", "#캐비어", seller_tag,
            "압구정 현대백화점 샤넬 매장에서 구매했습니다. 게런티카드도 같이 보내드립니다. 구배후 사용 몇번 안해서 새걱처럼 ...", "2300", 1, accessory, 0, 0, 13900, 0);


    //mypage alarm
    public static Model.AlarmItem[] alarmList0 = new Model.AlarmItem[]{
            new Model.AlarmItem(R.drawable.alarmlist_ic_genuine, R.drawable.alarmlist_ware01, "상품이 판매되었습니다. 상품을 발송하신 후, 운송장 번호를 입력해주세요", "", "1분"),
            new Model.AlarmItem(R.drawable.alarmlist_ic_genuine, R.drawable.shop_list_sample_01, "상품이 판매되었습니다. 상품을 발송하신 후, 운송장 번호를 입력해주세요.", "", "2분"),
            new Model.AlarmItem(R.drawable.alarmlist_ic_genuine, R.drawable.shop_list_sample_02, "상품이 판매되었습니다. 상품을 발송하신 후, 운송장 번호를 입력해주세요.", "", "3분"),
            new Model.AlarmItem(R.drawable.alarmlist_ic_genuine, R.drawable.shop_list_sample_03, "상품이 판매되었습니다. 상품을 발송하신 후, 운송장 번호를 입력해주세요.", "", "4분")
    };


    public static ArrayList<UsrCardDao> cardItemArrayList = new ArrayList<>();


    public static Model.FaqItem[] faqItems = new Model.FaqItem[]{
            new Model.FaqItem(),
            new Model.FaqItem(),
            new Model.FaqItem()

    };


    //------------------------------
    // 셀럽 주소 테스트주소
    //------------------------------
    public static final String SELLUV_CONTACT = "01049991004";
    public static final String SELLUV_NAME = "셀럽";
    public static final String SELLUV_ADDRESS = "서울틀별시 성동구 해탄로 15";

    public static final String DELIVERY_URL = "https://m.search.daum.net/search?w=tot&q=";

    //channel
    public static final String kPluginKey = "a364fb24-bbaf-4c0b-8a48-646d8596d787";

    //Twitter
    public static final String kTwitterKey = "kQv6NICUfwsx8Nh0QfOFWnDsK";
    public static final String kTwitterSecretKey = "Zt2yyba9phC9o4GMLepMhujLpCJIaYMc8DBXqcpUd6jZLJK6iu";

    //Twitter
    public static final String kInstagramID = "fea36ebbfb67488f9d715473d5fa6835";
    public static final String kInstagramSecret = "ad47a8bb9e7a4086b28569536d49c834";
    public static final String kInstagramRedirectUrl = "http://selluv.com";

}