package com.kyad.selluv.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.deal.DealDao;
import com.kyad.selluv.api.dto.deal.DealListDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.PopupDialog;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.detail.DetailActivity;
import com.kyad.selluv.mypage.FeedbackActivity;
import com.kyad.selluv.mypage.SalesDetailActivity;
import com.kyad.selluv.mypage.SalesHistoryActivity;
import com.kyad.selluv.mypage.SalesReturnActivity;
import com.kyad.selluv.mypage.SnsRewardActivity;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;

import retrofit2.Call;

public class HistoryListAdapter1 extends BaseAdapter {

    LayoutInflater inflater;
    public ArrayList<DealListDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;
    private long cNegoPrice = 0;

    public HistoryListAdapter1(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<DealListDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_purchase_history, parent, false);
            final DealListDto anItem = (DealListDto) getItem(position);
            if (anItem == null) {
                return convertView;
            }
            ImageView iv_ware = (ImageView) convertView.findViewById(R.id.iv_ware);
            RelativeLayout rl_btn_list = (RelativeLayout) convertView.findViewById(R.id.rl_btn_list);
            RelativeLayout rly_main = (RelativeLayout) convertView.findViewById(R.id.rly_main);
            Button btn_white = (Button) convertView.findViewById(R.id.btn_white);
            Button btn_black = (Button) convertView.findViewById(R.id.btn_black);
            Button btn_gray = (Button) convertView.findViewById(R.id.btn_gray);
            TextView tv_brand_name = (TextView) convertView.findViewById(R.id.tv_brand_name);
            TextView tv_ware_name = (TextView) convertView.findViewById(R.id.tv_ware_name);
            TextView tv_size = (TextView) convertView.findViewById(R.id.tv_size);
            TextView tv_seller_name = (TextView) convertView.findViewById(R.id.tv_seller_name);
            TextView tv_price_nego = (TextView) convertView.findViewById(R.id.tv_price_nego);
            TextView tv_shipping_charge = (TextView) convertView.findViewById(R.id.tv_shipping_charge);
            TextView tv_nego_price = (TextView) convertView.findViewById(R.id.tv_nego_price);
            TextView tv_status = (TextView) convertView.findViewById(R.id.tv_status);

            String pdtPrice = Utils.getAttachCommaFormat((float) anItem.getDeal().getPdtPrice());
            String reqPrice = Utils.getAttachCommaFormat((float) anItem.getDeal().getReqPrice());
            tv_brand_name.setText(anItem.getDeal().getBrandEn());
            tv_ware_name.setText(anItem.getDeal().getPdtTitle());
            tv_size.setText(anItem.getDeal().getPdtSize());
            tv_seller_name.setText(anItem.getUsr().getUsrNckNm());
            ImageUtils.load(iv_ware.getContext(), anItem.getDeal().getPdtImg(), R.drawable.ic_logo, R.drawable.ic_logo, iv_ware);
            tv_shipping_charge.setText(pdtPrice + " 원");
            tv_price_nego.setText(reqPrice + " 원");
            tv_nego_price.setText(activity.getString(R.string.nego_price));

            rly_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toDetail(anItem.getDeal().getPdtUid());
                }
            });

            tv_seller_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getDeal().getSellerUsrUid());
                    activity.startActivity(intent);
                }
            });

            switch (anItem.getDeal().getStatus()) {

                case 12:  //카운터 네고
                    tv_status.setText(activity.getString(R.string.counter_nego1));
                    rl_btn_list.setVisibility(View.GONE);
                    tv_nego_price.setText(activity.getString(R.string.counter_nego_price));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
                case 11: //네고제안
                    tv_status.setText(activity.getString(R.string.detail_nego_title));

                    btn_white.setText(activity.getString(R.string.nego_reject));
                    btn_gray.setText(activity.getString(R.string.counter_nego1));
                    btn_black.setText(activity.getString(R.string.nego_accept));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //네고 거절
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("")
                                    .setMessage(activity.getString(R.string.seller_nego_refuse_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqDealNegoRefuse(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });
                    btn_gray.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //카운터네고
                            showCounterNego(anItem);
                        }
                    });
                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //네고승인
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("")
                                    .setMessage(activity.getString(R.string.seller_nego_refuse_allow_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqDealNegoAllow(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });
                    break;
                case 1: //배송준비
                    tv_status.setText(activity.getString(R.string.shipping_wait));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_black.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);
                    btn_white.setText(R.string.sales_refues);
                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //판매거절 api
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("")
                                    .setMessage(activity.getString(R.string.sell_stop_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqDealCancelSeller(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });
                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
                case 2: //배송진행
                    tv_status.setText(activity.getString(R.string.delivery_success));
                    rl_btn_list.setVisibility(View.GONE);
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;

                case 3:  //정품인증
                    rl_btn_list.setVisibility(View.GONE);
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    tv_status.setText(activity.getString(R.string.delivery_success));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    break;
                case 4: //배송완료

                    tv_status.setText(activity.getString(R.string.delivery_finishh));
                    rl_btn_list.setVisibility(View.GONE);
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    break;

                case 5: //거래완료

                    tv_status.setText(activity.getString(R.string.deal_finish1));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);

                    btn_black.setText(activity.getString(R.string.deal_feedback));
                    btn_white.setText(activity.getString(R.string.sharing_reward_request));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });


                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //거래후기
                            Intent intent = new Intent(activity, FeedbackActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            intent.putExtra(Constants.NICKNAME, anItem.getUsr().getUsrNckNm());
                            activity.startActivity(intent);
                        }
                    });

                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //공유리워드 신청
                            Intent intent = new Intent(activity, SnsRewardActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            activity.startActivity(intent);
                        }
                    });

                    if (Utils.getDiffTime2(anItem.getDeal().getCompTime().getTime()) > 7) {
                        btn_white.setVisibility(View.GONE);
                    } else {
                        btn_white.setVisibility(View.VISIBLE);
                    }

                    if (Utils.getDiffTime2(anItem.getDeal().getCompTime().getTime()) > 3) {
                        btn_black.setVisibility(View.GONE);
                    } else {
                        btn_black.setVisibility(View.VISIBLE);
                    }

                    if (btn_white.getVisibility() == View.GONE && btn_white.getVisibility() == View.GONE && btn_gray.getVisibility() == View.GONE) {
                        rl_btn_list.setVisibility(View.GONE);
                    } else {
                        rl_btn_list.setVisibility(View.VISIBLE);
                    }

                    break;
                case 6: //정산완료
                    tv_status.setText(activity.getString(R.string.calculate_finish));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);
                    btn_black.setVisibility(View.GONE);
                    btn_white.setText(activity.getString(R.string.sharing_reward_request));

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });


                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //공유리워드 신청
                            Intent intent = new Intent(activity, SnsRewardActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            activity.startActivity(intent);
                        }
                    });

                    break;
                case 10: //주문취소
                    tv_status.setText(activity.getString(R.string.order_cancel1));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    rl_btn_list.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });
                    break;
                case 7: //반품접수

                    tv_status.setText(activity.getString(R.string.refrund_req1));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);

                    btn_black.setText(activity.getString(R.string.return_accept));
                    btn_white.setText(activity.getString(R.string.return_reject));
                    btn_gray.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //반품 승인
                            Intent intent = new Intent(activity, SalesReturnActivity.class);
                            intent.putExtra(Constants.DEAL_UID, anItem.getDeal().getDealUid());
                            activity.startActivity(intent);
                        }
                    });

                    btn_white.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //반품 거절
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("")
                                    .setMessage(activity.getString(R.string.refund_disallow_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqRefundDisallow(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });

                    break;
                case 8: //반품승인
                    tv_status.setText(activity.getString(R.string.refrund_accept));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);

                    btn_black.setText(activity.getString(R.string.return_confirm));
                    btn_white.setVisibility(View.GONE);
                    btn_gray.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    btn_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //반품 확인
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("")
                                    .setMessage(activity.getString(R.string.refund_confrim_alert))
                                    .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            reqRefundComplete(anItem.getDeal().getDealUid());
                                        }
                                    })
                                    .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                    });

                    break;
                case 9: //반품완료

                    tv_status.setText(activity.getString(R.string.refrund_finish));
                    tv_nego_price.setVisibility(View.GONE);
                    tv_price_nego.setVisibility(View.GONE);
                    rl_btn_list.setVisibility(View.GONE);

                    tv_status.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toSaleDetail(anItem.getDeal().getDealUid());
                        }
                    });

                    break;
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

    public void toDetail(int pdtUid) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, pdtUid);
        activity.startActivity(intent);
    }

    public void toSaleDetail(int dealUid) {
        Intent intent = new Intent(activity, SalesDetailActivity.class);
        intent.putExtra(Constants.DEAL_UID, dealUid);
        activity.startActivity(intent);
    }


    //판매자 네고거절
    private void reqDealNegoRefuse(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.reqDealNegoRefuse(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {

                ((SalesHistoryActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //네고승인
    private void reqDealNegoAllow(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqDealNegoAllow(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((SalesHistoryActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //판매거절
    private void reqDealCancelSeller(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqDealCancelSeller(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((SalesHistoryActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //반품확인
    private void reqRefundComplete(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqRefundComplete(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((SalesHistoryActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //반품거절
    private void reqRefundDisallow(int dealUid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<DealDao>> genRes = restAPI.reqRefundDisallow(Globals.userToken, dealUid);

        genRes.enqueue(new TokenCallback<DealDao>(activity) {
            @Override
            public void onSuccess(DealDao response) {

                ((SalesHistoryActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    //판매자 카운터 네고 요청
    private void reqDealNegoCounter(int dealUid, long Price, final PopupDialog plg) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.reqDealNegoCounter(Globals.userToken, dealUid, Price);

        genRes.enqueue(new TokenCallback<Void>(activity) {
            @Override
            public void onSuccess(Void response) {

                plg.dismiss();
                ((SalesHistoryActivity) activity).initData();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }

    private void showCounterNego(final DealListDto dicDealInfo) {
        final PopupDialog popup = new PopupDialog(activity, R.layout.popup_sales_counternego).setFullScreen().setBackGroundCancelable(true).setCancelable(true);
        Utils.hideKeypad(activity, popup.findView(R.id.ll_nego_background));

        ImageView ivPdt = ((ImageView) popup.findView(R.id.iv_nego_ware));
        TextView tvBrandEn = ((TextView) popup.findView(R.id.tv_ware_name_eng));
        TextView tvContent = ((TextView) popup.findView(R.id.tv_ware_name_ko));
        TextView tvSize = ((TextView) popup.findView(R.id.tv_ware_size));
        TextView tvPrice = ((TextView) popup.findView(R.id.tv_ware_price));
        TextView tvMinPrice = ((TextView) popup.findView(R.id.tv_price_min_value));
        TextView tvMaxPrice = ((TextView) popup.findView(R.id.tv_price_max_value));

        ImageUtils.load(ivPdt.getContext(), dicDealInfo.getDeal().getPdtImg(), R.drawable.img_default_square, R.drawable.img_default_square, ivPdt);
        tvBrandEn.setText(dicDealInfo.getDeal().getBrandEn());
        tvContent.setText(dicDealInfo.getDeal().getPdtTitle());
        tvSize.setText(dicDealInfo.getDeal().getPdtSize());
        tvPrice.setText("￦ " + Utils.getAttachCommaFormat((int) dicDealInfo.getDeal().getPrice()));
        tvMinPrice.setText(Utils.getAttachCommaFormat((int) dicDealInfo.getDeal().getReqPrice()));
        tvMaxPrice.setText(Utils.getAttachCommaFormat((int) dicDealInfo.getDeal().getPrice()));

        int negoPrice = (int) dicDealInfo.getDeal().getReqPrice();
        String str = String.format(activity.getResources().getString(R.string.counternego_text), Utils.getAttachCommaFormat(negoPrice));
        SpannableString nego = new SpannableString(str);
        nego.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.color_666666)),
                0, str.length(), 0);
        nego.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.color_42c2fe)),
                17, 17 + Utils.getAttachCommaFormat(negoPrice).length() + 1, 0);
        ((TextView) popup.findView(R.id.tv_nego_commit_description)).setText(nego);

        CrystalSeekbar seekbar = ((CrystalSeekbar) popup.findView(R.id.seekBar));
        seekbar.setMaxValue(dicDealInfo.getDeal().getPrice());
        seekbar.setMinValue(dicDealInfo.getDeal().getReqPrice());
        ((TextView) popup.findView(R.id.tv_commit_price_value)).setText(Utils.getAttachCommaFormat(dicDealInfo.getDeal().getReqPrice()));

        popup.setOnClickListener(R.id.btn_capture, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                if (cNegoPrice == 0) {
                    Utils.showToast(activity, activity.getString(R.string.empty_proposal_money));
                    return;
                }
                reqDealNegoCounter(dicDealInfo.getDeal().getDealUid(), cNegoPrice, popup);
            }
        });

        popup.setOnClickListener(R.id.btn_detail_nego_popup_close, new PopupDialog.bindOnClick() {
            @Override
            public void onClick() {
                popup.dismiss();
            }
        });

        seekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                cNegoPrice = (long) value.floatValue();
                ((TextView) popup.findView(R.id.tv_commit_price_value)).setText(Utils.getAttachCommaFormat(value.floatValue()));
            }
        });

        popup.show();
    }
}