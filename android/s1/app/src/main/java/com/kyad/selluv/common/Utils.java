package com.kyad.selluv.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.kyad.selluv.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils {

    public static Application sApplication;
    public static Activity currentActivity;

    public static void addImagesAndIndicator(final ViewPager pager, final List<String> imgs, IndicatorView indicator) {

        for (int i = 0; i < imgs.size(); i++) {
            RoundRectCornerImageView iv = new RoundRectCornerImageView(pager.getContext());
            iv.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            iv.setAdjustViewBounds(true);
            ImageUtils.load(pager.getContext(), imgs.get(i), android.R.color.transparent, R.drawable.img_default, iv);
            pager.addView(iv);
        }

        pager.setOffscreenPageLimit(imgs.size());
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return imgs.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup collection, int position) {
                return pager.getChildAt(position);
            }

            @Override
            public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
                collection.removeView((View) view);
            }
        };
        pager.setAdapter(mPagerAdapter);
        indicator.setCount(imgs.size());
        indicator.setPager(pager);
    }

    public static int getScreenWidth(@NonNull Context context) {
        Point size = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    public static int getScreenHeight(@NonNull Context context) {
        Point size = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(size);
        return size.y;
    }

    public static boolean isInLandscapeMode(@NonNull Context context) {
        boolean isLandscape = false;
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isLandscape = true;
        }
        return isLandscape;
    }

    public static void addTouchableImagesAndIndicator(final ViewPager pager, final List<String> urls, IndicatorView indicator) {

        for (int i = 0; i < urls.size(); i++) {
            final TouchImageView iv = new TouchImageView(pager.getContext());
            iv.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            iv.setAdjustViewBounds(true);
            iv.setScaleType(ImageView.ScaleType.FIT_CENTER);

            Glide.with(pager.getContext()).load(urls.get(i)).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                    iv.setImageBitmap(Utils.getRoundedCornerBitmap(((BitmapDrawable) resource).getBitmap(), 5));
                    iv.setImageBitmap(((BitmapDrawable) resource).getBitmap());
                }
            });

            pager.addView(iv);
        }

        pager.setOffscreenPageLimit(urls.size());
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return urls.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == (View) object;
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup collection, int position) {
                return pager.getChildAt(position);
            }

            @Override
            public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
                collection.removeView((View) view);
            }
        };
        pager.setAdapter(mPagerAdapter);
        indicator.setCount(urls.size());
        indicator.setPager(pager);
    }

    public static void heartAnimation(Activity activity, View v, boolean is_like) {
        if (v == null) {
            Log.d("Null Error", "Heart anim's view is null!");
        }

        final int originalPos[] = new int[2];
        v.getLocationOnScreen(originalPos);
        int orgx = originalPos[0];
        int orgy = originalPos[1];

        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screen_height = dm.heightPixels;
        int screen_width = dm.widthPixels;

        int statusbar_height = 0;//getStatusBarHeight();
        int heart_height = dpToPixel(activity, 98) / 3;
        int heart_width = dpToPixel(activity, 114) / 3;
        final ImageView heart = new ImageView(activity);
        heart.setBackgroundResource(is_like ? R.drawable.heart_like : R.drawable.heart_unlike);
        //heart.setAlpha(0.85f);
        RelativeLayout.LayoutParams rl_param = new RelativeLayout.LayoutParams(heart_width, heart_height);
        rl_param.topMargin = orgy - heart_height / 2 + v.getMeasuredHeight() / 2 - statusbar_height;
        rl_param.leftMargin = orgx - heart_width / 2 + v.getMeasuredWidth() / 2;
        heart.setLayoutParams(rl_param);

        RelativeLayout rl_anim;
        if ((rl_anim = (RelativeLayout) activity.findViewById(R.id.rl_animation)) == null) {
            rl_anim = new RelativeLayout(activity);
            rl_anim.setId(R.id.rl_animation);
            RelativeLayout.LayoutParams rl_anim_param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            activity.getWindow().addContentView(rl_anim, rl_anim_param);
        }
        rl_anim.addView(heart);
        heart.setScaleX(0.0f);
        heart.setScaleY(0.0f);

        final int xDest = screen_width / 2 - v.getMeasuredWidth() / 2;
        final int yDest = screen_height / 2;

        TranslateAnimation trans = new TranslateAnimation(0, xDest - originalPos[0], 0, yDest - originalPos[1]);
        trans.setInterpolator(new OvershootInterpolator());

        final RelativeLayout finalRl_anim = rl_anim;
        heart.animate().setDuration(500)
                .scaleX(3.0f)
                .scaleY(3.0f)
                .translationXBy(xDest - originalPos[0])
                .translationYBy(yDest - originalPos[1])
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        heart.animate().setDuration(400)
                                .setInterpolator(new AccelerateDecelerateInterpolator())
                                .scaleX(0.0f)
                                .scaleY(0.0f)
                                .alpha(0.0f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        finalRl_anim.removeView(heart);
                                    }
                                });
                    }
                });
    }

    public static int dpToPixel(Context context, float dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return (int) px;
    }

    public static int pixelToDp(Context context, int pixel) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float dp = pixel / (metrics.densityDpi / 160f);
        return (int) dp;
    }

    public static Bitmap getRoundedSquareBitmap(Bitmap bitmap, int pixels) {
        boolean imgLandscape = bitmap.getWidth() >= bitmap.getHeight();
        int imageWidth = Math.max(bitmap.getWidth(), bitmap.getHeight());
        Bitmap squareBmp = Bitmap.createBitmap(imageWidth, imageWidth, Bitmap.Config.ARGB_8888);
        Canvas squareCanvas = new Canvas(squareBmp);

        final Rect srcImgRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final Rect dstImgRect = new Rect(imgLandscape ? 0 : (imageWidth - bitmap.getWidth()) / 2,
                imgLandscape ? (imageWidth - bitmap.getHeight()) / 2 : 0,
                imgLandscape ? bitmap.getWidth() : (bitmap.getWidth() + imageWidth) / 2,
                imgLandscape ? (bitmap.getHeight() + imageWidth) / 2 : bitmap.getHeight());

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        squareCanvas.drawBitmap(bitmap, srcImgRect, dstImgRect, paint);
        return getRoundedCornerBitmap(squareBmp, pixels);
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap maskedBmp = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Bitmap outputBmp = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas maskCanvas = new Canvas(maskedBmp);
        Canvas outputCanvas = new Canvas(outputBmp);

        final int color = 0xffffffff;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        maskCanvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        maskCanvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        maskCanvas.drawBitmap(bitmap, rect, rect, paint);

        Paint rectPaint = new Paint();
        rectPaint.setColor(0xfff5f5f5);
        rectPaint.setAntiAlias(true);
        outputCanvas.drawRoundRect(rectF, roundPx, roundPx, rectPaint);
        Paint drawpaint = new Paint();
        drawpaint.setAntiAlias(true);
        drawpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        outputCanvas.drawBitmap(maskedBmp, rect, rect, drawpaint);
        maskedBmp.recycle();
        return outputBmp;
    }

    public static String getAttachCommaFormat(float data) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(data);
    }

    public static long getRemoveCommaValue(String commaStr) {
        return Long.valueOf(commaStr.replaceAll(",", ""));
    }


    public static Application getApp() {
        if (sApplication != null) return sApplication;
        throw new NullPointerException("u should init first");
    }

    public static Bitmap takeScreenShot(Activity activity) {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();

        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }

    public static void showToast(Context context, String MESSAGE) {
        Toast.makeText(context, MESSAGE, Toast.LENGTH_SHORT).show();
    }

    public static void setTabItemFontAndSize(Context context, TabLayout tl, int idx, Typeface fontType, float dp, int style) {
        ViewGroup vg = (ViewGroup) tl.getChildAt(0);
        int tabsCount = vg.getChildCount();
        if (idx < tabsCount) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(idx);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(fontType != null ? fontType : ((TextView) tabViewChild).getTypeface(), style != 0 ? style : Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(TypedValue.COMPLEX_UNIT_SP, dp);
                    ((TextView) tabViewChild).setSingleLine(true);
                }
            }
        }
    }

    public static void setViewHeightAsScreen(View v, int diff) {
        DisplayMetrics dm = new DisplayMetrics();
        currentActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        v.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dm.heightPixels - diff));
    }

    public static int str2int(String str) {
        String res = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= '0' && str.charAt(i) <= '9') res += str.charAt(i);
        }
        if (res.isEmpty()) return 0;
        return Integer.parseInt(res);
    }

    public static String getDiffTime(long time) {
        String diffString = "";
        Date nowDate = new Date();
        long now = nowDate.getTime();
        long timediff = (now - time) / 1000;
        if (timediff < 60)
            diffString = "방금전";
        else if (timediff < 3600)
            diffString = String.format("%d분전", (int) timediff / 60);
        else if (timediff < 3600 * 24)
            diffString = String.format("%d시간전", (int) timediff / 3600);
        else if (timediff < 3600 * 24 * 7)
            diffString = String.format("%d일전", (int) timediff / (3600 * 24));
        else {
            Date itemDate = new Date(time);
            if (nowDate.getYear() == itemDate.getYear()) {
                SimpleDateFormat format = new SimpleDateFormat("MM월 dd일");
                diffString = format.format(itemDate);
            } else {
                SimpleDateFormat format = new SimpleDateFormat("yyyy년 MM월 dd일");
                diffString = format.format(itemDate);
            }
        }
        return diffString;
    }

    public static String getDiffTime1(long time) {
        String diffString = "";
        Date nowDate = new Date();
        long now = nowDate.getTime();
        long timediff = (now - time) / 1000;
        if (timediff < 60)
            diffString = "방금전";
        else if (timediff < 3600)
            diffString = String.format("%d분", (int) timediff / 60);
        else if (timediff < 3600 * 24)
            diffString = String.format("%d시간", (int) timediff / 3600);
        else if (timediff < 3600 * 24 * 7)
            diffString = String.format("%d일", (int) timediff / (3600 * 24));
        else {
            Date itemDate = new Date(time);
            if (nowDate.getYear() == itemDate.getYear()) {
                SimpleDateFormat format = new SimpleDateFormat("MM월 dd일");
                diffString = format.format(itemDate);
            } else {
                SimpleDateFormat format = new SimpleDateFormat("yyyy년 MM월 dd일");
                diffString = format.format(itemDate);
            }
        }
        return diffString;
    }

    public static int getDiffTime2(long time) {
        int diffTime = 0;
        Date nowDate = new Date();
        long now = nowDate.getTime();
        long timediff = (now - time) / 1000;

        diffTime = (int) timediff / 3600;
        return diffTime;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void hideKeypad(final Activity activity, View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideSoftKeyboard(activity);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeypad(activity, innerView);
            }
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    // get 2018.05.04 from timestamp
    public static String getStringDate(long time, String format) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format(format, cal).toString();
        return date;
    }

    //convert 1111-****-2222-**** from 1111-2222-3333-444
    public static String convertCardNum(String num) {
        String result = "";

        String[] separated = num.split("-");

        result = separated[0].trim() + "-" + "****" + "-" + separated[2].trim() + "-" + "****";

        return result;

    }

    //convert 000-2222-3333 from 00022223333
    public static String convertPhoneNum(String num) {
        String result = "";
        String Phonenum = num.trim();

        result = Phonenum.substring(0, 3) + "-" + Phonenum.substring(3, 7) + "-" + Phonenum.substring(7, 11);

        return result;

    }
}