package com.kyad.selluv.shop;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.WindowManager;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;

public class ShopCategorySelectActivity extends BaseActivity {

    private ShopCategorySelectFragment categorySelectFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_category_select);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.color_000000_30));

        loadFragment();
    }

    private void loadFragment() {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        categorySelectFragment = ShopCategorySelectFragment.newInstance();
        transaction.replace(R.id.fl_fragment, categorySelectFragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (categorySelectFragment != null)
            categorySelectFragment.onBack(null);
    }

    public void closeResultCancelActivity() {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void closeResultOkActivity() {
        setResult(RESULT_OK);
        finish();
    }
}
