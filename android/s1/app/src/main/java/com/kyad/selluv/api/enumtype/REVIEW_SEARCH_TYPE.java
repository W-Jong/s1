package com.kyad.selluv.api.enumtype;

public enum REVIEW_SEARCH_TYPE {
    ALL(3), GOOD(2), NORMAL(1), BAD(0);

    private int code;

    private REVIEW_SEARCH_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
