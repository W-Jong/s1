package com.kyad.selluv.brand;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.BrandAddListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.api.request.MultiUidsReq;
import com.kyad.selluv.common.AlphabetIndexer.widget.IndexableListView;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.ghyeok.stickyswitch.widget.StickySwitch;
import retrofit2.Call;

import static android.widget.RelativeLayout.CENTER_IN_PARENT;

public class BrandAddActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.rl_hint)
    RelativeLayout rl_hint;
    @BindView(R.id.edt_search)
    EditText edt_search;
    @BindView(R.id.lv_filter)
    IndexableListView lv_filter;
    @BindView(R.id.iv_search)
    ImageView iv_search;
    @BindView(R.id.tv_hint)
    TextView tv_hint;
    @BindView(R.id.ib_clear)
    ImageButton ib_clear;
    @BindView(R.id.tv_cancel)
    TextView tv_cancel;
    @BindView(R.id.tv_right)
    TextView tv_right;

    //Variables
    int selectedLang = 0;   //0 : eng 1: kor

    BrandAddListAdapter adapter = null;
    String filterText = "";

    private List<BrandListDto> brandListDtoList;

    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        finish();
    }

    @OnClick(R.id.btn_ok)
    void onOK(View v) {
        saveBrand();
    }

    @OnClick(R.id.ib_clear)
    void onClear(View v) {
        edt_search.setText("");
    }

    @OnClick(R.id.tv_cancel)
    void onCancel(View v) {
        edt_search.setText("");
        edt_search.clearFocus();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_add);
        loadLayout();
        getAllDetailBrandList();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        edt_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    tv_hint.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) rl_hint.getLayoutParams();
                    rlp.removeRule(CENTER_IN_PARENT);
                    tv_cancel.setVisibility(View.VISIBLE);
                    ib_clear.setVisibility(View.VISIBLE);
                } else {
                    CharSequence charSequence = edt_search.getText();
                    if (charSequence.length() == 0) {
                        tv_hint.setVisibility(View.VISIBLE);
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) rl_hint.getLayoutParams();
                        rlp.addRule(CENTER_IN_PARENT);
                        tv_cancel.setVisibility(View.GONE);
                        ib_clear.setVisibility(View.GONE);
                    } else {
                        tv_hint.setVisibility(View.GONE);
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) rl_hint.getLayoutParams();
                        rlp.removeRule(CENTER_IN_PARENT);
                        tv_cancel.setVisibility(View.VISIBLE);
                        ib_clear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filterText = editable.toString();
                filterBrand();
            }
        });

        StickySwitch stickySwitch = (StickySwitch) findViewById(R.id.sw_lang);
        stickySwitch.onSelectedChangeListener = new StickySwitch.OnSelectedChangeListener() {
            @Override
            public void onSelectedChange(StickySwitch.Direction direction, String rightTextAlpha) {
                if (direction == StickySwitch.Direction.LEFT)
                    selectedLang = 0;
                else selectedLang = 1;
                getAllDetailBrandList();
            }
        };

        tv_right.setVisibility(View.INVISIBLE);
        adapter = new BrandAddListAdapter(this, selectedLang);
        lv_filter.setAdapter(adapter);
    }

    void saveBrand() {
        if (adapter.modifiedIndex.size() < 1) {
            setResult(RESULT_OK);
            finish();
            return;
        }

        List<Integer> likesBrandList = new ArrayList<>();
        List<Integer> unLikesBrandList = new ArrayList<>();

        for (Integer brandUid : adapter.modifiedIndex.keySet()) {
            if (adapter.modifiedIndex.get(brandUid)) {
                likesBrandList.add(brandUid);
            } else {
                unLikesBrandList.add(brandUid);
            }
        }

        likeBrandList(likesBrandList, unLikesBrandList, true);
    }

    void filterBrand() {
        if ("".equals(filterText) || filterText == null) {
            getAllDetailBrandList();
            return;
        } else {
            List<BrandListDto> filteredBrandListDtoList = new ArrayList<>();
            for (BrandListDto item : brandListDtoList) {

                if (item.getNameEn().toUpperCase().contains(filterText.toUpperCase()) || item.getNameKo().contains(filterText))
                    filteredBrandListDtoList.add(item);
            }

            updateLang(filteredBrandListDtoList);
        }
    }

    void updateLang(List<BrandListDto> filteredList) {
        adapter = new BrandAddListAdapter(this, selectedLang);
        adapter.setData(filteredList);
        lv_filter.setAdapter(adapter);
    }

    private void getAllDetailBrandList() {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<BrandListDto>>> genRes = restAPI.brandAllDetailList(Globals.userToken, selectedLang + 1);

        genRes.enqueue(new TokenCallback<List<BrandListDto>>(this) {
            @Override
            public void onSuccess(List<BrandListDto> response) {
                closeLoading();
                brandListDtoList = response;
                updateLang(brandListDtoList);
//                requestComplete();
//                showHasMore();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    private void likeBrandList(final List<Integer> likeBrandUidList, final List<Integer> unLikeBrandUidList, final boolean isLike) {
        if (isLike && likeBrandUidList.size() < 1) {
            likeBrandList(likeBrandUidList, unLikeBrandUidList, false);
            return;
        }

        if (!isLike && unLikeBrandUidList.size() < 1) {
            setResult(RESULT_OK);
            finish();
            return;
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();
        Call<GenericResponse<Void>> genRes;
        if (isLike) {
            MultiUidsReq multiUidsReq = new MultiUidsReq(likeBrandUidList);
            genRes = restAPI.brandLikes(Globals.userToken, multiUidsReq);
        } else {
            MultiUidsReq multiUidsReq = new MultiUidsReq(unLikeBrandUidList);
            genRes = restAPI.brandUnLikes(Globals.userToken, multiUidsReq);
        }

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();
                if (isLike) {
                    likeBrandList(likeBrandUidList, unLikeBrandUidList, false);
                } else {
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
