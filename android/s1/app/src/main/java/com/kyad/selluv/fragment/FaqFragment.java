/**
 * 홈
 */
package com.kyad.selluv.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.BaseFragment;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.FaqListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.FaqDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.model.Model;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class FaqFragment extends BaseFragment {

    public interface DataLoadInterface {
        Model.FaqItem[] loadData();
    }

    public FaqFragment() {
    }

    public void setTab(int index){

        if (index == 0){
            faqType = "SELL";
        } else if (index == 1){
            faqType = "BUY";
        } else {
            faqType = "ACTIVITY";
        }

        getFaqList();
    }

    //UI Referencs

    @BindView(R.id.lv_list)
    ListView lv_list;


    //Vars
    View rootView;
    private FaqListAdapter adapter;
    private DataLoadInterface loadInterface = null;

    public String faqType = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_alarm, container, false);
        ButterKnife.bind(this, rootView);
        adapter = new FaqListAdapter((BaseActivity) getActivity());
        loadLayout();
        loadData();
        return rootView;

    }

    private void loadLayout() {
        lv_list.setAdapter(adapter);
    }

    private void loadData() {
        if (adapter != null) {
//            Collections.addAll(adapter.arrData, loadInterface.loadData());
            adapter.notifyDataSetChanged();
        }

    }

    public void setLoadInterface(DataLoadInterface loadInterface) {
        this.loadInterface = loadInterface;
    }

    //get Faq list
    private void getFaqList(){
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<FaqDao>>> genRes = restAPI.otherFaq(Globals.userToken, faqType);

        genRes.enqueue(new TokenCallback<List<FaqDao>>(getActivity()) {
            @Override
            public void onSuccess(List<FaqDao> response) {
                closeLoading();

                adapter.arrData = response;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }
}
