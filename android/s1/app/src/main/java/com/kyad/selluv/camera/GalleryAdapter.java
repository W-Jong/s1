package com.kyad.selluv.camera;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;


import com.kyad.selluv.R;
import com.kyad.selluv.common.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by etiennelawlor on 8/20/15.
 */
public class GalleryAdapter extends BaseAdapter {

    // region Member Variables
    private List<String> images;
    public ArrayList<String> selectedImages = new ArrayList<>();

    private int gridItemWidth;
    GalleryFragment fragment;

    // region Constructors
    public GalleryAdapter(GalleryFragment fragment, List<String> images) {
        this.fragment = fragment;
        this.images = images;

        int screenWidth = Utils.getScreenWidth(fragment.getContext());
        int numOfColumns;
        if (Utils.isInLandscapeMode(fragment.getContext())) {
            numOfColumns = 4;
        } else {
            numOfColumns = 3;
        }

        gridItemWidth = screenWidth / numOfColumns;
    }
    // endregion

    @Override
    public int getCount() {
        if (images != null) {
            return images.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (images != null)
            return images.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(fragment.getContext()).inflate(R.layout.item_gallery, parent, false);
        convertView.setMinimumHeight(gridItemWidth);
        final String image = images.get(position);

        final ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_img);
        final ImageButton selectButton = (ImageButton) convertView.findViewById(R.id.ib_select);
        if (!TextUtils.isEmpty(image)) {
            Picasso.with(fragment.getContext())
                    .load("file://" + image)
                    .resize(gridItemWidth, gridItemWidth)
                    .centerCrop()
                    .into(imageView);
        } else {
            imageView.setImageDrawable(null);
        }

        if (fragment.fromPage == SellPicActivity.FROM_PAGE_MYPAGE) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PicCropFragment picCropFragment = new PicCropFragment();
                    picCropFragment.path = image;
                    picCropFragment.title = "";
                    picCropFragment.parent = fragment;
                    picCropFragment.imageSelectInterface = fragment.imageSelectInterface;
                    picCropFragment.fromMypage = true;
                    FragmentTransaction ft = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                    picCropFragment.show(ft, "picCropFragment");
                }
            });
        } else {
            final boolean checked = selectedImages.contains(image);
            if (!checked) {
                selectButton.setVisibility(View.INVISIBLE);
                imageView.setColorFilter(Color.TRANSPARENT);
            } else {
                selectButton.setVisibility(View.VISIBLE);
                imageView.setColorFilter(fragment.getContext().getResources().getColor(R.color.color_000000_30), PorterDuff.Mode.SRC_OVER);
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checked) {
                        selectedImages.remove(image);
                        notifyDataSetChanged();
                    } else {
                        selectedImages.add(image);
                        notifyDataSetChanged();
                    }
                }
            });
        }
        return convertView;
    }

    public void setData(List<String> images) {
        this.images = images;
    }

}
