/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.category.SizeRefDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.detail.WareEditActivity;
import com.kyad.selluv.sell.SellInfoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SellDirectPickerFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.np_size)
    com.shawnlin.numberpicker.NumberPicker np_size;
    @BindView(R.id.pk_std)
    com.shawnlin.numberpicker.NumberPicker pk_std;

    //Vars
    private View rootView;
    private int selectFist = 0;
    private int selectSecond = 0;

    private SizeRefDto sizeRefDto;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_direct_info_picker, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick(R.id.btn_cancel)
    void onCancel(View v) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_select)
    void onSelect(View v) {
        String s_size;
        if (Globals.isEditing) {
            s_size = sizeRefDto.getSizeFirst().get(selectFist) + " " + sizeRefDto.getSizeSecond().get(selectFist).get(selectSecond);
            Globals.pdtUpdateReq.setPdtSize(s_size);
        } else {
            s_size = sizeRefDto.getSizeFirst().get(selectFist) + " " + sizeRefDto.getSizeSecond().get(selectFist).get(selectSecond);
            Globals.pdtCreateReq.setPdtSize(s_size);
        }

        TextView tvSize = getActivity().findViewById(R.id.tv_item_size_value);
        tvSize.setText(s_size);

        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_guide)
    void onGuide(View v) {
        Intent intent = new Intent(getActivity(), GuidImagePopup.class);
        int imgID;
        if (Globals.currentShopperTypeIdx == 0)
            imgID = R.drawable.size_guide_man;
        else if (Globals.currentShopperTypeIdx == 1) {
            imgID = R.drawable.size_guide_woman;
        } else {
            if (Globals.categoryIndex[0] == 1)
                imgID = R.drawable.size_guide_baby;
            else if (Globals.categoryIndex[0] == 2 || Globals.categoryIndex[0] == 3)
                imgID = R.drawable.size_guide_kids;
            else
                imgID = R.drawable.size_guide_teen;
        }
        intent.putExtra(Constants.POPUP_IMAGE, imgID);
        startActivity(intent);
    }

    private void loadLayout() {
        if (Globals.isEditing) {
            sizeRefDto = ((WareEditActivity) getActivity()).sizeRefDto;
        } else {
            sizeRefDto = ((SellInfoActivity) getActivity()).sizeRefDto;
        }

        selectFist = 0;
        pk_std.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        pk_std.setMaxValue(sizeRefDto.getSizeFirst().size() - 1);
        pk_std.setMinValue(0);
        pk_std.setValue(0);
        pk_std.setDisplayedValues(sizeRefDto.getSizeFirst());
        pk_std.setOnValueChangedListener(new com.shawnlin.numberpicker.NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(com.shawnlin.numberpicker.NumberPicker picker, int oldVal, int newVal) {
                selectFist = newVal;
                np_size.setDisplayedValues(sizeRefDto.getSizeSecond().get(selectFist));
            }
        });

        selectSecond = 0;
        np_size.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np_size.setMaxValue(sizeRefDto.getSizeSecond().get(0).size() - 1);
        np_size.setMinValue(0);
        np_size.setValue(0);
        np_size.setDisplayedValues(sizeRefDto.getSizeSecond().get(selectFist));
        np_size.setOnValueChangedListener(new com.shawnlin.numberpicker.NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(com.shawnlin.numberpicker.NumberPicker picker, int oldVal, int newVal) {
                selectSecond = newVal;
            }
        });

        rootView.findViewById(R.id.frag_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

}
