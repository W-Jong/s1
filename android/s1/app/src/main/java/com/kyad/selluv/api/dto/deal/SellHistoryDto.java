package com.kyad.selluv.api.dto.deal;

import android.graphics.pdf.PdfDocument;

import com.kyad.selluv.api.dto.pdt.Page;

import java.util.List;

import lombok.Data;

@Data
public class SellHistoryDto {
    //("네고제안수")
    private long negoCount;
    //("정산완료수")
    private long cachedCount;
    //("발송대기수")
    private long preparedCount;
    //("배송진행수")
    private long progressCount;
    //("배송완료수")
    private long sentCount;
    //("거래완료수")
    private long completedCount;
    //("판매내역")
    private Page<DealListDto> history;//kkk - Page
}
