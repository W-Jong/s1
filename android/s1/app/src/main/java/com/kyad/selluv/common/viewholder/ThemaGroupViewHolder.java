package com.kyad.selluv.common.viewholder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.expandablerecyclerview.viewholders.GroupViewHolder;

public class ThemaGroupViewHolder extends GroupViewHolder {

    public TextView tv_thema_title;
    public TextView tv_item_cnt;
    public ImageButton ib_thema_more;
    public ImageButton ib_thema_close;
    public ImageView iv_thema;

    public ThemaGroupViewHolder(View itemView) {
        super(itemView);
        tv_thema_title = (TextView) itemView.findViewById(R.id.tv_thema_title);
        tv_item_cnt = (TextView) itemView.findViewById(R.id.tv_item_cnt);
        ib_thema_more = (ImageButton) itemView.findViewById(R.id.ib_thema_more);
        ib_thema_close = (ImageButton) itemView.findViewById(R.id.ib_thema_close);
        iv_thema = (ImageView) itemView.findViewById(R.id.iv_thema);
    }
}
