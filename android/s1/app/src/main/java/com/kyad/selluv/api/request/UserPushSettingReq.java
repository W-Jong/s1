package com.kyad.selluv.api.request;


import android.support.annotation.Size;

import lombok.Data;


@Data
public class UserPushSettingReq {

    //@ApiModelProperty("거래알림 사용여부 1-사용, 0-사용안함")
    private int dealYn = 1;
    //@ApiModelProperty("소식알림 사용여부 1-사용, 0-사용안함")
    private int newsYn = 1;
    //@ApiModelProperty("댓글알림 사용여부 1-사용, 0-사용안함")
    private int replyYn = 1;
    @Size(min = 1)
    //@Pattern(value = "^[01]{10}$")
    //@ApiModelProperty("거래알림 - 10자리 숫자배열(1-사용 0-사용안함)")
    private String dealSetting = "1111111111";
    @Size(min = 1)
    //@Pattern(value = "^[01]{12}$")
    //@ApiModelProperty("소식알림 - 12자리 숫자배열(1-사용 0-사용안함)")
    private String newsSetting = "111111111111";
    @Size(min = 1)
    //@Pattern(value = "^[01]{2}$")
    //@ApiModelProperty("댓글알림 - 2자리 숫자배열(1-사용 0-사용안함)")
    private String replySetting = "11";
}
