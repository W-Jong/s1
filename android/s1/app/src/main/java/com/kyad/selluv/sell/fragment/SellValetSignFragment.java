/**
 * 홈
 */
package com.kyad.selluv.sell.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.ValetCreateReq;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.sell.SellPolicyActivity;
import com.kyad.selluv.sell.SellValetCompleteActivity;
import com.kyad.selluv.sell.SellValetRequestActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


public class SellValetSignFragment extends DialogFragment {

    //UI Refs
    @BindView(R.id.sign_pad)
    SignaturePad mSignaturePad;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.tv_ware_name_ko)
    TextView tv_ware_name_ko;

    //Vars
    private View rootView;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    String signImageName = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sell_valet_sign, container, false);
        ButterKnife.bind(this, rootView);

        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick(R.id.tv_sell_policy)
    void onPolicy(View v) {
        ((BaseActivity) getActivity()).startActivity(SellPolicyActivity.class, false, 0, 0);
    }

    @OnClick(R.id.btn_close)
    void onClose(View v) {
        this.dismiss();
    }

    @OnClick(R.id.ll_nego_background)
    void onBackClick(View v) {
        this.dismiss();
    }

    @OnClick(R.id.btn_ok)
    void btnOk(View v) {
        Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
        if (!addJpgSignatureToGallery(signatureBitmap)) {
            Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
            return;
        }
//        if (!addSvgSignatureToGallery(mSignaturePad.getSignatureSvg())) {
//            Toast.makeText(getActivity(), "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
//        }

        uploadFiles();
    }

    private void loadLayout() {
        verifyStoragePermissions(getActivity());
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                mSignaturePad.setBackgroundResource(R.drawable.sell_direct_signing);
            }

            @Override
            public void onSigned() {
                btn_ok.setBackgroundColor(Color.BLACK);
                btn_ok.setEnabled(true);
            }

            @Override
            public void onClear() {
                btn_ok.setBackgroundColor(getResources().getColor(R.color.color_999999));
                btn_ok.setEnabled(false);
            }
        });

        String detail_info = Globals.selectedBrand.getNameEn() + " " + Constants.shopper_type[Globals.currentShopperTypeIdx];
        // + 상세카테고리
        if (Globals.getSelectedCategory().getCategoryName().length() > 0)
            detail_info = detail_info + " " + Globals.getSelectedCategory().getCategoryName();

        tv_ware_name_ko.setText(detail_info);
    }


    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    private void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            String temp = String.format("Signature_%d.jpg", System.currentTimeMillis());
            File photo = new File(getAlbumStorageDir("SignaturePad"), temp);
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            signImageName = photo.getPath();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

//    public boolean addSvgSignatureToGallery(String signatureSvg) {
//        boolean result = false;
//        try {
//            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
//            OutputStream stream = new FileOutputStream(svgFile);
//            OutputStreamWriter writer = new OutputStreamWriter(stream);
//            writer.write(signatureSvg);
//            writer.close();
//            stream.flush();
//            stream.close();
//            scanMediaFile(svgFile);
//            result = true;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }

    /**
     * Checks if the app has permission to write to device storage
     * <p/>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity the activity from which permissions are checked
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void cmdRegService() {

        final ValetCreateReq createReq = ((SellValetRequestActivity) getActivity()).getValetCreateReq(signImageName);
        createReq.setBrandUid(Globals.selectedBrand.getBrandUid());
        createReq.setCategoryUid(Globals.getSelectedCategory().getCategoryUid());

        ((BaseActivity) getActivity()).showLoading();

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.registerValet(Globals.userToken, createReq);
        genRes.enqueue(new TokenCallback<Void>(getActivity()) {
            @Override
            public void onSuccess(Void response) {
                ((BaseActivity) getActivity()).closeLoading();

                Intent intent = new Intent(getActivity(), SellValetCompleteActivity.class);
                intent.putExtra(Constants.NAME, ((TextView) getActivity().findViewById(R.id.edt_name)).getText().toString());
                intent.putExtra(Constants.ADDRESS, ((TextView) getActivity().findViewById(R.id.edt_address)).getText().toString());
                intent.putExtra(Constants.CONTACT, ((TextView) getActivity().findViewById(R.id.edt_contact)).getText().toString());
                intent.putExtra(Constants.DESIRED_PRICE, createReq.getReqPrice());
                intent.putExtra(Constants.SEND_METHOD, ((SellValetRequestActivity) getActivity()).sendMethod);
                intent.putExtra(Constants.VALETE_COMPLETE_TITLE, tv_ware_name_ko.getText().toString());
                getActivity().startActivity(intent);
                getActivity().finish();

            }

            @Override
            public void onFailed(Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
            }
        });
    }

    private void uploadFiles() {
        ((BaseActivity) getActivity()).showLoading();

        File file = new File(signImageName);
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("uploadfile", file.getName(), surveyBody);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<HashMap<String, String>>> genRes = restAPI.uploadImage(multipartBody);
        genRes.enqueue(new TokenCallback<HashMap<String, String>>(getActivity()) {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                ((BaseActivity) getActivity()).closeLoading();
                //fileName, fileURL
                signImageName = response.get("fileName");
                cmdRegService();
            }

            @Override
            public void onFailed(Throwable t) {
                ((BaseActivity) getActivity()).closeLoading();
            }
        });
    }

}
