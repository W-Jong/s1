package com.kyad.selluv.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.DbManager;
import com.kyad.selluv.R;
import com.kyad.selluv.ShareManager;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.request.UserProfileReq;
import com.kyad.selluv.camera.GalleryFragment;
import com.kyad.selluv.camera.SellPicActivity;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.DockAnimation;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.LoginStartActivity;
import com.kyad.selluv.model.Model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.kyad.selluv.ShareManager.SM_AUTO_LOGIN;
import static com.kyad.selluv.ShareManager.SM_LAST_LOGIN_ID;

public class MypageProfileActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.ib_nickname_cancel)
    ImageButton ib_nickname_cancel;
    @BindView(R.id.rl_introduce_edt)
    RelativeLayout rl_introduce_edt;
    @BindView(R.id.ib_male)
    ImageButton ib_male;
    @BindView(R.id.ib_female)
    ImageButton ib_female;
    @BindView(R.id.ib_kids)
    ImageButton ib_kids;
    @BindView(R.id.btn_drop)
    Button btn_drop;
    @BindView(R.id.edt_introduce)
    EditText edt_introduce;
    @BindView(R.id.edt_nickname)
    EditText edt_nickname;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.iv_profile_img)
    CircularImageView iv_profile_img;
    @BindView(R.id.iv_profile)
    ImageView iv_profile;
    @BindView(R.id.tv_id_show)
    TextView tv_id_show;
    @BindView(R.id.tv_sex_show)
    TextView tv_sex_show;

    private boolean bProfile = true;
    private String userProfile = "";
    private String userProfileBg = "";

    @OnClick(R.id.ib_left)
    void onLeft() {
        updateUserInfo();
    }

    @OnClick({R.id.iv_profile, R.id.btn_wallpaper})
    void onWallpaper() {
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.fromPage = SellPicActivity.FROM_PAGE_MYPAGE;
        galleryFragment.frameType = 2;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        galleryFragment.imageSelectInterface = new GalleryFragment.ImageSelectInterface() {
            @Override
            public void onImageSelect(Bitmap bmp) {

                bProfile = true;
                addJpgSignatureToGallery(bmp);
                iv_profile.setImageBitmap(bmp);
            }
        };
        galleryFragment.show(ft, "GalleryFragment");
    }

    @OnClick(R.id.btn_drop)
    void onDrop() {
        showDetailInput(dropped);
    }

    @OnClick(R.id.ib_male)
    void onMale() {
        maleSelected = !maleSelected;
        if (maleSelected) {
            ib_male.setBackgroundResource(R.drawable.mypage_profile_waregroup_man_on);
        } else {
            ib_male.setBackgroundResource(R.drawable.mypage_profile_waregroup_man_off);
        }
    }

    @OnClick(R.id.ib_female)
    void onFemale() {
        femaleSelected = !femaleSelected;
        if (femaleSelected) {
            ib_female.setBackgroundResource(R.drawable.mypage_profile_waregroup_woman_on);
        } else {
            ib_female.setBackgroundResource(R.drawable.mypage_profile_waregroup_woman_off);
        }
    }

    @OnClick(R.id.ib_kids)
    void onKids() {
        kidsSelected = !kidsSelected;
        if (kidsSelected) {
            ib_kids.setBackgroundResource(R.drawable.mypage_profile_waregroup_kids_on);
        } else {
            ib_kids.setBackgroundResource(R.drawable.mypage_profile_waregroup_kids_off);
        }
    }

    @OnClick(R.id.btn_logout)
    void onLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alarm))
                .setMessage(getString(R.string.userlogout_alert))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        ShareManager.manager().put(SM_AUTO_LOGIN, false);

                        Intent intent = new Intent(MypageProfileActivity.this, LoginStartActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    @OnClick(R.id.ib_nickname_cancel)
    void onNicknameCancel() {
        edt_nickname.setText("");
    }

    @OnClick({R.id.iv_profile_img, R.id.btn_profile_img})
    void onProfileImg() {
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.fromPage = SellPicActivity.FROM_PAGE_MYPAGE;
        galleryFragment.frameType = 1;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        galleryFragment.imageSelectInterface = new GalleryFragment.ImageSelectInterface() {
            @Override
            public void onImageSelect(Bitmap bmp) {
                bProfile = false;
                addJpgSignatureToGallery(bmp);
                iv_profile_img.setImageBitmap(bmp);
            }
        };
        galleryFragment.show(ft, "GalleryFragment");
    }

    //Variables
    boolean maleSelected;
    boolean femaleSelected;
    boolean kidsSelected;
    boolean dropped;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_profile);
        setStatusBarWhite();
        loadLayout();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));
        edt_nickname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ib_nickname_cancel.setVisibility(View.VISIBLE);
                } else
                    ib_nickname_cancel.setVisibility(View.GONE);
                ib_nickname_cancel.setVisibility(View.GONE);
            }
        });
        edt_introduce.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String len = String.valueOf(edt_introduce.getText().length());
                SpannableString statusText = new SpannableString(len + "/120");
                statusText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 0, len.length(), 0);
                tv_status.setText(statusText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        String len = edt_introduce.getText().toString();
        SpannableString statusText = new SpannableString(String.valueOf(len.length()) + "/120");
        statusText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 0, len.length(), 0);
        tv_status.setText(statusText);

        setUserData();
    }

    private void setUserData() {
        ImageUtils.load(iv_profile_img.getContext(), Globals.myInfo.getUsr().getProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_profile_img);
        ImageUtils.load(iv_profile.getContext(), Globals.myInfo.getUsr().getProfileBackImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_profile);

        edt_nickname.setText(Globals.myInfo.getUsr().getUsrNckNm());
        tv_id_show.setText(Globals.myInfo.getUsr().getUsrId());
        edt_introduce.setText(Globals.myInfo.getUsr().getDescription());
        if (Globals.myInfo.getUsr().getDescription().equals("")) {
            btn_drop.setText(R.string.input);
        } else {
            btn_drop.setText(R.string.modify);
        }

        if (Globals.myInfo.getUsr().getGender() == 1) {
            tv_sex_show.setText(getString(R.string.male));
        } else {
            tv_sex_show.setText(getString(R.string.female));
        }

        if ((Globals.myInfo.getUsr().getLikeGroup() & 1) == 0) {
            maleSelected = false;
            ib_male.setBackgroundResource(R.drawable.mypage_profile_waregroup_man_off);
        } else {
            maleSelected = true;
            ib_male.setBackgroundResource(R.drawable.mypage_profile_waregroup_man_on);
        }

        if ((Globals.myInfo.getUsr().getLikeGroup() & 2) == 0) {
            femaleSelected = false;
            ib_female.setBackgroundResource(R.drawable.mypage_profile_waregroup_woman_off);
        } else {
            femaleSelected = true;
            ib_female.setBackgroundResource(R.drawable.mypage_profile_waregroup_woman_on);
        }

        if ((Globals.myInfo.getUsr().getLikeGroup() & 4) == 0) {
            kidsSelected = false;
            ib_kids.setBackgroundResource(R.drawable.mypage_profile_waregroup_kids_off);
        } else {
            kidsSelected = true;
            ib_kids.setBackgroundResource(R.drawable.mypage_profile_waregroup_kids_on);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        updateUserInfo();
    }

    void showDetailInput(boolean isVisible) {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_introduce_edt, 300,
                DockAnimation.FRAME_LAYOUT);

        rl_introduce_edt.setVisibility(View.VISIBLE);

        int from = !isVisible ? 0 : Utils.dpToPixel(getApplicationContext(), 180);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(getApplicationContext(), 180) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dropped = !dropped;
                if (dropped) {
                    btn_drop.setText(R.string.close);
                } else {
                    if (edt_introduce.length() == 0)
                        btn_drop.setText(R.string.input);
                    else
                        btn_drop.setText(R.string.modify);
                }
            }
        });
        m_aniSlideShow.start();
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    private void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            String temp = String.format("Signature_%d.jpg", System.currentTimeMillis());
            File photo = new File(getAlbumStorageDir("SignaturePad"), temp);
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            uploadFile(photo.getPath());
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    //file upload
    private void uploadFile(String filename) {
        showLoading();

        File file = new File(filename);
        RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("uploadfile", file.getName(), surveyBody);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<HashMap<String, String>>> genRes = restAPI.uploadImage(multipartBody);
        genRes.enqueue(new TokenCallback<HashMap<String, String>>(this) {
            @Override
            public void onSuccess(HashMap<String, String> response) {
                closeLoading();
                //fileName, fileURL
                String imgFileName = response.get("fileName");

                if (bProfile) {
                    userProfile = imgFileName;
                } else {
                    userProfileBg = imgFileName;
                }
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }

    //프로필 정보 업데이트
    private void updateUserInfo() {

        String nickname = edt_nickname.getText().toString();
        if (nickname.equals("")) {
            Utils.showToast(this, getString(R.string.empty_nickname_alert));
            return;
        }

        if (!maleSelected && !femaleSelected && !kidsSelected) {
            Utils.showToast(this, getString(R.string.select_kind));
            return;
        }

        String intro = edt_introduce.getText().toString();

        UserProfileReq dicInfo = new UserProfileReq();
        dicInfo.setDescription(intro);
        dicInfo.setIsChildren(kidsSelected);
        dicInfo.setIsFemale(femaleSelected);
        dicInfo.setIsMale(maleSelected);
        dicInfo.setProfileBackImg(userProfileBg);
        dicInfo.setProfileImg(userProfile);
        dicInfo.setUsrNckNm(nickname);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse<Void>> genRes = restAPI.updateUserInfo(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {

                finish();
            }

            @Override
            public void onFailed(Throwable t) {
            }
        });
    }
}
