package com.kyad.selluv.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dao.UsrCardDao;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.mypage.CardManageActivity;
import com.kyad.selluv.mypage.WishAlarmActivity;

import java.util.ArrayList;
import java.util.List;

public class CardListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrCardDao> arrData = new ArrayList<>();
    BaseActivity activity = null;
    public int selectedPos = 0;
    public int nCurCardUid = -1;

    public CardListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrCardDao> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_card, parent, false);

            final UsrCardDao anItem = (UsrCardDao) getItem(position);
            if (anItem == null) {
                return convertView;
            }
            ImageButton ib_radio = (ImageButton) convertView.findViewById(R.id.ib_radio);
            ImageButton ib_close = (ImageButton) convertView.findViewById(R.id.ib_close);
            TextView tv_number = convertView.findViewById(R.id.tv_number);
            TextView tv_company = convertView.findViewById(R.id.tv_company);
            tv_number.setText(Utils.convertCardNum(anItem.getCardNumber()));
            tv_company.setText(anItem.getCardName());
            if (nCurCardUid == anItem.getUsrCardUid()) {
                ib_radio.setBackgroundResource(R.drawable.list_roundbox_on);
            } else {
                ib_radio.setBackgroundResource(R.drawable.list_roundbox_off);
            }
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectedPos = position;
//                    notifyDataSetChanged();
//                }
//            });
            ib_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (activity != null && activity instanceof CardManageActivity) {
                        ((CardManageActivity) activity).deleteCard(anItem.getUsrCardUid());
                    }
                }
            });

//            if (((CardManageActivity) activity).isCardSelect) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ((CardManageActivity) activity).finishWithResult(position);
                    if (activity != null && activity instanceof CardManageActivity) {
                        ((CardManageActivity) activity).selectCard(anItem);
                    }
                }
            });
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}