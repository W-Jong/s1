package com.kyad.selluv.sell;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.DockAnimation;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.sell.fragment.SellDirectInfoAddAcc;
import com.kyad.selluv.sell.fragment.SellDirectInfoAddColor;
import com.kyad.selluv.sell.fragment.SellModelFragment;

import butterknife.BindView;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.ACT_PDT_EDITED;

public class SellInfoAddActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.tv_status)
    TextView tv_status;//상세설명 힌트
    @BindView(R.id.edt_comment)
    EditText edt_comment;//상세설명
    @BindView(R.id.rl_introduce_edt)
    RelativeLayout rl_introduce_edt;
    @BindView(R.id.btn_drop_switch)
    Button btn_drop_switch;//입력-수정-닫기 버튼
    @BindView(R.id.tv_belonging_input)
    TextView tv_belonging_input;//부속품
    @BindView(R.id.tv_color_inputs)
    TextView tv_color_inputs;//컬러
    @BindView(R.id.tv_model_input)
    TextView tv_model_input;//모델명
    @BindView(R.id.frag_acc)
    RelativeLayout rl_acc;
    @BindView(R.id.frag_color)
    RelativeLayout rl_color;
    @BindView(R.id.frag_model)
    RelativeLayout rl_model;

    //Variables
    SellDirectInfoAddAcc directInfoAddAcc = new SellDirectInfoAddAcc();
    SellDirectInfoAddColor directInfoAddColor = new SellDirectInfoAddColor();
    SellModelFragment modelFragment = new SellModelFragment();
    boolean commentVisible = false;

    boolean fromMyPage = false;

    String strComponent = "";
    String strColor = "";
    String strModel = "";
    String strDesc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_infoadd);

        fromMyPage = getIntent().getBooleanExtra(Constants.FROM_MYPAGE, false);
        loadLayout();
    }

    /**
     * 뒤로 가기 버튼
     *
     * @param v
     */
    @OnClick(R.id.ib_left)
    void onLeft(View v) {
        onBackPressed();
    }

    /**
     * 확인 버튼 눌렀을 때
     *
     * @param v
     */
    @OnClick(R.id.tv_confirm)
    void onConfirm(View v) {

        String s_content = edt_comment.getText().toString();


        if (fromMyPage) {
            Globals.pdtUpdateReq.setContent(s_content);
            sendBroadcast(new Intent(ACT_PDT_EDITED));
        } else {
            Globals.pdtCreateReq.setContent(s_content);
        }

        finish();
    }

    /**
     * 부속품 버튼 눌렀을 때
     *
     * @param v
     */
    @OnClick(R.id.rl_belonging)
    void onBelonging(View v) {
        //부속품
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 컬러 버튼 눌렀을 때
     *
     * @param v
     */
    @OnClick(R.id.rl_color)
    void onColor(View v) {
        //컬러
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
    }

    /**
     * 모델명 버튼 눌렀을 때
     *
     * @param v
     */
    @OnClick(R.id.rl_model)
    void onModel(View v) {
        //모델명
        rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        rl_model.findViewById(R.id.frag_main).setVisibility(View.VISIBLE);
    }

    /**
     * 상세설명->수정 버튼 눌렀을 때
     *
     * @param v
     */
    @OnClick({R.id.rl_comment, R.id.btn_drop_switch})
    void onComment(View v) {
        showDetailInput(commentVisible);
    }

    void showDetailInput(boolean isVisible) {

        DockAnimation m_aniSlideShow = new DockAnimation(rl_introduce_edt, 300,
                DockAnimation.FRAME_LAYOUT);

        rl_introduce_edt.setVisibility(View.VISIBLE);

        int from = !isVisible ? 0 : Utils.dpToPixel(getApplicationContext(), 120);
        m_aniSlideShow.setHeight(from, Utils.dpToPixel(getApplicationContext(), 120) - from);
        m_aniSlideShow.setAniDuration(400);
        m_aniSlideShow.setAniType(DockAnimation.ANI_SCALE);
        m_aniSlideShow.setDirection(DockAnimation.DIR_VERTICAL);
        m_aniSlideShow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                commentVisible = !commentVisible;
                if (commentVisible) {
                    btn_drop_switch.setText(R.string.close);
                } else {
                    if (edt_comment.length() == 0)
                        btn_drop_switch.setText(R.string.input);
                    else
                        btn_drop_switch.setText(R.string.modify);
                }
            }
        });
        m_aniSlideShow.start();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        edt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                SpannableString text = new SpannableString(String.valueOf(editable.length()) + " / 300");
                text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_42c2fe)), 0, String.valueOf(editable.length()).length(), 0);
                text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_666666)), String.valueOf(editable.length()).length() + 1, text.length(), 0);
                tv_status.setText(text);
            }
        });

        if (Globals.isEditing) {
            strComponent = Globals.pdtUpdateReq.getComponent();
            strColor = Globals.pdtUpdateReq.getPdtColor();
            strModel = Globals.pdtUpdateReq.getPdtModel();
            strDesc = Globals.pdtUpdateReq.getContent();
        } else {
            strComponent = Globals.pdtCreateReq.getComponent();
            strColor = Globals.pdtCreateReq.getPdtColor();
            strModel = Globals.pdtCreateReq.getPdtModel();
            strDesc = Globals.pdtCreateReq.getContent();
        }

        String s_component;
        String s_etc;
        if (fromMyPage) {
            edt_comment.setText(Globals.pdtUpdateReq.getContent());
            s_component = Globals.componentCodeToName(Globals.pdtUpdateReq.getComponent());
            s_etc = Globals.pdtUpdateReq.getEtc();
            tv_color_inputs.setText(Globals.pdtUpdateReq.getPdtColor());
            tv_model_input.setText(Globals.pdtUpdateReq.getPdtModel());
        } else {
            edt_comment.setText(Globals.pdtCreateReq.getContent());
            s_component = Globals.componentCodeToName(Globals.pdtCreateReq.getComponent());
            s_etc = Globals.pdtCreateReq.getEtc();
            tv_color_inputs.setText(Globals.pdtCreateReq.getPdtColor());
            tv_model_input.setText(Globals.pdtCreateReq.getPdtModel());
        }
        if (s_etc.length() > 0) {
            s_component += (" " + s_etc);
        }
        tv_belonging_input.setText(s_component.trim());


        if (edt_comment.length() == 0)
            btn_drop_switch.setText(R.string.input);
        else
            btn_drop_switch.setText(R.string.modify);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.frag_acc, directInfoAddAcc);
        transaction.replace(R.id.frag_color, directInfoAddColor);
        transaction.replace(R.id.frag_model, modelFragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        String strNewComponent = "";
        String strNewColor = "";
        String strNewModel = "";
        String strNewDesc = "";

        if (Globals.isEditing) {
            strNewComponent = Globals.pdtUpdateReq.getComponent();
            strNewColor = Globals.pdtUpdateReq.getPdtColor();
            strNewModel = Globals.pdtUpdateReq.getPdtModel();
            strNewDesc = edt_comment.getText().toString();
        } else {
            strNewComponent = Globals.pdtCreateReq.getComponent();
            strNewColor = Globals.pdtCreateReq.getPdtColor();
            strNewModel = Globals.pdtCreateReq.getPdtModel();
            strNewDesc = edt_comment.getText().toString();
        }

        if (rl_acc.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            rl_acc.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (rl_color.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            rl_color.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else if (rl_model.findViewById(R.id.frag_main).getVisibility() == View.VISIBLE) {
            rl_model.findViewById(R.id.frag_main).setVisibility(View.GONE);
        } else {
            if (!strComponent.equals(strNewComponent)
                    || !strColor.equals(strNewColor)
                    || !strModel.equals(strNewModel)
                    || !strDesc.equals(strNewDesc)) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(this);
                }
                String finalStrNewDesc = strNewDesc;
                builder.setTitle(getString(R.string.alarm))
                        .setMessage(getString(R.string.pdt_reg_change_alert))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (Globals.isEditing) {
                                    Globals.pdtUpdateReq.setComponent(strComponent);
                                    Globals.pdtUpdateReq.setPdtColor(strColor);
                                    Globals.pdtUpdateReq.setPdtModel(strModel);
                                    Globals.pdtUpdateReq.setContent(strDesc);
                                } else {
                                    Globals.pdtCreateReq.setComponent(strComponent);
                                    Globals.pdtCreateReq.setPdtColor(strColor);
                                    Globals.pdtCreateReq.setPdtModel(strModel);
                                    Globals.pdtCreateReq.setContent(strDesc);
                                }
                                SellInfoAddActivity.super.onBackPressed();
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (fromMyPage) {
                                    Globals.pdtUpdateReq.setContent(finalStrNewDesc);
                                    sendBroadcast(new Intent(ACT_PDT_EDITED));
                                } else {
                                    Globals.pdtCreateReq.setContent(finalStrNewDesc);
                                }
                                SellInfoAddActivity.super.onBackPressed();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return;
            }
            super.onBackPressed();
        }
    }
}
