package com.kyad.selluv;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.kyad.selluv.api.dto.search.SearchWordDto;
import com.kyad.selluv.model.Model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KSH on 3/28/2018.
 */

public class DbManager {

    /**
     * DB
     */
    private static SQLiteDatabase sqliteDB;

    public static void init_database(Context context) {
        //SQLiteDatabase db;
        File file = new File(context.getFilesDir(), "login_info.db");
        System.out.println("PATH : " + file.toString());
        try {
            sqliteDB = SQLiteDatabase.openOrCreateDatabase(file, null);
        } catch (SQLiteException e) {
            e.printStackTrace();
            return;
        }
        init_tables();
    }

    private static void init_tables() {
        if (sqliteDB != null) {
            String sqlCreateTbl = "CREATE TABLE IF NOT EXISTS user (" +
                    "userid " + "TEXT," +
                    "password " + "TEXT," +
                    "nickname " + "TEXT," +
                    "profile_url " + "TEXT," +
                    "login_cnt " + "INTEGER," +
                    "login_type " + "TEXT" + ")";
            sqliteDB.execSQL(sqlCreateTbl);

            String sqlCreateTbl2 = "CREATE TABLE IF NOT EXISTS search_word (" +
                    "item_cnt " + "INTEGER," +
                    "word " + "TEXT" + ")";
            sqliteDB.execSQL(sqlCreateTbl2);
        }
    }

    public static void saveWord(String word, int item_cnt) {
        if (sqliteDB == null)
            return;
        String sqlQueryTbl = "SELECT * FROM search_word WHERE word = '" + word + "'";
        Cursor cursor = sqliteDB.rawQuery(sqlQueryTbl, null);
        if (cursor.moveToNext()) {
            String updateQuery = "UPDATE search_word SET item_cnt = " + item_cnt +

                    " WHERE word = '" + word + "'";
            sqliteDB.execSQL(updateQuery);
        } else {
            String insertQuery = "INSERT INTO search_word (word, item_cnt) VALUES ('" +
                    word + "', " + item_cnt + ")";
            sqliteDB.execSQL(insertQuery);
        }
    }

    public static List<Object> loadSearchWord() {
        List<Object> loadData = new ArrayList<>();
        if (sqliteDB == null)
            return loadData;
        String sqlQueryTbl = "SELECT word, item_cnt FROM search_word";
        Cursor cursor = sqliteDB.rawQuery(sqlQueryTbl, null);
        while (cursor.moveToNext()) {
            SearchWordDto usrLoginInfo = new SearchWordDto();
            usrLoginInfo.setWord(cursor.getString(0));
            usrLoginInfo.setCount(cursor.getInt(1));

            loadData.add(usrLoginInfo);
        }
        return loadData;
    }

    public static void deleteSearchWord() {
        if (sqliteDB == null)
            return;
        sqliteDB.execSQL("DELETE FROM search_word");
    }

    public static void saveInfo(String userid, String password, String nickname, String profile_url, String login_type) {
        if (sqliteDB == null)
            return;
        String sqlQueryTbl = "SELECT * FROM user WHERE userid = '" + userid + "'";
        Cursor cursor = sqliteDB.rawQuery(sqlQueryTbl, null);
        if (cursor.moveToNext()) {
            String updateQuery = "UPDATE user SET nickname = '" + nickname +
                    "', password = '" + password +
                    "', profile_url = '" + profile_url +
                    "', login_type = '" + login_type +
                    "' WHERE userid = '" + userid + "'";
            sqliteDB.execSQL(updateQuery);
        } else {
            String insertQuery = "INSERT INTO user (userid, password, nickname, profile_url, login_cnt, login_type) VALUES ('" +
                    userid + "', '" + password + "', '" + nickname + "', '" + profile_url + "', 0, '" + login_type + "')";
            sqliteDB.execSQL(insertQuery);
        }
    }

    public static void deleteInfo(String userid) {
        if (sqliteDB == null)
            return;
        String deleteQuery = "DELETE FROM user WHERE userid = '" + userid + "'";
        sqliteDB.execSQL(deleteQuery);
    }

    public static int loadLoginCount(String userid) {
        if (sqliteDB == null)
            return 0;
        String sqlQueryTbl = "SELECT login_cnt FROM user WHERE userid = '" + userid + "'";
        Cursor cursor = sqliteDB.rawQuery(sqlQueryTbl, null);
        while (cursor.moveToNext()) {
            return cursor.getInt(0);
        }
        return 0;
    }

    public static void updateLoginCount(String userid, int count) {
        if (sqliteDB == null)
            return;
        String sqlQueryTbl = "UPDATE user SET login_cnt = " + count + " WHERE userid = '" + userid + "'";
        sqliteDB.execSQL(sqlQueryTbl);
    }

    public static List<Model.UserLoginInfo> loadInfo() {
        List<Model.UserLoginInfo> loadData = new ArrayList<>();
        if (sqliteDB == null)
            return loadData;
        String sqlQueryTbl = "SELECT userid, password, nickname, profile_url, login_type, login_cnt FROM user";
        Cursor cursor = sqliteDB.rawQuery(sqlQueryTbl, null);
        while (cursor.moveToNext()) {
            Model.UserLoginInfo usrLoginInfo = new Model.UserLoginInfo();
            usrLoginInfo.userid = cursor.getString(0);
            usrLoginInfo.password = cursor.getString(1);
            usrLoginInfo.nickname = cursor.getString(2);
            usrLoginInfo.profile_url = cursor.getString(3);
            usrLoginInfo.login_type = cursor.getString(4);
            usrLoginInfo.login_cnt = cursor.getInt(5);

            loadData.add(usrLoginInfo);
        }
        return loadData;
    }

    public static Model.UserLoginInfo loadInfo(String userid) {
        if (sqliteDB == null)
            return null;
        String sqlQueryTbl = "SELECT userid, password, nickname, profile_url, login_type, login_cnt FROM user WHERE userid = '" + userid + "'";
        Cursor cursor = sqliteDB.rawQuery(sqlQueryTbl, null);
        while (cursor.moveToNext()) {
            Model.UserLoginInfo usrLoginInfo = new Model.UserLoginInfo();
            usrLoginInfo.userid = cursor.getString(0);
            usrLoginInfo.password = cursor.getString(1);
            usrLoginInfo.nickname = cursor.getString(2);
            usrLoginInfo.profile_url = cursor.getString(3);
            usrLoginInfo.login_type = cursor.getString(4);
            usrLoginInfo.login_cnt = cursor.getInt(5);
            return usrLoginInfo;
        }
        return null;
    }
}
