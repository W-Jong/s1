package com.kyad.selluv.api.dto.pdt;

import android.os.Parcel;
import android.os.Parcelable;

import com.kyad.selluv.api.dto.brand.BrandMiniDto;
import com.kyad.selluv.api.dto.category.CategoryMiniDto;
import com.kyad.selluv.api.dto.usr.UsrMiniDto;

import lombok.Data;

@Data
public class PdtListDto implements Parcelable {
    //("상품")
    private PdtMiniDto pdt;
    //("등록유저")
    private UsrMiniDto usr;
    //("브랜드")
    private BrandMiniDto brand;
    //("카테고리")
    private CategoryMiniDto category;
    //("스타일")
    private PdtStyleDao pdtStyle;
    //("상품좋아요 갯수")
    private int pdtLikeCount;
    //("유저 상품좋아요 상태")
    private Boolean pdtLikeStatus;

    //("상품프로필이미지 높이")
    private int pdtProfileImgHeight;

    //("상품프로필이미지 너비")
    private int pdtProfileImgWidth;

    //("스타일프로필이미지 높이")
    private int styleProfileImgHeight;

    //("스타일프로필이미지 너비")
    private int styleProfileImgWidth;

    public PdtListDto() {

    }

    protected PdtListDto(Parcel in) {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PdtListDto> CREATOR = new Creator<PdtListDto>() {
        @Override
        public PdtListDto createFromParcel(Parcel in) {
            return new PdtListDto(in);
        }

        @Override
        public PdtListDto[] newArray(int size) {
            return new PdtListDto[size];
        }
    };

    public String getWareName() {
        return getBrand().getNameKo() + " " + getPdt().getColorName() + " " + getPdt().getPdtModel() + " " + getCategory().getCategoryName();
    }
}
