package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.api.request.MultiUidsReq;
import com.kyad.selluv.common.CustomSpinner;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.login.PasswordResetActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountActivity extends BaseActivity  {

    String Banknames[] = {"신한은행", "제주은행", "국민은행", "농협은행", "우리은행", "keb하나은행", "외환은행", "우체국", "기업은행", "sc은행", "씨티은행", "수협은행", "상호저축은행", "신용협동조합", "새마을금고", "경남은행", "전북은행", "광주은행", "부산은행", "대구은행", "hsbc","도이치", "jp모건", "bnp파리", "산업은행", "Boa"};

    private String accountNm = "";
    private String accountNum  = "";
    private String bankNm  = "";

    //UI Reference
    @OnClick(R.id.ib_left)
    void onLeft() {
        updateAccountInfo();
    }

    @BindView(R.id.tv_owner_name)
    TextView tv_owner_name;

    @BindView(R.id.edt_account_number)
    EditText edt_account_number;

    @BindView(R.id.spinner2)
    CustomSpinner spinner_bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_account);
        setStatusBarWhite();

        loadLayout();
    }

    private void loadLayout() {
        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        accountNm = Globals.myInfo.getUsr().getAccountNm();
        accountNum = Globals.myInfo.getUsr().getAccountNum();

        tv_owner_name.setText(accountNm);
        edt_account_number.setText(accountNum);
        bankNm = Globals.myInfo.getUsr().getBankNm();

        spinner_bank.initializeStringValues(this, Banknames, getResources().getString(R.string.bank_select), null, null, 12.5f);

        for (int i = 0 ; i < Banknames.length ; i++){
            if (bankNm.equals(Banknames[i])){
                spinner_bank.setSelection(i + 1);
            }
        }

        spinner_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position != 0)
                    bankNm = Banknames[position - 1];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    //update accoutn info
    private void updateAccountInfo(){

        accountNum = edt_account_number.getText().toString();

        if (accountNum.equals("")){
            Utils.showToast(this, getString(R.string.empty_account_num));
            return;
        }

        if (bankNm.equals("")){
            Utils.showToast(this, getString(R.string.empty_bank_name));
            return;
        }

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("accountNum", accountNum);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            paramObject.put("bankNm", bankNm);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse> genRes = restAPI.updateAccountInfo(Globals.userToken, RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), paramObject.toString()));

        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();

                finish();
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(AccountActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        updateAccountInfo();
    }
}
