package com.kyad.selluv.adapter;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.other.UsrAlarmDto;
import com.kyad.selluv.common.CircularImageView;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.List;

public class AlarmListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    public List<UsrAlarmDto> arrData = new ArrayList<>();
    BaseActivity activity = null;
    int selectedPos = 0;

    public AlarmListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<UsrAlarmDto> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        try {
            convertView = inflater.inflate(R.layout.item_mypage_alarm, parent, false);
            final UsrAlarmDto anItem = (UsrAlarmDto) getItem(position);
            CircularImageView iv_icon = (CircularImageView) convertView.findViewById(R.id.iv_icon);
            ImageView iv_ware = (ImageView) convertView.findViewById(R.id.iv_ware);
            TextView tv_alarm = (TextView) convertView.findViewById(R.id.tv_alarm);

            ImageUtils.load(iv_ware.getContext(), anItem.getProfileImg(), R.drawable.img_default_square, R.drawable.img_default_square, iv_ware);
            iv_ware.setVisibility(View.VISIBLE);

            switch (anItem.getSubKind()) {
                case 1:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_parcel);
                    break;
                case 2:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_genuine);
                    break;
                case 3:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_money);
                    break;
                case 6:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_certification);
                    break;
                case 11:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_reward);
                    break;
                case 12:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_reward);
                    break;
                case 17:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_discount);
                    break;
                case 20:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_enrollment);
                    break;
                case 21:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_alarm);
                    break;
                case 22:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_event);
                    iv_ware.setVisibility(View.INVISIBLE);
                    break;
                case 23:
                    iv_icon.setImageResource(R.drawable.alarmlist_ic_notice);
                    iv_ware.setVisibility(View.INVISIBLE);
                    break;
                 default:
                     ImageUtils.load(iv_icon.getContext(), anItem.getUsrProfileImg(), R.drawable.ic_user_default, R.drawable.ic_user_default, iv_icon);
                     break;
            }

            SpannableString alarm = new SpannableString(anItem.getContent() + Utils.getDiffTime1(anItem.getRegTime().getTime()));
            alarm.setSpan(new ForegroundColorSpan(Color.BLACK), 0, anItem.getContent().length() + Utils.getDiffTime1(anItem.getRegTime().getTime()).length(), 0);
            tv_alarm.setText(alarm);

            if (anItem == null) {
                return convertView;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;
    }

}