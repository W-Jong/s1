package com.kyad.selluv.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.brand.BrandListDto;
import com.kyad.selluv.brand.BrandDetailActivity;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.common.viewholder.BrandPopularViewHolder;
import com.kyad.selluv.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class BrandPopularAdapter extends RecyclerView.Adapter {

    public List<BrandListDto> dataList = new ArrayList<>();
    private BaseActivity activity;
    private int selectedTabIndex = 1;

    public BrandPopularAdapter(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.item_brand_popular, null);
        BrandPopularViewHolder holder = new BrandPopularViewHolder(itemView);
        holder.rl_content = (RelativeLayout) itemView.findViewById(R.id.rl_content);
        holder.tv_rank = (TextView) itemView.findViewById(R.id.tv_ware_cnt);
        holder.tv_brand_en = (TextView) itemView.findViewById(R.id.tv_brand_en);
        holder.tv_brand_kr = (TextView) itemView.findViewById(R.id.tv_brand_kr);
        holder.tv_brand_logo = (TextView) itemView.findViewById(R.id.tv_brand_logo);
        holder.tv_follow_cnt = (TextView) itemView.findViewById(R.id.tv_follow_cnt);
        holder.iv_bg = (ImageView) itemView.findViewById(R.id.iv_bg);
        holder.iv_logo = (ImageView) itemView.findViewById(R.id.iv_logo);
        holder.iv_heart = (ImageView) itemView.findViewById(R.id.iv_heart);
        holder.iv_ware1 = (ImageView) itemView.findViewById(R.id.iv_ware1);
        holder.iv_ware2 = (ImageView) itemView.findViewById(R.id.iv_ware2);
        holder.iv_ware3 = (ImageView) itemView.findViewById(R.id.iv_ware3);
        holder.rl_brand_logo = (RelativeLayout) itemView.findViewById(R.id.rl_brand_logo);
        itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        BrandPopularViewHolder vh = (BrandPopularViewHolder) viewHolder;
        final BrandListDto item = dataList.get(position);

        if (position == (getItemCount() - 1)) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) vh.rl_content.getLayoutParams();
            params.bottomMargin = Utils.dpToPixel(activity, 70);
            vh.rl_content.setLayoutParams(params);
        } else {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) vh.rl_content.getLayoutParams();
            params.bottomMargin = Utils.dpToPixel(activity, 25);
            vh.rl_content.setLayoutParams(params);
        }

        vh.tv_rank.setText(String.valueOf(position + 1));
        vh.tv_brand_en.setText(item.getNameEn());
        vh.tv_brand_kr.setText(item.getNameKo());
        vh.tv_brand_logo.setText(item.getNameEn());
        vh.tv_follow_cnt.setText(String.valueOf(item.getBrandLikeCount()));
        if ("".equals(item.getLogoImg())) {
            vh.iv_logo.setVisibility(View.INVISIBLE);
            vh.tv_brand_logo.setVisibility(View.VISIBLE);
        } else {
            ImageUtils.load(activity, item.getLogoImg(), R.drawable.img_default_square, R.drawable.img_default_square, vh.iv_logo);
            vh.iv_logo.setVisibility(View.VISIBLE);
            vh.tv_brand_logo.setVisibility(View.INVISIBLE);
        }
        vh.iv_heart.setImageResource(item.getBrandLikeStatus() ? R.drawable.ic_brand_follow_active : R.drawable.ic_brand_follow_inactive);
        vh.iv_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brandLike(item);
            }
        });
        if (item.getPdtList().size() > 0) {
            vh.iv_ware1.setVisibility(View.VISIBLE);
            ImageUtils.load(activity, item.getPdtList().get(0).getProfileImg(), android.R.color.transparent, R.drawable.img_default_square, vh.iv_ware1);
        } else {
            vh.iv_ware1.setVisibility(View.INVISIBLE);
        }
        if (item.getPdtList().size() > 1) {
            vh.iv_ware2.setVisibility(View.VISIBLE);
            ImageUtils.load(activity, item.getPdtList().get(1).getProfileImg(), android.R.color.transparent, R.drawable.img_default_square, vh.iv_ware2);
        } else {
            vh.iv_ware2.setVisibility(View.INVISIBLE);
        }
        if (item.getPdtList().size() > 2) {
            vh.iv_ware3.setVisibility(View.VISIBLE);
            ImageUtils.load(activity, item.getPdtList().get(2).getProfileImg(), android.R.color.transparent, R.drawable.img_default_square, vh.iv_ware3);
        } else {
            vh.iv_ware3.setVisibility(View.INVISIBLE);
        }
        final RelativeLayout final_rl_brand_logo = vh.rl_brand_logo;
        final ImageView final_iv_bg = vh.iv_bg;
        if (!"".equals(item.getBackImg())) {
            Glide.with(activity).load(item.getBackImg())
                    .apply(new RequestOptions().placeholder(android.R.color.transparent).error(R.drawable.img_default))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                            final_rl_brand_logo.setBackground(resource);
                            final_iv_bg.setImageDrawable(resource);
                        }
                    });
        } else {
            vh.rl_brand_logo.setBackgroundResource(R.drawable.img_default);
        }
        vh.iv_ware1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item.getPdtList().size() > 0 ? item.getPdtList().get(0).getPdtUid() : 0);
            }
        });
        vh.iv_ware2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item.getPdtList().size() > 1 ? item.getPdtList().get(1).getPdtUid() : 0);
            }
        });
        vh.iv_ware3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDetail(view, item.getPdtList().size() > 2 ? item.getPdtList().get(2).getPdtUid() : 0);
            }
        });

        vh.rl_brand_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBrandDetailPage(v, item.getBrandUid());
            }
        });

        vh.tv_brand_en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBrandDetailPage(v, item.getBrandUid());
            }
        });

        vh.tv_brand_kr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBrandDetailPage(v, item.getBrandUid());
            }
        });
    }

    public void toDetail(View view, int pdtUid) {
        if (pdtUid == 0)
            return;

        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(DetailActivity.PDT_UID, pdtUid);
        activity.startActivity(intent);
    }

    public void setSelectedTabIndex(int index) {
        this.selectedTabIndex = index;
    }

    private void goBrandDetailPage(View view, int brandUid) {

        if (brandUid == 0)
            return;

        Intent intent = new Intent(activity, BrandDetailActivity.class);
        intent.putExtra(BrandDetailActivity.BRAND_UID, brandUid);
        intent.putExtra(BrandDetailActivity.BRAND_PRODUCT_GROUP, selectedTabIndex);
        activity.startActivity(intent);

    }

    private void brandLike(final BrandListDto item) {
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);

        Call<GenericResponse<Void>> genRes = !item.getBrandLikeStatus() ? restAPI.likeBrand(Globals.userToken, item.getBrandUid()) : restAPI.unLikeBrand(Globals.userToken, item.getBrandUid());
        genRes.enqueue(new TokenCallback<Void>(activity) {

            @Override
            public void onSuccess(Void response) {
                if (item.getBrandLikeStatus()) {
                    item.setBrandLikeStatus(false);
                    item.setBrandLikeCount(item.getBrandLikeCount() - 1);
                } else {
                    item.setBrandLikeStatus(true);
                    item.setBrandLikeCount(item.getBrandLikeCount() + 1);
                }
                notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                if (t instanceof com.kyad.selluv.api.APIException) {
                    if (((com.kyad.selluv.api.APIException) t).getCode().equals("150")) {
                        ((BaseActivity) activity).showLoginSuggestionPopup();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


}
