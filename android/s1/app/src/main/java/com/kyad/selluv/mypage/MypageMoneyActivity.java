package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.MoneyListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.other.UsrMoneyHisContentDto;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.Utils;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class MypageMoneyActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lv_history)
    ListView lv_history;
    @BindView(R.id.tv_amount)
    TextView tv_amount;
    @BindView(R.id.tv_sale_amount)
    TextView tv_sale_amount;
    @BindView(R.id.tv_count)
    TextView tv_count;

    private int currentPage = 0;
    private boolean isLoading = false;
    private boolean isLast = false;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    @OnClick(R.id.btn_get_money)
    void onGetMoney() {
        startActivity(EventActivity.class, false, 0, 0);
    }

    @OnClick(R.id.btn_account_manage)
    void onAccountManage() {
        startActivity(AccountActivity.class, false, 0, 0);
    }

    //Variables
    MoneyListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_money);
        setStatusBarWhite();
        loadLayout();

        getMoneyHistoryList();
    }

    private void loadLayout() {
        listAdapter = new MoneyListAdapter(this);
        lv_history.setAdapter(listAdapter);
        lv_history.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    currentPage++;
                    getMoneyHistoryList();
                }
            }
        });
//        Collections.addAll(listAdapter.arrData, Constants.moneyList0);
//        listAdapter.notifyDataSetChanged();
    }

    private void getMoneyHistoryList() {
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrMoneyHisContentDto>> genRes = restAPI.getMoneyHistory(Globals.userToken, currentPage);

        genRes.enqueue(new TokenCallback<UsrMoneyHisContentDto>(this) {
            @Override
            public void onSuccess(UsrMoneyHisContentDto response) {
                closeLoading();
                isLoading = false;

                if (currentPage == 0) {
                    listAdapter.arrData.clear();
                }
                isLast = response.getHistory().isLast();
                tv_amount.setText(Utils.getAttachCommaFormat((int) response.getMoney()));
                tv_sale_amount.setText(Utils.getAttachCommaFormat((int) response.getCashbackMoney()));
                tv_count.setText("(" + response.getHistory().getTotalElements() + ")");

                listAdapter.arrData.addAll(response.getHistory().getContent());
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }
}
