package com.kyad.selluv.mypage;

import android.os.Bundle;
import android.widget.ListView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.EventListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dao.NoticeDao;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.EventDao;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class EventActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.lv_event)
    ListView lv_event;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    EventListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage_event);
        setStatusBarWhite();

        loadLayout();
        getEventList();
    }

    private void loadLayout() {
        listAdapter = new EventListAdapter(this);
        lv_event.setAdapter(listAdapter);
    }

    //get Event list
    private void getEventList(){
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<List<EventDao>>> genRes = restAPI.getEventList(Globals.userToken);

        genRes.enqueue(new TokenCallback<List<EventDao>>(this) {
            @Override
            public void onSuccess(List<EventDao> response) {
                closeLoading();

                listAdapter.arrData = response;
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }
}
