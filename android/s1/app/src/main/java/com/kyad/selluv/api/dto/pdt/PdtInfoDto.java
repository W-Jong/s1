package com.kyad.selluv.api.dto.pdt;

import lombok.Data;

@Data
public class PdtInfoDto {
    //("상품UID")
    private int pdtUid;
    //("브랜드명")
    private String brandName;
    //("카테고리명")
    private String categoryName;
    //("이미지")
    private String profileImg;
    //("가격")
    private long price;
}
