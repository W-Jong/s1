package com.kyad.selluv.fragment;

import com.dinuscxj.refresh.IDragDistanceConverter;

public class ResistanceDragDistanceConvert implements IDragDistanceConverter {
    private final float mMaxScrollDistance;
    private final float paddingTop;

    public ResistanceDragDistanceConvert(float maxScrollDistance, float paddingTop) {
        this.mMaxScrollDistance = maxScrollDistance - paddingTop;
        this.paddingTop = paddingTop;
    }

    @Override
    public float convert(float scrollDistance, float refreshDistance) {
        return mMaxScrollDistance - 2 * (mMaxScrollDistance * mMaxScrollDistance)
                / (scrollDistance + 2 * mMaxScrollDistance);
    }
}
