package com.kyad.selluv.api.enumtype;

public enum MONEY_HIS_TYPE {
    MONEYUSE(1), SELLMONEY(2), SHAREREWARD(3), REPORTREWARD(4),
    INVITEREWARD(5), JOINREWARD(6), INVITEFIRSTSELL(7), REVIEWREWARD(8), BUYREWARD(9);

    private int code;

    private MONEY_HIS_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
