package com.kyad.selluv.login;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.kyad.selluv.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kyad.selluv.common.Constants.ACT_USER_SIGNUP;

public class LoginSuggestionDialog extends DialogFragment {

    //UI Reference
    @BindView(R.id.videoView)
    VideoView videoView;

    //Variables
    //private Context mContext;
    private View rootView;


    public LoginSuggestionDialog() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.dialog_login_suggestion, container, false);
        ButterKnife.bind(this, rootView);
        loadLayout();
        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick(R.id.tv_glance)
    void onGlance(View v) {
        dismiss();
    }

    @OnClick(R.id.btn_signup)
    void onSignup(View v) {
        dismiss();
        getActivity().sendBroadcast(new Intent(ACT_USER_SIGNUP));
    }


    private void loadLayout() {
        String path1 = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.test;
        MediaController mc = new MediaController(getActivity());
        mc.setAnchorView(videoView);
        mc.setMediaPlayer(videoView);
        Uri uri = Uri.parse(path1);
        videoView.setMediaController(mc);
        videoView.setVideoURI(uri);
        videoView.start();
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        videoView.setLayoutParams(params);
    }

}
