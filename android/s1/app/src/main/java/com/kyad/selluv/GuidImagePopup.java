/**
 * 홈
 */
package com.kyad.selluv;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.kyad.selluv.common.Constants;

import butterknife.BindView;
import butterknife.OnClick;


public class GuidImagePopup extends BaseActivity {

    @BindView(R.id.iv_img)
    ImageView iv_img;

    public int imageID = 0;

    @OnClick(R.id.ib_close)
    void onClose(View v) {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_full_image);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        imageID = getIntent().getIntExtra(Constants.POPUP_IMAGE, 0);
        if (imageID != 0)
            iv_img.setImageResource(imageID);
    }
}
