package com.kyad.selluv.top.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.dto.search.SearchWordDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.model.Model;
import com.kyad.selluv.top.SearchResultActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    BaseActivity activity;

    public List<Object> arrData = new ArrayList<>();
    public int nType = 1;// 1:인기검색 , 2: 최근검색

    public SearchListAdapter(BaseActivity activity) {
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    public void setData(List<Object> addData) {
        arrData = addData;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if (arrData == null || nType == 0)
            return 0;

        return arrData.size();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficient to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if (arrData == null)
            return null;
        return arrData.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     * ViewGroup)
     */

    public View getView(final int position, View convertView, ViewGroup parent) {
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        if (nType == 1 || nType == 2) {
            //인기검색 / 최근검색
            convertView = inflater.inflate(R.layout.item_top_search, parent, false);

            try {
                final SearchWordDto anItem = (SearchWordDto) getItem(position);
                TextView tv_view_count = convertView.findViewById(R.id.tv_view_count);
                ImageView iv_ware = convertView.findViewById(R.id.iv_ware);
                iv_ware.setVisibility(View.GONE);
                TextView tv_ware_name = convertView.findViewById(R.id.tv_ware_name);
                if (nType == 1)
                    tv_view_count.setText(String.format("%d views", anItem.getCount()));
                else if (nType == 2)
                    tv_view_count.setText(String.format("%d items", anItem.getCount()));

                tv_ware_name.setText(anItem.getWord());
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, SearchResultActivity.class);
                        intent.putExtra(Constants.SEARCH_WORD, anItem.getWord());
                        intent.putExtra(Constants.FROM_PAGE, Constants.WIRE_FRAG_SEARCH);
                        activity.startActivity(intent);
                    }
                });
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        } else if (nType == 3) {
            Model.SearchItem item = (Model.SearchItem) getItem(position);
            if (item.type == 0) {
                convertView = inflater.inflate(R.layout.item_search_seperator, parent, false);
                TextView tv_title = convertView.findViewById(R.id.tv_title);
                tv_title.setText(item.name);
            } else {
                convertView = inflater.inflate(R.layout.item_top_search, parent, false);
                TextView tv_ware_name = convertView.findViewById(R.id.tv_ware_name);
                TextView tv_info = convertView.findViewById(R.id.tv_info);
                TextView tv_view_count = convertView.findViewById(R.id.tv_view_count);
                tv_ware_name.setText(item.name);
                tv_info.setText(item.info);
                tv_view_count.setText(String.format("%d items", item.count));
                //                convertView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        activity.startActivity(SearchResultActivity.class, false, 0, 0);
//                    }
//                });

            }
        } else if (nType == 4) {
            convertView = inflater.inflate(R.layout.item_search_none, parent, false);

            final String s_key = (String) getItem(position);
            TextView tv_ware_name = convertView.findViewById(R.id.tv_ware_name);
            tv_ware_name.setText("'" + s_key + "' 로 검색하기");
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, SearchResultActivity.class);
                    intent.putExtra(Constants.SEARCH_WORD, s_key);
                    intent.putExtra(Constants.FROM_PAGE, Constants.WIRE_FRAG_SEARCH);
                    activity.startActivity(intent);
                }
            });
        }

        return convertView;
    }

    public void removeAllItems() {
        arrData.clear();
        notifyDataSetChanged();
    }

}