package com.kyad.selluv.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.ResponseMeta;
import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluv.api.request.UserLikeGroupUpdateReq;
import com.kyad.selluv.common.Globals;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyad.selluv.common.Constants.ACT_ERROR_ACCESS_TOKEN;
import static com.kyad.selluv.common.Constants.ACT_NOT_PERMISSION;

public class InterestSelectActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.ib_woman)
    ImageButton ib_woman;

    @BindView(R.id.ib_man)
    ImageButton ib_man;

    @BindView(R.id.ib_kids)
    ImageButton ib_kids;

    @BindView(R.id.btn_ok)
    Button btn_ok;


    boolean woman_on = false;
    boolean man_on = false;
    boolean kids_on = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_select);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        loadLayout();
    }

    private void loadLayout() {
        if ((Globals.myInfo.getUsr().getLikeGroup() & PDT_GROUP_TYPE.MALE.getCode()) == PDT_GROUP_TYPE.MALE.getCode()) {
            man_on = true;
        } else {
            man_on = false;
        }
        if ((Globals.myInfo.getUsr().getLikeGroup() & PDT_GROUP_TYPE.FEMALE.getCode()) == PDT_GROUP_TYPE.FEMALE.getCode()) {
            woman_on = true;
        } else {
            woman_on = false;
        }
        if ((Globals.myInfo.getUsr().getLikeGroup() & PDT_GROUP_TYPE.KIDS.getCode()) == PDT_GROUP_TYPE.KIDS.getCode()) {
            kids_on = true;
        } else {
            kids_on = false;
        }
        updateButtons();
    }

    private void updateButtons() {
        if (woman_on) {
            ib_woman.setBackgroundResource(R.drawable.mypage_profile_waregroup_woman_on);
        } else {
            ib_woman.setBackgroundResource(R.drawable.mypage_profile_waregroup_woman_off);
        }

        if (man_on) {
            ib_man.setBackgroundResource(R.drawable.mypage_profile_waregroup_man_on);
        } else {
            ib_man.setBackgroundResource(R.drawable.mypage_profile_waregroup_man_off);
        }

        if (kids_on) {
            ib_kids.setBackgroundResource(R.drawable.mypage_profile_waregroup_kids_on);
        } else {
            ib_kids.setBackgroundResource(R.drawable.mypage_profile_waregroup_kids_off);
        }

        validateButton();
    }

    @OnClick(R.id.ib_woman)
    void onWomanClick(View v) {
        woman_on = !woman_on;
        updateButtons();
    }

    @OnClick(R.id.ib_man)
    void onManClick(View v) {
        man_on = !man_on;
        updateButtons();
    }

    @OnClick(R.id.ib_kids)
    void onKidsClick(View v) {
        kids_on = !kids_on;
        updateButtons();
    }

    @OnClick(R.id.btn_ok)
    void onOk(View v) {

        UserLikeGroupUpdateReq req = new UserLikeGroupUpdateReq();
        req.setIsMale(man_on);
        req.setIsFemale(woman_on);
        req.setIsChildren(kids_on);

        showLoading();
        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse> genRes = restAPI.userLikeGroup(Globals.userToken, req);
        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    setResult(RESULT_OK);
                    finish();
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_ERROR_ACCESS_TOKEN.getErrCode()) {
                    //token이 만료됬을때.. 혹은 다른 기기에서 로그인
                    sendBroadcast(new Intent(ACT_ERROR_ACCESS_TOKEN));
                } else if (response.body().getMeta().getErrCode() == ResponseMeta.USER_TEMP_NOT_PERMISSION.getErrCode()) {
                    //임시회원인경우..
                    sendBroadcast(new Intent(ACT_NOT_PERMISSION));
                } else {//실패
                    Toast.makeText(InterestSelectActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(InterestSelectActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void validateButton() {
        if (woman_on || man_on || kids_on) {
            btn_ok.setEnabled(true);
            btn_ok.setText(R.string.confirm);
            btn_ok.setBackgroundResource(R.drawable.btn_azure);
        } else {
            btn_ok.setEnabled(false);
            btn_ok.setBackgroundResource(R.color.color_999999);
            btn_ok.setText(R.string.confirm);
        }
    }

}
