package com.kyad.selluv.common.viewholder;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyad.selluv.R;
import com.kyad.selluv.common.expandablerecyclerview.viewholders.ChildViewHolder;

public class ThemaItemViewHolder extends ChildViewHolder {

    public FrameLayout rl_thema_ware;
    public ImageView iv_ware;
    public TextView tv_ware_eng_name;
    public TextView tv_ware_name;
    public TextView tv_ware_price;

    public ThemaItemViewHolder(View itemView) {
        super(itemView);
        rl_thema_ware = (FrameLayout) itemView.findViewById(R.id.rl_thema_ware);
        iv_ware = (ImageView) itemView.findViewById(R.id.iv_ware);
        tv_ware_eng_name = (TextView) itemView.findViewById(R.id.tv_ware_eng_name);
        tv_ware_name = (TextView) itemView.findViewById(R.id.tv_ware_name);
        tv_ware_price = (TextView) itemView.findViewById(R.id.tv_ware_price);
    }
}
