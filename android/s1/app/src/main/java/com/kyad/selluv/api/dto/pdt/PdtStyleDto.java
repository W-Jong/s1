package com.kyad.selluv.api.dto.pdt;

import com.kyad.selluv.api.dto.usr.UsrMiniDto;

import lombok.Data;

@Data
public class PdtStyleDto {
    //("스타일")
    private PdtStyleDao pdtStyle;
    //("등록유저")
    private UsrMiniDto usr;
}
