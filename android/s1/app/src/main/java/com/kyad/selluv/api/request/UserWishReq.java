package com.kyad.selluv.api.request;

import android.support.annotation.IntRange;

import com.kyad.selluv.api.enumtype.PDT_GROUP_TYPE;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserWishReq {

    //@ApiModelProperty("브랜드UID")
    @IntRange(from = 1)
    private int brandUid;

    //@ApiModelProperty("상품군")
    private PDT_GROUP_TYPE pdtGroupType;

    //@ApiModelProperty("카테고리UID")
    @IntRange(from = 5)
    private int categoryUid;

    //@ApiModelProperty("모델")
    private String pdtModel;

    //@ApiModelProperty("사이즈")
    private String pdtSize;

    //@ApiModelProperty("컬러")
    private String colorName;
}
