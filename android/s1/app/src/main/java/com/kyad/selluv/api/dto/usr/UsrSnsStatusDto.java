package com.kyad.selluv.api.dto.usr;

import lombok.Data;

@Data
public class UsrSnsStatusDto {
    //("페이스북 연동상태")
    private boolean isFacekbook;
    //("네이버 연동상태")
    private boolean isNaver;
    //("카카오 연동상태")
    private boolean isKakaoTalk;
    //("인스타그램 연동상태")
    private boolean isInstagram;
    //("트위터 연동상태")
    private boolean isTwitter;
}
