package com.kyad.selluv.top;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.GuidImagePopup;
import com.kyad.selluv.R;
import com.kyad.selluv.api.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ConditionFilterActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.iv_new_check)
    ImageView iv_new_check;
    @BindView(R.id.iv_excellent_check)
    ImageView iv_excellent_check;
    @BindView(R.id.iv_good_check)
    ImageView iv_good_check;
    @BindView(R.id.iv_ok_check)
    ImageView iv_ok_check;

    private List<PDT_CONDITION_TYPE> conditionFilters;
    /**
     * 컨디션 순서는 Constants.PDT_CONDITIONS 와 동일해야한다.
     */
    private ImageView[] checkIVs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_condition);

        checkIVs = new ImageView[]{
                iv_new_check,
                iv_excellent_check,
                iv_good_check,
                iv_ok_check
        };

        conditionFilters = Globals.searchReq.getPdtConditionList();
        loadLayout();
    }
    /** 새상품 */
    @OnClick(R.id.rl_new)
    void onConditionNew() {
        onClickCondition(0);
    }
    /** 최상 */
    @OnClick(R.id.rl_excellent)
    void onCOnditionExcellent() {
        onClickCondition(1);
    }
    /** 상 */
    @OnClick(R.id.rl_good)
    void onConditionGood() {
        onClickCondition(2);
    }
    /** 중상 */
    @OnClick(R.id.rl_ok)
    void onConditionOk() {
        onClickCondition(3);
    }

    /**
     * 검색결과 보기
     */
    @OnClick(R.id.btn_ok)
    void onOk() {
        sendBroadcast(new Intent(Constants.ACT_SEARCH_CATEGORY));
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.rl_guide_btn)
    void onGuide() {
        Intent intent = new Intent(this, GuidImagePopup.class);
        intent.putExtra(Constants.POPUP_IMAGE, R.drawable.sell_direct_info_condition_guide);
        startActivity(intent);
    }

    @OnClick(R.id.tv_init)
    void onInit() {
        for (int i = 0; i < checkIVs.length; i++) {
            checkIVs[i].setVisibility(View.INVISIBLE);
        }

        conditionFilters.clear();
    }

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    private void loadLayout() {
        for (int i = 0; i < Constants.PDT_CONDITIONS.length; i++) {
            checkIVs[i].setVisibility(View.INVISIBLE);
            for (int j = 0; j < conditionFilters.size(); j++) {
                if (conditionFilters.get(j).equals(Constants.PDT_CONDITIONS[i])) {
                    checkIVs[i].setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    private void onClickCondition(int idx) {
        if (checkIVs[idx].getVisibility() == View.INVISIBLE) {
            checkIVs[idx].setVisibility(View.VISIBLE);
            conditionFilters.add(Constants.PDT_CONDITIONS[idx]);
        } else {
            checkIVs[idx].setVisibility(View.INVISIBLE);
            conditionFilters.remove(Constants.PDT_CONDITIONS[idx]);
        }
    }

}
