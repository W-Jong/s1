package com.kyad.selluv.api.dto.deal;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Created by KSH on 3/8/2018.
 */
@Data
public class DealDao {
    //"거래UID")
    private int dealUid;
    //"추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    //"상품UID")
    private int pdtUid;
    //"구매자 회원UID")
    private int usrUid;
    //"판매자 회원UID")
    private int sellerUsrUid;
    //"발렛UID  0-발렛상품 아님 0아니면 발렛상품")
    private int valetUid = 0;
    //"수령자 성함")
    private String recipientNm = "";
    //"수령자 연락처")
    private String recipientPhone = "";
    //"수령자 주소")
    private String recipientAddress = "";
    //"송장번호")
    private String deliveryNumber = "";
    //"인증전송장 번호")
    private String verifyDeliveryNumber = "";
    //"상품 영문브랜드")
    private String brandEn = "";
    //"상품 타이틀")
    private String pdtTitle = "";
    //"상품 사이즈")
    private String pdtSize;
    //"상품이미지")
    private String pdtImg = "";
    //"상품가격")
    private long pdtPrice = 0;
    //"배송비")
    private long sendPrice = 0;
    //"결제시간")
    private Timestamp payTime = null;
    //"정품인증가격")
    private long verifyPrice = 0;
    //"사용적립금")
    private long rewardPrice = 0;
    //"결제금액")
    private long price = 0;
    //"코드할인금액")
    private long payPromotionPrice = 0;
    //"구매프로모션")
    private String payPromotion = "";
    //"아임포트결제방법 1-간편결제신용카드 2-일반결제신용카드 3-실시간계좌이체 4-네이버페이")
    private int payImportMethod = 1;
    //"셀럽이용료")
    private long selluvPrice = 0;
    //"결제수수료")
    private long payFeePrice = 0;
    //"정산금액")
    private long cashPrice = 0;
    //"코드혜택금액")
    private long cashPromotionPrice = 0;
    //"판매프로모션")
    private String cashPromotion = "";
    //"정산계좌정보")
    private String cashAccount = "";
    //"네고 및 카운터 요청시간(status에 따라 결정됨)")
    private Timestamp negoTime = null;
    //"네고 및 카운터네고 제안금액")
    private long reqPrice = 0;
    //"배송완료시간")
    private Timestamp deliveryTime = null;
    //"거래완료시간")
    private Timestamp compTime = null;
    //"정산완료시간")
    private Timestamp cashCompTime = null;
    //"취소사유")
    private String cancelReason = "";
    //"취소시간")
    private Timestamp cancelTime = null;
    //"환불사유")
    private String refundReason = "";
    //"반품요청시간")
    private Timestamp refundReqTime = null;
    //"반품승인시간")
    private Timestamp refundCheckTime = null;
    //"반품완료시간")
    private Timestamp refundCompTime = null;
    //"반품송장")
    private String refundDeliveryNumber = "";
    //"반품첨부사진")
    //"거래상태 0-삭제 1-배송준비 2-배송진행 3-정품인증 4-배송완료 5-거래완료 " +
//            "6-정산완료 7-반품접수 8-반품승인 9-반품완료 10-주문취소 11-네고제안 12-카운터네고제안")
    private int status = 1;

}
