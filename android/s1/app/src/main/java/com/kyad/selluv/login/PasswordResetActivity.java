package com.kyad.selluv.login;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.common.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordResetActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.edt_email)
    EditText edt_email;
    @BindView(R.id.tv_send_email)
    TextView tv_send_email;
    @BindView(R.id.btn_ok)
    Button btn_ok;

    //Variables
    View rootView;
    int keyboardHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        loadLayout();
    }

    void onKeyboardShow() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) rootView.getLayoutParams();
        lp.topMargin = -keyboardHeight + Utils.dpToPixel(PasswordResetActivity.this, tv_send_email.getVisibility() == View.VISIBLE ? 105 : 125);
        rootView.setLayoutParams(lp);
    }

    void onKeyboardHidden() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) rootView.getLayoutParams();
        lp.topMargin = 0;
        rootView.setLayoutParams(lp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInputFromInputMethod(edt_email.getWindowToken(), InputMethodManager.SHOW_FORCED);
        }
        edt_email.requestFocus();
    }

    private void loadLayout() {

        rootView = findViewById(R.id.activity_main);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();
                keyboardHeight = screenHeight - (r.bottom);
                if (rootView.getRootView().getHeight() - (r.bottom - r.top) > Utils.dpToPixel(PasswordResetActivity.this, 100)) { // if more than 100 pixels, its probably a keyboard...
                    onKeyboardShow();
                } else {
                    onKeyboardHidden();
                }
            }
        });

        Utils.hideKeypad(this, findViewById(R.id.activity_main));

        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!edt_email.getText().toString().isEmpty())
                    btn_ok.setEnabled(true);
                else
                    btn_ok.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tv_send_email.setVisibility(View.GONE);
    }

    @OnClick(R.id.ib_back)
    void onBack(View v) {
        finish();
    }

    @OnClick(R.id.btn_ok)
    void onOk(View v) {
        tv_send_email.setVisibility(View.INVISIBLE);

        if (!Utils.isValidEmail(edt_email.getText())) {
            Toast.makeText(this, "이메일 형식이 올바르지 않습니다", Toast.LENGTH_SHORT).show();
            return;
        }

        showLoading();

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("content", edt_email.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        Call<GenericResponse> genRes = restAPI.findPwd(RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), paramObject.toString()));
        genRes.enqueue(new Callback<GenericResponse>() {

            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                closeLoading();
                Log.d("kyad-log", response.body().toString());
                if (response.body().getMeta().getErrCode() == 0) {//성공
                    tv_send_email.setVisibility(View.VISIBLE);
                } else {//실패
                    Toast.makeText(PasswordResetActivity.this, response.body().getMeta().getErrMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                closeLoading();
                Log.d("kyad-log", t.getMessage());
                Toast.makeText(PasswordResetActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

}
