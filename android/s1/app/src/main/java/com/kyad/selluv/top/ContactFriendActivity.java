package com.kyad.selluv.top;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.usr.UsrAddressBookDto;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.api.request.MultiStringReq;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.common.ImageUtils;
import com.kyad.selluv.common.Utils;
import com.kyad.selluv.user.UserActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class ContactFriendActivity extends BaseActivity {

    //UI Reference
    @BindView(R.id.ll_friend)
    LinearLayout ll_friend;
    @BindView(R.id.ll_not_login)
    LinearLayout ll_not_login;
    @BindView(R.id.tv_friend_count)
    TextView tv_friend_count;
    @BindView(R.id.rly_contact)
    RelativeLayout rly_contact;
    @BindView(R.id.rly_contact1)
    RelativeLayout rly_contact1;

    @OnClick(R.id.ib_left)
    void onLeft() {
        finish();
    }

    //Variables
    LayoutInflater inflater;
    private ArrayList<String> arrPhones = new ArrayList<String>();
    private ArrayList<UsrFollowListDto> arrJoinList = new ArrayList<UsrFollowListDto>();
    private ArrayList<UsrFollowListDto> arrContactList = new ArrayList<UsrFollowListDto>();
    private List<String> arrUsrList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_contact);
        setStatusBarWhite();
        getContactList();
    }

    private void loadLayout() {

        inflater = LayoutInflater.from(this);
        tv_friend_count.setText(String.format(getString(R.string.contact_friend_count), arrJoinList.size()));
        //tv_friend_count.setText(String.format(R.string.facebook_friend_count, 2));
        int count = arrJoinList.size();
        for (int i = 0; i < count; i++) {
            View item = inflater.inflate(R.layout.item_friend_follow, null);
            final UsrFollowListDto anItem = arrJoinList.get(i);
            ImageView iv_profile = (ImageView) item.findViewById(R.id.iv_profile);
            ImageUtils.load(iv_profile.getContext(), anItem.getProfileImg(), R.drawable.ic_logo, R.drawable.ic_logo, iv_profile);
            TextView name = (TextView) item.findViewById(R.id.tv_name);
            TextView en_name = (TextView) item.findViewById(R.id.tv_en_name);
            name.setText(anItem.getUsrNckNm());
            en_name.setText(anItem.getUsrUid());
            ToggleButton btn_follow = (ToggleButton) item.findViewById(R.id.btn_follow);
            if (anItem.isUsrLikeStatus()) {
                btn_follow.setChecked(false);
            } else {
                btn_follow.setChecked(true);
            }
            btn_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestUserFollow(anItem.isUsrLikeStatus(), anItem.getUsrUid());
                }
            });
            iv_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ContactFriendActivity.this, UserActivity.class);
                    intent.putExtra(UserActivity.USR_UID, anItem.getUsrUid());
                    startActivity(intent);
                }
            });

            ll_friend.addView(item);
        }
        if (count == 0) {
            rly_contact.setVisibility(View.GONE);
        } else {
            rly_contact.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < arrUsrList.size(); i++) {
            View item = inflater.inflate(R.layout.item_friend_follow1, null);
            String anItem = arrUsrList.get(i);
            ImageView iv_profile = (ImageView) item.findViewById(R.id.iv_profile);
            iv_profile.setVisibility(View.GONE);
            TextView name = (TextView) item.findViewById(R.id.tv_name);
            TextView en_name = (TextView) item.findViewById(R.id.tv_en_name);
            Button btn_follow = (Button) item.findViewById(R.id.btn_follow);
            btn_follow.setBackgroundResource(R.drawable.btn_azure);
            btn_follow.setTextColor(Color.WHITE);
            btn_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:"));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "SELLUV");
                    sendIntent.putExtra("sms_body", "SELLUV앱을 사용해보세요.");
                    startActivity(sendIntent);
                }
            });
            name.setText(getNameFrimPhone(anItem));
            if (anItem.length() == 11){
                en_name.setText(Utils.convertPhoneNum(anItem));
            } else {
                en_name.setText(anItem);
            }
            ll_not_login.addView(item);
        }
        if (arrUsrList.size() == 0) {
            rly_contact1.setVisibility(View.GONE);
        } else {
            rly_contact1.setVisibility(View.VISIBLE);
        }

    }

    //전화번호로부터 이름 얻기
    private String getNameFrimPhone(String num) {
        String result = "";

        for (int i = 0; i < arrContactList.size(); i++) {
            UsrFollowListDto dic = arrContactList.get(i);
            if (num.equals(dic.getUsrId())) {
                result = dic.getUsrNckNm();
            }
        }

        return result;
    }

    private void getContactList() {

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER, "data1", "contact_id"}, "has_phone_number=?", new String[]{"1"}, ContactsContract.Contacts.DISPLAY_NAME);
        int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int phoneNumberIndex = cursor.getColumnIndex("data1");

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    String name = cursor.getString(nameIndex);
                    String phoneNumber = cursor.getString(phoneNumberIndex);
                    if (!(name == null || phoneNumber == null)) {
                        String phoneNumber1 = phoneNumber.replaceAll("[^0-9]", "");

                        arrPhones.add(phoneNumber1);
                        UsrFollowListDto dic = new UsrFollowListDto();
                        dic.setUsrId(phoneNumber1);
                        dic.setUsrNckNm(name);
                        arrContactList.add(dic);
                    }
                }
            }
        }

        cursor.close();
        getFriendAddressList();
    }

    //연락처 친구찾기
    private void getFriendAddressList() {
        MultiStringReq dicInfo = new MultiStringReq();
        dicInfo.setList(arrPhones);

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<UsrAddressBookDto>> genRes = restAPI.getFriendAddressList(Globals.userToken, dicInfo);

        genRes.enqueue(new TokenCallback<UsrAddressBookDto>(this) {
            @Override
            public void onSuccess(UsrAddressBookDto response) {
                closeLoading();

                if (arrUsrList.size() > 0)
                    arrUsrList.clear();
                if (arrJoinList.size() > 0)
                    arrJoinList.clear();


                for (int i = 0; i < response.getJoinedList().size(); i++) {
                    arrJoinList.add(response.getJoinedList().get(i));
                }
                arrUsrList = response.getUnJoinedPhoneList();
                loadLayout();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();

            }
        });
    }

    //req user follow/unfollow
    private void requestUserFollow(boolean islike, int uid) {

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Void>> genRes = islike ?
                restAPI.unFollowUsr(Globals.userToken, uid) :
                restAPI.followUsr(Globals.userToken, uid);

        genRes.enqueue(new TokenCallback<Void>(this) {
            @Override
            public void onSuccess(Void response) {
                closeLoading();

                getContactList();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
            }
        });
    }
}
