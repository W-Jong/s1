package com.kyad.selluv.detail;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.kyad.selluv.BaseActivity;
import com.kyad.selluv.R;
import com.kyad.selluv.adapter.FollowListAdapter;
import com.kyad.selluv.api.RestAPI;
import com.kyad.selluv.api.TokenCallback;
import com.kyad.selluv.api.dto.GenericResponse;
import com.kyad.selluv.api.dto.pdt.Page;
import com.kyad.selluv.api.dto.usr.UsrFollowListDto;
import com.kyad.selluv.common.Constants;
import com.kyad.selluv.common.Globals;
import com.kyad.selluv.model.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class LikeActivity extends BaseActivity {

    public static final String PDT_UID = "PDT_UID";

    private int pdtUid;
    private int page = 0;
    private boolean isLoading = false;
    private boolean isLast = false;

    //UI Reference
    @BindView(R.id.lv_filter)
    ListView lv_filter;

    @OnClick(R.id.ib_left)
    void onBack(View v) {
        finish();
    }

    //Variables
    private FollowListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_likeuser);
        pdtUid = getIntent().getIntExtra(PDT_UID, 0);
        if (pdtUid < 1) {
            finish();
            return;
        }

        loadLayout();
        getPdtLikeUserList();
    }

    private void loadLayout() {
        adapter = new FollowListAdapter(LikeActivity.this);
        lv_filter.setAdapter(adapter);
        lv_filter.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && totalItemCount > 0 && !(isLast) && !isLoading) {
                    page++;
                    getPdtLikeUserList();
                }
            }
        });
    }

    private void getPdtLikeUserList() {
        if (isLoading || isLast)
            return;

        isLoading = true;

        RestAPI restAPI = RestAPI.retrofit.create(RestAPI.class);
        showLoading();

        Call<GenericResponse<Page<UsrFollowListDto>>> genRes = restAPI.pdtLikeUserList(Globals.userToken, pdtUid, page);

        genRes.enqueue(new TokenCallback<Page<UsrFollowListDto>>(this) {
            @Override
            public void onSuccess(Page<UsrFollowListDto> response) {
                closeLoading();
                isLoading = false;

                if (page == 0) {
                    adapter.arrData.clear();
                }
                isLast = response.isLast();
                adapter.arrData.addAll(response.getContent());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(Throwable t) {
                closeLoading();
                isLoading = false;
            }
        });
    }

}
