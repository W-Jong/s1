package com.kyad.selluv.api.request;


import lombok.Data;
import lombok.NonNull;

@Data
public class ContentReq {
    //@ApiModelProperty("content")
    private String content;
}
