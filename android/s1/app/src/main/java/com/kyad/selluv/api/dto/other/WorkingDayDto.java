package com.kyad.selluv.api.dto.other;


import lombok.Data;

@Data
public class WorkingDayDto {
//    @ApiModelProperty("타겟날짜")
//    @JsonSerialize(using = LocalDateSerializer.class)
//    @JsonDeserialize(using = LocalDateDeserializer.class)
    private String targetDate;
}
