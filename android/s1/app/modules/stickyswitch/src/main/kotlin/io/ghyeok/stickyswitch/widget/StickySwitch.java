/*
 * MIT License
 *
 * Copyright (c) 2017 GwonHyeok
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.ghyeok.stickyswitch.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import io.ghyeok.stickyswitch.R;

/**
 * Created by ghyeok on 2017. 3. 13..
 * <p>
 * This class implements a beautiful switch widget for android
 *
 * @author GwonHyeok
 */
public class StickySwitch extends View {

    private String TAG = "StickySwitch";

    // padding variables
    private int padding = 2;

    // text variables
    String leftText = "";
    String rightText = "";

    // colors
    int sliderBackgroundColor = Color.argb(0xff, 0xf0, 0xf0, 0xf0);
    int switchColor = Color.argb(0xff, 0xff, 0xff, 0xff);
    int activeTextColor = Color.argb(0xff, 0x33, 0x33, 0x33);
    int inactiveTextColor = Color.argb(0xff, 0x99, 0x99, 0x99);
    int borderColor = Color.argb(0xff, 0xc3, 0xc3, 0xc3);

    // rounded rect
    private Paint sliderBackgroundPaint = new Paint();
    private RectF sliderBackgroundRect = new RectF();

    private Paint sliderStrokePaint = new Paint();
    private RectF sliderStrokeRect = new RectF();

    // circular switch
    private Paint switchBackgroundPaint = new Paint();
    private RectF switchBackgroundRect = new RectF();

    private Paint switchStrokePaint = new Paint();
    private RectF switchStrokeRect = new RectF();

    // left, right text paint and size
    private Paint leftTextPaint = new Paint();
    private Rect leftTextRect = new Rect();
    private Paint rightTextPaint = new Paint();
    private Rect rightTextRect = new Rect();

    // left, right text size
    private float leftTextSize = 50f;
    private float rightTextSize = 50f;

    // text max,min transparency
    private int textAlphaMax = 255;
    private int textAlphaMin = 130;

    // text color transparency
    private int leftTextAlpha = textAlphaMax;
    private int rightTextAlpha = textAlphaMin;

    // text size
    private int textSize = 50;
    // text size when selected status
    private int selectedTextSize = 50;

    // switch Status
    // false : left status
    // true  : right status
    private boolean isSwitchOn = false;

    // percent of switch animation
    // animatePercent will be 0.0 ~ 1.0
    private float animatePercent = 0.0f;

    // circular switch bounce rate
    // animateBounceRate will be 1.1 ~ 0.0
    private double animateBounceRate = 1.0;

    // listener;
    public OnSelectedChangeListener onSelectedChangeListener = null;

    // AnimatorSet, Animation Options;
    AnimatorSet animatorSet = null;
    int animationDuration = 600;

    Context context;

    public StickySwitch(Context ctxt) {
        this(ctxt, null);
    }

    public StickySwitch(Context ctxt, AttributeSet attrs) {
        this(ctxt, attrs, 0);
    }

    public StickySwitch(Context ctxt, AttributeSet attrs, int defStyleAttr) {
        super(ctxt, attrs, defStyleAttr);
        context = ctxt;
        init(attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StickySwitch(Context ctxt, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(ctxt, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr, defStyleRes);
    }

    private void init(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this.setClickable(true);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StickySwitch, defStyleAttr, defStyleRes);

        // left switch icon
        leftText = typedArray.getString(R.styleable.StickySwitch_ss_leftText) != null ? typedArray.getString(R.styleable.StickySwitch_ss_leftText) : leftText;

        // right switch icon
        rightText = typedArray.getString(R.styleable.StickySwitch_ss_rightText) != null ? typedArray.getString(R.styleable.StickySwitch_ss_rightText) : rightText;

        // padding size
        padding = typedArray.getDimensionPixelSize(R.styleable.StickySwitch_ss_padding, padding);

        // saved text size
        textSize = typedArray.getDimensionPixelSize(R.styleable.StickySwitch_ss_textSize, textSize);
        selectedTextSize = typedArray.getDimensionPixelSize(R.styleable.StickySwitch_ss_selectedTextSize, selectedTextSize);

        // current text size
        leftTextSize = selectedTextSize;
        rightTextSize = textSize;

        // slider background color
        sliderBackgroundColor = typedArray.getColor(R.styleable.StickySwitch_ss_sliderBackgroundColor, sliderBackgroundColor);

        // switch color
        switchColor = typedArray.getColor(R.styleable.StickySwitch_ss_switchColor, switchColor);

        // text color
        activeTextColor = typedArray.getColor(R.styleable.StickySwitch_ss_activeTextColor, activeTextColor);
        inactiveTextColor = typedArray.getColor(R.styleable.StickySwitch_ss_inactiveTextColor, inactiveTextColor);

        // border color
        borderColor = typedArray.getColor(R.styleable.StickySwitch_ss_borderColor, borderColor);

        // animation duration
        animationDuration = typedArray.getInt(R.styleable.StickySwitch_ss_animationDuration, animationDuration);

        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // circle Radius
        int sliderRadius = getMeasuredHeight() / 2;

        // draw circular rect
        sliderBackgroundPaint.setColor(sliderBackgroundColor);
        sliderBackgroundRect.set(0 + padding, 0 + padding, this.getMeasuredWidth() - padding, getMeasuredHeight() - padding);
        canvas.drawRoundRect(sliderBackgroundRect, sliderRadius, sliderRadius, sliderBackgroundPaint);

        sliderStrokePaint.setColor(borderColor);
        sliderStrokePaint.setStyle(Paint.Style.STROKE);
        sliderStrokePaint.setStrokeWidth(2);
        sliderStrokePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        sliderStrokeRect.set(0 + padding, 0 + padding, this.getMeasuredWidth() - padding, getMeasuredHeight() - padding);
        canvas.drawRoundRect(sliderBackgroundRect, sliderRadius, sliderRadius, sliderStrokePaint);

        //translate offset
        double offsetX = (getMeasuredWidth() / 2 - padding) * animatePercent;

        // switch background
        switchBackgroundPaint.setColor(switchColor);
        switchBackgroundRect.set((float) evaluateBounceRate(offsetX) + padding, 0 + padding, (float) offsetX + (float) evaluateBounceRate((double) (this.getMeasuredWidth() / 2 - padding)) + padding, getMeasuredHeight() - padding);
        canvas.drawRoundRect(switchBackgroundRect, sliderRadius, sliderRadius, switchBackgroundPaint);

        switchStrokePaint.setStyle(Paint.Style.STROKE);
        switchStrokePaint.setStrokeWidth(2);
        switchStrokePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        switchStrokePaint.setColor(borderColor);
        switchStrokeRect.set((float) evaluateBounceRate(offsetX) + padding, padding, (float) offsetX + (float) evaluateBounceRate((double) (this.getMeasuredWidth() / 2 - padding)) + padding, getMeasuredHeight() - padding);
        canvas.drawRoundRect(switchStrokeRect, sliderRadius, sliderRadius, switchStrokePaint);

        // set text paint
        leftTextPaint.setColor(activeTextColor);
        leftTextPaint.setAlpha(leftTextAlpha);
        leftTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        rightTextPaint.setColor(activeTextColor);
        rightTextPaint.setAlpha(rightTextAlpha);
        rightTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        // set text size
        leftTextPaint.setTextSize(leftTextSize);
        rightTextPaint.setTextSize(rightTextSize);

        // draw text when isShowText is true
        // measure text size
        measureText();

        // left text position
        double leftTextX = (getMeasuredWidth() * 0.5 - padding - leftTextRect.width()) * 0.5;
        double leftTextY = getMeasuredHeight() * 0.5 + leftTextRect.height() * 0.5 - padding;

        // draw left text
        canvas.save();
        canvas.drawText(leftText, (float) leftTextX, (float) leftTextY, leftTextPaint);
        canvas.restore();

        // right text position
        double rightTextX = ((getMeasuredWidth() * 0.5 - padding - rightTextRect.width()) * 0.5) + (getMeasuredWidth() * 0.5);
        double rightTextY = getMeasuredHeight() * 0.5 + rightTextRect.height() * 0.5 - padding;

        // draw right text
        canvas.save();
        canvas.drawText(rightText, (float) rightTextX, (float) rightTextY, rightTextPaint);
        canvas.restore();
    }

    private double evaluateBounceRate(Double value) {
        return value * animateBounceRate;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled() || !isClickable()) return false;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            isSwitchOn = !isSwitchOn;
            animateCheckState(isSwitchOn);
            notifySelectedChange();
        }
        return super.onTouchEvent(event);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        measureText();

        double textWidth = leftTextRect.width() + rightTextRect.width() + padding * 2;
        double measuredTextHeight = leftTextRect.height() + padding * 2;

        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        double heightSize = 0;

        if (heightMode == MeasureSpec.UNSPECIFIED) {
            heightSize = heightMeasureSpec;
        }
        if (heightMode == MeasureSpec.AT_MOST) {
            heightSize = measuredTextHeight;
        }
        if (heightMode == MeasureSpec.EXACTLY) {
            heightSize = MeasureSpec.getSize(heightMeasureSpec);
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        double widthSize = 0;

        if (widthMode == MeasureSpec.UNSPECIFIED) {
            widthSize = widthMeasureSpec;
        }
        if (widthMode == MeasureSpec.AT_MOST) {
            widthSize = textWidth;
        }
        if (widthMode == MeasureSpec.EXACTLY) {
            widthSize = MeasureSpec.getSize(widthMeasureSpec);
        }
        setMeasuredDimension((int) widthSize, (int) heightSize);
    }

    Direction getDirection() {
        if (isSwitchOn) return Direction.RIGHT;
        else return Direction.LEFT;
    }

    String getText(Direction direction) {
        if (direction == Direction.LEFT)
            return leftText;
        else return rightText;
    }

    private void notifySelectedChange() {
        onSelectedChangeListener.onSelectedChange((isSwitchOn) ? Direction.RIGHT : Direction.LEFT, getText(getDirection()));
    }

    private void measureText() {
        leftTextPaint.getTextBounds(leftText, 0, leftText.length(), leftTextRect);
        rightTextPaint.getTextBounds(rightText, 0, rightText.length(), rightTextRect);
    }

    private void animateCheckState(Boolean newCheckedState) {
        this.animatorSet = new AnimatorSet();
        if (animatorSet != null) {
            animatorSet.playTogether(
                    getLiquidAnimator(newCheckedState),
                    leftTextSizeAnimator(newCheckedState),
                    rightTextSizeAnimator(newCheckedState),
                    leftTextAlphaAnimator(newCheckedState),
                    rightTextAlphaAnimator(newCheckedState),
                    getBounceAnimator()
            );
            animatorSet.setDuration(animationDuration);
            animatorSet.start();
        }
    }

    private void changeCheckState(Boolean newCheckedState) {
        // Change TextAlpha Without Animation
        leftTextAlpha = (newCheckedState) ? textAlphaMin : textAlphaMax;
        rightTextAlpha = (newCheckedState) ? textAlphaMax : textAlphaMin;

        // Change TextSize without animation
        leftTextSize = (newCheckedState) ? textSize : selectedTextSize;
        rightTextSize = (newCheckedState) ? selectedTextSize : textSize;

        // Change Animate Percent(LiquidAnimation) without animation
        animatePercent = (newCheckedState) ? 1.0f : 0.0f;
        animateBounceRate = 1.0f;
    }

    private Animator leftTextAlphaAnimator(Boolean newCheckedState) {
        int toAlpha = newCheckedState ? textAlphaMin : textAlphaMax;
        ValueAnimator animator = ValueAnimator.ofInt(leftTextAlpha, toAlpha);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setStartDelay(animationDuration / 3);
        animator.setDuration(animationDuration - (animationDuration / 3));
        animator.setTarget(leftTextAlpha);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                leftTextAlpha = (int) valueAnimator.getAnimatedValue();
                invalidate();
            }
        });
        return animator;
    }

    private Animator rightTextAlphaAnimator(Boolean newCheckedState) {
        int toAlpha = newCheckedState ? textAlphaMax : textAlphaMin;
        ValueAnimator animator = ValueAnimator.ofInt(rightTextAlpha, toAlpha);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setStartDelay(animationDuration / 3);
        animator.setDuration(animationDuration - (animationDuration / 3));
        animator.setTarget(rightTextAlpha);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                rightTextAlpha = (int) valueAnimator.getAnimatedValue();
                invalidate();
            }
        });
        return animator;
    }

    private Animator leftTextSizeAnimator(Boolean newCheckedState) {
        int toTextSize = newCheckedState ? textSize : selectedTextSize;
        ValueAnimator animator = ValueAnimator.ofInt((int) leftTextSize, toTextSize);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setStartDelay(animationDuration / 3);
        animator.setDuration(animationDuration - (animationDuration / 3));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                leftTextSize = (int) valueAnimator.getAnimatedValue();
                invalidate();
            }
        });
        return animator;
    }

    private Animator rightTextSizeAnimator(Boolean newCheckedState) {
        int toTextSize = newCheckedState ? selectedTextSize : textSize;
        ValueAnimator animator = ValueAnimator.ofInt((int) rightTextSize, toTextSize);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setStartDelay(animationDuration / 3);
        animator.setDuration(animationDuration - (animationDuration / 3));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                rightTextSize = (int) valueAnimator.getAnimatedValue();
                invalidate();
            }
        });
        return animator;
    }

    private Animator getLiquidAnimator(Boolean newCheckedState) {
        ValueAnimator animator = ValueAnimator.ofFloat(animatePercent, newCheckedState ? 1f : 0f);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.setDuration(animationDuration);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                animatePercent = (float) valueAnimator.getAnimatedValue();
                invalidate();
            }
        });

        return animator;
    }

    private Animator getBounceAnimator() {
        ValueAnimator animator = ValueAnimator.ofFloat(1f, 0.9f, 1f);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.setDuration((int) (animationDuration * 0.41));
        animator.setStartDelay(animationDuration);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                animateBounceRate = ((Float) valueAnimator.getAnimatedValue()).doubleValue();
                invalidate();
            }
        });
        return animator;
    }

    public enum Direction {
        LEFT, RIGHT
    }

    public interface OnSelectedChangeListener {
        void onSelectedChange(Direction direction, String rightTextAlpha);
    }
}