package com.kyad.selluv.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping("/file")
public class FileUploadController {

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody HashMap<String, String> fileUploadToTemp(@RequestParam("uploadfile") MultipartFile uploadfile) {

        OutputStream out = null;
        boolean flagSuccess = true;
        String fileName = "";

        try {
            // 파일명 얻기
            String originFileName = uploadfile.getOriginalFilename();
            //랜덤 파일명생성(확장자 유지)
            fileName = StarmanHelper.getUniqueString(FilenameUtils.getExtension(originFileName));
            // 파일의 바이트 정보 얻기
            byte[] bytes = uploadfile.getBytes();
            // 파일의 저장 경로 얻기
            String uploadPath = StarmanHelper.makeDirectory("temp") + "/" + fileName;

            // 파일 객체 생성
            File file = new File(uploadPath);

            // 파일 아웃풋 스트림 생성
            out = new FileOutputStream(file);
            // 파일 아웃풋 스트림에 파일의 바이트 쓰기
            out.write(bytes);

        } catch (IOException e) {
            flagSuccess = false;
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!flagSuccess) {
            HashMap<String, String> fileData = new HashMap<>();
            fileData.put("result", "error");
            return fileData;
        }
        HashMap<String, String> fileData = new HashMap<>();
        fileData.put("result", "ok");
        fileData.put("fileName", fileName);
        fileData.put("fileURL", StarmanHelper.UPLOAD_URL_PATH + "/temp/" + fileName);

        return fileData;
    }

    @RequestMapping(value = "/uploads", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody HashMap<String, Object> filesUploadsToTemp(@RequestParam("uploadfiles") MultipartFile[] uploadfiles) {

        if (uploadfiles.length == 0){
            HashMap<String, Object> response = new HashMap<>();
            response.put("result", "error");
            return response;
        }

        OutputStream out = null;
        boolean flagSuccess = true;
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        try {
            for (MultipartFile uploadfile : uploadfiles) {
                String fileName = "";
                // 파일명 얻기
                String originFileName = uploadfile.getOriginalFilename();
                //랜덤 파일명생성(확장자 유지)
                fileName = StarmanHelper.getUniqueString(FilenameUtils.getExtension(originFileName));
                // 파일의 바이트 정보 얻기
                byte[] bytes = uploadfile.getBytes();
                // 파일의 저장 경로 얻기
                String uploadPath = StarmanHelper.makeDirectory("temp") + "/" + fileName;

                // 파일 객체 생성
                File file = new File(uploadPath);

                // 파일 아웃풋 스트림 생성
                out = new FileOutputStream(file);
                // 파일 아웃풋 스트림에 파일의 바이트 쓰기
                out.write(bytes);

                HashMap<String, String> fileData = new HashMap<>();
                fileData.put("fileName", fileName);
                fileData.put("fileURL", StarmanHelper.UPLOAD_URL_PATH + "/temp/" + fileName);

                arrayList.add(fileData);
                if (out != null)
                	out.close();
            }
        } catch (IOException e) {
            flagSuccess = false;
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!flagSuccess) {
            HashMap<String, Object> response = new HashMap<>();
            response.put("result", "error");
            return response;
        }
        HashMap<String, Object> response = new HashMap<>();
        response.put("result", "ok");
        response.put("data", arrayList);
        return response;
    }
}
