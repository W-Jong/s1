package com.kyad.selluv.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

public class CommonUtil {
	
	private static String driver_name;
	private static String connection_url;
	private static String db_user;
	private static String db_password;
	
	static {
		Configuration config = new Configuration().configure();
		driver_name = config.getProperty("connection.driver_class");
		connection_url = config.getProperty("connection.url");
		db_user = config.getProperty("connection.username");
		db_password = config.getProperty("connection.password");
	}
	
	public static boolean isLogin(HttpSession session) {
		String is_login = "";
		try {
			is_login = session.getAttribute(Constant.SESSION_IS_LOGIN).toString();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		if (is_login == null || is_login.isEmpty())
			return false;
		return true;
	}
	
	public static String getDateFormat(String d){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Timestamp ts = null;
		String result = "";
		try{
			ts = Timestamp.valueOf(d);
			result = sdf.format(ts);
		}catch(Exception e){
			return "";
		}
		
		return result;
	}
	
	public static Connection sqlConnect() throws Exception {

		try {
			Class.forName(driver_name).newInstance();
			Connection connection = DriverManager.getConnection(connection_url, db_user, db_password);
			return connection;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
