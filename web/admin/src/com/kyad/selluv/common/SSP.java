package com.kyad.selluv.common;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import com.google.gson.Gson;


public class SSP {

	public interface SSPFormatter {
		String formater(String d, HashMap<String, Object> row);
	}

	private static String driver_name;
	private static String connection_url;
	private static String db_user;
	private static String db_password;

	static {
		Configuration config = new Configuration().configure();
		driver_name = config.getProperty("connection.driver_class");
		connection_url = config.getProperty("connection.url");
		db_user = config.getProperty("connection.username");
		db_password = config.getProperty("connection.password");
	}

	/**
	 * Create the data output array for the DataTables rows
	 * 
	 * @param array
	 *            $columns Column information array
	 * @param array
	 *            $data Data from the SQL get
	 * @return array Formatted data in a row based format
	 */
	private static ArrayList<HashMap<String, Object>> dataOutput(final ArrayList<HashMap<String, Object>> columns, final ArrayList<HashMap<String, Object>> data) {
		ArrayList<HashMap<String, Object>> out = new ArrayList<HashMap<String, Object>>();

		for (int i = 0, ien = data.size(); i < ien; i++) {
			HashMap<String, Object> row = new HashMap<String, Object>();

			for (int j = 0, jen = columns.size(); j < jen; j++) {
				HashMap<String, Object> column = columns.get(j);

				// Is there a formatter?
				if (column.containsKey("formatter")) {
					row.put("" + column.get("dt"), "" + ((SSPFormatter) column.get("formatter")).formater("" + (data.get(i).get(column.get("db"))), data.get(i)));
				} else {
					row.put("" + column.get("dt"), "" + data.get(i).get(column.get("db")));
				}
			}

			out.add(row);
		}

		return out;
	}

	/**
	 * Throw a fatal error.
	 * 
	 * This writes out an error message in a JSON string which DataTables will
	 * see and show to the user in the browser.
	 * 
	 * @param string
	 *            $msg Message to send to the client
	 */
	private static void fatal(HttpServletResponse response, String msg) throws Exception {
		response.addHeader("contentType", "utf8;text/json");
		HashMap<String, String> error = new HashMap<String, String>();
		error.put("error", msg);
		String json = new Gson().toJson(error);
		response.getWriter().print(json);
		Exception e = new Exception();
		throw e;

	}

	public static String fatal_msg(String msg) {
		HashMap<String, String> error = new HashMap<String, String>();
		error.put("error", msg);
		return new Gson().toJson(error);
	}

	/**
	 * Pull a particular property from each assoc. array in a numeric array,
	 * returning and array of the property values from each item.
	 * 
	 * @param array
	 *            $a Array to get data from
	 * @param string
	 *            $prop Property to read
	 * @return array Array of property values
	 */
	private static ArrayList<String> pluck(ArrayList<HashMap<String, Object>> a, String prop) {
		ArrayList<String> out = new ArrayList<String>();

		for (int i = 0, len = a.size(); i < len; i++) {
			out.add("" + a.get(i).get(prop));
		}

		return out;
	}

	private static String implode(String delimeter, ArrayList<String> arrayList) {
		if (arrayList == null || arrayList.size() == 0) {
			return "";
		}

		if (arrayList.size() == 1) {
			return arrayList.get(0);
		}

		String out = arrayList.get(0);
		for (int i = 1; i < arrayList.size(); i++) {
			out = out + delimeter + arrayList.get(i);
		}
		return out;
	}

	/**
	 * @param request
	 * @param strContain
	 * @return 0 is not exist, > 0 is count of strContain
	 */

	private static int lengthOfParameter(HttpServletRequest request, String strContain) {
		if (strContain == null || strContain.equals(""))
			return 0;
		Map<String, String[]> parameterMap = request.getParameterMap();
		String parameterKeys = parameterMap.keySet().toString();
		int cnt = 0;
		while (parameterKeys.contains(strContain + "[" + cnt + "]"))
			cnt++;
		return cnt;
	}

	/**
	 * Paging
	 * 
	 * Construct the LIMIT clause for server-side processing SQL query
	 * 
	 * @param array
	 *            $request Data sent to server by DataTables
	 * @param array
	 *            $columns Column information array
	 * @return string SQL limit clause
	 */
	private static String limit(HttpServletRequest request, ArrayList<HashMap<String, Object>> columns) {
		String limit = "";

		if (request.getParameter("start") != null && Integer.parseInt(request.getParameter("length")) != -1) {
			limit = " LIMIT " + Integer.parseInt(request.getParameter("start")) + ", " + Integer.parseInt(request.getParameter("length"));
		}

		return limit;
	}

	/**
	 * Ordering
	 * 
	 * Construct the ORDER BY clause for server-side processing SQL query
	 * 
	 * @param array
	 *            $request Data sent to server by DataTables
	 * @param array
	 *            $columns Column information array
	 * @return string SQL order by clause
	 */
	private static String order(HttpServletRequest request, ArrayList<HashMap<String, Object>> columns) {
		String order = "";

		if (lengthOfParameter(request, "order") > 0) {
			ArrayList<String> orderBy = new ArrayList<String>();
			ArrayList<String> dtColumns = pluck(columns, "dt");

			for (int i = 0, ien = lengthOfParameter(request, "order"); i < ien; i++) {

				int columnIdx = Integer.parseInt(request.getParameter("order[" + i + "][column]"));
				columnIdx = dtColumns.indexOf(request.getParameter("columns[" + columnIdx + "][data]"));

				HashMap<String, Object> column = columns.get(columnIdx);

				if (request.getParameter("columns[" + columnIdx + "][orderable]").equals("true")) {
					String dir = "";
					if (request.getParameter("order[" + i + "][dir]").equals("asc")) {
						dir = "ASC";
					} else {
						dir = "DESC";
					}

					orderBy.add("" + column.get("db") + " " + dir);
				}
			}

			order = " ORDER BY " + implode(", ", orderBy);
		}

		return order;
	}

	/**
	 * Searching / Filtering
	 * 
	 * Construct the WHERE clause for server-side processing SQL query.
	 * 
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here performance on large
	 * databases would be very poor
	 * 
	 * @param array
	 *            $request Data sent to server by DataTables
	 * @param array
	 *            $columns Column information array
	 * @param array
	 *            $bindings Array of values for PDO bindings, used in the
	 *            sql_exec() function
	 * @return string SQL where clause
	 */
	private static String filter(HttpServletRequest request, ArrayList<HashMap<String, Object>> columns, String custom_where) {
		ArrayList<String> globalSearch = new ArrayList<String>();
		ArrayList<String> columnSearch = new ArrayList<String>();
		ArrayList<String> dtColumns = pluck(columns, "dt");

		if (request.getParameter("search[value]") != null && !request.getParameter("search[value]") .equals("")) {
			String str = "";
			try {
				if (request.getMethod().equals("GET")) {
					str = new String(request.getParameter("search[value]").getBytes("iso-8859-1"), "utf-8");
				} else {
					str = request.getParameter("search[value]");
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			for (int i = 0, ien = lengthOfParameter(request, "columns"); i < ien; i++) {
				int columnIdx = dtColumns.indexOf(request.getParameter("columns[" + i + "][data]"));
				HashMap<String, Object> column = columns.get(columnIdx);

				if (request.getParameter("columns[" + i + "][searchable]").equals("true")) {
					String binding = "'%" + str + "%'";
					globalSearch.add("" + column.get("db") + " LIKE " + binding);
				}
			}
		}

		// Individual column filtering
		for (int i = 0, ien = lengthOfParameter(request, "columns"); i < ien; i++) {
			int columnIdx = dtColumns.indexOf(request.getParameter("columns[" + i + "][data]"));
			HashMap<String, Object> column = columns.get(columnIdx);

			String str = "";
			try {
				if (request.getMethod().equals("GET")) {
					str = new String(request.getParameter("columns[" + i + "][search][value]").getBytes("iso-8859-1"), "utf-8");
				} else {
					str = request.getParameter("columns[" + i + "][search][value]");
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			if (request.getParameter("columns[" + i + "][searchable]").equals("true") && !str.equals("")) {
				// PDO::PARAM_STR );
				String binding = "'%" + str + "%'";
				columnSearch.add("" + column.get("db") + " LIKE " + binding);
			}
		}

		// Combine the filters into a single string
		String where = "";

		if (globalSearch.size() > 0) {
			where = "(" + implode(" OR ", globalSearch) + ")";
		}

		if (columnSearch.size() > 0) {
			if (where.equals("")) {
				where = implode(" AND ", columnSearch);
			} else {
				where = where + " AND " + implode(" AND ", columnSearch);
			}
		}

		if (!where.equals("")) {
			where = " WHERE " + where;
		}

		if (!custom_where.equals("")) {
			if (!where.equals("")) {
				where = where + " AND " + custom_where;
			} else {
				where = " WHERE " + custom_where;
			}
		}

		return where;
	}

	/**
	 * Connect to the database
	 * 
	 * @param array
	 *            $sql_details SQL server connection details array, with the
	 *            properties: * host - host name * db - database name * user -
	 *            user name * pass - user password
	 * @return resource Database connection handle
	 */
	private static Connection sqlConnect(HttpServletResponse response) throws Exception {

		try {
			Class.forName(driver_name).newInstance();
			Connection connection = DriverManager.getConnection(connection_url, db_user, db_password);
			return connection;

		} catch (Exception e) {
			fatal(response, "An error occurred while connecting to the database. " + "The error reported by the server was: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Execute an SQL query on the database
	 * 
	 * @param resource
	 *            $db Database handler
	 * @param array
	 *            $bindings Array of PDO binding values from bind() to be used
	 *            for safely escaping strings. Note that this can be given as
	 *            the SQL query string if no bindings are required.
	 * @param string
	 *            $sql SQL query to execute.
	 * @return array Result from the query (all rows)
	 */
	private static ResultSet sqlExec(HttpServletResponse response, Connection connection, String bindings, String sql) throws Exception {
		if (sql == null) {
			sql = bindings;
		}

		// Execute
		ResultSet rs = null;
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
		} catch (Exception e) {
			fatal(response, "An SQL error occurred: " + e.getMessage());
		}

		return rs;
	}

	/**
	 * Perform the SQL queries needed for an server-side processing requested,
	 * utilising the helper functions of this class, limit(), order() and
	 * filter() among others. The returned array is ready to be encoded as JSON
	 * in response to an SSP request, or can be modified if needed before
	 * sending back to the client.
	 * 
	 * @param array
	 *            $request Data sent to server by DataTables
	 * @param array
	 *            $sql_details SQL connection details - see sql_connect()
	 * @param string
	 *            $table SQL table to query
	 * @param string
	 *            $primaryKey Primary key of the table
	 * @param array
	 *            $columns Column information array
	 * @return array Server-side processing response array
	 */
	public static String simple(HttpServletRequest request, HttpServletResponse response, String table, String primaryKey, ArrayList<HashMap<String, Object>> columns, String custom_where) throws Exception {
		String bindings = "";

		// Build the SQL query string from the request
		String limit = limit(request, columns);
		String order = order(request, columns);
		String where = filter(request, columns, custom_where);

		ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
		int recordsFiltered = 0;
		int recordsTotal = 0;

		Connection conn;
		conn = sqlConnect(response);

		// Main query to actually get the data
		ResultSet rsData = sqlExec(response, conn, bindings, "SELECT SQL_CALC_FOUND_ROWS " + implode(", ", pluck(columns, "db")) + " FROM " + table + where + order + limit);
		ResultSetMetaData rsMeta = rsData.getMetaData();
		while (rsData.next()) {
			HashMap<String, Object> recordData = new HashMap<String, Object>();
			for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
				String value = rsData.getString(i);
				if (value == null || value.trim().isEmpty())
					value = "";

				recordData.put(rsMeta.getColumnName(i), value);
			}
			data.add(recordData);
		}
		rsData.close();

		// Data set length after filtering
		ResultSet resFilterLength = sqlExec(response, conn, "SELECT FOUND_ROWS()", null);
		resFilterLength.next();
		recordsFiltered = resFilterLength.getInt(1);
		resFilterLength.close();

		// Total data set length
		if (!custom_where.equals("")) {
			custom_where = " WHERE " + custom_where;
		}
		ResultSet resTotalLength = sqlExec(response, conn, "SELECT COUNT(" + primaryKey + ") FROM " + table + custom_where, null);
		resTotalLength.next();
		recordsTotal = resTotalLength.getInt(1);
		resTotalLength.close();

		conn.close();

		/*
		 * Output
		 */
		HashMap<String, Object> output = new HashMap<String, Object>();
		output.put("draw", request.getParameter("draw"));
		output.put("recordsTotal", recordsTotal);
		output.put("recordsFiltered", recordsFiltered);
		output.put("data", dataOutput(columns, data));

		String json = new Gson().toJson(output);
		return json;
	}

}

