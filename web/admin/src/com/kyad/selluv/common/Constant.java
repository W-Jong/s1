package com.kyad.selluv.common;

public class Constant {
	//어드민용 API 토큰
	public static final String SELLUV_ADMIN_TOKEN = "ef8de1ae3192dfee311d3f9756c8cc24160647b";
	//셀럽 API 주소
	public static final String SELLUV_API_ADDRESS = "http://13.125.35.211/api";

	public static final int SIGNIN_SUCCESS = 1; //가입할때 성공하는 경우
	public static final int SIGNIN_ERR_WRONG_PHONE = -1; //가입할때 실패하는 경우(phone)
	public static final int SIGNIN_ERR_WRONG_PWD = -2; //가입할때 실패하는 경우(password)
	public static final int SIGNIN_ERR_WRONG_ID = -2; //가입할때 실패하는 경우(id)
	public static final int SIGNIN_ERR_DB = 0; //가입할때 실패하는 경우(DB)

	public static final int RESULT_SUCCESS = 1; //조작성공인 경우
	public static final int RESULT_PARAM_ERR = -1; // 파라미터가 잘못된 경우
	public static final int RESULT_DB_ERR = 0; // DB오류인 경우
	public static final int RESULT_FAILED = 0; // 실패인 경우	

	public static final int STATUS_REMOVED = 0;
	public static final int STATUS_USE = 1;
	
	public static final String SESSION_IS_LOGIN = "is_login";
	public static final String SESSION_ADMIN_ID = "admin_id";
	public static final String SESSION_ADMIN_NICK = "admin_nick";
	public static final String SESSION_ADMIN_UID = "admin_uid";
	public static final String SESSION_ADMIN_PRIVILEGE_UID = "admin_privilege_uid";
	public static final String SESSION_ADMIN_LOGOUT_FLAG = "admin_logout_flag";

	public static final Integer QNA_STATUS_DONE = 1;
	public static final Integer QNA_STATUS_REQ = 2;
	
	public static final int PDT_GROUP_MALE = 1;
	public static final int PDT_GROUP_FEMALE = 2;
	public static final int PDT_GROUP_KIDS = 4;
	
	public static final String DIR_NAME_PDT = "pdt";
	public static final String DIR_NAME_PDT_STYLE = "pdt_style";
	public static final String DIR_NAME_BANNER = "banner";
	public static final String DIR_NAME_EVENT = "event";
	public static final String DIR_NAME_THEME = "theme";
	public static final String DIR_NAME_BRAND = "brand";
	public static final String DIR_NAME_SHARE = "share";
	public static final String DIR_NAME_VALET = "valet";
	public static final String DIR_NAME_DEAL = "deal";
	
	public static final int BRAND_REGULAR = 1;
	public static final int BRAND_NEW = 2;
	
	public static final Integer PERIOD_DAY = 1;
	public static final Integer PERIOD_WEEK = 2;
	public static final Integer PERIOD_MONTH = 3;
	public static final Integer PERIOD_YEAR = 4;
	
	public static final int PDT_PHOTOSHOP_YES = 1;
	public static final int PDT_PHOTOSHOP_NO = 2;
	
	public static final int VALET_CASH_FINISH = 1;
	public static final int VALET_ACCEPT = 2;
	public static final int VALET_PDT_REG = 3;
	public static final int VALET_SALE_FINISH = 4;
	
	public static final int REPORT_SOLVED = 1;
	public static final int REPORT_UNSOLVED = 2;
	
	public static final int REPORT_PDT_SOLVED = 1;
	public static final int REPORT_PDT_UNSOLVED = 2;
	
	public static final int SHARE_SOLVED = 1;
	public static final int SHARE_UNSOLVED = 2;
	
	// 거래상태
	public static final int DEAL_STATUS_DELETE = 0; // 삭제
	public static final int DEAL_STATUS_DERIVERY_READY = 1; //배송준비
	public static final int DEAL_STATUS_DERIVERY = 2; // 배송진행 
	public static final int DEAL_STATUS_VERIFY = 3; // 정품인증
	public static final int DEAL_STATUS_DERIVERY_DONE = 4; // 배송완료
	public static final int DEAL_STATUS_DEAL_DONE = 5; // 거래완료
	public static final int DEAL_STATUS_CASH_COMP = 6; // 정산완료
	public static final int DEAL_STATUS_REFUND_REQ = 7; // 반품점수
	public static final int DEAL_STATUS_REFUND_CHECK = 8; // 반품승인
	public static final int DEAL_STATUS_REFUND_DONE = 9; // 반품완료
	public static final int DEAL_STATUS_CANCEL = 10; // 주문취소
	public static final int DEAL_STATUS_NEGO = 11; // 네고제안
	public static final int DEAL_STATUS_COUNTER_NEGO = 12; // 카운터네고제안
	
	// 정품인증가격
	public static final int DERIVERY_PRICE = 3900;
	
	// 결제방법
	public static final String PAY_METHOD_1 = "간편결제 신용카드";
	public static final String PAY_METHOD_2 = "일반결제 신용카드";
	public static final String PAY_METHOD_3 = "실시간계좌이체";
	public static final String PAY_METHOD_4 = "네이버페이";
	
	public static final int PAY_METHOD_1_VAL = 1;
	public static final int PAY_METHOD_2_VAL = 2;
	public static final int PAY_METHOD_3_VAL = 3;
	public static final int PAY_METHOD_4_VAL = 4;
	
	// 성별
	public static final int GENER_M = 1;
	public static final int GENER_W = 2;
}
