package com.kyad.selluv.brand;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.Brand;
import com.kyad.selluv.dbconnect.RecomBrand;

@Controller
@RequestMapping(value = "/brand/recommend_brand_mng")
public class RecommendBrandMng {
	public class RecomBrandModel {
		public Integer brand_uid;
		public String name_ko;

		public void setNameKo(String new_name_ko) {
			this.name_ko = new_name_ko;
		}
		
		public String getNameKo() {
			return this.name_ko;
		}
		
		public void setBrandUid(int new_uid) {
			this.brand_uid = new_uid;
		}
		
		public Integer getBrandUid() {
			return this.brand_uid;
		}
	}
	
	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		List<RecomBrand> list1 = RecomBrand.getListByPdtGroup(Constant.PDT_GROUP_MALE);
		List<RecomBrand> list2 = RecomBrand.getListByPdtGroup(Constant.PDT_GROUP_FEMALE);
		List<RecomBrand> list3 = RecomBrand.getListByPdtGroup(Constant.PDT_GROUP_KIDS);
		List<RecomBrandModel> model_list1 = new ArrayList<RecomBrandModel>();
		List<RecomBrandModel> model_list2 = new ArrayList<RecomBrandModel>();
		List<RecomBrandModel> model_list3 = new ArrayList<RecomBrandModel>();
		for (int i=0; i<list1.size(); i++) {
			model_list1.add(i, new RecomBrandModel());
		}
		for (int i=0; i<list1.size(); i++) {
			RecomBrandModel rmodel = new RecomBrandModel();
			rmodel.setBrandUid(list1.get(i).getBrandUid());
			Brand brand = Brand.getInstance(list1.get(i).getBrandUid());
			rmodel.setNameKo(brand.getNameKo());
			model_list1.set(list1.get(i).getBrandOrder() - 1, rmodel);
		}
		for (int i=0; i<list2.size(); i++) {
			model_list2.add(i, new RecomBrandModel());
		}
		for (int i=0; i<list2.size(); i++) {
			RecomBrandModel rmodel = new RecomBrandModel();
			rmodel.setBrandUid(list2.get(i).getBrandUid());
			Brand brand = Brand.getInstance(list2.get(i).getBrandUid());
			rmodel.setNameKo(brand.getNameKo());
			model_list2.set(list2.get(i).getBrandOrder() - 1, rmodel);
		}
		for (int i=0; i<list3.size(); i++) {
			model_list3.add(i, new RecomBrandModel());
		}
		for (int i=0; i<list3.size(); i++) {
			RecomBrandModel rmodel = new RecomBrandModel();
			rmodel.setBrandUid(list3.get(i).getBrandUid());
			Brand brand = Brand.getInstance(list3.get(i).getBrandUid());
			rmodel.setNameKo(brand.getNameKo());
			model_list3.set(list3.get(i).getBrandOrder() - 1, rmodel);
		}
		model.addAttribute("list1", model_list1);
		model.addAttribute("list2", model_list2);
		model.addAttribute("list3", model_list3);
		return "brand/recommend_brand_mng/index";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(ModelMap model,
			HttpServletRequest request, HttpSession session, String list1,
			String list2, String list3) {
		List<RecomBrand> rlist1 = RecomBrand.getListByPdtGroup(Constant.PDT_GROUP_MALE);
		List<RecomBrand> rlist2 = RecomBrand.getListByPdtGroup(Constant.PDT_GROUP_FEMALE);
		List<RecomBrand> rlist3 = RecomBrand.getListByPdtGroup(Constant.PDT_GROUP_KIDS);
		for (int i=0;i<rlist1.size();i++) {
			rlist1.get(i).setStatus(Constant.STATUS_REMOVED);
			rlist1.get(i).Update();
		}
		for (int i=0;i<rlist2.size();i++) {
			rlist2.get(i).setStatus(Constant.STATUS_REMOVED);
			rlist2.get(i).Update();
		}
		for (int i=0;i<rlist3.size();i++) {
			rlist3.get(i).setStatus(Constant.STATUS_REMOVED);
			rlist3.get(i).Update();
		}
		if (list1!=null && !list1.isEmpty()) {
			list1 = list1.substring(0, list1.length()-1);
			String[] model_list = list1.split(",");
			for (int i=0;i<model_list.length; i++) {
				RecomBrand brand = RecomBrand.getInstanceByPdtGroupAndBrand(Constant.PDT_GROUP_MALE, Integer.valueOf(model_list[i]));
				boolean is_new = false;
				if (brand == null) {
					is_new = true;
					brand = new RecomBrand();
					brand.setRegTime(new Timestamp(System.currentTimeMillis()));
					brand.setPdtGroup(Constant.PDT_GROUP_MALE);
					brand.setRecomBrandUid(0);
				}
				brand.setBrandOrder(i+1);
				brand.setBrandUid(Integer.valueOf(model_list[i]));
				brand.setStatus(Constant.STATUS_USE);
				if (is_new) {
					brand.Save();
				} else {
					brand.Update();
				}
			}
		}
		
		if (list2!=null && !list2.isEmpty()) {
			list2 = list2.substring(0, list2.length()-1);
			String[] model_list = list2.split(",");
			for (int i=0;i<model_list.length; i++) {
				RecomBrand brand = RecomBrand.getInstanceByPdtGroupAndBrand(Constant.PDT_GROUP_FEMALE, Integer.valueOf(model_list[i]));
				boolean is_new = false;
				if (brand == null) {
					is_new = true;
					brand = new RecomBrand();
					brand.setRegTime(new Timestamp(System.currentTimeMillis()));
					brand.setPdtGroup(Constant.PDT_GROUP_FEMALE);
					brand.setRecomBrandUid(0);
				}
				brand.setBrandOrder(i+1);
				brand.setBrandUid(Integer.valueOf(model_list[i]));
				brand.setStatus(Constant.STATUS_USE);
				if (is_new) {
					brand.Save();
				} else {
					brand.Update();
				}
			}
		}
		
		if (list3!=null && !list3.isEmpty()) {
			list3 = list3.substring(0, list3.length()-1);
			String[] model_list = list3.split(",");
			for (int i=0;i<model_list.length; i++) {
				RecomBrand brand = RecomBrand.getInstanceByPdtGroupAndBrand(Constant.PDT_GROUP_KIDS, Integer.valueOf(model_list[i]));
				boolean is_new = false;
				if (brand == null) {
					is_new = true;
					brand = new RecomBrand();
					brand.setRegTime(new Timestamp(System.currentTimeMillis()));
					brand.setPdtGroup(Constant.PDT_GROUP_KIDS);
					brand.setRecomBrandUid(0);
				}
				brand.setBrandOrder(i+1);
				brand.setBrandUid(Integer.valueOf(model_list[i]));
				brand.setStatus(Constant.STATUS_USE);
				if (is_new) {
					brand.Save();
				} else {
					brand.Update();
				}
			}
		}
		return "success";
	}
}
