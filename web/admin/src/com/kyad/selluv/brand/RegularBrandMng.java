package com.kyad.selluv.brand;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Brand;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/brand/regular_brand_mng")
public class RegularBrandMng {

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Brand.getCount(Constant.BRAND_REGULAR));
		return "brand/regular_brand_mng/regular_brand_list";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, String first_en,
			String first_ko, Integer sort) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "first_en", "name_en", "first_ko", "name_ko",
				"total_pdt_count", "male_count", "female_count", "kids_count",
				"brand_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status = " + Constant.BRAND_REGULAR;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " name_en like '%" + keyword + "%'";
			custom_where += " or name_ko like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!first_ko.isEmpty()) {
			custom_where += " and first_ko = '" + first_ko + "'";
		}

		if (!first_en.isEmpty()) {
			custom_where += " and first_en = '" + first_en + "'";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (sort == 1) {
			custom_where += " order by total_pdt_count desc";
		} else {
			custom_where += " order by first_en asc";
		}
		try {
			json = SSP
					.simple(request,
							response,
							"(select B.*, (male_count + female_count + kids_count) as total_pdt_count from (select A.*, "
									+ "(select count(pdt_uid) from pdt where pdt_group = 1 and brand_uid = A.brand_uid) as male_count, "
									+ "(select count(pdt_uid) from pdt where pdt_group = 2 and brand_uid = A.brand_uid) as female_count, "
									+ "(select count(pdt_uid) from pdt where pdt_group = 4 and brand_uid = A.brand_uid) as kids_count "
									+ "from brand as A) as B) as C", "brand_uid",
							columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/reg")
	public String RegHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		return "brand/regular_brand_mng/regular_brand_reg";
	}

	@RequestMapping(value = "/{brand_uid}/detail")
	public String DetailHandler(@PathVariable Integer brand_uid,
			ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Brand brand = Brand.getInstance(brand_uid);
		
		if (brand == null) {
			model.addAttribute("msg", "error");
			return "brand/regular_brand_mng/regular_brand_reg";
		}
		
		model.addAttribute("brand", brand);
		model.addAttribute("real_logo_img", StarmanHelper.getTargetImageUrl(
				Constant.DIR_NAME_BRAND, brand.getBrandUid(),
				brand.getLogoImg(), StarmanHelper.NO_IMAGE_PATH));
		model.addAttribute("real_profile_img", StarmanHelper.getTargetImageUrl(
				Constant.DIR_NAME_BRAND, brand.getBrandUid(),
				brand.getProfileImg(), StarmanHelper.NO_IMAGE_PATH));
		model.addAttribute("real_back_img", StarmanHelper.getTargetImageUrl(
				Constant.DIR_NAME_BRAND, brand.getBrandUid(),
				brand.getBackImg(), StarmanHelper.NO_IMAGE_PATH));
		return "brand/regular_brand_mng/regular_brand_reg";
	}

	@RequestMapping(value = "/{brand_uid}/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(@PathVariable Integer brand_uid, ModelMap model,
			HttpServletRequest request, HttpSession session, String first_en,
			String name_en, String first_ko, String name_ko,
			String license_url, String logo_img, String profile_img,
			String back_img) {
		Brand brand = Brand.getInstance(brand_uid);
		boolean is_new = false;
		if (brand == null) {
			brand = new Brand();
			is_new = true;
		}
		brand.setFirstEn(first_en);
		brand.setFirstKo(first_ko);
		brand.setLicenseUrl(license_url);
		brand.setNameEn(name_en);
		brand.setNameKo(name_ko);
		brand.setLicenseUrl(license_url);
		String oldLogoImg = brand.getLogoImg();
		String oldProfileImg = brand.getProfileImg();
		String oldBackImg = brand.getBackImg();
		brand.setLogoImg(logo_img);
		brand.setProfileImg(profile_img);
		brand.setBackImg(back_img);
		if (is_new) {
			brand.setBrandUid(0);
			brand.setRegTime(new Timestamp(System.currentTimeMillis()));
			brand.setPdtCount(0);
			brand.setUsrUid(0);
			brand.setTotalCount((long)0);
			brand.setTotalPrice((long)0);
			brand.setStatus(Constant.BRAND_REGULAR);
			if (brand.Save() == Constant.RESULT_SUCCESS) {
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BRAND,
						brand.getBrandUid(), logo_img);
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BRAND,
						brand.getBrandUid(), profile_img);
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BRAND,
						brand.getBrandUid(), back_img);
				return "success";
			}
		} else {
			brand.setStatus(Constant.BRAND_REGULAR);
			if (brand.Update() == Constant.RESULT_SUCCESS) {
				if (!logo_img.equals(oldLogoImg)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BRAND,
							brand.getBrandUid(), logo_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_BRAND,
							brand.getBrandUid(), oldLogoImg);
				}
				
				if (!profile_img.equals(oldProfileImg)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BRAND,
							brand.getBrandUid(), profile_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_BRAND,
							brand.getBrandUid(), oldProfileImg);
				}
				
				if (!back_img.equals(oldBackImg)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BRAND,
							brand.getBrandUid(), back_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_BRAND,
							brand.getBrandUid(), oldBackImg);
				}
				return "success";
			}
		}
		return "error";
	}

	@RequestMapping(value = "/{brand_uid}/remove")
	public @ResponseBody String RemoveHandler(@PathVariable Integer brand_uid,
			HttpSession session) {
		Brand brand = Brand.getInstance(brand_uid == null ? 0 : brand_uid);

		if (brand != null) {
			brand.setStatus(Constant.STATUS_REMOVED);
			if (brand.Update() == Constant.RESULT_SUCCESS)
				return "success";
		}

		return "error";
	}
}
