package com.kyad.selluv.brand;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Brand;

@Controller
@RequestMapping(value = "/brand/new_brand_mng")
public class NewBrandMng {

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Brand.getCount(Constant.BRAND_NEW));
		return "brand/new_brand_mng/new_brand_list";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_id", "name_en", "total_pdt_count", "reg_time",
				"brand_uid", "first_en", "usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status = " + Constant.BRAND_NEW;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " name_en like '%" + keyword + "%'";
			custom_where += " or usr_id like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		try {
			json = SSP
					.simple(request,
							response,
							"(select A.*, B.usr_id, (select count(pdt_uid) from pdt where brand_uid = A.brand_uid) as total_pdt_count from brand as A left join usr B on A.usr_uid = B.usr_uid) as C", "brand_uid",
							columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
}
