package com.kyad.selluv.usr;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kyad.selluv.dbconnect.*;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;

@Controller
@RequestMapping(value = "/usr/usr_mng")
public class UsrMng {

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Usr.getCount());
		return "usr/usr_mng/usr_list";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer sort) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_id", "usr_nck_nm", "usr_nm", "usr_phone",
				"point", "buy_count", "sell_count", "reg_time", "usr_uid",
				"price", "cash_price" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " usr_id like '%" + keyword + "%'";
			custom_where += " or usr_mail like '%" + keyword + "%'";
			custom_where += " or usr_nck_nm like '%" + keyword + "%'";
			custom_where += " or usr_nm like '%" + keyword + "%'";
			custom_where += " or usr_phone like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (sort == 1) {
			custom_where += " order by reg_time desc";
		} else {
			custom_where += " order by usr_id asc";
		}
		String table = "(select *, "
				+ "(select count(deal_uid) from deal where usr_uid = A.usr_uid and status >=1 and status <= 8) as buy_count, "
				+ "(select count(deal_uid) from deal where seller_usr_uid = A.usr_uid and status = 6) as sell_count, "
				+ "(select sum(price) from deal where usr_uid = A.usr_uid and status >=1 and status <= 8) as price, "
				+ "(select sum(cash_price) from deal where seller_usr_uid = A.usr_uid and status = 6) as cash_price "
				+ "from usr as A) as B";

		try {
			json = SSP.simple(request, response, table, "usr_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_pay_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxPayHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer usr_uid) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "deal_uid", "pdt_uid", "price",
				"pay_import_method", "pay_time" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("pay_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "usr_uid = " + usr_uid
				+ " and status >= 1 and status <= 8 order by pay_time desc";

		try {
			json = SSP.simple(request, response, "deal", "deal_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_cash_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxCashHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer usr_uid) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "deal_uid", "pdt_uid", "cash_price",
				"nicepay_code", "cash_comp_time" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("cash_comp_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "seller_usr_uid = " + usr_uid
				+ " and status = 6 order by cash_comp_time desc";

		try {
			json = SSP.simple(request, response, "deal", "deal_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_money_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxMoneyChangeHandler(
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Integer usr_uid) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "target", "pdt_uid", "amount", "kind",
				"reg_time", "target_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "usr_uid = " + usr_uid
				+ " order by reg_time desc";

		try {
			json = SSP
					.simple(request,
							response,
							"(select *, (case kind when 4 then '-' "
									+ "when 5 then (select usr_id from usr where usr_uid = target_uid) "
									+ "when 6 then (select usr_id from usr where usr_uid = target_uid) "
									+ "when 7 then (select usr_id from usr where usr_uid = target_uid) "
									+ "else target_uid end) as target from money_change_his) as A",
							"money_change_his_uid", columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/reg")
	public String RegHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		List<Bank> bank_list = Bank.getList();
		model.addAttribute("bank_list", bank_list);
		model.addAttribute("find_address_url", Constant.SELLUV_API_ADDRESS + "/web/address");
		return "usr/usr_mng/usr_reg";
	}

	@RequestMapping(value = "/{usr_uid}/detail")
	public String DetailHandler(@PathVariable Integer usr_uid, ModelMap model,
			HttpSession session, Integer buy_count, Integer sell_count,
			Integer price, Integer cash_price) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Usr usr = Usr.getInstance(usr_uid);
		if (usr == null) {
			model.addAttribute("msg", "error");
			return "usr/usr_mng/usr_detail";
		}
		model.addAttribute("usr", usr);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		model.addAttribute("birthday", sdf.format(usr.getBirthday()));
		model.addAttribute("reg_time", sdf.format(usr.getRegTime()));
		Integer type = usr.getUsrLoginType();
		if (type == 0)
			model.addAttribute("sns", "일반로그인");
		else if (type == 1)
			model.addAttribute("sns", "페이스북");
		else if (type == 2)
			model.addAttribute("sns", "네이버");
		else if (type == 3)
			model.addAttribute("sns", "카카오톡");
		model.addAttribute("login_count",
				String.valueOf(UsrLoginHis.getCountByUsrUid(usr_uid)));
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Timestamp last_login = UsrLoginHis.getLastLoginTime(usr_uid);
		if (last_login != null)
			model.addAttribute("login_time", sdf.format(last_login));
		model.addAttribute("buy_count", buy_count);
		model.addAttribute("sell_count", sell_count);
		model.addAttribute("price", price);
		model.addAttribute("cash_price", cash_price);
		model.addAttribute("pay_count", Deal.getPayCountByUsrUid(usr_uid));
		model.addAttribute("cash_count", Deal.getCashCountByUsrUid(usr_uid));
		model.addAttribute("money_count", MoneyChangeHis.getCountByUsrUid(usr_uid));
		UsrAddress address1 = UsrAddress
				.getInstanceByUsrUidAndOrder(usr_uid, 1);
		if (address1 != null) {
			String[] addressDetailArray = address1.getAddressDetail().split("\n");
			model.addAttribute("address1", addressDetailArray.length > 0 ? addressDetailArray[0] : "");
			model.addAttribute("addressDetail1", addressDetailArray.length > 1 ? addressDetailArray[1] : "");
			model.addAttribute("addressPostcode1", address1.getAddressDetailSub());
		}
		UsrAddress address2 = UsrAddress
				.getInstanceByUsrUidAndOrder(usr_uid, 2);
		if (address2 != null) {
			String[] addressDetailArray = address2.getAddressDetail().split("\n");
			model.addAttribute("address2", addressDetailArray.length > 0 ? addressDetailArray[0] : "");
			model.addAttribute("addressDetail2", addressDetailArray.length > 1 ? addressDetailArray[1] : "");
			model.addAttribute("addressPostcode2", address2.getAddressDetailSub());
		}
		UsrAddress address3 = UsrAddress
				.getInstanceByUsrUidAndOrder(usr_uid, 3);
		if (address3 != null) {
			String[] addressDetailArray = address3.getAddressDetail().split("\n");
			model.addAttribute("address3", addressDetailArray.length > 0 ? addressDetailArray[0] : "");
			model.addAttribute("addressDetail3", addressDetailArray.length > 1 ? addressDetailArray[1] : "");
			model.addAttribute("addressPostcode3", address3.getAddressDetailSub());
		}
		return "usr/usr_mng/usr_detail";
	}

	@RequestMapping(value = "/{usr_uid}/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(@PathVariable Integer usr_uid,
			ModelMap model, HttpServletRequest request, HttpSession session,
			String usr_id, String usr_pwd, String usr_mail, String usr_nm,
			String usr_phone, String usr_nck_nm, String address[],
			String address_detail[], Integer gender,
			String birthday, String bank_nm, String account_num, String memo) {
		Usr usr = Usr.getInstance(usr_uid);
		boolean is_new = false;
		if (usr == null) {
			usr = new Usr();
			is_new = true;
		}
		if (is_new) {
			List<Usr> temp = Usr.getListByUsrId(usr_id);
			if (temp.size() > 0)
				return "dup";
			usr.setUsrUid(0);
			usr.setRegTime(new Timestamp(System.currentTimeMillis()));
			usr.setEdtTime(new Timestamp(System.currentTimeMillis()));
			usr.setUsrId(usr_id);
			usr.setUsrPwd(usr_pwd);
			usr.setUsrMail(usr_mail);
			usr.setUsrNm(usr_nm);
			usr.setUsrNckNm(usr_nck_nm);
			usr.setUsrPhone(usr_phone);
			
			usr.setBankNm(bank_nm);
			usr.setAccountNum(account_num);
			usr.setBirthday(Date.valueOf(birthday));
			usr.setAccountNm("");
			usr.setGender(gender);
			usr.setProfileImg("");
			usr.setProfileBackImg("");
			usr.setDescription("");
			usr.setInviteUsrUid(0);
			usr.setPoint((long) 0);
			usr.setMoney((long) 0);
			usr.setUsrLoginType(0);
			usr.setSnsId("");
			usr.setLikeGroup(gender);
			usr.setAccessToken("");
			usr.setDeviceToken("");
			usr.setLoginTime(new Timestamp(System.currentTimeMillis()));
			usr.setSleepYn(0);
			usr.setFreeSendPrice((long) -1);
			usr.setSleepTime(new Timestamp(System.currentTimeMillis()));
			usr.setNoticeTime(new Timestamp(System.currentTimeMillis()));
			usr.setMemo("");
			usr.setStatus(Constant.STATUS_USE);
			if (usr.Save() == Constant.RESULT_SUCCESS) {
				for (int i=1; i<4; i++) {
					if (address[i - 1] != null && address_detail[i - 1] != null) {
						UsrAddress addr = new UsrAddress();
						addr.setUsrAddressUid(0);
						addr.setRegTime(new Timestamp(System.currentTimeMillis()));
						addr.setUsrUid(usr.getUsrUid());
						addr.setAddressOrder(i);
						addr.setAddressName(usr.getUsrNm());
						addr.setAddressPhone(usr.getUsrPhone());
						addr.setAddressDetail(address[i - 1]);
						addr.setAddressDetailSub(address_detail[i - 1]);
						addr.setStatus(Constant.STATUS_USE);
						addr.Save();
					}
				}
				UsrPushSetting usrPushSetting = new UsrPushSetting();
				usrPushSetting.setUsrUid(usr.getUsrUid());
				usrPushSetting.setRegTime(new Timestamp(System.currentTimeMillis()));
				usrPushSetting.setDealYn(1);
				usrPushSetting.setNewsYn(1);
				usrPushSetting.setReplyYn(1);
				usrPushSetting.setDealSetting("1111111111");
				usrPushSetting.setNewsSetting("111111111111");
				usrPushSetting.setReplySetting("11");
				usrPushSetting.setStatus(1);
				usrPushSetting.Save();
				return "success";
			}
		} else {
			usr.setEdtTime(new Timestamp(System.currentTimeMillis()));
			usr.setMemo(memo);
			if (usr.Update() == Constant.RESULT_SUCCESS)
				return "success";
		}
		return "error";
	}

	@RequestMapping(value = "/{usr_uid}/remove")
	public String RemoveHandler(@PathVariable Integer usr_uid,
			HttpSession session) {
		BaseHibernateDAO pdao = new BaseHibernateDAO();
		Transaction tx = pdao.getSession().beginTransaction();

		try {
			String sql = "update reply set status = 0 where usr_uid = "
					+ usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update report_pdt set status = 0 where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_pdt_like where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_pdt_wish where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_brand_like where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_feed where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_wish where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_alarm where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update report set status = 0 where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update qna set status = 0 where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update usr_card set status = 0 where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_like where usr_uid = " + usr_uid
					+ " or peer_usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_hide where usr_uid = " + usr_uid + " or peer_usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_block where usr_uid = " + usr_uid
					+ " or peer_usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete tp from theme_pdt tp join pdt p on tp.pdt_uid = p.pdt_uid where p.usr_uid = "
					+ usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete upw from usr_pdt_wish upw join pdt p on upw.pdt_uid = p.pdt_uid where p.usr_uid = "
					+ usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete upl from usr_pdt_like upl join pdt p on upl.pdt_uid = p.pdt_uid where p.usr_uid = "
					+ usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update reply r join pdt p on r.pdt_uid = p.pdt_uid set r.status = 0 where p.usr_uid = "
					+ usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update pdt set status = 0 where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_sns where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_push_setting where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update usr set status = 0 where usr_uid = " + usr_uid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}

		pdao.getSession().clear();
		pdao.getSession().close();
		return "redirect:/usr/usr_mng";
	}
}
