package com.kyad.selluv.statistics;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.Deal;

@Controller
@RequestMapping(value = "/statistics/deal_statistics")
public class DealStatistics {
	
	public class Graph {
		public String time;
		public Integer count;
		public Long price;
		
		public void setTime(String new_time) {
			this.time = new_time;
		}
		
		public String getTime() {
			return this.time;
		}
		
		public void setCount(Integer new_count) {
			this.count = new_count;
		}
		
		public Integer getCount() {
			return this.count;
		}
		
		public void setPrice(long new_price) {
			this.price = new_price;
		}
		
		public Long getPrice() {
			return this.price;
		}
	}

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		return "statistics/deal_statistics/index";
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String GetChildrenHandler(Integer period) {
		List<Graph> list = new ArrayList<Graph>();
		if (period == Constant.PERIOD_DAY) {
			Timestamp stamp = new Timestamp(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			for (int i=0; i<24; i++) {
				Graph graph = new Graph();
				String hh = String.valueOf(i);
				graph.setTime(hh + "시");
				graph.setCount(Deal.getCountByPeriod(sdf.format(stamp) + " " + hh));
				graph.setPrice(Deal.getPriceByPeriod(sdf.format(stamp) + " " + hh));
				list.add(graph);
			}
		} else if (period == Constant.PERIOD_WEEK) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			SimpleDateFormat sdf1 = new SimpleDateFormat("MM월");
			for (int i=6; i>=0; i--) {
				Graph graph = new Graph();
				Timestamp stamp = new Timestamp(System.currentTimeMillis() - (long)1000*3600*24*i);
				int day = stamp.getDate();
				String dd = String.valueOf(day);
				if (day<10)
					dd = "0" + day;
				graph.setTime(sdf1.format(stamp) + " " + dd + "일");
				graph.setCount(Deal.getCountByPeriod(sdf.format(stamp) + "-" + dd));
				graph.setPrice(Deal.getPriceByPeriod(sdf.format(stamp) + "-" + dd));
				list.add(graph);
			}
		} else if (period == Constant.PERIOD_MONTH) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			SimpleDateFormat sdf1 = new SimpleDateFormat("MM");
			for (int i=30; i>=0; i--) {
				Graph graph = new Graph();
				Timestamp stamp = new Timestamp(System.currentTimeMillis() - (long)1000*3600*24*i);
				int day = stamp.getDate();
				String dd = String.valueOf(day);
				if (day<10)
					dd = "0" + day;
				graph.setTime(sdf1.format(stamp) + "월" + dd + "일");
				graph.setCount(Deal.getCountByPeriod(sdf.format(stamp) + "-" + dd));
				graph.setPrice(Deal.getPriceByPeriod(sdf.format(stamp) + "-" + dd));
				list.add(graph);
			}
		} else if (period == Constant.PERIOD_YEAR) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			Timestamp stamp = new Timestamp(System.currentTimeMillis());
			int month = stamp.getMonth() + 1;
			int year = stamp.getYear() + 1900 - 1;
			for (int i=0; i<12; i++) {
				Graph graph = new Graph();
				month++;
				boolean is_new = false;
				if (month > 12) {
					month = 1;
					year++;
					is_new = true;
				}
				String mm = String.valueOf(month);
				if (month<10)
					mm = "0" + month;
				if (i == 0 || is_new)
					graph.setTime(year + "년 " + mm + "월");
				else
					graph.setTime(mm + "월");
				graph.setCount(Deal.getCountByPeriod(year + "-" + mm));
				graph.setPrice(Deal.getPriceByPeriod(year + "-" + mm));
				list.add(graph);
			}
		}
		String json = new Gson().toJson(list);
		return json;
	}
}
