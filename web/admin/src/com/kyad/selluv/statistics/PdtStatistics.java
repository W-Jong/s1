package com.kyad.selluv.statistics;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Brand;
import com.kyad.selluv.dbconnect.Category;

@Controller
@RequestMapping(value = "/statistics/pdt_statistics")
public class PdtStatistics {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Integer total_brand_count = Brand.getCount(Constant.BRAND_REGULAR);
		Integer total_category_count = Category.getChildCount();
		model.addAttribute("total_brand_count", total_brand_count);
		model.addAttribute("total_category_count", total_category_count);
		return "statistics/pdt_statistics/index";
	}

	@RequestMapping(value = "/ajax_brand_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxBrandHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer buyer,
			Integer standard, String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "brand_uid", "name_en", "deal_count",
				"deal_price" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			columns.add(column);
		}

		String json = "";

		String custom_where = "status = 1";

		if (standard != null) {
			if (standard == 1) {
				custom_where += " order by deal_count desc";
			} else {
				custom_where += " order by deal_price desc";
			}
		}

		String table = "(select *, "
				+ "(select count(deal_uid) from deal as D where D.status >= 1 and D.status <= 8 and pdt_uid in (select pdt_uid from pdt where brand_uid = A.brand_uid)";
		if (!period_from.isEmpty())
			table += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (buyer != null && buyer != 0) {
			table += " and (select gender from usr where usr_uid = D.usr_uid) = " + buyer;
		}
		if (!period_to.isEmpty())
			table += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";
		table += ") as deal_count, "
				+ "(select sum(price) from deal as D where D.status >= 1 and D.status <= 8 and pdt_uid in (select pdt_uid from pdt where brand_uid = A.brand_uid)";
		if (!period_from.isEmpty())
			table += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			table += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";
		if (buyer != null && buyer != 0) {
			table += " and (select gender from usr where usr_uid = D.usr_uid) = " + buyer;
		}
		table += ") as deal_price " + "from brand as A) as B";
		try {
			json = SSP.simple(request, response, table, "brand_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
	
	@RequestMapping(value = "/ajax_category_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxCategoryHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer buyer,
			Integer standard, String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "category_uid", "category_name", "deal_count",
				"deal_price" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			columns.add(column);
		}

		String json = "";

		String custom_where = "status = 1 and (select count(*) from category where parent_category_uid = B.category_uid and status = 1) = 0";

		if (standard != null) {
			if (standard == 1) {
				custom_where += " order by deal_count desc";
			} else {
				custom_where += " order by deal_price desc";
			}
		}

		String table = "(select *, "
				+ "(select count(deal_uid) from deal as D where D.status >= 1 and D.status <= 8 and pdt_uid in (select pdt_uid from pdt where category_uid = A.category_uid)";
		if (!period_from.isEmpty())
			table += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (buyer != null && buyer != 0) {
			table += " and (select gender from usr where usr_uid = D.usr_uid) = " + buyer;
		}
		if (!period_to.isEmpty())
			table += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";
		table += ") as deal_count, "
				+ "(select sum(price) from deal as D where D.status >= 1 and D.status <= 8 and pdt_uid in (select pdt_uid from pdt where category_uid = A.category_uid)";
		if (!period_from.isEmpty())
			table += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			table += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";
		if (buyer != null && buyer != 0) {
			table += " and (select gender from usr where usr_uid = D.usr_uid) = " + buyer;
		}
		table += ") as deal_price " + "from category as A) as B";
		try {
			json = SSP.simple(request, response, table, "category_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
}
