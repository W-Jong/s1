package com.kyad.selluv.statistics;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Usr;

@Controller
@RequestMapping(value = "/statistics/usr_statistics")
public class UsrStatistics {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Usr.getCount());
		return "statistics/usr_statistics/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer type,
			Integer gender, String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_uid", "usr_id", "gender",
				"buy_count", "sell_count", "price1", "cash_price1", "total_price" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			
			if (db_fields[i].compareTo("gender") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						if (Integer.parseInt(d) == Constant.GENER_M)
							return "남";
						else
							return "여";
					}
				});
			}
			
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";
		
		if (gender != null && gender != 0) {
			custom_where += " and gender = " + gender;
		}

		if (type != null) {
			if (type == 1) {
				custom_where += " order by total_price desc";
			} else if (type == 2) {
				custom_where += " order by price desc";
			} else {
				custom_where += " order by cash_price desc";
			}
		}

		String table = "(select *, (price1 + cash_price1) as total_price from "
				+ "(select *, if(price is null, 0, price) as price1, if(cash_price is null, 0, cash_price) as cash_price1 from "
				+ "(select *, "
				+ "(select count(deal_uid) from deal where usr_uid = A.usr_uid and status >=1 and status <= 8) as buy_count, "
				+ "(select count(deal_uid) from deal where seller_usr_uid = A.usr_uid and status = 6) as sell_count, "
				+ "(select sum(price) from deal where usr_uid = A.usr_uid and status >=1 and status <= 8) as price, "
				+ "(select sum(cash_price) from deal where seller_usr_uid = A.usr_uid and status = 6) as cash_price "
				+ "from usr as A) as B) as C) as D";
		try {
			json = SSP.simple(request, response, table, "usr_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
}
