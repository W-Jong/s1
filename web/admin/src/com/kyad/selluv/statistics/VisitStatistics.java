package com.kyad.selluv.statistics;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;

@Controller
@RequestMapping(value = "/statistics/visit_statistics")
public class VisitStatistics {
	
	public class Graph {
		public String time;
		public Integer newreg;
		public Integer usr;
		public Integer count;
		
		public void setTime(String new_time) {
			this.time = new_time;
		}
		
		public String getTime() {
			return this.time;
		}
		
		public void setNewreg(Integer nr) {
			this.newreg = nr;
		}
		
		public Integer getNewreg() {
			return this.newreg;
		}
		
		public void setUsr(Integer ur) {
			this.usr = ur;
		}
		
		public Integer getUsr() {
			return this.usr;
		}
		
		public void setCount(Integer new_count) {
			this.count = new_count;
		}
		
		public Integer getCount() {
			return this.count;
		}
	}

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		return "statistics/visit_statistics/index";
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String GetChildrenHandler(Integer period) {
		List<Graph> list = new ArrayList<Graph>();
		if (period == Constant.PERIOD_DAY) {
			Timestamp stamp = new Timestamp(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			for (int i=0; i<24; i++) {
				String hh = String.valueOf(i);
				
				Graph graph = getGraphByPeriod(sdf.format(stamp) + " " + hh);
				graph.setTime(hh + "시");
				list.add(graph);
			}
		} else if (period == Constant.PERIOD_WEEK) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			SimpleDateFormat sdf1 = new SimpleDateFormat("MM월");
			for (int i=6; i>=0; i--) {
				Timestamp stamp = new Timestamp(System.currentTimeMillis() - (long)1000*3600*24*i);
				int day = stamp.getDate();
				String dd = String.valueOf(day);
				if (day<10)
					dd = "0" + day;
				Graph graph = getGraphByPeriod(sdf.format(stamp) + "-" + dd);
				graph.setTime(sdf1.format(stamp) + " " + dd + "일");
				list.add(graph);
			}
		} else if (period == Constant.PERIOD_MONTH) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			SimpleDateFormat sdf1 = new SimpleDateFormat("MM");
			for (int i=30; i>=0; i--) {
				Timestamp stamp = new Timestamp(System.currentTimeMillis() - (long)1000*3600*24*i);
				int day = stamp.getDate();
				String dd = String.valueOf(day);
				if (day<10)
					dd = "0" + day;
				Graph graph = getGraphByPeriod(sdf.format(stamp) + "-" + dd);
				graph.setTime(sdf1.format(stamp) + "월" + dd + "일");
				list.add(graph);
			}
		} else if (period == Constant.PERIOD_YEAR) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			Timestamp stamp = new Timestamp(System.currentTimeMillis());
			int month = stamp.getMonth() + 1;
			int year = stamp.getYear() + 1900 - 1;
			for (int i=0; i<12; i++) {
				month++;
				boolean is_new = false;
				if (month > 12) {
					month = 1;
					year++;
					is_new = true;
				}
				String mm = String.valueOf(month);
				if (month<10)
					mm = "0" + month;
				Graph graph = getGraphByPeriod(year + "-" + mm);
				if (i == 0 || is_new)
					graph.setTime(year + "년 " + mm + "월");
				else
					graph.setTime(mm + "월");
				list.add(graph);
			}
		}
		String json = new Gson().toJson(list);
		return json;
	}
	
	public Graph getGraphByPeriod(String period) {
		String sqlStr = "SELECT newreg, usr, count FROM ((select count(*) as newreg from"
				+ " (select * from usr_login_his where reg_time like '%" + period + "%' and"
				+ " usr_uid not in (select usr_uid from usr_login_his where reg_time < '" + period + "') group by usr_uid) as D) as E,"
				+ " (SELECT count(*) AS count FROM usr_login_his WHERE reg_time LIKE '%" + period + "%') AS A,"
				+ " (SELECT count(*) AS usr"
				+ " FROM (SELECT * FROM usr_login_his WHERE reg_time LIKE '%" + period + "%' GROUP BY usr_uid)"
				+ " AS B) AS C)";
		Graph graph = new Graph();
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return graph;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
		} catch (Exception e) {
			e.printStackTrace();
			return graph;
		}
		
		try {
			if (rsData != null)
			while (rsData.next()) {
				graph.setNewreg(rsData.getInt(1));
				graph.setUsr(rsData.getInt(2));
				graph.setCount(rsData.getInt(3));
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return graph;
	}
}
