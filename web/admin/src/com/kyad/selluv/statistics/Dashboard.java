package com.kyad.selluv.statistics;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.Deal;
import com.kyad.selluv.dbconnect.Pdt;
import com.kyad.selluv.dbconnect.Qna;
import com.kyad.selluv.dbconnect.Report;
import com.kyad.selluv.dbconnect.ReportPdt;
import com.kyad.selluv.dbconnect.Share;
import com.kyad.selluv.dbconnect.Valet;

@Controller
@RequestMapping(value = "/dashboard")
public class Dashboard {
	public class Graph {
		public String time;
		public Integer newreg;
		public Integer usr;
		public Integer count;
		
		public void setTime(String new_time) {
			this.time = new_time;
		}
		
		public String getTime() {
			return this.time;
		}
		
		public void setNewreg(Integer nr) {
			this.newreg = nr;
		}
		
		public Integer getNewreg() {
			return this.newreg;
		}
		
		public void setUsr(Integer ur) {
			this.usr = ur;
		}
		
		public Integer getUsr() {
			return this.usr;
		}
		
		public void setCount(Integer new_count) {
			this.count = new_count;
		}
		
		public Integer getCount() {
			return this.count;
		}
	}
	
	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		//time
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Timestamp today = new Timestamp(System.currentTimeMillis());
		Timestamp yesterday = new Timestamp(System.currentTimeMillis()-(long)1000*3600*24);
		Timestamp last_week = new Timestamp(System.currentTimeMillis()-(long)1000*3600*24*7);
		model.addAttribute("reg_time", sdf.format(today));
		
		//usr
		SimpleDateFormat day_format = new SimpleDateFormat("yyyy-MM-dd");
		Graph graph = getGraphByPeriod(day_format.format(today));
		Integer newreg = graph.getNewreg();
		Integer usr = graph.getUsr();
		Integer newdeal = Deal.getCountByPeriod(day_format.format(today));
		Long price = Deal.getPriceByPeriod(day_format.format(today));
		model.addAttribute("newreg", newreg);
		model.addAttribute("usr", usr);
		model.addAttribute("newdeal", newdeal);
		model.addAttribute("price", price);
		
		//usr percent
		Graph graph_yesterday = getGraphByPeriod(day_format.format(yesterday));
		Integer newreg_yesterday = graph_yesterday.getNewreg();
		Integer usr_yesterday = graph_yesterday.getUsr();
		
		Graph graph_last_week = getGraphByPeriod(day_format.format(last_week));
		Integer newreg_last_week = graph_last_week.getNewreg();
		Integer usr_last_week = graph_last_week.getUsr();
		
		if (newreg_yesterday == 0)
			model.addAttribute("newreg_percent1", 100);
		else
			model.addAttribute("newreg_percent1", (newreg - newreg_yesterday) * 100 / newreg_yesterday);
		if (newreg_last_week == 0)
			model.addAttribute("newreg_percent2", 100);
		else
			model.addAttribute("newreg_percent2", (newreg - newreg_last_week) * 100 / newreg_last_week);
		if (usr_yesterday == 0)
			model.addAttribute("usr_percent1", 100);
		else
			model.addAttribute("usr_percent1", (usr - usr_yesterday) * 100 / usr_yesterday);
		if (usr_last_week == 0)
			model.addAttribute("usr_percent2", 100);
		else
			model.addAttribute("usr_percent2", (usr - usr_last_week) * 100 / usr_last_week);
		
		Integer newdeal_yesterday = Deal.getCountByPeriod(day_format.format(yesterday));
		Long price_yesterday = Deal.getPriceByPeriod(day_format.format(yesterday));
		Integer newdeal_last_week = Deal.getCountByPeriod(day_format.format(last_week));
		Long price_last_week = Deal.getPriceByPeriod(day_format.format(last_week));
		
		if (newdeal_yesterday == 0)
			model.addAttribute("newdeal_percent1", 100);
		else
			model.addAttribute("newdeal_percent1", (newdeal - newdeal_yesterday) * 100 / newdeal_yesterday);
		if (price_yesterday == 0)
			model.addAttribute("price_percent1", 100);
		else
			model.addAttribute("price_percent1", (price - price_yesterday) * 100 / price_yesterday);
		if (newdeal_last_week == 0)
			model.addAttribute("newdeal_percent2", 100);
		else
			model.addAttribute("newdeal_percent2", (newdeal - newdeal_last_week) * 100 / newdeal_last_week);
		if (price_last_week == 0)
			model.addAttribute("price_percent2", 100);
		else
			model.addAttribute("price_percent2", (price - price_last_week) * 100 / price_last_week);
		
		//deal
		Integer deal_cnt = 0;
		deal_cnt = Deal.getCount(Constant.DEAL_STATUS_DERIVERY_READY);
		model.addAttribute("derivery_ready_count", deal_cnt);
		
		deal_cnt = Deal.getCount(Constant.DEAL_STATUS_DERIVERY_DONE);
		model.addAttribute("derivery_done_count", deal_cnt);
		
		deal_cnt = Deal.getCount(Constant.DEAL_STATUS_DERIVERY);
		model.addAttribute("derivery_count", deal_cnt);
		
		deal_cnt = Deal.getCount(Constant.DEAL_STATUS_DEAL_DONE);
		model.addAttribute("deal_done_count", deal_cnt);
		
		deal_cnt = Deal.getCount(Constant.DEAL_STATUS_VERIFY);
		model.addAttribute("verify_count", deal_cnt);
		
		deal_cnt = Deal.getCount(Constant.DEAL_STATUS_REFUND_REQ);
		deal_cnt = deal_cnt + Deal.getCount(Constant.DEAL_STATUS_REFUND_CHECK);
		deal_cnt = deal_cnt + Deal.getCount(Constant.DEAL_STATUS_REFUND_DONE);
		model.addAttribute("refund_count", deal_cnt);
		
		//system
		model.addAttribute("pdt_count", Pdt.getCountByPhotoshopYn(Constant.PDT_PHOTOSHOP_NO));
		model.addAttribute("valet_count", Valet.getCountByStatus(Constant.VALET_ACCEPT));
		model.addAttribute("qna_count", Qna.getCountByStatus(Constant.QNA_STATUS_REQ));
		model.addAttribute("report_count", Report.getCountByStatus(Constant.REPORT_UNSOLVED));
		model.addAttribute("report_pdt_count", ReportPdt.getCountByStatus(Constant.REPORT_PDT_UNSOLVED));
		model.addAttribute("share_count", Share.getCountByStatus(Constant.SHARE_UNSOLVED));
		return "dashboard/index";
	}
	
	public Graph getGraphByPeriod(String period) {
		String sqlStr = "SELECT newreg, usr, count FROM ((select count(*) as newreg from"
				+ " (select * from usr_login_his where reg_time like '%" + period + "%' and"
				+ " usr_uid not in (select usr_uid from usr_login_his where reg_time < '" + period + "') group by usr_uid) as D) as E,"
				+ " (SELECT count(*) AS count FROM usr_login_his WHERE reg_time LIKE '%" + period + "%') AS A,"
				+ " (SELECT count(*) AS usr"
				+ " FROM (SELECT * FROM usr_login_his WHERE reg_time LIKE '%" + period + "%' GROUP BY usr_uid)"
				+ " AS B) AS C)";
		Graph graph = new Graph();
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return graph;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
		} catch (Exception e) {
			e.printStackTrace();
			return graph;
		}
		
		try {
			if (rsData != null)
			while (rsData.next()) {
				graph.setNewreg(rsData.getInt(1));
				graph.setUsr(rsData.getInt(2));
				graph.setCount(rsData.getInt(3));
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return graph;
	}
}
