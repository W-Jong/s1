package com.kyad.selluv.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.Adm;

@Controller
@RequestMapping(value = "/")
public class Login {

	@RequestMapping(value = { "/", ""})
	public String IndexHandler(ModelMap model, HttpServletRequest request) {
		return "redirect:/dashboard";
	}

	@RequestMapping(value = { "/login" })
	public String LoginHandler(ModelMap model, HttpServletRequest request,
							   HttpSession session) {
		Object flagLogout = session.getAttribute(Constant.SESSION_ADMIN_LOGOUT_FLAG);
		if (flagLogout != null) {
			model.addAttribute("referer", "/dashboard");
		} else {
			String referer = request.getHeader("referer");
			model.addAttribute("referer", (referer == null) ? "/dashboard" : referer);
		}
		session.removeAttribute(Constant.SESSION_ADMIN_LOGOUT_FLAG);
		return "login";
	}

	@RequestMapping(value = "/login/verify", method = RequestMethod.POST)
	public @ResponseBody int VerifyHandler(String id, String pwd,
			HttpSession session) {

		int res = Adm.Verify(id, pwd);
		if (res > 0) {
			session.setAttribute(Constant.SESSION_ADMIN_UID, res);
			session.setAttribute(Constant.SESSION_IS_LOGIN, "1");
			Adm adm = Adm.getInstance(res);
			session.setAttribute(Constant.SESSION_ADMIN_ID, adm.getAdmId());
			session.setAttribute(Constant.SESSION_ADMIN_NICK, adm.getAdmNck());
			session.setAttribute(Constant.SESSION_ADMIN_PRIVILEGE_UID,
					adm.getPrivilegeUid());
			return Constant.SIGNIN_SUCCESS;
		}
		return Constant.SIGNIN_ERR_WRONG_PWD;
	}

	@RequestMapping(value = "/pwd_change", method = RequestMethod.POST)
	public @ResponseBody String PwdChangeHandler(String old_pwd, String new_pwd,
			HttpSession session) {
		Adm adm = Adm.getInstance((Integer) session.getAttribute(Constant.SESSION_ADMIN_UID));
		if (!old_pwd.equals(adm.getAdmPwd()))
			return "wrong";
		adm.setAdmPwd(new_pwd);
		if (adm.Update() == Constant.RESULT_SUCCESS)
			return "success";
		return "error";
	}

	@RequestMapping(value = "/login/logout")
	public String LogoutHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		session.removeAttribute(Constant.SESSION_ADMIN_UID);
		session.removeAttribute(Constant.SESSION_IS_LOGIN);
		session.removeAttribute(Constant.SESSION_ADMIN_ID);
		session.removeAttribute(Constant.SESSION_ADMIN_NICK);
		session.removeAttribute(Constant.SESSION_ADMIN_PRIVILEGE_UID);
		session.setAttribute(Constant.SESSION_ADMIN_LOGOUT_FLAG, "1");
		return "redirect:/login";
	}
}
