package com.kyad.selluv.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class StarmanHelper {

    /**
     * upload등록부는 chown tomcat7:tomcat7 -R upload/ 로 사용자권한을 주어야 함.
     */
    public static final String UPLOAD_PATH = /*System.getProperty("catalina.home") + */"/selluv_upload";
    /**
     * application context파일에 설정추가
     * <mvc:resources location="file:/selluv_upload/" mapping="/uploads/**"/>
     */
    public static final String UPLOAD_URL_PATH = "http://13.125.35.211/uploads";
    public static final String NO_IMAGE_PATH = "/resources/dist/images/no-img.png";

    public static String getUniqueString() {
        return getUniqueString("");
    }

    public static String getUniqueString(String extension) {
        String strReturn = "" + System.currentTimeMillis() + generateRandomNumber(1000, 9999);
        if (extension != null && extension.length() != 0) {
            strReturn += "." + extension;
        }
        return strReturn;
    }

    public static int generateRandomNumber(int min, int max) {
        Double javaVersion = getJavaVersion();
        if (javaVersion < 1.7) {
            return min + (int) (Math.random() * ((max - min) + 1));
        } else {
            return ThreadLocalRandom.current().nextInt(min, max + 1);
        }
    }

    private static double getJavaVersion() {
        String version = System.getProperty("java.version");
        int pos = version.indexOf('.');
        pos = version.indexOf('.', pos + 1);
        return Double.parseDouble(version.substring(0, pos));
    }

    public static String makeDirectory(String path) {
        String[] dirs = path.split("/");

        String mkPath = UPLOAD_PATH;
        File file = new File(mkPath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        createDirectory(mkPath);

        for (String dir : dirs) {
            if (dir == null || dir.length() == 0)
                continue;

            mkPath += File.separator + dir;
            createDirectory(mkPath);
        }

        return mkPath;
    }

    private static void createDirectory(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
            file.setReadable(true, false);
            file.setWritable(true, false);
            file.setExecutable(true, false);
        }
    }

    public static void moveTargetFolder(String tableName, int uid, String fileName) {
        if (fileName == null || fileName.length() == 0)
            return;
        String filePath = makeDirectory(tableName + "/" + uid);
        String tempPath = makeDirectory("temp");
        File tempFile = new File(tempPath + "/" + fileName);
        File targetFile = new File(filePath + "/" + fileName);
        if (tempFile.exists()) {
            tempFile.renameTo(targetFile);
        }
    }

    public static void copyFromTargetFolder(String sourceTableName, int sourceUid, String sourceFileName, String destTableName, int destUid, String destFileName) {
        if (sourceFileName == null || sourceFileName.length() == 0 || destFileName == null || destFileName.length() == 0)
            return;
        String sourceFilePath = makeDirectory(sourceTableName + "/" + sourceUid);
        String destFilePath = makeDirectory(destTableName + "/" + destUid);
        File sourceFile = new File(sourceFilePath + "/" + sourceFileName);
        File destFile = new File(destFilePath + "/" + destFileName);
        if (sourceFile.exists()) {
            try {
                FileUtils.copyFile(sourceFile, destFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getTargetImageUrl(String tableName, int uid, String fileName, String defaultImagePath) //String defaultImagePath = "/assets/images/img_photo_default.png"
    {
        if (fileName == null || fileName.length() == 0)
            return defaultImagePath;
        if (fileName.startsWith("http://") || fileName.startsWith("https://"))
            return fileName;
        String returnStr = UPLOAD_URL_PATH + "/" + tableName + "/" + uid + "/" + fileName;
        File targetFile = new File(makeDirectory(tableName + "/" + uid) + "/" + fileName);
        return (targetFile.exists() && targetFile.isFile()) ? returnStr : defaultImagePath;
    }

    public static String getTargetImageUrl(String tableName, int uid, String fileName) {
        return getTargetImageUrl(tableName, uid, fileName, "");
    }

    public static boolean deleteTargetImage(String tableName, int uid, String fileName) {
        if (fileName == null || fileName.length() == 0)
            return true;

        File targetFile = new File(makeDirectory(tableName + "/" + uid) + "/" + fileName);
        return (targetFile.exists() && targetFile.isFile()) ? targetFile.delete() : true;
    }


    // 랜덤 알파벳문자열 생성
    public static String generateRandomString(int length) {
        String characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int charactersLength = characters.length();
        StringBuilder randomString = new StringBuilder();
        for (int i = 0; i < length; i++) {
            randomString.append(characters.charAt(generateRandomNumber(0, charactersLength - 1)));
        }
        return randomString.toString();
    }
}
