package com.kyad.selluv.helper;

import javax.servlet.http.HttpServletRequest;

public class CommonHelper {
   public static String getRemoteAddr(HttpServletRequest request) {
        if (request == null)
            return "";
        else
            return request.getRemoteAddr();
    }
}
