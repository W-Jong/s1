package com.kyad.selluv.deal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Deal;
import com.kyad.selluv.dbconnect.Usr;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/deal/deal_mng")
public class DealMng {
	@RequestMapping(value = { "/{index}/index", "" })
	public String IndexHandler(@PathVariable Integer index, ModelMap model,
			HttpServletRequest request, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";

		model.addAttribute("total_count", Deal.getCount(index));
		model.addAttribute("index", index);
		if (index == 0) {
			model.addAttribute("total_count", Deal.getCount());
			model.addAttribute("title", "거래리스트");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "송장번호", "주문상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 1) {
			model.addAttribute("title", "배송준비");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "주문취소예정", "주문상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 2) {
			model.addAttribute("title", "배송진행");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "송장번호", "주문상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 3) {
			model.addAttribute("title", "정품인증");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "인증전송장", "주문상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 4) {
			model.addAttribute("title", "배송완료");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "송장번호", "주문상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 5) {
			model.addAttribute("title", "거래완료");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "송장번호", "주문상태", "거래완료날짜",
					"계좌", "상세" });
			model.addAttribute("td_count", 10);
		} else if (index == 6) {
			model.addAttribute("title", "정산완료");
			model.addAttribute("td_fields",
					new String[] { "주문번호", "상품코드", "상품명", "구매자", "판매자", "결제금액",
							"송장번호", "주문상태", "거래완료날짜", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 7) {
			model.addAttribute("title", "반품과정");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "반품송장", "반품상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		} else if (index == 8) {
			model.addAttribute("total_count", Deal.getCount(10));
			model.addAttribute("title", "주문취소");
			model.addAttribute("td_fields", new String[] { "주문번호", "상품코드",
					"상품명", "구매자", "판매자", "결제금액", "취소사유", "주문상태",
					"주문일자<br>결제일자", "상세" });
			model.addAttribute("td_count", 9);
		}

		return "deal/deal_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer valet,
			Integer deal_status, Integer index) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();

		String[] db_fields = null;

		if (index == 0 || index == 2 || index == 4 || index == 6)
			db_fields = new String[] { "deal_uid", "pdt_uid", "pdt_title", "usr_uid", "seller_usr_uid",
					"usr_id", "seller_id", "price", "delivery_number",
					"status", "reg_time", "pay_time" };
		else if (index == 1)
			db_fields = new String[] { "deal_uid", "pdt_uid", "pdt_title", "usr_uid", "seller_usr_uid",
					"usr_id", "seller_id", "price", "cancel_time", "status",
					"reg_time", "pay_time" };
		else if (index == 3)
			db_fields = new String[] { "deal_uid", "pdt_uid", "pdt_title", "usr_uid", "seller_usr_uid",
					"usr_id", "seller_id", "price", "verify_delivery_number",
					"status", "reg_time", "pay_time" };
		else if (index == 5)
			db_fields = new String[] { "deal_uid", "pdt_uid", "pdt_title", "usr_uid", "seller_usr_uid",
					"usr_id", "seller_id", "price", "delivery_number",
					"status", "reg_time", "seller_uid", "pay_time" };
		else if (index == 7)
			db_fields = new String[] { "deal_uid", "pdt_uid", "pdt_title", "usr_uid", "seller_usr_uid",
					"usr_id", "seller_id", "price", "refund_delivery_number",
					"status", "reg_time", "pay_time" };
		else if (index == 8)
			db_fields = new String[] { "deal_uid", "pdt_uid", "pdt_title", "usr_uid", "seller_usr_uid",
					"usr_id", "seller_id", "price", "cancel_reason", "status",
					"reg_time", "pay_time" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0
					|| db_fields[i].compareTo("pay_time") == 0
					|| db_fields[i].compareTo("cancel_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}

			if (db_fields[i].compareTo("status") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						if (Integer.parseInt(d) == Constant.DEAL_STATUS_DERIVERY_READY)
							return "배송준비";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_DERIVERY)
							return "배송진행";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_VERIFY)
							return "정품인증";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_DERIVERY_DONE)
							return "배송완료";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_DEAL_DONE)
							return "거래완료";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_CASH_COMP)
							return "정산완료";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_REFUND_REQ)
							return "반품접수";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_REFUND_CHECK)
							return "반품승인";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_REFUND_DONE)
							return "반품완료";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_CANCEL)
							return "주문취소";
						else if (Integer.parseInt(d) == Constant.DEAL_STATUS_NEGO)
							return "네고제안";
						else
							return "카운터네고";
					}
				});
			}

			if (db_fields[i].compareTo("seller_uid") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						Usr usr = Usr.getInstance(Integer.parseInt(d));

						if (usr == null)
							return "N";

						if (usr.getAccountNum() == null
								|| usr.getAccountNum().compareTo("") == 0)
							return "N";

						return "Y";
					}
				});
			}

			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (pdt_title like '%" + keyword + "%' or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (valet == 1)
			custom_where += " and valet_uid <> 0";
		else if (valet == 2)
			custom_where += " and valet_uid = 0";

		if (deal_status != 0 && deal_status == 7)
			custom_where += " and (status = 7 or status = 8 or status = 9)";
		else if (deal_status != 0 && deal_status != 8)
			custom_where += " and status = " + deal_status;
		else if (deal_status == 8)
			custom_where += " and status = " + Constant.DEAL_STATUS_CANCEL;

		custom_where += " order by reg_time desc";

		String table = "(SELECT C.*, C.seller_usr_uid as seller_uid, D.usr_id as seller_id FROM (SELECT A.*, B.usr_id FROM deal as A LEFT JOIN usr as B on A.usr_uid = B.usr_uid) as C LEFT JOIN usr as D on C.seller_usr_uid = D.usr_uid) AS T";
		try {
			json = SSP.simple(request, response, table, "deal_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{deal_uid}/detail")
	public String DetailHandler(@PathVariable Integer deal_uid, ModelMap model,
			HttpSession session, Integer index) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Deal deal = Deal.getInstance(deal_uid);
		if (deal == null) {
			model.addAttribute("msg", "error");
			return "deal/deal_mng/detail";
		}

		try {
			Usr usr = Usr.getInstance(deal.getUsrUid());

			model.addAttribute("deal", deal);
			model.addAttribute("usr_id", usr.getUsrId());

			usr = Usr.getInstance(deal.getSellerUsrUid());
			model.addAttribute("seller_id", usr.getUsrId());
			model.addAttribute("seller", usr);

			model.addAttribute("delivery_price", Constant.DERIVERY_PRICE);

			String[] delivery = deal.getDeliveryNumber().split(" ");
			if (delivery.length > 1) {
				model.addAttribute("delivery", delivery[0]);
				model.addAttribute("delivery_number", delivery[1]);
			}

			String[] refund_delivery = deal.getRefundDeliveryNumber().split(" ");
			if (refund_delivery.length > 1) {
				model.addAttribute("refund_delivery", refund_delivery[0]);
				model.addAttribute("refund_delivery_number", refund_delivery[1]);
			}

			String recipient_address = deal.getRecipientAddress();
			int pos = recipient_address.lastIndexOf(" ");
			String address = recipient_address.substring(0, pos);
			String detail_address = recipient_address.substring(pos, recipient_address.length());
			model.addAttribute("address", address);
			model.addAttribute("detail_address", detail_address);

			if (deal.getPayImportMethod() == Constant.PAY_METHOD_1_VAL)
				model.addAttribute("pay_method", Constant.PAY_METHOD_1);
			else if (deal.getPayImportMethod() == Constant.PAY_METHOD_2_VAL)
				model.addAttribute("pay_method", Constant.PAY_METHOD_2);
			else if (deal.getPayImportMethod() == Constant.PAY_METHOD_3_VAL)
				model.addAttribute("pay_method", Constant.PAY_METHOD_3);
			else if (deal.getPayImportMethod() == Constant.PAY_METHOD_4_VAL)
				model.addAttribute("pay_method", Constant.PAY_METHOD_4);

			// 발렛코드
			if (deal.getValetUid() == 0)
				model.addAttribute("valet_code", "");
			else
				model.addAttribute("valet_code", deal.getValetUid());

			if (!deal.getRefundPhotos().isEmpty()) {
				String[] photos = deal.getRefundPhotos().split(",");
				List<String> refund_photos = new ArrayList<String>();
				for (int i = 0; i < photos.length; i++) {
					refund_photos.add(i, StarmanHelper.getTargetImageUrl(
							Constant.DIR_NAME_DEAL, deal.getDealUid(), photos[i],
							StarmanHelper.NO_IMAGE_PATH));
				}
				model.addAttribute("refund_photos", refund_photos);
			}
			// 결제금액
		} catch (Exception e) {

		}

		model.addAttribute("index", index);

		return "deal/deal_mng/detail";
	}

	@RequestMapping(value = "/{deal_uid}/save")
	public String SaveHandler(@PathVariable Integer deal_uid, ModelMap model,
			HttpSession session, Integer status, String memo, Integer index,
			String delivery, String delivery_number, String refund_delivery,
			String refund_delivery_number) {
		Deal deal = Deal.getInstance(deal_uid);

		if (deal == null)
			return "redirect:/deal/deal_mng/" + index + "/index";

        Timestamp nowTime = new Timestamp(System.currentTimeMillis());
		switch (status) {
		    //배송준비
			case 1:
				break;
            //배송진행
			case 2:
				break;
            //정품인증
			case 3:
				break;
            //배송완료
			case 4:
                deal.setDeliveryTime(nowTime);
				break;
            //거래완료
			case 5:
			    deal.setCompTime(nowTime);
				break;
            //정산완료
			case 6:
			    deal.setCashCompTime(nowTime);
				break;
            //반품접수
			case 7:
			    deal.setRefundReqTime(nowTime);
				break;
            //반품승인
			case 8:
			    deal.setRefundCheckTime(nowTime);
				break;
            //반품완료
			case 9:
			    deal.setRefundCompTime(nowTime);
				break;
            //주문취소
			case 10:
			    deal.setCancelReason("관리자 취소");
			    deal.setCancelTime(nowTime);
				break;
            default:
                break;
		}

		//정품인증상태에서 배송완료로 넘길때 구매자에게 푸시처리
        if (deal.getStatus() == 3 && status == 2) {
            HttpURLConnection conn = null;
            OutputStream outputStream = null;

            try {
                URL url = new URL(Constant.SELLUV_API_ADDRESS + "/version/alarm");

                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("adminToken", Constant.SELLUV_ADMIN_TOKEN);
                conn.setConnectTimeout(10000);

                String params = "";
                params += "pushType=PUSH02_B_DELIVERY_SENT";
                params += "&targetUid=" + deal_uid;

                outputStream = conn.getOutputStream();
                outputStream.write(params.getBytes("UTF-8"));
                outputStream.flush();

                int responseCode = conn.getResponseCode();

                StringBuilder responseReq = new StringBuilder();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        responseReq.append(line).append("\n");
                    }
                    br.close();
                } else {
                    responseReq.append("");
                }

                String result = responseReq.toString().trim();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (conn != null)
                    conn.disconnect();
            }
        }

		deal.setStatus(status);

		if (delivery != null && !delivery.isEmpty()) {
			deal.setDeliveryNumber(delivery + " " + delivery_number);
		}

//		deal.setRefundDeliveryNumber(refund_delivery + " "
//				+ refund_delivery_number);
		deal.setMemo(memo);

		deal.Update();

		model.addAttribute("index", index);

		return "redirect:/deal/deal_mng/" + index + "/index";
	}
}
