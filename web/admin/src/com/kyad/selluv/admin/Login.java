package com.kyad.selluv.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.Adm;

@Controller
@RequestMapping(value = "/")
public class Login {
	
	@RequestMapping(value = {"/", ""})	
	public String LoginHandler(ModelMap model,HttpServletRequest request) {
		return "login";
	}
	
	@RequestMapping(value = "/login/verify", method = RequestMethod.POST)
	public @ResponseBody
	int VerifyHandler(
			String id,
			String pwd,
			HttpSession session) {

		int res = Adm.Verify(id, pwd);
		if(res > 0) {
			session.setAttribute(Constant.SESSION_ADMIN_UID, res);
			session.setAttribute(Constant.SESSION_IS_LOGIN, "1");
			Adm adm = Adm.getInstance(res);
			session.setAttribute(Constant.SESSION_ADMIN_ID, adm.getAdmId());
			session.setAttribute(Constant.SESSION_ADMIN_NICK, adm.getAdmNck());
			session.setAttribute(Constant.SESSION_ADMIN_PRIVILEGE_UID, adm.getPrivilegeUid());
			return Constant.SIGNIN_SUCCESS;
		}
		return Constant.SIGNIN_ERR_WRONG_PWD;
	}
	
	@RequestMapping(value = "/login/logout")
	public String LogoutHandler(ModelMap model, HttpServletRequest request, HttpSession session) {
		session.removeAttribute(Constant.SESSION_ADMIN_UID);
		session.removeAttribute(Constant.SESSION_IS_LOGIN);
		session.removeAttribute(Constant.SESSION_ADMIN_ID);
		session.removeAttribute(Constant.SESSION_ADMIN_NICK);
		session.removeAttribute(Constant.SESSION_ADMIN_PRIVILEGE_UID);
		return "redirect:/admin";
	}
}
