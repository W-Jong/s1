package com.kyad.selluv.admin;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/pdt")
public class Pdt {
	
	@RequestMapping(value = {"/", ""})	
	public String LoginHandler(ModelMap model,HttpServletRequest request) {
		return "pdt/pdt_list";
	}
}
