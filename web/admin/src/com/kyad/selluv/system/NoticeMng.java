package com.kyad.selluv.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Notice;

@Controller
@RequestMapping(value = "/system/notice_mng")
public class NoticeMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Notice.getCount());
		return "system/notice_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer active_status) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "title", "reg_time", "cu_time", "notice_uid" };

		String cu_time = new Timestamp(System.currentTimeMillis()).toString();
		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and ((title like '%" + keyword + "%' or 1=0)";
			custom_where += " or (content like '%" + keyword + "%' or 1=0))";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (active_status == 2) {
			custom_where += " and (start_time = '1970-01-01 09:00:00' or (start_time < '" + cu_time
					+ "' and end_time > '" + cu_time + "'))";
		}
		else if (active_status == 1)
			custom_where += " and (end_time != '2099-12-31 09:00:00' and (end_time < '" + cu_time + "'))";

		custom_where += " order by reg_time desc";

		String table = "(select *, IF (start_time='1970-01-01 09:00:00', '', CONCAT(substr(start_time, 1, 10),'<br>',substr(end_time, 1, 10))) as cu_time FROM notice) as G";
		try {
			json = SSP.simple(request, response, table, "notice_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{notice_uid}/detail", method = RequestMethod.GET)
	public String DetailHandler(@PathVariable Integer notice_uid,
			ModelMap model, HttpSession session, HttpServletRequest request) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Notice notice = Notice.getInstance(notice_uid);
		model.addAttribute("detail_uid", notice_uid);
		if (notice == null && notice_uid != 0) {
			model.addAttribute("msg", "error");
			return "system/notice_mng/detail";
		} else if (notice != null) {
			model.addAttribute("title", notice.getTitle());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (notice.getStartTime() == null || notice.getStartTime().equals(Timestamp.valueOf("1970-01-01 09:00:00")))
				model.addAttribute("start_time", "");
			else
				model.addAttribute("start_time", sdf.format(notice.getStartTime()));
			if (notice.getEndTime() == null || notice.getEndTime().equals(Timestamp.valueOf("2099-12-31 09:00:00")))
				model.addAttribute("end_time", "");
			else
				model.addAttribute("end_time", sdf.format(notice.getEndTime()));
			model.addAttribute("content", notice.getContent());
		}

		return "system/notice_mng/detail";
	}

	@RequestMapping(value = "/delete_notice", produces = "application/json; charset=UTF-8")
	public @ResponseBody int DeleteNoticeHandler(Integer notice_uid,
			HttpSession session) {
		Notice notice = Notice.getInstance(notice_uid);
		if (notice == null)
			return Constant.RESULT_FAILED;

		notice.setStatus(Constant.STATUS_REMOVED);

		return notice.Update();
	}

	@RequestMapping(value = "/save", produces = "application/json; charset=UTF-8")
	public @ResponseBody Integer SaveHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session,
			Integer notice_uid, String title, String period_from,
			String period_to, String content) {
		if (notice_uid == 0) {
			Notice notice = new Notice();

			notice.setRegTime(new Timestamp(System.currentTimeMillis()));
			notice.setTitle(title);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (period_from == null || period_from.compareTo("") == 0) {
				notice.setStartTime(new Timestamp((long)0));
			} else {
				Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
				notice.setStartTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			if (period_to == null || period_to.compareTo("") == 0) {
				Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
				notice.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			} else {
				Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
				notice.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			}

			notice.setContent(content);

			notice.setStatus(Constant.STATUS_USE);
			int resultDB = notice.Save();

			Calendar cal = GregorianCalendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			long todayTwelve = cal.getTime().getTime();

			if (notice.getNoticeUid() > 0 && notice.getStartTime().getTime() >= todayTwelve &&
					notice.getStartTime().getTime() < (todayTwelve + 24 * 3600 * 1000)) {
				HttpURLConnection conn = null;
				OutputStream outputStream = null;

				try {
					URL url = new URL(Constant.SELLUV_API_ADDRESS + "/version/alarm");

					conn = (HttpURLConnection) url.openConnection();
					conn.setDoInput(true);
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("adminToken", Constant.SELLUV_ADMIN_TOKEN);
					conn.setConnectTimeout(10000);

					String params = "";
					params += "pushType=PUSH23_NOTICE";
					params += "&targetUid=" + notice.getNoticeUid();

					outputStream = conn.getOutputStream();
					outputStream.write(params.getBytes("UTF-8"));
					outputStream.flush();

					int responseCode = conn.getResponseCode();

					StringBuilder responseReq = new StringBuilder();
					if (responseCode == HttpURLConnection.HTTP_OK) {
						String line;
						BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						while ((line = br.readLine()) != null) {
							responseReq.append(line).append("\n");
						}
						br.close();
					} else {
						responseReq.append("");
					}

					String result = responseReq.toString().trim();

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (outputStream != null) {
						try {
							outputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if (conn != null)
						conn.disconnect();
				}
			}

			return resultDB;
		} else {
			Notice notice = Notice.getInstance(notice_uid);

			if (notice == null)
				return Constant.RESULT_FAILED;

			notice.setTitle(title);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (period_from == null || period_from.compareTo("") == 0) {
				notice.setStartTime(new Timestamp((long)0));
			} else {
				Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
				notice.setStartTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			if (period_to == null || period_to.compareTo("") == 0) {
				Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
				notice.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			} else {
				Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
				notice.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			}

			notice.setContent(content);
			notice.setStatus(Constant.STATUS_USE);

			return notice.Update();
		}
	}
}
