package com.kyad.selluv.system;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Faq;

@Controller
@RequestMapping(value = "/system/faq_mng")
public class FaqMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Faq.getCount());

		return "system/faq_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer type) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "kind", "title", "reg_time", "faq_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (title like '%" + keyword
					+ "%' or content like '%" + keyword + "%' or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (type != 0)
			custom_where += " and kind ='" + type + "'";

		custom_where += " order by reg_time desc";

		String table = "(select * FROM faq) as G";
		try {
			json = SSP.simple(request, response, table, "faq_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{faq_uid}/detail", method = RequestMethod.GET)
	public String DetailHandler(@PathVariable Integer faq_uid, ModelMap model,
			HttpSession session, HttpServletRequest request) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Faq faq = Faq.getInstance(faq_uid);
		model.addAttribute("detail_uid", faq_uid);

		if (faq == null && faq_uid != 0) {
			model.addAttribute("msg", "error");
			return "system/faq_mng/detail";
		} else if (faq != null) {
			model.addAttribute("type", faq.getKind());
			model.addAttribute("title", faq.getTitle());
			model.addAttribute("content", faq.getContent());
		}

		return "system/faq_mng/detail";
	}

	@RequestMapping(value = "/delete_faq", produces = "application/json; charset=UTF-8")
	public @ResponseBody int DeleteFaqHandler(Integer faq_uid,
			HttpSession session) {
		Faq faq = Faq.getInstance(faq_uid);
		if (faq == null)
			return Constant.RESULT_FAILED;

		faq.setStatus(Constant.STATUS_REMOVED);

		return faq.Update();
	}

	@RequestMapping(value = "/save", produces = "application/json; charset=UTF-8")
	public @ResponseBody Integer SaveHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer type,
			Integer faq_uid, String title, String content) {
		if (faq_uid == 0) {
			Faq faq = new Faq();

			faq.setRegTime(new Timestamp(System.currentTimeMillis()));
			faq.setKind(type);
			faq.setTitle(title);
			faq.setContent(content);
			faq.setStatus(Constant.STATUS_USE);

			return faq.Save();
		} else {
			Faq faq = Faq.getInstance(faq_uid);

			if (faq == null)
				return Constant.RESULT_FAILED;

			faq.setKind(type);
			faq.setTitle(title);
			faq.setContent(content);
			faq.setStatus(Constant.STATUS_USE);

			return faq.Update();
		}
	}
}
