package com.kyad.selluv.system;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Qna;
import com.kyad.selluv.dbconnect.Usr;

@Controller
@RequestMapping(value = "/system/qna_mng")
public class QnaMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session, Integer status) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Qna.getCount());
		if (status != null && status != 0)
			model.addAttribute("status", status);
		return "system/qna_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer status) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_id", "title", "status", "reg_time",
				"comp_time", "qna_uid", "usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0 || db_fields[i].compareTo("comp_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (usr_id like '%" + keyword + "%'";
			custom_where += " or title like '%" + keyword + "%'";
			custom_where += " or content like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (status != 0)
			custom_where += " and status =" + status;

		custom_where += " order by reg_time desc";

		String table = "(SELECT A.*, B.usr_id FROM qna as A left join usr as B on A.usr_uid = B.usr_uid) as G";
		try {
			json = SSP.simple(request, response, table, "qna_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/detail", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String DetailHandler(Integer qna_uid,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";

		Qna qna = Qna.getInstance(qna_uid);
		if (qna == null)
			return "";

		QnaDetail qna_detail = new QnaDetail();

		qna_detail.qna_uid = qna.getQnaUid();

		qna_detail.usr_id = Usr.getInstance(qna.getUsrUid()).getUsrId();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Timestamp ts = Timestamp.valueOf(qna.getRegTime().toString());

		qna_detail.reg_time = sdf.format(ts);
		qna_detail.title = qna.getTitle();
		qna_detail.content = qna.getContent();
		qna_detail.answer = qna.getAnswer();
		qna_detail.memo = qna.getMemo();

		return new Gson().toJson(qna_detail);
	}

	@RequestMapping(value = "/answer", method = RequestMethod.POST)
	public @ResponseBody Integer AnswerHandler(Integer qna_uid, String answer, String memo,
			HttpSession session) {

		Qna qna = Qna.getInstance(qna_uid);
		if (qna == null)
			return Constant.RESULT_FAILED;
		qna.setStatus(Constant.QNA_STATUS_DONE);
		qna.setAnswer(answer);
		qna.setCompTime(new Timestamp(System.currentTimeMillis()));
		qna.setMemo(memo);
		return qna.Update();
	}
}

class QnaDetail {
	public Integer qna_uid;
	public String usr_id;
	public String reg_time;
	public String title;
	public String content;
	public String answer;
	public String memo;
}
