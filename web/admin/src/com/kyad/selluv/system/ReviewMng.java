package com.kyad.selluv.system;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Review;

@Controller
@RequestMapping(value = "/system/review_mng")
public class ReviewMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Review.getCount());
		return "system/review_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer type) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "deal_uid", "usr_id", "type", "content",
				"point", "reg_time", "review_uid", "usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0
					|| db_fields[i].compareTo("edt_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (usr_id like '%" + keyword + "%'";
			custom_where += " or content like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (type != 0)
			custom_where += " and type =" + type;

		custom_where += " order by reg_time desc";

		String table = "(SELECT A.*, B.usr_id FROM review as A LEFT JOIN usr as B on A.usr_uid = B.usr_uid) as G";
		try {
			json = SSP.simple(request, response, table, "review_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/delete_review", method = RequestMethod.POST)
	public @ResponseBody Integer SaveHandler(Integer review_uid,
			HttpSession session) {
		Review report = Review.getInstance(review_uid);
		if (report == null)
			return Constant.RESULT_FAILED;

		report.setStatus(Constant.STATUS_REMOVED);

		return report.Update();
	}
}
