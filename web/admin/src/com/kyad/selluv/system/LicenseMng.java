package com.kyad.selluv.system;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.License;

@Controller
@RequestMapping(value = "/system/license_mng")
public class LicenseMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		List<License> license = License.getList();
		model.addAttribute("license", license);
		return "system/license_mng/index";
	}

	@RequestMapping(value = "/update_license", produces = "application/json; charset=UTF-8")
	public @ResponseBody Integer UpdateHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String license[]) {
		for (int i = 0; i < license.length; i++) {
			License inst = License.getInstance(i + 1);
			boolean is_new = false;
			if (inst == null) {
				is_new = true;
				inst = new License();
				inst.setLicenseUid(i+1);
			}
			inst.setRegTime(new Timestamp(System.currentTimeMillis()));
			inst.setEdtTime(new Timestamp(System.currentTimeMillis()));
			inst.setContent(license[i]);
			inst.setStatus(Constant.STATUS_USE);
			if (is_new) {
				inst.Save();
			} else {
				inst.Update();
			}
		}
		return 1;
	}
}
