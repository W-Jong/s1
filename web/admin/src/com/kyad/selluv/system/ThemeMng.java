package com.kyad.selluv.system;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kyad.selluv.dbconnect.BaseHibernateDAO;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Theme;
import com.kyad.selluv.dbconnect.ThemePdt;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/system/theme_mng")
public class ThemeMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Theme.getCount());
		return "system/theme_mng/index";
	}

	@RequestMapping(value = "/ajax_theme_pdt_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxThemePdtHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer theme_uid) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "pdt_uid", "profile_img", "pdt_model",
				"usr_nck_nm", "price", "status", "reg_time", "promotion_code",
				"edt_time", "name_ko", "pdt_group", "color_name",
				"category_name", "theme_pdt_uid", "memo", "usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0
					|| db_fields[i].compareTo("edt_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}

			else if (db_fields[i].compareTo("memo") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return StarmanHelper.UPLOAD_URL_PATH;
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "theme_uid = " + theme_uid;
		try {
			json = SSP
					.simple(request,
							response,
							"(select B.theme_pdt_uid, B.theme_uid, A.*, "
									+ "(select usr_nck_nm from usr where usr_uid = A.usr_uid) as usr_nck_nm, "
									+ "(select name_ko from brand where brand_uid = A.brand_uid) as name_ko, "
									+ "(select name_en from brand where brand_uid = A.brand_uid) as name_en, "
									+ "(select category_name from category where category_uid = A.category_uid) as category_name "
									+ "FROM theme_pdt as B LEFT JOIN pdt A ON B.pdt_uid = A.pdt_uid order by theme_pdt_uid desc) as C",
							"theme_pdt_uid", columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer type,
			Integer active_status) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "kind", "title_ko", "reg_time", "cu_time",
				"theme_uid" };

		String cu_time = new Timestamp(System.currentTimeMillis()).toString();
		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " title_ko like '%" + keyword + "%'";
			custom_where += " or title_en like '%" + keyword + "%'";
			custom_where += " or pdt_uid like '%" + keyword + "%'";
			custom_where += " or pdt_name like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (active_status == 2) {
			custom_where += " and (start_time = '1970-01-01 09:00:00' or (start_time < '"
					+ cu_time + "' and end_time > '" + cu_time + "'))";
		} else if (active_status == 1)
			custom_where += " and (end_time != '2099-12-31 09:00:00' and (end_time < '"
					+ cu_time + "'))";

		if (type != 0)
			custom_where += " and kind =" + type;

		custom_where += " order by reg_time desc";

		String table = "(select B.*, CONCAT_WS(' ',name_ko,pdt_group,color_name,pdt_model,category_name) as pdt_name "
				+ "from (select A.*,  P.pdt_uid, (select name_ko from brand where brand_uid = P.brand_uid) as name_ko, "
				+ "(select category_name from category where P.category_uid = category_uid) as category_name, "
				+ "(CASE P.pdt_group WHEN 1 THEN '남성' WHEN 2 THEN '여성' WHEN 4 THEN'키즈' END) as pdt_group,P.color_name,P.pdt_model, "
				+ "IF (A.start_time='1970-01-01 09:00:00', '', CONCAT(substr(A.start_time, 1, 10),'<br>',substr(A.end_time, 1, 10))) as cu_time "
				+ "from theme as A left join theme_pdt T on A.theme_uid = T.theme_uid and T.status<> 0 left join pdt P on P.pdt_uid = T.pdt_uid and "
				+ "P.status<> 0) as B where status <> 0 group by theme_uid) as C";
		try {
			json = SSP.simple(request, response, table, "theme_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{theme_uid}/save")
	public @ResponseBody String SaveHandler(@PathVariable Integer theme_uid,
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session, String title_ko, String title_en,
			String period_from, String period_to, String profile_img,
			Integer kind) {
		boolean is_new = false;
		Theme theme = Theme.getInstance(theme_uid);
		if (theme == null) {
			is_new = true;
			theme = new Theme();
			theme.setThemeUid(0);
			theme.setRegTime(new Timestamp(System.currentTimeMillis()));
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (period_from == null || period_from.compareTo("") == 0) {
			theme.setStartTime(new Timestamp((long) 0));
		} else {
			Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
			theme.setStartTime(Timestamp.valueOf(sdf.format(ts)));
		}

		if (period_to == null || period_to.compareTo("") == 0) {
			Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
			theme.setEndTime(Timestamp.valueOf(sdf.format(ts)));
		} else {
			Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
			theme.setEndTime(Timestamp.valueOf(sdf.format(ts)));
		}

		theme.setKind(kind);
		String oldProfileImg = theme.getProfileImg();
		theme.setProfileImg(profile_img);
		theme.setStatus(Constant.STATUS_USE);
		theme.setTitleKo(title_ko);
		theme.setTitleEn(title_en);
		if (is_new) {
			if (theme.Save() == Constant.RESULT_SUCCESS) {
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_THEME,
						theme.getThemeUid(), profile_img);
				BaseHibernateDAO pdao = new BaseHibernateDAO();
				Transaction tx = pdao.getSession().beginTransaction();
				String sql = "update theme_pdt set theme_uid = " + theme.getThemeUid() + " where theme_uid = 0";
				try {
					pdao.getSession().createSQLQuery(sql).executeUpdate();
					tx.commit();
				} catch (HibernateException e) {
					e.printStackTrace();
					tx.rollback();
				}
				pdao.getSession().clear();
				pdao.getSession().close();
			}
		} else {
			if (theme.Update() == Constant.RESULT_SUCCESS) {
				if (!oldProfileImg.equals(profile_img)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_THEME,
							theme.getThemeUid(), profile_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_THEME,
							theme.getThemeUid(), oldProfileImg);
				}
			}
		}
		return "success";
	}

	@RequestMapping(value = "/{theme_uid}/delete")
	public @ResponseBody String DeleteHandler(@PathVariable Integer theme_uid,
			HttpSession session) {
		Theme theme = Theme.getInstance(theme_uid);
		theme.setStatus(Constant.STATUS_REMOVED);
		if (theme.Update() == Constant.RESULT_SUCCESS) {
			return "success";
		}
		return "error";
	}

	@RequestMapping(value = "/{theme_uid}/detail")
	public String DetailHandler(@PathVariable Integer theme_uid,
			ModelMap model, HttpSession session, HttpServletRequest request) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("theme_uid", theme_uid);
		Theme theme = Theme.getInstance(theme_uid);
		if (theme == null && theme_uid != 0) {
			model.addAttribute("msg", "error");
			return "system/theme_mng/detail";
		} else if (theme != null) {
			model.addAttribute("theme", theme);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (theme.getStartTime() == null
					|| theme.getStartTime().equals(
							Timestamp.valueOf("1970-01-01 09:00:00")))
				model.addAttribute("start_time", "");
			else
				model.addAttribute("start_time",
						sdf.format(theme.getStartTime()));
			if (theme.getEndTime() == null
					|| theme.getEndTime().equals(
							Timestamp.valueOf("2099-12-31 09:00:00")))
				model.addAttribute("end_time", "");
			else
				model.addAttribute("end_time", sdf.format(theme.getEndTime()));
			model.addAttribute("real_profile_img", StarmanHelper
					.getTargetImageUrl(Constant.DIR_NAME_THEME,
							theme.getThemeUid(), theme.getProfileImg(),
							StarmanHelper.NO_IMAGE_PATH));
			model.addAttribute("profile_img", theme.getProfileImg());
		} else { //theme_uid == 0
			BaseHibernateDAO pdao = new BaseHibernateDAO();
			Transaction tx = pdao.getSession().beginTransaction();
			String sql = "delete from theme_pdt where theme_uid = 0";
			try {
				pdao.getSession().createSQLQuery(sql).executeUpdate();
				tx.commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				tx.rollback();
			}
			pdao.getSession().clear();
			pdao.getSession().close();
		}
		return "system/theme_mng/detail";
	}

	@RequestMapping(value = "/add_pdt")
	public @ResponseBody String AddPdtHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session,
			Integer theme_uid, Integer pdt_uid) {
		ThemePdt temp = ThemePdt.getInstanceByThemeAndPdt(theme_uid, pdt_uid);
		if (temp != null)
			return "dup";
		ThemePdt theme_pdt = new ThemePdt();
		theme_pdt.setRegTime(new Timestamp(System.currentTimeMillis()));
		theme_pdt.setThemePdtUid(0);
		theme_pdt.setThemeUid(theme_uid);
		theme_pdt.setPdtUid(pdt_uid);
		theme_pdt.setStatus(Constant.STATUS_USE);
		if (theme_pdt.Save() == Constant.RESULT_SUCCESS) {
			return "success";
		}
		return "error";
	}

	@RequestMapping(value = "/delete_pdt")
	public @ResponseBody String DeletePdtHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session,
			Integer theme_pdt_uid) {
		ThemePdt temp = ThemePdt.getInstance(theme_pdt_uid);
		if (temp.Delete() == Constant.RESULT_SUCCESS) {
			return "success";
		}
		return "error";
	}
}
