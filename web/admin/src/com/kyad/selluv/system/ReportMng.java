package com.kyad.selluv.system;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Report;

@Controller
@RequestMapping(value = "/system/report_mng")
public class ReportMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session, Integer status) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Report.getCount());
		if (status != null && status != 0)
			model.addAttribute("status", status);
		return "system/report_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer status) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_id", "peer_usr_id", "pdt_uid", "type",
				"content", "status", "reg_time", "report_uid", "usr_uid", "peer_usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0
					|| db_fields[i].compareTo("edt_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			
			if (db_fields[i].compareTo("pdt_uid") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						if (d == null || d.compareTo("0") == 0)
							return "-";
						
						return d;
					}
				});
			}
			
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " pdt_uid like '%" + keyword + "%'";
			custom_where += " or usr_id like '%" + keyword + "%'";
			custom_where += " or peer_usr_id like '%" + keyword + "%'";
			custom_where += " or memo like '%" + keyword + "%'";
			custom_where += " or content like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (status != 0)
			custom_where += " and status =" + status;
		custom_where += " order by reg_time desc";

		String table = "(SELECT A.*, "
				+ "(select usr_id from usr where A.usr_uid = usr.usr_uid) as usr_id, "
				+ "(select usr_id from usr where A.peer_usr_uid = usr.usr_uid) as peer_usr_id "
				+ "FROM report as A) as B";
		try {
			json = SSP.simple(request, response, table, "report_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/detail", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String DetailHandler(Integer report_uid,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";

		Report report = Report.getInstance(report_uid);
		if (report == null)
			return "";

		List<ReportDetail> report_list = new ArrayList<ReportDetail>();

		ReportDetail detail = new ReportDetail();
		detail.reportUid = report.getReportUid();

		if (report.getType() == 1)
			detail.type = "광고/스팸";
		else if (report.getType() == 2)
			detail.type = "휴먼 판매자";
		else if (report.getType() == 3)
			detail.type = "직거래 유도";
		else if (report.getType() == 4)
			detail.type = "기타";
		else
			detail.type = "취소";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Timestamp ts = Timestamp.valueOf(report.getRegTime().toString());
		detail.reg_time = sdf.format(ts);

		detail.content = report.getContent();
		detail.status = report.getStatus();

		detail.memo = report.getMemo();

		report_list.add(detail);

		return new Gson().toJson(report_list);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody Integer SaveHandler(Integer report_uid,
			Integer status, String memo, HttpSession session) {
		Report report = Report.getInstance(report_uid);
		if (report == null)
			return Constant.RESULT_FAILED;

		report.setStatus(status);
		report.setMemo(memo);

		return report.Update();
	}
}

class ReportDetail {
	public Integer reportUid;
	public String type;
	public String reg_time;
	public String content;
	public Integer status;
	public String memo;
}
