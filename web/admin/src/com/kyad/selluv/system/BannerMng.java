package com.kyad.selluv.system;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Banner;
import com.kyad.selluv.dbconnect.Event;
import com.kyad.selluv.dbconnect.Notice;
import com.kyad.selluv.dbconnect.Theme;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/system/banner_mng")
public class BannerMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Banner.getCount());

		return "system/banner_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer type,
			Integer active_status) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "type", "title", "reg_time", "cu_time",
				"banner_uid" };

		String cu_time = new Timestamp(System.currentTimeMillis()).toString();
		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (title like '%" + keyword + "%' or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (type != 0)
			custom_where += " and type =" + type;

		if (active_status == 2) {
			custom_where += " and (start_time = '1970-01-01 09:00:00' or (start_time < '" + cu_time
					+ "' and end_time > '" + cu_time + "'))";
		}
		else if (active_status == 1)
			custom_where += " and (end_time != '2099-12-31 09:00:00' and (end_time < '" + cu_time + "'))";

		custom_where += " order by reg_time desc";

		String table = "(select *, IF (start_time='1970-01-01 09:00:00', '', CONCAT(substr(start_time, 1, 10),'<br>',substr(end_time, 1, 10))) as cu_time FROM banner) as G";
		try {
			json = SSP.simple(request, response, table, "banner_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/list_ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String ListAjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer type,
			String search_content) {

		String uid;
		String table;
		String title = "title";
		if (type == 1) {
			uid = "notice_uid";
			table = "notice";
		} else if (type == 2) {
			uid = "theme_uid";
			table = "theme";
			title = "title_ko";
		} else {
			uid = "event_uid";
			table = "event";
		}

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { title, "reg_time", uid };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {

						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm");
						Timestamp ts = Timestamp.valueOf(d);
						return (sdf.format(ts));
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (search_content != null && !search_content.isEmpty()) {
			custom_where += " and (" + title + " like '%" + search_content
					+ "%' or 1=0)";
		}

		custom_where += " order by reg_time desc";

		try {
			json = SSP.simple(request, response, table, uid, columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{banner_uid}/detail", method = RequestMethod.GET)
	public String DetailHandler(@PathVariable Integer banner_uid,
			ModelMap model, HttpSession session, HttpServletRequest request) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Banner banner = Banner.getInstance(banner_uid);
		if (banner == null && banner_uid != 0) {
			model.addAttribute("msg", "error");
			return "system/banner_mng/detail";
		}
		
		model.addAttribute("detail_uid", banner_uid);

		if (banner != null) {
			model.addAttribute("type", banner.getType());

			Integer type = banner.getType();
			if (type == 1) {
				Notice notice = Notice.getInstance(banner.getTargetUid());

				if (notice != null) {
					model.addAttribute("select_title", notice.getTitle());
					model.addAttribute("select_uid", notice.getNoticeUid());
				}
			} else if (type == 2) {
				Theme theme = Theme.getInstance(banner.getTargetUid());

				if (theme != null) {
					model.addAttribute("select_title", theme.getTitleKo());
					model.addAttribute("select_uid", theme.getThemeUid());
				}
			} else if (type == 3) {
				Event event = Event.getInstance(banner.getTargetUid());

				if (event != null) {
					model.addAttribute("select_title", banner.getTitle());
					model.addAttribute("select_uid", event.getEventUid());
				}
			}

			model.addAttribute("title", banner.getTitle());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (banner.getStartTime() == null || banner.getStartTime().equals(Timestamp.valueOf("1970-01-01 09:00:00")))
				model.addAttribute("start_time", "");
			else
				model.addAttribute("start_time", sdf.format(banner.getStartTime()));
			if (banner.getEndTime() == null || banner.getEndTime().equals(Timestamp.valueOf("2099-12-31 09:00:00")))
				model.addAttribute("end_time", "");
			else
				model.addAttribute("end_time", sdf.format(banner.getEndTime()));
			model.addAttribute("real_profile_img", StarmanHelper.getTargetImageUrl(
					Constant.DIR_NAME_BANNER, banner.getBannerUid(),
					banner.getProfileImg(), StarmanHelper.NO_IMAGE_PATH));
			model.addAttribute("profile_img", banner.getProfileImg());
		}
		return "system/banner_mng/detail";
	}

	@RequestMapping(value = "/select", produces = "application/json; charset=UTF-8")
	public @ResponseBody String SelectHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer type,
			Integer uid) {

		String title = "";
		if (type == 1) {
			Notice notice = Notice.getInstance(uid);

			if (notice != null)
				title = notice.getTitle();
		} else if (type == 2) {
			Theme theme = Theme.getInstance(uid);

			if (theme != null)
				title = theme.getTitleKo();
		} else if (type == 3) {
			Event event = Event.getInstance(uid);

			if (event != null)
				title = event.getTitle();
		}

		BannerSelect bs = new BannerSelect();
		bs.uid = uid;
		bs.title = title;

		List<BannerSelect> list = new ArrayList<BannerSelect>();

		list.add(bs);
		return (new Gson()).toJson(list);
	}

	@RequestMapping(value = "/delete_banner", produces = "application/json; charset=UTF-8")
	public @ResponseBody int DeleteBannerHandler(Integer banner_uid,
			HttpSession session) {
		Banner banner = Banner.getInstance(banner_uid);
		if (banner == null)
			return Constant.RESULT_FAILED;

		banner.setStatus(Constant.STATUS_REMOVED);

		return banner.Update();
	}

	@RequestMapping(value = "/save", produces = "application/json; charset=UTF-8")
	public @ResponseBody Integer SaveHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session,
			Integer banner_uid, Integer type, Integer select_uid, String title,
			String period_from, String period_to, String profile_img) {
		if (banner_uid == 0) {
			Banner banner = new Banner();

			banner.setRegTime(new Timestamp(System.currentTimeMillis()));
			banner.setType(type);
			banner.setTitle(title);
			banner.setTargetUid(select_uid);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (period_from == null || period_from.compareTo("") == 0) {
				banner.setStartTime(new Timestamp((long)0));
			} else {
				Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
				banner.setStartTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			if (period_to == null || period_to.compareTo("") == 0) {
				Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
				banner.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			} else {
				Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
				banner.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			}

			banner.setProfileImg(profile_img);

			banner.setStatus(Constant.STATUS_USE);

			if (banner.Save() == Constant.RESULT_SUCCESS) {
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BANNER,
						banner.getBannerUid(), profile_img);
			}
			return 1;
		} else {
			Banner banner = Banner.getInstance(banner_uid);

			if (banner == null)
				return Constant.RESULT_FAILED;

			banner.setType(type);
			banner.setTitle(title);
			banner.setTargetUid(select_uid);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (period_from == null || period_from.compareTo("") == 0) {
				banner.setStartTime(new Timestamp((long)0));
			} else {
				Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
				banner.setStartTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			if (period_to == null || period_to.compareTo("") == 0) {
				Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
				banner.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			} else {
				Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
				banner.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			String oldProfileImg = banner.getProfileImg();
			banner.setProfileImg(profile_img);
			banner.setStatus(Constant.STATUS_USE);

			if (banner.Update() == Constant.RESULT_SUCCESS) {
				if (!oldProfileImg.equals(profile_img)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_BANNER,
							banner.getBannerUid(), profile_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_BANNER,
							banner.getBannerUid(), oldProfileImg);				
				}
			}
			return 1;
		}
	}
}

class BannerSelect {
	public Integer uid;
	public String title;
}
