package com.kyad.selluv.system;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Reply;

@Controller
@RequestMapping(value = "/system/reply_mng")
public class ReplyMng {
	@RequestMapping(value = {"/index", ""})	
	public String IndexHandler(ModelMap model,HttpServletRequest request, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";

		model.addAttribute("total_count", Reply.getCount());
		return "system/reply_mng/index";
	}
	
	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer valet) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_id", "target_pdt_uid", "target_usr_id",	"content", "reg_time", "reply_uid", "usr_uid", "target_usr_uid"};

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0
					|| db_fields[i].compareTo("edt_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " usr_id like '%" + keyword + "%'";
			custom_where += " or target_pdt_uid like '%" + keyword + "%'";
			custom_where += " or target_usr_id like '%" + keyword + "%'";
			custom_where += " or content like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}
		
		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from + "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to + "' or reg_time like '%" + period_to + "%')";
		
		if (valet == 1)
			custom_where += " and valet_uid != 0 ";
		else if (valet == 2)
			custom_where += " and valet_uid = 0 ";

		custom_where += " order by reg_time desc";
		
		String table = "(SELECT	E.*, F.usr_id as target_usr_id FROM (SELECT C.*, D.usr_uid AS target_usr_uid, D.pdt_uid AS target_pdt_uid, D.valet_uid FROM ";
		table += "(SELECT A.*, B.usr_id FROM reply AS A LEFT JOIN usr AS B ON A.usr_uid = B.usr_uid) AS C LEFT JOIN pdt AS D ON C.pdt_uid = D.pdt_uid) AS E LEFT JOIN usr as F on E.target_usr_uid=F.usr_uid) as G";
		try {
			json = SSP.simple(request, response, table, "reply_uid", columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
	
	@RequestMapping(value = "/delete_reply", produces = "application/json; charset=UTF-8")
	public @ResponseBody Integer SelectHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Integer reply_uid) {
		
		Reply reply = Reply.getInstance(reply_uid);
		if (reply == null)
			return Constant.RESULT_FAILED;
		
		reply.setStatus(Constant.STATUS_REMOVED);
		
		return reply.Update();
	}
}
