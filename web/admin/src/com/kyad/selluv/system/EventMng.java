package com.kyad.selluv.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Event;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/system/event_mng")
public class EventMng {
	@RequestMapping(value = { "/index", "" })
	public String IndexHandler(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Event.getCount());

		return "system/event_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer active_status) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "mypage_yn", "title", "reg_time", "cu_time",
				"event_uid" };

		String cu_time = new Timestamp(System.currentTimeMillis()).toString();
		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (title like '%" + keyword
					+ "%' or content like '%" + keyword + "%' or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (active_status == 2) {
			custom_where += " and (start_time = '1970-01-01 09:00:00' or (start_time < '" + cu_time
					+ "' and end_time > '" + cu_time + "'))";
		}
		else if (active_status == 1)
			custom_where += " and (end_time != '2099-12-31 09:00:00' and (end_time < '" + cu_time + "'))";

		custom_where += " order by reg_time desc";

		String table = "(select *, IF (start_time='1970-01-01 09:00:00', '', CONCAT(substr(start_time, 1, 10),'<br>',substr(end_time, 1, 10))) as cu_time FROM event) as G";
		try {
			json = SSP.simple(request, response, table, "event_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{event_uid}/detail", method = RequestMethod.GET)
	public String DetailHandler(@PathVariable Integer event_uid,
			ModelMap model, HttpSession session, HttpServletRequest request) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Event event = Event.getInstance(event_uid);
		model.addAttribute("detail_uid", event_uid);

		if (event == null && event_uid != 0) {
			model.addAttribute("msg", "error");
			return "system/event_mng/detail";
		} else if (event != null) {
			model.addAttribute("title", event.getTitle());
			model.addAttribute("event", event);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (event.getStartTime() == null || event.getStartTime().equals(Timestamp.valueOf("1970-01-01 09:00:00")))
				model.addAttribute("start_time", "");
			else
				model.addAttribute("start_time", sdf.format(event.getStartTime()));
			if (event.getEndTime() == null || event.getEndTime().equals(Timestamp.valueOf("2099-12-31 09:00:00")))
				model.addAttribute("end_time", "");
			else
				model.addAttribute("end_time", sdf.format(event.getEndTime()));
			model.addAttribute("real_profile_img", StarmanHelper.getTargetImageUrl(
					Constant.DIR_NAME_EVENT, event.getEventUid(),
					event.getProfileImg(), StarmanHelper.NO_IMAGE_PATH));
			model.addAttribute("profile_img", event.getProfileImg());
			
			model.addAttribute("real_event_img", StarmanHelper.getTargetImageUrl(
					Constant.DIR_NAME_EVENT, event.getEventUid(),
					event.getDetailImg(), StarmanHelper.NO_IMAGE_PATH));
			model.addAttribute("event_img", event.getDetailImg());

			model.addAttribute("content", event.getContent());
		}

		return "system/event_mng/detail";
	}

	@RequestMapping(value = "/delete_event", produces = "application/json; charset=UTF-8")
	public @ResponseBody int DeleteEventHandler(Integer event_uid,
			HttpSession session) {
		Event event = Event.getInstance(event_uid);
		if (event == null)
			return Constant.RESULT_FAILED;

		event.setStatus(Constant.STATUS_REMOVED);

		return event.Update();
	}

	@RequestMapping(value = "/save", produces = "application/json; charset=UTF-8")
	public @ResponseBody Integer SaveHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session,
			Integer event_uid, String title, String period_from,
			String period_to, String profile_img, String detail_img,
			Integer mypageYN) {
		if (event_uid == 0) {
			Event event = new Event();

			event.setRegTime(new Timestamp(System.currentTimeMillis()));
			event.setTitle(title);
			event.setMypageYn(mypageYN);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (period_from == null || period_from.compareTo("") == 0) {
				event.setStartTime(new Timestamp((long)0));
			} else {
				Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
				event.setStartTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			if (period_to == null || period_to.compareTo("") == 0) {
				Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
				event.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			} else {
				Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
				event.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			}

			event.setContent("");
			event.setProfileImg(profile_img);
			event.setDetailImg(detail_img);
			event.setStatus(Constant.STATUS_USE);

			if (event.Save() == Constant.RESULT_SUCCESS) {
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_EVENT,
						event.getEventUid(), profile_img);
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_EVENT,
						event.getEventUid(), detail_img);
			}

			Calendar cal = GregorianCalendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			long todayTwelve = cal.getTime().getTime();

			if (event.getEventUid() > 0 && event.getStartTime().getTime() >= todayTwelve &&
					event.getStartTime().getTime() < (todayTwelve + 24 * 3600 * 1000)) {
				HttpURLConnection conn = null;
				OutputStream outputStream = null;

				try {
					URL url = new URL(Constant.SELLUV_API_ADDRESS + "/version/alarm");

					conn = (HttpURLConnection) url.openConnection();
					conn.setDoInput(true);
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("adminToken", Constant.SELLUV_ADMIN_TOKEN);
					conn.setConnectTimeout(10000);

					String params = "";
					params += "pushType=PUSH22_EVENT";
					params += "&targetUid=" + event.getEventUid();

					outputStream = conn.getOutputStream();
					outputStream.write(params.getBytes("UTF-8"));
					outputStream.flush();

					int responseCode = conn.getResponseCode();

					StringBuilder responseReq = new StringBuilder();
					if (responseCode == HttpURLConnection.HTTP_OK) {
						String line;
						BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						while ((line = br.readLine()) != null) {
							responseReq.append(line).append("\n");
						}
						br.close();
					} else {
						responseReq.append("");
					}

					String result = responseReq.toString().trim();

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (outputStream != null) {
						try {
							outputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if (conn != null)
						conn.disconnect();
				}
			}
		} else {
			Event event = Event.getInstance(event_uid);

			if (event == null)
				return Constant.RESULT_FAILED;

			event.setTitle(title);
			event.setMypageYn(mypageYN);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (period_from == null || period_from.compareTo("") == 0) {
				event.setStartTime(new Timestamp((long)0));
			} else {
				Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
				event.setStartTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			if (period_to == null || period_to.compareTo("") == 0) {
				Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
				event.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			} else {
				Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
				event.setEndTime(Timestamp.valueOf(sdf.format(ts)));
			}
			
			event.setContent("");
			String oldProfileImg = event.getProfileImg();
			String oldDetailImg = event.getDetailImg();
			event.setProfileImg(profile_img);
			event.setDetailImg(detail_img);
			event.setStatus(Constant.STATUS_USE);

			if (event.Update() == Constant.RESULT_SUCCESS) {
				if (!oldProfileImg.equals(profile_img)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_EVENT,
							event.getEventUid(), profile_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_EVENT,
							event.getEventUid(), oldProfileImg);
				}
				
				if (!oldDetailImg.equals(detail_img)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_EVENT,
							event.getEventUid(), detail_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_EVENT,
							event.getEventUid(), oldDetailImg);
				}
			}
		}
		return Constant.RESULT_SUCCESS;
	}
}
