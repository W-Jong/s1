package com.kyad.selluv.pdt;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Brand;
import com.kyad.selluv.dbconnect.Category;
import com.kyad.selluv.dbconnect.Pdt;
import com.kyad.selluv.dbconnect.Usr;
import com.kyad.selluv.dbconnect.Valet;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/pdt/valet_mng")
public class ValetMng {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session,
			Integer status) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Valet.getCount());
		if (status != null && status != 0)
			model.addAttribute("status", status);
		return "pdt/valet_mng/valet_list";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			Integer status, String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "valet_uid", "pdt_uid", "req_price", "price",
				"usr_id", "req_name", "req_phone", "status", "reg_time",
				"edt_time" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " valet_uid like '%" + keyword + "%'";
			custom_where += " or pdt_uid like '%" + keyword + "%'";
			custom_where += " or usr_id like '%" + keyword + "%'";
			custom_where += " or req_name like '%" + keyword + "%'";
			custom_where += " or req_phone like '%" + keyword + "%'";
			custom_where += " or status like '%" + keyword + "%'";
			custom_where += " or memo like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (status != 0) {
			custom_where += " and status = " + status;
		}

		custom_where += " order by valet_uid desc";
		try {
			json = SSP
					.simple(request,
							response,
							"(SELECT valet.*, pdt.price, "
									+ "(SELECT usr_id FROM usr WHERE usr.usr_uid = valet.usr_uid) as usr_id "
									+ "FROM valet LEFT JOIN pdt ON pdt.pdt_uid = valet.pdt_uid) AS A",
							"valet_uid", columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/reg")
	public String RegHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		return "pdt/valet_mng/valet_reg";
	}

	@RequestMapping(value = "/{valet_uid}/detail")
	public String DetailHandler(@PathVariable Integer valet_uid,
			ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Valet valet = Valet.getInstance(valet_uid);
		if (valet == null) {
			model.addAttribute("msg", "error");
			return "pdt/valet_mng/valet_reg";
		}
		model.addAttribute("valet", valet);
		Usr usr = Usr.getInstance(valet.getUsrUid());
		if (usr != null) {
			model.addAttribute("usr_id", usr.getUsrId());
			model.addAttribute("usr_uid", usr.getUsrUid());
		}
		Pdt pdt = Pdt.getInstance(valet.getPdtUid());
		if (valet != null) {
			model.addAttribute("sign_img", StarmanHelper.getTargetImageUrl(
					Constant.DIR_NAME_VALET, valet.getValetUid(),
					valet.getSignImg(), StarmanHelper.NO_IMAGE_PATH));
		}
		if (pdt != null) {
			Brand brand = Brand.getInstance(pdt.getBrandUid());
			model.addAttribute("brandName", brand.getNameEn());
			Integer pdt_group = pdt.getPdtGroup();
			if (pdt_group == 1)
				model.addAttribute("pdt_group", "남성");
			else if (pdt_group == 2)
				model.addAttribute("pdt_group", "여성");
			else if (pdt_group == 4)
				model.addAttribute("pdt_group", "키즈");
			String category1 = "", category2 = "", category3 = "";
			Category category = Category.getInstance(pdt.getCategoryUid());
			if (category.getDepth() == 3) {
				category3 = Category.getInstance(pdt.getCategoryUid())
						.getCategoryName();
				Category new_ct = Category.getInstance(category
						.getParentCategoryUid());
				category2 = Category.getInstance(new_ct.getCategoryUid())
						.getCategoryName();
				new_ct = Category.getInstance(new_ct.getParentCategoryUid());
				category1 = Category.getInstance(new_ct.getCategoryUid())
						.getCategoryName();
			} else if (category.getDepth() == 2) {
				category2 = Category.getInstance(category.getCategoryUid())
						.getCategoryName();
				Category new_ct = Category.getInstance(category
						.getParentCategoryUid());
				category1 = Category.getInstance(new_ct.getCategoryUid())
						.getCategoryName();
			} else if (category.getDepth() == 1) {
				category1 = Category.getInstance(category.getCategoryUid())
						.getCategoryName();
			}
			model.addAttribute("category1", category1);
			model.addAttribute("category2", category2);
			model.addAttribute("category3", category3);
		}
		return "pdt/valet_mng/valet_reg";
	}

	@RequestMapping(value = "/{valet_uid}/save", method = RequestMethod.POST)
	public String SaveHandler(@PathVariable Integer valet_uid, ModelMap model,
			HttpServletRequest request, HttpSession session, Integer status,
			Integer pdt_uid, Integer req_price, String req_name,
			String req_phone, String req_address, String req_address_detail,
			Integer send_type, Integer receive_yn, String memo, Long earn_price) {
		Valet valet = Valet.getInstance(valet_uid);
		boolean is_new = false;
		if (valet == null) {
			valet = new Valet();
			is_new = true;
			valet.setRegTime(new Timestamp(System.currentTimeMillis()));
		}

		valet.setPdtUid(pdt_uid == null ? 0 : pdt_uid);
		if (pdt_uid != null) {
			Pdt pdt = Pdt.getInstance(pdt_uid);
			valet.setUsrUid(pdt.getUsrUid());
		}
		valet.setEarnPrice(earn_price == null ? 0 : earn_price);
		valet.setReqPrice(req_price == null ? 0 : (long) req_price);
		valet.setReqName(req_name == null ? "" : req_name);
		valet.setReqPhone(req_phone == null ? "" : req_phone);
		valet.setReqAddress(req_address == null ? "" : req_address);
		valet.setReqAddressDetail(req_address_detail == null ? ""
				: req_address_detail);
		valet.setSendType(send_type);
		valet.setReceiveYn(receive_yn);
		valet.setMemo(memo == null ? "" : memo);
		valet.setEdtTime(new Timestamp(System.currentTimeMillis()));
		valet.setStatus(status);

		if (is_new) {
			valet.setSignImg("");
			valet.setValetUid(0);
			valet.Save();
		} else {
			valet.Update();
		}
		return "redirect:/pdt/valet_mng";
	}

	@RequestMapping(value = "/{valetUid}/remove")
	public String RemoveHandler(@PathVariable Integer valetUid,
			HttpSession session) {
		Valet valet = Valet.getInstance(valetUid == null ? 0 : valetUid);

		if (valet != null) {
			valet.setStatus(Constant.STATUS_REMOVED);
			valet.Update();
		}

		return "redirect:/pdt/valet_mng";
	}
}
