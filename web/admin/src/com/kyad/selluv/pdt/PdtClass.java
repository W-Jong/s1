package com.kyad.selluv.pdt;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.Category;
import com.kyad.selluv.dbconnect.CategoryDAO;
import com.kyad.selluv.dbconnect.Pdt;

@Controller
@RequestMapping(value = "/pdt/pdt_class")
public class PdtClass {
	
	public class TreeNode {
		public String id;
		public String text;
		public List<TreeNode> children;
		
		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public String getText() {
			return text;
		}
		
		public void setText(String text) {
			this.text = text;
		}
		
		public List<TreeNode> getChildren() {
			return children;
		}
		
		public void setChildren(List<TreeNode> list) {
			this.children = list;
		}
	}

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		TreeNode node1 = getList(Constant.PDT_GROUP_MALE);
		TreeNode node2 = getList(Constant.PDT_GROUP_FEMALE);
		TreeNode node4 = getList(Constant.PDT_GROUP_KIDS);
		model.addAttribute("data1", new Gson().toJson(node1.getChildren()));
		model.addAttribute("data2", new Gson().toJson(node2.getChildren()));
		model.addAttribute("data4", new Gson().toJson(node4.getChildren()));
		return "pdt/pdt_class/index";
	}

	public TreeNode getList(int parent) {
		TreeNode node = new TreeNode();
		Category category = Category.getInstance(parent);
		node.setText(category.getCategoryName());
		node.setId(String.valueOf(category.getCategoryUid()));
		CategoryDAO cdao = new CategoryDAO();
		Category exam = new Category();
		exam.setStatus(Constant.STATUS_USE);
		exam.setParentCategoryUid(parent);
		List<Category> ct_list = cdao.findByExample(exam);
		List<TreeNode> list = new ArrayList<TreeNode>();
		for (int i=0; i<ct_list.size(); i++) {
			list.add(getList(ct_list.get(i).getCategoryUid()));
		}
		node.setChildren(list);
		return node;
	}
	
	@RequestMapping(value = "/save")
	public @ResponseBody String SaveHandler(ModelMap model, HttpServletRequest request, HttpSession session,
			String data1, String data2, String data4) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Type type = new TypeToken<List<TreeNode>>() {}.getType();
		List<TreeNode> list1 = (new Gson()).fromJson(data1, type);
		List<TreeNode> list2 = (new Gson()).fromJson(data2, type);
		List<TreeNode> list4 = (new Gson()).fromJson(data4, type);
		CategoryDAO cdao = new CategoryDAO();
		List<Category> list = cdao.findByStatus(Constant.STATUS_USE);
		for (int i=0; i<list.size(); i++) {
			if (list.get(i).getDepth() > 0) {
				list.get(i).setStatus(Constant.STATUS_REMOVED);
				list.get(i).Update();
			}
		}
		setList(list1, 1, 1);
		setList(list2, 2, 1);
		setList(list4, 4, 1);
		return "success";
	}
	
	public void setList(List<TreeNode> list, Integer parent, Integer depth) {
		for (int i=0; i<list.size(); i++) {
			TreeNode node = list.get(i);
			Category category = null;
			boolean is_new = false;
			if (!node.getId().startsWith("j")) {
				category = Category.getInstance(Integer.valueOf(node.getId()));
			}
			if (category == null) {
				category = new Category();
				is_new = true;
				category.setCategoryUid(0);
				category.setRegTime(new Timestamp(System.currentTimeMillis()));
			}
			category.setCategoryName(node.getText());
			category.setParentCategoryUid(parent);
			category.setDepth(depth);
			category.setCategoryOrder(i+1);
			category.setPdtCount(0);
			category.setTotalCount((long)0);
			category.setTotalPrice((long)0);
			category.setStatus(Constant.STATUS_USE);
			if (is_new)
				category.Save();
			else
				category.Update();
			setList(node.getChildren(), category.getCategoryUid(), depth + 1);
		}
	}
	
	@RequestMapping(value = "/{categoryUid}/confirm_remove")
	public @ResponseBody String RemoveHandler(@PathVariable Integer categoryUid,
			HttpSession session) {
		List<Pdt> list = Pdt.getListByCategoryUid(categoryUid);

		if (list != null && list.size() > 0)
			return "no";
		List<Category> ct_list = Category.getCategoryListByParentCategory(categoryUid);
		for (int i=0; i<ct_list.size(); i++) {
			list = Pdt.getListByCategoryUid(ct_list.get(i).getCategoryUid());
			if (list != null && list.size() > 0)
				return "no";
		}
		return "yes";
	}
}
