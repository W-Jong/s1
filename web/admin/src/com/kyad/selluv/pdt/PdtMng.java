package com.kyad.selluv.pdt;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.BaseHibernateDAO;
import com.kyad.selluv.dbconnect.Brand;
import com.kyad.selluv.dbconnect.Category;
import com.kyad.selluv.dbconnect.Pdt;
import com.kyad.selluv.dbconnect.PdtStyle;
import com.kyad.selluv.dbconnect.SizeRef;
import com.kyad.selluv.dbconnect.Usr;
import com.kyad.selluv.dbconnect.Valet;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/pdt/pdt_mng")
public class PdtMng {

	@RequestMapping(value = { "/", "" })
	public String PdtHandler(ModelMap model, HttpSession session, Integer photoshop_yn) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Pdt.getCount());
		if (photoshop_yn != null && photoshop_yn != 0)
			model.addAttribute("photoshop_yn", photoshop_yn);
		return "pdt/pdt_mng/pdt_list";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String period_from, String period_to, Integer pdt_group,
			Integer category1, Integer category2, Integer category3,
			Integer valet, Integer status, Integer photoshop, String brand_list) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "pdt_uid", "profile_img", "pdt_model", "price",
				"usr_id", "status", "reg_time", "promotion_code",
				"edt_time", "name_ko", "pdt_group", "color_name",
				"category_name", "memo", "usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0
					|| db_fields[i].compareTo("edt_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}

			else if (db_fields[i].compareTo("memo") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return StarmanHelper.UPLOAD_URL_PATH;
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " pdt_uid like '%" + keyword + "%'";
			custom_where += " or pdt_model like '%" + keyword + "%'";
			custom_where += " or usr_id like '%" + keyword + "%'";
			custom_where += " or price like '%" + keyword + "%'";
			custom_where += " or name_en like '%" + keyword + "%'";
			custom_where += " or name_ko like '%" + keyword + "%'";
			custom_where += " or pdt_group like '%" + keyword + "%'";
			custom_where += " or memo like '%" + keyword + "%'";
			custom_where += " or content like '%" + keyword + "%'";
			custom_where += " or category_name like '%" + keyword + "%'";
			custom_where += " or valet_uid like '%" + keyword + "%'";
			custom_where += " or tag like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (period_from != null && !period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (period_to != null && !period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (pdt_group != 0) {
			custom_where += " and pdt_group = " + pdt_group;
			if (category1 != 0) {
				custom_where += " and category1 = " + category1;
				if (category2 != 0) {
					custom_where += " and category2 = " + category2;
					if (category3 != 0) {
						custom_where += " and category3 = " + category3;
					}
				}
			}
		}
		
		if (brand_list != null && !brand_list.isEmpty()) {
			brand_list = brand_list.substring(0, brand_list.length() - 1);
			custom_where += " and brand_uid in (" + brand_list + ")";
		}
		
		if (valet != null && valet != 0) {
			if (valet == 1)
				custom_where += " and valet_uid != 0";
			else
				custom_where += " and valet_uid = 0";
		}

		if (status != null && status != 0) {
			custom_where += " and status = " + status;
		}

		if (photoshop != null && photoshop != 0) {
			custom_where += " and photoshop_yn = " + photoshop;
		}

		custom_where += " order by pdt_uid desc";
		try {
			json = SSP
					.simple(request,
							response,
							"(select A.*, C.category_name, "
									+ "(select usr_id from usr where usr_uid = A.usr_uid) as usr_id, "
									+ "(select name_ko from brand where brand_uid = A.brand_uid) as name_ko, "
									+ "(select name_en from brand where brand_uid = A.brand_uid) as name_en, "
									+ "(CASE depth WHEN 1 THEN C.category_uid WHEN 2 THEN C.parent_category_uid "
									+ "WHEN 3 THEN (SELECT parent_category_uid FROM category WHERE category_uid = C.parent_category_uid) "
									+ "END) AS category1, "
									+ "(CASE depth WHEN 1 THEN 0 "
									+ "WHEN 2 THEN C.category_uid "
									+ "WHEN 3 THEN C.parent_category_uid "
									+ "END) AS category2, "
									+ "(CASE depth WHEN 1 THEN 0 "
									+ "WHEN 2 THEN 0 "
									+ "WHEN 3 THEN C.category_uid "
									+ "END) AS category3 "
									+ "from pdt as A LEFT JOIN category C ON A.category_uid = C.category_uid) as B", "pdt_uid",
							columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_brand_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxBrandHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			String first_en, String first_ko) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "first_en", "name_en", "first_ko", "name_ko",
				"brand_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			columns.add(column);
		}

		String json = "";

		String custom_where = "status = " + Constant.BRAND_REGULAR;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " name_en like '%" + keyword + "%'";
			custom_where += " or name_ko like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (first_en != null && !first_en.isEmpty()) {
			custom_where += " and first_en = '" + first_en + "'";
		}

		if (first_ko != null && !first_ko.isEmpty()) {
			custom_where += " and first_ko = '" + first_ko + "'";
		}

		custom_where += " order by first_en asc";
		try {
			json = SSP.simple(request, response, "brand", "brand_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_usr_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxUsrHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "usr_id", "usr_nck_nm", "usr_nm", "usr_phone",
				"usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " usr_id like '%" + keyword + "%'";
			custom_where += " or usr_nck_nm like '%" + keyword + "%'";
			custom_where += " or usr_nm like '%" + keyword + "%'";
			custom_where += " or usr_phone like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		custom_where += " order by usr_id asc";

		try {
			json = SSP.simple(request, response, "usr", "usr_uid", columns,
					custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/ajax_size_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxSizeHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			Integer pdt_group, Integer category1, String category2,
			String category3) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "pdt_group_str", "category1_str", "category2",
				"category3", "basis", "type_name", "size_name", "size_ref_uid",
				"status" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " pdt_group_str like '%" + keyword + "%'";
			custom_where += " or category1_str like '%" + keyword + "%'";
			custom_where += " or category2 like '%" + keyword + "%'";
			custom_where += " or category3 like '%" + keyword + "%'";
			custom_where += " or basis like '%" + keyword + "%'";
			custom_where += " or type_name like '%" + keyword + "%'";
			custom_where += " or size_name like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}
		
		if (pdt_group != null && pdt_group != 0)
			custom_where += " AND pdt_group="+pdt_group;
		if (category1 != null && category1 != 0)
			custom_where += " AND category1="+category1;
		if (category2 != null && !category2.isEmpty())
			custom_where += " AND category2='"+category2+"'";
		if (category3 != null && !category3.isEmpty())
			custom_where += " AND category3='"+category3+"'";

		custom_where += " order by size_ref_uid asc";

		try {
			json = SSP.simple(request, response, "(select *, "
					+ "(case pdt_group when 1 then '남성' "
					+ "when 2 then '여성' when 4 then '키즈' end) as pdt_group_str, "
					+ "(select category_name from category where category_uid = category1) as category1_str "
					+ "from size_ref) as A", "size_ref_uid",
					columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/reg")
	public String RegHandler(ModelMap model, HttpSession session, Integer valet_uid) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		if (valet_uid != null && valet_uid != 0)
			model.addAttribute("valet_uid", valet_uid);
		List<Category> category1list = Category
				.getCategoryListByParentCategory(1);
		List<Category> category2list = Category
				.getCategoryListByParentCategory(category1list.get(0)
						.getCategoryUid());
		if (category2list != null && category2list.size() > 0) {
			List<Category> category3list = Category
					.getCategoryListByParentCategory(category2list.get(0)
							.getCategoryUid());
			model.addAttribute("category2_list", category2list);
			if (category3list != null && category3list.size() > 0)
				model.addAttribute("category3_list", category3list);
		}
		model.addAttribute("category1_list", category1list);
		return "pdt/pdt_mng/pdt_reg";
	}

	@RequestMapping(value = "/{pdt_uid}/detail")
	public String DetailHandler(@PathVariable Integer pdt_uid, ModelMap model,
			HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		Pdt pdt = null;
		try {
			pdt = Pdt.getInstance(pdt_uid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (pdt == null) {
			model.addAttribute("msg", "error");
			return "pdt/pdt_mng/pdt_reg";
		}
		model.addAttribute("profile_img", StarmanHelper.getTargetImageUrl(
				Constant.DIR_NAME_PDT, pdt.getPdtUid(),
				pdt.getProfileImg(), StarmanHelper.NO_IMAGE_PATH));
		model.addAttribute("sign_img", StarmanHelper.getTargetImageUrl(
				Constant.DIR_NAME_PDT, pdt.getPdtUid(),
				pdt.getSignImg(), StarmanHelper.NO_IMAGE_PATH));
		Usr usr = Usr.getInstance(pdt.getUsrUid());
		model.addAttribute("usrName", usr.getUsrId());
		Brand brand = Brand.getInstance(pdt.getBrandUid());
		model.addAttribute("brandName", brand.getNameEn());
		model.addAttribute("pdt", pdt);
		
		// pdt_component
		List<Integer> component_list = new ArrayList<Integer>();
		String component = pdt.getComponent();
		if (component != null) {
			for (int i = 0; i < component.length(); i++) {
				if (component.charAt(i) == '1')
					component_list.add(i, 1);
				else
					component_list.add(i, 0);
			}
			model.addAttribute("component", component_list);
		}

		// category
		Integer category1 = 0, category2 = 0, category3 = 0;
		Category category = Category.getInstance(pdt.getCategoryUid());
		if (category.getDepth() == 3) {
			category3 = pdt.getCategoryUid();
			Category new_ct = Category.getInstance(category
					.getParentCategoryUid());
			category2 = new_ct.getCategoryUid();
			new_ct = Category.getInstance(new_ct.getParentCategoryUid());
			category1 = new_ct.getCategoryUid();
		} else if (category.getDepth() == 2) {
			category2 = category.getCategoryUid();
			Category new_ct = Category.getInstance(category
					.getParentCategoryUid());
			category1 = new_ct.getCategoryUid();
		} else if (category.getDepth() == 1) {
			category1 = category.getCategoryUid();
		}
		List<Category> category1list = Category
				.getCategoryListByParentCategory(pdt.getPdtGroup());
		List<Category> category2list = Category
				.getCategoryListByParentCategory(category1);
		if (category2list != null && category2list.size() > 0) {
			List<Category> category3list = Category
					.getCategoryListByParentCategory(category2);
			model.addAttribute("category2_list", category2list);
			model.addAttribute("category2", category2);
			if (category3list != null && category3list.size() > 0) {
				model.addAttribute("category3_list", category3list);
				model.addAttribute("category3", category3);
			}
		}
		model.addAttribute("category1_list", category1list);
		model.addAttribute("category1", category1);

		// photos
		String[] photos = pdt.getPhotos().split(",");
		List<String> real_photo_list = new ArrayList<String>();
		List<String> photo_list = new ArrayList<String>();
		if (pdt.getPhotos() != null && !"".equals(pdt.getPhotos())) {
			for (int i = 0; i < photos.length; i++) {
				real_photo_list.add(i, StarmanHelper.getTargetImageUrl(
						Constant.DIR_NAME_PDT, pdt.getPdtUid(), photos[i],
						StarmanHelper.NO_IMAGE_PATH));
				photo_list.add(i, photos[i]);
			}
		}
		model.addAttribute("photos", photo_list);
		model.addAttribute("real_photos", real_photo_list);

		// tag
		List<String> real_tag_list = new ArrayList<String>();
		String tags = pdt.getTag();
		String[] tag_list = null;
		try{
			tag_list = tags.split(" ");
		for (int i = 0; i < tag_list.length; i++) {
			real_tag_list.add(i, tag_list[i].substring(1));
		}
		}catch(Exception e){
			
		}
		model.addAttribute("tags", real_tag_list);

		// style_img
		List<PdtStyle> pdt_style_list = PdtStyle.getInstanceByPdtUid(pdt_uid);
		if (pdt_style_list != null && pdt_style_list.size() > 0) {
			List<String> real_style_photo_list = new ArrayList<String>();
			List<String> style_photo_list = new ArrayList<String>();
			for (int i=0; i<pdt_style_list.size(); i++) {
				real_style_photo_list.add(i, StarmanHelper.getTargetImageUrl(
						Constant.DIR_NAME_PDT_STYLE, pdt_style_list.get(i).getPdtStyleUid(), pdt_style_list.get(i).getStyleImg(),
						StarmanHelper.NO_IMAGE_PATH));
				style_photo_list.add(i, pdt_style_list.get(i).getStyleImg());
			}
			model.addAttribute("real_style_photos", real_style_photo_list);
			model.addAttribute("style_photos", style_photo_list);
		}
		return "pdt/pdt_mng/pdt_reg";
	}

	@RequestMapping(value = "/{pdt_uid}/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(@PathVariable Integer pdt_uid, ModelMap model,
			HttpServletRequest request, HttpSession session, Integer status,
			Integer usr_uid, Integer pdt_group, Integer category1,
			Integer category2, Integer category3, Integer brand_uid,
			String pdt_size, String profile_img, String photo_list,
			String style_photo_list, Integer photoshop_yn, Long price,
			Long send_price, Integer pdt_condition, String component,
			String etc, String color_name, String pdt_model,
			String pdt_content, String tag_list, Integer valet_uid, String memo) {
		boolean is_new = false;
		Pdt pdt = Pdt.getInstance(pdt_uid);
		if (pdt == null) {
			is_new = true;
			pdt = new Pdt();
			pdt.setPdtUid(0);
		}
		pdt.setUsrUid(usr_uid);
		pdt.setBrandUid(brand_uid);
		if (pdt_group == Constant.PDT_GROUP_MALE ||
				pdt_group == Constant.PDT_GROUP_FEMALE ||
				pdt_group == Constant.PDT_GROUP_KIDS)
			pdt.setPdtGroup(pdt_group);
		else
			pdt.setPdtGroup(Constant.PDT_GROUP_MALE);

		if (category3 != null && category3 > 0)
			pdt.setCategoryUid(category3);
		else
			pdt.setCategoryUid((category2 != null && category2 > 0) ? category2
					: category1);
		if (pdt.getCategoryUid() < 5)
			pdt.setCategoryUid(5);

		pdt.setPdtSize(pdt_size);
		String oldProfileImg = pdt.getProfileImg();
		String oldPhotos = pdt.getPhotos();
		pdt.setProfileImg(profile_img);
		if (photo_list == null || "".equals(photo_list)) {
			pdt.setPhotos("");
		} else {
			pdt.setPhotos(photo_list.substring(0, photo_list.length() - 1));
		}
		pdt.setSendPrice(send_price);
		pdt.setPdtCondition(pdt_condition);
		pdt.setComponent(component);
		pdt.setEtc(etc == null ? "" : etc);
		pdt.setColorName(color_name);
		pdt.setPdtModel(pdt_model);
		pdt.setContent(pdt_content == null ? "" : pdt_content);
		pdt.setTag(tag_list == null ? "" : tag_list);
		pdt.setValetUid(valet_uid == null ? 0 : valet_uid);
		pdt.setMemo(memo == null ? "" : memo);
		pdt.setStatus(status);

		if (is_new) {
			pdt.setPhotoshopYn(photoshop_yn);
			pdt.setPrice(price);
			pdt.setOriginPrice((long) 0);
			pdt.setFameScore(0);
			pdt.setNegoYn(1);
			pdt.setSignImg("");
			pdt.setPromotionCode("");
			pdt.setRegTime(new Timestamp(System.currentTimeMillis()));
			pdt.setEdtTime(new Timestamp(System.currentTimeMillis()));
			if (pdt.Save() == Constant.RESULT_SUCCESS) {
				StarmanHelper.moveTargetFolder(Constant.DIR_NAME_PDT,
						pdt.getPdtUid(), profile_img);
				String[] photos = pdt.getPhotos().split(",");
				for (int i = 0; i < photos.length; i++) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_PDT,
							pdt.getPdtUid(), photos[i]);
				}
				if (valet_uid != null && valet_uid != 0) {
					Valet valet = Valet.getInstance(valet_uid);
					if (valet != null) {
						valet.setPdtUid(pdt.getPdtUid());
						valet.setUsrUid(pdt.getUsrUid());
						valet.Update();
					}
				}
			}
		} else {
			pdt.setOriginPrice(pdt.getPrice());
			pdt.setPrice(price);
			if (!(pdt.getPhotoshopYn() == 2 && photoshop_yn == 1))
				pdt.setEdtTime(new Timestamp(System.currentTimeMillis()));

			pdt.setPhotoshopYn(photoshop_yn);
			if (pdt.Update() == Constant.RESULT_SUCCESS) {
				if (!profile_img.equals(oldProfileImg)) {
					StarmanHelper.moveTargetFolder(Constant.DIR_NAME_PDT,
							pdt.getPdtUid(), profile_img);
					StarmanHelper.deleteTargetImage(Constant.DIR_NAME_PDT,
							pdt.getPdtUid(), oldProfileImg);
				}

				String[] photos = pdt.getPhotos().split(",");
				for (int i = 0; i < photos.length; i++) {
					if (!oldPhotos.contains(photos[i])) {
						StarmanHelper.moveTargetFolder(Constant.DIR_NAME_PDT,
								pdt.getPdtUid(), photos[i]);
					}
				}

				String[] old_photos = oldPhotos.split(",");
				for (int i = 0; i < old_photos.length; i++) {
					if (!pdt.getPhotos().contains(old_photos[i]))
						StarmanHelper.deleteTargetImage(Constant.DIR_NAME_PDT,
								pdt.getPdtUid(), old_photos[i]);
				}
				if (valet_uid != null && valet_uid != 0) {
					Valet valet = Valet.getInstance(valet_uid);
					try{
						valet.setPdtUid(pdt.getPdtUid());
						valet.setUsrUid(pdt.getUsrUid());
						valet.Update();
					}catch(Exception e){
						
					}
				}
			}
		}
		
		List<PdtStyle> pdt_style_list = PdtStyle.getInstanceByPdtUid(pdt_uid);
		if (pdt_style_list != null) {
			for (int i=0; i<pdt_style_list.size(); i++) {
				pdt_style_list.get(i).setStatus(Constant.STATUS_REMOVED);
				pdt_style_list.get(i).Update();
			}
		}
		
		if (style_photo_list != null && !style_photo_list.isEmpty()) {
			style_photo_list = style_photo_list.substring(0, style_photo_list.length() - 1);
			String[] style_photos = style_photo_list.split(",");
			for (int i = 0; i < style_photos.length; i++) {
				boolean is_style_new = false;
				PdtStyle pdt_style = PdtStyle.getInstanceByStyleImg(style_photos[i]);
				if (pdt_style == null) {
					is_style_new = true;
					pdt_style = new PdtStyle();
					pdt_style.setPdtStyleUid(0);
					pdt_style.setRegTime(new Timestamp(System.currentTimeMillis()));
				}
				String oldStylePhoto = pdt_style.getStyleImg();
				pdt_style.setPdtUid(pdt.getPdtUid());
				pdt_style.setStatus(Constant.STATUS_USE);
				pdt_style.setStyleImg(style_photos[i]);
				pdt_style.setUsrUid(usr_uid);
				if (is_style_new) {
					if (pdt_style.Save() == Constant.RESULT_SUCCESS) {
						StarmanHelper.moveTargetFolder(Constant.DIR_NAME_PDT_STYLE,
									pdt_style.getPdtStyleUid(), style_photos[i]);
					}
				} else {
					if (pdt_style.Update() == Constant.RESULT_SUCCESS) {
						if (!oldStylePhoto.equals(style_photos[i])) {
							StarmanHelper.moveTargetFolder(Constant.DIR_NAME_PDT_STYLE,
									pdt_style.getPdtStyleUid(), style_photos[i]);
							StarmanHelper.deleteTargetImage(Constant.DIR_NAME_PDT_STYLE,
									pdt_style.getPdtStyleUid(), oldStylePhoto);
						}
					}
				}
			}
		}
		PdtStyle.DeleteAllFromStatus(0);

		return "success";
	}

	@RequestMapping(value = "/get_children", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String GetChildrenHandler(Integer parent) {
		List<Category> categorylist = Category
				.getCategoryListByParentCategory(parent);
		String json = new Gson().toJson(categorylist);
		return json;
	}
	
	@RequestMapping(value = "/get_size_category2", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String GetSizeCategory2Handler(Integer parent) {
		List<SizeRef> category2_list = SizeRef.getCategory2ListByParent(parent);
		String json = new Gson().toJson(category2_list);
		return json;
	}
	
	@RequestMapping(value = "/get_size_category3", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String GetSizeCategory3Handler(String parent) {
		List<SizeRef> category3_list = SizeRef.getCategory3ListByParent(parent);
		String json = new Gson().toJson(category3_list);
		return json;
	}

	@RequestMapping(value = "/{pdtUid}/remove")
	public String RemoveHandler(@PathVariable Integer pdtUid,
			HttpSession session) {
		BaseHibernateDAO pdao = new BaseHibernateDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		try {
			String sql = "update pdt set status = 0 where pdt_uid = " + pdtUid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from theme_pdt where pdt_uid = " + pdtUid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_pdt_wish where pdt_uid = " + pdtUid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "delete from usr_pdt_like where pdt_uid = " + pdtUid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			sql = "update reply set status = 0 where pdt_uid = " + pdtUid;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		return "redirect:/pdt/pdt_mng";
	}
}
