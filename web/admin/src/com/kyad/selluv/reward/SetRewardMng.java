package com.kyad.selluv.reward;

import java.sql.Timestamp;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.dbconnect.SystemSetting;

@Controller
@RequestMapping(value = "/reward/set_reward_mng")
public class SetRewardMng {

	@RequestMapping(value = { "/", "" })
	public String IndexHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		for (int i=1; i<=8; i++) {
			SystemSetting inst = SystemSetting.getInstance(i);
			model.addAttribute("setting" + i, inst.getSetting());
		}
		return "reward/set_reward_mng/index";
	}

	@RequestMapping(value = { "/save" })
	public @ResponseBody String SaveHandler(ModelMap model,
			HttpSession session, String setting[]) {
		for (int i=0; i<setting.length; i++) {
			SystemSetting inst = SystemSetting.getInstance(i+1);
			boolean is_new = false;
			if (inst == null) {
				is_new = true;
				inst = new SystemSetting();
				inst.setSystemSettingUid(0);
				inst.setMemo("");
			}
			
			inst.setRegTime(new Timestamp(System.currentTimeMillis()));
			inst.setSetting(setting[i]);
			inst.setSettingExtra1("");
			inst.setSettingExtra2("");
			inst.setStatus(Constant.STATUS_USE);
			if (is_new) {
				if (inst.Save() != Constant.RESULT_SUCCESS)
					return "error";
			} else {
				if (inst.Update() != Constant.RESULT_SUCCESS)
					return "error";
			}
		}
		return "success";
	}
}
