package com.kyad.selluv.reward;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.InviteRewardHisUid;

@Controller
@RequestMapping(value = "/reward/invite_reward_mng")
public class InviteRewardMng {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", InviteRewardHisUid.getCount());
		return "reward/invite_reward_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			Integer status, String period_from, String period_to, Integer kind) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "inviter", "joiner", "usr_id", "deal_uid",
				"kind_str", "reward_price", "reg_time", "invite_usr_uid",
				"join_usr_uid", "usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " inviter like '%" + keyword + "%'";
			custom_where += " or joiner like '%" + keyword + "%'";
			custom_where += " or deal_uid like '%" + keyword + "%'";
			custom_where += " or kind_str like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";
		
		if (kind != 0)
			custom_where += " and kind="+kind;

		custom_where += " order by reg_time desc";
		try {
			json = SSP
					.simple(request,
							response,
							"(select *, "
									+ "(select usr_id from usr where usr.usr_uid = A.invite_usr_uid) as inviter, "
									+ "(select usr_id from usr where usr.usr_uid = A.join_usr_uid) as joiner, "
									+ "(select usr_id from usr where usr.usr_uid = A.usr_uid) as usr_id, "
									+ "(case kind when 1 then '초대인에게 가입리워드' "
									+ "when 2 then '가입자에게 가입리워드' "
									+ "when 3 then '초대인에게 가입자의 첫구매 리워드' end) as kind_str "
									+ "from invite_reward_his as A) as B",
							"invite_reward_his_uid", columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
}
