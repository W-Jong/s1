package com.kyad.selluv.reward;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.Brand;
import com.kyad.selluv.dbconnect.Category;
import com.kyad.selluv.dbconnect.PromotionCode;
import com.kyad.selluv.dbconnect.Usr;

@Controller
@RequestMapping(value = "/reward/code_reward_mng")
public class CodeRewardMng {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", PromotionCode.getCount());
		List<Category> category1list = Category
				.getCategoryListByParentCategory(1);
		List<Category> category2list = Category
				.getCategoryListByParentCategory(category1list.get(0)
						.getCategoryUid());
		if (category2list != null && category2list.size() > 0) {
			List<Category> category3list = Category
					.getCategoryListByParentCategory(category2list.get(0)
							.getCategoryUid());
			model.addAttribute("category2_list", category2list);
			if (category3list != null && category3list.size() > 0)
				model.addAttribute("category3_list", category3list);
		}
		model.addAttribute("category1_list", category1list);
		return "reward/code_reward_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			Integer is_available, Integer kind, String period_from,
			String period_to, Integer limit_type) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "kind_str", "title", "memo", "limit_type_str",
				"is_available_str", "reg_time", "cu_time", "promotion_code_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " kind_str like '%" + keyword + "%'";
			custom_where += " or title like '%" + keyword + "%'";
			custom_where += " or memo like '%" + keyword + "%'";
			custom_where += " or limit_type_str like '%" + keyword + "%'";
			custom_where += " or is_available_str like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (is_available != 0) {
			custom_where += " and is_available = " + is_available;
		}

		if (kind != 0) {
			custom_where += " and kind = " + kind;
		}
		
		if (limit_type != -1)
			custom_where += " and limit_type = " + limit_type;

		custom_where += " order by reg_time desc";
		try {
			json = SSP
					.simple(request,
							response,
							"(select *, IF(kind = 1, '구매용', '판매용') as kind_str, "
									+ "(case limit_type when 0 then '-' "
									+ "when 1 then '카테고리' when 2 then '브랜드' "
									+ "when 3 then '회원' when 4 then '상품' end) as limit_type_str, "
									+ "IF (start_time='1970-01-01 09:00:00', '', CONCAT(substr(start_time, 1, 10),'<br>',substr(end_time, 1, 10))) as cu_time, "
									+ "IF((start_time<=NOW() and NOW()<=end_time) || (start_time='1970-01-01 09:00:00'), '유효', '만료') as is_available_str, "
									+ "IF((start_time<=NOW() and NOW()<=end_time) || (start_time='1970-01-01 09:00:00'), '1', '2') as is_available "
									+ " from promotion_code) as A",
							"promotion_code_uid", columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{promotionCodeUid}/detail")
	public String DetailHandler(@PathVariable Integer promotionCodeUid,
			ModelMap model, HttpSession session) {
		PromotionCode pcode = PromotionCode.getInstance(promotionCodeUid);
		if (pcode == null) {
			model.addAttribute("msg", "error");
			return "reward/code_reward_mng/detail";
		}
		model.addAttribute("pcode", pcode);
		int type = pcode.getLimitType();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (pcode.getStartTime() == null || pcode.getStartTime().equals(Timestamp.valueOf("1970-01-01 09:00:00")))
			model.addAttribute("start_time", "");
		else
			model.addAttribute("start_time", sdf.format(pcode.getStartTime()));
		if (pcode.getEndTime() == null || pcode.getEndTime().equals(Timestamp.valueOf("2099-12-31 09:00:00")))
			model.addAttribute("end_time", "");
		else
			model.addAttribute("end_time", sdf.format(pcode.getEndTime()));
		if (type > 0) {
			List<String> limit_list = new ArrayList<String>();
			List<String> limit_real_list = new ArrayList<String>();
			String[] uids = pcode.getLimitUids().split(",");
			if (type == 1) {
				for (int i = 0; i < uids.length; i++) {
					Category category = Category.getInstance(Integer
							.valueOf(uids[i]));
					limit_list.add(i, category.getCategoryName());
					limit_real_list.add(i, uids[i]);
				}
			} else if (type == 2) {
				for (int i = 0; i < uids.length; i++) {
					Brand brand = Brand.getInstance(Integer.valueOf(uids[i]));
					limit_list.add(i, brand.getNameEn());
					limit_real_list.add(i, uids[i]);
				}
			} else if (type == 2) {
				for (int i = 0; i < uids.length; i++) {
					Usr usr = Usr.getInstance(Integer.valueOf(uids[i]));
					limit_list.add(i, usr.getUsrId());
					limit_real_list.add(i, uids[i]);
				}
			} else {
				for (int i = 0; i < uids.length; i++) {
					limit_list.add(i, uids[i]);
					limit_real_list.add(i, uids[i]);
				}
			}
			model.addAttribute("limit_uids", limit_list);
			model.addAttribute("limit_real_uids", limit_real_list);
		}
		return "reward/code_reward_mng/detail";
	}

	@RequestMapping(value = "/reg")
	public String DetailHandler(ModelMap model, HttpSession session) {
		return "reward/code_reward_mng/detail";
	}

	@RequestMapping(value = "/{promotionCodeUid}/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(
			@PathVariable Integer promotionCodeUid, ModelMap model,
			HttpServletRequest request, HttpSession session, Integer kind,
			String title, Long price, Integer price_unit, String period_from,
			String period_to, String memo, Integer limit_type, String limit_uids) {
		PromotionCode pcode = PromotionCode.getInstance(promotionCodeUid);
		boolean is_new = false;
		if (pcode == null) {
			is_new = true;
			pcode = new PromotionCode();
			pcode.setPromotionCodeUid(0);
			pcode.setRegTime(new Timestamp(System.currentTimeMillis()));
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (period_from == null || period_from.compareTo("") == 0) {
			pcode.setStartTime(new Timestamp((long)0));
		} else {
			Timestamp ts = Timestamp.valueOf(period_from + " 00:00:00");
			pcode.setStartTime(Timestamp.valueOf(sdf.format(ts)));
		}
		
		if (period_to == null || period_to.compareTo("") == 0) {
			Timestamp ts = Timestamp.valueOf("2099-12-31 09:00:00");
			pcode.setEndTime(Timestamp.valueOf(sdf.format(ts)));
		} else {
			Timestamp ts = Timestamp.valueOf(period_to + " 23:59:59");
			pcode.setEndTime(Timestamp.valueOf(sdf.format(ts)));
		}
		
		pcode.setKind(kind);
		pcode.setTitle(title);
		pcode.setPrice((long) price);
		pcode.setPriceUnit(price_unit);
		pcode.setMemo(memo);
		pcode.setLimitType(limit_type);
		if (limit_uids != null && limit_uids.length() > 0)
			limit_uids = limit_uids.substring(0, limit_uids.length() - 1);
		pcode.setLimitUids(limit_uids == null ? "" : limit_uids);
		pcode.setStatus(Constant.STATUS_USE);

		if (is_new) {
			if (pcode.Save() == Constant.RESULT_SUCCESS)
				return "success";
		} else {
			if (pcode.Update() == Constant.RESULT_SUCCESS)
				return "success";
		}
		return "success";
	}

	@RequestMapping(value = "/{promotionCodeUid}/remove")
	public String RemoveHandler(@PathVariable Integer promotionCodeUid,
			HttpSession session) {
		PromotionCode pcode = PromotionCode
				.getInstance(promotionCodeUid == null ? 0 : promotionCodeUid);
		pcode.setStatus(Constant.STATUS_REMOVED);
		pcode.Update();
		return "redirect:/reward/code_reward_mng";
	}
}
