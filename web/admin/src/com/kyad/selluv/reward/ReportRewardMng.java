package com.kyad.selluv.reward;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.BaseHibernateDAO;
import com.kyad.selluv.dbconnect.Pdt;
import com.kyad.selluv.dbconnect.ReportPdt;
import com.kyad.selluv.dbconnect.SystemSetting;
import com.kyad.selluv.dbconnect.Usr;

@Controller
@RequestMapping(value = "/reward/report_reward_mng")
public class ReportRewardMng {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session, Integer status) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", ReportPdt.getCount());
		if (status != null && status != 0)
			model.addAttribute("status", status);
		return "reward/report_reward_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			Integer status, String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "reporter", "pdt_uid", "seller", "pdt_name",
				"rank", "reward_yn_str", "reg_time", "status",
				"report_pdt_uid", "usr_uid", "seller_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " reporter like '%" + keyword + "%'";
			custom_where += " or pdt_uid like '%" + keyword + "%'";
			custom_where += " or seller like '%" + keyword + "%'";
			custom_where += " or pdt_name like '%" + keyword + "%'";
			custom_where += " or brand_name_en like '%" + keyword + "%'";
			custom_where += " or reward_yn_str like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (!period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (!period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (status != 0) {
			custom_where += " and status = " + status;
		}

		custom_where += " order by reg_time desc";
		try {
			json = SSP
					.simple(request,
							response,
							"(SELECT "
									+ "C.*, CONCAT_WS(' ', brand_name_ko, pdt_group, color_name, pdt_model, category_name ) AS pdt_name "
									+ "FROM ( "
									+ "SELECT "
									+ "A.*, B.usr_uid as seller_uid, B.color_name, B.pdt_model, "
									+ "(SELECT usr_id FROM usr WHERE usr.usr_uid = A.usr_uid) AS reporter, "
									+ "(SELECT usr_id FROM usr WHERE usr.usr_uid = B.usr_uid) AS seller, "
									+ "(SELECT name_en FROM brand WHERE brand.brand_uid = B.brand_uid) AS brand_name_en, "
									+ "(SELECT name_ko FROM brand WHERE brand.brand_uid = B.brand_uid) AS brand_name_ko, "
									+ "(CASE B.pdt_group WHEN '1' THEN '남성' "
									+ "WHEN '2' THEN '여성' "
									+ "ELSE '키즈' END) AS pdt_group, "
									+ "(SELECT category_name FROM category WHERE category.category_uid = B.category_uid) AS category_name, "
									+ "(CASE WHEN (A.status = '2') THEN '미처리' "
									+ "WHEN (A.status = '1' AND A.reward_yn = '1') THEN '지급' "
									+ "ELSE '미지급' END) AS reward_yn_str "
									+ "FROM report_pdt AS A LEFT JOIN pdt B ON B.pdt_uid = A.pdt_uid "
									+ ") AS C) as D", "report_pdt_uid",
							columns, custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{reportPdtUid}/detail", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String DetailHandler(
			@PathVariable Integer reportPdtUid, ModelMap model,
			HttpSession session) {
		ReportPdt rpdt = ReportPdt.getInstance(reportPdtUid);
		HashMap<String, String> list = new HashMap<String, String>();
		Usr reporter = Usr.getInstance(rpdt.getUsrUid());
		Pdt pdt = Pdt.getInstance(rpdt.getPdtUid());
		Usr seller = Usr.getInstance(pdt.getUsrUid());
		list.put("reporter", reporter.getUsrId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		list.put("reg_time", sdf.format(rpdt.getRegTime()));
		list.put("pdt_uid", String.valueOf(rpdt.getPdtUid()));
		list.put("seller", seller.getUsrId());
		list.put("pdt_name", Pdt.getPdtName(rpdt.getPdtUid()));
		list.put("content", rpdt.getContent());
		list.put("reward_yn", String.valueOf(rpdt.getRewardYn()));
		list.put("status", String.valueOf(rpdt.getStatus()));
		String json = new Gson().toJson(list);
		return json;
	}

	@RequestMapping(value = "/{reportPdtUid}/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(@PathVariable Integer reportPdtUid,
			ModelMap model, HttpServletRequest request, HttpSession session,
			Integer status) {
		ReportPdt rpdt = ReportPdt.getInstance(reportPdtUid);
		SystemSetting setting = SystemSetting.getInstance(4);
		Long price = Long.valueOf(setting.getSetting());
		int usr_uid = rpdt.getUsrUid();
		int pdt_uid = rpdt.getPdtUid();
		BaseHibernateDAO pdao = new BaseHibernateDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		try {
			if (status == 1) {
				String sql = "update report_pdt set reward_yn = " + status + ", comp_time = NOW()"
						+ ", status = 1, reward_price = " + price + " where report_pdt_uid = " + reportPdtUid;
				pdao.getSession().createSQLQuery(sql).executeUpdate();
				sql = "update usr set money = money + " + price + " where usr_uid = " + usr_uid;
				pdao.getSession().createSQLQuery(sql).executeUpdate();
				sql = "insert into money_change_his values(0, NOW(), "
						+ usr_uid + ", 4, " + usr_uid + ", "
						+ pdt_uid + ", '가품 신고 리워드 지급', " + price + ", 1)";
				pdao.getSession().createSQLQuery(sql).executeUpdate();
			} else {
				String sql = "update report_pdt set reward_yn = " + status + ", comp_time = NOW()"
						+ ", status = 1 where report_pdt_uid = " + reportPdtUid;
				pdao.getSession().createSQLQuery(sql).executeUpdate();
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		if (status == 1) {
			HttpURLConnection conn = null;
			OutputStream outputStream = null;

			try {
				URL url = new URL(Constant.SELLUV_API_ADDRESS + "/version/alarm");

				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("adminToken", Constant.SELLUV_ADMIN_TOKEN);
				conn.setConnectTimeout(10000);

				String params = "";
				params += "pushType=PUSH12_REWARD_REPORT";
				params += "&targetUid=" + reportPdtUid;

				outputStream = conn.getOutputStream();
				outputStream.write(params.getBytes("UTF-8"));
				outputStream.flush();

				int responseCode = conn.getResponseCode();

				StringBuilder response = new StringBuilder();
				if (responseCode == HttpURLConnection.HTTP_OK) {
					String line;
					BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					while ((line = br.readLine()) != null) {
						response.append(line).append("\n");
					}
					br.close();
				} else {
					response.append("");
				}

				String result = response.toString().trim();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (conn != null)
					conn.disconnect();
			}
		}
		return "success";
	}

	@RequestMapping(value = "/get_time", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String GetTimeHandler(ModelMap model, HttpServletRequest request, HttpSession session) {
		HashMap<String, String> list = new HashMap<String, String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		list.put("time", sdf.format(new Timestamp(System.currentTimeMillis())));
		String json = new Gson().toJson(list);
		return json;
	}

	@RequestMapping(value = "/{reportPdtUid}/remove")
	public String RemoveHandler(@PathVariable Integer reportPdtUid,
			HttpSession session) {
		ReportPdt rpdt = ReportPdt.getInstance(reportPdtUid == null ? 0
				: reportPdtUid);

		if (rpdt != null) {
			rpdt.setStatus(Constant.STATUS_REMOVED);
			rpdt.Update();
		}

		return "redirect:/reward/report_reward_mng";
	}
}
