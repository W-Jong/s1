package com.kyad.selluv.reward;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.common.Constant;
import com.kyad.selluv.common.SSP;
import com.kyad.selluv.dbconnect.BaseHibernateDAO;
import com.kyad.selluv.dbconnect.Deal;
import com.kyad.selluv.dbconnect.Pdt;
import com.kyad.selluv.dbconnect.Share;
import com.kyad.selluv.dbconnect.SystemSetting;
import com.kyad.selluv.dbconnect.Usr;
import com.kyad.selluv.helper.StarmanHelper;

@Controller
@RequestMapping(value = "/reward/share_reward_mng")
public class ShareRewardMng {

	@RequestMapping(value = { "/", "" })
	public String valetHandler(ModelMap model, HttpSession session,
			Integer status) {
		if (!CommonUtil.isLogin(session))
			return "redirect:/login";
		model.addAttribute("total_count", Share.getCount());
		if (status != null && status != 0)
			model.addAttribute("status", status);
		return "reward/share_reward_mng/index";
	}

	@RequestMapping(value = "/ajax_table", produces = "application/json; charset=UTF-8")
	public @ResponseBody String AjaxHandler(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String keyword,
			Integer status, String period_from, String period_to) {

		ArrayList<HashMap<String, Object>> columns = new ArrayList<HashMap<String, Object>>();
		String[] db_fields = { "reporter", "pdt_uid", "deal_uid", "pdt_name",
				"sns", "reward_yn", "reg_time", "status", "share_uid",
				"usr_uid", "deal_usr_uid" };

		for (int i = 0; i < db_fields.length; i++) {
			HashMap<String, Object> column = new HashMap<String, Object>();
			column.put("db", db_fields[i]);
			column.put("dt", i);
			if (db_fields[i].compareTo("reg_time") == 0) {
				column.put("formatter", new SSP.SSPFormatter() {

					@Override
					public String formater(String d, HashMap<String, Object> row) {
						return CommonUtil.getDateFormat(d);
					}
				});
			}
			columns.add(column);
		}

		String json = "";

		String custom_where = "status != " + Constant.STATUS_REMOVED;
		if (keyword != null && !keyword.isEmpty()) {
			custom_where += " and (";
			custom_where += " reporter like '%" + keyword + "%'";
			custom_where += " or pdt_uid like '%" + keyword + "%'";
			custom_where += " or order_uid like '%" + keyword + "%'";
			custom_where += " or pdt_name like '%" + keyword + "%'";
			custom_where += " or reward_yn like '%" + keyword + "%'";
			custom_where += " or 1=0)";
		}

		if (period_from != null && !period_from.isEmpty())
			custom_where += " and (reg_time > '" + period_from
					+ "' or reg_time like '%" + period_from + "%')";
		if (period_to != null && !period_to.isEmpty())
			custom_where += " and (reg_time < '" + period_to
					+ "' or reg_time like '%" + period_to + "%')";

		if (status != 0) {
			custom_where += " and status = " + status;
		}

		custom_where += " order by reg_time desc";
		try {
			json = SSP
					.simple(request,
							response,
							"(SELECT *, CONCAT_WS(' ', brand_name_ko, pdt_group, color_name, pdt_model, category_name) AS pdt_name, "
									+ "(CASE WHEN (D. STATUS = 2) THEN '미처리' "
									+ "WHEN (D. STATUS = 1 AND D.reward_price = 0) THEN '미지급' "
									+ "WHEN (D. STATUS = 1 AND D.reward_price > 0) THEN CONCAT('지급 ', D.sns) "
									+ "END) AS reward_yn FROM "
									+ "(SELECT A.*, C.usr_uid as deal_usr_uid, C.pdt_uid, (A.instagram_yn + A.twitter_yn + A.naverblog_yn + A.kakaostory_yn + A.facebook_yn) AS sns, "
									+ "B.color_name, B.pdt_model, "
									+ "(SELECT usr_id FROM usr WHERE usr.usr_uid = A.usr_uid) AS reporter, "
									+ "(SELECT usr_id FROM usr WHERE usr.usr_uid = B.usr_uid) AS seller, "
									+ "(SELECT name_en FROM brand WHERE brand.brand_uid = B.brand_uid) AS brand_name_en, "
									+ "(SELECT name_ko FROM brand WHERE brand.brand_uid = B.brand_uid) AS brand_name_ko, "
									+ "(CASE B.pdt_group WHEN '1' THEN '남성' "
									+ "WHEN '2' THEN '여성' "
									+ "ELSE '키즈' END) AS pdt_group, "
									+ "(SELECT category_name FROM category WHERE category.category_uid = B.category_uid) AS category_name "
									+ "FROM share AS A "
									+ "LEFT JOIN deal C ON A.deal_uid = C.deal_uid "
									+ "LEFT JOIN pdt B ON C.pdt_uid = B.pdt_uid "
									+ ") AS D) as E", "share_uid", columns,
							custom_where);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@RequestMapping(value = "/{shareUid}/detail", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public @ResponseBody String DetailHandler(@PathVariable Integer shareUid,
			ModelMap model, HttpSession session) {
		Share share = Share.getInstance(shareUid);
		HashMap<String, String> list = new HashMap<String, String>();
		Usr reporter = Usr.getInstance(share.getUsrUid());
		Deal deal = Deal.getInstance(share.getDealUid());
		list.put("reporter", reporter.getUsrId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		list.put("reg_time", sdf.format(share.getRegTime()));
		list.put("pdt_uid", String.valueOf(deal.getPdtUid()));
		list.put("deal_uid", String.valueOf(share.getDealUid()));
		list.put("pdt_name", Pdt.getPdtName(deal.getPdtUid()));
		list.put("status", String.valueOf(share.getStatus()));
		int sns = share.getInstagramYn() + share.getTwitterYn()
				+ share.getNaverblogYn() + share.getKakaostoryYn()
				+ share.getFacebookYn();
		list.put("sns", String.valueOf(sns));
		if (share.getRewardPrice() > 0)
			list.put("reward_yn", String.valueOf(1));
		else
			list.put("reward_yn", String.valueOf(0));
		String sns_str = "";
		if (sns > 0) {
			sns_str = "(";
			if (share.getInstagramYn() == 1)
				sns_str += "인스타그램, ";
			if (share.getTwitterYn() == 1)
				sns_str += "트위터, ";
			if (share.getNaverblogYn() == 1)
				sns_str += "네이버블로그, ";
			if (share.getKakaostoryYn() == 1)
				sns_str += "카카오스토리, ";
			if (share.getFacebookYn() == 1)
				sns_str += "페이스북, ";
			sns_str = sns_str.substring(0, sns_str.length() - 2);
			sns_str += ")";
		}
		list.put("sns_str", sns_str);
		list.put("instagram_yn", String.valueOf(share.getInstagramYn()));
		list.put("twitter_yn", String.valueOf(share.getTwitterYn()));
		list.put("naverblog_yn", String.valueOf(share.getNaverblogYn()));
		list.put("kakostory_yn", String.valueOf(share.getKakaostoryYn()));
		list.put("facebook_yn", String.valueOf(share.getFacebookYn()));
		String instagram_array[] = share.getInstagramPhotos().split("");
		List<String> instagram_list = new ArrayList<String>();
		for (int i = 0; i < instagram_array.length; i++) {
			instagram_list.add(i, StarmanHelper.getTargetImageUrl(
					Constant.DIR_NAME_SHARE, share.getShareUid(),
					instagram_array[i], StarmanHelper.NO_IMAGE_PATH));
		}
		list.put("instagram_list", instagram_list.toString());
		String twitter_array[] = share.getInstagramPhotos().split("");
		List<String> twitter_list = new ArrayList<String>();
		for (int i = 0; i < twitter_array.length; i++) {
			twitter_list.add(i, twitter_array[i]);
		}
		list.put("twitter_list", twitter_list.toString());
		String naverblog_array[] = share.getInstagramPhotos().split("");
		List<String> naverblog_list = new ArrayList<String>();
		for (int i = 0; i < naverblog_array.length; i++) {
			naverblog_list.add(i, naverblog_array[i]);
		}
		list.put("naverblog_list", naverblog_list.toString());
		String kakaostory_array[] = share.getInstagramPhotos().split("");
		List<String> kakaostory_list = new ArrayList<String>();
		for (int i = 0; i < kakaostory_array.length; i++) {
			kakaostory_list.add(i, kakaostory_array[i]);
		}
		list.put("kakaostory_list", kakaostory_list.toString());
		String facebook_array[] = share.getInstagramPhotos().split("");
		List<String> facebook_list = new ArrayList<String>();
		for (int i = 0; i < facebook_array.length; i++) {
			facebook_list.add(i, facebook_array[i]);
		}
		list.put("facebook_list", facebook_list.toString());
		String json = new Gson().toJson(list);
		return json;
	}

	@RequestMapping(value = "/{shareUid}/save", method = RequestMethod.POST)
	public @ResponseBody String SaveHandler(@PathVariable Integer shareUid,
			ModelMap model, HttpServletRequest request, HttpSession session,
			Integer status, Integer instagram_yn, Integer twitter_yn,
			Integer naverblog_yn, Integer kakaostory_yn, Integer facebook_yn) {
		Share rpdt = Share.getInstance(shareUid);
		SystemSetting setting = SystemSetting.getInstance(4);
		Long price = Long.valueOf(setting.getSetting());
		int usr_uid = rpdt.getUsrUid();
		Deal deal = Deal.getInstance(rpdt.getDealUid());
		int deal_uid = deal.getDealUid();
		int pdt_uid = deal.getPdtUid();
		int sns = instagram_yn + twitter_yn
				+ naverblog_yn + kakaostory_yn
				+ facebook_yn;
		BaseHibernateDAO pdao = new BaseHibernateDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		try {
			if (status == 1) {
				String sql = "update share set comp_time = NOW()"
						+ ", status = 1, reward_price = " + price
						+ ", instagram_yn = " + instagram_yn
						+ ", twitter_yn = " + twitter_yn
						+ ", naverblog_yn = " + naverblog_yn
						+ ", kakaostory_yn = " + kakaostory_yn
						+ ", facebook_yn = " + facebook_yn
						+ " where share_uid = " + shareUid;
				pdao.getSession().createSQLQuery(sql).executeUpdate();
				sql = "update usr set money = money + " + (price * sns)
						+ " where usr_uid = " + usr_uid;
				pdao.getSession().createSQLQuery(sql).executeUpdate();
				sql = "insert into money_change_his values(0, NOW(), "
						+ usr_uid + ", 3, " + deal_uid + ", " + pdt_uid
						+ ", '공유 리워드 지급', " + (price * sns) + ", 1)";
				pdao.getSession().createSQLQuery(sql).executeUpdate();
			} else {
				String sql = "update share set comp_time = NOW()"
						+ ", status = 1, reward_price = 0"
						+ ", instagram_yn = " + instagram_yn
						+ ", twitter_yn = " + twitter_yn
						+ ", naverblog_yn = " + naverblog_yn
						+ ", kakaostory_yn = " + kakaostory_yn
						+ ", facebook_yn = " + facebook_yn
						+ " where share_uid = " + shareUid;
				pdao.getSession().createSQLQuery(sql).executeUpdate();
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		return "success";
	}
}
