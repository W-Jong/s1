package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrLoginHis entity provides the base persistence definition of the
 * UsrLoginHis entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrLoginHis implements java.io.Serializable {

	// Fields

	private Long usrLoginHisUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer osTp;
	private Integer loginTp;
	private String connAddr;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrLoginHis() {
	}

	/** full constructor */
	public AbstractUsrLoginHis(Timestamp regTime, Integer usrUid, Integer osTp,
			Integer loginTp, String connAddr, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.osTp = osTp;
		this.loginTp = loginTp;
		this.connAddr = connAddr;
		this.status = status;
	}

	// Property accessors

	public Long getUsrLoginHisUid() {
		return this.usrLoginHisUid;
	}

	public void setUsrLoginHisUid(Long usrLoginHisUid) {
		this.usrLoginHisUid = usrLoginHisUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getOsTp() {
		return this.osTp;
	}

	public void setOsTp(Integer osTp) {
		this.osTp = osTp;
	}

	public Integer getLoginTp() {
		return this.loginTp;
	}

	public void setLoginTp(Integer loginTp) {
		this.loginTp = loginTp;
	}

	public String getConnAddr() {
		return this.connAddr;
	}

	public void setConnAddr(String connAddr) {
		this.connAddr = connAddr;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}