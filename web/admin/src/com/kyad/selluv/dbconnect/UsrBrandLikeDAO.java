package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrBrandLike entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrBrandLike
 * @author MyEclipse Persistence Tools
 */
public class UsrBrandLikeDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrBrandLikeDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String BRAND_UID = "brandUid";
	public static final String STATUS = "status";

	public void save(UsrBrandLike transientInstance) {
		log.debug("saving UsrBrandLike instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrBrandLike persistentInstance) {
		log.debug("deleting UsrBrandLike instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrBrandLike findById(java.lang.Integer id) {
		log.debug("getting UsrBrandLike instance with id: " + id);
		try {
			UsrBrandLike instance = (UsrBrandLike) getSession().get(
					"com.kyad.selluv.dbconnect.UsrBrandLike", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrBrandLike instance) {
		log.debug("finding UsrBrandLike instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrBrandLike")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrBrandLike instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from UsrBrandLike as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByBrandUid(Object brandUid) {
		return findByProperty(BRAND_UID, brandUid);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrBrandLike instances");
		try {
			String queryString = "from UsrBrandLike";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrBrandLike merge(UsrBrandLike detachedInstance) {
		log.debug("merging UsrBrandLike instance");
		try {
			UsrBrandLike result = (UsrBrandLike) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrBrandLike instance) {
		log.debug("attaching dirty UsrBrandLike instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrBrandLike instance) {
		log.debug("attaching clean UsrBrandLike instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}