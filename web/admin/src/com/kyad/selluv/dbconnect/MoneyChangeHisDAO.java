package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * MoneyChangeHis entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.MoneyChangeHis
 * @author MyEclipse Persistence Tools
 */
public class MoneyChangeHisDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(MoneyChangeHisDAO.class);

	public void save(MoneyChangeHis transientInstance) {
		log.debug("saving MoneyChangeHis instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(MoneyChangeHis persistentInstance) {
		log.debug("deleting MoneyChangeHis instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public MoneyChangeHis findById(java.lang.Integer id) {
		log.debug("getting MoneyChangeHis instance with id: " + id);
		try {
			MoneyChangeHis instance = (MoneyChangeHis) getSession().get(
					"com.kyad.selluv.dbconnect.MoneyChangeHis", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(MoneyChangeHis instance) {
		log.debug("finding MoneyChangeHis instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.MoneyChangeHis")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding MoneyChangeHis instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from MoneyChangeHis as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findAll() {
		log.debug("finding all MoneyChangeHis instances");
		try {
			String queryString = "from MoneyChangeHis";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public MoneyChangeHis merge(MoneyChangeHis detachedInstance) {
		log.debug("merging MoneyChangeHis instance");
		try {
			MoneyChangeHis result = (MoneyChangeHis) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(MoneyChangeHis instance) {
		log.debug("attaching dirty MoneyChangeHis instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(MoneyChangeHis instance) {
		log.debug("attaching clean MoneyChangeHis instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCountByUsrUid(int usr_uid) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.MoneyChangeHis")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq("usrUid", usr_uid));
			Long result = (Long)crit.uniqueResult();
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}