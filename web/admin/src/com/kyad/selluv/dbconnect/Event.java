package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Event entity. @author MyEclipse Persistence Tools
 */
public class Event extends AbstractEvent implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Event() {
	}

	/** full constructor */
	public Event(Timestamp regTime, Integer mypageYn, String title,
			String profileImg, String content, String detailImg,
			Timestamp startTime, Timestamp endTime, Integer status) {
		super(regTime, mypageYn, title, profileImg, content, detailImg,
				startTime, endTime, status);
	}

	public static Event getInstance(int event_uid) {
		Event event = null;
		
		if(event_uid < 1)
			return event;
		
		EventDAO ndao = new EventDAO();
		try {
			event = ndao.findById(event_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return event;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new EventDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		EventDAO ndao = new EventDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public int Save(){
		int res = Constant.RESULT_DB_ERR;
		
		EventDAO ndao = new EventDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
}
