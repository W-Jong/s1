package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrFeed entity provides the base persistence definition of the
 * UsrFeed entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrFeed implements java.io.Serializable {

	// Fields

	private Integer usrFeedUid;
	private Timestamp refTime;
	private Integer usrUid;
	private Integer pdtUid;
	private Integer type;
	private Integer peerUsrUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrFeed() {
	}

	/** full constructor */
	public AbstractUsrFeed(Timestamp refTime, Integer usrUid, Integer pdtUid,
			Integer type, Integer peerUsrUid, Integer status) {
		this.refTime = refTime;
		this.usrUid = usrUid;
		this.pdtUid = pdtUid;
		this.type = type;
		this.peerUsrUid = peerUsrUid;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrFeedUid() {
		return this.usrFeedUid;
	}

	public void setUsrFeedUid(Integer usrFeedUid) {
		this.usrFeedUid = usrFeedUid;
	}

	public Timestamp getRefTime() {
		return this.refTime;
	}

	public void setRefTime(Timestamp refTime) {
		this.refTime = refTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getPeerUsrUid() {
		return this.peerUsrUid;
	}

	public void setPeerUsrUid(Integer peerUsrUid) {
		this.peerUsrUid = peerUsrUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}