package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * PdtStyle entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.PdtStyle
 * @author MyEclipse Persistence Tools
 */
public class PdtStyleDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(PdtStyleDAO.class);
	// property constants
	public static final String PDT_UID = "pdtUid";
	public static final String USR_UID = "usrUid";
	public static final String STYLE_IMG = "styleImg";
	public static final String STATUS = "status";

	public void save(PdtStyle transientInstance) {
		log.debug("saving PdtStyle instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(PdtStyle persistentInstance) {
		log.debug("deleting PdtStyle instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public PdtStyle findById(java.lang.Integer id) {
		log.debug("getting PdtStyle instance with id: " + id);
		try {
			PdtStyle instance = (PdtStyle) getSession().get(
					"com.kyad.selluv.dbconnect.PdtStyle", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(PdtStyle instance) {
		log.debug("finding PdtStyle instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.PdtStyle")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding PdtStyle instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from PdtStyle as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByPdtUid(Object pdtUid) {
		return findByProperty(PDT_UID, pdtUid);
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByStyleImg(Object styleImg) {
		return findByProperty(STYLE_IMG, styleImg);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all PdtStyle instances");
		try {
			String queryString = "from PdtStyle";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public PdtStyle merge(PdtStyle detachedInstance) {
		log.debug("merging PdtStyle instance");
		try {
			PdtStyle result = (PdtStyle) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(PdtStyle instance) {
		log.debug("attaching dirty PdtStyle instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(PdtStyle instance) {
		log.debug("attaching clean PdtStyle instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}