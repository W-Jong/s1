package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrFeed entity. @author MyEclipse Persistence Tools
 */
public class UsrFeed extends AbstractUsrFeed implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrFeed() {
	}

	/** full constructor */
	public UsrFeed(Timestamp refTime, Integer usrUid, Integer pdtUid,
			Integer type, Integer peerUsrUid, Integer status) {
		super(refTime, usrUid, pdtUid, type, peerUsrUid, status);
	}

}
