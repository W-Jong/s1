package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * AdmUdtHis entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.AdmUdtHis
 * @author MyEclipse Persistence Tools
 */
public class AdmUdtHisDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(AdmUdtHisDAO.class);
	// property constants
	public static final String ADM_UID = "admUid";
	public static final String KIND = "kind";
	public static final String CONTENT = "content";
	public static final String STATUS = "status";

	public void save(AdmUdtHis transientInstance) {
		log.debug("saving AdmUdtHis instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(AdmUdtHis persistentInstance) {
		log.debug("deleting AdmUdtHis instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public AdmUdtHis findById(java.lang.Integer id) {
		log.debug("getting AdmUdtHis instance with id: " + id);
		try {
			AdmUdtHis instance = (AdmUdtHis) getSession().get(
					"com.kyad.selluv.dbconnect.AdmUdtHis", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(AdmUdtHis instance) {
		log.debug("finding AdmUdtHis instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.AdmUdtHis")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding AdmUdtHis instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from AdmUdtHis as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByAdmUid(Object admUid) {
		return findByProperty(ADM_UID, admUid);
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all AdmUdtHis instances");
		try {
			String queryString = "from AdmUdtHis";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public AdmUdtHis merge(AdmUdtHis detachedInstance) {
		log.debug("merging AdmUdtHis instance");
		try {
			AdmUdtHis result = (AdmUdtHis) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(AdmUdtHis instance) {
		log.debug("attaching dirty AdmUdtHis instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(AdmUdtHis instance) {
		log.debug("attaching clean AdmUdtHis instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}