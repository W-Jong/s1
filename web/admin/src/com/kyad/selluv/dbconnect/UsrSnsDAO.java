package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrSns entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrSns
 * @author MyEclipse Persistence Tools
 */
public class UsrSnsDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(UsrSnsDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String SNS_TYPE = "snsType";
	public static final String SNS_ID = "snsId";
	public static final String SNS_EMAIL = "snsEmail";
	public static final String SNS_TOKEN = "snsToken";
	public static final String STATUS = "status";

	public void save(UsrSns transientInstance) {
		log.debug("saving UsrSns instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrSns persistentInstance) {
		log.debug("deleting UsrSns instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrSns findById(java.lang.Integer id) {
		log.debug("getting UsrSns instance with id: " + id);
		try {
			UsrSns instance = (UsrSns) getSession().get(
					"com.kyad.selluv.dbconnect.UsrSns", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrSns instance) {
		log.debug("finding UsrSns instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrSns")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrSns instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrSns as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findBySnsType(Object snsType) {
		return findByProperty(SNS_TYPE, snsType);
	}

	public List findBySnsId(Object snsId) {
		return findByProperty(SNS_ID, snsId);
	}

	public List findBySnsEmail(Object snsEmail) {
		return findByProperty(SNS_EMAIL, snsEmail);
	}

	public List findBySnsToken(Object snsToken) {
		return findByProperty(SNS_TOKEN, snsToken);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrSns instances");
		try {
			String queryString = "from UsrSns";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrSns merge(UsrSns detachedInstance) {
		log.debug("merging UsrSns instance");
		try {
			UsrSns result = (UsrSns) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrSns instance) {
		log.debug("attaching dirty UsrSns instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrSns instance) {
		log.debug("attaching clean UsrSns instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}