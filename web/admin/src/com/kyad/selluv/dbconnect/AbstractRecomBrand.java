package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractRecomBrand entity provides the base persistence definition of the
 * RecomBrand entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractRecomBrand implements java.io.Serializable {

	// Fields

	private Integer recomBrandUid;
	private Timestamp regTime;
	private Integer pdtGroup;
	private Integer brandOrder;
	private Integer brandUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractRecomBrand() {
	}

	/** full constructor */
	public AbstractRecomBrand(Timestamp regTime, Integer pdtGroup,
			Integer brandOrder, Integer brandUid, Integer status) {
		this.regTime = regTime;
		this.pdtGroup = pdtGroup;
		this.brandOrder = brandOrder;
		this.brandUid = brandUid;
		this.status = status;
	}

	// Property accessors

	public Integer getRecomBrandUid() {
		return this.recomBrandUid;
	}

	public void setRecomBrandUid(Integer recomBrandUid) {
		this.recomBrandUid = recomBrandUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getPdtGroup() {
		return this.pdtGroup;
	}

	public void setPdtGroup(Integer pdtGroup) {
		this.pdtGroup = pdtGroup;
	}

	public Integer getBrandOrder() {
		return this.brandOrder;
	}

	public void setBrandOrder(Integer brandOrder) {
		this.brandOrder = brandOrder;
	}

	public Integer getBrandUid() {
		return this.brandUid;
	}

	public void setBrandUid(Integer brandUid) {
		this.brandUid = brandUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}