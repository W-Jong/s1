package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrPayment entity provides the base persistence definition of the
 * UsrPayment entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrPayment implements java.io.Serializable {

	// Fields

	private Integer usrPaymentUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer kind;
	private Long amount;
	private String iamportCode;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrPayment() {
	}

	/** full constructor */
	public AbstractUsrPayment(Timestamp regTime, Integer usrUid, Integer kind,
			Long amount, String iamportCode, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.kind = kind;
		this.amount = amount;
		this.iamportCode = iamportCode;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrPaymentUid() {
		return this.usrPaymentUid;
	}

	public void setUsrPaymentUid(Integer usrPaymentUid) {
		this.usrPaymentUid = usrPaymentUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public Long getAmount() {
		return this.amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getIamportCode() {
		return this.iamportCode;
	}

	public void setIamportCode(String iamportCode) {
		this.iamportCode = iamportCode;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}