package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Brand entity. @author MyEclipse Persistence Tools
 */
public class Brand extends AbstractBrand implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Brand() {
	}

	/** full constructor */
	public Brand(Timestamp regTime, String firstEn, String firstKo,
			String nameEn, String nameKo, String licenseUrl, String logoImg,
			String profileImg, String backImg, Integer BrandUid,
			Integer BrandCount, Long totalCount, Long totalPrice, Integer status) {
		super(regTime, firstEn, firstKo, nameEn, nameKo, licenseUrl, logoImg,
				profileImg, backImg, BrandUid, BrandCount, totalCount, totalPrice,
				status);
	}

	public static Brand getInstance(int brand_uid) {
		Brand brand = null;
		
		if(brand_uid < 1)
			return brand;
		
		BrandDAO ndao = new BrandDAO();
		try {
			brand = ndao.findById(brand_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return brand;
	}

	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		BrandDAO pdao = new BrandDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		BrandDAO pdao = new BrandDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static int getCount(int status){
		int count = 0;
		
		try{
			count = (new BrandDAO()).getCount(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
