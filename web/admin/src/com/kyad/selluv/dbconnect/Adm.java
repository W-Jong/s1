package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Adm entity. @author MyEclipse Persistence Tools
 */
public class Adm extends AbstractAdm implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Adm() {
	}

	/** full constructor */
	public Adm(Timestamp regTime, String admId, String admPwd, String admNck,
			String admMail, String admPhn, Integer privilegeUid,
			Timestamp edtTime, Integer status) {
		super(regTime, admId, admPwd, admNck, admMail, admPhn, privilegeUid,
				edtTime, status);
	}

	public static Adm getInstance(int id) {
		Adm adm = null;

		if (id < 1)
			return adm;

		AdmDAO adao = new AdmDAO();		
		try {
			adm = adao.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		adao.getSession().clear();
		adao.getSession().close();

		return adm;
	}

	public static int Verify(String id, String pwd) {

		AdmDAO adao = new AdmDAO();
		Adm adm = null;

		int res = Constant.SIGNIN_SUCCESS;

		try {
			@SuppressWarnings("unchecked")
			List<Adm> alist = adao.findByAdmId(id);

			if (alist == null || alist.size() < 1)
				res = Constant.SIGNIN_ERR_WRONG_ID;
			else {
				adm = alist.get(0);
				if (adm.getAdmPwd().compareTo(pwd) != 0)
					res = Constant.SIGNIN_ERR_WRONG_PWD;
				else
					res = adm.getAdmUid();
			}
		} catch (Exception e) {
			e.printStackTrace();
			res = Constant.SIGNIN_ERR_DB;
		}

		adao.getSession().clear();
		adao.getSession().close();

		return res;
	}

	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		AdmDAO adao = new AdmDAO();
		Transaction tx = adao.getSession().beginTransaction();
		
		try {
			adao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		adao.getSession().clear();
		adao.getSession().close();
		
		return res;
	}
}
