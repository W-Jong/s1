package com.kyad.selluv.dbconnect;

import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * RecomBrand entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.RecomBrand
 * @author MyEclipse Persistence Tools
 */
public class RecomBrandDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(RecomBrandDAO.class);

	public void save(RecomBrand transientInstance) {
		log.debug("saving RecomBrand instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(RecomBrand persistentInstance) {
		log.debug("deleting RecomBrand instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public RecomBrand findById(java.lang.Integer id) {
		log.debug("getting RecomBrand instance with id: " + id);
		try {
			RecomBrand instance = (RecomBrand) getSession().get(
					"com.kyad.selluv.dbconnect.RecomBrand", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(RecomBrand instance) {
		log.debug("finding RecomBrand instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.RecomBrand")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding RecomBrand instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from RecomBrand as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findAll() {
		log.debug("finding all RecomBrand instances");
		try {
			String queryString = "from RecomBrand";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public RecomBrand merge(RecomBrand detachedInstance) {
		log.debug("merging RecomBrand instance");
		try {
			RecomBrand result = (RecomBrand) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(RecomBrand instance) {
		log.debug("attaching dirty RecomBrand instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(RecomBrand instance) {
		log.debug("attaching clean RecomBrand instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}