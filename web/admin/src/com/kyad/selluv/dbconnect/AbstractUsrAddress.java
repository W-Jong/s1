package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrAddress entity provides the base persistence definition of the
 * UsrAddress entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrAddress implements java.io.Serializable {

	// Fields

	private Integer usrAddressUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer addressOrder;
	private String addressName;
	private String addressPhone;
	private String addressDetail;
	private String addressDetailSub;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrAddress() {
	}

	/** full constructor */
	public AbstractUsrAddress(Timestamp regTime, Integer usrUid,
			Integer addressOrder, String addressName, String addressPhone,
			String addressDetail, String addressDetailSub, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.addressOrder = addressOrder;
		this.addressName = addressName;
		this.addressPhone = addressPhone;
		this.addressDetail = addressDetail;
		this.addressDetailSub = addressDetailSub;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrAddressUid() {
		return this.usrAddressUid;
	}

	public void setUsrAddressUid(Integer usrAddressUid) {
		this.usrAddressUid = usrAddressUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getAddressOrder() {
		return this.addressOrder;
	}

	public void setAddressOrder(Integer addressOrder) {
		this.addressOrder = addressOrder;
	}

	public String getAddressName() {
		return this.addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public String getAddressPhone() {
		return this.addressPhone;
	}

	public void setAddressPhone(String addressPhone) {
		this.addressPhone = addressPhone;
	}

	public String getAddressDetail() {
		return this.addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public String getAddressDetailSub() {
		return this.addressDetailSub;
	}

	public void setAddressDetailSub(String addressDetailSub) {
		this.addressDetailSub = addressDetailSub;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}