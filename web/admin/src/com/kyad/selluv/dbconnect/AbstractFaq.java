package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractFaq entity provides the base persistence definition of the Faq
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractFaq implements java.io.Serializable {

	// Fields

	private Integer faqUid;
	private Timestamp regTime;
	private Integer kind;
	private String title;
	private String content;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractFaq() {
	}

	/** full constructor */
	public AbstractFaq(Timestamp regTime, Integer kind, String title,
			String content, Integer status) {
		this.regTime = regTime;
		this.kind = kind;
		this.title = title;
		this.content = content;
		this.status = status;
	}

	// Property accessors

	public Integer getFaqUid() {
		return this.faqUid;
	}

	public void setFaqUid(Integer faqUid) {
		this.faqUid = faqUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}