package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractSystemSetting entity provides the base persistence definition of the
 * SystemSetting entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSystemSetting implements java.io.Serializable {

	// Fields

	private Integer systemSettingUid;
	private Timestamp regTime;
	private String setting;
	private String settingExtra1;
	private String settingExtra2;
	private String memo;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractSystemSetting() {
	}

	/** full constructor */
	public AbstractSystemSetting(Integer systemSettingUid, Timestamp regTime,
			String setting, String settingExtra1, String settingExtra2,
			String memo, Integer status) {
		this.systemSettingUid = systemSettingUid;
		this.regTime = regTime;
		this.setting = setting;
		this.settingExtra1 = settingExtra1;
		this.settingExtra2 = settingExtra2;
		this.memo = memo;
		this.status = status;
	}

	// Property accessors

	public Integer getSystemSettingUid() {
		return this.systemSettingUid;
	}

	public void setSystemSettingUid(Integer systemSettingUid) {
		this.systemSettingUid = systemSettingUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getSetting() {
		return this.setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	public String getSettingExtra1() {
		return this.settingExtra1;
	}

	public void setSettingExtra1(String settingExtra1) {
		this.settingExtra1 = settingExtra1;
	}

	public String getSettingExtra2() {
		return this.settingExtra2;
	}

	public void setSettingExtra2(String settingExtra2) {
		this.settingExtra2 = settingExtra2;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}