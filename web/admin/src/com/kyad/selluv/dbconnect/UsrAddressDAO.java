package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrAddress entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrAddress
 * @author MyEclipse Persistence Tools
 */
public class UsrAddressDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrAddressDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String ADDRESS_ORDER = "addressOrder";
	public static final String ADDRESS_NAME = "addressName";
	public static final String ADDRESS_PHONE = "addressPhone";
	public static final String ADDRESS_DETAIL = "addressDetail";
	public static final String ADDRESS_DETAIL_SUB = "addressDetailSub";
	public static final String STATUS = "status";

	public void save(UsrAddress transientInstance) {
		log.debug("saving UsrAddress instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrAddress persistentInstance) {
		log.debug("deleting UsrAddress instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrAddress findById(java.lang.Integer id) {
		log.debug("getting UsrAddress instance with id: " + id);
		try {
			UsrAddress instance = (UsrAddress) getSession().get(
					"com.kyad.selluv.dbconnect.UsrAddress", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrAddress instance) {
		log.debug("finding UsrAddress instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrAddress")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrAddress instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrAddress as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByAddressOrder(Object addressOrder) {
		return findByProperty(ADDRESS_ORDER, addressOrder);
	}

	public List findByAddressName(Object addressName) {
		return findByProperty(ADDRESS_NAME, addressName);
	}

	public List findByAddressPhone(Object addressPhone) {
		return findByProperty(ADDRESS_PHONE, addressPhone);
	}

	public List findByAddressDetail(Object addressDetail) {
		return findByProperty(ADDRESS_DETAIL, addressDetail);
	}

	public List findByAddressDetailSub(Object addressDetailSub) {
		return findByProperty(ADDRESS_DETAIL_SUB, addressDetailSub);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrAddress instances");
		try {
			String queryString = "from UsrAddress";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrAddress merge(UsrAddress detachedInstance) {
		log.debug("merging UsrAddress instance");
		try {
			UsrAddress result = (UsrAddress) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrAddress instance) {
		log.debug("attaching dirty UsrAddress instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrAddress instance) {
		log.debug("attaching clean UsrAddress instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}