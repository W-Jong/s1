package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrCashback entity. @author MyEclipse Persistence Tools
 */
public class UsrCashback extends AbstractUsrCashback implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrCashback() {
	}

	/** full constructor */
	public UsrCashback(Timestamp regTime, Integer usrUid, Integer kind,
			Long amount, Integer status) {
		super(regTime, usrUid, kind, amount, status);
	}

}
