package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Usr
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Usr
 * @author MyEclipse Persistence Tools
 */
public class UsrDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(UsrDAO.class);
	// property constants
	public static final String USR_MAIL = "usrMail";
	public static final String USR_PWD = "usrPwd";
	public static final String USR_PHONE = "usrPhone";
	public static final String USR_ID = "usrId";
	public static final String USR_NM = "usrNm";
	public static final String USR_NCK_NM = "usrNckNm";
	public static final String PROFILE_IMG = "profileImg";
	public static final String PROFILE_BACK_IMG = "profileBackImg";
	public static final String GENDER = "gender";
	public static final String BANK_NM = "bankNm";
	public static final String ACCOUNT_NM = "accountNm";
	public static final String ACCOUNT_NUM = "accountNum";
	public static final String DESCRIPTION = "description";
	public static final String INVITE_USR_UID = "inviteUsrUid";
	public static final String POINT = "point";
	public static final String MONEY = "money";
	public static final String USR_LOGIN_TYPE = "usrLoginType";
	public static final String SNS_ID = "snsId";
	public static final String LIKE_GROUP = "likeGroup";
	public static final String ACCESS_TOKEN = "accessToken";
	public static final String DEVICE_TOKEN = "deviceToken";
	public static final String SLEEP_YN = "sleepYn";
	public static final String FREE_SEND_PRICE = "freeSendPrice";
	public static final String MEMO = "memo";
	public static final String STATUS = "status";

	public void save(Usr transientInstance) {
		log.debug("saving Usr instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Usr persistentInstance) {
		log.debug("deleting Usr instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Usr findById(java.lang.Integer id) {
		log.debug("getting Usr instance with id: " + id);
		try {
			Usr instance = (Usr) getSession().get(
					"com.kyad.selluv.dbconnect.Usr", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Usr instance) {
		log.debug("finding Usr instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Usr")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Usr instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Usr as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrMail(Object usrMail) {
		return findByProperty(USR_MAIL, usrMail);
	}

	public List findByUsrPwd(Object usrPwd) {
		return findByProperty(USR_PWD, usrPwd);
	}

	public List findByUsrPhone(Object usrPhone) {
		return findByProperty(USR_PHONE, usrPhone);
	}

	public List findByUsrId(Object usrId) {
		return findByProperty(USR_ID, usrId);
	}

	public List findByUsrNm(Object usrNm) {
		return findByProperty(USR_NM, usrNm);
	}

	public List findByUsrNckNm(Object usrNckNm) {
		return findByProperty(USR_NCK_NM, usrNckNm);
	}

	public List findByProfileImg(Object profileImg) {
		return findByProperty(PROFILE_IMG, profileImg);
	}

	public List findByProfileBackImg(Object profileBackImg) {
		return findByProperty(PROFILE_BACK_IMG, profileBackImg);
	}

	public List findByGender(Object gender) {
		return findByProperty(GENDER, gender);
	}

	public List findByBankNm(Object bankNm) {
		return findByProperty(BANK_NM, bankNm);
	}

	public List findByAccountNm(Object accountNm) {
		return findByProperty(ACCOUNT_NM, accountNm);
	}

	public List findByAccountNum(Object accountNum) {
		return findByProperty(ACCOUNT_NUM, accountNum);
	}

	public List findByDescription(Object description) {
		return findByProperty(DESCRIPTION, description);
	}

	public List findByInviteUsrUid(Object inviteUsrUid) {
		return findByProperty(INVITE_USR_UID, inviteUsrUid);
	}

	public List findByPoint(Object point) {
		return findByProperty(POINT, point);
	}

	public List findByMoney(Object money) {
		return findByProperty(MONEY, money);
	}

	public List findByUsrLoginType(Object usrLoginType) {
		return findByProperty(USR_LOGIN_TYPE, usrLoginType);
	}

	public List findBySnsId(Object snsId) {
		return findByProperty(SNS_ID, snsId);
	}

	public List findByLikeGroup(Object likeGroup) {
		return findByProperty(LIKE_GROUP, likeGroup);
	}

	public List findByAccessToken(Object accessToken) {
		return findByProperty(ACCESS_TOKEN, accessToken);
	}

	public List findByDeviceToken(Object deviceToken) {
		return findByProperty(DEVICE_TOKEN, deviceToken);
	}

	public List findBySleepYn(Object sleepYn) {
		return findByProperty(SLEEP_YN, sleepYn);
	}

	public List findByFreeSendPrice(Object freeSendPrice) {
		return findByProperty(FREE_SEND_PRICE, freeSendPrice);
	}
	
	public List findByMemo(Object memo) {
		return findByProperty(MEMO, memo);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Usr instances");
		try {
			String queryString = "from Usr";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Usr merge(Usr detachedInstance) {
		log.debug("merging Usr instance");
		try {
			Usr result = (Usr) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Usr instance) {
		log.debug("attaching dirty Usr instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Usr instance) {
		log.debug("attaching clean Usr instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Usr")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}