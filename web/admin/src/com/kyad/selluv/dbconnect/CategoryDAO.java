package com.kyad.selluv.dbconnect;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kyad.selluv.common.CommonUtil;

/**
 * A data access object (DAO) providing persistence and search support for
 * Category entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Category
 * @author MyEclipse Persistence Tools
 */
public class CategoryDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(CategoryDAO.class);
	// property constants
	public static final String PARENT_CATEGORY_UID = "parentCategoryUid";
	public static final String DEPTH = "depth";
	public static final String CATEGORY_NAME = "category_name";
	public static final String CATEGORY_ORDER = "categoryOrder";
	public static final String PDT_COUNT = "pdtCount";
	public static final String TOTAL_COUNT = "totalCount";
	public static final String TOTAL_PRICE = "totalPrice";
	public static final String STATUS = "status";

	public void save(Category transientInstance) {
		log.debug("saving Category instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Category persistentInstance) {
		log.debug("deleting Category instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Category findById(java.lang.Integer id) {
		log.debug("getting Category instance with id: " + id);
		try {
			Category instance = (Category) getSession().get(
					"com.kyad.selluv.dbconnect.Category", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Category instance) {
		log.debug("finding Category instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Category")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Category instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Category as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByParentCategoryUid(Object parentCategoryUid) {
		return findByProperty(PARENT_CATEGORY_UID, parentCategoryUid);
	}

	public List findByDepth(Object depth) {
		return findByProperty(DEPTH, depth);
	}

	public List findByCategoryName(Object name) {
		return findByProperty(CATEGORY_NAME, name);
	}

	public List findByCategoryOrder(Object categoryOrder) {
		return findByProperty(CATEGORY_ORDER, categoryOrder);
	}

	public List findByPdtCount(Object pdtCount) {
		return findByProperty(PDT_COUNT, pdtCount);
	}

	public List findByTotalCount(Object totalCount) {
		return findByProperty(TOTAL_COUNT, totalCount);
	}

	public List findByTotalPrice(Object totalPrice) {
		return findByProperty(TOTAL_PRICE, totalPrice);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Category instances");
		try {
			String queryString = "from Category";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Category merge(Category detachedInstance) {
		log.debug("merging Category instance");
		try {
			Category result = (Category) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Category instance) {
		log.debug("attaching dirty Category instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Category instance) {
		log.debug("attaching clean Category instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getChildCount() {
		String sqlStr = "select count(*) from category as A"
				+ " where status = 1 and (select count(*) from category where"
				+ " parent_category_uid = A.category_uid and status = 1) = 0";
		Integer res = 0;
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return 0;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
			
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		try {
			if (rsData != null)
			while (rsData.next()) {
				res = rsData.getInt(1);
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res==null?0:res;
	}
}