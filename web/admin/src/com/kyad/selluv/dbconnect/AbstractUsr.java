package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;

/**
 * AbstractUsr entity provides the base persistence definition of the Usr
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsr implements java.io.Serializable {

	// Fields

	private Integer usrUid;
	private Timestamp regTime;
	private String usrMail;
	private String usrPwd;
	private String usrPhone;
	private String usrId;
	private String usrNm;
	private String usrNckNm;
	private String profileImg;
	private String profileBackImg;
	private Date birthday;
	private Integer gender;
	private String bankNm;
	private String accountNm;
	private String accountNum;
	private String description;
	private Integer inviteUsrUid;
	private Long point;
	private Long money;
	private Integer usrLoginType;
	private String snsId;
	private Integer likeGroup;
	private String accessToken;
	private String deviceToken;
	private Timestamp loginTime;
	private Integer sleepYn;
	private Long freeSendPrice;
	private Timestamp sleepTime;
	private Timestamp edtTime;
	private Timestamp noticeTime;
	private String memo;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsr() {
	}

	/** full constructor */
	public AbstractUsr(Timestamp regTime, String usrMail, String usrPwd,
			String usrPhone, String usrId, String usrNm, String usrNckNm,
			String profileImg, String profileBackImg, Date birthday,
			Integer gender, String bankNm, String accountNm,
			String accountNum, String description, Integer inviteUsrUid,
			Long point, Long money, Integer usrLoginType, String snsId,
			Integer likeGroup, String accessToken, String deviceToken,
			Timestamp loginTime, Integer sleepYn, Long freeSendPrice,
			Timestamp sleepTime, Timestamp edtTime, Timestamp noticeTime,
			String memo, Integer status) {
		this.regTime = regTime;
		this.usrMail = usrMail;
		this.usrPwd = usrPwd;
		this.usrPhone = usrPhone;
		this.usrId = usrId;
		this.usrNm = usrNm;
		this.usrNckNm = usrNckNm;
		this.profileImg = profileImg;
		this.profileBackImg = profileBackImg;
		this.birthday = birthday;
		this.gender = gender;
		this.bankNm = bankNm;
		this.accountNm = accountNm;
		this.accountNum = accountNum;
		this.description = description;
		this.inviteUsrUid = inviteUsrUid;
		this.point = point;
		this.money = money;
		this.usrLoginType = usrLoginType;
		this.snsId = snsId;
		this.likeGroup = likeGroup;
		this.accessToken = accessToken;
		this.deviceToken = deviceToken;
		this.loginTime = loginTime;
		this.sleepYn = sleepYn;
		this.freeSendPrice = freeSendPrice;
		this.sleepTime = sleepTime;
		this.edtTime = edtTime;
		this.noticeTime = noticeTime;
		this.memo = memo;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getUsrMail() {
		return this.usrMail;
	}

	public void setUsrMail(String usrMail) {
		this.usrMail = usrMail;
	}

	public String getUsrPwd() {
		return this.usrPwd;
	}

	public void setUsrPwd(String usrPwd) {
		this.usrPwd = usrPwd;
	}

	public String getUsrPhone() {
		return this.usrPhone;
	}

	public void setUsrPhone(String usrPhone) {
		this.usrPhone = usrPhone;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getUsrNm() {
		return this.usrNm;
	}

	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}

	public String getUsrNckNm() {
		return this.usrNckNm;
	}

	public void setUsrNckNm(String usrNckNm) {
		this.usrNckNm = usrNckNm;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public String getProfileBackImg() {
		return this.profileBackImg;
	}

	public void setProfileBackImg(String profileBackImg) {
		this.profileBackImg = profileBackImg;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getBankNm() {
		return this.bankNm;
	}

	public void setBankNm(String bankNm) {
		this.bankNm = bankNm;
	}

	public String getAccountNm() {
		return this.accountNm;
	}

	public void setAccountNm(String accountNm) {
		this.accountNm = accountNm;
	}

	public String getAccountNum() {
		return this.accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getInviteUsrUid() {
		return this.inviteUsrUid;
	}

	public void setInviteUsrUid(Integer inviteUsrUid) {
		this.inviteUsrUid = inviteUsrUid;
	}

	public Long getPoint() {
		return this.point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public Long getMoney() {
		return this.money;
	}

	public void setMoney(Long money) {
		this.money = money;
	}

	public Integer getUsrLoginType() {
		return this.usrLoginType;
	}

	public void setUsrLoginType(Integer usrLoginType) {
		this.usrLoginType = usrLoginType;
	}

	public String getSnsId() {
		return this.snsId;
	}

	public void setSnsId(String snsId) {
		this.snsId = snsId;
	}

	public Integer getLikeGroup() {
		return this.likeGroup;
	}

	public void setLikeGroup(Integer likeGroup) {
		this.likeGroup = likeGroup;
	}

	public String getAccessToken() {
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getDeviceToken() {
		return this.deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Timestamp getLoginTime() {
		return this.loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	public Integer getSleepYn() {
		return this.sleepYn;
	}

	public void setSleepYn(Integer sleepYn) {
		this.sleepYn = sleepYn;
	}

	public Long getFreeSendPrice() {
		return this.freeSendPrice;
	}

	public void setFreeSendPrice(Long freeSendPrice) {
		this.freeSendPrice = freeSendPrice;
	}

	public Timestamp getSleepTime() {
		return this.sleepTime;
	}

	public void setSleepTime(Timestamp sleepTime) {
		this.sleepTime = sleepTime;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Timestamp getNoticeTime() {
		return this.noticeTime;
	}

	public void setNoticeTime(Timestamp noticeTime) {
		this.noticeTime = noticeTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getMemo() {
		return this.memo;
	}
	
	public void setMemo(String memo) {
		this.memo = memo;
	}

}