package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * UsrAddress entity. @author MyEclipse Persistence Tools
 */
public class UsrAddress extends AbstractUsrAddress implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrAddress() {
	}

	/** full constructor */
	public UsrAddress(Timestamp regTime, Integer usrUid, Integer addressOrder,
			String addressName, String addressPhone, String addressDetail,
			String addressDetailSub, Integer status) {
		super(regTime, usrUid, addressOrder, addressName, addressPhone,
				addressDetail, addressDetailSub, status);
	}

	public int Update() {
		int res = Constant.RESULT_DB_ERR;

		UsrAddressDAO pdao = new UsrAddressDAO();
		Transaction tx = pdao.getSession().beginTransaction();

		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;

		UsrAddressDAO pdao = new UsrAddressDAO();
		Transaction tx = pdao.getSession().beginTransaction();

		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		return res;
	}

	@SuppressWarnings("unchecked")
	public static UsrAddress getInstanceByUsrUidAndOrder(Integer usrUid, Integer order) {
		List<UsrAddress> list = null;
		
		UsrAddressDAO cdao = new UsrAddressDAO();
		UsrAddress exam = new UsrAddress();
		exam.setUsrUid(usrUid);
		exam.setAddressOrder(order);
		exam.setStatus(Constant.STATUS_USE);
		try {
			list = cdao.findByExample(exam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		if (list == null || list.size() == 0)
			return null;
		return list.get(0);
	}
}
