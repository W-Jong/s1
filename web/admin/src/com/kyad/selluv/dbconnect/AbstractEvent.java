package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractEvent entity provides the base persistence definition of the Event
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractEvent implements java.io.Serializable {

	// Fields

	private Integer eventUid;
	private Timestamp regTime;
	private Integer mypageYn;
	private String title;
	private String profileImg;
	private String content;
	private String detailImg;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractEvent() {
	}

	/** full constructor */
	public AbstractEvent(Timestamp regTime, Integer mypageYn, String title,
			String profileImg, String content, String detailImg,
			Timestamp startTime, Timestamp endTime, Integer status) {
		this.regTime = regTime;
		this.mypageYn = mypageYn;
		this.title = title;
		this.profileImg = profileImg;
		this.content = content;
		this.detailImg = detailImg;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
	}

	// Property accessors

	public Integer getEventUid() {
		return this.eventUid;
	}

	public void setEventUid(Integer eventUid) {
		this.eventUid = eventUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getMypageYn() {
		return this.mypageYn;
	}

	public void setMypageYn(Integer mypageYn) {
		this.mypageYn = mypageYn;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDetailImg() {
		return this.detailImg;
	}

	public void setDetailImg(String detailImg) {
		this.detailImg = detailImg;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}