package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractDealCancel entity provides the base persistence definition of the
 * DealCancel entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractDealCancel implements java.io.Serializable {

	// Fields

	private Integer dealCancelUid;
	private Timestamp regTime;
	private Integer dealUid;
	private String reason;
	private String photos;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractDealCancel() {
	}

	/** full constructor */
	public AbstractDealCancel(Timestamp regTime, Integer dealUid,
			String reason, String photos, Integer status) {
		this.regTime = regTime;
		this.dealUid = dealUid;
		this.reason = reason;
		this.photos = photos;
		this.status = status;
	}

	// Property accessors

	public Integer getDealCancelUid() {
		return this.dealCancelUid;
	}

	public void setDealCancelUid(Integer dealCancelUid) {
		this.dealCancelUid = dealCancelUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getDealUid() {
		return this.dealUid;
	}

	public void setDealUid(Integer dealUid) {
		this.dealUid = dealUid;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPhotos() {
		return this.photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}