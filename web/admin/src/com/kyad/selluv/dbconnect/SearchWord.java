package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * SearchWord entity. @author MyEclipse Persistence Tools
 */
public class SearchWord extends AbstractSearchWord implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public SearchWord() {
	}

	/** full constructor */
	public SearchWord(Timestamp regTime, String content, Integer pdtCount,
			Timestamp edtTime, Integer status) {
		super(regTime, content, pdtCount, edtTime, status);
	}

}
