package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrBlock entity provides the base persistence definition of the
 * UsrBlock entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrBlock implements java.io.Serializable {

	// Fields

	private Integer usrBlockUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer peerUsrUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrBlock() {
	}

	/** full constructor */
	public AbstractUsrBlock(Timestamp regTime, Integer usrUid,
			Integer peerUsrUid, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.peerUsrUid = peerUsrUid;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrBlockUid() {
		return this.usrBlockUid;
	}

	public void setUsrBlockUid(Integer usrBlockUid) {
		this.usrBlockUid = usrBlockUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPeerUsrUid() {
		return this.peerUsrUid;
	}

	public void setPeerUsrUid(Integer peerUsrUid) {
		this.peerUsrUid = peerUsrUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}