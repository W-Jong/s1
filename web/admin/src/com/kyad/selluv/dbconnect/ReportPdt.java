package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * ReportPdt entity. @author MyEclipse Persistence Tools
 */
public class ReportPdt extends AbstractReportPdt implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public ReportPdt() {
	}

	/** minimal constructor */
	public ReportPdt(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Integer rank, String content, Integer rewardYn, Long rewardPrice,
			Integer status) {
		super(regTime, usrUid, pdtUid, rank, content, rewardYn, rewardPrice,
				status);
	}

	/** full constructor */
	public ReportPdt(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Integer rank, String content, Integer rewardYn, Long rewardPrice,
			Timestamp compTime, Integer status) {
		super(regTime, usrUid, pdtUid, rank, content, rewardYn, rewardPrice,
				compTime, status);
	}


	public static ReportPdt getInstance(int reportPdtUid) {
		ReportPdt rpdt = null;
		
		if(reportPdtUid < 1)
			return rpdt;
		
		ReportPdtDAO ndao = new ReportPdtDAO();
		try {
			rpdt = ndao.findById(reportPdtUid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return rpdt;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		ReportPdtDAO pdao = new ReportPdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		ReportPdtDAO pdao = new ReportPdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ReportPdtDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByStatus(int status){
		int count = 0;
		
		try{
			count = (new ReportPdtDAO()).getCountByStatus(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
