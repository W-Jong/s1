package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Share entity. @author MyEclipse Persistence Tools
 */
public class Share extends AbstractShare implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Share() {
	}

	/** minimal constructor */
	public Share(Timestamp regTime, Integer ShareUid, Integer dealUid,
			Integer instagramYn, Integer twitterYn, Integer naverblogYn,
			Integer kakaostoryYn, Integer facebookYn, String instagramPhotos,
			String twitterPhotos, String naverblogPhotos,
			String kakaostoryPhotos, String facebookPhotos, Long rewardPrice,
			Integer status) {
		super(regTime, ShareUid, dealUid, instagramYn, twitterYn, naverblogYn,
				kakaostoryYn, facebookYn, instagramPhotos, twitterPhotos,
				naverblogPhotos, kakaostoryPhotos, facebookPhotos, rewardPrice,
				status);
	}

	/** full constructor */
	public Share(Timestamp regTime, Integer ShareUid, Integer dealUid,
			Integer instagramYn, Integer twitterYn, Integer naverblogYn,
			Integer kakaostoryYn, Integer facebookYn, String instagramPhotos,
			String twitterPhotos, String naverblogPhotos,
			String kakaostoryPhotos, String facebookPhotos, Long rewardPrice,
			Timestamp compTime, Integer status) {
		super(regTime, ShareUid, dealUid, instagramYn, twitterYn, naverblogYn,
				kakaostoryYn, facebookYn, instagramPhotos, twitterPhotos,
				naverblogPhotos, kakaostoryPhotos, facebookPhotos, rewardPrice,
				compTime, status);
	}

	public static Share getInstance(int share_uid) {
		Share share = null;
		
		if(share_uid < 1)
			return share;
		
		ShareDAO ndao = new ShareDAO();
		try {
			share = ndao.findById(share_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return share;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		ShareDAO pdao = new ShareDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ShareDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByStatus(int status){
		int count = 0;
		
		try{
			count = (new ShareDAO()).getCountByStatus(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
