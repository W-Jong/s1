package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Faq entity. @author MyEclipse Persistence Tools
 */
public class Faq extends AbstractFaq implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Faq() {
	}

	/** full constructor */
	public Faq(Timestamp regTime, Integer kind, String title, String content,
			Integer status) {
		super(regTime, kind, title, content, status);
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new FaqDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static Faq getInstance(int faq_uid) {
		Faq faq = null;
		
		if(faq_uid < 1)
			return faq;
		
		FaqDAO ndao = new FaqDAO();
		try {
			faq = ndao.findById(faq_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return faq;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		FaqDAO ndao = new FaqDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public int Save(){
		int res = Constant.RESULT_DB_ERR;
		
		FaqDAO ndao = new FaqDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
}
