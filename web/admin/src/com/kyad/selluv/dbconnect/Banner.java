package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Banner entity. @author MyEclipse Persistence Tools
 */
public class Banner extends AbstractBanner implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Banner() {
	}

	/** full constructor */
	public Banner(Timestamp regTime, Integer type, Integer targetUid,
			String title, String profileImg, Timestamp startTime,
			Timestamp endTime, Integer status) {
		super(regTime, type, targetUid, title, profileImg, startTime, endTime,
				status);
	}

	public static Banner getInstance(int banner_uid) {
		Banner banner = null;
		
		if(banner_uid < 1)
			return banner;
		
		BannerDAO ndao = new BannerDAO();
		try {
			banner = ndao.findById(banner_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return banner;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new BannerDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		BannerDAO ndao = new BannerDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public int Save(){
		int res = Constant.RESULT_DB_ERR;
		
		BannerDAO ndao = new BannerDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
}
