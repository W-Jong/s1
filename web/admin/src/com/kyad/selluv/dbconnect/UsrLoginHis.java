package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import com.kyad.selluv.common.Constant;
import com.kyad.selluv.statistics.VisitStatistics.Graph;

/**
 * UsrLoginHis entity. @author MyEclipse Persistence Tools
 */
public class UsrLoginHis extends AbstractUsrLoginHis implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrLoginHis() {
	}

	/** full constructor */
	public UsrLoginHis(Timestamp regTime, Integer usrUid, Integer osTp,
			Integer loginTp, String connAddr, Integer status) {
		super(regTime, usrUid, osTp, loginTp, connAddr, status);
	}

	@SuppressWarnings("unchecked")
	public static Integer getCountByUsrUid(Integer usrUid){
		List<UsrLoginHis> list = null;
		
		UsrLoginHisDAO cdao = new UsrLoginHisDAO();
		UsrLoginHis exam = new UsrLoginHis();
		exam.setUsrUid(usrUid);
		exam.setStatus(Constant.STATUS_USE);
		try {
			list = cdao.findByExample(exam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		return list.size();
	}
	
	public static Timestamp getLastLoginTime(Integer usrUid){
		Timestamp ts = null;
		
		try{
			ts = (new UsrLoginHisDAO()).getLastLoginTime(usrUid);
		}catch(Exception e){
			return ts;
		}
		
		return ts;
	}
}
