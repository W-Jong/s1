package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * DealCancel entity. @author MyEclipse Persistence Tools
 */
public class DealCancel extends AbstractDealCancel implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public DealCancel() {
	}

	/** full constructor */
	public DealCancel(Timestamp regTime, Integer dealUid, String reason,
			String photos, Integer status) {
		super(regTime, dealUid, reason, photos, status);
	}

}
