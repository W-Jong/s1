package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Deal entity. @author MyEclipse Persistence Tools
 */
public class Deal extends AbstractDeal implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Deal() {
	}

	/** minimal constructor */
	public Deal(Timestamp regTime, Integer pdtUid, Integer usrUid,
			Integer sellerUsrUid, Integer valetUid, String recipientNm,
			String recipientPhone, String recipientAddress,
			String deliveryNumber, String verifyDeliveryNumber, String brandEn,
			String pdtTitle, String pdtImg, Long pdtPrice, Long sendPrice,
			Long verifyPrice, Long rewardPrice, Long price,
			Long payPromotionPrice, String payPromotion, String payImportCode,
			Integer payImportMethod, Long selluvPrice, Long payFeePrice,
			Long cashPrice, Long cashPromotionPrice, String cashPromotion,
			String nicepayCode, String cashAccount, Long reqPrice,
			String cancelReason, String refundReason,
			String refundDeliveryNumber, String refundPhotos, String memo,
			Integer status) {
		super(regTime, pdtUid, usrUid, sellerUsrUid, valetUid, recipientNm,
				recipientPhone, recipientAddress, deliveryNumber,
				verifyDeliveryNumber, brandEn, pdtTitle, pdtImg, pdtPrice,
				sendPrice, verifyPrice, rewardPrice, price, payPromotionPrice,
				payPromotion, payImportCode, payImportMethod, selluvPrice,
				payFeePrice, cashPrice, cashPromotionPrice, cashPromotion,
				nicepayCode, cashAccount, reqPrice, cancelReason, refundReason,
				refundDeliveryNumber, refundPhotos, memo, status);
	}

	/** full constructor */
	public Deal(Timestamp regTime, Integer pdtUid, Integer usrUid,
			Integer sellerUsrUid, Integer valetUid, String recipientNm,
			String recipientPhone, String recipientAddress,
			String deliveryNumber, String verifyDeliveryNumber, String brandEn,
			String pdtTitle, String pdtImg, Long pdtPrice, Long sendPrice,
			Timestamp payTime, Long verifyPrice, Long rewardPrice, Long price,
			Long payPromotionPrice, String payPromotion, String payImportCode,
			Integer payImportMethod, Long selluvPrice, Long payFeePrice,
			Long cashPrice, Long cashPromotionPrice, String cashPromotion,
			String nicepayCode, String cashAccount, Timestamp negoTime,
			Long reqPrice, Timestamp deliveryTime, Timestamp compTime,
			Timestamp cashCompTime, String cancelReason, Timestamp cancelTime,
			String refundReason, Timestamp refundReqTime,
			Timestamp refundCheckTime, Timestamp refundCompTime,
			String refundDeliveryNumber, String refundPhotos, String memo,
			Integer status) {
		super(regTime, pdtUid, usrUid, sellerUsrUid, valetUid, recipientNm,
				recipientPhone, recipientAddress, deliveryNumber,
				verifyDeliveryNumber, brandEn, pdtTitle, pdtImg, pdtPrice,
				sendPrice, payTime, verifyPrice, rewardPrice, price,
				payPromotionPrice, payPromotion, payImportCode,
				payImportMethod, selluvPrice, payFeePrice, cashPrice,
				cashPromotionPrice, cashPromotion, nicepayCode, cashAccount,
				negoTime, reqPrice, deliveryTime, compTime, cashCompTime,
				cancelReason, cancelTime, refundReason, refundReqTime,
				refundCheckTime, refundCompTime, refundDeliveryNumber,
				refundPhotos, memo, status);
	}

	public static Deal getInstance(int deal_uid) {
		Deal deal = null;
		
		if(deal_uid < 1)
			return deal;
		
		DealDAO ndao = new DealDAO();
		try {
			deal = ndao.findById(deal_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return deal;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new DealDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCount(int status){
		int count = 0;
		
		try{
			count = (new DealDAO()).getCount(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		DealDAO pdao = new DealDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		DealDAO pdao = new DealDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static int getBrandCount(){
		int count = 0;
		
		try{
			count = (new DealDAO()).getBrandCount();
		}catch(Exception e){	
			return count;
		}
		
		return count;
	}
	
	public static int getCategoryCount(){
		int count = 0;
		
		try{
			count = (new DealDAO()).getCategoryCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByPeriod(String period) {
		int count = 0;
		
		try{
			count = (new DealDAO()).getCountByPeriod(period);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static long getPriceByPeriod(String period) {
		long price = 0;
		
		try{
			price = (new DealDAO()).getPriceByPeriod(period);
		}catch(Exception e){
			return price;
		}
		
		return price;
	}
	
	public static int getPayCountByUsrUid(int usr_uid) {
		int count = 0;
		try{
			count = (new DealDAO()).getPayCountByUsrUid(usr_uid);
		}catch(Exception e){
			throw(e);
		}
		return count;
	}
	
	public static int getCashCountByUsrUid(int usr_uid) {
		int count = 0;
		try{
			count = (new DealDAO()).getCashCountByUsrUid(usr_uid);
		}catch(Exception e){
			return count;
		}
		return count;
	}
}
