package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Pdt
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Pdt
 * @author MyEclipse Persistence Tools
 */
public class PdtDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(PdtDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String BRAND_UID = "brandUid";
	public static final String PDT_GROUP = "pdtGroup";
	public static final String CATEGORY_UID = "categoryUid";
	public static final String PDT_SIZE = "pdtSize";
	public static final String PROFILE_IMG = "profileImg";
	public static final String PHOTOS = "photos";
	public static final String PHOTOSHOP_YN = "photoshopYn";
	public static final String ORIGIN_PRICE = "originPrice";
	public static final String PRICE = "price";
	public static final String SEND_PRICE = "sendPrice";
	public static final String PDT_CONDITION = "pdtCondition";
	public static final String COMPONENT = "component";
	public static final String ETC = "etc";
	public static final String COLOR_NAME = "colorName";
	public static final String PDT_MODEL = "pdtModel";
	public static final String PROMOTION_CODE = "promotionCode";
	public static final String CONTENT = "content";
	public static final String TAG = "tag";
	public static final String SIGN_IMG = "signImg";
	public static final String VALET_UID = "valetUid";
	public static final String MEMO = "memo";
	public static final String FAME_SCORE = "fameScore";
	public static final String NEGO_YN = "negoYn";
	public static final String STATUS = "status";

	public void save(Pdt transientInstance) {
		log.debug("saving Pdt instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Pdt persistentInstance) {
		log.debug("deleting Pdt instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Pdt findById(java.lang.Integer id) {
		log.debug("getting Pdt instance with id: " + id);
		try {
			Pdt instance = (Pdt) getSession().get(
					"com.kyad.selluv.dbconnect.Pdt", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Pdt instance) {
		log.debug("finding Pdt instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Pdt")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Pdt instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Pdt as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByBrandUid(Object brandUid) {
		return findByProperty(BRAND_UID, brandUid);
	}

	public List findByPdtGroup(Object pdtGroup) {
		return findByProperty(PDT_GROUP, pdtGroup);
	}

	public List findByCategoryUid(Object categoryUid) {
		return findByProperty(CATEGORY_UID, categoryUid);
	}

	public List findByPdtSize(Object pdtSize) {
		return findByProperty(PDT_SIZE, pdtSize);
	}

	public List findByProfileImg(Object profileImg) {
		return findByProperty(PROFILE_IMG, profileImg);
	}

	public List findByPhotos(Object photos) {
		return findByProperty(PHOTOS, photos);
	}

	public List findByPhotoshopYn(Object photoshopYn) {
		return findByProperty(PHOTOSHOP_YN, photoshopYn);
	}

	public List findByOriginPrice(Object originPrice) {
		return findByProperty(ORIGIN_PRICE, originPrice);
	}

	public List findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List findBySendPrice(Object sendPrice) {
		return findByProperty(SEND_PRICE, sendPrice);
	}

	public List findByPdtCondition(Object pdtCondition) {
		return findByProperty(PDT_CONDITION, pdtCondition);
	}

	public List findByComponent(Object component) {
		return findByProperty(COMPONENT, component);
	}

	public List findByEtc(Object etc) {
		return findByProperty(ETC, etc);
	}

	public List findByColorName(Object colorName) {
		return findByProperty(COLOR_NAME, colorName);
	}

	public List findByPdtModel(Object pdtModel) {
		return findByProperty(PDT_MODEL, pdtModel);
	}

	public List findByPromotionCode(Object promotionCode) {
		return findByProperty(PROMOTION_CODE, promotionCode);
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByTag(Object tag) {
		return findByProperty(TAG, tag);
	}

	public List findBySignImg(Object signImg) {
		return findByProperty(SIGN_IMG, signImg);
	}

	public List findByValetUid(Object valetUid) {
		return findByProperty(VALET_UID, valetUid);
	}

	public List findByMemo(Object memo) {
		return findByProperty(MEMO, memo);
	}

	public List findByFameScore(Object fameScore) {
		return findByProperty(FAME_SCORE, fameScore);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Pdt instances");
		try {
			String queryString = "from Pdt";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Pdt merge(Pdt detachedInstance) {
		log.debug("merging Pdt instance");
		try {
			Pdt result = (Pdt) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Pdt instance) {
		log.debug("attaching dirty Pdt instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Pdt instance) {
		log.debug("attaching clean Pdt instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Pdt")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCountByPhotoshopYn(int photoshop_yn) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Pdt")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq(PHOTOSHOP_YN, photoshop_yn))
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}