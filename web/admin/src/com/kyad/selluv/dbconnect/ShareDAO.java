package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Share
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Share
 * @author MyEclipse Persistence Tools
 */
public class ShareDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(ShareDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String DEAL_UID = "dealUid";
	public static final String INSTAGRAM_YN = "instagramYn";
	public static final String TWITTER_YN = "twitterYn";
	public static final String NAVERBLOG_YN = "naverblogYn";
	public static final String KAKAOSTORY_YN = "kakaostoryYn";
	public static final String FACEBOOK_YN = "facebookYn";
	public static final String INSTAGRAM_PHOTOS = "instagramPhotos";
	public static final String TWITTER_PHOTOS = "twitterPhotos";
	public static final String NAVERBLOG_PHOTOS = "naverblogPhotos";
	public static final String KAKAOSTORY_PHOTOS = "kakaostoryPhotos";
	public static final String FACEBOOK_PHOTOS = "facebookPhotos";
	public static final String REWARD_PRICE = "rewardPrice";
	public static final String STATUS = "status";

	public void save(Share transientInstance) {
		log.debug("saving Share instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Share persistentInstance) {
		log.debug("deleting Share instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Share findById(java.lang.Integer id) {
		log.debug("getting Share instance with id: " + id);
		try {
			Share instance = (Share) getSession().get(
					"com.kyad.selluv.dbconnect.Share", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Share instance) {
		log.debug("finding Share instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Share")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Share instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Share as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByDealUid(Object dealUid) {
		return findByProperty(DEAL_UID, dealUid);
	}

	public List findByInstagramYn(Object instagramYn) {
		return findByProperty(INSTAGRAM_YN, instagramYn);
	}

	public List findByTwitterYn(Object twitterYn) {
		return findByProperty(TWITTER_YN, twitterYn);
	}

	public List findByNaverblogYn(Object naverblogYn) {
		return findByProperty(NAVERBLOG_YN, naverblogYn);
	}

	public List findByKakaostoryYn(Object kakaostoryYn) {
		return findByProperty(KAKAOSTORY_YN, kakaostoryYn);
	}

	public List findByFacebookYn(Object facebookYn) {
		return findByProperty(FACEBOOK_YN, facebookYn);
	}

	public List findByInstagramPhotos(Object instagramPhotos) {
		return findByProperty(INSTAGRAM_PHOTOS, instagramPhotos);
	}

	public List findByTwitterPhotos(Object twitterPhotos) {
		return findByProperty(TWITTER_PHOTOS, twitterPhotos);
	}

	public List findByNaverblogPhotos(Object naverblogPhotos) {
		return findByProperty(NAVERBLOG_PHOTOS, naverblogPhotos);
	}

	public List findByKakaostoryPhotos(Object kakaostoryPhotos) {
		return findByProperty(KAKAOSTORY_PHOTOS, kakaostoryPhotos);
	}

	public List findByFacebookPhotos(Object facebookPhotos) {
		return findByProperty(FACEBOOK_PHOTOS, facebookPhotos);
	}

	public List findByRewardPrice(Object rewardPrice) {
		return findByProperty(REWARD_PRICE, rewardPrice);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Share instances");
		try {
			String queryString = "from Share";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Share merge(Share detachedInstance) {
		log.debug("merging Share instance");
		try {
			Share result = (Share) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Share instance) {
		log.debug("attaching dirty Share instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Share instance) {
		log.debug("attaching clean Share instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Share")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCountByStatus(int status) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Share")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq(STATUS, status));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}