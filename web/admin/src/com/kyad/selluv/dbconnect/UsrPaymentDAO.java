package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrPayment entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrPayment
 * @author MyEclipse Persistence Tools
 */
public class UsrPaymentDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrPaymentDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String KIND = "kind";
	public static final String AMOUNT = "amount";
	public static final String IAMPORT_CODE = "iamportCode";
	public static final String STATUS = "status";

	public void save(UsrPayment transientInstance) {
		log.debug("saving UsrPayment instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrPayment persistentInstance) {
		log.debug("deleting UsrPayment instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrPayment findById(java.lang.Integer id) {
		log.debug("getting UsrPayment instance with id: " + id);
		try {
			UsrPayment instance = (UsrPayment) getSession().get(
					"com.kyad.selluv.dbconnect.UsrPayment", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrPayment instance) {
		log.debug("finding UsrPayment instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrPayment")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrPayment instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrPayment as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByAmount(Object amount) {
		return findByProperty(AMOUNT, amount);
	}

	public List findByIamportCode(Object iamportCode) {
		return findByProperty(IAMPORT_CODE, iamportCode);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrPayment instances");
		try {
			String queryString = "from UsrPayment";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrPayment merge(UsrPayment detachedInstance) {
		log.debug("merging UsrPayment instance");
		try {
			UsrPayment result = (UsrPayment) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrPayment instance) {
		log.debug("attaching dirty UsrPayment instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrPayment instance) {
		log.debug("attaching clean UsrPayment instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}