package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Usr entity. @author MyEclipse Persistence Tools
 */
public class Usr extends AbstractUsr implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Usr() {
	}

	/** full constructor */
	public Usr(Timestamp regTime, String usrMail, String usrPwd,
			String usrPhone, String usrId, String usrNm, String usrNckNm,
			String profileImg, String profileBackImg, Date birthday,
			Integer gender, String bankNm, String accountNm, String accountNum,
			String description, Integer inviteUsrUid, Long point, Long money,
			Integer usrLoginType, String snsId, Integer likeGroup,
			String accessToken, String deviceToken, Timestamp loginTime,
			Integer sleepYn, Long freeSendPrice, Timestamp sleepTime,
			Timestamp edtTime, Timestamp noticeTime, String memo, Integer status) {
		super(regTime, usrMail, usrPwd, usrPhone, usrId, usrNm, usrNckNm,
				profileImg, profileBackImg, birthday, gender, bankNm,
				accountNm, accountNum, description, inviteUsrUid, point, money,
				usrLoginType, snsId, likeGroup, accessToken, deviceToken,
				loginTime, sleepYn, freeSendPrice, sleepTime, edtTime,
				noticeTime, memo, status);
	}

	public static Usr getInstance(int usr_uid) {
		Usr usr = null;

		if (usr_uid < 1)
			return usr;

		UsrDAO ndao = new UsrDAO();
		try {
			usr = ndao.findById(usr_uid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();

		return usr;
	}

	public int Update() {
		int res = Constant.RESULT_DB_ERR;

		UsrDAO pdao = new UsrDAO();
		Transaction tx = pdao.getSession().beginTransaction();

		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;

		UsrDAO pdao = new UsrDAO();
		Transaction tx = pdao.getSession().beginTransaction();

		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		return res;
	}

	public static int getCount() {
		int count = 0;

		try {
			count = (new UsrDAO()).getCount();
		} catch (Exception e) {
			return count;
		}

		return count;
	}

	@SuppressWarnings("unchecked")
	public static List<Usr> getListByUsrId(String usrId) {
		List<Usr> list = null;

		UsrDAO cdao = new UsrDAO();
		Usr exam = new Usr();
		exam.setUsrId(usrId);
		exam.setStatus(Constant.STATUS_USE);
		try {
			list = cdao.findByExample(exam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();

		return list;
	}
}
