package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Qna entity. @author MyEclipse Persistence Tools
 */
public class Qna extends AbstractQna implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Qna() {
	}

	/** minimal constructor */
	public Qna(Timestamp regTime, Integer usrUid, String title, String content,
			String answer, String memo, Integer status) {
		super(regTime, usrUid, title, content, answer, memo, status);
	}

	/** full constructor */
	public Qna(Timestamp regTime, Integer usrUid, String title, String content,
			String answer, Timestamp compTime, String memo, Integer status) {
		super(regTime, usrUid, title, content, answer, compTime, memo, status);
	}

	public static Qna getInstance(int qna_uid) {
		Qna qna = null;
		
		if(qna_uid < 1)
			return qna;
		
		QnaDAO ndao = new QnaDAO();
		try {
			qna = ndao.findById(qna_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return qna;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		QnaDAO ndao = new QnaDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new QnaDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByStatus(int status){
		int count = 0;
		
		try{
			count = (new QnaDAO()).getCountByStatus(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
