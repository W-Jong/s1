package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrPdtWish entity. @author MyEclipse Persistence Tools
 */
public class UsrPdtWish extends AbstractUsrPdtWish implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrPdtWish() {
	}

	/** full constructor */
	public UsrPdtWish(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Integer status) {
		super(regTime, usrUid, pdtUid, status);
	}

}
