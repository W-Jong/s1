package com.kyad.selluv.dbconnect;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kyad.selluv.common.CommonUtil;
import com.kyad.selluv.statistics.VisitStatistics.Graph;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrLoginHis entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrLoginHis
 * @author MyEclipse Persistence Tools
 */
public class UsrLoginHisDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrLoginHisDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String OS_TP = "osTp";
	public static final String LOGIN_TP = "loginTp";
	public static final String CONN_ADDR = "connAddr";
	public static final String STATUS = "status";

	public void save(UsrLoginHis transientInstance) {
		log.debug("saving UsrLoginHis instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrLoginHis persistentInstance) {
		log.debug("deleting UsrLoginHis instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrLoginHis findById(java.lang.Long id) {
		log.debug("getting UsrLoginHis instance with id: " + id);
		try {
			UsrLoginHis instance = (UsrLoginHis) getSession().get(
					"com.kyad.selluv.dbconnect.UsrLoginHis", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrLoginHis instance) {
		log.debug("finding UsrLoginHis instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrLoginHis")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrLoginHis instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrLoginHis as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByOsTp(Object osTp) {
		return findByProperty(OS_TP, osTp);
	}

	public List findByLoginTp(Object loginTp) {
		return findByProperty(LOGIN_TP, loginTp);
	}

	public List findByConnAddr(Object connAddr) {
		return findByProperty(CONN_ADDR, connAddr);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrLoginHis instances");
		try {
			String queryString = "from UsrLoginHis";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrLoginHis merge(UsrLoginHis detachedInstance) {
		log.debug("merging UsrLoginHis instance");
		try {
			UsrLoginHis result = (UsrLoginHis) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrLoginHis instance) {
		log.debug("attaching dirty UsrLoginHis instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrLoginHis instance) {
		log.debug("attaching clean UsrLoginHis instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public Timestamp getLastLoginTime(Integer usrUid) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrLoginHis")
					.addOrder(Order.desc("regTime"))
					.add(Restrictions.eq(USR_UID, usrUid))
					.setProjection(Projections.property("regTime"));
			
			Timestamp result = (Timestamp)crit.uniqueResult();			
			return result;
		} catch (RuntimeException re) {
			throw re;
		}
	}
}