package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractQna entity provides the base persistence definition of the Qna
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractQna implements java.io.Serializable {

	// Fields

	private Integer qnaUid;
	private Timestamp regTime;
	private Integer usrUid;
	private String title;
	private String content;
	private String answer;
	private Timestamp compTime;
	private String memo;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractQna() {
	}

	/** minimal constructor */
	public AbstractQna(Timestamp regTime, Integer usrUid, String title,
			String content, String answer, String memo, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.title = title;
		this.content = content;
		this.answer = answer;
		this.memo = memo;
		this.status = status;
	}

	/** full constructor */
	public AbstractQna(Timestamp regTime, Integer usrUid, String title,
			String content, String answer, Timestamp compTime, String memo, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.title = title;
		this.content = content;
		this.answer = answer;
		this.compTime = compTime;
		this.memo = memo;
		this.status = status;
	}

	// Property accessors

	public Integer getQnaUid() {
		return this.qnaUid;
	}

	public void setQnaUid(Integer qnaUid) {
		this.qnaUid = qnaUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Timestamp getCompTime() {
		return this.compTime;
	}

	public void setCompTime(Timestamp compTime) {
		this.compTime = compTime;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}