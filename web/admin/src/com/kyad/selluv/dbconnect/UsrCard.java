package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;

/**
 * UsrCard entity. @author MyEclipse Persistence Tools
 */
public class UsrCard extends AbstractUsrCard implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrCard() {
	}

	/** full constructor */
	public UsrCard(Timestamp regTime, Integer usrUid, Integer kind,
			String cardNumber, Date validDate, String validNum,
			String password, Integer iamportValidYn, Integer status) {
		super(regTime, usrUid, kind, cardNumber, validDate, validNum, password,
				iamportValidYn, status);
	}

}
