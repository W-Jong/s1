package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractSearchWord entity provides the base persistence definition of the
 * SearchWord entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSearchWord implements java.io.Serializable {

	// Fields

	private Integer searchWordUid;
	private Timestamp regTime;
	private String content;
	private Integer pdtCount;
	private Timestamp edtTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractSearchWord() {
	}

	/** full constructor */
	public AbstractSearchWord(Timestamp regTime, String content,
			Integer pdtCount, Timestamp edtTime, Integer status) {
		this.regTime = regTime;
		this.content = content;
		this.pdtCount = pdtCount;
		this.edtTime = edtTime;
		this.status = status;
	}

	// Property accessors

	public Integer getSearchWordUid() {
		return this.searchWordUid;
	}

	public void setSearchWordUid(Integer searchWordUid) {
		this.searchWordUid = searchWordUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getPdtCount() {
		return this.pdtCount;
	}

	public void setPdtCount(Integer pdtCount) {
		this.pdtCount = pdtCount;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}