package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * InviteRewardHisUid entity. @author MyEclipse Persistence Tools
 */
public class InviteRewardHisUid extends AbstractInviteRewardHisUid implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public InviteRewardHisUid() {
	}

	/** full constructor */
	public InviteRewardHisUid(Timestamp regTime, Integer usrUid,
			Integer inviteUsrUid, Integer joinUsrUid, Integer kind,
			Integer dealUid, Long rewardPrice, Integer status) {
		super(regTime, usrUid, inviteUsrUid, joinUsrUid, kind, dealUid,
				rewardPrice, status);
	}

	public static InviteRewardHisUid getInstance(int inviteRewardHisUid) {
		InviteRewardHisUid rpdt = null;
		
		if(inviteRewardHisUid < 1)
			return rpdt;
		
		InviteRewardHisUidDAO ndao = new InviteRewardHisUidDAO();
		try {
			rpdt = ndao.findById(inviteRewardHisUid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return rpdt;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		InviteRewardHisUidDAO pdao = new InviteRewardHisUidDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		InviteRewardHisUidDAO pdao = new InviteRewardHisUidDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public static int getCount(){
		int count = 0;
		
		try{
			count = (new InviteRewardHisUidDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
