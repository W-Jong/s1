package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Adm
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Adm
 * @author MyEclipse Persistence Tools
 */
public class AdmDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(AdmDAO.class);
	// property constants
	public static final String ADM_ID = "admId";
	public static final String ADM_PWD = "admPwd";
	public static final String ADM_NCK = "admNck";
	public static final String ADM_MAIL = "admMail";
	public static final String ADM_PHN = "admPhn";
	public static final String PRIVILEGE_UID = "privilegeUid";
	public static final String STATUS = "status";

	public void save(Adm transientInstance) {
		log.debug("saving Adm instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Adm persistentInstance) {
		log.debug("deleting Adm instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Adm findById(java.lang.Integer id) {
		log.debug("getting Adm instance with id: " + id);
		try {
			Adm instance = (Adm) getSession().get(
					"com.kyad.selluv.dbconnect.Adm", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Adm instance) {
		log.debug("finding Adm instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Adm")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Adm instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Adm as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByAdmId(Object admId) {
		return findByProperty(ADM_ID, admId);
	}

	public List findByAdmPwd(Object admPwd) {
		return findByProperty(ADM_PWD, admPwd);
	}

	public List findByAdmNck(Object admNck) {
		return findByProperty(ADM_NCK, admNck);
	}

	public List findByAdmMail(Object admMail) {
		return findByProperty(ADM_MAIL, admMail);
	}

	public List findByAdmPhn(Object admPhn) {
		return findByProperty(ADM_PHN, admPhn);
	}

	public List findByPrivilegeUid(Object privilegeUid) {
		return findByProperty(PRIVILEGE_UID, privilegeUid);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Adm instances");
		try {
			String queryString = "from Adm";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Adm merge(Adm detachedInstance) {
		log.debug("merging Adm instance");
		try {
			Adm result = (Adm) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Adm instance) {
		log.debug("attaching dirty Adm instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Adm instance) {
		log.debug("attaching clean Adm instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}