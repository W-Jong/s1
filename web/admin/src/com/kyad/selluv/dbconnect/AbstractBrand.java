package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractBrand entity provides the base persistence definition of the Brand
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractBrand implements java.io.Serializable {

	// Fields

	private Integer brandUid;
	private Timestamp regTime;
	private String firstEn;
	private String firstKo;
	private String nameEn;
	private String nameKo;
	private String licenseUrl;
	private String logoImg;
	private String profileImg;
	private String backImg;
	private Integer usrUid;
	private Integer pdtCount;
	private Long totalCount;
	private Long totalPrice;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractBrand() {
	}

	/** full constructor */
	public AbstractBrand(Timestamp regTime, String firstEn, String firstKo,
			String nameEn, String nameKo, String licenseUrl, String logoImg,
			String profileImg, String backImg, Integer usrUid,
			Integer pdtCount, Long totalCount, Long totalPrice, Integer status) {
		this.regTime = regTime;
		this.firstEn = firstEn;
		this.firstKo = firstKo;
		this.nameEn = nameEn;
		this.nameKo = nameKo;
		this.licenseUrl = licenseUrl;
		this.logoImg = logoImg;
		this.profileImg = profileImg;
		this.backImg = backImg;
		this.usrUid = usrUid;
		this.pdtCount = pdtCount;
		this.totalCount = totalCount;
		this.totalPrice = totalPrice;
		this.status = status;
	}

	// Property accessors

	public Integer getBrandUid() {
		return this.brandUid;
	}

	public void setBrandUid(Integer brandUid) {
		this.brandUid = brandUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getFirstEn() {
		return this.firstEn;
	}

	public void setFirstEn(String firstEn) {
		this.firstEn = firstEn;
	}

	public String getFirstKo() {
		return this.firstKo;
	}

	public void setFirstKo(String firstKo) {
		this.firstKo = firstKo;
	}

	public String getNameEn() {
		return this.nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getNameKo() {
		return this.nameKo;
	}

	public void setNameKo(String nameKo) {
		this.nameKo = nameKo;
	}

	public String getLicenseUrl() {
		return this.licenseUrl;
	}

	public void setLicenseUrl(String licenseUrl) {
		this.licenseUrl = licenseUrl;
	}

	public String getLogoImg() {
		return this.logoImg;
	}

	public void setLogoImg(String logoImg) {
		this.logoImg = logoImg;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public String getBackImg() {
		return this.backImg;
	}

	public void setBackImg(String backImg) {
		this.backImg = backImg;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPdtCount() {
		return this.pdtCount;
	}

	public void setPdtCount(Integer pdtCount) {
		this.pdtCount = pdtCount;
	}

	public Long getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}