package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrWish entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrWish
 * @author MyEclipse Persistence Tools
 */
public class UsrWishDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(UsrWishDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String BRAND_UID = "brandUid";
	public static final String PDT_GROUP = "pdtGroup";
	public static final String CATEGORY_UID = "categoryUid";
	public static final String PDT_MODEL = "pdtModel";
	public static final String PDT_SIZE = "pdtSize";
	public static final String COLOR_NAME = "colorName";
	public static final String STATUS = "status";

	public void save(UsrWish transientInstance) {
		log.debug("saving UsrWish instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrWish persistentInstance) {
		log.debug("deleting UsrWish instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrWish findById(java.lang.Integer id) {
		log.debug("getting UsrWish instance with id: " + id);
		try {
			UsrWish instance = (UsrWish) getSession().get(
					"com.kyad.selluv.dbconnect.UsrWish", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrWish instance) {
		log.debug("finding UsrWish instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrWish")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrWish instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrWish as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByBrandUid(Object brandUid) {
		return findByProperty(BRAND_UID, brandUid);
	}

	public List findByPdtGroup(Object pdtGroup) {
		return findByProperty(PDT_GROUP, pdtGroup);
	}

	public List findByCategoryUid(Object categoryUid) {
		return findByProperty(CATEGORY_UID, categoryUid);
	}

	public List findByPdtModel(Object pdtModel) {
		return findByProperty(PDT_MODEL, pdtModel);
	}

	public List findByPdtSize(Object pdtSize) {
		return findByProperty(PDT_SIZE, pdtSize);
	}

	public List findByColorName(Object colorName) {
		return findByProperty(COLOR_NAME, colorName);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrWish instances");
		try {
			String queryString = "from UsrWish";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrWish merge(UsrWish detachedInstance) {
		log.debug("merging UsrWish instance");
		try {
			UsrWish result = (UsrWish) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrWish instance) {
		log.debug("attaching dirty UsrWish instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrWish instance) {
		log.debug("attaching clean UsrWish instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}