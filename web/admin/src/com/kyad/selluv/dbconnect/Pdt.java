package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Pdt entity. @author MyEclipse Persistence Tools
 */
public class Pdt extends AbstractPdt implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Pdt() {
	}

	/** full constructor */
	public Pdt(Timestamp regTime, Integer usrUid, Integer brandUid,
			Integer pdtGroup, Integer categoryUid, String pdtSize,
			String profileImg, String photos, Integer photoshopYn,
			Long originPrice, Long price, Long sendPrice, Integer pdtCondition,
			String component, String etc, String colorName, String pdtModel,
			String promotionCode, String content, String tag, String signImg,
			Integer valetUid, String memo, Timestamp edtTime,
			Integer fameScore, Integer negoYn, Integer status) {
		super(regTime, usrUid, brandUid, pdtGroup, categoryUid, pdtSize,
				profileImg, photos, photoshopYn, originPrice, price, sendPrice,
				pdtCondition, component, etc, colorName, pdtModel,
				promotionCode, content, tag, signImg, valetUid, memo, edtTime,
				fameScore, negoYn, status);
	}

	public static Pdt getInstance(int pdt_uid) {
		Pdt pdt = null;
		
		if(pdt_uid < 1)
			return pdt;
		
		PdtDAO ndao = new PdtDAO();
		try {
			pdt = ndao.findById(pdt_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return pdt;
	}
	
	public static List<Pdt> getListByCategoryUid(int categoryUid) {
		List<Pdt> list = null;
		
		if(categoryUid < 1)
			return list;
		
		PdtDAO ndao = new PdtDAO();
		try {
			list = ndao.findByCategoryUid(categoryUid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return list;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		PdtDAO pdao = new PdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		PdtDAO pdao = new PdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new PdtDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByPhotoshopYn(int photoshop_yn){
		int count = 0;
		
		try{
			count = (new PdtDAO()).getCountByPhotoshopYn(photoshop_yn);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static String getPdtName(int pdt_uid) {
		Pdt pdt = Pdt.getInstance(pdt_uid);
		Category category = Category.getInstance(pdt.getCategoryUid());
		Brand brand = Brand.getInstance(pdt.getBrandUid());
		String group = "";
		if (pdt.getPdtGroup() == Constant.PDT_GROUP_MALE)
			group = "남성";
		if (pdt.getPdtGroup() == Constant.PDT_GROUP_MALE)
			group = "여성";
		else
			group = "키즈";
		return brand.getNameKo() + ' ' + group + ' ' + pdt.getColorName() + ' ' + pdt.getPdtModel() + ' ' + category.getCategoryName();
	}
}
