package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrSns entity provides the base persistence definition of the UsrSns
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrSns implements java.io.Serializable {

	// Fields

	private Integer usrSnsUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer snsType;
	private String snsId;
	private String snsEmail;
	private String snsToken;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrSns() {
	}

	/** full constructor */
	public AbstractUsrSns(Timestamp regTime, Integer usrUid, Integer snsType,
			String snsId, String snsEmail, String snsToken, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.snsType = snsType;
		this.snsId = snsId;
		this.snsEmail = snsEmail;
		this.snsToken = snsToken;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrSnsUid() {
		return this.usrSnsUid;
	}

	public void setUsrSnsUid(Integer usrSnsUid) {
		this.usrSnsUid = usrSnsUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getSnsType() {
		return this.snsType;
	}

	public void setSnsType(Integer snsType) {
		this.snsType = snsType;
	}

	public String getSnsId() {
		return this.snsId;
	}

	public void setSnsId(String snsId) {
		this.snsId = snsId;
	}

	public String getSnsEmail() {
		return this.snsEmail;
	}

	public void setSnsEmail(String snsEmail) {
		this.snsEmail = snsEmail;
	}

	public String getSnsToken() {
		return this.snsToken;
	}

	public void setSnsToken(String snsToken) {
		this.snsToken = snsToken;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}