package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractDeal entity provides the base persistence definition of the Deal
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractDeal implements java.io.Serializable {

	// Fields

	private Integer dealUid;
	private Timestamp regTime;
	private Integer pdtUid;
	private Integer usrUid;
	private Integer sellerUsrUid;
	private Integer valetUid;
	private String recipientNm;
	private String recipientPhone;
	private String recipientAddress;
	private String deliveryNumber;
	private String verifyDeliveryNumber;
	private String brandEn;
	private String pdtTitle;
	private String pdtImg;
	private Long pdtPrice;
	private Long sendPrice;
	private Timestamp payTime;
	private Long verifyPrice;
	private Long rewardPrice;
	private Long price;
	private Long payPromotionPrice;
	private String payPromotion;
	private String payImportCode;
	private Integer payImportMethod;
	private Long selluvPrice;
	private Long payFeePrice;
	private Long cashPrice;
	private Long cashPromotionPrice;
	private String cashPromotion;
	private String nicepayCode;
	private String cashAccount;
	private Timestamp negoTime;
	private Long reqPrice;
	private Timestamp deliveryTime;
	private Timestamp compTime;
	private Timestamp cashCompTime;
	private String cancelReason;
	private Timestamp cancelTime;
	private String refundReason;
	private Timestamp refundReqTime;
	private Timestamp refundCheckTime;
	private Timestamp refundCompTime;
	private String refundDeliveryNumber;
	private String refundPhotos;
	private String memo;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractDeal() {
	}

	/** minimal constructor */
	public AbstractDeal(Timestamp regTime, Integer pdtUid, Integer usrUid,
			Integer sellerUsrUid, Integer valetUid, String recipientNm,
			String recipientPhone, String recipientAddress,
			String deliveryNumber, String verifyDeliveryNumber, String brandEn,
			String pdtTitle, String pdtImg, Long pdtPrice, Long sendPrice,
			Long verifyPrice, Long rewardPrice, Long price,
			Long payPromotionPrice, String payPromotion, String payImportCode,
			Integer payImportMethod, Long selluvPrice, Long payFeePrice,
			Long cashPrice, Long cashPromotionPrice, String cashPromotion,
			String nicepayCode, String cashAccount, Long reqPrice,
			String cancelReason, String refundReason,
			String refundDeliveryNumber, String refundPhotos, String memo,
			Integer status) {
		this.regTime = regTime;
		this.pdtUid = pdtUid;
		this.usrUid = usrUid;
		this.sellerUsrUid = sellerUsrUid;
		this.valetUid = valetUid;
		this.recipientNm = recipientNm;
		this.recipientPhone = recipientPhone;
		this.recipientAddress = recipientAddress;
		this.deliveryNumber = deliveryNumber;
		this.verifyDeliveryNumber = verifyDeliveryNumber;
		this.brandEn = brandEn;
		this.pdtTitle = pdtTitle;
		this.pdtImg = pdtImg;
		this.pdtPrice = pdtPrice;
		this.sendPrice = sendPrice;
		this.verifyPrice = verifyPrice;
		this.rewardPrice = rewardPrice;
		this.price = price;
		this.payPromotionPrice = payPromotionPrice;
		this.payPromotion = payPromotion;
		this.payImportCode = payImportCode;
		this.payImportMethod = payImportMethod;
		this.selluvPrice = selluvPrice;
		this.payFeePrice = payFeePrice;
		this.cashPrice = cashPrice;
		this.cashPromotionPrice = cashPromotionPrice;
		this.cashPromotion = cashPromotion;
		this.nicepayCode = nicepayCode;
		this.cashAccount = cashAccount;
		this.reqPrice = reqPrice;
		this.cancelReason = cancelReason;
		this.refundReason = refundReason;
		this.refundDeliveryNumber = refundDeliveryNumber;
		this.refundPhotos = refundPhotos;
		this.memo = memo;
		this.status = status;
	}

	/** full constructor */
	public AbstractDeal(Timestamp regTime, Integer pdtUid, Integer usrUid,
			Integer sellerUsrUid, Integer valetUid, String recipientNm,
			String recipientPhone, String recipientAddress,
			String deliveryNumber, String verifyDeliveryNumber, String brandEn,
			String pdtTitle, String pdtImg, Long pdtPrice, Long sendPrice,
			Timestamp payTime, Long verifyPrice, Long rewardPrice, Long price,
			Long payPromotionPrice, String payPromotion, String payImportCode,
			Integer payImportMethod, Long selluvPrice, Long payFeePrice,
			Long cashPrice, Long cashPromotionPrice, String cashPromotion,
			String nicepayCode, String cashAccount, Timestamp negoTime,
			Long reqPrice, Timestamp deliveryTime, Timestamp compTime,
			Timestamp cashCompTime, String cancelReason, Timestamp cancelTime,
			String refundReason, Timestamp refundReqTime,
			Timestamp refundCheckTime, Timestamp refundCompTime,
			String refundDeliveryNumber, String refundPhotos, String memo,
			Integer status) {
		this.regTime = regTime;
		this.pdtUid = pdtUid;
		this.usrUid = usrUid;
		this.sellerUsrUid = sellerUsrUid;
		this.valetUid = valetUid;
		this.recipientNm = recipientNm;
		this.recipientPhone = recipientPhone;
		this.recipientAddress = recipientAddress;
		this.deliveryNumber = deliveryNumber;
		this.verifyDeliveryNumber = verifyDeliveryNumber;
		this.brandEn = brandEn;
		this.pdtTitle = pdtTitle;
		this.pdtImg = pdtImg;
		this.pdtPrice = pdtPrice;
		this.sendPrice = sendPrice;
		this.payTime = payTime;
		this.verifyPrice = verifyPrice;
		this.rewardPrice = rewardPrice;
		this.price = price;
		this.payPromotionPrice = payPromotionPrice;
		this.payPromotion = payPromotion;
		this.payImportCode = payImportCode;
		this.payImportMethod = payImportMethod;
		this.selluvPrice = selluvPrice;
		this.payFeePrice = payFeePrice;
		this.cashPrice = cashPrice;
		this.cashPromotionPrice = cashPromotionPrice;
		this.cashPromotion = cashPromotion;
		this.nicepayCode = nicepayCode;
		this.cashAccount = cashAccount;
		this.negoTime = negoTime;
		this.reqPrice = reqPrice;
		this.deliveryTime = deliveryTime;
		this.compTime = compTime;
		this.cashCompTime = cashCompTime;
		this.cancelReason = cancelReason;
		this.cancelTime = cancelTime;
		this.refundReason = refundReason;
		this.refundReqTime = refundReqTime;
		this.refundCheckTime = refundCheckTime;
		this.refundCompTime = refundCompTime;
		this.refundDeliveryNumber = refundDeliveryNumber;
		this.refundPhotos = refundPhotos;
		this.memo = memo;
		this.status = status;
	}

	// Property accessors

	public Integer getDealUid() {
		return this.dealUid;
	}

	public void setDealUid(Integer dealUid) {
		this.dealUid = dealUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getSellerUsrUid() {
		return this.sellerUsrUid;
	}

	public void setSellerUsrUid(Integer sellerUsrUid) {
		this.sellerUsrUid = sellerUsrUid;
	}

	public Integer getValetUid() {
		return this.valetUid;
	}

	public void setValetUid(Integer valetUid) {
		this.valetUid = valetUid;
	}

	public String getRecipientNm() {
		return this.recipientNm;
	}

	public void setRecipientNm(String recipientNm) {
		this.recipientNm = recipientNm;
	}

	public String getRecipientPhone() {
		return this.recipientPhone;
	}

	public void setRecipientPhone(String recipientPhone) {
		this.recipientPhone = recipientPhone;
	}

	public String getRecipientAddress() {
		return this.recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public String getDeliveryNumber() {
		return this.deliveryNumber;
	}

	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}

	public String getVerifyDeliveryNumber() {
		return this.verifyDeliveryNumber;
	}

	public void setVerifyDeliveryNumber(String verifyDeliveryNumber) {
		this.verifyDeliveryNumber = verifyDeliveryNumber;
	}

	public String getBrandEn() {
		return this.brandEn;
	}

	public void setBrandEn(String brandEn) {
		this.brandEn = brandEn;
	}

	public String getPdtTitle() {
		return this.pdtTitle;
	}

	public void setPdtTitle(String pdtTitle) {
		this.pdtTitle = pdtTitle;
	}

	public String getPdtImg() {
		return this.pdtImg;
	}

	public void setPdtImg(String pdtImg) {
		this.pdtImg = pdtImg;
	}

	public Long getPdtPrice() {
		return this.pdtPrice;
	}

	public void setPdtPrice(Long pdtPrice) {
		this.pdtPrice = pdtPrice;
	}

	public Long getSendPrice() {
		return this.sendPrice;
	}

	public void setSendPrice(Long sendPrice) {
		this.sendPrice = sendPrice;
	}

	public Timestamp getPayTime() {
		return this.payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}

	public Long getVerifyPrice() {
		return this.verifyPrice;
	}

	public void setVerifyPrice(Long verifyPrice) {
		this.verifyPrice = verifyPrice;
	}

	public Long getRewardPrice() {
		return this.rewardPrice;
	}

	public void setRewardPrice(Long rewardPrice) {
		this.rewardPrice = rewardPrice;
	}

	public Long getPrice() {
		return this.price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getPayPromotionPrice() {
		return this.payPromotionPrice;
	}

	public void setPayPromotionPrice(Long payPromotionPrice) {
		this.payPromotionPrice = payPromotionPrice;
	}

	public String getPayPromotion() {
		return this.payPromotion;
	}

	public void setPayPromotion(String payPromotion) {
		this.payPromotion = payPromotion;
	}

	public String getPayImportCode() {
		return this.payImportCode;
	}

	public void setPayImportCode(String payImportCode) {
		this.payImportCode = payImportCode;
	}

	public Integer getPayImportMethod() {
		return this.payImportMethod;
	}

	public void setPayImportMethod(Integer payImportMethod) {
		this.payImportMethod = payImportMethod;
	}

	public Long getSelluvPrice() {
		return this.selluvPrice;
	}

	public void setSelluvPrice(Long selluvPrice) {
		this.selluvPrice = selluvPrice;
	}

	public Long getPayFeePrice() {
		return this.payFeePrice;
	}

	public void setPayFeePrice(Long payFeePrice) {
		this.payFeePrice = payFeePrice;
	}

	public Long getCashPrice() {
		return this.cashPrice;
	}

	public void setCashPrice(Long cashPrice) {
		this.cashPrice = cashPrice;
	}

	public Long getCashPromotionPrice() {
		return this.cashPromotionPrice;
	}

	public void setCashPromotionPrice(Long cashPromotionPrice) {
		this.cashPromotionPrice = cashPromotionPrice;
	}

	public String getCashPromotion() {
		return this.cashPromotion;
	}

	public void setCashPromotion(String cashPromotion) {
		this.cashPromotion = cashPromotion;
	}

	public String getNicepayCode() {
		return this.nicepayCode;
	}

	public void setNicepayCode(String nicepayCode) {
		this.nicepayCode = nicepayCode;
	}

	public String getCashAccount() {
		return this.cashAccount;
	}

	public void setCashAccount(String cashAccount) {
		this.cashAccount = cashAccount;
	}

	public Timestamp getNegoTime() {
		return this.negoTime;
	}

	public void setNegoTime(Timestamp negoTime) {
		this.negoTime = negoTime;
	}

	public Long getReqPrice() {
		return this.reqPrice;
	}

	public void setReqPrice(Long reqPrice) {
		this.reqPrice = reqPrice;
	}

	public Timestamp getDeliveryTime() {
		return this.deliveryTime;
	}

	public void setDeliveryTime(Timestamp deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Timestamp getCompTime() {
		return this.compTime;
	}

	public void setCompTime(Timestamp compTime) {
		this.compTime = compTime;
	}

	public Timestamp getCashCompTime() {
		return this.cashCompTime;
	}

	public void setCashCompTime(Timestamp cashCompTime) {
		this.cashCompTime = cashCompTime;
	}

	public String getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Timestamp getCancelTime() {
		return this.cancelTime;
	}

	public void setCancelTime(Timestamp cancelTime) {
		this.cancelTime = cancelTime;
	}

	public String getRefundReason() {
		return this.refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public Timestamp getRefundReqTime() {
		return this.refundReqTime;
	}

	public void setRefundReqTime(Timestamp refundReqTime) {
		this.refundReqTime = refundReqTime;
	}

	public Timestamp getRefundCheckTime() {
		return this.refundCheckTime;
	}

	public void setRefundCheckTime(Timestamp refundCheckTime) {
		this.refundCheckTime = refundCheckTime;
	}

	public Timestamp getRefundCompTime() {
		return this.refundCompTime;
	}

	public void setRefundCompTime(Timestamp refundCompTime) {
		this.refundCompTime = refundCompTime;
	}

	public String getRefundDeliveryNumber() {
		return this.refundDeliveryNumber;
	}

	public void setRefundDeliveryNumber(String refundDeliveryNumber) {
		this.refundDeliveryNumber = refundDeliveryNumber;
	}

	public String getRefundPhotos() {
		return this.refundPhotos;
	}

	public void setRefundPhotos(String refundPhotos) {
		this.refundPhotos = refundPhotos;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}