package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * TagWord entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.TagWord
 * @author MyEclipse Persistence Tools
 */
public class TagWordDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(TagWordDAO.class);
	// property constants
	public static final String CONTENT = "content";
	public static final String PDT_COUNT = "pdtCount";
	public static final String STATUS = "status";

	public void save(TagWord transientInstance) {
		log.debug("saving TagWord instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(TagWord persistentInstance) {
		log.debug("deleting TagWord instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TagWord findById(java.lang.Integer id) {
		log.debug("getting TagWord instance with id: " + id);
		try {
			TagWord instance = (TagWord) getSession().get(
					"com.kyad.selluv.dbconnect.TagWord", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TagWord instance) {
		log.debug("finding TagWord instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.TagWord")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding TagWord instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from TagWord as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByPdtCount(Object pdtCount) {
		return findByProperty(PDT_COUNT, pdtCount);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all TagWord instances");
		try {
			String queryString = "from TagWord";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public TagWord merge(TagWord detachedInstance) {
		log.debug("merging TagWord instance");
		try {
			TagWord result = (TagWord) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(TagWord instance) {
		log.debug("attaching dirty TagWord instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TagWord instance) {
		log.debug("attaching clean TagWord instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}