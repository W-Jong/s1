package com.kyad.selluv.dbconnect;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kyad.selluv.common.CommonUtil;

/**
 * A data access object (DAO) providing persistence and search support for Deal
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Deal
 * @author MyEclipse Persistence Tools
 */
public class DealDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(DealDAO.class);
	public static final String STATUS = "status";

	public void save(Deal transientInstance) {
		log.debug("saving Deal instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Deal persistentInstance) {
		log.debug("deleting Deal instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Deal findById(java.lang.Integer id) {
		log.debug("getting Deal instance with id: " + id);
		try {
			Deal instance = (Deal) getSession().get(
					"com.kyad.selluv.dbconnect.Deal", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Deal instance) {
		log.debug("finding Deal instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Deal")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Deal instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Deal as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findAll() {
		log.debug("finding all Deal instances");
		try {
			String queryString = "from Deal";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Deal merge(Deal detachedInstance) {
		log.debug("merging Deal instance");
		try {
			Deal result = (Deal) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Deal instance) {
		log.debug("attaching dirty Deal instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Deal instance) {
		log.debug("attaching clean Deal instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getBrandCount() {
		String sqlStr = "select count(*) from (select brand_uid from deal as A left join pdt B on A.pdt_uid = B.pdt_uid group by brand_uid) as C";
		Integer res = 0;
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return 0;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		try {
			while (rsData.next()) {
				res = rsData.getInt(1);
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Deal")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCount(int status) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Deal")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq(STATUS, status));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCategoryCount() {
		String sqlStr = "select count(*) from (select category_uid from deal as A left join pdt B on A.pdt_uid = B.pdt_uid group by category_uid) as C";
		Integer res = 0;
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return 0;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		try {
			while (rsData.next()) {
				res = rsData.getInt(1);
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public int getCountByPeriod(String period) {
		String sqlStr = "select count(*) from deal where"
				+ " pay_import_code is not null and pay_import_code != '' and"
				+ " status != 9 and status != 10 and reg_time like '%" + period + "%'";
		Integer res = 0;
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return 0;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
			
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		try {
			if (rsData != null)
			while (rsData.next()) {
				res = rsData.getInt(1);
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res==null?0:res;
	}
	
	public long getPriceByPeriod(String period) {
		String sqlStr = "select sum(price) from deal where"
				+ " pay_import_code is not null and pay_import_code != '' and"
				+ " status != 9 and status != 10 and reg_time like '%" + period + "%'";
		Long res = (long)0;
		Connection conn;
		try {
			conn = CommonUtil.sqlConnect();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return 0;
		}
		
		ResultSet rsData = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sqlStr);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		try {
			if (rsData != null)
			while (rsData.next()) {
				res = rsData.getLong(1);
			}
			rsData.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res==null?0:res;
	}
	
	public int getPayCountByUsrUid(int usr_uid) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Deal")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq("usrUid", usr_uid))
					.add(Restrictions.between(STATUS, 1, 8));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCashCountByUsrUid(int usr_uid) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Deal")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq("sellerUsrUid", usr_uid))
					.add(Restrictions.eq(STATUS, 6));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}