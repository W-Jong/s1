package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrLike entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrLike
 * @author MyEclipse Persistence Tools
 */
public class UsrLikeDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(UsrLikeDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String PEER_USR_UID = "peerUsrUid";
	public static final String STATUS = "status";

	public void save(UsrLike transientInstance) {
		log.debug("saving UsrLike instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrLike persistentInstance) {
		log.debug("deleting UsrLike instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrLike findById(java.lang.Integer id) {
		log.debug("getting UsrLike instance with id: " + id);
		try {
			UsrLike instance = (UsrLike) getSession().get(
					"com.kyad.selluv.dbconnect.UsrLike", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrLike instance) {
		log.debug("finding UsrLike instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrLike")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrLike instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrLike as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByPeerUsrUid(Object peerUsrUid) {
		return findByProperty(PEER_USR_UID, peerUsrUid);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrLike instances");
		try {
			String queryString = "from UsrLike";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrLike merge(UsrLike detachedInstance) {
		log.debug("merging UsrLike instance");
		try {
			UsrLike result = (UsrLike) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrLike instance) {
		log.debug("attaching dirty UsrLike instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrLike instance) {
		log.debug("attaching clean UsrLike instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}