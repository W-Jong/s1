package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Faq
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Faq
 * @author MyEclipse Persistence Tools
 */
public class FaqDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(FaqDAO.class);
	// property constants
	public static final String KIND = "kind";
	public static final String TITLE = "title";
	public static final String CONTENT = "content";
	public static final String STATUS = "status";

	public void save(Faq transientInstance) {
		log.debug("saving Faq instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Faq persistentInstance) {
		log.debug("deleting Faq instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Faq findById(java.lang.Integer id) {
		log.debug("getting Faq instance with id: " + id);
		try {
			Faq instance = (Faq) getSession().get(
					"com.kyad.selluv.dbconnect.Faq", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Faq instance) {
		log.debug("finding Faq instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Faq")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Faq instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Faq as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByTitle(Object title) {
		return findByProperty(TITLE, title);
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Faq instances");
		try {
			String queryString = "from Faq";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Faq merge(Faq detachedInstance) {
		log.debug("merging Faq instance");
		try {
			Faq result = (Faq) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Faq instance) {
		log.debug("attaching dirty Faq instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Faq instance) {
		log.debug("attaching clean Faq instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Faq")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}