package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * Privilege entity. @author MyEclipse Persistence Tools
 */
public class Privilege extends AbstractPrivilege implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Privilege() {
	}

	/** full constructor */
	public Privilege(Timestamp regTime, Integer regAdmUid, String menu,
			String privilegeNm, Integer edtAdmUid, Timestamp edtTime,
			Integer status) {
		super(regTime, regAdmUid, menu, privilegeNm, edtAdmUid, edtTime, status);
	}

}
