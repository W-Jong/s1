package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrCashback entity provides the base persistence definition of the
 * UsrCashback entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrCashback implements java.io.Serializable {

	// Fields

	private Integer usrCashbackUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer kind;
	private Long amount;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrCashback() {
	}

	/** full constructor */
	public AbstractUsrCashback(Timestamp regTime, Integer usrUid, Integer kind,
			Long amount, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.kind = kind;
		this.amount = amount;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrCashbackUid() {
		return this.usrCashbackUid;
	}

	public void setUsrCashbackUid(Integer usrCashbackUid) {
		this.usrCashbackUid = usrCashbackUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public Long getAmount() {
		return this.amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}