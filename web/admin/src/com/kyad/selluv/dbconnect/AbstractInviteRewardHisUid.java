package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractInviteRewardHisUid entity provides the base persistence definition of
 * the InviteRewardHisUid entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractInviteRewardHisUid implements
		java.io.Serializable {

	// Fields

	private Integer inviteRewardHisUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer inviteUsrUid;
	private Integer joinUsrUid;
	private Integer kind;
	private Integer dealUid;
	private Long rewardPrice;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractInviteRewardHisUid() {
	}

	/** full constructor */
	public AbstractInviteRewardHisUid(Timestamp regTime, Integer usrUid,
			Integer inviteUsrUid, Integer joinUsrUid, Integer kind,
			Integer dealUid, Long rewardPrice, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.inviteUsrUid = inviteUsrUid;
		this.joinUsrUid = joinUsrUid;
		this.kind = kind;
		this.dealUid = dealUid;
		this.rewardPrice = rewardPrice;
		this.status = status;
	}

	// Property accessors

	public Integer getInviteRewardHisUid() {
		return this.inviteRewardHisUid;
	}

	public void setInviteRewardHisUid(Integer inviteRewardHisUid) {
		this.inviteRewardHisUid = inviteRewardHisUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getInviteUsrUid() {
		return this.inviteUsrUid;
	}

	public void setInviteUsrUid(Integer inviteUsrUid) {
		this.inviteUsrUid = inviteUsrUid;
	}

	public Integer getJoinUsrUid() {
		return this.joinUsrUid;
	}

	public void setJoinUsrUid(Integer joinUsrUid) {
		this.joinUsrUid = joinUsrUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public Integer getDealUid() {
		return this.dealUid;
	}

	public void setDealUid(Integer dealUid) {
		this.dealUid = dealUid;
	}

	public Long getRewardPrice() {
		return this.rewardPrice;
	}

	public void setRewardPrice(Long rewardPrice) {
		this.rewardPrice = rewardPrice;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}