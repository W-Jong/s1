package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractReportPdt entity provides the base persistence definition of the
 * ReportPdt entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractReportPdt implements java.io.Serializable {

	// Fields

	private Integer reportPdtUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer pdtUid;
	private Integer rank;
	private String content;
	private Integer rewardYn;
	private Long rewardPrice;
	private Timestamp compTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractReportPdt() {
	}

	/** minimal constructor */
	public AbstractReportPdt(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Integer rank, String content, Integer rewardYn, Long rewardPrice,
			Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.pdtUid = pdtUid;
		this.rank = rank;
		this.content = content;
		this.rewardYn = rewardYn;
		this.rewardPrice = rewardPrice;
		this.status = status;
	}

	/** full constructor */
	public AbstractReportPdt(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Integer rank, String content, Integer rewardYn, Long rewardPrice,
			Timestamp compTime, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.pdtUid = pdtUid;
		this.rank = rank;
		this.content = content;
		this.rewardYn = rewardYn;
		this.rewardPrice = rewardPrice;
		this.compTime = compTime;
		this.status = status;
	}

	// Property accessors

	public Integer getReportPdtUid() {
		return this.reportPdtUid;
	}

	public void setReportPdtUid(Integer reportPdtUid) {
		this.reportPdtUid = reportPdtUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getRewardYn() {
		return this.rewardYn;
	}

	public void setRewardYn(Integer rewardYn) {
		this.rewardYn = rewardYn;
	}

	public Long getRewardPrice() {
		return this.rewardPrice;
	}

	public void setRewardPrice(Long rewardPrice) {
		this.rewardPrice = rewardPrice;
	}

	public Timestamp getCompTime() {
		return this.compTime;
	}

	public void setCompTime(Timestamp compTime) {
		this.compTime = compTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}