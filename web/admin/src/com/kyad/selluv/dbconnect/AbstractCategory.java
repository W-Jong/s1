package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractCategory entity provides the base persistence definition of the
 * Category entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractCategory implements java.io.Serializable {

	// Fields

	private Integer categoryUid;
	private Timestamp regTime;
	private Integer parentCategoryUid;
	private Integer depth;
	private String categoryName;
	private Integer categoryOrder;
	private Integer pdtCount;
	private Long totalCount;
	private Long totalPrice;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractCategory() {
	}

	/** full constructor */
	public AbstractCategory(Timestamp regTime, Integer parentCategoryUid,
			Integer depth, String categoryName, Integer categoryOrder,
			Integer pdtCount, Long totalCount, Long totalPrice, Integer status) {
		this.regTime = regTime;
		this.parentCategoryUid = parentCategoryUid;
		this.depth = depth;
		this.categoryName = categoryName;
		this.categoryOrder = categoryOrder;
		this.pdtCount = pdtCount;
		this.totalCount = totalCount;
		this.totalPrice = totalPrice;
		this.status = status;
	}

	// Property accessors

	public Integer getCategoryUid() {
		return this.categoryUid;
	}

	public void setCategoryUid(Integer categoryUid) {
		this.categoryUid = categoryUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getParentCategoryUid() {
		return this.parentCategoryUid;
	}

	public void setParentCategoryUid(Integer parentCategoryUid) {
		this.parentCategoryUid = parentCategoryUid;
	}

	public Integer getDepth() {
		return this.depth;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getCategoryOrder() {
		return this.categoryOrder;
	}

	public void setCategoryOrder(Integer categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	public Integer getPdtCount() {
		return this.pdtCount;
	}

	public void setPdtCount(Integer pdtCount) {
		this.pdtCount = pdtCount;
	}

	public Long getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}