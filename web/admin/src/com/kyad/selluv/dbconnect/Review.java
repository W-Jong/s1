package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Review entity. @author MyEclipse Persistence Tools
 */
public class Review extends AbstractReview implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Review() {
	}

	/** full constructor */
	public Review(Timestamp regTime, Integer usrUid, Integer dealUid,
			Integer type, String content, Integer point, Long rewardPrice,
			Integer status) {
		super(regTime, usrUid, dealUid, type, content, point, rewardPrice,
				status);
	}

	public static Review getInstance(int review_uid) {
		Review review = null;
		
		if(review_uid < 1)
			return review;
		
		ReviewDAO ndao = new ReviewDAO();
		try {
			review = ndao.findById(review_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return review;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		ReviewDAO ndao = new ReviewDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ReviewDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
