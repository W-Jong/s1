package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * PromotionCode entity. @author MyEclipse Persistence Tools
 */
public class PromotionCode extends AbstractPromotionCode implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public PromotionCode() {
	}

	/** full constructor */
	public PromotionCode(Timestamp regTime, Integer kind, String title,
			Long price, Integer priceUnit, String memo, Integer limitType,
			String limitUids, Timestamp startTime, Timestamp endTime,
			Integer status) {
		super(regTime, kind, title, price, priceUnit, memo, limitType,
				limitUids, startTime, endTime, status);
	}

	public static int getCount(){
		int count = 0;
		
		try{
			count = (new PromotionCodeDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static PromotionCode getInstance(int promotion_code_uid) {
		PromotionCode pcode = null;
		
		if(promotion_code_uid < 1)
			return pcode;
		
		PromotionCodeDAO ndao = new PromotionCodeDAO();
		try {
			pcode = ndao.findById(promotion_code_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return pcode;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		PromotionCodeDAO ndao = new PromotionCodeDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public int Save(){
		int res = Constant.RESULT_DB_ERR;
		
		PromotionCodeDAO ndao = new PromotionCodeDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
}
