package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrWish entity provides the base persistence definition of the
 * UsrWish entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrWish implements java.io.Serializable {

	// Fields

	private Integer usrWishUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer brandUid;
	private Integer pdtGroup;
	private Integer categoryUid;
	private String pdtModel;
	private String pdtSize;
	private String colorName;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrWish() {
	}

	/** full constructor */
	public AbstractUsrWish(Timestamp regTime, Integer usrUid, Integer brandUid,
			Integer pdtGroup, Integer categoryUid, String pdtModel,
			String pdtSize, String colorName, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.brandUid = brandUid;
		this.pdtGroup = pdtGroup;
		this.categoryUid = categoryUid;
		this.pdtModel = pdtModel;
		this.pdtSize = pdtSize;
		this.colorName = colorName;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrWishUid() {
		return this.usrWishUid;
	}

	public void setUsrWishUid(Integer usrWishUid) {
		this.usrWishUid = usrWishUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getBrandUid() {
		return this.brandUid;
	}

	public void setBrandUid(Integer brandUid) {
		this.brandUid = brandUid;
	}

	public Integer getPdtGroup() {
		return this.pdtGroup;
	}

	public void setPdtGroup(Integer pdtGroup) {
		this.pdtGroup = pdtGroup;
	}

	public Integer getCategoryUid() {
		return this.categoryUid;
	}

	public void setCategoryUid(Integer categoryUid) {
		this.categoryUid = categoryUid;
	}

	public String getPdtModel() {
		return this.pdtModel;
	}

	public void setPdtModel(String pdtModel) {
		this.pdtModel = pdtModel;
	}

	public String getPdtSize() {
		return this.pdtSize;
	}

	public void setPdtSize(String pdtSize) {
		this.pdtSize = pdtSize;
	}

	public String getColorName() {
		return this.colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}