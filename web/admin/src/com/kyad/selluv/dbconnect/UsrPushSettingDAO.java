package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrPushSetting entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrPushSetting
 * @author MyEclipse Persistence Tools
 */
public class UsrPushSettingDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrPushSettingDAO.class);
	// property constants
	public static final String DEAL_YN = "dealYn";
	public static final String NEWS_YN = "newsYn";
	public static final String REPLY_YN = "replyYn";
	public static final String DEAL_SETTING = "dealSetting";
	public static final String NEWS_SETTING = "newsSetting";
	public static final String REPLY_SETTING = "replySetting";
	public static final String STATUS = "status";

	public void save(UsrPushSetting transientInstance) {
		log.debug("saving UsrPushSetting instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrPushSetting persistentInstance) {
		log.debug("deleting UsrPushSetting instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrPushSetting findById(java.lang.Integer id) {
		log.debug("getting UsrPushSetting instance with id: " + id);
		try {
			UsrPushSetting instance = (UsrPushSetting) getSession().get(
					"com.kyad.selluv.dbconnect.UsrPushSetting", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrPushSetting instance) {
		log.debug("finding UsrPushSetting instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrPushSetting")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrPushSetting instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from UsrPushSetting as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByDealYn(Object dealYn) {
		return findByProperty(DEAL_YN, dealYn);
	}

	public List findByNewsYn(Object newsYn) {
		return findByProperty(NEWS_YN, newsYn);
	}

	public List findByReplyYn(Object replyYn) {
		return findByProperty(REPLY_YN, replyYn);
	}

	public List findByDealSetting(Object dealSetting) {
		return findByProperty(DEAL_SETTING, dealSetting);
	}

	public List findByNewsSetting(Object newsSetting) {
		return findByProperty(NEWS_SETTING, newsSetting);
	}

	public List findByReplySetting(Object replySetting) {
		return findByProperty(REPLY_SETTING, replySetting);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrPushSetting instances");
		try {
			String queryString = "from UsrPushSetting";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrPushSetting merge(UsrPushSetting detachedInstance) {
		log.debug("merging UsrPushSetting instance");
		try {
			UsrPushSetting result = (UsrPushSetting) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrPushSetting instance) {
		log.debug("attaching dirty UsrPushSetting instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrPushSetting instance) {
		log.debug("attaching clean UsrPushSetting instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}