package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractSizeRef entity provides the base persistence definition of the
 * SizeRef entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSizeRef implements java.io.Serializable {

	// Fields

	private Integer sizeRefUid;
	private Timestamp regTime;
	private Integer pdtGroup;
	private Integer category1;
	private String category2;
	private String category3;
	private String basis;
	private String typeName;
	private String sizeName;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractSizeRef() {
	}

	/** full constructor */
	public AbstractSizeRef(Timestamp regTime, Integer pdtGroup,
			Integer category1, String category2, String category3,
			String basis, String typeName, String sizeName, Integer status) {
		this.regTime = regTime;
		this.pdtGroup = pdtGroup;
		this.category1 = category1;
		this.category2 = category2;
		this.category3 = category3;
		this.basis = basis;
		this.typeName = typeName;
		this.sizeName = sizeName;
		this.status = status;
	}

	// Property accessors

	public Integer getSizeRefUid() {
		return this.sizeRefUid;
	}

	public void setSizeRefUid(Integer sizeRefUid) {
		this.sizeRefUid = sizeRefUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getPdtGroup() {
		return this.pdtGroup;
	}

	public void setPdtGroup(Integer pdtGroup) {
		this.pdtGroup = pdtGroup;
	}

	public Integer getCategory1() {
		return this.category1;
	}

	public void setCategory1(Integer category1) {
		this.category1 = category1;
	}

	public String getCategory2() {
		return this.category2;
	}

	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	public String getCategory3() {
		return this.category3;
	}

	public void setCategory3(String category3) {
		this.category3 = category3;
	}

	public String getBasis() {
		return this.basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getSizeName() {
		return this.sizeName;
	}

	public void setSizeName(String sizeName) {
		this.sizeName = sizeName;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}