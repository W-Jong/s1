package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractPdt entity provides the base persistence definition of the Pdt
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPdt implements java.io.Serializable {

	// Fields

	private Integer pdtUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer brandUid;
	private Integer pdtGroup;
	private Integer categoryUid;
	private String pdtSize;
	private String profileImg;
	private String photos;
	private Integer photoshopYn;
	private Long originPrice;
	private Long price;
	private Long sendPrice;
	private Integer pdtCondition;
	private String component;
	private String etc;
	private String colorName;
	private String pdtModel;
	private String promotionCode;
	private String content;
	private String tag;
	private String signImg;
	private Integer valetUid;
	private String memo;
	private Timestamp edtTime;
	private Integer fameScore;
	private Integer negoYn;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractPdt() {
	}

	/** full constructor */
	public AbstractPdt(Timestamp regTime, Integer usrUid, Integer brandUid,
			Integer pdtGroup, Integer categoryUid, String pdtSize,
			String profileImg, String photos, Integer photoshopYn,
			Long originPrice, Long price, Long sendPrice, Integer pdtCondition,
			String component, String etc, String colorName, String pdtModel,
			String promotionCode, String content, String tag, String signImg,
			Integer valetUid, String memo, Timestamp edtTime,
			Integer fameScore, Integer negoYn, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.brandUid = brandUid;
		this.pdtGroup = pdtGroup;
		this.categoryUid = categoryUid;
		this.pdtSize = pdtSize;
		this.profileImg = profileImg;
		this.photos = photos;
		this.photoshopYn = photoshopYn;
		this.originPrice = originPrice;
		this.price = price;
		this.sendPrice = sendPrice;
		this.pdtCondition = pdtCondition;
		this.component = component;
		this.etc = etc;
		this.colorName = colorName;
		this.pdtModel = pdtModel;
		this.promotionCode = promotionCode;
		this.content = content;
		this.tag = tag;
		this.signImg = signImg;
		this.valetUid = valetUid;
		this.memo = memo;
		this.edtTime = edtTime;
		this.fameScore = fameScore;
		this.negoYn = negoYn;
		this.status = status;
	}

	// Property accessors

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getBrandUid() {
		return this.brandUid;
	}

	public void setBrandUid(Integer brandUid) {
		this.brandUid = brandUid;
	}

	public Integer getPdtGroup() {
		return this.pdtGroup;
	}

	public void setPdtGroup(Integer pdtGroup) {
		this.pdtGroup = pdtGroup;
	}

	public Integer getCategoryUid() {
		return this.categoryUid;
	}

	public void setCategoryUid(Integer categoryUid) {
		this.categoryUid = categoryUid;
	}

	public String getPdtSize() {
		return this.pdtSize;
	}

	public void setPdtSize(String pdtSize) {
		this.pdtSize = pdtSize;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public String getPhotos() {
		return this.photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public Integer getPhotoshopYn() {
		return this.photoshopYn;
	}

	public void setPhotoshopYn(Integer photoshopYn) {
		this.photoshopYn = photoshopYn;
	}

	public Long getOriginPrice() {
		return this.originPrice;
	}

	public void setOriginPrice(Long originPrice) {
		this.originPrice = originPrice;
	}

	public Long getPrice() {
		return this.price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getSendPrice() {
		return this.sendPrice;
	}

	public void setSendPrice(Long sendPrice) {
		this.sendPrice = sendPrice;
	}

	public Integer getPdtCondition() {
		return this.pdtCondition;
	}

	public void setPdtCondition(Integer pdtCondition) {
		this.pdtCondition = pdtCondition;
	}

	public String getComponent() {
		return this.component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getEtc() {
		return this.etc;
	}

	public void setEtc(String etc) {
		this.etc = etc;
	}

	public String getColorName() {
		return this.colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	public String getPdtModel() {
		return this.pdtModel;
	}

	public void setPdtModel(String pdtModel) {
		this.pdtModel = pdtModel;
	}

	public String getPromotionCode() {
		return this.promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getSignImg() {
		return this.signImg;
	}

	public void setSignImg(String signImg) {
		this.signImg = signImg;
	}

	public Integer getValetUid() {
		return this.valetUid;
	}

	public void setValetUid(Integer valetUid) {
		this.valetUid = valetUid;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Integer getFameScore() {
		return this.fameScore;
	}

	public void setFameScore(Integer fameScore) {
		this.fameScore = fameScore;
	}
	
	public Integer getNegoYn() {
		return this.negoYn;
	}

	public void setNegoYn(Integer negoYn) {
		this.negoYn = negoYn;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}