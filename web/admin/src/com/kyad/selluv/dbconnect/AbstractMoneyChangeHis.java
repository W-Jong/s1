package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractMoneyChangeHis entity provides the base persistence definition of the
 * MoneyChangeHis entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractMoneyChangeHis implements java.io.Serializable {

	// Fields

	private Integer moneyChangeHisUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer kind;
	private Integer targetUid;
	private String pdtUid;
	private String content;
	private Long amount;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractMoneyChangeHis() {
	}

	/** full constructor */
	public AbstractMoneyChangeHis(Timestamp regTime, Integer usrUid,
			Integer kind, Integer targetUid, String pdtUid, String content,
			Long amount, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.kind = kind;
		this.targetUid = targetUid;
		this.pdtUid = pdtUid;
		this.content = content;
		this.amount = amount;
		this.status = status;
	}

	// Property accessors

	public Integer getMoneyChangeHisUid() {
		return this.moneyChangeHisUid;
	}

	public void setMoneyChangeHisUid(Integer moneyChangeHisUid) {
		this.moneyChangeHisUid = moneyChangeHisUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public Integer getTargetUid() {
		return this.targetUid;
	}

	public void setTargetUid(Integer targetUid) {
		this.targetUid = targetUid;
	}

	public String getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(String pdtUid) {
		this.pdtUid = pdtUid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getAmount() {
		return this.amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}