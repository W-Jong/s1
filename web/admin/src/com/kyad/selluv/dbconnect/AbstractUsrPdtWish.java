package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrPdtWish entity provides the base persistence definition of the
 * UsrPdtWish entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrPdtWish implements java.io.Serializable {

	// Fields

	private Integer usrPdtWishUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer pdtUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrPdtWish() {
	}

	/** full constructor */
	public AbstractUsrPdtWish(Timestamp regTime, Integer usrUid,
			Integer pdtUid, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.pdtUid = pdtUid;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrPdtWishUid() {
		return this.usrPdtWishUid;
	}

	public void setUsrPdtWishUid(Integer usrPdtWishUid) {
		this.usrPdtWishUid = usrPdtWishUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}