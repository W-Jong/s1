package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Category entity. @author MyEclipse Persistence Tools
 */
public class Category extends AbstractCategory implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Category() {
	}

	/** full constructor */
	public Category(Timestamp regTime, Integer parentCategoryUid,
			Integer depth, String name, Integer categoryOrder,
			Integer pdtCount, Long totalCount, Long totalPrice, Integer status) {
		super(regTime, parentCategoryUid, depth, name, categoryOrder, pdtCount,
				totalCount, totalPrice, status);
	}

	@SuppressWarnings("unchecked")
	public static List<Category> getCategoryListByParentCategory(int parent_category) {
		List<Category> list = null;
		
		CategoryDAO cdao = new CategoryDAO();
		try {
			list = cdao.findByParentCategoryUid(parent_category);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		return list;
	}
	
	public static Category getInstance(int category_uid) {
		Category category = null;
		
		if(category_uid < 1)
			return category;
		
		CategoryDAO ndao = new CategoryDAO();
		try {
			category = ndao.findById(category_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return category;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		CategoryDAO pdao = new CategoryDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		CategoryDAO pdao = new CategoryDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static int getChildCount(){
		int count = 0;
		
		try{
			count = (new CategoryDAO()).getChildCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
