package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrPayment entity. @author MyEclipse Persistence Tools
 */
public class UsrPayment extends AbstractUsrPayment implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrPayment() {
	}

	/** full constructor */
	public UsrPayment(Timestamp regTime, Integer usrUid, Integer kind,
			Long amount, String iamportCode, Integer status) {
		super(regTime, usrUid, kind, amount, iamportCode, status);
	}

}
