package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * ThemePdt entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.ThemePdt
 * @author MyEclipse Persistence Tools
 */
public class ThemePdtDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(ThemePdtDAO.class);
	// property constants
	public static final String THEME_UID = "themeUid";
	public static final String PDT_UID = "pdtUid";
	public static final String STATUS = "status";

	public void save(ThemePdt transientInstance) {
		log.debug("saving ThemePdt instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(ThemePdt persistentInstance) {
		log.debug("deleting ThemePdt instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ThemePdt findById(java.lang.Integer id) {
		log.debug("getting ThemePdt instance with id: " + id);
		try {
			ThemePdt instance = (ThemePdt) getSession().get(
					"com.kyad.selluv.dbconnect.ThemePdt", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(ThemePdt instance) {
		log.debug("finding ThemePdt instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.ThemePdt")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding ThemePdt instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from ThemePdt as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByThemeUid(Object themeUid) {
		return findByProperty(THEME_UID, themeUid);
	}

	public List findByPdtUid(Object pdtUid) {
		return findByProperty(PDT_UID, pdtUid);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all ThemePdt instances");
		try {
			String queryString = "from ThemePdt";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public ThemePdt merge(ThemePdt detachedInstance) {
		log.debug("merging ThemePdt instance");
		try {
			ThemePdt result = (ThemePdt) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(ThemePdt instance) {
		log.debug("attaching dirty ThemePdt instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ThemePdt instance) {
		log.debug("attaching clean ThemePdt instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}