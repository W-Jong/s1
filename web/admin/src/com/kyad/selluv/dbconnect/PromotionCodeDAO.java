package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * PromotionCode entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.PromotionCode
 * @author MyEclipse Persistence Tools
 */
public class PromotionCodeDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(PromotionCodeDAO.class);
	// property constants
	public static final String KIND = "kind";
	public static final String TITLE = "title";
	public static final String PRICE = "price";
	public static final String PRICE_UNIT = "priceUnit";
	public static final String MEMO = "memo";
	public static final String LIMIT_TYPE = "limitType";
	public static final String LIMIT_UIDS = "limitUids";
	public static final String STATUS = "status";

	public void save(PromotionCode transientInstance) {
		log.debug("saving PromotionCode instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(PromotionCode persistentInstance) {
		log.debug("deleting PromotionCode instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public PromotionCode findById(java.lang.Integer id) {
		log.debug("getting PromotionCode instance with id: " + id);
		try {
			PromotionCode instance = (PromotionCode) getSession().get(
					"com.kyad.selluv.dbconnect.PromotionCode", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(PromotionCode instance) {
		log.debug("finding PromotionCode instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.PromotionCode")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding PromotionCode instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from PromotionCode as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByTitle(Object title) {
		return findByProperty(TITLE, title);
	}

	public List findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List findByPriceUnit(Object priceUnit) {
		return findByProperty(PRICE_UNIT, priceUnit);
	}

	public List findByMemo(Object memo) {
		return findByProperty(MEMO, memo);
	}

	public List findByLimitType(Object limitType) {
		return findByProperty(LIMIT_TYPE, limitType);
	}

	public List findByLimitUids(Object limitUids) {
		return findByProperty(LIMIT_UIDS, limitUids);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all PromotionCode instances");
		try {
			String queryString = "from PromotionCode";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public PromotionCode merge(PromotionCode detachedInstance) {
		log.debug("merging PromotionCode instance");
		try {
			PromotionCode result = (PromotionCode) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(PromotionCode instance) {
		log.debug("attaching dirty PromotionCode instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(PromotionCode instance) {
		log.debug("attaching clean PromotionCode instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.PromotionCode")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}