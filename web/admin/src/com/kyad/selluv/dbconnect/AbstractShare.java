package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractShare entity provides the base persistence definition of the Share
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractShare implements java.io.Serializable {

	// Fields

	private Integer shareUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer dealUid;
	private Integer instagramYn;
	private Integer twitterYn;
	private Integer naverblogYn;
	private Integer kakaostoryYn;
	private Integer facebookYn;
	private String instagramPhotos;
	private String twitterPhotos;
	private String naverblogPhotos;
	private String kakaostoryPhotos;
	private String facebookPhotos;
	private Long rewardPrice;
	private Timestamp compTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractShare() {
	}

	/** minimal constructor */
	public AbstractShare(Timestamp regTime, Integer usrUid, Integer dealUid,
			Integer instagramYn, Integer twitterYn, Integer naverblogYn,
			Integer kakaostoryYn, Integer facebookYn, String instagramPhotos,
			String twitterPhotos, String naverblogPhotos,
			String kakaostoryPhotos, String facebookPhotos, Long rewardPrice,
			Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.dealUid = dealUid;
		this.instagramYn = instagramYn;
		this.twitterYn = twitterYn;
		this.naverblogYn = naverblogYn;
		this.kakaostoryYn = kakaostoryYn;
		this.facebookYn = facebookYn;
		this.instagramPhotos = instagramPhotos;
		this.twitterPhotos = twitterPhotos;
		this.naverblogPhotos = naverblogPhotos;
		this.kakaostoryPhotos = kakaostoryPhotos;
		this.facebookPhotos = facebookPhotos;
		this.rewardPrice = rewardPrice;
		this.status = status;
	}

	/** full constructor */
	public AbstractShare(Timestamp regTime, Integer usrUid, Integer dealUid,
			Integer instagramYn, Integer twitterYn, Integer naverblogYn,
			Integer kakaostoryYn, Integer facebookYn, String instagramPhotos,
			String twitterPhotos, String naverblogPhotos,
			String kakaostoryPhotos, String facebookPhotos, Long rewardPrice,
			Timestamp compTime, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.dealUid = dealUid;
		this.instagramYn = instagramYn;
		this.twitterYn = twitterYn;
		this.naverblogYn = naverblogYn;
		this.kakaostoryYn = kakaostoryYn;
		this.facebookYn = facebookYn;
		this.instagramPhotos = instagramPhotos;
		this.twitterPhotos = twitterPhotos;
		this.naverblogPhotos = naverblogPhotos;
		this.kakaostoryPhotos = kakaostoryPhotos;
		this.facebookPhotos = facebookPhotos;
		this.rewardPrice = rewardPrice;
		this.compTime = compTime;
		this.status = status;
	}

	// Property accessors

	public Integer getShareUid() {
		return this.shareUid;
	}

	public void setShareUid(Integer shareUid) {
		this.shareUid = shareUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getDealUid() {
		return this.dealUid;
	}

	public void setDealUid(Integer dealUid) {
		this.dealUid = dealUid;
	}

	public Integer getInstagramYn() {
		return this.instagramYn;
	}

	public void setInstagramYn(Integer instagramYn) {
		this.instagramYn = instagramYn;
	}

	public Integer getTwitterYn() {
		return this.twitterYn;
	}

	public void setTwitterYn(Integer twitterYn) {
		this.twitterYn = twitterYn;
	}

	public Integer getNaverblogYn() {
		return this.naverblogYn;
	}

	public void setNaverblogYn(Integer naverblogYn) {
		this.naverblogYn = naverblogYn;
	}

	public Integer getKakaostoryYn() {
		return this.kakaostoryYn;
	}

	public void setKakaostoryYn(Integer kakaostoryYn) {
		this.kakaostoryYn = kakaostoryYn;
	}

	public Integer getFacebookYn() {
		return this.facebookYn;
	}

	public void setFacebookYn(Integer facebookYn) {
		this.facebookYn = facebookYn;
	}

	public String getInstagramPhotos() {
		return this.instagramPhotos;
	}

	public void setInstagramPhotos(String instagramPhotos) {
		this.instagramPhotos = instagramPhotos;
	}

	public String getTwitterPhotos() {
		return this.twitterPhotos;
	}

	public void setTwitterPhotos(String twitterPhotos) {
		this.twitterPhotos = twitterPhotos;
	}

	public String getNaverblogPhotos() {
		return this.naverblogPhotos;
	}

	public void setNaverblogPhotos(String naverblogPhotos) {
		this.naverblogPhotos = naverblogPhotos;
	}

	public String getKakaostoryPhotos() {
		return this.kakaostoryPhotos;
	}

	public void setKakaostoryPhotos(String kakaostoryPhotos) {
		this.kakaostoryPhotos = kakaostoryPhotos;
	}

	public String getFacebookPhotos() {
		return this.facebookPhotos;
	}

	public void setFacebookPhotos(String facebookPhotos) {
		this.facebookPhotos = facebookPhotos;
	}

	public Long getRewardPrice() {
		return this.rewardPrice;
	}

	public void setRewardPrice(Long rewardPrice) {
		this.rewardPrice = rewardPrice;
	}

	public Timestamp getCompTime() {
		return this.compTime;
	}

	public void setCompTime(Timestamp compTime) {
		this.compTime = compTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}