package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

/**
 * UsrSns entity. @author MyEclipse Persistence Tools
 */
public class UsrSns extends AbstractUsrSns implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrSns() {
	}

	/** full constructor */
	public UsrSns(Timestamp regTime, Integer usrUid, Integer snsType,
			String snsId, String snsEmail, String snsToken, Integer status) {
		super(regTime, usrUid, snsType, snsId, snsEmail, snsToken, status);
	}

	@SuppressWarnings("unchecked")
	public static String getSnsTypeStrBySnsId(String snsId){
		List<UsrSns> list = null;
		
		UsrSnsDAO cdao = new UsrSnsDAO();
		try {
			list = cdao.findBySnsId(snsId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		if (list == null || list.size() < 1)
			return "";
		Integer type = list.get(0).getSnsType();
		if (type == 1) {
			return "페이스북";
		} else if (type == 2) {
			return "네이버";
		} else if (type == 3) {
			return "카카오톡";
		} else if (type == 3) {
			return "인스타그램";
		} else {
			return "트위터";
		}
	}
}
