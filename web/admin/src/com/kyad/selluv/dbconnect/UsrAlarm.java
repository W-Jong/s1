package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrAlarm entity. @author MyEclipse Persistence Tools
 */
public class UsrAlarm extends AbstractUsrAlarm implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrAlarm() {
	}

	/** full constructor */
	public UsrAlarm(Timestamp regTime, Integer usrUid, Integer kind,
			Integer subKind, Integer targetUid, String content, Integer pdtUid,
			String profileImg, Integer status) {
		super(regTime, usrUid, kind, subKind, targetUid, content, pdtUid,
				profileImg, status);
	}

}
