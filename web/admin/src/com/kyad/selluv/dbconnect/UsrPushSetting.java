package com.kyad.selluv.dbconnect;

import com.kyad.selluv.common.Constant;
import org.hibernate.Transaction;

import java.sql.Timestamp;

/**
 * UsrPushSetting entity. @author MyEclipse Persistence Tools
 */
public class UsrPushSetting extends AbstractUsrPushSetting implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrPushSetting() {
	}

	/** minimal constructor */
	public UsrPushSetting(Integer usrUid, Timestamp regTime, Integer dealYn,
			Integer newsYn, Integer replyYn, Integer status) {
		super(usrUid, regTime, dealYn, newsYn, replyYn, status);
	}

	/** full constructor */
	public UsrPushSetting(Integer usrUid, Timestamp regTime, Integer dealYn,
			Integer newsYn, Integer replyYn, String dealSetting,
			String newsSetting, String replySetting, Integer status) {
		super(usrUid, regTime, dealYn, newsYn, replyYn, dealSetting,
				newsSetting, replySetting, status);
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;

		UsrPushSettingDAO pdao = new UsrPushSettingDAO();
		Transaction tx = pdao.getSession().beginTransaction();

		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();

		return res;
	}

}
