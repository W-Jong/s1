package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrLike entity. @author MyEclipse Persistence Tools
 */
public class UsrLike extends AbstractUsrLike implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrLike() {
	}

	/** full constructor */
	public UsrLike(Timestamp regTime, Integer usrUid, Integer peerUsrUid,
			Integer status) {
		super(regTime, usrUid, peerUsrUid, status);
	}

}
