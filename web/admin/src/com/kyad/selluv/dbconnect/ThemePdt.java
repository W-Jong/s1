package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * ThemePdt entity. @author MyEclipse Persistence Tools
 */
public class ThemePdt extends AbstractThemePdt implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public ThemePdt() {
	}

	/** full constructor */
	public ThemePdt(Timestamp regTime, Integer themeUid, Integer pdtUid,
			Integer status) {
		super(regTime, themeUid, pdtUid, status);
	}

	@SuppressWarnings("unchecked")
	public static ThemePdt getInstanceByThemeAndPdt(int theme_uid, int pdt_uid) {
		List<ThemePdt> list = null;

		ThemePdtDAO kdao = new ThemePdtDAO();
		ThemePdt exam = new ThemePdt();
		exam.setThemeUid(theme_uid);
		exam.setPdtUid(pdt_uid);
		
		
		try {
			list = kdao.findByExample(exam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		kdao.getSession().clear();
		kdao.getSession().close();		
		if (list != null && list.size() != 0)
			return list.get(0);
		return null;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		ThemePdtDAO pdao = new ThemePdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		ThemePdtDAO pdao = new ThemePdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public int Delete() {
		int res = Constant.RESULT_DB_ERR;
		
		ThemePdtDAO pdao = new ThemePdtDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.delete(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static ThemePdt getInstance(int theme_pdt_uid) {
		ThemePdt pdt = null;
		
		if(theme_pdt_uid < 1)
			return pdt;
		
		ThemePdtDAO ndao = new ThemePdtDAO();
		try {
			pdt = ndao.findById(theme_pdt_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return pdt;
	}
}
