package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrCashback entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrCashback
 * @author MyEclipse Persistence Tools
 */
public class UsrCashbackDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrCashbackDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String KIND = "kind";
	public static final String AMOUNT = "amount";
	public static final String STATUS = "status";

	public void save(UsrCashback transientInstance) {
		log.debug("saving UsrCashback instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrCashback persistentInstance) {
		log.debug("deleting UsrCashback instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrCashback findById(java.lang.Integer id) {
		log.debug("getting UsrCashback instance with id: " + id);
		try {
			UsrCashback instance = (UsrCashback) getSession().get(
					"com.kyad.selluv.dbconnect.UsrCashback", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrCashback instance) {
		log.debug("finding UsrCashback instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrCashback")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrCashback instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrCashback as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByAmount(Object amount) {
		return findByProperty(AMOUNT, amount);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrCashback instances");
		try {
			String queryString = "from UsrCashback";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrCashback merge(UsrCashback detachedInstance) {
		log.debug("merging UsrCashback instance");
		try {
			UsrCashback result = (UsrCashback) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrCashback instance) {
		log.debug("attaching dirty UsrCashback instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrCashback instance) {
		log.debug("attaching clean UsrCashback instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}