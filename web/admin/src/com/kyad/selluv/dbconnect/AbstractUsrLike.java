package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrLike entity provides the base persistence definition of the
 * UsrLike entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrLike implements java.io.Serializable {

	// Fields

	private Integer usrLikeUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer peerUsrUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrLike() {
	}

	/** full constructor */
	public AbstractUsrLike(Timestamp regTime, Integer usrUid,
			Integer peerUsrUid, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.peerUsrUid = peerUsrUid;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrLikeUid() {
		return this.usrLikeUid;
	}

	public void setUsrLikeUid(Integer usrLikeUid) {
		this.usrLikeUid = usrLikeUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPeerUsrUid() {
		return this.peerUsrUid;
	}

	public void setPeerUsrUid(Integer peerUsrUid) {
		this.peerUsrUid = peerUsrUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}