package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractValet entity provides the base persistence definition of the Valet
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractValet implements java.io.Serializable {

	// Fields

	private Integer valetUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer pdtUid;
	private Long reqPrice;
	private Long earnPrice;
	private String reqName;
	private String reqPhone;
	private String reqAddress;
	private String reqAddressDetail;
	private Integer sendType;
	private Integer receiveYn;
	private String signImg;
	private String memo;
	private Timestamp edtTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractValet() {
	}

	/** full constructor */
	public AbstractValet(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Long reqPrice, Long earnPrice, String reqName, String reqPhone,
			String reqAddress, String reqAddressDetail, Integer sendType,
			Integer receiveYn, String signImg, String memo, Timestamp edtTime,
			Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.pdtUid = pdtUid;
		this.reqPrice = reqPrice;
		this.earnPrice = earnPrice;
		this.reqName = reqName;
		this.reqPhone = reqPhone;
		this.reqAddress = reqAddress;
		this.reqAddressDetail = reqAddressDetail;
		this.sendType = sendType;
		this.receiveYn = receiveYn;
		this.signImg = signImg;
		this.memo = memo;
		this.edtTime = edtTime;
		this.status = status;
	}

	// Property accessors

	public Integer getValetUid() {
		return this.valetUid;
	}

	public void setValetUid(Integer valetUid) {
		this.valetUid = valetUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Long getReqPrice() {
		return this.reqPrice;
	}

	public void setReqPrice(Long reqPrice) {
		this.reqPrice = reqPrice;
	}

	public Long getEarnPrice() {
		return this.earnPrice;
	}

	public void setEarnPrice(Long earnPrice) {
		this.earnPrice = earnPrice;
	}

	public String getReqName() {
		return this.reqName;
	}

	public void setReqName(String reqName) {
		this.reqName = reqName;
	}

	public String getReqPhone() {
		return this.reqPhone;
	}

	public void setReqPhone(String reqPhone) {
		this.reqPhone = reqPhone;
	}

	public String getReqAddress() {
		return this.reqAddress;
	}

	public void setReqAddress(String reqAddress) {
		this.reqAddress = reqAddress;
	}

	public String getReqAddressDetail() {
		return this.reqAddressDetail;
	}

	public void setReqAddressDetail(String reqAddressDetail) {
		this.reqAddressDetail = reqAddressDetail;
	}

	public Integer getSendType() {
		return this.sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	public Integer getReceiveYn() {
		return this.receiveYn;
	}

	public void setReceiveYn(Integer receiveYn) {
		this.receiveYn = receiveYn;
	}

	public String getSignImg() {
		return this.signImg;
	}

	public void setSignImg(String signImg) {
		this.signImg = signImg;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}