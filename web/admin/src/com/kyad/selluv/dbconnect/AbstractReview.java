package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractReview entity provides the base persistence definition of the Review
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractReview implements java.io.Serializable {

	// Fields

	private Integer reviewUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer dealUid;
	private Integer type;
	private String content;
	private Integer point;
	private Long rewardPrice;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractReview() {
	}

	/** full constructor */
	public AbstractReview(Timestamp regTime, Integer usrUid, Integer dealUid,
			Integer type, String content, Integer point, Long rewardPrice,
			Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.dealUid = dealUid;
		this.type = type;
		this.content = content;
		this.point = point;
		this.rewardPrice = rewardPrice;
		this.status = status;
	}

	// Property accessors

	public Integer getReviewUid() {
		return this.reviewUid;
	}

	public void setReviewUid(Integer reviewUid) {
		this.reviewUid = reviewUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getDealUid() {
		return this.dealUid;
	}

	public void setDealUid(Integer dealUid) {
		this.dealUid = dealUid;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getPoint() {
		return this.point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Long getRewardPrice() {
		return this.rewardPrice;
	}

	public void setRewardPrice(Long rewardPrice) {
		this.rewardPrice = rewardPrice;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}