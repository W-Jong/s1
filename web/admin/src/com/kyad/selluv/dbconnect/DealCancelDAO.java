package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * DealCancel entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.DealCancel
 * @author MyEclipse Persistence Tools
 */
public class DealCancelDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(DealCancelDAO.class);
	// property constants
	public static final String DEAL_UID = "dealUid";
	public static final String REASON = "reason";
	public static final String PHOTOS = "photos";
	public static final String STATUS = "status";

	public void save(DealCancel transientInstance) {
		log.debug("saving DealCancel instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(DealCancel persistentInstance) {
		log.debug("deleting DealCancel instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public DealCancel findById(java.lang.Integer id) {
		log.debug("getting DealCancel instance with id: " + id);
		try {
			DealCancel instance = (DealCancel) getSession().get(
					"com.kyad.selluv.dbconnect.DealCancel", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(DealCancel instance) {
		log.debug("finding DealCancel instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.DealCancel")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding DealCancel instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from DealCancel as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByDealUid(Object dealUid) {
		return findByProperty(DEAL_UID, dealUid);
	}

	public List findByReason(Object reason) {
		return findByProperty(REASON, reason);
	}

	public List findByPhotos(Object photos) {
		return findByProperty(PHOTOS, photos);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all DealCancel instances");
		try {
			String queryString = "from DealCancel";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public DealCancel merge(DealCancel detachedInstance) {
		log.debug("merging DealCancel instance");
		try {
			DealCancel result = (DealCancel) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(DealCancel instance) {
		log.debug("attaching dirty DealCancel instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(DealCancel instance) {
		log.debug("attaching clean DealCancel instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}