package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

/**
 * SizeRef entity. @author MyEclipse Persistence Tools
 */
public class SizeRef extends AbstractSizeRef implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public SizeRef() {
	}

	/** full constructor */
	public SizeRef(Timestamp regTime, Integer pdtGroup, Integer category1,
			String category2, String category3, String basis, String typeName,
			String sizeName, Integer status) {
		super(regTime, pdtGroup, category1, category2, category3, basis,
				typeName, sizeName, status);
	}

	@SuppressWarnings("unchecked")
	public static List<SizeRef> getCategory2ListByParent(int parent_category) {
		List<SizeRef> list = null;
		
		SizeRefDAO cdao = new SizeRefDAO();
		try {
			list = cdao.getCategory2ListByParent(parent_category);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public static List<SizeRef> getCategory3ListByParent(String parent_category) {
		List<SizeRef> list = null;
		
		SizeRefDAO cdao = new SizeRefDAO();
		try {
			list = cdao.getCategory3ListByParent(parent_category);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		return list;
	}
}
