package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;

/**
 * AbstractStatisticsCache entity provides the base persistence definition of
 * the StatisticsCache entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractStatisticsCache implements java.io.Serializable {

	// Fields

	private Integer statisticsCacheUid;
	private Timestamp regTime;
	private Date targetDate;
	private Long dealCount;
	private Long dealAmount;
	private Long joinCount;
	private Long visitUserCount;
	private Long visitCount;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractStatisticsCache() {
	}

	/** full constructor */
	public AbstractStatisticsCache(Timestamp regTime, Date targetDate,
			Long dealCount, Long dealAmount, Long joinCount,
			Long visitUserCount, Long visitCount, Integer status) {
		this.regTime = regTime;
		this.targetDate = targetDate;
		this.dealCount = dealCount;
		this.dealAmount = dealAmount;
		this.joinCount = joinCount;
		this.visitUserCount = visitUserCount;
		this.visitCount = visitCount;
		this.status = status;
	}

	// Property accessors

	public Integer getStatisticsCacheUid() {
		return this.statisticsCacheUid;
	}

	public void setStatisticsCacheUid(Integer statisticsCacheUid) {
		this.statisticsCacheUid = statisticsCacheUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Date getTargetDate() {
		return this.targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public Long getDealCount() {
		return this.dealCount;
	}

	public void setDealCount(Long dealCount) {
		this.dealCount = dealCount;
	}

	public Long getDealAmount() {
		return this.dealAmount;
	}

	public void setDealAmount(Long dealAmount) {
		this.dealAmount = dealAmount;
	}

	public Long getJoinCount() {
		return this.joinCount;
	}

	public void setJoinCount(Long joinCount) {
		this.joinCount = joinCount;
	}

	public Long getVisitUserCount() {
		return this.visitUserCount;
	}

	public void setVisitUserCount(Long visitUserCount) {
		this.visitUserCount = visitUserCount;
	}

	public Long getVisitCount() {
		return this.visitCount;
	}

	public void setVisitCount(Long visitCount) {
		this.visitCount = visitCount;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}