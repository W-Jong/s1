package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Banner entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Banner
 * @author MyEclipse Persistence Tools
 */
public class BannerDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(BannerDAO.class);
	// property constants
	public static final String TYPE = "type";
	public static final String TARGET_UID = "targetUid";
	public static final String TITLE = "title";
	public static final String PROFILE_IMG = "profileImg";
	public static final String STATUS = "status";

	public void save(Banner transientInstance) {
		log.debug("saving Banner instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Banner persistentInstance) {
		log.debug("deleting Banner instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Banner findById(java.lang.Integer id) {
		log.debug("getting Banner instance with id: " + id);
		try {
			Banner instance = (Banner) getSession().get(
					"com.kyad.selluv.dbconnect.Banner", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Banner instance) {
		log.debug("finding Banner instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Banner")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Banner instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Banner as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List findByTargetUid(Object targetUid) {
		return findByProperty(TARGET_UID, targetUid);
	}

	public List findByTitle(Object title) {
		return findByProperty(TITLE, title);
	}

	public List findByProfileImg(Object profileImg) {
		return findByProperty(PROFILE_IMG, profileImg);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Banner instances");
		try {
			String queryString = "from Banner";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Banner merge(Banner detachedInstance) {
		log.debug("merging Banner instance");
		try {
			Banner result = (Banner) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Banner instance) {
		log.debug("attaching dirty Banner instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Banner instance) {
		log.debug("attaching clean Banner instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Banner")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}