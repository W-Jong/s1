package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;

/**
 * StatisticsCache entity. @author MyEclipse Persistence Tools
 */
public class StatisticsCache extends AbstractStatisticsCache implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public StatisticsCache() {
	}

	/** full constructor */
	public StatisticsCache(Timestamp regTime, Date targetDate, Long dealCount,
			Long dealAmount, Long joinCount, Long visitUserCount,
			Long visitCount, Integer status) {
		super(regTime, targetDate, dealCount, dealAmount, joinCount,
				visitUserCount, visitCount, status);
	}

}
