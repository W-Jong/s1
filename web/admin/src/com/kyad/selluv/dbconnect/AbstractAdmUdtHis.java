package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractAdmUdtHis entity provides the base persistence definition of the
 * AdmUdtHis entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractAdmUdtHis implements java.io.Serializable {

	// Fields

	private Integer admUdtHisUid;
	private Timestamp regTime;
	private Integer admUid;
	private Integer kind;
	private String content;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractAdmUdtHis() {
	}

	/** full constructor */
	public AbstractAdmUdtHis(Timestamp regTime, Integer admUid, Integer kind,
			String content, Integer status) {
		this.regTime = regTime;
		this.admUid = admUid;
		this.kind = kind;
		this.content = content;
		this.status = status;
	}

	// Property accessors

	public Integer getAdmUdtHisUid() {
		return this.admUdtHisUid;
	}

	public void setAdmUdtHisUid(Integer admUdtHisUid) {
		this.admUdtHisUid = admUdtHisUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getAdmUid() {
		return this.admUid;
	}

	public void setAdmUid(Integer admUid) {
		this.admUid = admUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}