package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * RecomBrand entity. @author MyEclipse Persistence Tools
 */
public class RecomBrand extends AbstractRecomBrand implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public RecomBrand() {
	}

	/** full constructor */
	public RecomBrand(Timestamp regTime, Integer pdtGroup, Integer brandOrder,
			Integer brandUid, Integer status) {
		super(regTime, pdtGroup, brandOrder, brandUid, status);
	}

	@SuppressWarnings("unchecked")
	public static List<RecomBrand> getListByPdtGroup(int pdt_group) {
		List<RecomBrand> list = null;
		
		RecomBrandDAO ndao = new RecomBrandDAO();
		RecomBrand instance = new RecomBrand();
		if (pdt_group > 0)
			instance.setPdtGroup(pdt_group);
		instance.setStatus(Constant.STATUS_USE);
		try {
			list = ndao.findByExample(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return list;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		RecomBrandDAO pdao = new RecomBrandDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		RecomBrandDAO pdao = new RecomBrandDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	@SuppressWarnings("unchecked")
	public static RecomBrand getInstanceByPdtGroupAndBrand(int pdt_group, int brand_uid) {
		List<RecomBrand> list = null;
		
		RecomBrandDAO ndao = new RecomBrandDAO();
		RecomBrand instance = new RecomBrand();
		instance.setBrandUid(brand_uid);
		instance.setPdtGroup(pdt_group);
		try {
			list = ndao.findByExample(instance);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		if (list.size() > 0)
			return list.get(0);
		return null;
	}
}
