package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Color
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Color
 * @author MyEclipse Persistence Tools
 */
public class ColorDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(ColorDAO.class);
	// property constants
	public static final String COLOR_NAME = "colorName";
	public static final String COLOR_CODE = "colorCode";
	public static final String STATUS = "status";

	public void save(Color transientInstance) {
		log.debug("saving Color instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Color persistentInstance) {
		log.debug("deleting Color instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Color findById(java.lang.Integer id) {
		log.debug("getting Color instance with id: " + id);
		try {
			Color instance = (Color) getSession().get(
					"com.kyad.selluv.dbconnect.Color", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Color instance) {
		log.debug("finding Color instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Color")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Color instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Color as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByColorName(Object colorName) {
		return findByProperty(COLOR_NAME, colorName);
	}

	public List findByColorCode(Object colorCode) {
		return findByProperty(COLOR_CODE, colorCode);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Color instances");
		try {
			String queryString = "from Color";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Color merge(Color detachedInstance) {
		log.debug("merging Color instance");
		try {
			Color result = (Color) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Color instance) {
		log.debug("attaching dirty Color instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Color instance) {
		log.debug("attaching clean Color instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}