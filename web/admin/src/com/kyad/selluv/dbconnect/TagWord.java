package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * TagWord entity. @author MyEclipse Persistence Tools
 */
public class TagWord extends AbstractTagWord implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public TagWord() {
	}

	/** full constructor */
	public TagWord(Timestamp regTime, String content, Integer pdtCount,
			Timestamp edtTime, Integer status) {
		super(regTime, content, pdtCount, edtTime, status);
	}

}
