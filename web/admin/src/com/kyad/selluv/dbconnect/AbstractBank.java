package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractBank entity provides the base persistence definition of the Bank
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractBank implements java.io.Serializable {

	// Fields

	private Integer bankUid;
	private Timestamp regTime;
	private String bankNm;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractBank() {
	}

	/** full constructor */
	public AbstractBank(Timestamp regTime, String bankNm, Integer status) {
		this.regTime = regTime;
		this.bankNm = bankNm;
		this.status = status;
	}

	// Property accessors

	public Integer getBankUid() {
		return this.bankUid;
	}

	public void setBankUid(Integer bankUid) {
		this.bankUid = bankUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getBankNm() {
		return this.bankNm;
	}

	public void setBankNm(String bankNm) {
		this.bankNm = bankNm;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}