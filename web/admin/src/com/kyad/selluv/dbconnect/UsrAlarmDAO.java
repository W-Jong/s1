package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrAlarm entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrAlarm
 * @author MyEclipse Persistence Tools
 */
public class UsrAlarmDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrAlarmDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String KIND = "kind";
	public static final String SUB_KIND = "subKind";
	public static final String TARGET_UID = "targetUid";
	public static final String CONTENT = "content";
	public static final String PDT_UID = "pdtUid";
	public static final String PROFILE_IMG = "profileImg";
	public static final String STATUS = "status";

	public void save(UsrAlarm transientInstance) {
		log.debug("saving UsrAlarm instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrAlarm persistentInstance) {
		log.debug("deleting UsrAlarm instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrAlarm findById(java.lang.Integer id) {
		log.debug("getting UsrAlarm instance with id: " + id);
		try {
			UsrAlarm instance = (UsrAlarm) getSession().get(
					"com.kyad.selluv.dbconnect.UsrAlarm", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrAlarm instance) {
		log.debug("finding UsrAlarm instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrAlarm")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrAlarm instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrAlarm as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findBySubKind(Object subKind) {
		return findByProperty(SUB_KIND, subKind);
	}

	public List findByTargetUid(Object targetUid) {
		return findByProperty(TARGET_UID, targetUid);
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByPdtUid(Object pdtUid) {
		return findByProperty(PDT_UID, pdtUid);
	}

	public List findByProfileImg(Object profileImg) {
		return findByProperty(PROFILE_IMG, profileImg);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrAlarm instances");
		try {
			String queryString = "from UsrAlarm";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrAlarm merge(UsrAlarm detachedInstance) {
		log.debug("merging UsrAlarm instance");
		try {
			UsrAlarm result = (UsrAlarm) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrAlarm instance) {
		log.debug("attaching dirty UsrAlarm instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrAlarm instance) {
		log.debug("attaching clean UsrAlarm instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}