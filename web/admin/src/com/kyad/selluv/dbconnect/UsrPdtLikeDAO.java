package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrPdtLike entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrPdtLike
 * @author MyEclipse Persistence Tools
 */
public class UsrPdtLikeDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsrPdtLikeDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String PDT_UID = "pdtUid";
	public static final String STATUS = "status";

	public void save(UsrPdtLike transientInstance) {
		log.debug("saving UsrPdtLike instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrPdtLike persistentInstance) {
		log.debug("deleting UsrPdtLike instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrPdtLike findById(java.lang.Integer id) {
		log.debug("getting UsrPdtLike instance with id: " + id);
		try {
			UsrPdtLike instance = (UsrPdtLike) getSession().get(
					"com.kyad.selluv.dbconnect.UsrPdtLike", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrPdtLike instance) {
		log.debug("finding UsrPdtLike instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrPdtLike")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrPdtLike instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrPdtLike as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByPdtUid(Object pdtUid) {
		return findByProperty(PDT_UID, pdtUid);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrPdtLike instances");
		try {
			String queryString = "from UsrPdtLike";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrPdtLike merge(UsrPdtLike detachedInstance) {
		log.debug("merging UsrPdtLike instance");
		try {
			UsrPdtLike result = (UsrPdtLike) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrPdtLike instance) {
		log.debug("attaching dirty UsrPdtLike instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrPdtLike instance) {
		log.debug("attaching clean UsrPdtLike instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}