package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kyad.selluv.common.Constant;

/**
 * A data access object (DAO) providing persistence and search support for Brand
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Brand
 * @author MyEclipse Persistence Tools
 */
public class BrandDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(BrandDAO.class);
	// property constants
	public static final String FIRST_EN = "firstEn";
	public static final String FIRST_KO = "firstKo";
	public static final String NAME_EN = "nameEn";
	public static final String NAME_KO = "nameKo";
	public static final String LICENSE_URL = "licenseUrl";
	public static final String LOGO_IMG = "logoImg";
	public static final String PROFILE_IMG = "profileImg";
	public static final String BACK_IMG = "backImg";
	public static final String USR_UID = "usrUid";
	public static final String PDT_COUNT = "pdtCount";
	public static final String TOTAL_COUNT = "totalCount";
	public static final String TOTAL_PRICE = "totalPrice";
	public static final String STATUS = "status";

	public void save(Brand transientInstance) {
		log.debug("saving Brand instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Brand persistentInstance) {
		log.debug("deleting Brand instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Brand findById(java.lang.Integer id) {
		log.debug("getting Brand instance with id: " + id);
		try {
			Brand instance = (Brand) getSession().get(
					"com.kyad.selluv.dbconnect.Brand", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Brand instance) {
		log.debug("finding Brand instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Brand")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Brand instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Brand as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByFirstEn(Object firstEn) {
		return findByProperty(FIRST_EN, firstEn);
	}

	public List findByFirstKo(Object firstKo) {
		return findByProperty(FIRST_KO, firstKo);
	}

	public List findByNameEn(Object nameEn) {
		return findByProperty(NAME_EN, nameEn);
	}

	public List findByNameKo(Object nameKo) {
		return findByProperty(NAME_KO, nameKo);
	}

	public List findByLicenseUrl(Object licenseUrl) {
		return findByProperty(LICENSE_URL, licenseUrl);
	}

	public List findByLogoImg(Object logoImg) {
		return findByProperty(LOGO_IMG, logoImg);
	}

	public List findByProfileImg(Object profileImg) {
		return findByProperty(PROFILE_IMG, profileImg);
	}

	public List findByBackImg(Object backImg) {
		return findByProperty(BACK_IMG, backImg);
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByPdtCount(Object pdtCount) {
		return findByProperty(PDT_COUNT, pdtCount);
	}

	public List findByTotalCount(Object totalCount) {
		return findByProperty(TOTAL_COUNT, totalCount);
	}

	public List findByTotalPrice(Object totalPrice) {
		return findByProperty(TOTAL_PRICE, totalPrice);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Brand instances");
		try {
			String queryString = "from Brand";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Brand merge(Brand detachedInstance) {
		log.debug("merging Brand instance");
		try {
			Brand result = (Brand) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Brand instance) {
		log.debug("attaching dirty Brand instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Brand instance) {
		log.debug("attaching clean Brand instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount(int status) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Brand")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq(STATUS, status));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}