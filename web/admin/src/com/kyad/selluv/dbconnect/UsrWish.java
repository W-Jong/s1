package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * UsrWish entity. @author MyEclipse Persistence Tools
 */
public class UsrWish extends AbstractUsrWish implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrWish() {
	}

	/** full constructor */
	public UsrWish(Timestamp regTime, Integer usrUid, Integer brandUid,
			Integer pdtGroup, Integer categoryUid, String pdtModel,
			String pdtSize, String colorName, Integer status) {
		super(regTime, usrUid, brandUid, pdtGroup, categoryUid, pdtModel,
				pdtSize, colorName, status);
	}
}
