package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * SystemSetting entity. @author MyEclipse Persistence Tools
 */
public class SystemSetting extends AbstractSystemSetting implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public SystemSetting() {
	}

	/** full constructor */
	public SystemSetting(Integer systemSettingUid, Timestamp regTime,
			String setting, String settingExtra1, String settingExtra2,
			String memo, Integer status) {
		super(systemSettingUid, regTime, setting, settingExtra1, settingExtra2,
				memo, status);
	}
	public static SystemSetting getInstance(int inst_uid) {
		SystemSetting inst = null;
		
		if(inst_uid < 1)
			return inst;
		
		SystemSettingDAO ndao = new SystemSettingDAO();
		try {
			inst = ndao.findById(inst_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return inst;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		SystemSettingDAO pdao = new SystemSettingDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		SystemSettingDAO pdao = new SystemSettingDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
}
