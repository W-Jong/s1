package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Review entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Review
 * @author MyEclipse Persistence Tools
 */
public class ReviewDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(ReviewDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String DEAL_UID = "dealUid";
	public static final String TYPE = "type";
	public static final String CONTENT = "content";
	public static final String POINT = "point";
	public static final String REWARD_PRICE = "rewardPrice";
	public static final String STATUS = "status";

	public void save(Review transientInstance) {
		log.debug("saving Review instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Review persistentInstance) {
		log.debug("deleting Review instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Review findById(java.lang.Integer id) {
		log.debug("getting Review instance with id: " + id);
		try {
			Review instance = (Review) getSession().get(
					"com.kyad.selluv.dbconnect.Review", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Review instance) {
		log.debug("finding Review instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Review")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Review instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Review as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByDealUid(Object dealUid) {
		return findByProperty(DEAL_UID, dealUid);
	}

	public List findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByPoint(Object point) {
		return findByProperty(POINT, point);
	}

	public List findByRewardPrice(Object rewardPrice) {
		return findByProperty(REWARD_PRICE, rewardPrice);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Review instances");
		try {
			String queryString = "from Review";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Review merge(Review detachedInstance) {
		log.debug("merging Review instance");
		try {
			Review result = (Review) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Review instance) {
		log.debug("attaching dirty Review instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Review instance) {
		log.debug("attaching clean Review instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Review")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}