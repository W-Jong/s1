package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractPdtStyle entity provides the base persistence definition of the
 * PdtStyle entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPdtStyle implements java.io.Serializable {

	// Fields

	private Integer pdtStyleUid;
	private Timestamp regTime;
	private Integer pdtUid;
	private Integer usrUid;
	private String styleImg;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractPdtStyle() {
	}

	/** full constructor */
	public AbstractPdtStyle(Timestamp regTime, Integer pdtUid, Integer usrUid,
			String styleImg, Integer status) {
		this.regTime = regTime;
		this.pdtUid = pdtUid;
		this.usrUid = usrUid;
		this.styleImg = styleImg;
		this.status = status;
	}

	// Property accessors

	public Integer getPdtStyleUid() {
		return this.pdtStyleUid;
	}

	public void setPdtStyleUid(Integer pdtStyleUid) {
		this.pdtStyleUid = pdtStyleUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public String getStyleImg() {
		return this.styleImg;
	}

	public void setStyleImg(String styleImg) {
		this.styleImg = styleImg;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}