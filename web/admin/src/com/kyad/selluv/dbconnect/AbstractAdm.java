package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractAdm entity provides the base persistence definition of the Adm
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractAdm implements java.io.Serializable {

	// Fields

	private Integer admUid;
	private Timestamp regTime;
	private String admId;
	private String admPwd;
	private String admNck;
	private String admMail;
	private String admPhn;
	private Integer privilegeUid;
	private Timestamp edtTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractAdm() {
	}

	/** full constructor */
	public AbstractAdm(Timestamp regTime, String admId, String admPwd,
			String admNck, String admMail, String admPhn, Integer privilegeUid,
			Timestamp edtTime, Integer status) {
		this.regTime = regTime;
		this.admId = admId;
		this.admPwd = admPwd;
		this.admNck = admNck;
		this.admMail = admMail;
		this.admPhn = admPhn;
		this.privilegeUid = privilegeUid;
		this.edtTime = edtTime;
		this.status = status;
	}

	// Property accessors

	public Integer getAdmUid() {
		return this.admUid;
	}

	public void setAdmUid(Integer admUid) {
		this.admUid = admUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getAdmId() {
		return this.admId;
	}

	public void setAdmId(String admId) {
		this.admId = admId;
	}

	public String getAdmPwd() {
		return this.admPwd;
	}

	public void setAdmPwd(String admPwd) {
		this.admPwd = admPwd;
	}

	public String getAdmNck() {
		return this.admNck;
	}

	public void setAdmNck(String admNck) {
		this.admNck = admNck;
	}

	public String getAdmMail() {
		return this.admMail;
	}

	public void setAdmMail(String admMail) {
		this.admMail = admMail;
	}

	public String getAdmPhn() {
		return this.admPhn;
	}

	public void setAdmPhn(String admPhn) {
		this.admPhn = admPhn;
	}

	public Integer getPrivilegeUid() {
		return this.privilegeUid;
	}

	public void setPrivilegeUid(Integer privilegeUid) {
		this.privilegeUid = privilegeUid;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}