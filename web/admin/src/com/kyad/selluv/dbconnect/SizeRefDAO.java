package com.kyad.selluv.dbconnect;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * SizeRef entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.SizeRef
 * @author MyEclipse Persistence Tools
 */
public class SizeRefDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(SizeRefDAO.class);
	// property constants
	public static final String PDT_GROUP = "pdtGroup";
	public static final String CATEGORY1 = "category1";
	public static final String CATEGORY2 = "category2";
	public static final String CATEGORY3 = "category3";
	public static final String BASIS = "basis";
	public static final String TYPE_NAME = "typeName";
	public static final String SIZE_NAME = "sizeName";
	public static final String STATUS = "status";

	public void save(SizeRef transientInstance) {
		log.debug("saving SizeRef instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SizeRef persistentInstance) {
		log.debug("deleting SizeRef instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SizeRef findById(java.lang.Integer id) {
		log.debug("getting SizeRef instance with id: " + id);
		try {
			SizeRef instance = (SizeRef) getSession().get(
					"com.kyad.selluv.dbconnect.SizeRef", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SizeRef instance) {
		log.debug("finding SizeRef instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.SizeRef")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SizeRef instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from SizeRef as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByPdtGroup(Object pdtGroup) {
		return findByProperty(PDT_GROUP, pdtGroup);
	}

	public List findByCategory1(Object category1) {
		return findByProperty(CATEGORY1, category1);
	}

	public List findByCategory2(Object category2) {
		return findByProperty(CATEGORY2, category2);
	}

	public List findByCategory3(Object category3) {
		return findByProperty(CATEGORY3, category3);
	}

	public List findByBasis(Object basis) {
		return findByProperty(BASIS, basis);
	}

	public List findByTypeName(Object typeName) {
		return findByProperty(TYPE_NAME, typeName);
	}

	public List findBySizeName(Object sizeName) {
		return findByProperty(SIZE_NAME, sizeName);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all SizeRef instances");
		try {
			String queryString = "from SizeRef";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SizeRef merge(SizeRef detachedInstance) {
		log.debug("merging SizeRef instance");
		try {
			SizeRef result = (SizeRef) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SizeRef instance) {
		log.debug("attaching dirty SizeRef instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SizeRef instance) {
		log.debug("attaching clean SizeRef instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
	
	public List getCategory2ListByParent(Integer parent) {
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.SizeRef")
					.setProjection(Projections.projectionList().add(Projections.groupProperty(CATEGORY2)))
					.add(Restrictions.eq(CATEGORY1, parent))
					.add(Restrictions.not(Restrictions.eq(CATEGORY2, ""))).list();
			return results;
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List getCategory3ListByParent(String parent) {
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.SizeRef")
					.setProjection(Projections.projectionList().add(Projections.groupProperty(CATEGORY3)))
					.add(Restrictions.eq(CATEGORY2, parent))
					.add(Restrictions.not(Restrictions.eq(CATEGORY3, ""))).list();
			return results;
		} catch (RuntimeException re) {
			throw re;
		}
	}
}