package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * MoneyChangeHis entity. @author MyEclipse Persistence Tools
 */
public class MoneyChangeHis extends AbstractMoneyChangeHis implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public MoneyChangeHis() {
	}

	/** full constructor */
	public MoneyChangeHis(Timestamp regTime, Integer usrUid, Integer kind,
			Integer targetUid, String pdtUid, String content, Long amount,
			Integer status) {
		super(regTime, usrUid, kind, targetUid, pdtUid, content, amount, status);
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		MoneyChangeHisDAO pdao = new MoneyChangeHisDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
	
	public static int getCountByUsrUid(int usr_uid) {
		int count = 0;
		try{
			count = (new MoneyChangeHisDAO()).getCountByUsrUid(usr_uid);
		}catch(Exception e){
			return count;
		}
		return count;
	}
}
