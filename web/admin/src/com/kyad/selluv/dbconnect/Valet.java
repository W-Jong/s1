package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Valet entity. @author MyEclipse Persistence Tools
 */
public class Valet extends AbstractValet implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Valet() {
	}

	/** full constructor */
	public Valet(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Long reqPrice, Long earnPrice, String reqName, String reqPhone,
			String reqAddress, String reqAddressDetail, Integer sendType,
			Integer receiveYn, String signImg, String memo, Timestamp edtTime,
			Integer status) {
		super(regTime, usrUid, pdtUid, reqPrice, earnPrice, reqName, reqPhone,
				reqAddress, reqAddressDetail, sendType, receiveYn, signImg,
				memo, edtTime, status);
	}

	public static Valet getInstance(int valet_uid) {
		Valet valet = null;
		
		if(valet_uid < 1)
			return valet;
		
		ValetDAO ndao = new ValetDAO();
		try {
			valet = ndao.findById(valet_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return valet;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		ValetDAO pdao = new ValetDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		ValetDAO pdao = new ValetDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ValetDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByStatus(int status){
		int count = 0;
		
		try{
			count = (new ValetDAO()).getCountByStatus(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
