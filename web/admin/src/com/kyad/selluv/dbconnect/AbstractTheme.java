package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractTheme entity provides the base persistence definition of the Theme
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractTheme implements java.io.Serializable {

	// Fields

	private Integer themeUid;
	private Timestamp regTime;
	private Integer kind;
	private String titleEn;
	private String titleKo;
	private String profileImg;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractTheme() {
	}

	/** full constructor */
	public AbstractTheme(Timestamp regTime, Integer kind, String titleEn,
			String titleKo, String profileImg, Timestamp startTime,
			Timestamp endTime, Integer status) {
		this.regTime = regTime;
		this.kind = kind;
		this.titleEn = titleEn;
		this.titleKo = titleKo;
		this.profileImg = profileImg;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
	}

	// Property accessors

	public Integer getThemeUid() {
		return this.themeUid;
	}

	public void setThemeUid(Integer themeUid) {
		this.themeUid = themeUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getTitleEn() {
		return this.titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public String getTitleKo() {
		return this.titleKo;
	}

	public void setTitleKo(String titleKo) {
		this.titleKo = titleKo;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}