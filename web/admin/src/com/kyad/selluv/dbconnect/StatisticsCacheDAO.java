package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * StatisticsCache entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.StatisticsCache
 * @author MyEclipse Persistence Tools
 */
public class StatisticsCacheDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(StatisticsCacheDAO.class);
	// property constants
	public static final String DEAL_COUNT = "dealCount";
	public static final String DEAL_AMOUNT = "dealAmount";
	public static final String JOIN_COUNT = "joinCount";
	public static final String VISIT_USER_COUNT = "visitUserCount";
	public static final String VISIT_COUNT = "visitCount";
	public static final String STATUS = "status";

	public void save(StatisticsCache transientInstance) {
		log.debug("saving StatisticsCache instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(StatisticsCache persistentInstance) {
		log.debug("deleting StatisticsCache instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public StatisticsCache findById(java.lang.Integer id) {
		log.debug("getting StatisticsCache instance with id: " + id);
		try {
			StatisticsCache instance = (StatisticsCache) getSession().get(
					"com.kyad.selluv.dbconnect.StatisticsCache", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(StatisticsCache instance) {
		log.debug("finding StatisticsCache instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.StatisticsCache")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding StatisticsCache instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from StatisticsCache as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByDealCount(Object dealCount) {
		return findByProperty(DEAL_COUNT, dealCount);
	}

	public List findByDealAmount(Object dealAmount) {
		return findByProperty(DEAL_AMOUNT, dealAmount);
	}

	public List findByJoinCount(Object joinCount) {
		return findByProperty(JOIN_COUNT, joinCount);
	}

	public List findByVisitUserCount(Object visitUserCount) {
		return findByProperty(VISIT_USER_COUNT, visitUserCount);
	}

	public List findByVisitCount(Object visitCount) {
		return findByProperty(VISIT_COUNT, visitCount);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all StatisticsCache instances");
		try {
			String queryString = "from StatisticsCache";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public StatisticsCache merge(StatisticsCache detachedInstance) {
		log.debug("merging StatisticsCache instance");
		try {
			StatisticsCache result = (StatisticsCache) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(StatisticsCache instance) {
		log.debug("attaching dirty StatisticsCache instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(StatisticsCache instance) {
		log.debug("attaching clean StatisticsCache instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}