package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrPushSetting entity provides the base persistence definition of the
 * UsrPushSetting entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrPushSetting implements java.io.Serializable {

	// Fields

	private Integer usrUid;
	private Timestamp regTime;
	private Integer dealYn;
	private Integer newsYn;
	private Integer replyYn;
	private String dealSetting;
	private String newsSetting;
	private String replySetting;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrPushSetting() {
	}

	/** minimal constructor */
	public AbstractUsrPushSetting(Integer usrUid, Timestamp regTime,
			Integer dealYn, Integer newsYn, Integer replyYn, Integer status) {
		this.usrUid = usrUid;
		this.regTime = regTime;
		this.dealYn = dealYn;
		this.newsYn = newsYn;
		this.replyYn = replyYn;
		this.status = status;
	}

	/** full constructor */
	public AbstractUsrPushSetting(Integer usrUid, Timestamp regTime,
			Integer dealYn, Integer newsYn, Integer replyYn,
			String dealSetting, String newsSetting, String replySetting,
			Integer status) {
		this.usrUid = usrUid;
		this.regTime = regTime;
		this.dealYn = dealYn;
		this.newsYn = newsYn;
		this.replyYn = replyYn;
		this.dealSetting = dealSetting;
		this.newsSetting = newsSetting;
		this.replySetting = replySetting;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getDealYn() {
		return this.dealYn;
	}

	public void setDealYn(Integer dealYn) {
		this.dealYn = dealYn;
	}

	public Integer getNewsYn() {
		return this.newsYn;
	}

	public void setNewsYn(Integer newsYn) {
		this.newsYn = newsYn;
	}

	public Integer getReplyYn() {
		return this.replyYn;
	}

	public void setReplyYn(Integer replyYn) {
		this.replyYn = replyYn;
	}

	public String getDealSetting() {
		return this.dealSetting;
	}

	public void setDealSetting(String dealSetting) {
		this.dealSetting = dealSetting;
	}

	public String getNewsSetting() {
		return this.newsSetting;
	}

	public void setNewsSetting(String newsSetting) {
		this.newsSetting = newsSetting;
	}

	public String getReplySetting() {
		return this.replySetting;
	}

	public void setReplySetting(String replySetting) {
		this.replySetting = replySetting;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}