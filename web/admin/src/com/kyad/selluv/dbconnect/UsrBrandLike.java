package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrBrandLike entity. @author MyEclipse Persistence Tools
 */
public class UsrBrandLike extends AbstractUsrBrandLike implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrBrandLike() {
	}

	/** full constructor */
	public UsrBrandLike(Timestamp regTime, Integer usrUid, Integer brandUid,
			Integer status) {
		super(regTime, usrUid, brandUid, status);
	}

}
