package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrBlock entity. @author MyEclipse Persistence Tools
 */
public class UsrBlock extends AbstractUsrBlock implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrBlock() {
	}

	/** full constructor */
	public UsrBlock(Timestamp regTime, Integer usrUid, Integer peerUsrUid,
			Integer status) {
		super(regTime, usrUid, peerUsrUid, status);
	}

}
