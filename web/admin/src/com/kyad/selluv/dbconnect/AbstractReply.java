package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractReply entity provides the base persistence definition of the Reply
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractReply implements java.io.Serializable {

	// Fields

	private Integer replyUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer pdtUid;
	private String targetUids;
	private String content;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractReply() {
	}

	/** full constructor */
	public AbstractReply(Timestamp regTime, Integer usrUid, Integer pdtUid,
			String targetUids, String content, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.pdtUid = pdtUid;
		this.targetUids = targetUids;
		this.content = content;
		this.status = status;
	}

	// Property accessors

	public Integer getReplyUid() {
		return this.replyUid;
	}

	public void setReplyUid(Integer replyUid) {
		this.replyUid = replyUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public String getTargetUids() {
		return this.targetUids;
	}

	public void setTargetUids(String targetUids) {
		this.targetUids = targetUids;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}