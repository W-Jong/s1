package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import com.kyad.selluv.common.Constant;

/**
 * Bank entity. @author MyEclipse Persistence Tools
 */
public class Bank extends AbstractBank implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Bank() {
	}

	/** full constructor */
	public Bank(Timestamp regTime, String bankNm, Integer status) {
		super(regTime, bankNm, status);
	}
	
	@SuppressWarnings("unchecked")
	public static List<Bank> getList(){
		List<Bank> list = null;
		
		BankDAO cdao = new BankDAO();
		try {
			list = cdao.findByStatus(Constant.STATUS_USE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		return list;
	}
}
