package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractPromotionCode entity provides the base persistence definition of the
 * PromotionCode entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPromotionCode implements java.io.Serializable {

	// Fields

	private Integer promotionCodeUid;
	private Timestamp regTime;
	private Integer kind;
	private String title;
	private Long price;
	private Integer priceUnit;
	private String memo;
	private Integer limitType;
	private String limitUids;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractPromotionCode() {
	}

	/** full constructor */
	public AbstractPromotionCode(Timestamp regTime, Integer kind, String title,
			Long price, Integer priceUnit, String memo, Integer limitType,
			String limitUids, Timestamp startTime, Timestamp endTime,
			Integer status) {
		this.regTime = regTime;
		this.kind = kind;
		this.title = title;
		this.price = price;
		this.priceUnit = priceUnit;
		this.memo = memo;
		this.limitType = limitType;
		this.limitUids = limitUids;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
	}

	// Property accessors

	public Integer getPromotionCodeUid() {
		return this.promotionCodeUid;
	}

	public void setPromotionCodeUid(Integer promotionCodeUid) {
		this.promotionCodeUid = promotionCodeUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getPrice() {
		return this.price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Integer getPriceUnit() {
		return this.priceUnit;
	}

	public void setPriceUnit(Integer priceUnit) {
		this.priceUnit = priceUnit;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getLimitType() {
		return this.limitType;
	}

	public void setLimitType(Integer limitType) {
		this.limitType = limitType;
	}

	public String getLimitUids() {
		return this.limitUids;
	}

	public void setLimitUids(String limitUids) {
		this.limitUids = limitUids;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}