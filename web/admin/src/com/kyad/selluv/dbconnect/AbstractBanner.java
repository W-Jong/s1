package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractBanner entity provides the base persistence definition of the Banner
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractBanner implements java.io.Serializable {

	// Fields

	private Integer bannerUid;
	private Timestamp regTime;
	private Integer type;
	private Integer targetUid;
	private String title;
	private String profileImg;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractBanner() {
	}

	/** full constructor */
	public AbstractBanner(Timestamp regTime, Integer type, Integer targetUid,
			String title, String profileImg, Timestamp startTime,
			Timestamp endTime, Integer status) {
		this.regTime = regTime;
		this.type = type;
		this.targetUid = targetUid;
		this.title = title;
		this.profileImg = profileImg;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
	}

	// Property accessors

	public Integer getBannerUid() {
		return this.bannerUid;
	}

	public void setBannerUid(Integer bannerUid) {
		this.bannerUid = bannerUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getTargetUid() {
		return this.targetUid;
	}

	public void setTargetUid(Integer targetUid) {
		this.targetUid = targetUid;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}