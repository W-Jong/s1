package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractNotice entity provides the base persistence definition of the Notice
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractNotice implements java.io.Serializable {

	// Fields

	private Integer noticeUid;
	private Timestamp regTime;
	private String title;
	private String content;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractNotice() {
	}

	/** full constructor */
	public AbstractNotice(Timestamp regTime, String title, String content,
			Timestamp startTime, Timestamp endTime, Integer status) {
		this.regTime = regTime;
		this.title = title;
		this.content = content;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
	}

	// Property accessors

	public Integer getNoticeUid() {
		return this.noticeUid;
	}

	public void setNoticeUid(Integer noticeUid) {
		this.noticeUid = noticeUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}