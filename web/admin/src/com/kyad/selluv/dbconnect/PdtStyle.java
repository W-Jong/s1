package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * PdtStyle entity. @author MyEclipse Persistence Tools
 */
public class PdtStyle extends AbstractPdtStyle implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public PdtStyle() {
	}

	/** full constructor */
	public PdtStyle(Timestamp regTime, Integer pdtUid, Integer usrUid,
			String styleImg, Integer status) {
		super(regTime, pdtUid, usrUid, styleImg, status);
	}

	public static PdtStyle getInstanceByStyleImg(String img) {
		List<PdtStyle> list = new ArrayList<PdtStyle>();
		if(img == null || img.isEmpty())
			return null;
		
		PdtStyleDAO ndao = new PdtStyleDAO();
		try {
			list = ndao.findByStyleImg(img);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		if (list == null || list.size() < 1)
			return null;
		return list.get(0);
	}
	
	public static List<PdtStyle> getInstanceByPdtUid(Integer pdtUid) {
		List<PdtStyle> list = new ArrayList<PdtStyle>();
		if(pdtUid < 1)
			return null;
		
		PdtStyleDAO ndao = new PdtStyleDAO();
		PdtStyle instance = new PdtStyle();
		instance.setPdtUid(pdtUid);
		instance.setStatus(Constant.STATUS_USE);
		try {
			
			list = ndao.findByExample(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		if (list == null || list.size() < 1)
			return null;
		return list;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		PdtStyleDAO pdao = new PdtStyleDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		PdtStyleDAO pdao = new PdtStyleDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public static void DeleteAllFromStatus(int status) {
		PdtStyleDAO pdao = new PdtStyleDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		try {

			String sql = "delete from pdt_style where status = " + status;
			pdao.getSession().createSQLQuery(sql).executeUpdate();
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
	}
}
