package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractPrivilege entity provides the base persistence definition of the
 * Privilege entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPrivilege implements java.io.Serializable {

	// Fields

	private Integer privilegeUid;
	private Timestamp regTime;
	private Integer regAdmUid;
	private String menu;
	private String privilegeNm;
	private Integer edtAdmUid;
	private Timestamp edtTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractPrivilege() {
	}

	/** full constructor */
	public AbstractPrivilege(Timestamp regTime, Integer regAdmUid, String menu,
			String privilegeNm, Integer edtAdmUid, Timestamp edtTime,
			Integer status) {
		this.regTime = regTime;
		this.regAdmUid = regAdmUid;
		this.menu = menu;
		this.privilegeNm = privilegeNm;
		this.edtAdmUid = edtAdmUid;
		this.edtTime = edtTime;
		this.status = status;
	}

	// Property accessors

	public Integer getPrivilegeUid() {
		return this.privilegeUid;
	}

	public void setPrivilegeUid(Integer privilegeUid) {
		this.privilegeUid = privilegeUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getRegAdmUid() {
		return this.regAdmUid;
	}

	public void setRegAdmUid(Integer regAdmUid) {
		this.regAdmUid = regAdmUid;
	}

	public String getMenu() {
		return this.menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getPrivilegeNm() {
		return this.privilegeNm;
	}

	public void setPrivilegeNm(String privilegeNm) {
		this.privilegeNm = privilegeNm;
	}

	public Integer getEdtAdmUid() {
		return this.edtAdmUid;
	}

	public void setEdtAdmUid(Integer edtAdmUid) {
		this.edtAdmUid = edtAdmUid;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}