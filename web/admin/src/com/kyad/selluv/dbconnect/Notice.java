package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Notice entity. @author MyEclipse Persistence Tools
 */
public class Notice extends AbstractNotice implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Notice() {
	}

	/** full constructor */
	public Notice(Timestamp regTime, String title, String content,
			Timestamp startTime, Timestamp endTime, Integer status) {
		super(regTime, title, content, startTime, endTime, status);
	}

	public static Notice getInstance(int notice_uid) {
		Notice notice = null;
		
		if(notice_uid < 1)
			return notice;
		
		NoticeDAO ndao = new NoticeDAO();
		try {
			notice = ndao.findById(notice_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return notice;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new NoticeDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		NoticeDAO ndao = new NoticeDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public int Save(){
		int res = Constant.RESULT_DB_ERR;
		
		NoticeDAO ndao = new NoticeDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
}
