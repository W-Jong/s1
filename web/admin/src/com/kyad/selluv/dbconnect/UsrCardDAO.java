package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UsrCard entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.UsrCard
 * @author MyEclipse Persistence Tools
 */
public class UsrCardDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(UsrCardDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String KIND = "kind";
	public static final String CARD_NUMBER = "cardNumber";
	public static final String VALID_NUM = "validNum";
	public static final String PASSWORD = "password";
	public static final String IAMPORT_VALID_YN = "iamportValidYn";
	public static final String STATUS = "status";

	public void save(UsrCard transientInstance) {
		log.debug("saving UsrCard instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UsrCard persistentInstance) {
		log.debug("deleting UsrCard instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsrCard findById(java.lang.Integer id) {
		log.debug("getting UsrCard instance with id: " + id);
		try {
			UsrCard instance = (UsrCard) getSession().get(
					"com.kyad.selluv.dbconnect.UsrCard", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UsrCard instance) {
		log.debug("finding UsrCard instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.UsrCard")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UsrCard instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UsrCard as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByCardNumber(Object cardNumber) {
		return findByProperty(CARD_NUMBER, cardNumber);
	}

	public List findByValidNum(Object validNum) {
		return findByProperty(VALID_NUM, validNum);
	}

	public List findByPassword(Object password) {
		return findByProperty(PASSWORD, password);
	}

	public List findByIamportValidYn(Object iamportValidYn) {
		return findByProperty(IAMPORT_VALID_YN, iamportValidYn);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all UsrCard instances");
		try {
			String queryString = "from UsrCard";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UsrCard merge(UsrCard detachedInstance) {
		log.debug("merging UsrCard instance");
		try {
			UsrCard result = (UsrCard) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UsrCard instance) {
		log.debug("attaching dirty UsrCard instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsrCard instance) {
		log.debug("attaching clean UsrCard instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}