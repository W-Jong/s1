package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * SystemSetting entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.SystemSetting
 * @author MyEclipse Persistence Tools
 */
public class SystemSettingDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(SystemSettingDAO.class);
	// property constants
	public static final String SETTING = "setting";
	public static final String SETTING_EXTRA1 = "settingExtra1";
	public static final String SETTING_EXTRA2 = "settingExtra2";
	public static final String MEMO = "memo";
	public static final String STATUS = "status";

	public void save(SystemSetting transientInstance) {
		log.debug("saving SystemSetting instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SystemSetting persistentInstance) {
		log.debug("deleting SystemSetting instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SystemSetting findById(java.lang.Integer id) {
		log.debug("getting SystemSetting instance with id: " + id);
		try {
			SystemSetting instance = (SystemSetting) getSession().get(
					"com.kyad.selluv.dbconnect.SystemSetting", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SystemSetting instance) {
		log.debug("finding SystemSetting instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.SystemSetting")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SystemSetting instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from SystemSetting as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findBySetting(Object setting) {
		return findByProperty(SETTING, setting);
	}

	public List findBySettingExtra1(Object settingExtra1) {
		return findByProperty(SETTING_EXTRA1, settingExtra1);
	}

	public List findBySettingExtra2(Object settingExtra2) {
		return findByProperty(SETTING_EXTRA2, settingExtra2);
	}

	public List findByMemo(Object memo) {
		return findByProperty(MEMO, memo);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all SystemSetting instances");
		try {
			String queryString = "from SystemSetting";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SystemSetting merge(SystemSetting detachedInstance) {
		log.debug("merging SystemSetting instance");
		try {
			SystemSetting result = (SystemSetting) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SystemSetting instance) {
		log.debug("attaching dirty SystemSetting instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SystemSetting instance) {
		log.debug("attaching clean SystemSetting instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}