package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrAlarm entity provides the base persistence definition of the
 * UsrAlarm entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrAlarm implements java.io.Serializable {

	// Fields

	private Integer usrAlarmUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer kind;
	private Integer subKind;
	private Integer targetUid;
	private String content;
	private Integer pdtUid;
	private String profileImg;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrAlarm() {
	}

	/** full constructor */
	public AbstractUsrAlarm(Timestamp regTime, Integer usrUid, Integer kind,
			Integer subKind, Integer targetUid, String content, Integer pdtUid,
			String profileImg, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.kind = kind;
		this.subKind = subKind;
		this.targetUid = targetUid;
		this.content = content;
		this.pdtUid = pdtUid;
		this.profileImg = profileImg;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrAlarmUid() {
		return this.usrAlarmUid;
	}

	public void setUsrAlarmUid(Integer usrAlarmUid) {
		this.usrAlarmUid = usrAlarmUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public Integer getSubKind() {
		return this.subKind;
	}

	public void setSubKind(Integer subKind) {
		this.subKind = subKind;
	}

	public Integer getTargetUid() {
		return this.targetUid;
	}

	public void setTargetUid(Integer targetUid) {
		this.targetUid = targetUid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public String getProfileImg() {
		return this.profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}