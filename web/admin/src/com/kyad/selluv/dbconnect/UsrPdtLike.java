package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * UsrPdtLike entity. @author MyEclipse Persistence Tools
 */
public class UsrPdtLike extends AbstractUsrPdtLike implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UsrPdtLike() {
	}

	/** full constructor */
	public UsrPdtLike(Timestamp regTime, Integer usrUid, Integer pdtUid,
			Integer status) {
		super(regTime, usrUid, pdtUid, status);
	}

}
