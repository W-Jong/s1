package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractUsrBrandLike entity provides the base persistence definition of the
 * UsrBrandLike entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrBrandLike implements java.io.Serializable {

	// Fields

	private Integer usrBrandLikeUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer brandUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrBrandLike() {
	}

	/** full constructor */
	public AbstractUsrBrandLike(Timestamp regTime, Integer usrUid,
			Integer brandUid, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.brandUid = brandUid;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrBrandLikeUid() {
		return this.usrBrandLikeUid;
	}

	public void setUsrBrandLikeUid(Integer usrBrandLikeUid) {
		this.usrBrandLikeUid = usrBrandLikeUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getBrandUid() {
		return this.brandUid;
	}

	public void setBrandUid(Integer brandUid) {
		this.brandUid = brandUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}