package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Report entity. @author MyEclipse Persistence Tools
 */
public class Report extends AbstractReport implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Report() {
	}

	/** full constructor */
	public Report(Timestamp regTime, Integer usrUid, Integer peerUsrUid,
			Integer pdtUid, Integer type, String content, String memo,
			Integer status) {
		super(regTime, usrUid, peerUsrUid, pdtUid, type, content, memo, status);
	}

	public static Report getInstance(int report_uid) {
		Report report = null;
		
		if(report_uid < 1)
			return report;
		
		ReportDAO ndao = new ReportDAO();
		try {
			report = ndao.findById(report_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return report;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		ReportDAO ndao = new ReportDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ReportDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public static int getCountByStatus(int status){
		int count = 0;
		
		try{
			count = (new ReportDAO()).getCountByStatus(status);
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
}
