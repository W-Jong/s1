package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AdmUdtHis entity. @author MyEclipse Persistence Tools
 */
public class AdmUdtHis extends AbstractAdmUdtHis implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public AdmUdtHis() {
	}

	/** full constructor */
	public AdmUdtHis(Timestamp regTime, Integer admUid, Integer kind,
			String content, Integer status) {
		super(regTime, admUid, kind, content, status);
	}

}
