package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * License entity. @author MyEclipse Persistence Tools
 */
public class License extends AbstractLicense implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public License() {
	}

	/** full constructor */
	public License(Integer licenseUid, Timestamp regTime, String content,
			Timestamp edtTime, Integer status) {
		super(licenseUid, regTime, content, edtTime, status);
	}
	
	@SuppressWarnings("unchecked")
	public static List<License> getList() {
		List<License> list = null;
		
		LicenseDAO cdao = new LicenseDAO();
		try {
			list = cdao.findByStatus(Constant.STATUS_USE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cdao.getSession().clear();
		cdao.getSession().close();
		
		return list;
	}

	public static License getInstance(int License_uid) {
		License License = null;
		
		if(License_uid < 1)
			return License;
		
		LicenseDAO ndao = new LicenseDAO();
		try {
			License = ndao.findById(License_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return License;
	}
	
	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		LicenseDAO pdao = new LicenseDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		LicenseDAO pdao = new LicenseDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
}
