package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractThemePdt entity provides the base persistence definition of the
 * ThemePdt entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractThemePdt implements java.io.Serializable {

	// Fields

	private Integer themePdtUid;
	private Timestamp regTime;
	private Integer themeUid;
	private Integer pdtUid;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractThemePdt() {
	}

	/** full constructor */
	public AbstractThemePdt(Timestamp regTime, Integer themeUid,
			Integer pdtUid, Integer status) {
		this.regTime = regTime;
		this.themeUid = themeUid;
		this.pdtUid = pdtUid;
		this.status = status;
	}

	// Property accessors

	public Integer getThemePdtUid() {
		return this.themePdtUid;
	}

	public void setThemePdtUid(Integer themePdtUid) {
		this.themePdtUid = themePdtUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getThemeUid() {
		return this.themeUid;
	}

	public void setThemeUid(Integer themeUid) {
		this.themeUid = themeUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}