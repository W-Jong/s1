package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * SearchWord entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.SearchWord
 * @author MyEclipse Persistence Tools
 */
public class SearchWordDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(SearchWordDAO.class);
	// property constants
	public static final String CONTENT = "content";
	public static final String PDT_COUNT = "pdtCount";
	public static final String STATUS = "status";

	public void save(SearchWord transientInstance) {
		log.debug("saving SearchWord instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SearchWord persistentInstance) {
		log.debug("deleting SearchWord instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SearchWord findById(java.lang.Integer id) {
		log.debug("getting SearchWord instance with id: " + id);
		try {
			SearchWord instance = (SearchWord) getSession().get(
					"com.kyad.selluv.dbconnect.SearchWord", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SearchWord instance) {
		log.debug("finding SearchWord instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.SearchWord")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SearchWord instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from SearchWord as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByPdtCount(Object pdtCount) {
		return findByProperty(PDT_COUNT, pdtCount);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all SearchWord instances");
		try {
			String queryString = "from SearchWord";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SearchWord merge(SearchWord detachedInstance) {
		log.debug("merging SearchWord instance");
		try {
			SearchWord result = (SearchWord) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SearchWord instance) {
		log.debug("attaching dirty SearchWord instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SearchWord instance) {
		log.debug("attaching clean SearchWord instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}