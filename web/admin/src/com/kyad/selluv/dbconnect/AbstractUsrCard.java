package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.Date;

/**
 * AbstractUsrCard entity provides the base persistence definition of the
 * UsrCard entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUsrCard implements java.io.Serializable {

	// Fields

	private Integer usrCardUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer kind;
	private String cardNumber;
	private Date validDate;
	private String validNum;
	private String password;
	private Integer iamportValidYn;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractUsrCard() {
	}

	/** full constructor */
	public AbstractUsrCard(Timestamp regTime, Integer usrUid, Integer kind,
			String cardNumber, Date validDate, String validNum,
			String password, Integer iamportValidYn, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.kind = kind;
		this.cardNumber = cardNumber;
		this.validDate = validDate;
		this.validNum = validNum;
		this.password = password;
		this.iamportValidYn = iamportValidYn;
		this.status = status;
	}

	// Property accessors

	public Integer getUsrCardUid() {
		return this.usrCardUid;
	}

	public void setUsrCardUid(Integer usrCardUid) {
		this.usrCardUid = usrCardUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getKind() {
		return this.kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getValidDate() {
		return this.validDate;
	}

	public void setValidDate(Date validDate) {
		this.validDate = validDate;
	}

	public String getValidNum() {
		return this.validNum;
	}

	public void setValidNum(String validNum) {
		this.validNum = validNum;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getIamportValidYn() {
		return this.iamportValidYn;
	}

	public void setIamportValidYn(Integer iamportValidYn) {
		this.iamportValidYn = iamportValidYn;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}