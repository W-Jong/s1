package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kyad.selluv.common.Constant;

/**
 * A data access object (DAO) providing persistence and search support for Valet
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.Valet
 * @author MyEclipse Persistence Tools
 */
public class ValetDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(ValetDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String PDT_UID = "pdtUid";
	public static final String REQ_PRICE = "reqPrice";
	public static final String EARN_PRICE = "earnPrice";
	public static final String REQ_NAME = "reqName";
	public static final String REQ_PHONE = "reqPhone";
	public static final String REQ_ADDRESS = "reqAddress";
	public static final String REQ_ADDRESS_DETAIL = "reqAddressDetail";
	public static final String SEND_TYPE = "sendType";
	public static final String RECEIVE_YN = "receiveYn";
	public static final String SIGN_IMG = "signImg";
	public static final String MEMO = "memo";
	public static final String STATUS = "status";

	public void save(Valet transientInstance) {
		log.debug("saving Valet instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Valet persistentInstance) {
		log.debug("deleting Valet instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Valet findById(java.lang.Integer id) {
		log.debug("getting Valet instance with id: " + id);
		try {
			Valet instance = (Valet) getSession().get(
					"com.kyad.selluv.dbconnect.Valet", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Valet instance) {
		log.debug("finding Valet instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Valet")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Valet instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Valet as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByPdtUid(Object pdtUid) {
		return findByProperty(PDT_UID, pdtUid);
	}

	public List findByReqPrice(Object reqPrice) {
		return findByProperty(REQ_PRICE, reqPrice);
	}

	public List findByEarnPrice(Object earnPrice) {
		return findByProperty(EARN_PRICE, earnPrice);
	}

	public List findByReqName(Object reqName) {
		return findByProperty(REQ_NAME, reqName);
	}

	public List findByReqPhone(Object reqPhone) {
		return findByProperty(REQ_PHONE, reqPhone);
	}

	public List findByReqAddress(Object reqAddress) {
		return findByProperty(REQ_ADDRESS, reqAddress);
	}

	public List findByReqAddressDetail(Object reqAddressDetail) {
		return findByProperty(REQ_ADDRESS_DETAIL, reqAddressDetail);
	}

	public List findBySendType(Object sendType) {
		return findByProperty(SEND_TYPE, sendType);
	}

	public List findByReceiveYn(Object receiveYn) {
		return findByProperty(RECEIVE_YN, receiveYn);
	}

	public List findBySignImg(Object signImg) {
		return findByProperty(SIGN_IMG, signImg);
	}

	public List findByMemo(Object memo) {
		return findByProperty(MEMO, memo);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all Valet instances");
		try {
			String queryString = "from Valet";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Valet merge(Valet detachedInstance) {
		log.debug("merging Valet instance");
		try {
			Valet result = (Valet) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Valet instance) {
		log.debug("attaching dirty Valet instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Valet instance) {
		log.debug("attaching clean Valet instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Valet")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCountByStatus(int status) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.Valet")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq(STATUS, status));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}