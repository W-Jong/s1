package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractReport entity provides the base persistence definition of the Report
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractReport implements java.io.Serializable {

	// Fields

	private Integer reportUid;
	private Timestamp regTime;
	private Integer usrUid;
	private Integer peerUsrUid;
	private Integer pdtUid;
	private Integer type;
	private String content;
	private String memo;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractReport() {
	}

	/** full constructor */
	public AbstractReport(Timestamp regTime, Integer usrUid,
			Integer peerUsrUid, Integer pdtUid, Integer type, String content,
			String memo, Integer status) {
		this.regTime = regTime;
		this.usrUid = usrUid;
		this.peerUsrUid = peerUsrUid;
		this.pdtUid = pdtUid;
		this.type = type;
		this.content = content;
		this.memo = memo;
		this.status = status;
	}

	// Property accessors

	public Integer getReportUid() {
		return this.reportUid;
	}

	public void setReportUid(Integer reportUid) {
		this.reportUid = reportUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public Integer getUsrUid() {
		return this.usrUid;
	}

	public void setUsrUid(Integer usrUid) {
		this.usrUid = usrUid;
	}

	public Integer getPeerUsrUid() {
		return this.peerUsrUid;
	}

	public void setPeerUsrUid(Integer peerUsrUid) {
		this.peerUsrUid = peerUsrUid;
	}

	public Integer getPdtUid() {
		return this.pdtUid;
	}

	public void setPdtUid(Integer pdtUid) {
		this.pdtUid = pdtUid;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}