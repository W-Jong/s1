package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Theme entity. @author MyEclipse Persistence Tools
 */
public class Theme extends AbstractTheme implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Theme() {
	}

	/** full constructor */
	public Theme(Timestamp regTime, Integer kind, String titleEn,
			String titleKo, String profileImg, Timestamp startTime,
			Timestamp endTime, Integer status) {
		super(regTime, kind, titleEn, titleKo, profileImg, startTime, endTime,
				status);
	}

	public static Theme getInstance(int theme_uid) {
		Theme theme = null;
		
		if(theme_uid < 1)
			return theme;
		
		ThemeDAO ndao = new ThemeDAO();
		try {
			theme = ndao.findById(theme_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return theme;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ThemeDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	

	public int Update() {
		int res = Constant.RESULT_DB_ERR;
		
		ThemeDAO pdao = new ThemeDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}

	public int Save() {
		int res = Constant.RESULT_DB_ERR;
		
		ThemeDAO pdao = new ThemeDAO();
		Transaction tx = pdao.getSession().beginTransaction();
		
		try {
			pdao.save(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		pdao.getSession().clear();
		pdao.getSession().close();
		
		return res;
	}
}
