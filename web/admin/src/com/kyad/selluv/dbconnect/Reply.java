package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

import org.hibernate.Transaction;

import com.kyad.selluv.common.Constant;

/**
 * Reply entity. @author MyEclipse Persistence Tools
 */
public class Reply extends AbstractReply implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Reply() {
	}

	/** full constructor */
	public Reply(Timestamp regTime, Integer usrUid, Integer pdtUid,
			String targetUids, String content, Integer status) {
		super(regTime, usrUid, pdtUid, targetUids, content, status);
	}

	public static Reply getInstance(int reply_uid) {
		Reply reply = null;
		
		if(reply_uid < 1)
			return reply;
		
		ReplyDAO ndao = new ReplyDAO();
		try {
			reply = ndao.findById(reply_uid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return reply;
	}
	
	public static int getCount(){
		int count = 0;
		
		try{
			count = (new ReplyDAO()).getCount();
		}catch(Exception e){
			return count;
		}
		
		return count;
	}
	
	public int Update(){
		int res = Constant.RESULT_DB_ERR;
		
		ReplyDAO ndao = new ReplyDAO();
		Transaction tx = ndao.getSession().beginTransaction();
		
		try {
			ndao.getSession().update(this);
			tx.commit();
			res = Constant.RESULT_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		ndao.getSession().clear();
		ndao.getSession().close();
		
		return res;
	}
}
