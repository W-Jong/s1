package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * Color entity. @author MyEclipse Persistence Tools
 */
public class Color extends AbstractColor implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Color() {
	}

	/** full constructor */
	public Color(Timestamp regTime, String colorName, String colorCode,
			Integer status) {
		super(regTime, colorName, colorCode, status);
	}

}
