package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * InviteRewardHisUid entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.InviteRewardHisUid
 * @author MyEclipse Persistence Tools
 */
public class InviteRewardHisUidDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(InviteRewardHisUidDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String INVITE_USR_UID = "inviteUsrUid";
	public static final String JOIN_USR_UID = "joinUsrUid";
	public static final String KIND = "kind";
	public static final String DEAL_UID = "dealUid";
	public static final String REWARD_PRICE = "rewardPrice";
	public static final String STATUS = "status";

	public void save(InviteRewardHisUid transientInstance) {
		log.debug("saving InviteRewardHisUid instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(InviteRewardHisUid persistentInstance) {
		log.debug("deleting InviteRewardHisUid instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public InviteRewardHisUid findById(java.lang.Integer id) {
		log.debug("getting InviteRewardHisUid instance with id: " + id);
		try {
			InviteRewardHisUid instance = (InviteRewardHisUid) getSession()
					.get("com.kyad.selluv.dbconnect.InviteRewardHisUid", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(InviteRewardHisUid instance) {
		log.debug("finding InviteRewardHisUid instance by example");
		try {
			List results = getSession()
					.createCriteria(
							"com.kyad.selluv.dbconnect.InviteRewardHisUid")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding InviteRewardHisUid instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from InviteRewardHisUid as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByInviteUsrUid(Object inviteUsrUid) {
		return findByProperty(INVITE_USR_UID, inviteUsrUid);
	}

	public List findByJoinUsrUid(Object joinUsrUid) {
		return findByProperty(JOIN_USR_UID, joinUsrUid);
	}

	public List findByKind(Object kind) {
		return findByProperty(KIND, kind);
	}

	public List findByDealUid(Object dealUid) {
		return findByProperty(DEAL_UID, dealUid);
	}

	public List findByRewardPrice(Object rewardPrice) {
		return findByProperty(REWARD_PRICE, rewardPrice);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all InviteRewardHisUid instances");
		try {
			String queryString = "from InviteRewardHisUid";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public InviteRewardHisUid merge(InviteRewardHisUid detachedInstance) {
		log.debug("merging InviteRewardHisUid instance");
		try {
			InviteRewardHisUid result = (InviteRewardHisUid) getSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(InviteRewardHisUid instance) {
		log.debug("attaching dirty InviteRewardHisUid instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(InviteRewardHisUid instance) {
		log.debug("attaching clean InviteRewardHisUid instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.InviteRewardHisUid")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}