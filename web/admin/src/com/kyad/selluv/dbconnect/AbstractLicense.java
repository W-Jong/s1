package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractLicense entity provides the base persistence definition of the
 * License entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractLicense implements java.io.Serializable {

	// Fields

	private Integer licenseUid;
	private Timestamp regTime;
	private String content;
	private Timestamp edtTime;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractLicense() {
	}

	/** full constructor */
	public AbstractLicense(Integer licenseUid, Timestamp regTime,
			String content, Timestamp edtTime, Integer status) {
		this.licenseUid = licenseUid;
		this.regTime = regTime;
		this.content = content;
		this.edtTime = edtTime;
		this.status = status;
	}

	// Property accessors

	public Integer getLicenseUid() {
		return this.licenseUid;
	}

	public void setLicenseUid(Integer licenseUid) {
		this.licenseUid = licenseUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getEdtTime() {
		return this.edtTime;
	}

	public void setEdtTime(Timestamp edtTime) {
		this.edtTime = edtTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}