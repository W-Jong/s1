package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;

/**
 * AbstractColor entity provides the base persistence definition of the Color
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractColor implements java.io.Serializable {

	// Fields

	private Integer colorUid;
	private Timestamp regTime;
	private String colorName;
	private String colorCode;
	private Integer status;

	// Constructors

	/** default constructor */
	public AbstractColor() {
	}

	/** full constructor */
	public AbstractColor(Timestamp regTime, String colorName, String colorCode,
			Integer status) {
		this.regTime = regTime;
		this.colorName = colorName;
		this.colorCode = colorCode;
		this.status = status;
	}

	// Property accessors

	public Integer getColorUid() {
		return this.colorUid;
	}

	public void setColorUid(Integer colorUid) {
		this.colorUid = colorUid;
	}

	public Timestamp getRegTime() {
		return this.regTime;
	}

	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

	public String getColorName() {
		return this.colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	public String getColorCode() {
		return this.colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}