package com.kyad.selluv.dbconnect;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * ReportPdt entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.kyad.selluv.dbconnect.ReportPdt
 * @author MyEclipse Persistence Tools
 */
public class ReportPdtDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(ReportPdtDAO.class);
	// property constants
	public static final String USR_UID = "usrUid";
	public static final String PDT_UID = "pdtUid";
	public static final String RANK = "rank";
	public static final String CONTENT = "content";
	public static final String REWARD_YN = "rewardYn";
	public static final String REWARD_PRICE = "rewardPrice";
	public static final String STATUS = "status";

	public void save(ReportPdt transientInstance) {
		log.debug("saving ReportPdt instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(ReportPdt persistentInstance) {
		log.debug("deleting ReportPdt instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ReportPdt findById(java.lang.Integer id) {
		log.debug("getting ReportPdt instance with id: " + id);
		try {
			ReportPdt instance = (ReportPdt) getSession().get(
					"com.kyad.selluv.dbconnect.ReportPdt", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(ReportPdt instance) {
		log.debug("finding ReportPdt instance by example");
		try {
			List results = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.ReportPdt")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding ReportPdt instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from ReportPdt as model where model."
					+ propertyName + "= '" + value + "'";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsrUid(Object usrUid) {
		return findByProperty(USR_UID, usrUid);
	}

	public List findByPdtUid(Object pdtUid) {
		return findByProperty(PDT_UID, pdtUid);
	}

	public List findByRank(Object rank) {
		return findByProperty(RANK, rank);
	}

	public List findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List findByRewardYn(Object rewardYn) {
		return findByProperty(REWARD_YN, rewardYn);
	}

	public List findByRewardPrice(Object rewardPrice) {
		return findByProperty(REWARD_PRICE, rewardPrice);
	}

	public List findByStatus(Object status) {
		return findByProperty(STATUS, status);
	}

	public List findAll() {
		log.debug("finding all ReportPdt instances");
		try {
			String queryString = "from ReportPdt";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public ReportPdt merge(ReportPdt detachedInstance) {
		log.debug("merging ReportPdt instance");
		try {
			ReportPdt result = (ReportPdt) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(ReportPdt instance) {
		log.debug("attaching dirty ReportPdt instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ReportPdt instance) {
		log.debug("attaching clean ReportPdt instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public int getCount() {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.ReportPdt")
					.setProjection(Projections.rowCount())
					.add(Restrictions.not(Restrictions.eq(STATUS, 0)));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public int getCountByStatus(int status) {
		try {
			Criteria crit = getSession()
					.createCriteria("com.kyad.selluv.dbconnect.ReportPdt")
					.setProjection(Projections.rowCount())
					.add(Restrictions.eq(STATUS, status));
			Long result = (Long)crit.uniqueResult();			
			return (result == null ? 0 : result.intValue());
		} catch (RuntimeException re) {
			throw re;
		}
	}
}