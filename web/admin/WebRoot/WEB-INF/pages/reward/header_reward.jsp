<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="page-bar">
	<div class="header_title">
		<strong><span style="font-size: 20px; color: black;">리워드</span></strong>
	</div>
	<ul class="nav nav-tabs">
		<li>
			<a href="<c:url value="/reward/report_reward_mng"/>" style="cursor: pointer;">
			가품 </a>
		</li>
		<li>
			<a href="<c:url value="/reward/share_reward_mng"/>" style="cursor: pointer;">
			공유 </a>
		</li>
		<li>
			<a href="<c:url value="/reward/code_reward_mng"/>" style="cursor: pointer;">
			코드 </a>
		</li>
		<li>
			<a href="<c:url value="/reward/invite_reward_mng"/>" style="cursor: pointer;">
			초대 </a>
		</li>
		<li>
			<a href="<c:url value="/reward/set_reward_mng"/>" style="cursor: pointer;">
			설정 </a>
		</li>
	</ul>
</div>

<script>
	$(document)
	  .ajaxStart(function() {
	    loadingOverlay();
	}).ajaxStop(function() {
	    loadingOverlayRemove();
	});
</script>
