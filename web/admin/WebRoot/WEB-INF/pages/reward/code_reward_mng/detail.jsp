<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	height: 53px;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/reward/report_reward_mng"/>">리워드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/reward/code_reward_mng"/>" class="page_nav">프로모션코드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		<c:choose>
			<c:when test="${pcode == null }">
				&nbsp;&nbsp;<a href="#" class="page_nav">프로모션코드생성</a>
			</c:when>
			<c:otherwise>
				&nbsp;&nbsp;<a href="#" class="page_nav">프로모션코드수정</a>
			</c:otherwise>
		</c:choose>
	</div>
	<c:choose>
		<c:when test="${pcode == null }">
		</c:when>
		<c:otherwise>
			<div class="col-md-offset-5 col-md-1">
				<a class="btn btn-red-inline btn-block" onclick="onDelete(${pcode.promotionCodeUid})">삭제</a>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<c:choose>
	<c:when test="${pcode == null }">
		<div class="row" style="margin-top: 20px;">
			<h4><strong>프로모션코드 생성</strong></h4>
		</div>
	</c:when>
	<c:otherwise>
		<div class="row">
			<h4><strong>프로모션코드 수정</strong></h4>
		</div>
	</c:otherwise>
</c:choose>

<div class="row">
	<form id="pcode_form" method="post">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">코드분류</td>
				<td colspan="2">
					<div class="col-md-5">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default col-md-6 <c:if test="${pcode.kind == 1 }">active</c:if>">
							<input type="radio" class="toggle kinds" name="kinds" value="1"> 구매용 </label>
							<label class="btn btn-default col-md-6 <c:if test="${pcode.kind == 2 }">active</c:if>">
							<input type="radio" class="toggle kinds" name="kinds" value="2"> 판매용 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">코드명</td>
				<td colspan="2">
					<div class="col-md-6">
						<input type="text" class="form-control" id="title" name="title" value="${pcode.title }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">혜택</td>
				<td colspan="2">
					<div class="col-md-3">
						<input type="text" class="form-control" id="price" name="price" value="${pcode.price }"/>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="price_unit" name="price_unit">
							<option value="1" <c:if test="${pcode.priceUnit == 1 }">selected</c:if>>% 할인</option>
							<option value="2" <c:if test="${pcode.priceUnit == 2 }">selected</c:if>>￦ 할인</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">유효기간</td>
				<td colspan="2">
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from" value="${start_time }">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to" value="${end_time }">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">혜택메모</td>
				<td colspan="2">
					<div class="col-md-12">
						<input type="text" class="form-control" id="memo" name="memo" value="${pcode.memo }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label" rowspan="5">사용범위</td>
				<td class="td-label">
					<label class="control-label" style="padding-top: 5px; width: 170px;">
		    			<input class="limit_types" type="radio" name="limit_types" value="0" <c:if test="${pcode.limitType == 0 }">checked</c:if>/>&nbsp;제한 없음
		    		</label>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td class="td-label">
					<label class="control-label" style="padding-top: 5px;">
		    			<input class="limit_types" type="radio" name="limit_types" value="1" <c:if test="${pcode.limitType == 1 }">checked</c:if>/>&nbsp;카테고리 제한
		    		</label>
				</td>
				<td>
					<div id="category_pad" <c:if test="${pcode.limitType != 1 }">style="display: none;"</c:if>>
					<div class="col-md-2">
						<a class="btn btn-dark btn-block" onclick="onCategorySelect()">카테고리선택</a>
					</div>
					<c:if test="${pcode.limitType == 1 }">
						<c:forEach var="uid" items="${limit_uids}" varStatus="vstatus">
	                        <div class="col-md-2" style="padding-top: 10px;">
	                        	<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i>
	                        	${uid }
	                        	<input type="hidden" class="uids_category" value="${limit_real_uids[vstatus.index] }"/>
	                        </div>
	                    </c:forEach>
                    </c:if>
                    </div>
				</td>
			</tr>
			<tr>
				<td class="td-label">
					<label class="control-label" style="padding-top: 5px;">
		    			<input class="limit_types" type="radio" name="limit_types" value="2" <c:if test="${pcode.limitType == 2 }">checked</c:if>/>&nbsp;브랜드 제한
		    		</label>
				</td>
				<td>
					<div id="brand_pad" <c:if test="${pcode.limitType != 2 }">style="display: none;"</c:if>>
					<div class="col-md-2">
						<a class="btn btn-dark btn-block" onclick="onBrandSelect()">브랜드선택</a>
					</div>
					<c:if test="${pcode.limitType == 2 }">
						<c:forEach var="uid" items="${limit_uids}" varStatus="vstatus">
	                        <div class="col-md-2" style="padding-top: 10px;">
	                        	<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i>
	                        	${uid }
	                        	<input type="hidden" class="uids_brand" value="${limit_real_uids[vstatus.index] }"/>
	                        </div>
	                    </c:forEach>
                    </c:if>
                    </div>
				</td>
			</tr>
			<tr>
				<td class="td-label">
					<label class="control-label" style="padding-top: 5px;">
		    			<input class="limit_types" type="radio" name="limit_types" value="3" <c:if test="${pcode.limitType == 3 }">checked</c:if>/>&nbsp;회원 제한
		    		</label>
				</td>
				<td>
					<div id="usr_pad" <c:if test="${pcode.limitType != 3 }">style="display: none;"</c:if>>
					<div class="col-md-2">
						<a class="btn btn-dark btn-block" onclick="onUsrSelect()">회원선택</a>
					</div>
					<c:if test="${pcode.limitType == 3 }">
						<c:forEach var="uid" items="${limit_uids}" varStatus="vstatus">
	                        <div class="col-md-2" style="padding-top: 10px;">
	                        	<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i>
	                        	${uid }
	                        	<input type="hidden" class="uids_usr" value="${limit_real_uids[vstatus.index] }"/>
	                        </div>
	                    </c:forEach>
                    </c:if>
                    </div>
				</td>
			</tr>
			<tr>
				<td class="td-label">
					<label class="control-label" style="padding-top: 5px;">
		    			<input class="limit_types" type="radio" name="limit_types" value="4" <c:if test="${pcode.limitType == 4 }">checked</c:if>/>&nbsp;상품 제한
		    		</label>
				</td>
				<td>
					<div id="pdt_pad" <c:if test="${pcode.limitType != 4 }">style="display: none;"</c:if>>
					<div class="col-md-2">
						<a class="btn btn-dark btn-block" onclick="onPdtSelect()">상품선택</a>
					</div>
					<c:if test="${pcode.limitType == 4 }">
						<c:forEach var="uid" items="${limit_uids}" varStatus="vstatus">
	                        <div class="col-md-2" style="padding-top: 10px;">
	                        	<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i>
	                        	${uid }
	                        	<input type="hidden" class="uids_pdt" value="${limit_real_uids[vstatus.index] }"/>
	                        </div>
	                    </c:forEach>
                    </c:if>
                    </div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="kind" name="kind" value="${pcode == null ? 0 : pcode.kind }"/>
	<input type="hidden" id="limit_uids" name="limit_uids"/>
	<input type="hidden" id="limit_type" name="limit_type" value="${pcode.limitType }"/>
	<input type="hidden" id="promotionCodeUid" name="promotionCodeUid" value="${pcode == null ? 0 : pcode.promotionCodeUid }"/>
	</form>
	<div class="row">
		<div class="col-md-1">
			<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
		</div>
		<div class="col-md-offset-5 col-md-1">
			<a class="btn btn-dark btn-block" onclick="onSave()">
				<c:choose>
					<c:when test="${pcode == null }">
						코드생성
					</c:when>
					<c:otherwise>
						수정
					</c:otherwise>
				</c:choose>
			</a>
		</div>
	</div>
</div>

<a class="btn default hidden" data-toggle="modal" href="#category_modal" id="btn_category_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="category_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>카테고리 선택</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black;">카테고리</td>
								<td style="border-right: none; border-top-color: black;">
									<div class="col-md-3">
										<select class="form-control select" id="limit_pdt_group" name="limit_pdt_group" onchange="onLimitPdtGroupChange($(this).val())">
											<option value="1">남성</option>
											<option value="2">여성</option>
											<option value="4">키즈</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="limit_category1" name="limit_category1" onchange="onLimitCategory1Change($(this).val())">
											<c:forEach var="category1" items="${category1_list}" varStatus="vstatus">
				                            	<option value="${category1.categoryUid }">${category1.categoryName }</option>
				                        	</c:forEach>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="limit_category2" name="limit_category2" onchange="onLimitCategory2Change($(this).val())">
											<c:forEach var="category2" items="${category2_list}" varStatus="vstatus">
				                            	<option value="${category2.categoryUid }">${category2.categoryName }</option>
				                        	</c:forEach>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="limit_category3" name="limit_category3">
											<c:forEach var="category3" items="${category3_list}" varStatus="vstatus">
				                            	<option value="${category3.categoryUid }">${category3.categoryName }</option>
				                        	</c:forEach>
										</select>
									</div>
								</td>
								<td width="15%" style="border-left: none; border-top-color: black;">
									<div class="col-md-12">
										<a class="btn btn-dark btn-block" onclick="onCategoryAdd()"><i class="fa fa-plus"></i>&nbsp;&nbsp;추가</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<a class="btn default hidden" data-toggle="modal" href="#large" id="btn_brand_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">브랜드검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="brand_keyword" name="brand_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onBrandSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">색인</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="first_en" name="first_en">
											<option value="">A-Z</option>
											<option value="a">a</option>
											<option value="b">b</option>
											<option value="c">c</option>
											<option value="d">d</option>
											<option value="e">e</option>
											<option value="f">f</option>
											<option value="g">g</option>
											<option value="h">h</option>
											<option value="i">i</option>
											<option value="j">j</option>
											<option value="k">k</option>
											<option value="l">l</option>
											<option value="m">m</option>
											<option value="n">n</option>
											<option value="o">o</option>
											<option value="p">p</option>
											<option value="q">q</option>
											<option value="r">r</option>
											<option value="s">s</option>
											<option value="t">t</option>
											<option value="u">u</option>
											<option value="v">v</option>
											<option value="w">w</option>
											<option value="x">x</option>
											<option value="y">y</option>
											<option value="z">z</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="first_ko" name="first_ko">
											<option value="">ㄱ-ㅎ</option>
											<option value="ㄱ">ㄱ</option>
											<option value="ㄴ">ㄴ</option>
											<option value="ㄷ">ㄷ</option>
											<option value="ㄹ">ㄹ</option>
											<option value="ㅁ">ㅁ</option>
											<option value="ㅂ">ㅂ</option>
											<option value="ㅅ">ㅅ</option>
											<option value="ㅇ">ㅇ</option>
											<option value="ㅈ">ㅈ</option>
											<option value="ㅊ">ㅊ</option>
											<option value="ㅋ">ㅋ</option>
											<option value="ㅌ">ㅌ</option>
											<option value="ㅍ">ㅍ</option>
											<option value="ㅎ">ㅎ</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>브랜드 선택</strong></h4>
					</div>
					<div class="col-md-offset-8 col-md-2">
						<select class="form-control select" id="brand_length" name="brand_length" onchange="onBrandLengthChange()">
							<option value="100">100개 보기</option>
							<option value="500">500개 보기</option>
							<option value="-1">전체 보기</option>
						</select>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="brand_list" width="100%">
						<thead>
							<tr>
								<th>A-Z</th>
								<th>영문 브랜드명</th>
								<th>ㄱ-ㅎ</th>
								<th>한글 브랜드명</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<a class="btn default hidden" data-toggle="modal" href="#usr_modal" id="btn_usr_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="usr_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">유저검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-10">
										<input type="text" class="form-control" id="usr_keyword" name="usr_keyword"/>
									</div>
									<div class="col-md-2">
										<a class="btn btn-dark btn-block" onclick="onUsrSearch()">검색</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>유저 선택</strong></h4>
					</div>
					<div class="col-md-offset-8 col-md-2">
						<select class="form-control select" id="usr_length" name="usr_length" onchange="onUsrLengthChange()">
							<option value="100">100개 보기</option>
							<option value="500">500개 보기</option>
							<option value="-1">전체 보기</option>
						</select>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="usr_list" width="100%">
						<thead>
							<tr>
								<th>아이디</th>
								<th>닉네임</th>
								<th>실명</th>
								<th>핸드폰번호</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<a class="btn default hidden" data-toggle="modal" href="#pdt_modal" id="btn_pdt_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="pdt_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="pdt_keyword" name="pdt_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onPdtSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">카테고리</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="pdt_group" name="pdt_group" onchange="onPdtGroupChange($(this).val())">
											<option value="0">전체</option>
											<option value="1">남성</option>
											<option value="2">여성</option>
											<option value="4">키즈</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category1" name="category1" onchange="onCategory1Change($(this).val())">
											<option value="0">전체</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category2" name="category2" onchange="onCategory2Change($(this).val())">
											<option value="0">전체</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category3" name="category3">
											<option value="0">전체</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>상품추가</strong></h4>
					</div>
				</div>
				<div class="row" style="height: 300px; overflow-y: auto;">
					<table class="table table-bordered" id="pdt_list" width="100%">
						<thead>
							<tr>
								<th>코드</th>
								<th>이미지</th>
								<th>상품명</th>
								<th>판매자</th>
								<th>판매가</th>
								<th>추가</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	var ajax_brand_table;
	var ajax_usr_table;
	var ajax_pdt_table;
	var brand_keyword = '';
	var first_en = '';
	var first_ko = '';
	var usr_keyword = '';
	var pdt_keyword = '';
	var pdt_group = 0;
	var category1 = 0;
	var category2 = 0;
	var category3 = 0;
	var ajax_pdt_table;
	$(document).ready(function () {
		$("#menu_reward").addClass("active");
		$("#menu_reward li:eq(2)").addClass("active");
		$(".nav-tabs li:eq(2)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 리워드입니다.");
			onBack();
		}
		
		$('.control-label input[type="radio"]').iCheck({
            checkboxClass: 'iradio_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
		$('.date-picker').datepicker({
    		container: 'html',
    		autoclose: true
        });
        
        ajax_brand_table = $("#brand_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_brand_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#brand_length").val();
                    d.keyword = brand_keyword;
                    d.first_ko = first_ko;
                    d.first_en = first_en;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(4)', row).html('선택');
           		$('td:eq(4)', row).css('cursor', 'pointer');
           		$('td:eq(4)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(4)', row).attr('onclick', 'onBrandSelectEach("' + data[1] + '", "' + data[4] + '")');
           	}
		});
		
		ajax_usr_table = $("#usr_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_usr_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#usr_length").val();
                    d.keyword = usr_keyword;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(4)', row).html('선택');
           		$('td:eq(4)', row).css('cursor', 'pointer');
           		$('td:eq(4)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(4)', row).attr('onclick', 'onUsrSelectEach("' + data[0] + '", "' + data[4] + '")');
           	}
		});
		
		ajax_pdt_table = $("#pdt_list").DataTable({
			dom : 't',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "paging": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.keyword = pdt_keyword;
                    d.pdt_group = pdt_group;
                    d.category1 = category1;
                    d.category2 = category2;
                    d.category3 = category3;
                    d.status = 1;
                }
            },
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(1)', row).html('');
           		$('td:eq(1)', row).html('<img src="' + data[13] + '/pdt/' + data[0] + '/' + data[1] + '" width="100px" height="100px"/>');
           		var pdt_group_str = '';
           		if (data[10] == 1)
           			pdt_group_str = '남성';
           		else if (data[10] == 2)
           			pdt_group_str = '여성';
           		else if (data[10] == 4)
           			pdt_group_str = '키즈';
          		$('td:eq(2)', row).css("text-align", "left");
           		$('td:eq(2)', row).html(data[9] + ' ' + pdt_group_str + ' ' + data[11] + ' ' + data[2] + ' ' + data[12]);
           		$('td:eq(5)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(5)', row).css('cursor', 'pointer');
           		$('td:eq(5)', row).html('추가');
           		$('td:eq(5)', row).attr('onclick', 'onPdtSelectEach(' + data[0] + ')');	
           		$('td:eq(0)', row).css('cursor', 'pointer');
           		$('td:eq(0)', row).html(uid_format(data[0]));
           		$('td:eq(0)', row).attr('onclick', 'onPdtDetail(' + data[0] + ')');
           	}
		});
	});
	
	function onSave() {
	//check
		if ($("#kind").val() == '0') {
			$toast = toastr['error']('', '코드분류를 선택하세요.');
			return;
		}
		
		if ($.trim($("#title").val()) == '') {
			$toast = toastr['error']('', '코드명을 입력하세요.');
			$("#title").focus();
			return;
		}
		
		if ($.trim($("#price").val()) == '') {
			$toast = toastr['error']('', '혜택을 입력하세요.');
			$("#price").focus();
			return;
		}
		
		if ($("#memo").val() == '') {
			$toast = toastr['error']('', '혜택메모를 입력하세요.');
			$("#memo").focus();
			return;
		}
		
		if ($("#limit_type").val() == '') {
			$toast = toastr['error']('', '사용범위를 선택하세요.');
			return;
		}
		
		var type = $("#limit_type").val();
		var limit_uids = '';
		if (type == 1) {
			$(".uids_category").each(function () {
				limit_uids += $(this).val() + ',';
			});
		} else if (type == 2) {
			$(".uids_brand").each(function () {
				limit_uids += $(this).val() + ',';
			});
		} else if (type == 3) {
			$(".uids_usr").each(function () {
				limit_uids += $(this).val() + ',';
			});
		} else if (type == 4) {
			$(".uids_pdt").each(function () {
				limit_uids += $(this).val() + ',';
			});
		}
		
		if (type > 0 && limit_uids == '') {
			$toast = toastr['error']('', '사용범위목록을 선택하세요.');
			return;
		}
		
		$("#limit_uids").val(limit_uids);
		
 		if($('#price_unit').val() < 2) {
 			if($('#price').val().match(/\D/g) != null ||$('#price').val() > 100) {
 				$toast = toastr['error']('', '%할인인 경우 혜택은 0~100까지의 수값범위에서 선택해야 합니다.');
 				return;
 			}
 		}
		
		$.post('<c:url value="/reward/code_reward_mng/"/>' + $("#promotionCodeUid").val() + '/save', $("#pcode_form").serialize(),
			function (result) {
				if (result == "success") {
					$toast = toastr['success']('', '성과적으로 저장되었습니다.');
					setTimeout('onBack()', 1000);
				} else {
					$toast = toastr['error']('', '실패했습니다.');
				}
			}
		);
	}
	
	function onDelete() {
		if (!confirm("선택한 자료를 삭제하시겠어요?"))
			return;
			
		$.post('<c:url value="/reward/code_reward_mng/"/>' + $("#promotionCodeUid").val() + '/remove', {},
			function (result) {
				if (result == "success") {
					$toast = toastr['success']('', '성과적으로 삭제되었습니다.');
					setTimeout('onBack()', 1000);
				} else {
					$toast = toastr['error']('', '실패했습니다.');
				}
			}
		);
	}
	
	function onUidDelete(obj) {
    	$(obj).parent('div').remove();
    }
    
    $(".limit_types").on('ifClicked', function (event) {
		$("#limit_type").val($(this).val());
    	if ($(this).val() == 0) {
    		$("#category_pad").fadeOut();
    		$("#brand_pad").fadeOut();
    		$("#usr_pad").fadeOut();
    		$("#pdt_pad").fadeOut();
    	} else if ($(this).val() == 1) {
    		$("#category_pad").fadeIn();
    		$("#brand_pad").fadeOut();
    		$("#usr_pad").fadeOut();
    		$("#pdt_pad").fadeOut();
    	} else if ($(this).val() == 2) {
    		$("#category_pad").fadeOut();
    		$("#brand_pad").fadeIn();
    		$("#usr_pad").fadeOut();
    		$("#pdt_pad").fadeOut();
    	} else if ($(this).val() == 3) {
    		$("#category_pad").fadeOut();
    		$("#brand_pad").fadeOut();
    		$("#usr_pad").fadeIn();
    		$("#pdt_pad").fadeOut();
    	} else if ($(this).val() == 4) {
    		$("#category_pad").fadeOut();
    		$("#brand_pad").fadeOut();
    		$("#usr_pad").fadeOut();
    		$("#pdt_pad").fadeIn();
    	}
    });
    
    $(".kinds").change(function () {
    	$("#kind").val($(this).val());
    });
    
    function onBrandSearch() {
		first_en = $("#first_en").val();
		first_ko = $("#first_ko").val();
		brand_keyword = $("#brand_keyword").val();
		ajax_brand_table.draw(false);
	}
    
    function onUsrSearch() {
		usr_keyword = $("#usr_keyword").val();
		ajax_usr_table.draw(false);
	}
	
	function onPdtSearch() {
		pdt_keyword = $("#pdt_keyword").val();
		pdt_group = $("#pdt_group").val();
		category1 = $("#category1").val();
		category2 = $("#category2").val();
		category3 = $("#category3").val();
		ajax_pdt_table.draw(false);
	}
    
    $("#brand_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onBrandSearch();
        }
    });
    
    $("#usr_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onUsrSearch();
        }
    });
    
	function onBrandLengthChange() {
		ajax_brand_table.draw(false);
	}
	
	function onUsrLengthChange() {
		ajax_usr_table.draw(false);
	}
	
	function onPdtLengthChange() {
		ajax_pdt_table.draw(false);
	}
	
	function onCategoryAdd() {
		var name_en = $("#limit_category3").find('option:selected').html();
		var id = $("#limit_category3").val();
		if (name_en == undefined) {
			name_en = $("#limit_category2").find('option:selected').html();
			id = $("#limit_category2").val();
		}
		if (name_en == undefined) {
			name_en = $("#limit_category1").find('option:selected').html();
			id = $("#limit_category1").val();
		}
		var flag = 0;
    	$('.uids_category').each(function () {
    		if ($(this).val() == id) {
    			flag = 1;
    			return;
    		}
    	});
    	if (flag) {
    		$toast = toastr['error']('그 카테고리는 이미 선택되여 있습니다.');
    		return;
    	}
    	var html = '<div class="col-md-2" style="padding-top: 10px;"> ' +
        	'<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i> ' +
        	name_en +
        	'<input type="hidden" class="uids_category" value="' + id + '"/> ' +
        '</div>';
        $("#category_pad").append(html);
    	$(".close").trigger('click');
    }
    
	function onBrandSelectEach(name_en, id) {
		var flag = 0;
    	$('.uids_brand').each(function () {
    		if ($(this).val() == id) {
    			flag = 1;
    			return;
    		}
    	});
    	if (flag) {
    		$toast = toastr['error']('그 브랜드는 이미 선택되여 있습니다.');
    		return;
    	}
    	var html = '<div class="col-md-2" style="padding-top: 10px;"> ' +
        	'<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i> ' +
        	name_en +
        	'<input type="hidden" class="uids_brand" value="' + id + '"/> ' +
        '</div>';
        $("#brand_pad").append(html);
    	$(".close").trigger('click');
    }
    
    function onUsrSelectEach(usr_id, id) {
    	var flag = 0;
    	$('.uids_usr').each(function () {
    		if ($(this).val() == id) {
    			flag = 1;
    			return;
    		}
    	});
    	if (flag) {
    		$toast = toastr['error']('그 유저는 이미 선택되여 있습니다.');
    		return;
    	}
    	var html = '<div class="col-md-2" style="padding-top: 10px;"> ' +
        	'<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i> ' +
        	usr_id +
        	'<input type="hidden" class="uids_usr" value="' + id + '"/> ' +
        '</div>';
        $("#usr_pad").append(html);
    	$(".close").trigger('click');
    }
	
    function onPdtSelectEach(id) {
    	var flag = 0;
    	$('.uids_pdt').each(function () {
    		if ($(this).val() == id) {
    			flag = 1;
    			return;
    		}
    	});
    	if (flag) {
    		$toast = toastr['error']('그 상품은 이미 선택되여 있습니다.');
    		return;
    	}
    	var html = '<div class="col-md-2" style="padding-top: 10px;"> ' +
        	'<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onUidDelete(this)"></i> ' +
        	uid_format(id) +
        	'<input type="hidden" class="uids_pdt" value="' + id + '"/> ' +
        '</div>';
        $("#pdt_pad").append(html);
    	$(".close").trigger('click');
    }
    
	function onCategorySelect() {
		onLimitPdtGroupChange(1);
    	$("#btn_category_modal").trigger('click');
    }
    
    function onBrandSelect() {
    	$("#btn_brand_modal").trigger('click');
    }
    
    function onUsrSelect() {
    	$("#btn_usr_modal").trigger('click');
    }
    
    function onPdtSelect() {
    	onPdtGroupChange(0);
    	$("#btn_pdt_modal").trigger('click');
    }
    
    function onPdtGroupChange(id) {
    	$('#category1').empty();
        $('#category1').append('<option value="0">전체</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category1').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory1Change(json[0].categoryUid);
	            else
	            	onCategory1Change(0);
	        });
	    } else {
	    	onCategory1Change(0);
	    }
	}
	
	function onCategory1Change(id) {
		$('#category2').empty();
        $('#category2').append('<option value="0">전체</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category2').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory2Change(json[0].categoryUid);
	            else
	            	onCategory2Change(0);
	        });
	   	} else {
	   		onCategory2Change(0);
	   	}
	}
	
	function onCategory2Change(id) {
		$('#category3').empty();
        $('#category3').append('<option value="0">전체</option>');
		if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category3').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	        });
	    }
	}
	
	function onLimitPdtGroupChange(id) {
		$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
            var json = result;
            $('#limit_category1').empty();
            for (var i = 0; i < json.length; i++) {
                $('#limit_category1').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
            }
            if (json.length > 0)
            	onLimitCategory1Change(json[0].categoryUid);
            else
            	onLimitCategory1Change(-1);
        });
	}
	
	function onLimitCategory1Change(id) {
		$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
            var json = result;
            $('#limit_category2').empty();
            for (var i = 0; i < json.length; i++) {
                $('#limit_category2').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
            }
            if (json.length > 0)
            	onLimitCategory2Change(json[0].categoryUid);
            else
            	onLimitCategory2Change(-1);
        });
	}
	
	function onLimitCategory2Change(id) {
		$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
            var json = result;
            $('#limit_category3').empty();
            for (var i = 0; i < json.length; i++) {
                $('#limit_category3').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
            }
        });
	}
	
	$(".period").change(function () {
		onPeriodChange2($(this).val());
	});
	
	function onPdtDetail(uid) {
		go_url("<c:url value="/pdt/pdt_mng/"/>" + uid + "/detail");
	}
	
	function onDelete(uid) {
		if (!confirm('정말 삭제하시겠습니까?'))
			return;
		location.href = "<c:url value="/reward/code_reward_mng/"/>" + uid + "/remove";
	}
</script>
