<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		color: RGB(89,89,89);
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/reward/report_reward_mng"/>">리워드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/reward/set_reward_mng"/>" class="page_nav">리워드설정</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>리워드설정</strong></h4>
</div>

<div class="row">
	<form id="set_reward_form" method="post" action="<c:url value="/reward/set_reward_mng/save"/>">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">구매적립</td>
				<td style="border-right: none; border-top-color: black;">
					<div class="col-md-4">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting1 }"/>
					</div>
					<div style="padding-top: 7px;">%</div>
				</td>
				<td colspan="2" style="border-top-color: black; border-left: none;">
				</td>
			</tr>
			<tr>
				<td class="td-label">구매후기리워드</td>
				<td>
					<div class="col-md-4">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting2 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
				<td class="td-label">판매후기리워드</td>
				<td>
					<div class="col-md-4">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting3 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">가품리워드</td>
				<td>
					<div class="col-md-4">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting4 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
				<td class="td-label">공유리워드</td>
				<td>
					<div class="col-md-4">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting5 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
			</tr>
			<tr>
				<td class="td-label" rowspan="3">초대리워드</td>
				<td colspan="3">
					<div class="col-md-3" style="padding-top: 7px;">
						초대자에게 지급되는 가입 리워드
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting6 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="col-md-3" style="padding-top: 7px;">
						가입자에게 지급되는 가입 리워드
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting7 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="col-md-3" style="padding-top: 7px;">
						초대자에게 가입자의 첫 구매 후 지급되는 리워드
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="setting" name="setting" value="${setting8 }"/>
					</div>
					<div style="padding-top: 7px;">원</div>
				</td>
			</tr>
		</tbody>
	</table>
	</form>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSave()">수정</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<script>
	
	$(document).ready(function () {
		$("#menu_reward").addClass("active");
		$("#menu_reward li:eq(4)").addClass("active");
		$(".nav-tabs li:eq(4)").addClass("active");
	});
	
	function onInitialize() {
		location.reload();
	}
	
	function onSave() {
		$.post('<c:url value="/reward/set_reward_mng/save"/>', $("#set_reward_form").serialize(), function (result) {
			if (result == "success") {
				$toast = toastr['success']('', '성과적으로 저장되었습니다.');
			} else {
				$toast = toastr['error']('', '실패했습니다.');
			}
		});
	}
</script>