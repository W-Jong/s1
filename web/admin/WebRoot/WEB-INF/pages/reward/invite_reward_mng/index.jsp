<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		color: RGB(89,89,89);
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/reward/report_reward_mng"/>">리워드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/reward/invite_reward_mng"/>" class="page_nav">초대리워드</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>검색</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">검색어</td>
				<td style="border-top-color: black;">
					<div class="col-md-5" style="padding-right: 39px;">
						<input type="text" class="form-control" id="keyword" name="keyword"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">지급일</td>
				<td>
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">지급유형</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="kind" type="radio" name="kind" id="kind_all" checked value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="kind" type="radio" name="kind" value="1"/>&nbsp;초대인에게 가입리워드&nbsp;&nbsp;
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="kind" type="radio" name="kind" value="2"/>&nbsp;가입자에게 가입리워드&nbsp;&nbsp;
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="kind" type="radio" name="kind" value="3"/>&nbsp;초대인에게 가입자의 첫구매 리워드
		    			</label>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSearch()">검색</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
		<h4><strong>목록</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 3px;">
	</div>
	<div class="col-md-offset-6 col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row">
	<table class="table table-bordered" id="invite_reward_list">
		<thead>
			<tr>
				<th>초대인</th>
				<th>가입자</th>
				<th>지급받은 유저</th>
				<th>주문번호</th>
				<th>지급유형</th>
				<th>지급 리워드</th>
				<th>지급일</th>
			</tr>
		</thead>
	</table>
</div>

<input type="hidden" id="total_count" value="${total_count }"/>

<script>
	var keyword = '';
	var period_from = '';
	var period_to = '';
	var ajax_table;
	var kind = 0;
	
	$(document).ready(function () {
		$("#menu_reward").addClass("active");
		$("#menu_reward li:eq(3)").addClass("active");
		$(".nav-tabs li:eq(3)").addClass("active");
		$('.control-label input[type="radio"]').iCheck({
            checkboxClass: 'iradio_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
        
        $('.date-picker').datepicker({
    		container: 'html',
    		autoclose: true
        });
        
        ajax_table = $("#invite_reward_list").DataTable({
			dom : 'itp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
               	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
                "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/reward/invite_reward_mng/ajax_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#length").val();
                    d.keyword = keyword;
                    d.period_from = period_from;
                    d.period_to = period_to;
                    d.kind = kind;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		if (data[3] == '0') {
           			$('td:eq(3)', row).html('-');
           		}
           		$('td:eq(0)', row).css('cursor', 'pointer');
           		$('td:eq(0)', row).attr('onclick', "go_url('<c:url value="/usr/usr_mng/"/>" + data[7] + "/detail')");
           		$('td:eq(1)', row).css('cursor', 'pointer');
           		$('td:eq(1)', row).attr('onclick', "go_url('<c:url value="/usr/usr_mng/"/>" + data[8] + "/detail')");
           		$('td:eq(2)', row).css('cursor', 'pointer');
           		$('td:eq(2)', row).attr('onclick', "go_url('<c:url value="/usr/usr_mng/"/>" + data[9] + "/detail')");
           		if (data[3] != '0') {
	           		$('td:eq(3)', row).css('cursor', 'pointer');
	           		$('td:eq(3)', row).html(uid_format(data[3]));
	           		$('td:eq(3)', row).attr('onclick', "go_url('<c:url value="/deal/deal_mng/"/>" + data[3] + "/detail')");
	           	}
           	}
		});
		
		$("#table_info").append($("#invite_reward_list_info"));
	});
	
	function onSearch() {
		keyword = $("#keyword").val();
		period_from = $("#period_from").val();
		period_to = $("#period_to").val();
		ajax_table.draw(false);
	}
	
	$(".kind").on('ifClicked', function (event) {	
		kind = $(this).val();
	});
	
	function onLengthChange() {
		ajax_table.draw(false);
	}
	
	$(".period").change(function () {
		onPeriodChange($(this).val());
		ajax_table.draw(false);
	});
	
	function onInitialize() {
		$("#keyword").val("");
		$("#period_from").val("");
		$("#period_to").val("");
		$("#period_all").trigger('click');
		$("#kind_all").iCheck("check");
		kind = 0;
		onSearch();
	}
	
	$("#keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onSearch();
        }
    });
    function onUsrDetail(id) {
    	go_url("<c:url value="/usr/usr_mng/"/>" + id + "/detail");
    }
</script>