<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .td-label-modal {
    	background: #ebeef3 !important;
    	width: 15%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		color: RGB(89,89,89);
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/reward/report_reward_mng"/>">리워드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/reward/share_reward_mng"/>" class="page_nav">공유리워드</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>검색</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">검색어</td>
				<td style="border-top-color: black;">
					<div class="col-md-5" style="padding-right: 39px;">
						<input type="text" class="form-control" id="keyword" name="keyword"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">등록일</td>
				<td>
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">처리</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="status" type="radio" name="status" id="status_all" <c:if test="${status==null }">checked</c:if> value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="status" type="radio" name="status" <c:if test="${status==2 }">checked</c:if> value="2"/>&nbsp;미처리
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="status" type="radio" name="status" value="1"/>&nbsp;처리
		    			</label>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSearch()">검색</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
		<h4><strong>목록</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 3px;">
	</div>
	<div class="col-md-offset-6 col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row">
	<table class="table table-bordered" id="share_reward_list">
		<thead>
			<tr>
				<th>신청자</th>
				<th>상품코드</th>
				<th>주문번호</th>
				<th>상품명</th>
				<th>SNS</th>
				<th>지급여부</th>
				<th>신청일</th>
				<th>상태변경</th>
			</tr>
		</thead>
	</table>
</div>

<input type="hidden" id="total_count" value="${total_count }"/>

<a class="btn default hidden" data-toggle="modal" href="#detail_modal" id="btn_detail_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="detail_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>공유리워드 신청상세</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label-modal">신청자</td>
								<td width="35%">
									<div class="col-md-12" id="reporter"></div>
								</td>
								<td class="td-label-modal">신청일</td>
								<td>
									<div class="col-md-12" id="reg_time"></div>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">상품코드</td>
								<td width="35%">
									<div class="col-md-12" id="pdt_uid"></div>
								</td>
								<td class="td-label-modal">주문번호</td>
								<td>
									<div class="col-md-12" id="deal_uid"></div>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">상품명</td>
								<td colspan="3">
									<div class="col-md-12" id="pdt_name"></div>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">
									인스타그램
									<div class="row" style="margin-top: 10px;">
									<input class="sns" type="checkbox" id="instagram_yn"/>
									</div>
								</td>
								<td width="35%">
									<c:forEach var="photo" items="${instagram_list}" varStatus="vstatus">
										<div class="col-md-2">
				                   			<img src="${photo }" width="150px" height="150px"></img>
		                            	</div>
		                            </c:forEach>
								</td>
								<td class="td-label-modal">
									트위터
									<div class="row" style="margin-top: 10px;">
									<input class="sns" type="checkbox" id="twitter_yn"/>
									</div>
								</td>
								<td>
									<c:forEach var="photo" items="${twitter_list}" varStatus="vstatus">
										<div class="col-md-2">
				                   			<img src="${photo }" width="150px" height="150px"></img>
		                            	</div>
		                            </c:forEach>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">
									네이버블로그
									<div class="row" style="margin-top: 10px;">
									<input class="sns" type="checkbox" id="naverblog_yn"/>
									</div>
								</td>
								<td width="35%">
									<c:forEach var="photo" items="${naverblog_list}" varStatus="vstatus">
										<div class="col-md-2">
				                   			<img src="${photo }" width="150px" height="150px"></img>
		                            	</div>
		                            </c:forEach>
								</td>
								<td class="td-label-modal">
									카카오스토리
									<div class="row" style="margin-top: 10px;">
									<input class="sns" type="checkbox" id="kakaostory_yn"/>
									</div>
								</td>
								<td>
									<c:forEach var="photo" items="${kakaostory_list}" varStatus="vstatus">
										<div class="col-md-2">
				                   			<img src="${photo }" width="150px" height="150px"></img>
		                            	</div>
		                            </c:forEach>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">
									페이스북
									<div class="row" style="margin-top: 10px;">
									<input class="sns" type="checkbox" id="facebook_yn"/>
									</div>
								</td>
								<td colspan="3">
									<c:forEach var="photo" items="${facebook_list}" varStatus="vstatus">
										<div class="col-md-2">
				                   			<img src="${photo }" width="150px" height="150px"></img>
		                            	</div>
		                            </c:forEach>
								</td>
							</tr>
							<tr>
								<td colspan="4" style="padding-top: 20px; padding-bottom: 20px;">
									<h4 class="modal-title" style="padding-left: 5px;"><strong>공유 리워드 지급</strong></h4>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">지급</td>
								<td colspan="3">
									<div class="col-md-3">
										<a class="btn btn-blue btn-block" onclick="onRewardYes()">리워드 지급하기</a>
									</div>
									<div class="col-md-9" id="reward_yes_pad" style="padding-top: 9px;">
									
									</div>
								</td>
							</tr>
							<tr>
								<td class="td-label-modal">미지급</td>
								<td colspan="3">
									<div class="col-md-3">
										<a class="btn btn-blue-inline btn-block" onclick="onRewardNo()">지급하지 않기</a>
									</div>
									<div class="col-md-9" id="reward_no_pad" style="padding-top: 9px;">
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div class="row" style="margin-bottom: 30px;">
					<div class="col-md-offset-5 col-md-2">
						<a class="btn btn-dark btn-block" id="btn_confirm">확인</a>
					</div>
				</div>
				<input type="hidden" id="status" name="status"/>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	var keyword = '';
	var period_from = '';
	var period_to = '';
	var status = ${status==null?0:status};
	var ajax_table;
	var reward_status;
	var share_uid;
	
	$(document).ready(function () {
		$("#menu_reward").addClass("active");
		$("#menu_reward li:eq(1)").addClass("active");
		$(".nav-tabs li:eq(1)").addClass("active");
		$('.control-label input[type="radio"]').iCheck({
            checkboxClass: 'iradio_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
        
        $('.sns').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'icheckbox_minimal-blue',
            increaseArea: '20%' // optional
        });
        
        $('.sns').parent('div').removeClass("disabled");
        
        $('.date-picker').datepicker({
    		container: 'html',
    		autoclose: true
        });
        
        ajax_table = $("#share_reward_list").DataTable({
			dom : 'itp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
               	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
                "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/reward/share_reward_mng/ajax_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#length").val();
                    d.keyword = keyword;
                    d.period_from = period_from;
                    d.period_to = period_to;
                    d.status = status;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(0)', row).css('cursor', 'pointer');
           		$('td:eq(0)', row).attr('onclick', 'onUsrDetail(' + data[9] + ')');
           		$('td:eq(1)', row).css('cursor', 'pointer');
           		$('td:eq(1)', row).attr('onclick', 'onPdtDetail(' + data[1] + ')');
           		$('td:eq(1)', row).html(uid_format(data[1]));
           		$('td:eq(2)', row).css('cursor', 'pointer');
           		$('td:eq(2)', row).html(uid_format(data[2]));
           		$('td:eq(2)', row).attr('onclick', 'onDealDetail(' + data[2] + ')');
           		if (data[7] == 1) {
	           		$('td:eq(7)', row).css('background', 'RGB(242,242,242)');
	           		$('td:eq(7)', row).html('완료');
           		} else {
           			$('td:eq(7)', row).css('background', '#5d9bfc');
	           		$('td:eq(7)', row).html('미처리');
	           		$('td:eq(7)', row).css('color', 'white');
           		}
           		$('td:eq(7)', row).css('cursor', 'pointer');
           		$('td:eq(7)', row).attr('onclick', 'onDetail(' + data[8] + ')');
           	}
		});
		
		$("#table_info").append($("#share_reward_list_info"));
	});
	
	function onSearch() {
		keyword = $("#keyword").val();
		period_from = $("#period_from").val();
		period_to = $("#period_to").val();
		ajax_table.draw(false);
	}
	
	function onLengthChange() {
		ajax_table.draw(false);
	}
	
	$(".period").change(function () {
		onPeriodChange($(this).val());
		ajax_table.draw(false);
	});
	
	$(".status").on('ifClicked', function (event) {	
		status = $(this).val();
	});
	
	function onInitialize() {
		$("#keyword").val("");
		$("#period_from").val("");
		$("#period_to").val("");
		$("#period_all").trigger('click');
		$("#status_all").iCheck("check");
		status = 0;
		onSearch();
	}
	
	$("#keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onSearch();
        }
    });
    
    function onDetail(id) {
    	share_uid = id;
    	$.post('<c:url value="/reward/share_reward_mng/"/>' + id + '/detail', {shareUid: id},
    		function (result) {
    			var json = result;
    			$("#reporter").html(json.reporter);
    			$("#reg_time").html(json.reg_time);
    			$("#pdt_uid").html(uid_format(json.pdt_uid));
    			$("#deal_uid").html(uid_format(json.deal_uid));
    			$("#pdt_name").html(json.pdt_name);
    			if (json.instagram_yn == '1') {
    				$("#instagram_yn").iCheck("check");
    			}
    			if (json.twitter_yn == '1') {
    				$("#twitter_yn").iCheck("check");
    			}
    			if (json.naverblog_yn == '1') {
    				$("#naverblog_yn").iCheck("check");
    			}
    			if (json.kakaostory_yn == '1') {
    				$("#kakaostory_yn").iCheck("check");
    			}
    			if (json.facebook_yn == '1') {
    				$("#facebook_yn").iCheck("check");
    			}
    			var sns = json.sns;
    			var sns_str = json.sns_str;
    			$("#status").val(json.status);
    			reward_status = json.reward_yn;
    			if (json.status == 1) {
    				$(".sns").attr("disabled", "disabled");
    				if (reward_status == 1) {
	    				$("#reward_yes_pad").html('<span style="color: red;">총 ' + json.sns + '개의 공유 리워드' + json.sns_str + '가 ' + json.reporter + '에게 지급되었습니다. (' + json.reg_time + ')</span>');
	    				$("#reward_no_pad").html("");
	    			} else {
	    				$("#reward_no_pad").html('<span style="color: red;">공유 리워드를 지급하지 않는 것으로 처리되었습니다. (' + json.reg_time + ')</span>');
	    				$("#reward_yes_pad").html("");
	    			}
    			} else {
    				$(".sns").removeAttr("disabled", "disabled");
    				$("#reward_yes_pad").html("");
	    			$("#reward_no_pad").html("");
    			}
    		}
    	);
    	$("#btn_detail_modal").trigger('click');
    }
    
    function onUsrDetail(id) {
    	go_url("<c:url value="/usr/usr_mng/"/>" + id + "/detail");
    }
    
    function onPdtDetail(id) {
    	go_url("<c:url value="/pdt/pdt_mng/"/>" + id + "/detail");
    }
    
    function onDealDetail(id) {
    	go_url("<c:url value="/deal/deal_mng/"/>" + id + "/detail");
    }
    
    $("#btn_confirm").click(function () {
    	if ($("#status").val() == 1) {
    		$(".close").trigger('click');
    		return;
    	}
    	var twitter_yn = 0, naverblog_yn = 0, kakaostory_yn = 0, facebook_yn = 0,instagram_yn = 0;
		if ($("#instagram_yn").is(":checked")) {
    		instagram_yn = 1;
    	}
    	if ($("#twitter_yn").is(":checked")) {
    		twitter_yn = 1;
    	}
    	if ($("#naverblog_yn").is(":checked")) {
    		naverblog_yn = 1;
    	}
    	if ($("#kakaostory_yn").is(":checked")) {
    		kakaostory_yn = 1;
    	}
    	if ($("#facebook_yn").is(":checked")) {
    		facebook_yn = 1;
    	}
    	$.post('<c:url value="/reward/share_reward_mng/"/>' + share_uid + '/save', {shareUid: share_uid, 
	    	status: reward_status,instagram_yn: instagram_yn,twitter_yn: twitter_yn, naverblog_yn: naverblog_yn, kakaostory_yn: kakaostory_yn, facebook_yn: facebook_yn},
    		function (result) {
    			if (result == "success") {
    				if (reward_status == 1)
    					$toast = toastr['success']('', '정확히 지급되었습니다.');
    				else
			    		$toast = toastr['success']('', '지급하지 않은 것으로 처리되었습니다.');
			    	ajax_table.draw(false);
		   		} else {
		   			$toast = toastr['error']('', '실패했습니다.');
		   		}
		   		$(".close").trigger('click');
    		}
    	);
    });
    
    function onRewardYes() {
    	if ($("#status").val() == 1) {
    		$toast = toastr['error']('', '이미 처리완료된 상태입니다.');
    		return;
    	}
    	
		var sns = 0;
		var sns_str = '(';
    	if ($("#instagram_yn").is(":checked")) {
    		sns++;
    		sns_str += "인스타그램, ";
    	}
    	if ($("#twitter_yn").is(":checked")) {
    		sns++;
    		sns_str += "트위터, ";
    	}
    	if ($("#naverblog_yn").is(":checked")) {
    		sns++;
    		sns_str += "네이버블로그, ";
    	}
    	if ($("#kakaostory_yn").is(":checked")) {
    		sns++;
    		sns_str += "카카오스토리, ";
    	}
    	if ($("#facebook_yn").is(":checked")) {
    		sns++;
    		sns_str += "페이스북, ";
    	}

		sns_str = sns_str.substr(0, sns_str.length-2);
    	sns_str += ')';
		
    	reward_status = 1;
    	$.post('<c:url value="/reward/report_reward_mng/get_time"/>', {},
    		function (result) {
    			$("#reward_yes_pad").html('<span style="color: red;">총 ' + sns + '개의 공유 리워드' + sns_str + '가 ' + $("#reporter").html() + '에게 지급되었습니다. (' + result.time + ')</span>');
				$("#reward_no_pad").html("");
    		}
    	);
    }
    
    function onRewardNo() {
    	if ($("#status").val() == 1) {
    		$toast = toastr['error']('', '이미 처리완료된 상태입니다.');
    		return;
    	}
    	reward_status = 0;
    	$.post('<c:url value="/reward/report_reward_mng/get_time"/>', {},
    		function (result) {
			   	$("#reward_no_pad").html('<span style="color: red;">공유 리워드를 지급하지 않는 것으로 처리되었습니다. (' + result.time + ')</span>');
				$("#reward_yes_pad").html("");
			}
		);
    }
</script>