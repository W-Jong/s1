<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script src="<c:url value="/resources/global/plugins/amcharts/amcharts/amcharts.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/global/plugins/amcharts/amcharts/serial.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/global/plugins/amcharts/amcharts/themes/custom.js"/>" type="text/javascript"></script>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		color: RGB(89,89,89);
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/statistics/deal_statistics"/>">통계</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/statistics/deal_statistics"/>" class="page_nav">거래통계</a>
	</div>
</div>

<div class="row" style="margin-top: 20px; border-bottom: 1px solid black;">
	<div class="col-md-4">
		<h4><strong>거래건수 / 거래대금</strong></h4>
	</div>
	<div class="col-md-offset-5 col-md-3">
		<div class="btn-group" data-toggle="buttons" style="width: 100%;">
			<label class="btn btn-default col-md-3">
			<input type="radio" class="toggle period" name="period" value="1"> 일 </label>
			<label class="btn btn-default active col-md-3">
			<input type="radio" class="toggle period" name="period" value="2" checked> 주 </label>
			<label class="btn btn-default col-md-3">
			<input type="radio" class="toggle period" name="period" value="3"> 월 </label>
			<label class="btn btn-default col-md-3">
			<input type="radio" class="toggle period" name="period" value="4"> 년 </label>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div style="background: white; border: 1px solid RGB(217,217,217)">
		<div id="chart_1" class="chart" style="height: 500px;">
		</div>
		<div class="loading-overlay2">
	        <img class="spinner" src="<c:url value="/resources/admin/layout/img/ajax-loading.gif"/>">
	    </div>
		<div class="row" style="margin-bottom: 20px;">
			<div class="col-md-offset-4 col-md-1" style="height: 20px; background: #5d9bfc;"></div>
			<div class="col-md-1">거래</div>
			<div class="col-md-1" style="margin-top: 5px; height: 10px; background: #fc9d5d;"></div>
			<div class="col-md-1">거래대금</div>
		</div>
	</div>
</div>

<script>
	var chart;
	var period = 2;
	var graph_data;
	$(document).ready(function () {
		$("#menu_statistics").addClass("active");
		$("#menu_statistics li:eq(0)").addClass("active");
		$(".nav-tabs li:eq(0)").addClass("active");
		onDrawGraph(period);
	});
	
	$(".period").change(function () {
		period = $(this).val();
		onDrawGraph(period);
	});
		
	function onDrawGraph(period) {
		$.post('<c:url value="/statistics/deal_statistics/update"/>', {period: period}, function (result) {
			graph_data = result;
			chart = AmCharts.makeChart("chart_1", {
	            "type": "serial",
	            "theme": "custom",
	            "autoMargins": false,
	            "marginLeft": 50,
	            "marginRight": 50,
	            "marginTop": 30,
	            "marginBottom": 50,
	            "fontFamily": 'Open Sans',            
	            "color":    '#888',
	            "dataProvider": graph_data,
	            "valueAxes": [{
	            	"id": "countAxis",
	                "axisAlpha": 0,
	                "position": "left"
	            },
	            {
	            	"id": "priceAxis",
	                "axisAlpha": 0,
	                "position": "right",
	                "gridAlpha": 0
	            }],
	            "startDuration": 0,
	            "graphs": [{
	                "alphaField": "alpha",
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthColumn",
	                "fillAlphas": 1,
	                "title": "거래",
	                "type": "column",
	                "valueField": "count",
	                "valueAxis": "countAxis"
	            }, {
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthLine",
	                "lineThickness": 3,
	                "fillAlphas": 0,
	                "lineAlpha": 1,
	                "title": "거래대금",
	                "valueField": "price",
	                "valueAxis": "priceAxis"
	            }],
	            "categoryField": "time",
	            "categoryAxis": {
	                "gridPosition": "start",
	                "axisAlpha": 0,
	                "tickLength": 0,
	                "gridAlpha": 0
	            }
	        });
	        $("#chart_1").find('a').text("");
		});
	}
</script>