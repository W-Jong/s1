<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.loading-overlay2 {
         position: absolute;
         left: 0px;
         top: 0px;
         width: 100%;
         height: 100%;
         background: white;
         opacity: 0.5;
         filter: alpha(opacity=60);
         z-index: 10000;
         display: none;
     }
</style>

<div class="page-bar">
	<div class="header_title">
		<strong><span style="font-size: 20px; color: black;">통계</span></strong>
	</div>
	<ul class="nav nav-tabs">
		<li>
			<a href="<c:url value="/statistics/deal_statistics"/>" style="cursor: pointer;">
			거래 </a>
		</li>
		<li>
			<a href="<c:url value="/statistics/visit_statistics"/>" style="cursor: pointer;">
			방문 </a>
		</li>
		<li>
			<a href="<c:url value="/statistics/pdt_statistics"/>" style="cursor: pointer;">
			상품 </a>
		</li>
		<li>
			<a href="<c:url value="/statistics/usr_statistics"/>" style="cursor: pointer;">
			유저 </a>
		</li>
	</ul>
</div>

<script>
	$(document)
	  .ajaxStart(function() {
	    loadingOverlay2();
	}).ajaxStop(function() {
	    loadingOverlayRemove2();
	});
	
	var loadingOverlay2=function(){
        var winHeight=window.innerHeight;
        $('.loading-overlay2').show();
    }

    var loadingOverlayRemove2=function(){
        $('.loading-overlay2').hide();
    }
    
    $.ajaxSetup({
        timeout: 60000
    });
</script>
	