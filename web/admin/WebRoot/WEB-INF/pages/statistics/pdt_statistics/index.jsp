<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		color: RGB(89,89,89);
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/statistics/deal_statistics"/>">통계</a>&nbsp;&nbsp;
			<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<a href="<c:url value="/statistics/pdt_statistics"/>" class="page_nav">상품통계</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>설정</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">분류</td>
				<td style="border-top-color: black; border-right: none;" width="40%">
					<div class="col-md-11">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default active col-md-6">
							<input type="radio" class="toggle type" name="type" value="1" checked> 브랜드 </label>
							<label class="btn btn-default col-md-6">
							<input type="radio" class="toggle type" name="type" value="2"> 카테고리 </label>
						</div>
					</div>
				</td>
				<td colspan="2" style="border-left: none; border-top-color: black;"></td>
			</tr>
			<tr>
				<td class="td-label">구매자 설정</td>
				<td width="40%">
					<div class="col-md-11">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default active col-md-4">
							<input type="radio" class="toggle buyer" name="buyer" value="0" checked> 전체 </label>
							<label class="btn btn-default col-md-4">
							<input type="radio" class="toggle buyer" name="buyer" value="1"> 남성 구매자 </label>
							<label class="btn btn-default col-md-4">
							<input type="radio" class="toggle buyer" name="buyer" value="2"> 여성 구매자 </label>
						</div>
					</div>
				</td>
				<td class="td-label">기준 설정</td>
				<td>
					<div class="col-md-10">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default active col-md-6">
							<input type="radio" class="toggle standard" name="standard" value="1" checked> 거래건수 </label>
							<label class="btn btn-default col-md-6">
							<input type="radio" class="toggle standard" name="standard" value="2"> 거래대금 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">거래일 설정</td>
				<td colspan="3">
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0" checked> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-2">
		<a class="btn btn-dark btn-block" onclick="onSearch()">집계</a>
	</div>
</div>

<div class="col-md-12" style="padding: 0;">
<div id="brand_pad">
	<div class="row">
		<div class="col-md-1">
			<h4><strong>목록</strong></h4>
		</div>
		<div class="col-md-3" id="brand_table_info" style="padding-top: 3px;">
		</div>
		<div class="col-md-offset-6 col-md-2">
			<select class="form-control select" id="brand_length" name="brand_length" onchange="onBrandLengthChange()">
				<option value="100">100개 보기</option>
				<option value="500">500개 보기</option>
				<option value="-1">전체 보기</option>
			</select>
		</div>
	</div>
	
	<div class="col-md-12">
		<table class="table table-bordered" id="sum_brand_list">
			<thead>
				<tr>
					<th>순위</th>
					<th>브랜드</th>
					<th>거래건수</th>
					<th>거래대금</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="category_pad" style="display: none;">
	<div class="row">
		<div class="col-md-1">
			<h4><strong>목록</strong></h4>
		</div>
		<div class="col-md-3" id="category_table_info" style="padding-top: 3px;">
		</div>
		<div class="col-md-offset-6 col-md-2">
			<select class="form-control select" id="category_length" name="category_length" onchange="onCategoryLengthChange()">
				<option value="100">100개 보기</option>
				<option value="500">500개 보기</option>
				<option value="-1">전체 보기</option>
			</select>
		</div>
	</div>
	
	<div class="row">
		<table class="table table-bordered" id="sum_category_list">
			<thead>
				<tr>
					<th>순위</th>
					<th>카테고리</th>
					<th>거래건수</th>
					<th>거래대금</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div class="loading-overlay2">
    <img class="spinner" src="<c:url value="/resources/admin/layout/img/ajax-loading.gif"/>">
</div>
</div>
<input type="hidden" id="total_brand_count" value="${total_brand_count }"/>
<input type="hidden" id="total_category_count" value="${total_category_count }"/>

<script>
	var type = 1;
	var buyer = 0;
	var standard = 1;
	var period_from = '';
	var period_to = '';
	var ajax_brand_table;
	var ajax_category_table;
	
	$(document).ready(function () {
		$("#menu_statistics").addClass("active");
		$("#menu_statistics li:eq(2)").addClass("active");
		$(".nav-tabs li:eq(2)").addClass("active");
		$('.control-label input[type="radio"]').iCheck({
            checkboxClass: 'iradio_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
        
        $('.date-picker').datepicker({
    		container: 'html',
    		autoclose: true
        });
        
        ajax_brand_table = $("#sum_brand_list").DataTable({
			dom : 'itp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
               	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_brand_count").val() + "개",
                "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_brand_count").val() + "개",
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/statistics/pdt_statistics/ajax_brand_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#brand_length").val();
                    d.buyer = buyer;
                    d.standard = standard;
                    d.period_from = period_from;
                    d.period_to = period_to;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(0)', row).html(start_index + dataIndex + 1);
           		if (data[2] == '')
           			$('td:eq(2)', row).html('0');
           		if (data[3] == '')
           			$('td:eq(3)', row).html('0');
           	}
		});
		
		$("#brand_table_info").append($("#sum_brand_list_info"));
		
		ajax_category_table = $("#sum_category_list").DataTable({
			dom : 'itp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
               	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_category_count").val() + "개",
                "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_category_count").val() + "개",
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/statistics/pdt_statistics/ajax_category_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#category_length").val();
                    d.buyer = buyer;
                    d.standard = standard;
                    d.period_from = period_from;
                    d.period_to = period_to;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(0)', row).html(start_index + dataIndex + 1);
           		if (data[2] == '')
           			$('td:eq(2)', row).html('0');
           		if (data[3] == '')
           			$('td:eq(3)', row).html('0');
           	}
		});
		
		$("#category_table_info").append($("#sum_category_list_info"));
	});
	
	function onSearch() {
		period_from = $("#period_from").val();
		period_to = $("#period_to").val();
		if (type == 2) {
			$("#brand_pad").hide();
			$("#category_pad").show();
			ajax_category_table.draw(false);
		}
		else {
			$("#brand_pad").show();
			$("#category_pad").hide();
			ajax_brand_table.draw(false);
		}
	}
	
	function onBrandLengthChange() {
		ajax_brand_table.draw(false);
	}
	
	function onCategoryLengthChange() {
		ajax_category_table.draw(false);
	}
	
	$(".period").change(function () {
		onPeriodChange($(this).val());
	});
	
	$(".type").change(function () {
		type = $(this).val();
	});
	
	$(".standard").change(function () {
		standard = $(this).val();
	});
	
	$(".buyer").change(function () {
		buyer = $(this).val();
	});
</script>