<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/usr/usr_mng"/>">유저</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/usr/usr_mng"/>" class="page_nav">유저리스트</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="#" class="page_nav">유저상세</a>
	</div>
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-red-inline btn-block" onclick="remove_usr()">삭제</a>
	</div>
</div>

<div class="row">
	<h4><strong>유저상세</strong></h4>
</div>

<div class="row">
	<form id="usr_form" method="post" action="<c:url value="/usr/usr_mng/"/>${usr.usrUid}/save">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">아이디</td>
				<td width="40%">
					<div class="col-md-12">
						${usr.usrId }
					</div>
				</td>
				<td class="td-label">이메일</td>
				<td colspan="3">
					<div class="col-md-12">
						${usr.usrMail }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">실명</td>
				<td width="40%">
					<div class="col-md-12">
						${usr.usrNm }
					</div>
				</td>
				<td class="td-label">핸드폰</td>
				<td width="15%">
					<div class="col-md-12">
						${usr.usrPhone }
					</div>
				</td>
				<td class="td-label">닉네임</td>
				<td width="15%">
					<div class="col-md-12">
						${usr.usrNckNm }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소1</td>
				<td width="40%">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${address1 == null || address1.isEmpty() }">
								-
							</c:when>
							<c:otherwise>
								${address1 }
							</c:otherwise>
						</c:choose>
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${addressDetail1 == null || addressDetail1.isEmpty() }">
								-
							</c:when>
							<c:otherwise>
								${addressDetail1 }
							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소2</td>
				<td width="40%">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${address2 == null || address2.isEmpty() }">
								-
							</c:when>
							<c:otherwise>
								${address2 }
							</c:otherwise>
						</c:choose>
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${addressDetail2 == null || addressDetail2.isEmpty() }">
								-
							</c:when>
							<c:otherwise>
								${addressDetail2 }
							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소3</td>
				<td width="40%">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${address3 == null || address3.isEmpty() }">
								-
							</c:when>
							<c:otherwise>
								${address3 }
							</c:otherwise>
						</c:choose>
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${addressDetail3 == null || addressDetail3.isEmpty() }">
								-
							</c:when>
							<c:otherwise>
								${addressDetail3 }
							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">성별</td>
				<td width="40%">
					<div class="col-md-12">
						<c:if test="${usr.gender==1 }">남성</c:if>
						<c:if test="${usr.gender==2 }">여성</c:if>
					</div>
				</td>
				<td class="td-label">생년월일</td>
				<td width="15%">
					<div class="col-md-12">
						${birthday }
					</div>
				</td>
				<td class="td-label">SNS로그인</td>
				<td>
					<div class="col-md-12">
						${sns }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">최근방문</td>
				<td width="40%">
					<div class="col-md-12">
						${login_time }
					</div>
				</td>
				<td class="td-label">방문횟수</td>
				<td width="15%">
					<div class="col-md-12">
						${login_count }
					</div>
				</td>
				<td class="td-label">회원가입</td>
				<td>
					<div class="col-md-12">
						${reg_time }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">매너포인트</td>
				<td width="40%">
					<div class="col-md-12">
						${usr.point }
					</div>
				</td>
				<td class="td-label">구매건수</td>
				<td width="15%">
					<div class="col-md-12">
						${buy_count }
					</div>
				</td>
				<td class="td-label">판매건수</td>
				<td>
					<div class="col-md-12">
						${sell_count }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">셀럽머니</td>
				<td width="40%">
					<div class="col-md-12">
						${usr.money }
					</div>
				</td>
				<td class="td-label">결제금액</td>
				<td width="15%">
					<div class="col-md-12">
						${price }
					</div>
				</td>
				<td class="td-label">정산금액</td>
				<td>
					<div class="col-md-12">
						${cash_price }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">계좌정보</td>
				<td colspan="5">
					<div class="col-md-4">
						${usr.bankNm }&nbsp;${usr.accountNum }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">관리자메모</td>
				<td colspan="5">
					<div class="col-md-12">
						<textarea class="form-control" rows="5" id="memo" name="memo">${usr.memo }</textarea>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	</form>
</div>

<input type="hidden" id="usr_uid" name="usr_uid" value="${usr == null ? 0 : usr.usrUid }"/>

<div class="row">
	<div class="col-md-1">
		<h4><strong id="table_title">결제내역</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 11px;">
		총 ${pay_count }개
	</div>
	<div class="col-md-offset-4 col-md-2">
		<select class="form-control select" id="filter" name="filter" onchange="onFilterChange()">
			<option value="1">결제내역</option>
			<option value="2">정산내역</option>
			<option value="3">적립금내역</option>
		</select>
	</div>
	<div class="col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row" id="pay_pad">
	<table class="table table-bordered" id="pay_list">
		<thead>
			<tr>
				<th>주문번호</th>
				<th>상품코드</th>
				<th>결제금액</th>
				<th>결제방법</th>
				<th>결제일자</th>
			</tr>
		</thead>
	</table>
</div>

<div class="row" id="cash_pad" style="display: none;">
	<table class="table table-bordered" id="cash_list">
		<thead>
			<tr>
				<th>주문번호</th>
				<th>상품코드</th>
				<th>정산금액</th>
				<th>지급코드</th>
				<th>정산일자</th>
			</tr>
		</thead>
	</table>
</div>

<div class="row" id="money_pad" style="display: none;">
	<table class="table table-bordered" id="money_list">
		<thead>
			<tr>
				<th>주문번호</th>
				<th>상품코드</th>
				<th>적립금</th>
				<th>상세</th>
				<th>변동일자</th>
			</tr>
		</thead>
	</table>
</div>

<div class="row">
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
	</div>
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSave()">저장</a>
	</div>
</div>

<script>
	var type = 1;
	var ajax_pay_table;
	var ajax_cash_table;
	var ajax_money_table;
	$(document).ready(function () {
		$("#menu_usr").addClass("active");
		$("#menu_usr li:eq(0)").addClass("active");
		$(".nav-tabs li:eq(0)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 유저입니다.");
			onBack();
		}
		
		$('.date-picker').datepicker({
			container: 'html',
    		autoclose: true
        });
        
        ajax_pay_table = $("#pay_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	}
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/usr/usr_mng/ajax_pay_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#length").val();
                    d.usr_uid = $("#usr_uid").val();
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		if (data[3] == 1)
           			$('td:eq(3)', row).html('간편결제신용카드');
           		else if (data[3] == 2)
           			$('td:eq(3)', row).html('일반결제신용카드');
           		else if (data[3] == 3)
           			$('td:eq(3)', row).html('실시간계좌이체');
           		else
           			$('td:eq(3)', row).html('네이버페이');
           		$('td:eq(0)', row).css('cursor', 'pointer');
           		$('td:eq(0)', row).html(uid_format(data[0]));
           		$('td:eq(0)', row).attr('onclick', 'onDealDetail(' + data[0] + ')');
           		$('td:eq(1)', row).css('cursor', 'pointer');
           		$('td:eq(1)', row).attr('onclick', 'onPdtDetail(' + data[1] + ')');
           		$('td:eq(1)', row).html(uid_format(data[1]));
           	}
		});
		
		ajax_cash_table = $("#cash_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	}
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/usr/usr_mng/ajax_cash_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#length").val();
                    d.usr_uid = $("#usr_uid").val();
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(0)', row).css('cursor', 'pointer');
           		$('td:eq(0)', row).html(uid_format(data[0]));
           		$('td:eq(0)', row).attr('onclick', 'onDealDetail(' + data[0] + ')');
           		$('td:eq(1)', row).css('cursor', 'pointer');
           		$('td:eq(1)', row).attr('onclick', 'onPdtDetail(' + data[1] + ')');
           		$('td:eq(1)', row).html(uid_format(data[1]));	
           	}
		});
		
		ajax_money_table = $("#money_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	}
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/usr/usr_mng/ajax_money_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#length").val();
                    d.usr_uid = $("#usr_uid").val();
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		if (data[3] == 1 || data[3] == 2)
           			$('td:eq(3)', row).html('적립금 사용');
           		else if (data[3] == 3)
           			$('td:eq(3)', row).html('SNS공유 리워드');
           		else if (data[3] == 4)
           			$('td:eq(3)', row).html('가품신고 리워드');
           		else if (data[3] == 8)
           			$('td:eq(3)', row).html('판매후기 리워드');
           		else if (data[3] == 9)
           			$('td:eq(3)', row).html('구매후기 리워드');
           		else if (data[3] >= 5 && data[3] <= 7) {
           			$('td:eq(1)', row).html('-');
           			$('td:eq(3)', row).html('친구초대 리워드');
           			$('td:eq(0)', row).css('cursor', 'pointer');
           			$('td:eq(0)', row).attr('onclick', 'onUsrDetail(' + data[5] + ')');
           		}
           		
           		if (!(data[3] >= 5 && data[3] <= 7)) {
           			$('td:eq(1)', row).css('cursor', 'pointer');
           			$('td:eq(1)', row).attr('onclick', 'onPdtDetail(' + data[1] + ')');
           			$('td:eq(1)', row).html(uid_format(data[1]));
           		}
           		
           		if (data[2] > 0) {
           			$('td:eq(2)', row).html('+' + data[2]);
           		}
           		
           		if (data[0] != '-' && !(data[3]>=4 && data[3] <= 7)) {
           			$('td:eq(0)', row).css('cursor', 'pointer');
           			$('td:eq(0)', row).html(uid_format(data[0]));
           			$('td:eq(0)', row).attr('onclick', 'onDealDetail(' + data[0] + ')');
           		}
           	}
		});
	});
	
	$('.genders').change(function () {
		$("#gender").val($(this).val());
	});
	
	function onSave() {
		//check
		$.post('<c:url value="/usr/usr_mng/"/>' + $("#usr_uid").val() + '/save', {memo: $("#memo").val()}, function (result) {
			if (result == "success") {
				$toast = toastr['success']('', '성과적으로 저장되었습니다.');
				setTimeout('onBack()', 1000);
			} else {
				$toast = toastr['error']('', '실패했습니다.');
			}
		});
	}
	
	function onFilterChange() {
		type = $("#filter").val();
		if (type == 1) {
			$("#table_title").html('결제내역');
			$("#table_info").html('총 ${pay_count}개');
			$("#cash_pad").hide();
			$("#money_pad").hide();
			$("#pay_pad").show();
		} else if (type == 2) {
			$("#table_title").html('정산내역');
			$("#table_info").html('총 ${cash_count}개');
			$("#money_pad").hide();
			$("#pay_pad").hide();
			$("#cash_pad").show();
		} else {
			$("#table_title").html('적립금내역');
			$("#table_info").html('총 ${money_count}개');
			$("#cash_pad").hide();
			$("#pay_pad").hide();
			$("#money_pad").show();
		}
	}
	
	function onLengthChange() {
		if (type == 1)
			ajax_pay_table.draw(false);
		else if (type == 2)
			ajax_cash_table.draw(false);
		else
			ajax_money_table.draw(false);
	}
	
	function onPdtDetail(id) {
		if (id != 0) {
			go_url("<c:url value="/pdt/pdt_mng/"/>" + id + "/detail");
		}
	}
	
	function remove_usr() {
		if(!confirm('해당 유저를 삭제하시겠습니까?'))
			return;
		location.href = "<c:url value="/usr/usr_mng/"/>${usr.usrUid }/remove";
	}
	
	function onDealDetail(id) {
		if (id != 0) {
			go_url("<c:url value="/deal/deal_mng/"/>" + id + "/detail");
		}
	}
	
	function onUsrDetail(id) {
		if (id != 0) {
			go_url("<c:url value="/usr/usr_mng/"/>" + id + "/detail");
		}
	}	
</script>
