<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}

	.row {
		margin: 0 0;
	}

	.row > h4 {
		padding-left: 15px;
	}

    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }

    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }

    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }

    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }

    .table > thead > tr {
    	background: RGB(89,89,89);
    }

    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}

   	.table .btn {
	    margin-right: 0;
	}

	.input-group-btn {
		padding-left: 10px;
	}

	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}

	.select {
		border-radius: 8px !important;
	}

	.icon-calendar {
		color: RGB(166,166,166);
	}

	div.dataTables_paginate {
		float: none;
		text-align: center;
	}

	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}

	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}

	.modal .modal-header {
		border-bottom: none;
	}

	.modal-body {
		padding: 0;
	}

	.modal-content {
		background: #f8f8fa;
	}

	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/usr/usr_mng"/>">유저</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/usr/usr_mng"/>" class="page_nav">유저리스트</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="#" class="page_nav">유저등록</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>유저등록</strong></h4>
</div>

<div class="row">
	<form id="usr_form" method="post" action="<c:url value="/usr/usr_mng/0/save"/>">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">아이디</td>
				<td width="15%">
					<div class="col-md-12">
						<input type="text" class="form-control" id="usr_id" name="usr_id"/>
					</div>
				</td>
				<td class="td-label">비밀번호</td>
				<td width="15%">
					<div class="col-md-12">
						<input type="text" class="form-control" id="usr_pwd" name="usr_pwd"/>
					</div>
				</td>
				<td class="td-label">이메일</td>
				<td colspan="3">
					<div class="col-md-12">
						<input type="text" class="form-control" id="usr_mail" name="usr_mail"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">실명</td>
				<td width="15%" style="border-right: none;">
					<div class="col-md-12">
						<input type="text" class="form-control" id="usr_nm" name="usr_nm"/>
					</div>
				</td>
				<td colspan="2" style="border-left: none;">
				</td>
				<td class="td-label">핸드폰</td>
				<td width="15%">
					<div class="col-md-12">
						<input type="text" class="form-control" id="usr_phone" name="usr_phone"/>
					</div>
				</td>
				<td class="td-label">닉네임</td>
				<td width="15%">
					<div class="col-md-12">
						<input type="text" class="form-control" id="usr_nck_nm" name="usr_nck_nm"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소1</td>
				<td width="40%" colspan="3">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onAddSearch(1)">주소검색</a>
					</div>
					<div class="col-md-9" id="text_address1">
					    <input type="text" readonly class="form-control" id="address1"/>
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<input type="text" readonly class="form-control" id="address_detail1"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소2</td>
				<td width="40%" colspan="3">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onAddSearch(2)">주소검색</a>
					</div>
					<div class="col-md-9" id="text_address2">
					    <input type="text" readonly class="form-control" id="address2"/>
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<input type="text" readonly class="form-control" id="address_detail2"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소3</td>
				<td width="40%" colspan="3">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onAddSearch(3)">주소검색</a>
					</div>
					<div class="col-md-9" id="text_address3">
						<input type="text" readonly class="form-control" id="address3"/>
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<input type="text" readonly class="form-control" id="address_detail3"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">성별</td>
				<td width="15%" style="border-right: none;">
					<div class="col-md-12">
					<div class="btn-group btn-block" data-toggle="buttons">
						<label class="btn btn-default col-md-6">
						<input type="radio" class="toggle genders" name="genders" value="1"> 남성 </label>
						<label class="btn btn-default col-md-6">
						<input type="radio" class="toggle genders" name="genders" value="2"> 여성 </label>
					</div>
					</div>
				</td>
				<td colspan="2" style="border-left: none;">
				</td>
				<td class="td-label">생년월일</td>
				<td width="15%" style="border-right: none;">
					<div class="col-md-12">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="birthday" id="birthday">
							<span class="input-group-btn"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</td>
				<td colspan="2" style="border-left: none;">

				</td>
			</tr>
			<tr>
				<td class="td-label">계좌정보</td>
				<td colspan="7">
					<div class="col-md-2">
						<select class="form-control select" id="bank_nm" name="bank_nm">
							<option value=""></option>
							<c:forEach var="bank" items="${bank_list}" varStatus="status">
                            	<option value="${bank.bankNm }">${bank.bankNm }</option>
                        	</c:forEach>
						</select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="account_num" name="account_num"/>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="gender" name="gender" value=""/>
    <textarea style="display: none" id="address_main1" readonly name="address"></textarea>
    <textarea style="display: none" id="address_main2" readonly name="address"></textarea>
    <textarea style="display: none" id="address_main3" readonly name="address"></textarea>
    <input type="hidden" id="address_main_sub1" readonly name="address_detail">
    <input type="hidden" id="address_main_sub2" readonly name="address_detail">
    <input type="hidden" id="address_main_sub3" readonly name="address_detail">
	</form>
	<div class="row">
		<div class="col-md-1">
			<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
		</div>
		<div class="col-md-offset-5 col-md-1">
			<a class="btn btn-dark btn-block" onclick="onSave()">등록</a>
		</div>
	</div>
</div>

<a class="btn default hidden" data-toggle="modal" href="#addr_modal" id="btn_addr_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="addr_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>주소검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">

				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
    var selectedAddressIndex = 1;
    var find_address_window = undefined;
    function onAddSearch(index) {
        selectedAddressIndex = index;
        var popup_width = 600;
        var popup_height = 600;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width/2)-(popup_width/2)) + dualScreenLeft;
        var top = ((height/2)-(popup_height/2)) + dualScreenTop;
        find_address_window = window.open('${find_address_url}', '주소찾기', 'width=' + popup_width + ', height=' + popup_height + ', top=' + top + ', left=' + left + ', menubar=no, location=no, directories=no, status=no, toolbar=no, copyhistory=no');
    }

    function findAddress(address_detail, postcode){
        document.getElementById("address_main" + selectedAddressIndex).value = address_detail;
        document.getElementById("address_main_sub" + selectedAddressIndex).value = postcode;
        var address_detail_array = address_detail.split('\n', 2);
        document.getElementById("address" + selectedAddressIndex).value = address_detail_array[0];
        document.getElementById("address_detail" + selectedAddressIndex).value = address_detail_array[1];
    }

    window.addEventListener("message", function (ev) {
        var objects = JSON.parse(ev.data);
        findAddress(objects.address, objects.postcode);
    });
	$(document).ready(function () {
		$("#menu_usr").addClass("active");
		$("#menu_usr li:eq(0)").addClass("active");
		$(".nav-tabs li:eq(0)").addClass("active");
		$('.date-picker').datepicker({
			container: 'html',
    		autoclose: true
        });
	});

	$('.genders').change(function () {
		$("#gender").val($(this).val());
	});

	function onSave() {
		//check
		if ($.trim($("#usr_id").val()) == '') {
			$toast = toastr['error']('', '아이디를 입력하세요.');
			$("#usr_id").focus();
			return;
		}

		if ($.trim($("#usr_pwd").val()) == '') {
			$toast = toastr['error']('', '비밀번호를 입력하세요.');
			$("#usr_pwd").focus();
			return;
		}

		if ($.trim($("#usr_mail").val()) == '') {
			$toast = toastr['error']('', '이메일을 입력하세요.');
			$("#usr_mail").focus();
			return;
		}

		var reg_email = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
        if($("#usr_mail").val().match(reg_email) == null){
        	$toast = toastr['error']('', '이메일형식이 맞지않습니다.');
			$("#usr_mail").focus();
			return;
        }

		if ($.trim($("#usr_nm").val()) == '') {
			$toast = toastr['error']('', '실명을 입력하세요.');
			$("#usr_nm").focus();
			return;
		}

		if ($.trim($("#usr_phone").val()) == '') {
			$toast = toastr['error']('', '핸드폰을 입력하세요.');
			$("#usr_phone").focus();
			return;
		}

		if ($.trim($("#usr_nck_nm").val()) == '') {
			$toast = toastr['error']('', '닉네임을 입력하세요.');
			$("#usr_nck_nm").focus();
			return;
		}

		if ($.trim($("#birthday").val()) == '') {
			$toast = toastr['error']('', '생년월일을 입력하세요.');
			$("#birthday").focus();
			return;
		}

		if ($("#bank_nm").val() == '') {
			$toast = toastr['error']('', '은행명을 선택하세요.');
			return;
		}

		if ($.trim($("#account_num").val()) == '') {
			$toast = toastr['error']('', '계좌번호를 입력하세요.');
			return;
		}

		if ($("#gender").val() == '') {
			$toast = toastr['error']('', '성별을 선택하세요.');
			return;
		}

		$.post('<c:url value="/usr/usr_mng/0/save"/>', $("#usr_form").serialize(),
			function (result) {
				if (result == "dup") {
					$toast = toastr['error']('', '아이디가 중복됩니다.');
					$("#usr_id").focus();
				} else if (result == "success") {
					$toast = toastr['success']('', '성과적으로 저장되었습니다.');
					setTimeout('onBack()', 1000);
				} else {
					$toast = toastr['error']('', '실패했습니다.');
				}
			}
		);
	}
</script>
