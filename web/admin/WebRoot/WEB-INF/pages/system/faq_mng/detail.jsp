<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/faq_mng"/>" class="page_nav">FAQ</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<c:if test="${detail_uid == 0 }">
		<a href="#" class="page_nav">FAQ등록</a>
		</c:if>
		<c:if test="${detail_uid != 0 }">
		<a href="#" class="page_nav">FAQ수정</a>
		</c:if>
	</div>
		<div class="col-md-offset-5 col-md-1">
			<c:if test="${detail_uid != 0 }">
				<a class="btn btn-red-inline btn-block" onclick="delete_faq()">삭제</a>
			</c:if>
		</div>
</div>

<c:if test="${detail_uid == 0 }">
<div class="row" style="margin-top: 20px;">
	<h4><strong>FAQ등록</strong></h4>
</div>
</c:if>

<c:if test="${detail_uid != 0 }">
<div class="row">
	<h4><strong>FAQ수정</strong></h4>
</div>
</c:if>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">분류</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-8">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default col-md-3 <c:if test='${type == null || type == 1 }'>active</c:if>">
							<input type="radio" class="toggle type" id="type_faq" name="type" value="1"> 판매 </label>
							<label class="btn btn-default col-md-3 <c:if test='${type == 2 }'>active</c:if>">
							<input type="radio" class="toggle type" id="type_theme" name="type" value="2"> 구매 </label>
							<label class="btn btn-default col-md-3 <c:if test='${type == 3 }'>active</c:if>">
							<input type="radio" class="toggle type" id="type_event" name="type" value="3"> 활동 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">타이틀</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="title" value="${title }"/>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">상세</td>
				<td>
					<div class="col-md-12">
						<textarea class="form-control" id="content">${content }</textarea>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;뒤로</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark btn-block" onclick="save()">
		<c:if test="${detail_uid == 0 }">
		등록
		</c:if>
		<c:if test="${detail_uid != 0 }">
		수정
		</c:if>
		</a>
	</div>
</div>

<input type="hidden" id="detail_uid" value="${detail_uid }"/>
<input type="hidden" id="type" value="${type }"/>

<script>
	$(document).ready(function () {
		$(".page-sidebar-menu li:eq(17)").addClass("active");
		$(".page-sidebar-menu li:eq(26)").addClass("active");
		$(".nav-tabs li:eq(8)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 FAQ입니다.");
			onBack();
		}
	});

	function delete_faq(){
		if (!confirm("FAQ정보를 삭제하시겠어요?"))
			return;
			
		$.ajax({
			url: "<c:url value="/system/faq_mng/delete_faq" />",
			type: "POST",
			data: {
				faq_uid: $("#detail_uid").val()
			},
			success: function (result) {
				$toast = toastr["success"]('', '성과적으로  삭제되었습니다.');
				setTimeout('onBack()', 1000);
			}
		});
	}
	
	var type = 1;
	if ($("#type").val() != "")
		type = $("#type").val();
	
	$('.type').on('change', function (event) {	
		type = $(this).val();
	});
	
	function save(){	
		if ($("#title").val() == ""){
			$toast = toastr['error']('', '타이틀을 입력하세요.');
			$("#title").focus();
			return;
		}
		
		if ($("#content").val() == ""){
			$toast = toastr['error']('', '내용을 입력하세요.');
			$("#content").focus();
			return;
		}
		
		$.ajax({
			url: "<c:url value="/system/faq_mng/save" />",
			type: "POST",
			data: {
				faq_uid: $("#detail_uid").val(),
				type: type,
				title: $("#title").val(),
				content: $("#content").val()
			},
			success: function (result) {
				$toast = toastr["success"]('', '성과적으로  저장되었습니다.');
				setTimeout('onBack()', 1000);
			}
		});
	}
</script>