<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/theme_mng"/>" class="page_nav">테마</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<c:if test="${theme_uid == 0 }">
		<a href="#" class="page_nav">테마등록</a>
		</c:if>
		<c:if test="${theme_uid != 0 }">
		<a href="#" class="page_nav">테마수정</a>
		</c:if>
	</div>
		<div class="col-md-offset-5 col-md-1">
			<c:if test="${theme_uid != 0 }">
				<a class="btn btn-red-inline btn-block" onclick="delete_theme()">삭제</a>
			</c:if>
		</div>
</div>

<c:if test="${theme_uid == 0 }">
<div class="row" style="margin-top: 20px;">
	<h4><strong>테마등록</strong></h4>
</div>
</c:if>

<c:if test="${theme_uid != 0 }">
<div class="row">
	<h4><strong>테마수정</strong></h4>
</div>
</c:if>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">분류</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-6">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default col-md-4 <c:if test='${theme.kind == 1 }'>active</c:if>">
							<input type="radio" class="toggle kind" name="kind" value="1"> 남성 </label>
							<label class="btn btn-default col-md-4 <c:if test='${theme.kind == 2 }'>active</c:if>">
							<input type="radio" class="toggle kind" name="kind" value="2"> 여성 </label>
							<label class="btn btn-default col-md-4 <c:if test='${theme.kind == 4 }'>active</c:if>">
							<input type="radio" class="toggle kind" name="kind" value="4"> 키즈 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">타이틀</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="title_ko" name="title_ko" value="${theme.titleKo }"/>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">영문타이틀</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="title_en" name="title_en" value="${theme.titleEn }"/>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">노출기간</td>
				<td>
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from" value="${start_time }">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to" value="${end_time }">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">타이틀이미지</td>
				<td>
					<div class="col-md-2">
						<button class="btn btn-block btn-dark" id="btn_profile_img">사진찾기</button>
	    			</div>
				</td>
			</tr>
			<tr id="profile_img_pad" <c:if test="${profile_img==null || profile_img.isEmpty() }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="col-md-2 portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px; padding-left: ' + padding_left + 'px">
						<div class="img_pad_bottom portlet-title">
							<a onclick='onImageClick("${real_profile_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview"> 
                   				<img id="img_profile" src="${real_profile_img }" width="150px" height="150px"></img>
                   			</a>
                   			<div class="img_pad_this" onclick="delete_img(this)">
                        		<i style="color: white;" class="fa fa-times"></i>
                            </div>
                        </div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">테마상품</td>
				<td>
					<div class="col-md-2">
						<button class="btn btn-block btn-dark" onclick="onPdtAdd()">상품추가</button>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="table table-bordered" id="theme_pdt_list" width="100%">
		<thead>
			<tr>
				<th>코드</th>
				<th>이미지</th>
				<th>상품명</th>
				<th>판매자</th>
				<th>판매가</th>
				<th>삭제</th>
			</tr>
		</thead>
	</table>
</div>
<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;뒤로</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark btn-block" onclick="save()">
		<c:if test="${theme_uid == 0 }">
		등록
		</c:if>
		<c:if test="${theme_uid != 0 }">
		수정
		</c:if>
		</a>
	</div>
</div>

<a class="btn default hidden" data-toggle="modal" href="#pdt_modal" id="btn_pdt_modal">View Demo </a>
<div class="modal fade" id="pdt_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="keyword" name="keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">카테고리</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="pdt_group" name="pdt_group" onchange="onPdtGroupChange($(this).val())">
											<option value="0">전체</option>
											<option value="1">남성</option>
											<option value="2">여성</option>
											<option value="4">키즈</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category1" name="category1" onchange="onCategory1Change($(this).val())">
											<option value="0">전체</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category2" name="category2" onchange="onCategory2Change($(this).val())">
											<option value="0">전체</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category3" name="category3">
											<option value="0">전체</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>상품추가</strong></h4>
					</div>
				</div>
				<div class="row" style="height: 300px; overflow-y: auto;">
					<table class="table table-bordered" id="pdt_list" width="100%">
						<thead>
							<tr>
								<th>코드</th>
								<th>이미지</th>
								<th>상품명</th>
								<th>판매자</th>
								<th>판매가</th>
								<th>추가</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<input type="hidden" id="theme_uid" name="theme_uid" value="${theme_uid }"/>
<input type="hidden" id="kind" name="kind" value="${theme.kind }"/>
<input type="hidden" id="profile_img" name="profile_img" value="${profile_img }"/>

<script>
	var keyword = '';
	var pdt_group = 0;
	var category1 = 0;
	var category2 = 0;
	var category3 = 0;
	var ajax_table;
	var ajax_theme_pdt_table;
	
	$(document).ready(function () {
		$(".page-sidebar-menu li:eq(17)").addClass("active");
		$(".page-sidebar-menu li:eq(23)").addClass("active");
		$(".nav-tabs li:eq(5)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 테마입니다.");
			onBack();
		}
		
		$('.date-picker').datepicker({
			container: 'html',
   			autoclose: true
		});
		
		ajax_table = $("#pdt_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	}
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.keyword = keyword;
                    d.pdt_group = pdt_group;
                    d.category1 = category1;
                    d.category2 = category2;
                    d.category3 = category3;
                    d.status = 1;
                }
            },
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(1)', row).html('');
           		var img_src = data[13] + '/pdt/' + data[0] + '/' + data[1];
           		$('td:eq(1)', row).html('<img src="' + img_src + '" width="100px" height="100px"/>');
           		var pdt_group_str = '';
           		if (data[10] == 1)
           			pdt_group_str = '남성';
           		else if (data[10] == 2)
           			pdt_group_str = '여성';
           		else if (data[10] == 4)
           			pdt_group_str = '키즈';
          		$('td:eq(2)', row).css("text-align", "left");
           		$('td:eq(2)', row).html(data[9] + ' ' + pdt_group_str + ' ' + data[11] + ' ' + data[2] + ' ' + data[12]);
           		
           		$('td:eq(3)', row).html(data[4]);
           		$('td:eq(3)', row).css('cursor', 'pointer');
          		$('td:eq(3)', row).attr('onclick', "go_url('<c:url value="/usr/usr_mng/"/>" + data[14] + "/detail')");
          		
           		$('td:eq(4)', row).html(data[3]);
           		
           		$('td:eq(5)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(5)', row).css('cursor', 'pointer');
           		$('td:eq(5)', row).html('추가');
           		$('td:eq(5)', row).attr('onclick', 'onAdd(' + data[0] + ')');
           		
          		$('td:eq(0)', row).html(uid_format(data[0]));
           	}
		});
		
		ajax_theme_pdt_table = $("#theme_pdt_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "paging": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/system/theme_mng/ajax_theme_pdt_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.theme_uid = $("#theme_uid").val();
                }
            },
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(1)', row).html('');
           		var img_src = data[14] + '/pdt/' + data[0] + '/' + data[1];
           		$('td:eq(1)', row).html('<a onclick="onImageClick(\'' + img_src + '\')" class="fancybox-button" data-rel="fancybox-button" target="_blank"><img src="' + img_src + '" width="100px" height="100px"/></a>');
           		var pdt_group_str = '';
           		if (data[10] == 1)
           			pdt_group_str = '남성';
           		else if (data[10] == 2)
           			pdt_group_str = '여성';
           		else if (data[10] == 4)
           			pdt_group_str = '키즈';
          		$('td:eq(2)', row).css("text-align", "left");
           		$('td:eq(2)', row).html(data[9] + ' ' + pdt_group_str + ' ' + data[11] + ' ' + data[2] + ' ' + data[12]);
           		
           		$('td:eq(3)', row).css('cursor', 'pointer');
          		$('td:eq(3)', row).attr('onclick', "go_url('<c:url value="/usr/usr_mng/"/>" + data[15] + "/detail')");
           		
           		$('td:eq(5)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(5)', row).css('cursor', 'pointer');
           		$('td:eq(5)', row).html('삭제');           		
           		$('td:eq(5)', row).attr('onclick', 'onDelete(' + data[13] + ')');
           			
           		$('td:eq(0)', row).css('cursor', 'pointer');
          		$('td:eq(0)', row).html(uid_format(data[0]));
          		$('td:eq(0)', row).attr('onclick', "go_url('<c:url value="/pdt/pdt_mng/"/>" + data[0] + "/detail')");	
           	}
		});
	
		try {
	        new AjaxUpload($("#btn_profile_img"), {
	            action: "<c:url value="/file/upload"/>",
	            name: 'uploadfile',
	            responseType: "json",
	            onComplete: function (file, response) {
	                if (response.result === "ok") {
	                    $("#profile_img_pad").removeClass("hidden");
	                    $("#img_preview").attr("onclick", "onImageClick('" + response.fileURL + "')");
	                    $("#img_profile").attr("src", response.fileURL);
	                    $("#profile_img").val(response.fileName);
	                } else {
	                    $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
	                    return;
	                }
	            }
	        })
	    } catch (e) {
	        alert(e);
	    }
	});
	
	$(".period").change(function () {
		onPeriodChange2($(this).val());
	});
	
	function onSearch() {
		keyword = $("#keyword").val();
		pdt_group = $("#pdt_group").val();
		category1 = $("#category1").val();
		category2 = $("#category2").val();
		category3 = $("#category3").val();
		ajax_table.draw(false);
	}
	
	$("#keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onSearch();
        }
    });
	
	function delete_theme(){
		$.ajax({
			url: "<c:url value="/system/theme_mng/"/>" + $("#theme_uid").val() + "/delete",
			type: "POST",
			success: function (result) {
				$toast = toastr["success"]('', '성과적으로  삭제되었습니다.');
				setTimeout('onBack()', 1000);
			}
		});
	}
	
	$('.kind').change(function () {
		$("#kind").val($(this).val());
	});
	
	function save(){
		if ($.trim($("#kind").val()) == "") {
			$toast = toastr['error']('', '분류를 선택해주세요.');
			return;
		}
	
		if ($.trim($("#title_ko").val()) == "") {
			$toast = toastr['error']('', '타이틀을 입력해주세요.');
			$("#title_ko").focus();
			return;
		}
		
		if ($.trim($("#title_en").val()) == "") {
			$toast = toastr['error']('', '영문타이틀을 입력해주세요.');
			$("#title_en").focus();
			return;
		}
		
		if ($("#profile_img").val() == '') {
			$toast = toastr['error']('', '타이틀이미지를 선택하세요.');
			return;
		}

		$.ajax({
			url: "<c:url value="/system/theme_mng/" />" + $("#theme_uid").val() + "/save",
			type: "POST",
			data: {
				theme_uid: $("#theme_uid").val(),
				title_ko: $("#title_ko").val(),
				title_en: $("#title_en").val(),
				period_from: $("#period_from").val(),
				period_to: $("#period_to").val(),
				profile_img: $("#profile_img").val(),
				kind: $("#kind").val()
			},
			success: function (result) {
				$toast = toastr["success"]('', '성과적으로  저장되었습니다.');
				setTimeout('onBack()', 1000);
			}
		});
	}
	
	function onPdtAdd() {
		onSearch();
		$("#btn_pdt_modal").trigger('click');
	}
	
	function delete_img(obj) {
		if (!confirm("이미지를 삭제하시겠습니까?"))
			return;
		$("#profile_img_pad").addClass('hidden');
		$("#profile_img").val("");
	}
	
	function onPdtGroupChange(id) {
    	$('#category1').empty();
        $('#category1').append('<option value="0">전체</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category1').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory1Change(json[0].categoryUid);
	            else
	            	onCategory1Change(0);
	        });
	    } else {
	    	onCategory1Change(0);
	    }
	}
	
	function onCategory1Change(id) {
		$('#category2').empty();
        $('#category2').append('<option value="0">전체</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category2').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory2Change(json[0].categoryUid);
	            else
	            	onCategory2Change(0);
	        });
	   	} else {
	   		onCategory2Change(0);
	   	}
	}
	
	function onCategory2Change(id) {
		$('#category3').empty();
        $('#category3').append('<option value="0">전체</option>');
		if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category3').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	        });
	    }
	}
	
	function onAdd(id) {
		$.post('<c:url value="/system/theme_mng/add_pdt"/>', {pdt_uid: id, theme_uid: $("#theme_uid").val()}, function (result) {
            if (result == "dup") {
            	$toast = toastr['error']('그 상품은 이미 추가되여있습니다.');
            } else if (result == "success") {
            	$toast = toastr['success']('성공적으로 추가되었습니다.');
            	ajax_theme_pdt_table.draw(false);
            } else {
            	$toast = toastr['error']('추가가 실패했습니다.');
            	$(".close").trigger('click');
            }
        });
	}
	
	function onDelete(id) {
		if (!confirm("테마에 추가된 상품을 삭제하시겠습니까?"))
			return;
		$.post('<c:url value="/system/theme_mng/delete_pdt"/>', {theme_pdt_uid: id}, function (result) {
            if (result == "success") {
            	ajax_theme_pdt_table.draw(false);
            } else {
            	$toast = toastr['error']('삭제가 실패했습니다.');
            	$(".close").trigger('click');
            }
        });
	}
</script>