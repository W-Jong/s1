<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/notice_mng"/>" class="page_nav">공지</a>
	</div>
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-blue" onclick="onRegister()">공지등록</a>
	</div>
</div>

<div class="row">
	<h4><strong>검색</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">검색어</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-5" style="padding-right: 39px;">
						<input type="text" class="form-control" id="keyword" name="keyword"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">등록일</td>
				<td colspan="3">
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7" colspan="3">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">활성</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="active_status" type="radio" id="active_status_all" name="active_status" checked value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="active_status" type="radio" id="active_status_yes" name="active_status" value="2"/>&nbsp;활성
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="active_status" type="radio" id="active_status_no" name="active_status" value="1"/>&nbsp;비활성
		    			</label>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSearch()">검색</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
		<h4><strong>목록</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 3px;">
	</div>
	<div class="col-md-offset-6 col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row">
	<table class="table table-bordered" id="notice_list">
		<thead>
			<tr>
				<th>공지 타이틀</th>
				<th>등록일</th>
				<th>시작일<br>종료일</th>
				<th>수정</th>
			</tr>
		</thead>
	</table>
</div>

<input type="hidden" id="total_count" value="${total_count }"/>

<script>
var ajax_table;
var period_from = '';
var period_to = '';
var active_status = 0;
var keyword = '';

$(document).ready(function () {
	$(".page-sidebar-menu li:eq(17)").addClass("active");
	$(".page-sidebar-menu li:eq(24)").addClass("active");
	$(".nav-tabs li:eq(6)").addClass("active");
	$('.date-picker').datepicker({
   		container: 'html',
   		autoclose: true
    });
	$('.control-label input[type="radio"]').iCheck({
        checkboxClass: 'iradio_minimal-blue',
        radioClass: 'iradio_minimal-blue',
        increaseArea: '20%' // optional
    });
	
	ajax_table = $("#notice_list").DataTable({
		dom : 'itp',
		"stateSave": false,
           "autowidth": true,
           "serverSide": true,
           "ordering": false,
           "language" : {
              	"emptyTable": '<center>데이터가 없습니다.</center>',
              	"paginate" : {
              		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
              		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
              	},
              	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
               "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
           },
           "deferRender": true,
           "ajax": {
               "url": "<c:url value='/system/notice_mng/ajax_table'/>",
               "type": "POST",
               "data":   function ( d ) {
                   start_index = d.start;
                   d.length = $("#length").val();
                   d.keyword = keyword;
                   d.period_from = period_from;
                   d.period_to = period_to;
                   d.active_status = active_status;
               }
           },
           "pageLength": 100,
          	"createdRow": function (row, data, dataIndex) {          	
          		$('td:eq(3)', row).html('수정');
          		$('td:eq(3)', row).attr('onclick', 'edit_notice(' + data[3] + ')');
          		$('td:eq(3)', row).css('cursor', 'pointer');
          	}
	});
	
	$("#table_info").append($("#notice_list_info"));
});
	
function onSearch() {
	keyword = $("#keyword").val();
	period_from = $("#period_from").val();
	period_to = $("#period_to").val();
	ajax_table.draw(false);
}

function onInitialize() {
	$("#keyword").val("");
	$("#period_from").val("");
	$("#period_to").val("");
	$("#period_all").trigger('click');
	$("#active_status_all").iCheck("check");
	active_status = 0;
	onSearch();
}

$(".active_status").on('ifClicked', function (event) {	
	active_status = $(this).val();
});

function edit_notice(notice_uid){
	location.href = "<c:url value='/system/notice_mng/"+notice_uid+"/detail'/>";
}

function onRegister() {
	location.href = "<c:url value="/system/notice_mng/0/detail"/>";
}

$(".period").change(function () {
	onPeriodChange($(this).val());
});
</script>