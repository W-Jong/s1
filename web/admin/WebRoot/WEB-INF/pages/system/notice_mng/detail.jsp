<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/notice_mng"/>" class="page_nav">공지</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<c:if test="${detail_uid == 0 }">
		<a href="#" class="page_nav">공지등록</a>
		</c:if>
		<c:if test="${detail_uid != 0 }">
		<a href="#" class="page_nav">공지수정</a>
		</c:if>
	</div>
		<div class="col-md-offset-5 col-md-1">
			<c:if test="${detail_uid != 0 }">
				<a class="btn btn-red-inline btn-block" onclick="delete_notice()">삭제</a>
			</c:if>
		</div>
</div>

<c:if test="${detail_uid == 0 }">
	<div class="row" style="margin-top: 20px;">
		<h4><strong>공지사항등록</strong></h4>
	</div>
</c:if>
<c:if test="${detail_uid != 0 }">
	<div class="row">
		<h4><strong>공지사항수정</strong></h4>
	</div>
</c:if>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">타이틀</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="title" value="${title }"/>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">노출기간</td>
				<td>
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from" value="${start_time }">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to" value="${end_time }">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">상세</td>
				<td>
					<div class="col-md-12">
						<textarea class="form-control" id="content">${content }</textarea>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;뒤로</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark btn-block" onclick="save()">
		<c:if test="${detail_uid == 0 }">
		등록
		</c:if>
		<c:if test="${detail_uid != 0 }">
		수정
		</c:if>
		</a>
	</div>
</div>

<input type="hidden" id="detail_uid" value="${detail_uid }"/>

<script>
	$(document).ready(function () {
		$(".page-sidebar-menu li:eq(17)").addClass("active");
		$(".page-sidebar-menu li:eq(24)").addClass("active");
		$(".nav-tabs li:eq(6)").addClass("active");
		$('.date-picker').datepicker({
			container: 'html',
		   	autoclose: true
		});
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 공지입니다.");
			onBack();
		}
	});

	$(".period").change(function () {
		onPeriodChange2($(this).val());
	});
	
	function delete_notice(){
		if (!confirm("공지사항을 삭제하시겠어요?"))
			return;
			
		$.ajax({
			url: "<c:url value="/system/notice_mng/delete_notice" />",
			type: "POST",
			data: {
				notice_uid: $("#detail_uid").val()
			},
			success: function (result) {
				$toast = toastr["success"]('', '성과적으로 삭제되었습니다.');
				setTimeout('onBack()', 1000);
			}
		});
	}
	
	function save(){	
		if ($("#title").val() == "") {
			$toast = toastr['error']('', '타이틀을 입력하세요.');
			$("#title").focus();
			return;
		}
		
		if ($("#content").val() == "") {
			$toast = toastr['error']('', '상세를 입력하세요.');
			$("#content").focus();
			return;
		}
		
		$.ajax({
			url: "<c:url value="/system/notice_mng/save" />",
			type: "POST",
			data: {
				notice_uid: $("#detail_uid").val(),
				title: $("#title").val(),
				period_from: $("#period_from").val(),
				period_to: $("#period_to").val(),
				content: $("#content").val()
			},
			success: function (result) {
				$toast = toastr["success"]('', '성과적으로 저장되었습니다.');
				setTimeout('onBack()', 1000);
			}
		});
	}
</script>