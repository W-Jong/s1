<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="page-bar">
	<div class="header_title">
		<strong><span style="font-size: 20px; color: black;">운영</span></strong>
	</div>
	<ul class="nav nav-tabs">
		<li>
			<a href="<c:url value="/system/reply_mng/index"/>" style="cursor: pointer;">
			댓글 </a>
		</li>
		<li>
			<a href="<c:url value="/system/report_mng/index"/>" style="cursor: pointer;">
			신고 </a>
		</li>
		<li>
			<a href="<c:url value="/system/qna_mng/index"/>" style="cursor: pointer;">
			문의 </a>
		</li>
		<li>
			<a href="<c:url value="/system/review_mng/index"/>" style="cursor: pointer;">
			후기 </a>
		</li>
		<li>
			<a href="<c:url value="/system/banner_mng/index"/>" style="cursor: pointer;">
			배너 </a>
		</li>
		<li>
			<a href="<c:url value="/system/theme_mng/index"/>" style="cursor: pointer;">
			테마 </a>
		</li>
		<li>
			<a href="<c:url value="/system/notice_mng/index"/>" style="cursor: pointer;">
			공지 </a>
		</li>
		<li>
			<a href="<c:url value="/system/event_mng/index"/>" style="cursor: pointer;">
			이벤트 </a>
		</li>
		<li>
			<a href="<c:url value="/system/faq_mng/index"/>" style="cursor: pointer;">
			FAQ </a>
		</li>
		<li>
			<a href="<c:url value="/system/license_mng/index"/>" style="cursor: pointer;">
			정책 </a>
		</li>
	</ul>
</div>

<script>
	$(document)
	  .ajaxStart(function() {
	    loadingOverlay();
	}).ajaxStop(function() {
	    loadingOverlayRemove();
	});
</script>
