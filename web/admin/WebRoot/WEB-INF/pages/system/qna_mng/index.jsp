<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/qna_mng"/>" class="page_nav">문의</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>검색</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">검색어</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-5" style="padding-right: 39px;">
						<input type="text" class="form-control" id="keyword" name="keyword"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">작성일</td>
				<td colspan="3">
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">답변상태</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="status" type="radio" id="status_all" name="status" <c:if test="${status==null }">checked</c:if> value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="status" type="radio" id="status_no" name="status" <c:if test="${status==2 }">checked</c:if> value="2"/>&nbsp;답변대기
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="status" type="radio" id="status_yes" name="status" value="1"/>&nbsp;답변완료
		    			</label>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSearch()">검색</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
		<h4><strong>목록</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 3px;">
	</div>
	<div class="col-md-offset-6 col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row">
	<table class="table table-bordered" id="qna_list">
		<thead>
			<tr>
				<th>작성자</th>
				<th>제목</th>
				<th>답변상태</th>
				<th>작성일</th>
				<th>답변일</th>
				<th>답변</th>
			</tr>
		</thead>
	</table>
</div>

<input type="hidden" id="total_count" value="${total_count }"/>

<a class="btn default hidden" data-toggle="modal" href="#large" id="btn_detail">View Demo </a>
<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>문의답변</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="width: 20%;">작성자</td>
								<td style="border-right: none;">
									<div class="col-md-12" id="detail_usr_id">
									</div>
								</td>
								<td class="td-label" style="width: 20%;">작성일</td>
								<td style="border-right: none;">
									<div class="col-md-12" id="detail_reg_time">
									</div>
								</td>
							</tr>
							<tr>
								<td class="td-label">문의제목</td>
								<td style="border-right: none;" colspan="3">
									<div class="col-md-12" id="detail_title">
									</div>
								</td>
							</tr>
							<tr>
								<td class="td-label">문의내용</td>
								<td style="border-right: none;" colspan="3">
									<div class="col-md-6" style="height: 80px;" id="detail_content">									
									</div>
								</td>
							</tr>
							<tr>
								<td class="td-label">문의답변</td>
								<td colspan="3" style="border-right: none;">
									<textarea class="form-control" id="detail_answer"></textarea>
								</td>
							</tr>
							<tr>
								<td class="td-label">관리자메모</td>
								<td colspan="3" style="border-right: none;">
									<textarea class="form-control" id="detail_memo"></textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="row" style="margin-bottom: 20px;">
					<div class="col-md-offset-4 col-md-2">
						<a class="btn btn-dark btn-block" onclick="answer()">저장</a>
					</div>
					<div class="col-md-2">
						<a class="btn btn-dark-inline btn-block" data-dismiss="modal" id ="btn_cancel">닫기</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
	<input type="hidden" id="detail_uid"/>
</div>

<script>
var ajax_table;
var status = ${status==null?0:status};
var period_from = '';
var period_to = '';
var keyword = '';

$(document).ready(function () {
	$(".page-sidebar-menu li:eq(17)").addClass("active");
	$(".page-sidebar-menu li:eq(20)").addClass("active");
	$(".nav-tabs li:eq(2)").addClass("active");
	$('.date-picker').datepicker({
   		container: 'html',
   		autoclose: true
    });
	$('.control-label input[type="radio"]').iCheck({
        checkboxClass: 'iradio_minimal-blue',
        radioClass: 'iradio_minimal-blue',
        increaseArea: '20%' // optional
    });
	
	ajax_table = $("#qna_list").DataTable({
		dom : 'itp',
		"stateSave": false,
           "autowidth": true,
           "serverSide": true,
           "ordering": false,
           "language" : {
              	"emptyTable": '<center>데이터가 없습니다.</center>',
              	"paginate" : {
              		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
              		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
              	},
              	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
               "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
           },
           "deferRender": true,
           "ajax": {
               "url": "<c:url value='/system/qna_mng/ajax_table'/>",
               "type": "POST",
               "data":   function ( d ) {
                   start_index = d.start;
                   d.length = $("#length").val();
                   d.keyword = keyword;
                   d.period_from = period_from;
                   d.period_to = period_to;
                   d.status = status;
               }
           },
           "pageLength": 100,
          	"createdRow": function (row, data, dataIndex) {
          		if (data[2] == 1)
          			$('td:eq(2)', row).html('완료');
          		else if (data[2] == 2)
          			$('td:eq(2)', row).html('대기');
          			
          		if (data[4] == '')
          			$('td:eq(4)', row).html('-');          		
          	
          		if (data[2] == 1)
          			$('td:eq(5)', row).html('완료');
          		else if (data[2] == 2) {
          			$('td:eq(5)', row).html('답변하기');
          			$('td:eq(5)', row).css('background', '#5d9bfc');
          			$('td:eq(5)', row).css('color', 'white');
          		}
          		$('td:eq(5)', row).attr('onclick', 'onDetail(' + data[5] + ')');
          		$('td:eq(5)', row).css('cursor', 'pointer');
          		
          		$('td:eq(0)', row).css('cursor', 'pointer');
          		$('td:eq(0)', row).attr('onclick', "go_url('<c:url value="/usr/usr_mng/"/>" + data[6] + "/detail')");
          	}
	});
	
	$("#table_info").append($("#qna_list_info"));
});
	
function onSearch() {
	keyword = $("#keyword").val();
	period_from = $("#period_from").val();
	period_to = $("#period_to").val();
	ajax_table.draw(false);
}

function onInitialize() {
	$("#keyword").val("");
	$("#period_from").val("");
	$("#period_to").val("");
	$("#period_all").trigger('click');
	$("#status_all").iCheck("check");
	status = 0;
	onSearch();
}
	
$(".status").on('ifClicked', function (event) {	
	status = $(this).val();
});

function onDetail(qna_uid) {
    $("#detail_uid").val('');
    $("#detail_title").html('');
    $("#detail_content").html('');
    $("#detail_usr_id").html('');
    $("#detail_answer").html('');
    $("#detail_memo").html('');
    $("#detail_reg_time").html('');
	$.ajax({
		url: "<c:url value="/system/qna_mng/detail" />",
		type: "POST",
		data: {
			qna_uid: qna_uid,
		},
		success: function (result) {
			var json = result;
			
			$("#detail_uid").val(json.qna_uid);
			$("#detail_title").html(json.title);
			$("#detail_content").html(json.content);
			$("#detail_usr_id").html(json.usr_id);
			$("#detail_answer").html(json.answer);
			$("#detail_memo").html(json.memo);
			$("#detail_reg_time").html(json.reg_time);
			change_status(json.status);
		}
	});
   $("#btn_detail").trigger('click');
}

var detail_status;
function change_status(status){
	if (status == 1){
		$("#btn_status_yes").addClass("btn-dark");
		$("#btn_status_yes").removeClass("btn-dark-inline");
		
		$("#btn_status_no").addClass("btn-dark-inline");
		$("#btn_status_no").removeClass("btn-dark");
		detail_status = 1;
	}else if (status == 2){
		$("#btn_status_no").addClass("btn-dark");
		$("#btn_status_no").removeClass("btn-dark-inline");
		
		$("#btn_status_yes").addClass("btn-dark-inline");
		$("#btn_status_yes").removeClass("btn-dark");
		detail_status = 2;
	}
}

function answer(){
    if ($("#detail_uid").val() == "" || $("#detail_uid").val() < 1) {
        alert("DB오류입니다.");
        $("#btn_cancel").trigger("click");
        return;
	}
	if ($("#detail_answer").val() == ""){
		alert("답변을 입력해주세요");
		return;
	}
	$.ajax({
		url: "<c:url value="/system/qna_mng/answer" />",
		type: "POST",
		data: {
			qna_uid: $("#detail_uid").val(),
			answer: $("#detail_answer").val(),
			memo: $("#detail_memo").val()
		},
		success: function (result) {
			ajax_table.draw(false);
		}
	});
	
	$("#btn_cancel").trigger("click");
}

$(".period").change(function () {
	onPeriodChange($(this).val());
});
</script>