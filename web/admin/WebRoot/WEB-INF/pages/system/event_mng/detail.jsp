<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/event_mng"/>" class="page_nav">이벤트</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<c:if test="${detail_uid == 0 }">
		<a href="#" class="page_nav">이벤트등록</a>
		</c:if>
		<c:if test="${detail_uid != 0 }">
		<a href="#" class="page_nav">이벤트수정</a>
		</c:if>
	</div>
		<div class="col-md-offset-5 col-md-1">
			<c:if test="${detail_uid != 0 }">
				<a class="btn btn-red-inline btn-block" onclick="delete_event()">삭제</a>
			</c:if>
		</div>
</div>

<c:if test="${detail_uid == 0 }">
<div class="row" style="margin-top: 20px;">
	<h4><strong>이벤트등록</strong></h4>
</div>
</c:if>

<c:if test="${detail_uid != 0 }">
<div class="row">
	<h4><strong>이벤트수정</strong></h4>
</div>
</c:if>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">마이페이지노출</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-8">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default col-md-3 <c:if test='${event == null || event.mypageYn == 1 }'>active</c:if>">
							<input type="radio" class="toggle mypageYN" id="mypageYN_y" name="mypageYn" value="1"> 예 </label>
							<label class="btn btn-default col-md-3 <c:if test='${event.mypageYn == 0 }'>active</c:if>">
							<input type="radio" class="toggle mypageYN" id="mypageYN_n" name="mypageYn" value="0"> 아니오 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">타이틀</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="title" value="${title }"/>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">노출기간</td>
				<td>
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from" value="${start_time }">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to" value="${end_time }">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">타이틀이미지</td>
				<td>
					<div class="col-md-2">
						<button class="btn btn-block btn-dark" id="btn_profile_img">사진찾기</button>
	    			</div>
				</td>
			</tr>
			<tr id="profile_img_pad" <c:if test="${profile_img==null || profile_img.isEmpty() }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="img_pad_bottom portlet-title">
						<a onclick='onImageClick("${real_profile_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview1"> 
               			<img id="img_profile" src="${real_profile_img }" width="150px" height="150px"></img>
               			</a>
               			<div class="img_pad_this" onclick="delete_img(this, 1)">
                    		<i style="color: white;" class="fa fa-times"></i>
                        </div>
                    </div>
				</td>
			</tr>
			<tr>
				<td class="td-label">이벤트이미지</td>
				<td>
					<div class="col-md-2">
						<button class="btn btn-block btn-dark" id="btn_event_img">사진찾기</button>
	    			</div>
				</td>
			</tr>
			<tr id="event_img_pad" <c:if test="${event_img==null || event_img.isEmpty() }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="img_pad_bottom portlet-title">
						<a onclick='onImageClick("${real_event_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview2"> 
               			<img id="img_event" src="${real_event_img }" width="150px" height="150px"></img>
               			</a>
               			<div class="img_pad_this" onclick="delete_img(this, 2)">
                    		<i style="color: white;" class="fa fa-times"></i>
                        </div>
                    </div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;뒤로</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark btn-block" onclick="save()">
		<c:if test="${detail_uid == 0 }">
		등록
		</c:if>
		<c:if test="${detail_uid != 0 }">
		수정
		</c:if>
		</a>
	</div>
</div>

<input type="hidden" id="detail_uid" value="${detail_uid }"/>
<input type="hidden" id="mypageYN" value="${mypageYn }"/>
<input type="hidden" id="profile_img" name="profile_img" value="${profile_img }"/>
<input type="hidden" id="event_img" name="event_img" value="${event_img }"/>

<script>
$(document).ready(function () {
	$(".page-sidebar-menu li:eq(17)").addClass("active");
	$(".page-sidebar-menu li:eq(25)").addClass("active");
	$(".nav-tabs li:eq(7)").addClass("active");
	
	if ('${msg}' == 'error')
	{
		alert("DB 접속에 실패하였거나 존재하지 않는 이벤트입니다.");
		onBack();
	}
	
	$('.date-picker').datepicker({
		container: 'html',
   		autoclose: true
	});		

	try {
        new AjaxUpload($("#btn_profile_img"), {
            action: "<c:url value="/file/upload"/>",
            name: 'uploadfile',
            responseType: "json",
            onComplete: function (file, response) {
                if (response.result === "ok") {
                    $("#profile_img_pad").removeClass("hidden");
                    $("#img_preview1").attr("href", response.fileURL);
                    $("#img_profile").attr("src", response.fileURL);
                    $("#profile_img").val(response.fileName);
                } else {
                    $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                    return;
                }
            }
        })
    } catch (e) {
        alert(e);
    }
    
    try {
        new AjaxUpload($("#btn_event_img"), {
            action: "<c:url value="/file/upload"/>",
            name: 'uploadfile',
            responseType: "json",
            onComplete: function (file, response) {
                if (response.result === "ok") {
                    $("#event_img_pad").removeClass("hidden");
                    $("#img_preview2").attr("href", response.fileURL);
                    $("#img_event").attr("src", response.fileURL);
                    $("#event_img").val(response.fileName);
                } else {
                    $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                    return;
                }
            }
        })
    } catch (e) {
        alert(e);
    }
});

$(".period").change(function () {
	onPeriodChange2($(this).val());
});

function delete_event(){
	$.ajax({
		url: "<c:url value="/system/event_mng/delete_event" />",
		type: "POST",
		data: {
			event_uid: $("#detail_uid").val()
		},
		success: function (result) {
			$toast = toastr["success"]('', '성과적으로  삭제되었습니다.');
			setTimeout('onBack()', 1000);
		}
	});
}

var mypageYN = 1;
if ($("#mypageYN").val() != "")
	mypageYN = $("#mypageYN").val();

$('.mypageYN').on('change', function (event) {	
	mypageYN = $(this).val();
});

function save(){
	if ($.trim($("#title").val()) == "") {
		$toast = toastr['error']('타이틀을 입력해주세요.');
		$("#title").focus();
		return;
	}
	
	if ($("#profile_img").val() == '') {
		$toast = toastr['error']('타이틀이미지를 선택하세요.');
		return;
	}
	
	if ($("#event_img").val() == '') {
		$toast = toastr['error']('이벤트이미지를 선택하세요.');
		return;
	}
	
	$.ajax({
		url: "<c:url value="/system/event_mng/save" />",
		type: "POST",
		data: {
			event_uid: $("#detail_uid").val(),
			title: $("#title").val(),
			period_from: $("#period_from").val(),
			period_to: $("#period_to").val(),
			profile_img: $("#profile_img").val(),
			detail_img: $("#event_img").val(),
			mypageYN: mypageYN,
		},
		success: function (result) {
			$toast = toastr["success"]('', '성과적으로  저장되었습니다.');
			setTimeout('onBack()', 1000);
		}
	});
}

function delete_img(obj, type) {
	if (type == 1){
		if (!confirm("타이틀이미지를 삭제하시겠습니까?"))
			return;
		$("#profile_img_pad").addClass('hidden');
		$("#profile_img").val("");
	}
	
	if (type == 2){
		if (!confirm("이벤트이미지를 삭제하시겠습니까?"))
			return;
		$("#event_img_pad").addClass('hidden');
		$("#event_img").val("");
	}
}
</script>