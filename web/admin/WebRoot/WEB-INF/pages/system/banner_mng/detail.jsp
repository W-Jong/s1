<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/banner_mng"/>" class="page_nav">배너</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<c:if test="${detail_uid == 0 }">
		<a href="#" class="page_nav">배너등록</a>
		</c:if>
		<c:if test="${detail_uid != 0 }">
		<a href="#" class="page_nav">배너수정</a>
		</c:if>
	</div>
	<c:if test="${detail_uid != 0 }">
		<div class="col-md-offset-5 col-md-1">
			<a class="btn btn-red-inline btn-block" onclick="delete_banner()">삭제</a>
		</div>
	</c:if>
</div>

<c:if test="${detail_uid == 0 }">
<div class="row" style="margin-top: 20px;">
	<h4><strong>배너등록</strong></h4>
</div>
</c:if>

<c:if test="${detail_uid != 0 }">
<div class="row">
	<h4><strong>배너수정</strong></h4>
</div>
</c:if>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">종류</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-8">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default col-md-3 ${type == 1 || detail_uid == 0 ? "active" : ""}" >
							<input type="radio" class="toggle type" id="type_notice" name="type" value="1" ${type == 1 || detail_uid == 0 ? "checked" : ""}> 공지사항 </label>
							<label class="btn btn-default col-md-3 ${type == 2 ? "active" : ""}">
							<input type="radio" class="toggle type" id="type_theme" name="type" value="2" ${type == 1 ? "checked" : ""}> 테마 </label>
							<label class="btn btn-default col-md-3 ${type == 3 ? "active" : ""}">
							<input type="radio" class="toggle type" id="type_event" name="type" value="3" ${type == 1 ? "checked" : ""}> 이벤트 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">불러오기</td>
				<td colspan="3">
					<div class="col-md-2">
						<a class="btn btn-block btn-dark" id="btn_get_list" onclick="get_list();">목록</a>
					</div>
					<div class="col-md-6" id="div_select_title" style="padding-top:8px;">${select_title }</div>
					<input type="hidden" id="select_uid" value="${select_uid }">
				</td>
			</tr>
			<tr>
				<td class="td-label">타이틀</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="title" value="${title }"/>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">노출기간</td>
				<td>
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from" value="${start_time }">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to" value="${end_time }">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">배너이미지</td>
				<td>
					<div class="col-md-2">
						<button class="btn btn-block btn-dark" id="btn_profile_img">사진찾기</button>
	    			</div>
				</td>
			</tr>
			<tr id="profile_img_pad" <c:if test="${profile_img==null || profile_img.isEmpty() }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="col-md-2 portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px; padding-left: ' + padding_left + 'px">
						<div class="img_pad_bottom portlet-title">
							<a onclick='onImageClick("${real_profile_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview"> 
                   			<img id="img_profile" src="${real_profile_img }" width="150px" height="150px"></img>
                   			</a>
                   			<div class="img_pad_this" onclick="delete_img(this)">
                        		<i style="color: white;" class="fa fa-times"></i>
                            </div>
                           </div>
                    <div class="portlet portlet-sortable-empty"></div></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp;뒤로</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark btn-block" onclick="save()">
		<c:if test="${detail_uid == 0 }">등록</c:if>
		<c:if test="${detail_uid != 0 }">수정</c:if>
		</a>
	</div>
</div>

<input type="hidden" id="detail_uid" value="${detail_uid }"/>
<input type="hidden" id="profile_img" name="profile_img" value="${profile_img }"/>

<a class="btn default hidden" data-toggle="modal" href="#large" id="btn_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
<a class="btn default hidden" data-dismiss="modal" id="btn_cancel_modal">View Demo </a>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="width: 20%;">검색</td>
								<td style="border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="search_content"/>
									</div>
								</td>
								<td style="border-right: none;">
									<div class="col-md-12" id="detail_reg_time">
										<a class="btn btn-block btn-dark" id="btn_search">검색</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="row">
					<table class="table table-bordered" id="list">
						<thead>
							<tr>
								<th>타이틀</th>
								<th>등록일</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script>

var ajax_table;
$(document).ready(function(){
	$(".page-sidebar-menu li:eq(17)").addClass("active");
	$(".page-sidebar-menu li:eq(22)").addClass("active");
	$(".nav-tabs li:eq(4)").addClass("active");
	
	if ('${msg}' == 'error')
	{
		alert("DB 접속에 실패하였거나 존재하지 않는 배너입니다.");
		onBack();
	}
	
	$('.date-picker').datepicker({
   		container: 'html',
    	autoclose: true
    });
    PortletDraggable.init();
	ajax_table = $("#list").DataTable({
		dom : 'tp',
		"stateSave": false,
           "autowidth": true,
           "serverSide": true,
           "ordering": false,
           "language" : {
              	"emptyTable": '<center>데이터가 없습니다.</center>',
           },
           "deferRender": true,
           "ajax": {
               "url": "<c:url value='/system/banner_mng/list_ajax_table'/>",
               "type": "POST",
               "data":   function ( d ) {
                   start_index = d.start;
                   d.search_content = $("#search_content").val();
                   d.type = type;
               }
           },
           "pageLength": 100,
          	"createdRow": function (row, data, dataIndex) {
          		$('td:eq(2)', row).html('선택');
          		$('td:eq(2)', row).attr('onclick', 'select_list(type, ' + data[2] + ')');
          		$('td:eq(2)', row).css('cursor', 'pointer');
          	}
	});
	
	try {
        new AjaxUpload($("#btn_profile_img"), {
            action: "<c:url value="/file/upload"/>",
            name: 'uploadfile',
            responseType: "json",
            onComplete: function (file, response) {
                if (response.result === "ok") {
                    $("#profile_img_pad").removeClass("hidden");
                    $("#img_preview").attr("onclick", "onImageClick('" + response.fileURL + "')");
                    $("#img_profile").attr("src", response.fileURL);
                    $("#profile_img").val(response.fileName);
                } else {
                    $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                    return;
                }
            }
        })
    } catch (e) {
        alert(e);
    }
});

function delete_img(obj) {
	if (!confirm("이미지를 삭제하시겠습니까?"))
		return;
	$('#profile_img_pad').addClass('hidden');
	$("#profile_img").val("");
}

var type = 1;
$('.type').on('change', function (event) {	
	type = $(this).val();
	
	$("#select_uid").val("");
	$("#div_select_title").html("");
});

function get_list(){
	ajax_table.draw(false);
	$("#btn_modal").trigger("click");
}

$("#btn_search").click(function(){
	ajax_table.draw(false);
})

function select_list(type, uid){
	$.ajax({
		url: "<c:url value="/system/banner_mng/select" />",
		type: "POST",
		data: {
			type: type,
			uid: uid,
		},
		success: function (result) {
			var json = result;
			
			$("#select_uid").val(json[0].uid);
			$("#div_select_title").html(json[0].title);
		}
	});
	
	$("#btn_cancel_modal").trigger('click');
}

$(".period").change(function () {
	onPeriodChange2($(this).val());
});

function delete_banner(){
	if (!confirm("선택한 배너를 삭제하시겠어요?"))
		return;
		
	$.ajax({
		url: "<c:url value="/system/banner_mng/delete_banner" />",
		type: "POST",
		data: {
			banner_uid: $("#detail_uid").val()
		},
		success: function (result) {
			$toast = toastr["success"]('', '성과적으로 삭제되었습니다.');
			setTimeout('onBack()', 1000);
		}
	});
}

function save(){
	if ($("#select_uid").val() == "") {
		$toast = toastr['error']('', '목록을 선택하세요.');
		return;
	}
	
	if ($("#title").val() == ""){
		$toast = toastr['error']('', '타이틀을 입력하세요.');
		$("#title").focus();
		return;
	}
	
	if ($("#profile_img").val() == '') {
		$toast = toastr['error']('', '배너이미지를 선택하세요.');
		return;
	}
	
	$.ajax({
		url: "<c:url value="/system/banner_mng/save" />",
		type: "POST",
		data: {
			banner_uid: $("#detail_uid").val(),
			type: type,
			select_uid: $("#select_uid").val(),
			title: $("#title").val(),
			period_from: $("#period_from").val(),
			period_to: $("#period_to").val(),
			profile_img: $("#profile_img").val()
		},
		success: function (result) {
			$toast = toastr["success"]('', '성과적으로 저장되었습니다.');
			setTimeout('onBack()', 1000);
		}
	});
}

</script>