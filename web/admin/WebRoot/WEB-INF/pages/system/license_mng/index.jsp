<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/system/reply_mng"/>">운영</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/system/license_mng"/>" class="page_nav">정책</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4><strong>정책</strong></h4>
</div>

<form id="license_form">
<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">이용약관</td>
				<td style="border-top-color: black;" colspan="3">
					<textarea rows="15" cols="" class="form-control" id="license" name="license">${license[0].content }</textarea>
				</td>
			</tr>
			<tr>
				<td class="td-label">개인정보<br/>취급방침</td>
				<td colspan="3">
					<textarea rows="15" cols="" class="form-control" id="license" name="license">${license[1].content }</textarea>
				</td>
			</tr>
			<tr>
				<td class="td-label">판매 및 <br>환불 약관</td>
				<td>
					<textarea rows="15" cols="" class="form-control" id="license" name="license">${license[2].content }</textarea>
				</td>
			</tr>
			<tr>
				<td class="td-label">정품 보증<br>100%</td>
				<td>
					<textarea rows="15" cols="" class="form-control" style="text-align:center;" id="license" name="license">${license[3].content }</textarea>
				</td>
			</tr>
			<tr>
				<td class="td-label">구매자 보호<br>에스크로</td>
				<td>
					<textarea rows="15" cols="" class="form-control" style="text-align:center;" id="license" name="license">${license[4].content }</textarea>
				</td>
			</tr>
			<tr>
				<td class="td-label">배송 및 반품</td>
				<td>
					<textarea rows="15" cols="" class="form-control" style="text-align:center;" id="license" name="license">${license[5].content }</textarea>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</form>
<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="update()">수정</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>
<script>
$(document).ready(function () {
	$(".page-sidebar-menu li:eq(17)").addClass("active");
	$(".page-sidebar-menu li:eq(27)").addClass("active");
	$(".nav-tabs li:eq(9)").addClass("active");
});

function update(){
	$.ajax({
		url: "<c:url value="/system/license_mng/update_license" />",
		type: "POST",
		data: $("#license_form").serialize(),
		success: function (result) {
			$toast = toastr["success"]('', '성과적으로  저장되었습니다.');
		}
	});
}

function onInitialize(){
	location.href = "<c:url value="/system/license_mng/index"/>";
}
</script>