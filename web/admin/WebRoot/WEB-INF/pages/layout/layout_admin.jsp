<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>SelluvAdmin</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<link href="<c:url value="/resources/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/icheck/skins/all.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/jstree/dist/themes/default/style.min.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/css/components.css"/>" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/css/plugins.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/admin/layout/css/layout.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/admin/layout/css/themes/darkblue.css"/>" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<c:url value="/resources/admin/layout/css/custom.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/select2/select2.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/bootstrap-toastr/toastr.min.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>" rel="stylesheet" type="text/css"/>

	<link rel="shortcut icon" href="favicon.ico"/>
	
	<script src="<c:url value="/resources/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/icheck/icheck.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/jquery-ui/jquery-ui.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/scripts/metronic.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/datatables/media/js/jquery.dataTables.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"/>" type="text/javascript"></script>
	
	
	<script src="<c:url value="/resources/admin/layout/scripts/layout.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/admin/layout/scripts/quick-sidebar.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/admin/layout/scripts/demo.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/admin/layout/scripts/ajaxupload.3.6.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/admin/pages/scripts/portlet-draggable.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/bootstrap-toastr/toastr.min.js"/>"></script>
	<script src="<c:url value="/resources/global/plugins/bootbox/bootbox.min.js"/>"></script>
	
	<style>
		.page-sidebar .page-sidebar-menu > li.start > a {
			background: #5d9bfc;
		}
		
		.page-sidebar .page-sidebar-menu > li.start > a:hover {
			background: #5d9bfc;
		}
	
		.page-sidebar .page-sidebar-menu > li.active > a {
			border-left: 3px solid #5d9bfc;
			background: #2f303a;
			padding-left: 8px !important;
		}
		
		.page-sidebar .page-sidebar-menu > li.active > a:hover {
			border-left: 3px solid #5d9bfc;
			background: #2f303a;
			padding-left: 8px;
		}
		
		.page-sidebar .page-sidebar-menu > li > a {
			height: 50px;
			padding: 15px 15px;
			border-top-color: transparent;
		}

		.page-container-bg-solid .page-content {
			background: #f8f8fa;
		}
		
		.page-sidebar-closed .page-sidebar .page-sidebar-menu.page-sidebar-menu-closed > li:hover {
			width: 45px !important;
			box-shadow: none;
		}
		
		.page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover, .page-sidebar {
			background: #2f303a;
		}
		
		body {
			background: #2f303a;
			min-width: 1245px;
			overflow-x: auto;
		}
		
		.page-sidebar-closed .page-sidebar .page-sidebar-menu.page-sidebar-menu-closed > li:hover > .sub-menu {
			background: #2f303a;
			left: 45px;
			top: 0;
		}
		
		.page-sidebar-menu.page-sidebar-menu-closed > li:hover > .sub-menu {
			box-shadow: none;
		}
		
		.page-sidebar .page-sidebar-menu .sub-menu li > a {
			padding-top: 15px;
			padding-bottom: 15px;
			color: #b2b9ca;
		}
		
		.page-sidebar .page-sidebar-menu .sub-menu > li:hover > a {
			color: white;
		}
		
		.page-sidebar .page-sidebar-menu > li > a > i {
			color: #b2b9ca;
		}
		
		.page-sidebar .page-sidebar-menu > li:hover > a > i,
		.page-sidebar .page-sidebar-menu > li:active > a > i {
			color: white !important;
		}

		.sub-menu {
			width: 120px !important;
		}
		
		.menu-item-center {
			font-size: 12px !important;
			padding-left: 0 !important;
			padding-right: 0 !important;
			text-align: center;
		}
		
		.page-bar {
			height: 50px;
			padding: 13px 0 !important;
			position: fixed !important;
			width: 100%;
			min-width: 1200px !important;
			z-index: 1000;
			margin: -25px -20px 0 0 !important;
		}
		
		.page-content-wrapper .page-content {
			padding-left: 0 !important;
			padding-right: 0 !important;
		}
		
		.header_title {
			float: left;
			padding: 0 30px;
		}
		
		.nav-tabs > li.active > a,
		.nav-tabs > li.active > a:focus,
		.nav-tabs > li.active > a:hover {
			background: #f8f8fa;
			color: #5d9bfc;
		}
				
		.nav > li > a {
			padding: 8px 40px 8px 20px;
			color: #888;
		}
		
		.page-bar .nav-tabs > li > a:before {
			content: "";
		    position: absolute;
		    top: -1px;
		    right: 0;
		    border-right: 20px solid white;
		    border-bottom: 38px solid transparent;
	    }
	    
	    .page-bar .nav-tabs > li > a:after {
	    	content: "";
		    position: absolute;
		    top: -2px;
		    right: -1px;
		    border-right: 20px solid white;
		    border-bottom: 38px solid transparent;
	    }
	    
	    .page-bar .nav-tabs > li.active > a:before {
	    	border-right-color: #ddd;
	    }

		.btn-dark {
			transition: 0.3s;
			background: RGB(89,89,89);
			border: 1px solid RGB(89,89,89);
			color: white;
		}
		
		.btn-dark:hover {
			transition: 0.3s;
			background: white;
			border: 1px solid RGB(89,89,89);
			color: RGB(89,89,89);
		}
		
		.btn-dark-inline {
			transition: 0.3s;
			background: white;
			border: 1px solid RGB(89,89,89);
			color: RGB(89,89,89);
		}
		
		.btn-dark-inline:hover {
			transition: 0.3s;
			background: RGB(89,89,89);
			border: 1px solid RGB(89,89,89);
			color: white;
		}
		
		.btn-blue {
			transition: 0.3s;
			background: #5d9bfc;
			border: 1px solid #5d9bfc;
			color: white;
		}
		
		.btn-blue:hover {
			transition: 0.3s;
			background: white;
			border: 1px solid #5d9bfc;
			color: #5d9bfc;
		}
		
		.btn-blue-inline {
			transition: 0.3s;
			background: white;
			border: 1px solid #5d9bfc;
			color: #5d9bfc;
		}
		
		.btn-blue-inline:hover {
			transition: 0.3s;
			background: #5d9bfc;
			border: 1px solid #5d9bfc;
			color: white;
		}

		.btn-red {
			transition: 0.3s;
			background: #ff3300;
			border: 1px solid #ff3300;
			color: white;
		}
		
		.btn-red:hover {
			transition: 0.3s;
			background: white;
			border: 1px solid #ff3300;
			color: #ff3300;
		}
		
		.btn-red-inline {
			transition: 0.3s;
			background: white;
			border: 1px solid #ff3300;
			color: #ff3300;
		}
		
		.btn-red-inline:hover {
			transition: 0.3s;
			background: #ff3300;
			border: 1px solid #ff3300;
			color: white;
		}
		
		.loading-overlay {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            background: white;
            opacity: 0.5;
            filter: alpha(opacity=60);
            z-index: 100000;
            display: none;
        }
        
        .spinner {
            position: absolute;
            left: 50%;
            top: 50%;
            width: 40px;
            height: 40px;
            margin-left: -15px;
            margin-top: -15px;
            vertical-align: middle;
            border: 0;
            /*outline: none !important;*/
        }
        
        .page-sidebar {
        	border-top: none;
        }
        
        .close {
        	background-image: none !important;
    		text-indent: 0;
    		width: 20px;
    		height: 20px;
        }
        
        .modal .modal-header .close {
        	margin-top: -10px !important;
        	margin-right: -10px !important;
        }
	</style>
</head>

<body class="page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed">

	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper" style="position: fixed; z-index: 10000;">
			<div class="page-sidebar navbar-collapse">
				<ul class="page-sidebar-menu page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
					<li class="start" style="box-shadow: none;" id="menu_dashboard">
						<a href="<c:url value="/dashboard"/>">
						<i class="icon-home" style="color: white;"></i>
						</a>
					</li>
					<li>
						<a href="<c:url value="/pdt/pdt_mng"/>">
						<i class="icon-handbag"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/pdt/pdt_mng"/>">리스트</a>
							</li>
							<li>
								<a href="<c:url value="/pdt/valet_mng"/>">발렛관리</a>
							</li>
							<li>
								<a href="<c:url value="/pdt/pdt_class"/>">상품분류</a>
							</li>
						</ul>
					</li>
					
					<li id="menu_usr">
						<a href="<c:url value="/usr/usr_mng"/>">
						<i class="icon-user"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/usr/usr_mng"/>">리스트</a>
							</li>
						</ul>
					</li>
					
					<li id="menu_deal">
						<a href="<c:url value="/deal/deal_mng/0/index"/>">
						<i class="fa fa-truck"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/deal/deal_mng/0/index"/>">리스트</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/1/index"/>">배송준비</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/2/index"/>">배송진행</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/3/index"/>">정품인증</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/4/index"/>">배송완료</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/5/index"/>">거래완료</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/6/index"/>">정산완료</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/7/index"/>">반품과정</a>
							</li>
							<li>
								<a href="<c:url value="/deal/deal_mng/8/index"/>">주문취소</a>
							</li>
						</ul>
					</li>
					
					<li>
						<a href="<c:url value="/system/reply_mng/index"/>">
						<i class="fa fa-file-text-o"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/system/reply_mng/index"/>">댓글</a>
							</li>
							<li>
								<a href="<c:url value="/system/report_mng/index"/>">신고</a>
							</li>
							<li>
								<a href="<c:url value="/system/qna_mng/index"/>">문의</a>
							</li>
							<li>
								<a href="<c:url value="/system/review_mng/index"/>">후기</a>
							</li>
							<li>
								<a href="<c:url value="/system/banner_mng/index"/>">배너</a>
							</li>
							<li>
								<a href="<c:url value="/system/theme_mng/index"/>">테마</a>
							</li>
							<li>
								<a href="<c:url value="/system/notice_mng/index"/>">공지</a>
							</li>
							<li>
								<a href="<c:url value="/system/event_mng/index"/>">이벤트</a>
							</li>
							<li>
								<a href="<c:url value="/system/faq_mng/index"/>">FAQ</a>
							</li>
							<li>
								<a href="<c:url value="/system/license_mng/index"/>">정책</a>
							</li>
						</ul>
					</li>
					
					<li id="menu_brand">
						<a href="<c:url value="/brand/regular_brand_mng"/>">
						<i class="fa icon-pencil"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/brand/regular_brand_mng"/>">정규</a>
							</li>
							<li>
								<a href="<c:url value="/brand/new_brand_mng"/>">신규</a>
							</li>
							<li>
								<a href="<c:url value="/brand/recommend_brand_mng"/>">추천</a>
							</li>
						</ul>
					</li>
					
					<li id="menu_reward">
						<a href="<c:url value="/reward/report_reward_mng"/>">
						<i class="fa fa-money"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/reward/report_reward_mng"/>">가품</a>
							</li>
							<li>
								<a href="<c:url value="/reward/share_reward_mng"/>">공유</a>
							</li>
							<li>
								<a href="<c:url value="/reward/code_reward_mng"/>">코드</a>
							</li>
							<li>
								<a href="<c:url value="/reward/invite_reward_mng"/>">초대</a>
							</li>
							<li>
								<a href="<c:url value="/reward/set_reward_mng"/>">설정</a>
							</li>
						</ul>
					</li>
					
					<li id="menu_statistics">
						<a href="<c:url value="/statistics/deal_statistics"/>">
						<i class="fa fa-bar-chart-o"></i>
						</a>
						<ul class="sub-menu">
							<li>
								<a href="<c:url value="/statistics/deal_statistics"/>">거래</a>
							</li>
							<li>
								<a href="<c:url value="/statistics/visit_statistics"/>">방문</a>
							</li>
							<li>
								<a href="<c:url value="/statistics/pdt_statistics"/>">상품</a>
							</li>
							<li>
								<a href="<c:url value="/statistics/usr_statistics"/>">유저</a>
							</li>
						</ul>
					</li>
					
					<li style="position: fixed !important; bottom: 0; width: 45px;">
						<a href="javascript:;" class="menu-item-center">admin</a>
						<ul class="sub-menu" style="margin-top: -50px;">
							<li>
								<a href="#" onclick="onPwdChange()">비번변경</a>
							</li>
							<li>
								<a href="<c:url value="/login/logout"/>">로그아웃</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<tiles:insertAttribute name="header" ignore="true" />
				<tiles:insertAttribute name="body" ignore="true" />
			</div>
		</div>
	</div>
	<!-- END CONTAINER -->
	
	
    <div class="loading-overlay">
        <img class="spinner" src="<c:url value="/resources/admin/layout/img/ajax-loading.gif"/>">
    </div>
    
    <a class="btn default hidden" data-toggle="modal" href="#profile_modal" id="btn_profile_modal">View Demo </a>
	<div class="modal fade" id="profile_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn_pwd_close"></button>
					<h4 class="modal-title"><strong>비밀번호 변경</strong></h4>
				</div>
				<div class="modal-body">
					<div class="row" style="margin-top: 20px;">
						<div class="col-md-12">
							<input type="password" class="form-control" id="old_pwd" name="old_pwd" placeholder="이전 비밀번호"/>
						</div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<div class="col-md-12">
							<input type="password" class="form-control" id="new_pwd" name="new_pwd" placeholder="비밀번호"/>
						</div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<div class="col-md-12">
							<input type="password" class="form-control" id="new_confirm_pwd" name="new_confirm_pwd" placeholder="비밀번호 확인"/>
						</div>
					</div>
					<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
						<div class="col-md-offset-4 col-md-4">
							<a class="btn btn-blue btn-block" id="btn_pwd_save">저장</a>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
	
	<a class="btn default hidden" data-toggle="modal" href="#image_modal" id="btn_image_modal">View Demo </a>
	<div class="modal fade" id="image_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" id="img_preview_content" style="padding: 0 20px 20px 20px; width: 640px; height: 640px;">
				<div class="modal-header" style="height: 20px; padding-top: 10px; padding-right: 10px; margin-right: -20px;">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				</div>
				<div class="modal-body">
					<img src="" id="img_preview_all"/>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
		

	<script>
		$(document).ready(function() {    
			Metronic.init(); // init metronic core componets
			Layout.init(); // init layout
			QuickSidebar.init(); // init quick sidebar
			Demo.init(); // init demo features
			$(".close").html('<i class="fa fa-times" style="font-size: 20px;"></i>');
		});
		
		function onPeriodChange(value) {
			var date = new Date(<%(new Date()).getTime();%>);
			var date2;
			var mili = date.getTime();
			if (value == 1) {
				date2 = date;
			} else if (value == 2) {
				date2 = new Date(mili - 1000*60*60*24*7);
			} else if (value == 3) {
				date2 = new Date(mili - 1000*60*60*24*15);
			} else if (value == 4) {
				date2 = new Date(mili - 1000*60*60*24*30);
			} else if (value == 5) {
				date2 = new Date(mili - 1000*60*60*24*30*3);
			} else {
				$("#period_to").val("");
				$("#period_from").val("");
				return;
			}
			var y = date.getYear()+1900;
			var m = date.getMonth()+1;
			var d = date.getDate();
			if (m < 10)
				m = '0' + m;
			if (d < 10)
				d = '0' + d;
			var date_str = y + '-' + m + '-' + d;
			$("#period_to").val(date_str);
			y = date2.getYear() + 1900;
			m = date2.getMonth() + 1;
			d = date2.getDate();
			if (m < 10)
				m = '0' + m;
			if (d < 10)
				d = '0' + d;
			date_str = y + '-' + m + '-' + d;
			$("#period_from").val(date_str);
		}
		
		function onPeriodChange2(value) {
			var date = new Date(<%(new Date()).getTime();%>);
			var date2;
			var mili = date.getTime();
			if (value == 1) {
				date2 = date;
			} else if (value == 2) {
				date2 = new Date(mili + 1000*60*60*24*7);
			} else if (value == 3) {
				date2 = new Date(mili + 1000*60*60*24*15);
			} else if (value == 4) {
				date2 = new Date(mili + 1000*60*60*24*30);
			} else if (value == 5) {
				date2 = new Date(mili + 1000*60*60*24*30*3);
			} else {
				$("#period_to").val("");
				$("#period_from").val("");
				return;
			}
			var y = date.getYear()+1900;
			var m = date.getMonth()+1;
			var d = date.getDate();
			if (m < 10)
				m = '0' + m;
			if (d < 10)
				d = '0' + d;
			var date_str = y + '-' + m + '-' + d;
			$("#period_from").val(date_str);
			y = date2.getYear() + 1900;
			m = date2.getMonth() + 1;
			d = date2.getDate();
			if (m < 10)
				m = '0' + m;
			if (d < 10)
				d = '0' + d;
			date_str = y + '-' + m + '-' + d;
			$("#period_to").val(date_str);
		}
		
		var length = 9;
		function uid_format(uid) {
			var origin_length = 1;
			var temp = uid;
			while (temp/10 > 1) {
				origin_length++;
				temp = temp/10;
			}
			var result = '';
			for (var i=0;i<length-origin_length;i++)
				result += '0';
			result += uid;
			return result;
		}
		
		function go_url(url) {
			var win = window.open(url, '_blank');
			win.focus();
		}
		
		var loadingOverlay=function(){
            var winHeight=window.innerHeight;
            $('.loading-overlay').show();
        }

        var loadingOverlayRemove=function(){
            $('.loading-overlay').hide();
        }
		
		function onPwdChange() {
			$("#btn_profile_modal").trigger('click');
		}
		
		$("#btn_pwd_save").click(function () {
			if ($("#old_pwd").val() == '') {
				$toast = toastr['error']('', '이전 비밀번호를 입력하세요.');
				$("#old_pwd").focus();
				return;
			}
		
			if ($("#new_pwd").val() == '') {
				$toast = toastr['error']('', '비밀번호를 입력하세요.');
				$("#new_pwd").focus();
				return;
			}
			
			if ($("#new_pwd").val() != $("#new_confirm_pwd").val()) {
				$toast = toastr['error']('', '비밀번호를 다시 확인해주세요.');
				$("#new_confirm_pwd").focus();
				return;
			}
			
			$.post('<c:url value="/pwd_change"/>', {new_pwd: $("#new_pwd").val(), old_pwd: $("#old_pwd").val()}, 
				function (result) {
					if (result == "success") {
						$("#btn_pwd_close").trigger('click');
						$toast = toastr['success']('', '정확히 변경되었습니다.');
					} else if (result == "wrong") {
						$("#old_pwd").focus();
						$toast = toastr['error']('', '이전 비밀번호가 맞지 않습니다.');
					} else {
						$toast = toastr['error']('', '실패했습니다.');
					}
				}
			);
		});
		
		function onBack() {
			history.back();
			window.close();
		}
		
		function onImageClick(src) {
			$("#img_preview_all").css('margin-top', 0).css('margin-left', 0);
//			$("#img_preview_content").css('width', 640).css('height', 640);
			var img = $('<img src="' + src + '">');
			var img_width = img[0].width;
			var img_height = img[0].height;
			var width = 600, height = 600;
			if(img_width > img_height) {
				$("#img_preview_all").css('width', width).css('height', img_height * width / img_width).css('margin-top', (width - img_height * width / img_width) / 2)
//				$("#img_preview_content").css('width', width + 40).css('height', img_height * width / img_width + 40);
			} else {
				$("#img_preview_all").css('height', height).css('width' , img_width * height / img_height).css('margin-left', (width - img_width * width / img_height) / 2);
//				$("#img_preview_content").css('height', height + 40).css('width' , img_width * height / img_height + 40);
			}
			$("#img_preview_all").attr("src", src);
			$("#btn_image_modal").trigger("click");
		}
		
		$(document).scroll(function () {
			$(".page-bar").css('left', -$(this).scrollLeft() + 45);
		});
	</script>
</body>

</html>
