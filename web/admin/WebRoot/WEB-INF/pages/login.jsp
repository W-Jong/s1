<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE HTML>
<html>
<head>

	<meta charset="utf-8"/>
	<title>로그인</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<link href="<c:url value="/resources/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/admin/pages/css/login.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/css/components.css"/>" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<c:url value="/resources/global/plugins/bootstrap-toastr/toastr.min.css"/>" rel="stylesheet" type="text/css"/>

	<style>
		.login {
			margin-top: 150px !important;
		}
	</style>
</head>

<!-- BEGIN BODY -->
<body class="login">
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler">
	</div>
	<!-- END SIDEBAR TOGGLER BUTTON -->
	
	<!-- BEGIN LOGIN -->
	<div class="content">
		<form class="login-form" id="login_form">
			<h3 class="form-title">관리자로그인</h3>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Username</label>
				<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="아이디" id="adm_id"/>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="비밀번호" id="adm_pwd"/>
			</div>

			<div class="create-account" id="btn_login" style="cursor: pointer;">
				<p>
					<a href="#" class="uppercase" style="font-size: 20px; margin-top: 0;">로그인</a>
				</p>
			</div>
		</form>
	</div>
	<!-- END LOGIN -->

	<script src="<c:url value="/resources/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/global/plugins/bootstrap-toastr/toastr.min.js"/>"></script>

	<script>
		$("#btn_login").click(function () {
			if ($("#adm_id").val() == ""){
				$toast = toastr["error"]('', '관리자아이디를 입력하세요.');
				$("#adm_id").focus();
				return;
			}
			
			if ($("#adm_pwd").val() == ""){
				$toast = toastr["error"]('', '비밀번호를 입력하세요.');
				$("#adm_pwd").focus();
				return;
			}
			
			$.ajax({
				url: "<c:url value="/login/verify" />",
				type: "POST",
				data: {
					id: $("#adm_id").val(),
					pwd: $("#adm_pwd").val()
				},
				success: function (result) {
					if (result == 1) {
						location.href = "<c:url value="${referer}" />";
					} else {
						$toast = toastr["error"]('', '관리자정보가 정확하지 않습니다.');
					}
				}
			});
		});
		
		$("#login_form").keyup(function(event){
	        if (event.keyCode == 13) {
	            $("#btn_login").trigger('click');
	        }
	    });
	</script>
</body>

</html>
