<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: #5d9bfc;
		border-color: #5d9bfc;
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
    a.img-remove{
        width: 17px;
	    height: 17px;
	    position: absolute;
	    right: 3px;
		top: 3px;
	    background: black;
	    text-align: center;
	    border-radius: 50% !important;
	    line-height: 17px;
	    text-decoration: none;
    }
    
    a.img-remove:hover,
    a.img-remove:active,
    a.img-remove:focus {
    	color: white !important;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/brand/regular_brand_mng"/>">브랜드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/brand/regular_brand_mng"/>" class="page_nav">정규브랜드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		<c:choose>
			<c:when test="${brand == null }">
			&nbsp;&nbsp;<a href="#" class="page_nav">브랜드등록</a>
			</c:when>
			<c:otherwise>
			&nbsp;&nbsp;<a href="#" class="page_nav">브랜드수정</a>
			</c:otherwise>
		</c:choose>
	</div>
	<c:choose>
		<c:when test="${brand == null }">
		</c:when>
		<c:otherwise>
			<div class="col-md-offset-5 col-md-1">
				<a class="btn btn-red-inline btn-block" onclick="onDelete()">삭제</a>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<div class="row">
	<c:choose>
		<c:when test="${brand == null }">
		<h4><strong>브랜드등록</strong></h4>
		</c:when>
		<c:otherwise>
		<h4><strong>브랜드수정</strong></h4>
		</c:otherwise>
	</c:choose>
</div>

<div class="row">
	<form id="brand_form" method="post">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">영문</td>
				<td style="border-top-color: black;">
					<div class="col-md-2">
						<select class="form-control select" id="first_en" name="first_en">
							<option value="">A-Z</option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							<option value="D">D</option>
							<option value="E">E</option>
							<option value="F">F</option>
							<option value="G">G</option>
							<option value="H">H</option>
							<option value="I">I</option>
							<option value="J">J</option>
							<option value="K">K</option>
							<option value="L">L</option>
							<option value="M">M</option>
							<option value="N">N</option>
							<option value="O">O</option>
							<option value="P">P</option>
							<option value="Q">Q</option>
							<option value="R">R</option>
							<option value="S">S</option>
							<option value="T">T</option>
							<option value="U">U</option>
							<option value="V">V</option>
							<option value="W">W</option>
							<option value="X">X</option>
							<option value="Y">Y</option>
							<option value="Z">Z</option>
						</select>
					</div>
					<div class="col-md-3">
						<input type="text" class="form-control" id="name_en" name="name_en" value="${brand.nameEn }"/>
					</div>
				</td>
			</tr>
			
			<tr>
				<td class="td-label">한글</td>
				<td>
					<div class="col-md-2">
						<select class="form-control select" id="first_ko" name="first_ko">
							<option value="">ㄱ-ㅎ</option>
							<option value="ㄱ">ㄱ</option>
							<option value="ㄴ">ㄴ</option>
							<option value="ㄷ">ㄷ</option>
							<option value="ㄹ">ㄹ</option>
							<option value="ㅁ">ㅁ</option>
							<option value="ㅂ">ㅂ</option>
							<option value="ㅅ">ㅅ</option>
							<option value="ㅇ">ㅇ</option>
							<option value="ㅈ">ㅈ</option>
							<option value="ㅊ">ㅊ</option>
							<option value="ㅋ">ㅋ</option>
							<option value="ㅌ">ㅌ</option>
							<option value="ㅍ">ㅍ</option>
							<option value="ㅎ">ㅎ</option>
						</select>
					</div>
					<div class="col-md-3">
						<input type="text" class="form-control" id="name_ko" name="name_ko" value="${brand.nameKo }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">라이센스</td>
				<td>
					<div class="col-md-5">
						<input type="text" class="form-control" id="license_url" name="license_url" value="${brand.licenseUrl }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">로고이미지</td>
				<td>
					<div class="col-md-4">
						<a class="btn btn-dark col-md-4" id="btn_logo_img">사진찾기</a>
					</div>
				</td>
			</tr>
			<tr id="logo_img_pad" <c:if test="${brand==null }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="col-md-12" style="padding-top: 15px; padding-bottom: 15px;">
						<div style="width: 200px; height: 200px; background: black;position:relative;">
							<a onclick='onImageClick("${real_logo_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview1">
							<img id="img_logo" src="${real_logo_img }" width="200px" height="200px"/>
							</a>
							<a class="img-remove" onclick="removeImg('logo_img',this)">x</a>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">썸네일이미지</td>
				<td>
					<div class="col-md-4">
						<a class="btn btn-dark col-md-4" id="btn_profile_img">사진찾기</a>
					</div>
				</td>
			</tr>
			<tr id="profile_img_pad" <c:if test="${brand==null }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="col-md-12" style="padding-top: 15px; padding-bottom: 15px;">
						<div style="width: 200px; height: 200px; background: white;position:relative;">
							<a onclick='onImageClick("${real_profile_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview2">
							<img id="img_profile" src="${real_profile_img }" width="200px" height="200px"/>
							</a>
							<a class="img-remove" onclick="removeImg('profile_img',this)">x</a>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">백그라운드이미지</td>
				<td>
					<div class="col-md-4">
						<a class="btn btn-dark col-md-4" id="btn_back_img">사진찾기</a>
					</div>
				</td>
			</tr>
			<tr id="back_img_pad" <c:if test="${brand==null }">class="hidden"</c:if>>
				<td colspan="2">
					<div class="col-md-12" style="padding-top: 15px; padding-bottom: 15px;">
						<div style="width: 200px; height: 200px;position:relative;">
							<a onclick='onImageClick("${real_back_img }")' class="fancybox-button" data-rel="fancybox-button" id="img_preview3">
							<img id="img_back" src="${real_back_img }" width="200px" height="200px"/>
							</a>
							<a class="img-remove" onclick="removeImg('back_img',this)">x</a>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="logo_img" name="logo_img" value="${brand.logoImg }"/>
	<input type="hidden" id="profile_img" name="profile_img" value="${brand.profileImg }"/>
	<input type="hidden" id="back_img" name="back_img" value="${brand.backImg }"/>
	<input type="hidden" id="brand_uid" name="brand_uid" value="${brand==null?0:brand.brandUid }"/>
	</form>
	<div class="row">
		<div class="col-md-1">
			<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
		</div>
		<div class="col-md-offset-5 col-md-1">
			<a class="btn btn-dark btn-block" onclick="onSave()">
				<c:choose>
					<c:when test="${brand == null }">
						등록
					</c:when>
					<c:otherwise>
						수정
					</c:otherwise>
				</c:choose>
			</a>
		</div>
	</div>
</div>

<script>
	var ajax_brand_table;
	var brand_keyword = '';
	var first_en = '';
	var first_ko = '';
	$(document).ready(function () {
		$("#menu_brand").addClass("active");
		$("#menu_brand li:eq(0)").addClass("active");
		$(".nav-tabs li:eq(0)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 브랜드입니다.");
			onBack();
		}
		
		$("#first_ko").val("${brand.firstKo}");
		$("#first_en").val("${brand.firstEn}");
		
		try {
            new AjaxUpload($("#btn_logo_img"), {
                action: "<c:url value="/file/upload"/>",
                name: 'uploadfile',
                responseType: "json",
                onComplete: function (file, response) {
                    if (response.result === "ok") {
                        $("#logo_img_pad").removeClass("hidden");
                        $("#img_preview1").attr("href", response.fileURL);
                        $("#img_logo").attr("src", response.fileURL);
                        $("#logo_img").val(response.fileName);
                    } else {
                        $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                        return;
                    }
                }
            })
        } catch (e) {
            alert(e);
        }
        
        try {
            new AjaxUpload($("#btn_profile_img"), {
                action: "<c:url value="/file/upload"/>",
                name: 'uploadfile',
                responseType: "json",
                onComplete: function (file, response) {
                    if (response.result === "ok") {
                        $("#profile_img_pad").removeClass("hidden");
                        $("#img_preview2").attr("href", response.fileURL);
                        $("#img_profile").attr("src", response.fileURL);
                        $("#profile_img").val(response.fileName);
                    } else {
                        $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                        return;
                    }
                }
            })
        } catch (e) {
            alert(e);
        }
        
        try {
            new AjaxUpload($("#btn_back_img"), {
                action: "<c:url value="/file/upload"/>",
                name: 'uploadfile',
                responseType: "json",
                onComplete: function (file, response) {
                    if (response.result === "ok") {
                        $("#back_img_pad").removeClass("hidden");
                        $("#img_preview3").attr("href", response.fileURL);
                        $("#img_back").attr("src", response.fileURL);
                        $("#back_img").val(response.fileName);
                    } else {
                        $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                        return;
                    }
                }
            })
        } catch (e) {
            alert(e);
        }
	});
	
	function onSave() {
	//check
		if ($("#first_en").val() == '') {
			$toast = toastr['error']('', '영문첫글자를 선택하세요.');
			return;
		}

		if ($("#name_en").val() == '') {
			$toast = toastr['error']('', '영문이름을 입력하세요.');
			$("#name_en").focus();
			return;
		}
		
		if ($("#first_ko").val() == '') {
			$toast = toastr['error']('', '한글첫글자를 선택하세요.');
			return;
		}

		if ($("#name_ko").val() == '') {
			$toast = toastr['error']('', '한글이름을 입력하세요.');
			$("#name_ko").focus();
			return;
		}
		
		if ($("#license_url").val() == '') {
			$toast = toastr['error']('', '라이센스를 입력하세요.');
			$("#license_url").focus();
			return;
		}
		
		/* if ($("#logo_img").val() == '') {
			$toast = toastr['error']('', '로고이미지를 선택하세요.');
			return;
		} */
		
		if ($("#profile_img").val() == '') {
			$toast = toastr['error']('', '썸네일이미지를 선택하세요.');
			return;
		}
		
		if ($("#back_img").val() == '') {
			$toast = toastr['error']('', '백그라운드이미지를 선택하세요.');
			return;
		}
		
		$.post('<c:url value="/brand/regular_brand_mng/"/>' + $("#brand_uid").val() + '/save', $("#brand_form").serialize(),
			function (result) {
				if (result == "success") {
					$toast = toastr['success']('', '성과적으로 저장되었습니다.');
					setTimeout('onBack()', 1000);
				} else {
					$toast = toastr['error']('', '실패했습니다.');
				}
			}
		);
	}
	
	function onDelete() {
		if(!confirm("선택한 브랜드를 삭제하시겠어요?"))
			return;
			
		$.post('<c:url value="/brand/regular_brand_mng/"/>' + $("#brand_uid").val() + '/remove', {},
			function (result) {
				if (result == "success") {
					$toast = toastr['success']('', '성과적으로 삭제되었습니다.');
					setTimeout('onBack()', 1000);
				} else {
					$toast = toastr['error']('', '실패했습니다.');
				}
			}
		);
	}
	
	function removeImg(id, obj) {
		if (!confirm("해당 이미지를 삭제하시겠습니까?"))
			return;
		$('#' + id).val('');
		$(obj).closest('tr').addClass('hidden');
	}
</script>
