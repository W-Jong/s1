<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="page-bar">
	<div class="header_title">
		<strong><span style="font-size: 20px; color: black;">브랜드</span></strong>
	</div>
	<ul class="nav nav-tabs">
		<li>
			<a href="<c:url value="/brand/regular_brand_mng"/>" style="cursor: pointer;">
			정규 </a>
		</li>
		<li>
			<a href="<c:url value="/brand/new_brand_mng"/>" style="cursor: pointer;">
			신규 </a>
		</li>
		<li>
			<a href="<c:url value="/brand/recommend_brand_mng"/>" style="cursor: pointer;">
			추천 </a>
		</li>
	</ul>
</div>

<script>
	$(document)
	  .ajaxStart(function() {
	    loadingOverlay();
	}).ajaxStop(function() {
	    loadingOverlayRemove();
	});
</script>
