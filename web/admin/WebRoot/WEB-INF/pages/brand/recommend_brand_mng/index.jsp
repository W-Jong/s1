<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .div-label {
    	background: #ebeef3 !important;
    	padding-left: 20px !important;
    	color: black;
    	height: 400px;
    	padding-top: 190px;
    }
    
    .col-md-1, .col-md-3 {
    	padding-left: 0;
    	padding-right: 0;
    }
    
    .list-group-item {
    	height: 50px;
    	color: black !important;
    }
    
    .list-group-item.active,
    .list-group-item.active:focus,
    .list-group-item.active:hover {
    	background: RGB(0,148,200);
    	color: white !important;
    }
    
    .table-custom > tbody > tr > td {
    	border-top: none;
    }
    
    .list-group {
    	margin-bottom: 0;
    	height: 400px;
    	overflow-y: auto;
    	background: white;
    }
    
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table-custom > tbody > tr > td {
    	background: transparent !important;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: #5d9bfc;
		border-color: #5d9bfc;
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/brand/regular_brand_mng"/>">브랜드</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/brand/recommend_brand_mng"/>" class="page_nav">추천브랜드</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4 style="margin-bottom: 0; padding-bottom: 10px; border-bottom: 1px solid black;"><strong>추천브랜드</strong></h4>
</div>

<div class="row">
	<div class="col-md-1 div-label">
		남성
	</div>
	<div class="col-md-3">
		<div class="list-group" id="list_group1">
			<c:forEach var="item" items="${list1}" varStatus="status">
				<a onclick="onListSelect(this)" class="list-group-item">
					${item.nameKo }
					<input type="hidden" class="item-value1" value="${item.brandUid }"/>
				</a>
			</c:forEach>
		</div>
	</div>
	<div class="col-md-1 div-label">
		여성
	</div>
	<div class="col-md-3">
		<div class="list-group" id="list_group2">
			<c:forEach var="item" items="${list2}" varStatus="status">
				<a onclick="onListSelect(this)" class="list-group-item">
					${item.nameKo }
					<input type="hidden" class="item-value2" value="${item.brandUid }"/>
				</a>
			</c:forEach>
		</div>
	</div>
	<div class="col-md-1 div-label">
		키즈
	</div>
	<div class="col-md-3">
		<div class="list-group" id="list_group3">
			<c:forEach var="item" items="${list3}" varStatus="status">
				<a onclick="onListSelect(this)" class="list-group-item">
					${item.nameKo }
					<input type="hidden" class="item-value3" value="${item.brandUid }"/>
				</a>
			</c:forEach>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-1 col-md-3">
		<table class="table table-custom" style="border-top: none;">
			<tbody>
				<tr>
					<td><a class="btn btn-dark btn-block" onclick="onBrandAdd(1)">브랜드추가</a></td>
					<td><a class="btn btn-red-inline btn-block" onclick="onBrandRemove(1)">제거</a></td>
					<td><a class="btn btn-dark-inline btn-block" onclick="onBrandDown(1)"><i class="fa fa-long-arrow-down"></i></a></td>
					<td><a class="btn btn-dark-inline btn-block" onclick="onBrandUp(1)"><i class="fa fa-long-arrow-up"></i></a></td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div class="col-md-offset-1 col-md-3">
		<table class="table table-custom" style="border-top: none;">
			<tbody>
				<tr>
					<td><a class="btn btn-dark btn-block" onclick="onBrandAdd(2)">브랜드추가</a></td>
					<td><a class="btn btn-red-inline btn-block" onclick="onBrandRemove(2)">제거</a></td>
					<td><a class="btn btn-dark-inline btn-block" onclick="onBrandDown(2)"><i class="fa fa-long-arrow-down"></i></a></td>
					<td><a class="btn btn-dark-inline btn-block" onclick="onBrandUp(2)"><i class="fa fa-long-arrow-up"></i></a></td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div class="col-md-offset-1 col-md-3">
		<table class="table table-custom" style="border-top: none;">
			<tbody>
				<tr>
					<td><a class="btn btn-dark btn-block" onclick="onBrandAdd(3)">브랜드추가</a></td>
					<td><a class="btn btn-red-inline btn-block" onclick="onBrandRemove(3)">제거</a></td>
					<td><a class="btn btn-dark-inline btn-block" onclick="onBrandDown(3)"><i class="fa fa-long-arrow-down"></i></a></td>
					<td><a class="btn btn-dark-inline btn-block" onclick="onBrandUp(3)"><i class="fa fa-long-arrow-up"></i></a></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row" style="margin-top: 15px;">
	<div class="col-md-offset-5 col-md-1" style="padding-left: 15px; padding-right: 15px;">
		<a class="btn btn-dark btn-block" onclick="onSave()">수정</a>
	</div>
	<div class="col-md-1" style="padding-left: 15px; padding-right: 15px;">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<a class="btn default hidden" data-toggle="modal" href="#brand_modal" id="btn_brand_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="brand_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">브랜드검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="brand_keyword" name="brand_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onBrandSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">색인</td>
								<td>
									<div class="col-md-3" style="padding-left: 15px; padding-right: 15px;">
										<select class="form-control select" id="first_en" name="first_en">
											<option value="">A-Z</option>
											<option value="A">A</option>
											<option value="B">B</option>
											<option value="C">C</option>
											<option value="D">D</option>
											<option value="E">E</option>
											<option value="F">F</option>
											<option value="G">G</option>
											<option value="H">H</option>
											<option value="I">I</option>
											<option value="J">J</option>
											<option value="K">K</option>
											<option value="L">L</option>
											<option value="M">M</option>
											<option value="N">N</option>
											<option value="O">O</option>
											<option value="P">P</option>
											<option value="Q">Q</option>
											<option value="R">R</option>
											<option value="S">S</option>
											<option value="T">T</option>
											<option value="U">U</option>
											<option value="V">V</option>
											<option value="W">W</option>
											<option value="X">X</option>
											<option value="Y">Y</option>
											<option value="Z">Z</option>
										</select>
									</div>
									<div class="col-md-3" style="padding-left: 15px; padding-right: 15px;">
										<select class="form-control select" id="first_ko" name="first_ko">
											<option value="">ㄱ-ㅎ</option>
											<option value="ㄱ">ㄱ</option>
											<option value="ㄴ">ㄴ</option>
											<option value="ㄷ">ㄷ</option>
											<option value="ㄹ">ㄹ</option>
											<option value="ㅁ">ㅁ</option>
											<option value="ㅂ">ㅂ</option>
											<option value="ㅅ">ㅅ</option>
											<option value="ㅇ">ㅇ</option>
											<option value="ㅈ">ㅈ</option>
											<option value="ㅊ">ㅊ</option>
											<option value="ㅋ">ㅋ</option>
											<option value="ㅌ">ㅌ</option>
											<option value="ㅍ">ㅍ</option>
											<option value="ㅎ">ㅎ</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>브랜드 선택</strong></h4>
					</div>
					<div class="col-md-offset-8 col-md-2">
						<select class="form-control select" id="brand_length" name="brand_length" onchange="onBrandLengthChange()">
							<option value="100">100개 보기</option>
							<option value="500">500개 보기</option>
							<option value="-1">전체 보기</option>
						</select>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="brand_list" width="100%">
						<thead>
							<tr>
								<th>A-Z</th>
								<th>영문 브랜드명</th>
								<th>ㄱ-ㅎ</th>
								<th>한글 브랜드명</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	var ajax_brand_table;
	var brand_keyword = '';
	var first_en = '';
	var first_ko = '';
	var type = 0;
	$(document).ready(function () {
		$("#menu_brand").addClass("active");
		$("#menu_brand li:eq(2)").addClass("active");
		$(".nav-tabs li:eq(2)").addClass("active");
		
		ajax_brand_table = $("#brand_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_brand_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#brand_length").val();
                    d.keyword = brand_keyword;
                    d.first_ko = first_ko;
                    d.first_en = first_en;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(4)', row).html('선택');
           		$('td:eq(4)', row).css('cursor', 'pointer');
           		$('td:eq(4)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(4)', row).attr('onclick', 'onBrandSelectEach("' + data[3] + '", "' + data[4] + '")');
           	}
		});
	});
	
    function onBrandSelectEach(name_ko, id) {
    	var flag = 0;
    	$('.item-value' + type).each(function () {
    		if ($(this).val() == id) {
    			flag = 1;
    			return;
    		}
    	});
    	if (flag) {
    		$toast = toastr['error']('그 브랜드는 이미 등록되여 있습니다.');
    		return;
    	}
    	var html = '<a onclick="onListSelect(this)" class="list-group-item">' + name_ko + 
    				'<input type="hidden" class="item-value' + type + '" value="' + id + '"/></a>';
    	$("#list_group" + type).append(html);
    	$(".close").trigger('click');
    }
    
	function onBrandLengthChange() {
		ajax_brand_table.draw(false);
	}
	
	function onBrandSearch() {
		first_en = $("#first_en").val();
		first_ko = $("#first_ko").val();
		brand_keyword = $("#brand_keyword").val();
		ajax_brand_table.draw(false);
	}
    
    $("#brand_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onBrandSearch();
        }
    });
	
	function onListSelect(obj) {
		$(obj).parent(".list-group").find(".list-group-item").removeClass("active");
		$(obj).addClass("active");
	}
	
	function onBrandAdd(s_type) {
		type = s_type;
		$("#btn_brand_modal").trigger('click');
	}
	
	function onBrandRemove(type) {
		$("#list_group" + type).find('.active').remove();
	}
	
	function onBrandUp(type) {
		var obj = $("#list_group" + type).find('.active');
		var prev = $("#list_group" + type).find('.active').prev();
		if (prev.html() == "" || prev.html() == undefined)
			return;
		$(obj).insertBefore(prev);
	}

	function onBrandDown(type) {
		var obj = $("#list_group" + type).find('.active');
		var after = $("#list_group" + type).find('.active').next();
		if (after.html() == "" || after.html() == undefined)
			return;
		$(obj).insertAfter(after);
	}
	
	function onSave() {
		var list1 = '';
		$(".item-value1").each(function () {
			list1 += $(this).val() + ',';
		});
		var list2 = '';
		$(".item-value2").each(function () {
			list2 += $(this).val() + ',';
		});
		var list3 = '';
		$(".item-value3").each(function () {
			list3 += $(this).val() + ',';
		});
		$.post('<c:url value="/brand/recommend_brand_mng/save"/>', {list1: list1, list2: list2, list3: list3},
			function (result) {
				if (result == "success") {
					$toast = toastr['success']('', '성과적으로 저장되었습니다.');
				} else {
					$toast = toastr['error']('', '실패했습니다.');
				}
			}
		);
	}
	
	function onInitialize() {
		location.reload();
	}
</script>