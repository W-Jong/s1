<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/deal/deal_mng/0/index"/>">거래</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/deal/deal_mng/${index }/index"/>" class="page_nav">${title }</a>
	</div>
</div>

<div class="row" style="margin-top:20px;">
	<h4><strong>검색</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">검색어</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-5" style="padding-right: 39px;">
						<input type="text" class="form-control" id="keyword" name="keyword"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주문일</td>
				<td colspan="3">
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">발렛</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="valet" type="radio" id="valet_all" name="valet" checked value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="valet" type="radio" id="valet_pdt" name="valet" value="1"/>&nbsp;발렛상품
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="valet" type="radio" id="valet_general" name="valet" value="2"/>&nbsp;일반상품
		    			</label>
	    			</div>
				</td>
			</tr>
			<c:if test="${index == 0 }">
			<tr>
				<td class="td-label">주문상태</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" id="deal_status_all" name="deal_status" checked value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="11" name="deal_status"/>&nbsp;네고제안
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="12" name="deal_status"/>&nbsp;카운터네고
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="1" name="deal_status"/>&nbsp;배송준비
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="3" name="deal_status"/>&nbsp;정품인증
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="2" name="deal_status"/>&nbsp;배송진행
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="4" name="deal_status"/>&nbsp;배송완료
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="5" name="deal_status"/>&nbsp;거래완료
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="6" name="deal_status"/>&nbsp;정산완료
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="7" name="deal_status"/>&nbsp;반품과정
		    			</label>
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="deal_status" type="radio" value="10" name="deal_status"/>&nbsp;주문취소
		    			</label>
	    			</div>
				</td>
			</tr>
			</c:if>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSearch()">검색</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
		<h4><strong>목록</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 3px;">
	</div>
	<div class="col-md-offset-6 col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row">
	<table class="table table-bordered" id="deal_list">
		<thead>
			<tr>
				<c:forEach var="i" begin="0" end="${td_count }" step="1">
				<th>${td_fields[i] }</th>
				</c:forEach>
			</tr>
		</thead>
	</table>
</div>

<input type="hidden" id="total_count" value="${total_count }"/>
<input type="hidden" id="index" name="index" value="${index }"/>

<script>
var ajax_table;
var period_from = '';
var period_to = '';
var valet = 0;
var deal_status = $('#index').val();
var keyword = '';
var index = 0;
$(document).ready(function () {
	index = $('#index').val();
	$("#menu_deal").addClass("active");
	$("#menu_deal li:eq("+index+")").addClass("active");
	$(".nav-tabs li:eq("+index+")").addClass("active");
	$('.date-picker').datepicker({
   		container: 'html',
    	autoclose: true
    });
	$('.control-label input[type="radio"]').iCheck({
        checkboxClass: 'iradio_minimal-blue',
        radioClass: 'iradio_minimal-blue',
        increaseArea: '20%' // optional
    });
	
	ajax_table = $("#deal_list").DataTable({
		dom : 'itp',
		"stateSave": false,
           "autowidth": true,
           "serverSide": true,
           "ordering": false,
           "language" : {
              	"emptyTable": '<center>데이터가 없습니다.</center>',
              	"paginate" : {
              		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
              		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
              	},
              	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
               "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
           },
           "deferRender": true,
           "ajax": {
               "url": "<c:url value='/deal/deal_mng/ajax_table'/>",
               "type": "POST",
               "data":   function ( d ) {
                   start_index = d.start;
                   d.length = $("#length").val();
                   d.keyword = keyword;
                   d.period_from = period_from;
                   d.period_to = period_to;
                   d.valet = valet;
                   d.deal_status = deal_status;
                   d.index = $('#index').val();
               }
           },
           "pageLength": 100,
          	"createdRow": function (row, data, dataIndex) {
          		$('td:eq(0)', row).html(uid_format(data[0]));
          		$('td:eq(0)', row).css('cursor', 'pointer');
          		$('td:eq(0)', row).attr('onclick', 'onDetail(' + data[0] + ')');
          		
          		$('td:eq(1)', row).html(uid_format(data[1]));
          		$('td:eq(1)', row).css('cursor', 'pointer');
          		$('td:eq(1)', row).attr('onclick', 'onPdtDetail(' + data[1] + ')');
          		
          		$('td:eq(3)', row).css('cursor', 'pointer');
          		$('td:eq(3)', row).html(data[5]);
          		$('td:eq(3)', row).attr('onclick', 'onUsrDetail(' + data[3] + ')');
          		$('td:eq(4)', row).css('cursor', 'pointer');
          		$('td:eq(4)', row).html(data[6]);
          		$('td:eq(4)', row).attr('onclick', 'onUsrDetail(' + data[4] + ')');
          		
          		$('td:eq(5)', row).html(data[7]);
          		$('td:eq(6)', row).html(data[8]);
          		$('td:eq(7)', row).html(data[9]);
          		$('td:eq(8)', row).html(data[10]);
          		$('td:eq(9)', row).html(data[11]);
          		
          		if (data[2].length > 10)
          			$('td:eq(2)', row).html(data[2].substr(0, 10) + ' ...');
          		
          		if (data[7] == '0')
          			$('td:eq(5)', row).html('-');
          		if (data[8] == '')
          			$('td:eq(6)', row).html('-');
          		else {
          			var array = data[8].split(" ");
          			if (array.length > 1)
          				$('td:eq(6)', row).html(array[0] + "<br/>" + array[1]);
          			else
          				$('td:eq(6)', row).html(data[8]);
          		}
				
				if ($('#index').val() == 5){
	          		$('td:eq(10)', row).html('<label>상세</label>');
	          		$('td:eq(10)', row).css('background', 'RGB(242,242,242)');
	           		$('td:eq(10)', row).css('cursor', 'pointer');
	           		$('td:eq(10)', row).attr('onclick', 'onDetail(' + data[0] + ')');
           		}else{
           			$('td:eq(8)', row).html(data[10]+'<br>'+data[11]);
	          		$('td:eq(9)', row).html('<label>상세</label>');
	          		$('td:eq(9)', row).css('background', 'RGB(242,242,242)');
	           		$('td:eq(9)', row).css('cursor', 'pointer');
	           		$('td:eq(9)', row).attr('onclick', 'onDetail(' + data[0] + ')');
           		}
           		
           		if (data[9] == '네고제안' || data[10] == '카운터네고')
           			$('td:eq(5)', row).html('-');
          	}
	});
	
	$("#table_info").append($("#deal_list_info"));
});
	
function onSearch() {
	keyword = $("#keyword").val();
	period_from = $("#period_from").val();
	period_to = $("#period_to").val();
	ajax_table.draw(false);
}

function onInitialize() {
	$("#keyword").val("");
	$("#period_from").val("");
	$("#period_to").val("");
	$("#period_all").trigger('click');
	$("#valet_all").iCheck("check");
	$("#deal_status_all").iCheck("check");
	valet = 0;
	deal_status = 0;
	onSearch();
}

$(".valet").on('ifClicked', function (event) {	
	valet = $(this).val();
});

$(".deal_status").on('ifClicked', function (event) {	
	deal_status = $(this).val();
});

$(".period").change(function () {
	onPeriodChange($(this).val());
});

function onDetail(deal_id){
	location.href = '<c:url value="/deal/deal_mng/"/>' + deal_id + '/detail?index='+$('#index').val();
}

function onUsrDetail(id) {
	go_url("<c:url value="/usr/usr_mng/"/>" + id + "/detail");
}

function onPdtDetail(id) {
	go_url("<c:url value="/pdt/pdt_mng/"/>" + id + "/detail");
}
</script>