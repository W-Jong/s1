<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/deal/deal_mng/0/index"/>">거래</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/deal/deal_mng/${index }/index"/>" class="page_nav">거래리스트</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="#" class="page_nav">주문상세</a>
	</div>
</div>

<form id="deal_form" method="post" action="<c:url value="/deal/deal_mng/"/>${deal.dealUid}/save">

<div class="row">
	<h4><strong>주문상세</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">주문상태</td>
				<td colspan="5">
					<div class="col-md-12 btn-group" data-toggle="buttons">
						<label class="btn btn-default <c:if test="${deal.status == 1 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_derivery_ready" name="status" value="1"> 배송준비 </label>
						<label class="btn btn-default <c:if test="${deal.status == 2 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_derivery" name="status" value="2"> 배송진행 </label>
						<label class="btn btn-default <c:if test="${deal.status == 3 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_verify" name="status" value="3"> 정품인증 </label>
						<label class="btn btn-default <c:if test="${deal.status == 4 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_derivery_done" name="status" value="4"> 배송완료 </label>
						<label class="btn btn-default <c:if test="${deal.status == 5 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_deal_done" name="status" value="5"> 거래완료 </label>
						<label class="btn btn-default <c:if test="${deal.status == 6 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_cash_comp" name="status" value="6"> 정산완료 </label>
						<label class="btn btn-default <c:if test="${deal.status == 7 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_refund_req" name="status" value="7"> 반품접수 </label>
						<label class="btn btn-default <c:if test="${deal.status == 8 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_refund_check" name="status" value="8"> 반품승인 </label>
						<label class="btn btn-default <c:if test="${deal.status == 9 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_refund_done" name="status" value="9"> 반품완료 </label>
						<label class="btn btn-default <c:if test="${deal.status == 10 }">active</c:if>">
						<input type="radio" class="toggle status" id="status_cancel" name="status" value="10"> 주문취소 </label>
					</div>
					<input type="hidden" id="status" name="status" value="${deal.status }">
				</td>
			</tr>
			<tr>
				<td class="td-label">주문번호</td>
				<td width="30%">
					<div class="col-md-12" id="div_deal_uid">
						${deal.dealUid }
					</div>
					<input type="hidden" id="deal_uid" name="deal_uid" value="${deal.dealUid }">
				</td>
				<td class="td-label">주문일자</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.regTime }
					</div>
				</td>
				<td class="td-label">주문취소일자</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.cancelTime }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">상품코드</td>
				<td width="30%">
					<div class="col-md-12" id="div_pdt_uid">
						${deal.pdtUid }
					</div>
					<input type="hidden" id="pdt_uid" value="${deal.pdtUid }">
				</td>
				<td class="td-label">결제일자</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.payTime }
					</div>
				</td>
				<td class="td-label">발렛코드</td>
				<td width="15%">
					<div class="col-md-12" id="div_valet_code">
						
					</div>
					<input type="hidden" id="valet_code" value="${valet_code }">
				</td>
			</tr>
			<tr>
				<td class="td-label">상품명</td>
				<td width="30%">
					<div class="col-md-12">
						${deal.pdtTitle }
					</div>
				</td>
				<td class="td-label">정품인증</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						<c:if test="${deal.verifyDeliveryNumber == null }">N</c:if>
						<c:if test="${deal.verifyDeliveryNumber != null }">Y</c:if>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">구매자</td>
				<td width="30%">
					<div class="col-md-12">
						${usr_id }
					</div>
				</td>
				<td class="td-label">판매자</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${seller_id }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">주소</td>
				<td width="30%">
					<div class="col-md-12">
						${address }
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${detail_address }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">송장번호</td>
				<td width="30%">
					<div class="col-md-4">
						<select class="form-control select" id="delivery" name="delivery">
							<option value="">택배사선택</option>
							<option value="우체국택배">우체국택배</option>
							<option value="CJ대한통운">CJ대한통운</option>
							<option value="한진택배">한진택배</option>
							<option value="롯데택배">롯데택배</option>
							<option value="로젠택배">로젠택배</option>
							<option value="KGB택배">KGB택배</option>
							<option value="KG로지스">KG로지스</option>
							<option value="GTX로지스">GTX로지스</option>
							<option value="포스트박스">포스트박스</option>
							<option value="SC로지스">SC로지스</option>
							<option value="경동합동택배">경동합동택배</option>
							<option value="아주택배">아주택배</option>
							<option value="고려택배">고려택배</option>
							<option value="하나로택배">하나로택배</option>
							<option value="대신택배">대신택배</option>
							<option value="천일택배">천일택배</option>
							<option value="건영택배">건영택배</option>
							<option value="한의사랑택배">한의사랑택배</option>
							<option value="용마로지스">용마로지스</option>
							<option value="일양로지스">일양로지스</option>
						</select>
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control" id="delivery_number" name="delivery_number" value="${delivery_number }"/>
					</div>
					<div class="col-md-4">
						<a class="btn btn-block btn-dark" onclick="search_delivery_main()">배송조회</a>
					</div>
				</td>
				<td class="td-label">인증전송장</td>
				<td width="30%" colspan="3">
					<c:if test="${not empty deal.verifyDeliveryNumber}">
						<div class="col-md-8">
							${deal.verifyDeliveryNumber}
						</div>
						<div class="col-md-4">
							<a class="btn btn-block btn-dark" onclick="search_delivery('${deal.verifyDeliveryNumber}')">배송조회</a>
						</div>
					</c:if>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<h4><strong>걸제상세</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">상품가격</td>
				<td>
					<div class="col-md-12">
						${deal.pdtPrice }
					</div>
				</td>
				<td class="td-label">배송비</td>
				<td>
					<div class="col-md-12">
						${deal.sendPrice }
					</div>
				</td>
				<td class="td-label">정품인증</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.verifyPrice }
					</div>
				</td>
				<td class="td-label">사용적립금</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.rewardPrice }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">결제금액</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${deal.price }
					</div>
				</td>
				<td class="td-label">코드할인</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.payPromotionPrice }
					</div>
				</td>
				<td class="td-label">프로모션코드</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.payPromotion }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">아임포트</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${deal.payImportCode }
					</div>
				</td>
				<td class="td-label">결제방법</td>
				<td colspan=3>
					<div class="col-md-12">
						${pay_method }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">셀럽이용료</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${deal.selluvPrice }
					</div>
				</td>
				<td class="td-label">결제수수료</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${deal.payFeePrice }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">정산금액</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${deal.cashPrice }
					</div>
				</td>
				<td class="td-label">코드혜택</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.cashPromotionPrice }
					</div>
				</td>
				<td class="td-label">프로모션코드</td>
				<td width="15%">
					<div class="col-md-12">
						${deal.cashPromotion }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">지급코드</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${deal.nicepayCode }
					</div>
				</td>
				<td class="td-label">계좌정보</td>
				<td width="30%" colspan="3">
					<div class="col-md-12">
						${seller.bankNm} ${seller.accountNum}
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<h4><strong>반품취소</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label">취소사유</td>
				<td>
					<div class="col-md-12">
						${deal.cancelReason }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">반품사유</td>
				<td width="80%">
					<div class="col-md-12">
						${deal.refundReason }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">반품송장</td>
				<td width="80%">
					<div class="col-md-2">
						<select class="form-control select"  disabled="disabled" id="refund_delivery" name="refund_delivery">
							<option value="">택배사선택</option>
							<option value="우체국택배">우체국택배</option>
							<option value="CJ대한통운">CJ대한통운</option>
							<option value="한진택배">한진택배</option>
							<option value="롯데택배">롯데택배</option>
							<option value="로젠택배">로젠택배</option>
							<option value="KGB택배">KGB택배</option>
							<option value="KG로지스">KG로지스</option>
							<option value="GTX로지스">GTX로지스</option>
							<option value="포스트박스">포스트박스</option>
							<option value="SC로지스">SC로지스</option>
							<option value="경동합동택배">경동합동택배</option>
							<option value="아주택배">아주택배</option>
							<option value="고려택배">고려택배</option>
							<option value="하나로택배">하나로택배</option>
							<option value="대신택배">대신택배</option>
							<option value="천일택배">천일택배</option>
							<option value="건영택배">건영택배</option>
							<option value="한의사랑택배">한의사랑택배</option>
							<option value="용마로지스">용마로지스</option>
							<option value="일양로지스">일양로지스</option>
						</select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" readonly id="refund_delivery_number" name="refund_delivery_number" value="${refund_delivery_number }"/>
					</div>
					<div class="col-md-2">
						<c:if test="${not empty deal.refundDeliveryNumber}">
							<a class="btn btn-block btn-dark" onclick="search_delivery('${deal.refundDeliveryNumber}')">배송조회</a>
						</c:if>
						<c:if test="${empty deal.refundDeliveryNumber}">
							<a class="btn btn-block btn-dark">배송조회</a>
						</c:if>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">반품첨부사진</td>
				<td width="80%">
					<c:forEach var="refund_photo" items="${refund_photos}" varStatus="vstatus">
						<div class="col-md-2">
                   			<img src="${refund_photo }" width="150px" height="150px"></img>
                        </div>
                    </c:forEach>
				</td>
			</tr>
			<tr>
				<td class="td-label">관리자메모</td>
				<td>
					<div class="col-md-12">
						<textarea class="form-control" id="memo" name="memo">${deal.memo }</textarea>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<input type="hidden" id="index" name="index" value="${index }">
</form>

<div class="row">
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
	</div>
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSave()">저장</a>
	</div>
</div>
<script>
    if ('${msg}' === 'error')
    {
        alert("DB 접속에 실패하였거나 존재하지 않는 거래입니다.");
        onBack();
    }

	var index = $('#index').val();
	$(document).ready(function () {
		$("#menu_deal").addClass("active");
		$("#menu_deal li:eq("+index+")").addClass("active");
		$(".nav-tabs li:eq("+index+")").addClass("active");
		
		$('.date-picker').datepicker({
			container: 'html',
    		autoclose: true
        });
        
        $("#delivery").val("${delivery}");
		$("#refund_delivery").val("${refund_delivery}");
        $('#div_deal_uid').html(uid_format($("#deal_uid").val()));
        $('#div_pdt_uid').html(uid_format($("#pdt_uid").val()));
        if ($("#valet_code").val() != "")
        	$('#div_valet_code').html(uid_format($("#valet_code").val()));
	});

	var status = $('#status').val();
	$(".status").change(function () {
		status = $(this).val();
	});
	
	function onSave(){
		$('#status').val(status);

		if (Number(status) === 2) {
            var deliveryCompany = document.getElementById("delivery").value;
            var deliveryNumber = document.getElementById("delivery_number").value;
            if (deliveryCompany.length === 0) {
                $toast = toastr["error"]('', '택배사를 선택해주세요.');
                document.getElementById("delivery").focus();
                return;
            }
            if (deliveryNumber.length < 10) {
                $toast = toastr["error"]('', '정확한 운송장번호를 입력해주세요.');
                document.getElementById("delivery_number").focus();
                return;
            }
		}
		
		$('#deal_form').submit();
	}
	
	function search_delivery(deliveryNumber) {
	    var searchUrl = "https://m.search.daum.net/search?w=tot&q=" + encodeURIComponent(deliveryNumber);
        go_url(searchUrl);
    }

    function search_delivery_main() {
        var deliveryCompany = document.getElementById("delivery").value;
        var deliveryNumber = document.getElementById("delivery_number").value;
        if (deliveryCompany.length === 0) {
            $toast = toastr["error"]('', '택배사를 선택해주세요.');
            document.getElementById("delivery").focus();
            return;
		}
        if (deliveryNumber.length < 10) {
            $toast = toastr["error"]('', '정확한 운송장번호를 입력해주세요.');
            document.getElementById("delivery_number").focus();
            return;
        }
        search_delivery(deliveryCompany+ " " + deliveryNumber);
    }
</script>
