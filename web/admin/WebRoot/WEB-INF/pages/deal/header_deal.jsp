<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="page-bar">
	<div class="header_title">
		<strong><span style="font-size: 20px; color: black;">거래</span></strong>
	</div>
	<ul class="nav nav-tabs">
		<li>
			<a href="<c:url value="/deal/deal_mng/0/index"/>" style="cursor: pointer;">
			리스트 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/1/index"/>" style="cursor: pointer;">
			배송준비 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/2/index"/>" style="cursor: pointer;">
			배송진행 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/3/index"/>" style="cursor: pointer;">
			정품인증 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/4/index"/>" style="cursor: pointer;">
			배송완료 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/5/index"/>" style="cursor: pointer;">
			거래완료 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/6/index"/>" style="cursor: pointer;">
			정산완료 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/7/index"/>" style="cursor: pointer;">
			반품과정 </a>
		</li>
		<li>
			<a href="<c:url value="/deal/deal_mng/8/index"/>" style="cursor: pointer;">
			주문취소 </a>
		</li>
	</ul>
</div>

<script>
	$(document)
	  .ajaxStart(function() {
	    loadingOverlay();
	}).ajaxStop(function() {
	    loadingOverlayRemove();
	});
</script>
