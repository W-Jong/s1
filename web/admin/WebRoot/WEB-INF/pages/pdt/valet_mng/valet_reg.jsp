<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: #5d9bfc;
		border-color: #5d9bfc;
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/pdt/pdt_mng"/>">상품</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/pdt/valet_mng"/>" class="page_nav">발렛관리</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		<c:choose>
			<c:when test="${valet == null }">
			&nbsp;&nbsp;<a href="#" class="page_nav">발렛등록</a>
			</c:when>
			<c:otherwise>
			&nbsp;&nbsp;<a href="#" class="page_nav">발렛상세</a>
			</c:otherwise>
		</c:choose>
	</div>
	<c:choose>
		<c:when test="${valet == null }">
		</c:when>
		<c:otherwise>
			<div class="col-md-offset-5 col-md-1">
				<a class="btn btn-red-inline btn-block" onclick="onDelete(${valet.valetUid })">삭제</a>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<c:choose>
	<c:when test="${valet == null }">
		<div class="row" style="margin-top: 20px;">
			<h4><strong>발렛등록</strong></h4>
		</div>
	</c:when>
	<c:otherwise>
		<div class="row">
			<h4><strong>발렛상세</strong></h4>
		</div>
	</c:otherwise>
</c:choose>

<div class="row">
	<c:choose>
		<c:when test="${valet == null }">
			<form id="valet_form" method="post" action="<c:url value="/pdt/valet_mng/0/save"/>">
		</c:when>
		<c:otherwise>
			<form id="valet_form" method="post" action="<c:url value="/pdt/valet_mng/"/>${valet.valetUid}/save">
		</c:otherwise>
	</c:choose>
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">발렛상태</td>
				<td style="border-top-color: black;" colspan="5">
					<div class="col-md-8">
						<c:choose>
							<c:when test="${valet == null }">
								발렛접수
							</c:when>
							<c:otherwise>
								<div class="btn-group" data-toggle="buttons" style="width: 100%;">
									<label class="btn btn-default <c:if test="${valet.status == 2 || valet == null}">active</c:if> col-md-3">
									<input type="radio" class="toggle status" name="status" value="2"> 발렛접수 </label>
									<label class="btn btn-default <c:if test="${valet.status == 3}">active</c:if> col-md-3">
									<input type="radio" class="toggle status" name="status" value="3"> 상품등록 </label>
									<label class="btn btn-default <c:if test="${valet.status == 4}">active</c:if> col-md-3">
									<input type="radio" class="toggle status" name="status" value="4"> 판매완료 </label>
									<label class="btn btn-default <c:if test="${valet.status == 1}">active</c:if> col-md-3">
									<input type="radio" class="toggle status" name="status" value="1"> 정산완료 </label>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">발렛코드</td>
				<td width="40%">
					<div class="col-md-12">
					<a id="valet_uid_pad"><c:choose><c:when test="${valet == null }">-</c:when><c:otherwise>${valet.valetUid }</c:otherwise></c:choose></a>
					</div>
				</td>
				<td class="td-label">상품코드</td>
				<td colspan="3">
					<c:choose>
						<c:when test="${valet == null }">
							<div class="col-md-12">
								<a>-</a>
							</div>
						</c:when>
						<c:otherwise>
							<div class="col-md-3">
								<a class="btn btn-dark btn-block" onclick="onPdtSelect()">상품등록하기</a>
							</div>
							<div class="col-md-9" style="padding-top: 9px;"><a id="pdt_pad">${valet.pdtUid != 0 ? valet.pdtUid : '' }</a></div>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td class="td-label">카테고리</td>
				<td colspan="5">
					<div class="col-md-2">
						<select class="form-control select" id="valet_pdt_group">
							<option value="" selected>${pdt_group }</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="valet_category1">
							<option value="" selected>${category1 }</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="valet_category2">
							<option value="" selected>${category2 }</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="valet_category3">
							<option value="" selected>${category3 }</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">브랜드</td>
				<td width="40%" style="border-right: none;">
					<c:choose>
						<c:when test="${valet == null }">
							<div class="col-md-12"><a>-</a></div>
						</c:when>
						<c:otherwise>
							<div class="col-md-12" id="brand_pad" style="padding-top: 9px;">
								${brandName}
							</div>
						</c:otherwise>
					</c:choose>
				</td>
				<td colspan="4" style="border-left: none;"></td>
			</tr>
			<tr>
				<td class="td-label">받고싶은금액</td>
				<td width="40%">
					<div class="col-md-4">
						<input type="text" class="form-control" id="req_price" name="req_price" value="${valet.reqPrice }"/>
					</div>
				</td>
				<td class="td-label">예상수입금액</td>
				<td width="15%">
					<div class="col-md-12">
						<c:choose>
							<c:when test="${valet == null }">
								<a>-</a>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" id="expected_earn_price" name="expected_earn_price" value="${expected_earn_Price }" readonly/>
							</c:otherwise>
						</c:choose>
					</div>
				</td>
				<td class="td-label">수입금액</td>
				<td>
					<div class="col-md-12">
						<c:choose>
							<c:when test="${valet == null }">
								<a>-</a>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" id="earn_price" name="earn_price" value="${earn_price }" readonly/>
							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">신청자아이디</td>
				<td width="40%">
					<div class="col-md-4">
						<a href="<c:url value="/usr/usr_mng/"/>${usr_uid }/detail" target="_blank">${usr_id }</a>
					</div>
				</td>
				<td class="td-label">실명</td>
				<td width="15%">
					<div class="col-md-12">
						<input type="text" class="form-control" id="req_name" name="req_name" value="${valet.reqName }"/>
					</div>
				</td>
				<td class="td-label">연락처</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" id="req_phone" name="req_phone" value="${valet.reqPhone }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">신청자주소</td>
				<td width="40%">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onAddressSearch()">주소검색</a>
					</div>
					<div class="col-md-9" id="address" style="padding-top: 9px;">
						${valet.reqAddress}
					</div>
				</td>
				<td class="td-label">상세주소</td>
				<td colspan="3">
					<div class="col-md-12">
						<input type="text" class="form-control" id="req_address_detail" name="req_address_detail" value="${valet.reqAddressDetail }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">발송방법</td>
				<td width="40%">
					<div class="col-md-6">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default <c:if test="${valet.sendType == 1}">active</c:if> col-md-6">
							<input type="radio" class="toggle send" name="send" value="1"> 셀럽키트 </label>
							<label class="btn btn-default <c:if test="${valet.sendType == 2 || valet == null}">active</c:if> col-md-6">
							<input type="radio" class="toggle send" name="send" value="2"> 직접발송 </label>
						</div>
					</div>
				</td>
				<td class="td-label">상품수거</td>
				<td colspan="3">
					<div class="col-md-6">
						<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default <c:if test="${valet.receiveYn == 2 || valet == null}">active</c:if> col-md-6">
							<input type="radio" class="toggle receive" name="receive" value="2"> 상품수거전 </label>
							<label class="btn btn-default <c:if test="${valet.receiveYn == 1}">active</c:if> col-md-6">
							<input type="radio" class="toggle receive" name="receive" value="1"> 상품수거완료 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">신청일</td>
				<td width="40%">
					<div class="col-md-12">
						<a>
						<c:choose>
							<c:when test="${valet == null }">
								-
							</c:when>
							<c:otherwise>
								${valet.regTime }
							</c:otherwise>
						</c:choose>
						</a>
					</div>
				</td>
				<td class="td-label">서명사진</td>
				<td colspan="3">
					<c:if test="${sign_img != null && !sign_img.isEmpty() }">
						<div class="col-md-12">
							<div style="width: 150px; height: 50px;">
								<a onclick='onImageClick("${sign_img }")' class="fancybox-button" data-rel="fancybox-button">
								<img id="sign_img" src="${sign_img }" width="150px" height="50px"/>
								</a>
							</div>
						</div>
					</c:if>
				</td>
			</tr>
			<tr>
				<td class="td-label">관리자메모</td>
				<td colspan="5">
					<div class="col-md-12">
						<textarea class="form-control" rows="4" id="memo" name="memo">${valet.memo }</textarea>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="pdt_uid" name="pdt_uid" value="${valet.pdtUid }"/>
	<input type="hidden" id="req_address" name="req_address" value="${valet.reqAddress }"/>
	<c:choose>
		<c:when test="${valet == null }">
			<input type="hidden" id="send_type" name="send_type" value="2"/>
			<input type="hidden" id="receive_yn" name="receive_yn" value="2"/>
			<input type="hidden" id="status" name="status" value="2"/>
		</c:when>
		<c:otherwise>
			<input type="hidden" id="send_type" name="send_type" value="${valet.sendType }"/>
			<input type="hidden" id="receive_yn" name="receive_yn" value="${valet.receiveYn }"/>
			<input type="hidden" id="status" name="status" value="${valet.status }"/>	
		</c:otherwise>
	</c:choose>
	</form>
	<div class="row">
		<div class="col-md-1">
			<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
		</div>
		<div class="col-md-offset-5 col-md-1">
				<a class="btn btn-dark btn-block" onclick="onSave()">
					<c:choose>
						<c:when test="${valet == null }">
							등록
						</c:when>
						<c:otherwise>
							수정
						</c:otherwise>
					</c:choose>
				</a>
		</div>
	</div>
</div>

<a class="btn default hidden" data-toggle="modal" href="#pdt_modal" id="btn_pdt_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="pdt_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="pdt_keyword" name="pdt_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onPdtSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">카테고리</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="pdt_group" name="pdt_group" onchange="onPdtGroupChange($(this).val())">
											<option value="1">남성</option>
											<option value="2">여성</option>
											<option value="4">키즈</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category1" name="category1" onchange="onCategory1Change($(this).val())">
											<c:forEach var="category1" items="${category1_list}" varStatus="vstatus">
				                            	<option value="${category1.categoryUid }">${category1.categoryName }</option>
				                        	</c:forEach>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category2" name="category2" onchange="onCategory2Change($(this).val())">
											<c:forEach var="category2" items="${category2_list}" varStatus="vstatus">
				                            	<option value="${category2.categoryUid }">${category2.categoryName }</option>
				                        	</c:forEach>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="category3" name="category3">
											<c:forEach var="category3" items="${category3_list}" varStatus="vstatus">
				                            	<option value="${category3.categoryUid }">${category3.categoryName }</option>
				                        	</c:forEach>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>상품추가</strong></h4>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="pdt_list" width="100%">
						<thead>
							<tr>
								<th>코드</th>
								<th>이미지</th>
								<th>상품명</th>
								<th>판매자</th>
								<th>판매가</th>
								<th>추가</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<form id="pdt_form" method="post" action="<c:url value="/pdt/pdt_mng/reg"/>">
	<input type="hidden" id="valet_uid" name="valet_uid" value="${valet.valetUid }"/>
</form>

<script>
	
	$(document).ready(function () {
		$(".page-sidebar-menu li:eq(1)").addClass("active");
		$(".page-sidebar-menu li:eq(3)").addClass("active");
		$(".nav-tabs li:eq(1)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 발렛입니다.");
			onBack();
		}
		
		if ($("#valet_uid_pad").html() > 0)
		{
			$("#valet_uid_pad").html(uid_format($("#valet_uid_pad").html()));
		}
		
		if ($("#pdt_pad").html() > 0)
		{
			var pdt_uid = $("#pdt_pad").html();
			$("#pdt_pad").html(uid_format(pdt_uid));
			$("#pdt_pad").attr("onclick", "go_url('<c:url value="/pdt/pdt_mng/"/>" + pdt_uid + "/detail')");
		}
	});
    
    $(".status").change(function () {
		$("#status").val($(this).val());
	});
	
	$(".send").change(function () {
		$("#send_type").val($(this).val());
	});
	
	$(".receive").change(function () {
		$("#receive_yn").val($(this).val());
	});
	
	function onSave() {
	//check
	
		$("#valet_form").submit();
	}
	
	function onPdtSelect() {
		$("#pdt_form").submit();
	}
	
	function onDelete(id) {
		if (!confirm("해당 발렛을 삭제하시겠습니까?"))
			return;
		go_url("<c:url value="/pdt/valet_mng/"/>${valet.valetUid }/remove");
	}
</script>
