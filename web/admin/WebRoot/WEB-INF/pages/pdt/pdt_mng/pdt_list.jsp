<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		color: RGB(89,89,89);
		background: transparent !important;
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/pdt/pdt_mng"/>">상품</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="<c:url value="/pdt/pdt_mng"/>" class="page_nav">상품리스트</a>
	</div>
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-blue" onclick="onRegister()">상품등록</a>
	</div>
</div>

<div class="row">
	<h4><strong>검색</strong></h4>
</div>

<div class="row">
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">검색어</td>
				<td style="border-top-color: black;" colspan="3">
					<div class="col-md-5" style="padding-right: 39px;">
						<input type="text" class="form-control" id="keyword" name="keyword"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">등록일</td>
				<td colspan="3">
					<div class="col-md-5">
						<div class="input-group date-picker input-daterange" data-date-format="yyyy-mm-dd">
			    			<input type="text" class="form-control" name="period_from" id="period_from">
			        		<span class="input-group-btn"><i class="icon-calendar"></i></span>
			    			<span class="input-group-addon">~</span>
			    			<input type="text" class="form-control" name="period_to" id="period_to">
			         		<span class="input-group-btn"><i class="icon-calendar"></i></span>
	    				</div>
	    			</div>
	    			<div class="col-md-7">
	    				<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_today" name="period" value="1"> 오늘 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_week" name="period" value="2"> 7일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_twoweek" name="period" value="3"> 15일 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_month" name="period" value="4"> 1개월 </label>
							<label class="btn btn-default">
							<input type="radio" class="toggle period" id="period_threemonth" name="period" value="5"> 3개월 </label>
							<label class="btn btn-default active">
							<input type="radio" class="toggle period" id="period_all" name="period" value="0"> 전체 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">카테고리</td>
				<td colspan="3">
					<div class="col-md-2">
						<select class="form-control select" id="pdt_group" name="pdt_group" onchange="onPdtGroupChange($(this).val())">
							<option value="0">전체</option>
							<option value="1">남성</option>
							<option value="2">여성</option>
							<option value="4">키즈</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="category1" name="category1" onchange="onCategory1Change($(this).val())">
							<option value="0">전체</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="category2" name="category2" onchange="onCategory2Change($(this).val())">
							<option value="0">전체</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="category3" name="category3">
							<option value="0">전체</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">브랜드</td>
				<td width="40%">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onBrandSelect()">브랜드선택</a>
					</div>
					<div id="pdt_brand_list">
					
					</div>
				</td>
				<td class="td-label">발렛</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="valet" type="radio" id="valet_all" name="valet" checked value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="valet" type="radio" id="valet_pdt" name="valet" value="1"/>&nbsp;발렛상품
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="valet" type="radio" id="valet_general" name="valet" value="2"/>&nbsp;일반상품
		    			</label>
	    			</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">상태</td>
				<td width="40%">
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="sale" type="radio" id="sale_all" name="sale" checked value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="sale" type="radio" id="sale_at" name="sale" value="1"/>&nbsp;판매중
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="sale" type="radio" id="sale_finish" name="sale" value="2"/>&nbsp;판매완료
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="sale" type="radio" id="sale_stop" name="sale" value="3"/>&nbsp;판매중지
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="sale" type="radio" id="sale_break_mode" name="sale" value="4"/>&nbsp;휴가모드
		    			</label>
	    			</div>
				</td>
				<td class="td-label">포토샵</td>
				<td>
					<div class="col-md-12">
						<label class="control-label" style="padding-top: 5px;">
		    				<input class="photoshop" type="radio" id="photoshop_all" name="photoshop" <c:if test="${photoshop_yn==null }">checked</c:if> value="0"/>&nbsp;전체
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="photoshop" type="radio" id="photoshop_before" name="photoshop" <c:if test="${photoshop_yn==2 }">checked</c:if> value="2"/>&nbsp;포토샵전
		    			</label>&nbsp;&nbsp;
		    			<label class="control-label" style="padding-top: 5px;">
		    				<input class="photoshop" type="radio" id="photoshop_after" name="photoshop" value="1"/>&nbsp;포토샵후
		    			</label>
	    			</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSearch()">검색</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
		<h4><strong>목록</strong></h4>
	</div>
	<div class="col-md-3" id="table_info" style="padding-top: 3px;">
	</div>
	<div class="col-md-offset-6 col-md-2">
		<select class="form-control select" id="length" name="length" onchange="onLengthChange()">
			<option value="100">100개 보기</option>
			<option value="500">500개 보기</option>
			<option value="-1">전체 보기</option>
		</select>
	</div>
</div>

<div class="row">
	<table class="table table-bordered" id="pdt_list">
		<thead>
			<tr>
				<th>상품코드</th>
				<th>이미지</th>
				<th>상품명</th>
				<th>판매가</th>
				<th>판매자</th>
				<th>상태</th>
				<th>등록일/수정일</th>
				<th>수정</th>
			</tr>
		</thead>
	</table>
</div>

<input type="hidden" id="total_count" value="${total_count }"/>

<a class="btn default hidden" data-toggle="modal" href="#large" id="btn_brand_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">브랜드검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="brand_keyword" name="brand_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onBrandSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">색인</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="first_en" name="first_en">
											<option value="">A-Z</option>
											<option value="a">a</option>
											<option value="b">b</option>
											<option value="c">c</option>
											<option value="d">d</option>
											<option value="e">e</option>
											<option value="f">f</option>
											<option value="g">g</option>
											<option value="h">h</option>
											<option value="i">i</option>
											<option value="j">j</option>
											<option value="k">k</option>
											<option value="l">l</option>
											<option value="m">m</option>
											<option value="n">n</option>
											<option value="o">o</option>
											<option value="p">p</option>
											<option value="q">q</option>
											<option value="r">r</option>
											<option value="s">s</option>
											<option value="t">t</option>
											<option value="u">u</option>
											<option value="v">v</option>
											<option value="w">w</option>
											<option value="x">x</option>
											<option value="y">y</option>
											<option value="z">z</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="first_ko" name="first_ko">
											<option value="">ㄱ-ㅎ</option>
											<option value="ㄱ">ㄱ</option>
											<option value="ㄴ">ㄴ</option>
											<option value="ㄷ">ㄷ</option>
											<option value="ㄹ">ㄹ</option>
											<option value="ㅁ">ㅁ</option>
											<option value="ㅂ">ㅂ</option>
											<option value="ㅅ">ㅅ</option>
											<option value="ㅇ">ㅇ</option>
											<option value="ㅈ">ㅈ</option>
											<option value="ㅊ">ㅊ</option>
											<option value="ㅋ">ㅋ</option>
											<option value="ㅌ">ㅌ</option>
											<option value="ㅍ">ㅍ</option>
											<option value="ㅎ">ㅎ</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>브랜드 선택</strong></h4>
					</div>
					<div class="col-md-offset-8 col-md-2">
						<select class="form-control select" id="brand_length" name="brand_length" onchange="onBrandLengthChange()">
							<option value="100">100개 보기</option>
							<option value="500">500개 보기</option>
							<option value="-1">전체 보기</option>
						</select>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="brand_list">
						<thead>
							<tr>
								<th>A-Z</th>
								<th>영문 브랜드명</th>
								<th>ㄱ-ㅎ</th>
								<th>한글 브랜드명</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	var keyword = '';
	var period_from = '';
	var period_to = '';
	var period = 0;
	var pdt_group = 0;
	var category1 = $("#category1").val();
	var category2 = $("#category2").val();
	var category3 = $("#category3").val();
	var valet = 0;
	var status = 0;
	var photoshop = ${photoshop_yn==null?0:photoshop_yn};
	var brand_keyword = '';
	var first_en = '';
	var first_ko = '';
	var ajax_table;
	var ajax_brand_table;
	var brand_list = '';

	$(document).ready(function () {
		$(".page-sidebar-menu li:eq(1)").addClass("active");
		$(".page-sidebar-menu li:eq(2)").addClass("active");
		$(".nav-tabs li:eq(0)").addClass("active");
		$('.control-label input[type="radio"]').iCheck({
            checkboxClass: 'iradio_minimal-blue',
            radioClass: 'iradio_minimal-blue',
            increaseArea: '20%' // optional
        });
        
        $('.date-picker').datepicker({
    		container: 'html',
    		autoclose: true
        });
        
        ajax_table = $("#pdt_list").DataTable({
			dom : 'itp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
               	"sInfo": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
                "sInfoEmpty": "검색&nbsp;&nbsp;&nbsp;<span style='color: #5d9bfc;'>_TOTAL_</span> 개&nbsp;&nbsp;/&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;" + $("#total_count").val() + "개",
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#length").val();
                    d.keyword = keyword;
                    d.period_from = period_from;
                    d.period_to = period_to;
                    d.pdt_group = pdt_group;
                    d.category1 = category1;
                    d.category2 = category2;
                    d.category3 = category3;
                    d.valet = valet;
                    d.status = status;
                    d.photoshop = photoshop;
                    d.brand_list = brand_list;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(1)', row).html('');
           		var img_src = data[13] + '/pdt/' + data[0] + '/' + data[1];
           		$('td:eq(1)', row).html('<a onclick="onImageClick(\'' + img_src + '\')" class="fancybox-button" data-rel="fancybox-button"><img src="' + img_src + '" width="100px" height="100px"/></a>');
           	
           		var pdt_group_str = '';
           		if (data[10] == 1)
           			pdt_group_str = '남성';
           		else if (data[10] == 2)
           			pdt_group_str = '여성';
           		else if (data[10] == 4)
           			pdt_group_str = '키즈';
          		$('td:eq(2)', row).css("text-align", "left");
           		$('td:eq(2)', row).html(data[9] + ' ' + pdt_group_str + ' ' + data[11] + ' ' + data[2] + ' ' + data[12]);
           		$('td:eq(4)', row).css("cursor", "pointer");
           		$('td:eq(4)', row).attr("onclick", "onUsrDetail(" + data[14] + ")");
           		var status_str = '';
           		if (data[5] == 1)
           			status_str = '판매중';
           		else if (data[5] == 2)
           			status_str = '판매완료';
          		else if (data[5] == 3)
           			status_str = '판매정지';
           		else if (data[5] == 4)
           			status_str = '휴가모드';
           		$('td:eq(5)', row).html(status_str);
           		$('td:eq(6)', row).html(data[6] + '<br/>' + data[8]);
           		$('td:eq(7)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(7)', row).css('cursor', 'pointer');
           		$('td:eq(7)', row).html('수정');
           		$('td:eq(7)', row).attr('onclick', 'onDetail(' + data[0] + ')');
           		
           		$('td:eq(0)', row).css('cursor', 'pointer');
           		$('td:eq(0)', row).html(uid_format(data[0]));
           		$('td:eq(0)', row).attr('onclick', 'onDetail(' + data[0] + ')');
           	}
		});
		
		$("#table_info").append($("#pdt_list_info"));
		
		ajax_brand_table = $("#brand_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_brand_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#brand_length").val();
                    d.keyword = brand_keyword;
                    d.first_ko = first_ko;
                    d.first_en = first_en;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(4)', row).html('선택');
           		$('td:eq(4)', row).css('cursor', 'pointer');
           		$('td:eq(4)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(4)', row).attr('onclick', 'onBrandSelectEach("' + data[1] + '", "' + data[4] + '")');
           	}
		});
	});
	
	function onSearch() {
		keyword = $("#keyword").val();
		period_from = $("#period_from").val();
		period_to = $("#period_to").val();
		pdt_group = $("#pdt_group").val();
		category1 = $("#category1").val();
		category2 = $("#category2").val();
		category3 = $("#category3").val();
		brand_list = '';
		$('.brand-value').each(function () {
    		brand_list += $(this).val() + ',';
    	});
		ajax_table.draw(false);
	}
	
	function onBrandSearch() {
		first_en = $("#first_en").val();
		first_ko = $("#first_ko").val();
		brand_keyword = $("#brand_keyword").val();
		ajax_brand_table.draw(false);
	}
	
	function onLengthChange() {
		ajax_table.draw(false);
	}
	
	function onBrandLengthChange() {
		ajax_brand_table.draw(false);
	}
	
	$(".period").change(function () {
		onPeriodChange($(this).val());
		ajax_table.draw(false);
	});
	
	$(".sale").on('ifClicked', function (event) {	
		status = $(this).val();
	});
	
	$(".photoshop").on('ifClicked', function (event) {
		photoshop = $(this).val();
	});
	
	$(".valet").on('ifClicked', function (event) {
		valet = $(this).val();
	});
	
	function onInitialize() {
		$("#pdt_group").val(0);
		onPdtGroupChange(0);
		$("#keyword").val("");
		$("#period_from").val("");
		$("#period_to").val("");
		$("#period_all").trigger('click');
		$("#sale_all").iCheck("check");
		$("#valet_all").iCheck("check");
		$("#photoshop_all").iCheck("check");
		$("#pdt_brand_list").empty();
		brand_list = '';
		photoshop = 0;
		status = 0;
		valet = 0;
		setTimeout('onSearch()', 500);
	}
	
	$("#keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onSearch();
        }
    });
    
    $("#brand_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onBrandSearch();
        }
    });
    
    function onBrandSelect() {
    	$("#btn_brand_modal").trigger('click');
    }
    
    function onRegister() {
    	location.href = "<c:url value="/pdt/pdt_mng/reg"/>";
    }
    
    function onBrandSelectEach(name_en, brand_uid) {
    	var flag = 0;
    	$('.brand-value').each(function () {
    		if ($(this).val() == brand_uid) {
    			flag = 1;
    			return;
    		}
    	});
    	if (flag) {
    		$toast = toastr['error']('그 브랜드는 이미 선택되여 있습니다.');
    		return;
    	}
    	$("#pdt_brand_list").append('<div class="col-md-2" style="padding-top: 9px;">' +
    	 name_en + '<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onBrandDelete(this)"></i>' +
    	 '<input type="hidden" class="brand-value" value="' + brand_uid + '"/></div>');
    	$(".close").trigger('click');
    }
    
    function onBrandDelete(obj) {
    	$(obj).parent('div').remove();
    }
    
    function onDetail(id) {
    	location.href = "<c:url value="/pdt/pdt_mng/"/>" + id + "/detail";
    }
    
    function onPdtGroupChange(id) {
    	$('#category1').empty();
        $('#category1').append('<option value="0">전체</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category1').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory1Change(json[0].categoryUid);
	            else
	            	onCategory1Change(0);
	        });
	    } else {
	    	onCategory1Change(0);
	    }
	}
	
	function onCategory1Change(id) {
		$('#category2').empty();
        $('#category2').append('<option value="0">전체</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category2').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory2Change(json[0].categoryUid);
	            else
	            	onCategory2Change(0);
	        });
	   	} else {
	   		onCategory2Change(0);
	   	}
	}
	
	function onCategory2Change(id) {
		$('#category3').empty();
        $('#category3').append('<option value="0">전체</option>');
		if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category3').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	        });
	    }
	}
	
	function onUsrDetail(id) {
		go_url("<c:url value="/usr/usr_mng/"/>" + id + "/detail");
	}
</script>