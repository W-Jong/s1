<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
	.row > h4 {
		padding-left: 15px;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	width: 10%;
    	padding-left: 20px !important;
    	height: 51px;
    }
    
    .search-table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: left !important;
    	background: white;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
    .table > thead > tr > th {
    	vertical-align: middle;
    	text-align: center;
    	color: white;
    	font-weight: normal;
    }
    
    .table > thead > tr {
    	background: RGB(89,89,89);
    }
    
    .input-group-addon {
   		border: none !important;
   		background: none !important;
   		padding-top: 2px;
   	}
   	
   	.table .btn {
	    margin-right: 0;
	}
	
	.input-group-btn {
		padding-left: 10px;
	}
	
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: #5d9bfc;
		border-color: #5d9bfc;
		color: white;
	}
	
	.select {
		border-radius: 8px !important;
	}
	
	.icon-calendar {
		color: RGB(166,166,166);
	}
	
	div.dataTables_paginate {
		float: none;
		text-align: center;
	}
	
	.pagination > li > a {
		border: none;
		background: transparent !important;
		color: RGB(89,89,89);
	}
	
	.pagination > .active > a {
		border: none;
		border-radius: 50% !important;
		color: white;
		background: RGB(89,89,89) !important;
	}
	
	.modal .modal-header {
		border-bottom: none;
	}
	
	.modal-body {
		padding: 0;
	}
	
	.modal-content {
		background: #f8f8fa;
	}
	
	.img_pad_bottom {
        float: left;
        position: relative;
    }

    .img_pad_this {
        display: block;
        position: absolute;
        top: 5px;
        right: 5px;
        width: 20px;
        padding-left: 4px;
        height: 20px;
        border-radius: 50% !important;
        background: black;
        cursor: pointer;
    }
    
    .length {
    	position: absolute;
    	right: 30px;
    	bottom: 0;
    }
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/pdt/pdt_mng"/>">상품</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		&nbsp;&nbsp;<a href="<c:url value="/pdt/pdt_mng"/>" class="page_nav">상품리스트</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>
		<c:choose>
			<c:when test="${pdt == null }">
			&nbsp;&nbsp;<a href="#" class="page_nav">상품등록</a>
			</c:when>
			<c:otherwise>
			&nbsp;&nbsp;<a href="#" class="page_nav">상품수정</a>
			</c:otherwise>
		</c:choose>
	</div>
	<c:choose>
		<c:when test="${pdt == null }">
		</c:when>
		<c:otherwise>
			<div class="col-md-offset-5 col-md-1">
				<a class="btn btn-red-inline btn-block" onclick="remove_pdt()">삭제</a>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<c:choose>
	<c:when test="${pdt == null }">
		<div class="row" style="margin-top: 20px;">
			<h4><strong>상품등록</strong></h4>
		</div>
	</c:when>
	<c:otherwise>
		<div class="row">
			<h4><strong>상품수정</strong></h4>
		</div>
	</c:otherwise>
</c:choose>

<div class="row">
	<c:choose>
		<c:when test="${pdt == null }">
			<form id="pdt_form" method="post" action="<c:url value="/pdt/pdt_mng/0/save"/>">
		</c:when>
		<c:otherwise>
			<form id="pdt_form" method="post" action="<c:url value="/pdt/pdt_mng/"/>${pdt.pdtUid}/save">
		</c:otherwise>
	</c:choose>
	<table class="table table-bordered search-table">
		<tbody>
			<tr>
				<td class="td-label" style="border-top-color: black;">상품상태</td>
				<td style="border-top-color: black;" colspan="5">
					<div class="col-md-8">
	    				<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default <c:if test="${pdt.status == 1}">active</c:if> col-md-3">
							<input type="radio" class="toggle sale" id="sale_at" name="sale" value="1"> 판매중 </label>
							<label class="btn btn-default <c:if test="${pdt.status == 2}">active</c:if> col-md-3">
							<input type="radio" class="toggle sale" id="sale_finish" name="sale" value="2"> 판매완료 </label>
							<label class="btn btn-default <c:if test="${pdt.status == 3}">active</c:if> col-md-3">
							<input type="radio" class="toggle sale" id="sale_stop" name="sale" value="3"> 판매중지 </label>
							<label class="btn btn-default <c:if test="${pdt.status == 4}">active</c:if> col-md-3">
							<input type="radio" class="toggle sale" id="sale_break" name="sale" value="4"> 휴가모드 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">등록유저</td>
				<td style="border-right: 0;">
					<c:choose>
						<c:when test="${pdt == null }">
							<div class="col-md-3">
								<a class="btn btn-dark btn-block" onclick="onUsrSelect()">유저선택</a>
							</div>
							<div class="col-md-9" id="pdt_usr_list" style="padding-top: 9px;">
							</div>
						</c:when>
						<c:otherwise>
							<div class="col-md-12">
								<a href="<c:url value="/usr/usr_mng/"/>${pdt.usrUid }/detail" target="_blank">${usrName }</a>
							</div>
						</c:otherwise>
					</c:choose>
				</td>
				<td colspan="4" style="border-left: 0;">
				</td>
			</tr>
			<tr>
				<td class="td-label">카테고리</td>
				<td colspan="5">
					<div class="col-md-2">
						<select class="form-control select" id="pdt_group" name="pdt_group" onchange="onPdtGroupChange($(this).val())">
							<option value="0">상품군</option>
							<option value="1" <c:if test="${pdt.pdtGroup == 1}">selected</c:if>>남성</option>
							<option value="2" <c:if test="${pdt.pdtGroup == 2}">selected</c:if>>여성</option>
							<option value="4" <c:if test="${pdt.pdtGroup == 4}">selected</c:if>>키즈</option>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="category1" name="category1" onchange="onCategory1Change($(this).val())">
							<option value="0">카테고리1</option>
							<c:forEach var="category" items="${category1_list}" varStatus="vstatus">
                            	<option value="${category.categoryUid }" <c:if test="${category.categoryUid == category1}">selected</c:if>>${category.categoryName }</option>
                        	</c:forEach>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="category2" name="category2" onchange="onCategory2Change($(this).val())">
							<option value="0">카테고리2</option>
							<c:forEach var="category" items="${category2_list}" varStatus="vstatus">
                            	<option value="${category.categoryUid }" <c:if test="${category.categoryUid == category2}">selected</c:if>>${category.categoryName }</option>
                        	</c:forEach>
						</select>
					</div>
					<div class="col-md-2">
						<select class="form-control select" id="category3" name="category3">
							<option value="0">카테고리3</option>
							<c:forEach var="category" items="${category3_list}" varStatus="vstatus">
                            	<option value="${category.categoryUid }" <c:if test="${category.categoryUid == category3}">selected</c:if>>${category.categoryName }</option>
                        	</c:forEach>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">브랜드</td>
				<td width="40%">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onBrandSelect()">브랜드선택</a>
					</div>
					<div class="col-md-9" id="pdt_brand_list" style="padding-top: 9px;">
						${brandName}
					</div>
				</td>
				<td class="td-label">사이즈</td>
				<td colspan="3">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" onclick="onSizeSelect()">사이즈선택</a>
					</div>
					<div class="col-md-9" id="size" style="padding-top: 9px;">
						${pdt.pdtSize }
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">상품사진</td>
				<td width="40%">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" id="btn_profile_img">대표사진수정</a>
					</div>
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" id="btn_photo_search">사진찾기</a>
					</div>
					<div class="col-md-offset-2 col-md-4">
						<select class="form-control select" id="photoshop_yn" name="photoshop_yn">
							<option value="2" <c:if test="${pdt.photoshopYn == 2 }">selected</c:if>>포토샵전</option>
							<option value="1" <c:if test="${pdt.photoshopYn == 1 }">selected</c:if>>포토샵후</option>
						</select>
					</div>
				</td>
				<td class="td-label">판매가격</td>
				<td width="15%">
					<div class="col-md-12">
						<input class="form-control" type="number" id="price" name="price" value="${pdt.price }"/>
					</div>
				</td>
				<td class="td-label">배송비</td>
				<td>
					<div class="col-md-12">
						<input class="form-control" type="number" id="send_price" name="send_price" value="${pdt.sendPrice }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="6">
					<div class="row" id="sortable_portlets">
						<div class="column sortable" id="img_pad">
							<div class="col-md-2 <c:if test="${profile_img == null || empty profile_img }">hidden</c:if> portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px;" id="profile_img_pad">
								<c:choose>
									<c:when test="${profile_img == null || empty profile_img }">
									</c:when>
									<c:otherwise>
										<div class="img_pad_bottom portlet-title">
											<a onclick='onImageClick("${profile_img }")' class="fancybox-button" data-rel="fancybox-button">
											<img src="${profile_img }" width="150px" height="150px"></img>
											</a> 
				                        	<div class="img_pad_this" onclick="delete_img(this, 1)">
				                        		<i style="color: white;" class="fa fa-times"></i>
				                            </div>
			                            </div>
			                            <div class="portlet portlet-sortable-empty"></div>
		                            </c:otherwise>
	                            </c:choose>
							</div>
							<c:if test="${not empty photos}">
                                <c:forEach var="real_photo" items="${real_photos}" varStatus="vstatus">
                                    <div class="col-md-2 portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px;">
                                        <div class="img_pad_bottom portlet-title">
                                            <a onclick='onImageClick("${real_photo }")' class="fancybox-button" data-rel="fancybox-button">
                                            <img src="${real_photo }" width="150px" height="150px"></img>
                                            </a>
                                            <div class="img_pad_this" onclick="delete_img(this, 2)">
                                                <i style="color: white;" class="fa fa-times"></i>
                                            </div>
                                        </div>
                                        <input type="hidden" class="photos" value="${photos[vstatus.index] }"/>
                                        <div class="portlet portlet-sortable-empty"></div>
                                    </div>
                                </c:forEach>
                            </c:if>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">컨디션</td>
				<td colspan="5">
					<div class="col-md-8">
	    				<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default <c:if test="${pdt.pdtCondition == 1}">active</c:if> col-md-3">
							<input type="radio" class="toggle condition" id="condition_new" name="condition" value="1"> 새상품 </label>
							<label class="btn btn-default <c:if test="${pdt.pdtCondition == 2}">active</c:if> col-md-3">
							<input type="radio" class="toggle condition" id="condition_super" name="condition" value="2"> 최상 </label>
							<label class="btn btn-default <c:if test="${pdt.pdtCondition == 3}">active</c:if> col-md-3">
							<input type="radio" class="toggle condition" id="condition_high" name="condition" value="3"> 상 </label>
							<label class="btn btn-default <c:if test="${pdt.pdtCondition == 4}">active</c:if> col-md-3">
							<input type="radio" class="toggle condition" id="condition_medium" name="condition" value="4"> 중상 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">구성품</td>
				<td colspan="5">
					<div class="col-md-8">
	    				<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default <c:if test="${component[0] == 1}">active</c:if> col-md-2">
							<input type="checkbox" class="toggle component" name="pdt_component" value="1" <c:if test="${component[0] == 1}">checked</c:if>> 상품택 </label>
							<label class="btn btn-default <c:if test="${component[1] == 1}">active</c:if> col-md-2">
							<input type="checkbox" class="toggle component" name="pdt_component" value="2" <c:if test="${component[1] == 1}">checked</c:if>> 게런티카드 </label>
							<label class="btn btn-default <c:if test="${component[2] == 1}">active</c:if> col-md-2">
							<input type="checkbox" class="toggle component" name="pdt_component" value="3" <c:if test="${component[2] == 1}">checked</c:if>> 영수증 </label>
							<label class="btn btn-default <c:if test="${component[3] == 1}">active</c:if> col-md-2">
							<input type="checkbox" class="toggle component" name="pdt_component" value="4" <c:if test="${component[3] == 1}">checked</c:if>> 여분부속품 </label>
							<label class="btn btn-default <c:if test="${component[4] == 1}">active</c:if> col-md-2">
							<input type="checkbox" class="toggle component" name="pdt_component" value="5" <c:if test="${component[4] == 1}">checked</c:if>> 브랜드박스 </label>
							<label class="btn btn-default <c:if test="${component[5] == 1}">active</c:if> col-md-2">
							<input type="checkbox" class="toggle component" name="pdt_component" value="6" <c:if test="${component[5] == 1}">checked</c:if>> 더스트백 </label>
						</div>
					</div>
					
					<div class="col-md-offset-1 col-md-1" style="padding-top: 8px; text-align: right;">
						ETC
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="etc" name="etc" value="${pdt.etc }">
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">컬러</td>
				<td colspan="5">
					<div class="col-md-12">
	    				<div class="btn-group" data-toggle="buttons" style="width: 100%;">
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('블랙') }">active</c:if>" style="width: 6.6666%;">
							<input type="radio" class="toggle color" name="color" value="블랙"> 블랙 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('그레이') }">active</c:if>" style="width: 6.6666%;">
							<input type="radio" class="toggle color" name="color" value="그레이"> 그레이 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('화이트') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="화이트"> 화이트 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('베이지') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="베이지"> 베이지 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('레드') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="레드"> 레드 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('핑크') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="핑크"> 핑크 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('블루') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="블루"> 블루 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('그린') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="그린"> 그린 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('옐로우') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="옐로우"> 옐로우 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('오렌지') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="오렌지"> 오렌지 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('퍼플') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="퍼플"> 퍼플 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('브라운') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="브라운"> 브라운 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('골드') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="골드"> 골드 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('실버') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="실버"> 실버 </label>
							<label class="btn btn-default <c:if test="${pdt.colorName.equals('멀티') }">active</c:if>" style="width: 6.666%;">
							<input type="radio" class="toggle color" name="color" value="멀티"> 멀티 </label>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">모델명</td>
				<td width="40%">
					<div class="col-md-4">
						<input type="text" class="form-control" id="pdt_model" name="pdt_model" value="${pdt.pdtModel }"/>
					</div>
				</td>
				<td class="td-label">프로모션코드</td>
				<td colspan="3">
					${pdt.promotionCode }
				</td>
			</tr>
			<tr>
				<td class="td-label">상세설명</td>
				<td colspan="5">
					<div class="col-md-12">
						<textarea class="form-control" rows="4" id="pdt_content" name="pdt_content" data-parsley-trigger="change" maxlength="300">${pdt.content }</textarea>
						<p class="length"><span style="color: #5d9bfc;" id="content_length"></span>&nbsp;&nbsp;/&nbsp;&nbsp;300</p>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">태그등록</td>
				<td colspan="5" id="pdt_tag_list">
					<div class="col-md-2">
						<input type="text" class="form-control" id="tag" name="tag"/>
					</div>
					<div class="col-md-1">
						<a class="btn btn-dark btn-block" onclick="onTagAdd()"><i class="fa fa-plus"></i>&nbsp;추가</a>
					</div>
					<c:forEach var="tag" items="${tags}" varStatus="vstatus">
						<div class="col-md-2" style="padding-top: 9px;">
							${tag }
						 	<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onTagDelete(this)"></i>
						 	<input type="hidden" class="tags" value="${tag }"/>
						</div>
                    </c:forEach>
				</td>
			</tr>
			
			<tr>
				<td class="td-label">스타일사진</td>
				<td width="40%">
					<div class="col-md-3">
						<a class="btn btn-dark btn-block" id="btn_photo_style">사진찾기</a>
					</div>
				</td>
				<td class="td-label">서명사진</td>
				<td colspan="3">
					<c:if test="${sign_img != null && !sign_img.isEmpty() }">
						<div class="col-md-12">
							<div style="width: 150px; height: 50px;">
								<a onclick='onImageClick("${sign_img }")' class="fancybox-button" data-rel="fancybox-button">
								<img id="sign_img" src="${sign_img }" width="150px" height="50px"/>
								</a>
							</div>
						</div>
					</c:if>
				</td>
			</tr>
			<tr>
				<td colspan="6">
					
					<div class="row" id="style_img_pad">
						<c:forEach var="real_style_photo" items="${real_style_photos}" varStatus="vstatus">
							<div class="col-md-2 portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px; padding-left: ' + padding_left + 'px">
								<div class="img_pad_bottom portlet-title">
								 	<a onclick='onImageClick("${real_style_photo }")' class="fancybox-button" data-rel="fancybox-button">
	                    			<img src="${real_style_photo }" width="150px" height="150px"></img>
	                    			</a>
	                    			<div class="img_pad_this" onclick="delete_img(this, 2)">
		                        		<i style="color: white;" class="fa fa-times"></i>
		                            </div>
	                            </div>
	                            <input type="hidden" class="style_photos" value="${style_photos[vstatus.index] }"/>
		                    <div class="portlet portlet-sortable-empty"></div></div>
                        </c:forEach>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">발렛코드</td>
				<td colspan="5">
					<div class="col-md-3">
						<input type="number" class="form-control" id="valet_uid" name="valet_uid" value="${pdt == null ? valet_uid : pdt.valetUid }"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td-label">관리자메모</td>
				<td colspan="5">
					<div class="col-md-12">
						<textarea class="form-control" rows="4" id="memo" name="memo">${pdt.memo }</textarea>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="profile_img" name="profile_img" value="${pdt.profileImg }"/>
	<input type="hidden" id="brand_uid" name="brand_uid" value="${pdt.brandUid }"/>
	<input type="hidden" id="usr_uid" name="usr_uid" value="${pdt.usrUid }"/>
	<input type="hidden" id="pdt_size" name="pdt_size" value="${pdt.pdtSize }"/>
	<input type="hidden" id="color_name" name="color_name" value="${pdt.colorName }">
	<c:choose>
		<c:when test="${pdt == null }">
			<input type="hidden" id="status" name="status" value="1">
			<input type="hidden" id="component" name="component" value="1">
			<input type="hidden" id="pdt_condition" name="pdt_condition" value="">
		</c:when>
		<c:otherwise>
			<input type="hidden" id="status" name="status" value="${pdt.status }">
			<input type="hidden" id="component" name="component" value="${pdt.component }">
			<input type="hidden" id="pdt_condition" name="pdt_condition" value="${pdt.pdtCondition }">
		</c:otherwise>
	</c:choose>
	<input type="hidden" id="photo_list" name="photo_list">
	<input type="hidden" id="tag_list" name="tag_list">
	<input type="hidden" id="style_photo_list" name="style_photo_list">
	<input type="hidden" id="pdt_uid" name="pdt_uid" value="${pdt==null?0:pdt.pdtUid }"/>
	</form>
	<div class="row">
		<div class="col-md-1">
			<a class="btn btn-dark-inline btn-block" onclick="onBack()"><i class="fa fa-arrow-left"></i>&nbsp;뒤로</a>
		</div>
		<div class="col-md-offset-5 col-md-1">
				<a class="btn btn-dark btn-block" onclick="onSave()">
					<c:choose>
						<c:when test="${pdt == null }">
							등록
						</c:when>
						<c:otherwise>
							수정
						</c:otherwise>
					</c:choose>
				</a>
		</div>
	</div>
</div>

<a class="btn default hidden" data-toggle="modal" href="#brand_modal" id="btn_brand_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="brand_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">브랜드검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="brand_keyword" name="brand_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onBrandSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">색인</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="first_en" name="first_en">
											<option value="">A-Z</option>
											<option value="A">A</option>
											<option value="B">B</option>
											<option value="C">C</option>
											<option value="D">D</option>
											<option value="E">E</option>
											<option value="F">F</option>
											<option value="G">G</option>
											<option value="H">H</option>
											<option value="I">I</option>
											<option value="J">J</option>
											<option value="K">K</option>
											<option value="L">L</option>
											<option value="M">M</option>
											<option value="N">N</option>
											<option value="O">O</option>
											<option value="P">P</option>
											<option value="Q">Q</option>
											<option value="R">R</option>
											<option value="S">S</option>
											<option value="T">T</option>
											<option value="U">U</option>
											<option value="V">V</option>
											<option value="W">W</option>
											<option value="X">X</option>
											<option value="Y">Y</option>
											<option value="Z">Z</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="first_ko" name="first_ko">
											<option value="">ㄱ-ㅎ</option>
											<option value="ㄱ">ㄱ</option>
											<option value="ㄴ">ㄴ</option>
											<option value="ㄷ">ㄷ</option>
											<option value="ㄹ">ㄹ</option>
											<option value="ㅁ">ㅁ</option>
											<option value="ㅂ">ㅂ</option>
											<option value="ㅅ">ㅅ</option>
											<option value="ㅇ">ㅇ</option>
											<option value="ㅈ">ㅈ</option>
											<option value="ㅊ">ㅊ</option>
											<option value="ㅋ">ㅋ</option>
											<option value="ㅌ">ㅌ</option>
											<option value="ㅍ">ㅍ</option>
											<option value="ㅎ">ㅎ</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>브랜드 선택</strong></h4>
					</div>
					<div class="col-md-offset-8 col-md-2">
						<select class="form-control select" id="brand_length" name="brand_length" onchange="onBrandLengthChange()">
							<option value="100">100개 보기</option>
							<option value="500">500개 보기</option>
							<option value="-1">전체 보기</option>
						</select>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="brand_list" width="100%">
						<thead>
							<tr>
								<th>A-Z</th>
								<th>영문 브랜드명</th>
								<th>ㄱ-ㅎ</th>
								<th>한글 브랜드명</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<a class="btn default hidden" data-toggle="modal" href="#usr_modal" id="btn_usr_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="usr_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>검색</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">유저검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-10">
										<input type="text" class="form-control" id="usr_keyword" name="usr_keyword"/>
									</div>
									<div class="col-md-2">
										<a class="btn btn-dark btn-block" onclick="onUsrSearch()">검색</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>유저 선택</strong></h4>
					</div>
					<div class="col-md-offset-8 col-md-2">
						<select class="form-control select" id="usr_length" name="usr_length" onchange="onUsrLengthChange()">
							<option value="100">100개 보기</option>
							<option value="500">500개 보기</option>
							<option value="-1">전체 보기</option>
						</select>
					</div>
				</div>
				<div class="row">
					<table class="table table-bordered" id="usr_list" width="100%">
						<thead>
							<tr>
								<th>아이디</th>
								<th>닉네임</th>
								<th>실명</th>
								<th>핸드폰번호</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<a class="btn default hidden" data-toggle="modal" href="#size_modal" id="btn_size_modal">View Demo </a>
<div class="modal fade bs-modal-lg" id="size_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong>사이즈</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<table class="table table-bordered search-table">
						<tbody>
							<tr>
								<td class="td-label" style="border-top-color: black; width: 20%;">사이즈검색</td>
								<td style="border-top-color: black; border-right: none;">
									<div class="col-md-12">
										<input type="text" class="form-control" id="size_keyword" name="size_keyword"/>
									</div>
								</td>
								<td rowspan="2" style="border-top-color: black; width: 15%; border-left: none;">
									<a class="btn btn-dark btn-block" style="padding: 30px 14px;" onclick="onSizeSearch()">검색</a>
								</td>
							</tr>
							<tr>
								<td class="td-label">카테고리</td>
								<td>
									<div class="col-md-3">
										<select class="form-control select" id="size_pdt_group" name="size_pdt_group" onchange="onPdtGroupChange1($(this).val())">
											<option value="0">상품군</option>
											<option value="1">남성</option>
											<option value="2">여성</option>
											<option value="4">키즈</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="size_category1" name="size_category1" onchange="onCategory1Change1($(this).val())">
											<option value="0">카테고리1</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="size_category2" name="size_category2" onchange="onCategory2Change1($(this).val())">
											<option value="0">카테고리2</option>
										</select>
									</div>
									<div class="col-md-3">
										<select class="form-control select" id="size_category3" name="size_category3">
											<option value="0">카테고리3</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<div class="row">
					<div class="col-md-2">
						<h4><strong>사이즈 선택</strong></h4>
					</div>
				</div>
				<div class="row" style="height: 300px; overflow-y: auto;">
					<table class="table table-bordered" id="size_list" width="100%">
						<thead>
							<tr>
								<th>상품군</th>
								<th>카테고리1</th>
								<th>카테고리2</th>
								<th>카테고리3</th>
								<th>기준</th>
								<th>분류</th>
								<th>사이즈</th>
								<th>선택</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	var ajax_brand_table;
	var usr_keyword = '';
	var brand_keyword = '';
	var first_en = '';
	var first_ko = '';
	var size_keyword = '';
	var size_pdt_group = 0;
 	var size_category1 = 0;
	var size_category2 = '';
	var size_category3 = '';
	$(document).ready(function () {
		$(".page-sidebar-menu li:eq(1)").addClass("active");
		$(".page-sidebar-menu li:eq(2)").addClass("active");
		$(".nav-tabs li:eq(0)").addClass("active");
		
		if ('${msg}' == 'error')
		{
			alert("DB 접속에 실패하였거나 존재하지 않는 상품입니다.");
			onBack();
		}
		
		var padding_left = ($("#profile_img_pad").width() - 150)/2 + 15;
	    PortletDraggable.init();
		try {
            new AjaxUpload($("#btn_profile_img"), {
                action: "<c:url value="/file/upload"/>",
                name: 'uploadfile',
                responseType: "json",
                onComplete: function (file, response) {
                    if (response.result === "ok") {
                        var img = '<div class="img_pad_bottom portlet-title"><a onclick="onImageClick(\'' + response.fileURL + '\')" class="fancybox-button" data-rel="fancybox-button"> ' + 
                        		  '<img src="' + response.fileURL + '" width="150px" height="150px"/></a> ' + 
	                        	  '<div class="img_pad_this" onclick="delete_img(this, 1)"> ' +
	                        	  '<i style="color: white;" class="fa fa-times"></i>' +
	                              '</div></div><div class="portlet portlet-sortable-empty"></div>';
	                    $("#profile_img_pad").css("padding-left", padding_left);
                        $("#profile_img_pad").html(img);
                        $("#profile_img_pad").removeClass("hidden");
                        $("#profile_img").val(response.fileName);
                    } else {
                        $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                        return;
                    }
                }
            })
        } catch (e) {
            alert(e);
        }
        
        try {
            new AjaxUpload($("#btn_photo_search"), {
                action: "<c:url value="/file/uploads"/>",
                name: 'uploadfiles',
                responseType: "json",
                onComplete: function (file, response) {
                    if (response.result === "ok") {
                    	var json = response.data;
                   		for (var i=0; i<json.length; i++) {
	                    	var div = '<div class="col-md-2 portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px; padding-left: ' + padding_left + 'px"><div class="img_pad_bottom portlet-title"> ' +
	                    			  '<a onclick="onImageClick(\'' + json[i].fileURL + '\')" class="fancybox-button" data-rel="fancybox-button">' + 
	                    			  '<img src="' + json[i].fileURL + '" width="150px" height="150px"/></a> ' +
	                    			  '<div class="img_pad_this" onclick="delete_img(this, 2)"> ' +
		                        	  '<i style="color: white;" class="fa fa-times"></i>' +
		                              '</div></div><input type="hidden" class="photos" value="' + json[i].fileName + '"/>' +
		                              '<div class="portlet portlet-sortable-empty"></div></div>';
	                        $("#img_pad").append(div);
                        }
                    } else {
                        $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                        return;
                    }
                }
            })
        } catch (e) {
            alert(e);
        }
        
        try {
            new AjaxUpload($("#btn_photo_style"), {
                action: "<c:url value="/file/uploads"/>",
                name: 'uploadfiles',
                responseType: "json",
                onComplete: function (file, response) {
                    if (response.result === "ok") {
                    	var json = response.data;
                   		for (var i=0; i<json.length; i++) {
	                    	var div = '<div class="col-md-2 portlet portlet-sortable light" style="padding-top: 15px; padding-bottom: 15px; padding-left: ' + padding_left + 'px"><div class="img_pad_bottom portlet-title"> ' +
	                    			  '<a onclick="onImageClick(\'' + json[i].fileURL + '\')" class="fancybox-button" data-rel="fancybox-button">' + 
	                    			  '<img src="' + json[i].fileURL + '" width="150px" height="150px"/></a> ' +
	                    			  '<div class="img_pad_this" onclick="delete_img(this, 2)"> ' +
		                        	  '<i style="color: white;" class="fa fa-times"></i>' +
		                              '</div></div><input type="hidden" class="style_photos" value="' + json[i].fileName + '"/>' +
		                              '<div class="portlet portlet-sortable-empty"></div></div>';
	                        $("#style_img_pad").append(div);
                        }
                    } else {
                        $toast = toastr["error"]('', '이미지업로드가 실패했습니다.');
                        return;
                    }
                }
            })
        } catch (e) {
            alert(e);
        }
        
        ajax_brand_table = $("#brand_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_brand_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#brand_length").val();
                    d.keyword = brand_keyword;
                    d.first_ko = first_ko;
                    d.first_en = first_en;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(4)', row).html('선택');
           		$('td:eq(4)', row).css('cursor', 'pointer');
           		$('td:eq(4)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(4)', row).attr('onclick', 'onBrandSelectEach("' + data[1] + '", "' + data[4] + '")');
           	}
		});
		
		ajax_usr_table = $("#usr_list").DataTable({
			dom : 'tp',
			"stateSave": false,
            "autowidth": true,
            "serverSide": true,
            "ordering": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_usr_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.length = $("#usr_length").val();
                    d.keyword = usr_keyword;
                }
            },
            "pageLength": 100,
           	"createdRow": function (row, data, dataIndex) {
           		$('td:eq(4)', row).html('선택');
           		$('td:eq(4)', row).css('cursor', 'pointer');
           		$('td:eq(4)', row).css('background', 'RGB(242,242,242)');
           		$('td:eq(4)', row).attr('onclick', 'onUsrSelectEach("' + data[0] + '", "' + data[4] + '")');
           	}
		});
		
		ajax_size_table = $("#size_list").DataTable({
			dom : 't',
			"stateSave": false,
            "serverSide": true,
            "ordering": false,
            "paging": false,
            "language" : {
               	"emptyTable": '<center>데이터가 없습니다.</center>',
               	"paginate" : {
               		"previous" : '<i class="fa fa-caret-left" style="font-size: 24px; padding-top: 2px;"></i>',
               		"next" : '<i class="fa fa-caret-right" style="font-size: 24px; padding-top: 2px;"></i>',
               	},
            },
            "deferRender": true,
            "ajax": {
                "url": "<c:url value='/pdt/pdt_mng/ajax_size_table'/>",
                "type": "POST",
                "data":   function ( d ) {
                    start_index = d.start;
                    d.keyword = size_keyword;
                    d.pdt_group = size_pdt_group;
                    d.category1 = size_category1;
                    d.category2 = size_category2;
                    d.category3 = size_category3;
                }
            },
           	"createdRow": function (row, data, dataIndex) {
           		if (data[2] == '')
           			$('td:eq(2)', row).html('-');
           		if (data[3] == '')
           			$('td:eq(3)', row).html('-');
           		if (data[4] == '')
           			$('td:eq(4)', row).html('-');
           		$('td:eq(6)', row).css('width', '220px');
           		$('td:eq(7)', row).html('선택');
           		$('td:eq(7)', row).css('cursor', 'pointer');
           		$('td:eq(7)', row).css('background', 'RGB(242,242,242)');
           		if (data[8] == 1) {
           			$('td:eq(6)', row).html('<input type="text" class="form-control size_width" placeholder="가로" style="display: inline; width: 60px;"/>' +
           			' X <input type="text" class="form-control size_height" placeholder="세로" style="display: inline; width: 60px;"/>' +
           			' X <input type="text" class="form-control size_wide" placeholder="폭" style="display: inline; width: 60px;"/>');
           			$('td:eq(7)', row).attr('onclick', 'onSizeSelectEach(' + data[8] + ', "", this)');
           		} else if (data[8] == 2) {
           			$('td:eq(6)', row).css('color', '#5d9bfc');
           			$('td:eq(6)', row).html('FREE SIZE');
           			$('td:eq(7)', row).attr('onclick', 'onSizeSelectEach(' + data[8] + ', "FREE SIZE", this)');
           		} else if (data[8] == 3) {
           			$('td:eq(6)', row).html('<input type="text" class="form-control size_input" placeholder="직접입력(10자)" data-parsley-trigger="change" maxlength="10"/>');
           			$('td:eq(7)', row).attr('onclick', 'onSizeSelectEach(' + data[8] + ', "", this)');
           		} else {
           			$('td:eq(6)', row).css('color', '#5d9bfc');
           			$('td:eq(7)', row).attr('onclick', 'onSizeSelectEach(' + data[8] + ', "' + data[5] + ' ' + data[6] + '", this)');
           		}
           	}
		});
	});
	
	function delete_img(obj, tp) {
		if (!confirm("해당 이미지를 삭제하시겠습니까?"))
			return;
		if (tp == 1) {
			$(obj).parent('div').parent('div').html("");
			$("#profile_img_pad").addClass("hidden");
		} else {
			$(obj).parent('div').parent('div').remove();
		}
	}
	
	function onBrandSelect() {
    	$("#btn_brand_modal").trigger('click');
    }
    
    function onBrandSelectEach(name_en, id) {
    	$("#pdt_brand_list").html(name_en);
    	$("#brand_uid").val(id);
    	$(".close").trigger('click');
    }
    
	function onBrandLengthChange() {
		ajax_brand_table.draw(false);
	}
	
	function onBrandSearch() {
		first_en = $("#first_en").val();
		first_ko = $("#first_ko").val();
		brand_keyword = $("#brand_keyword").val();
		ajax_brand_table.draw(false);
	}
	
	$("#usr_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onUsrSearch();
        }
    });
    
    $("#brand_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onBrandSearch();
        }
    });
    
    $("#size_keyword").keyup(function(event){
        if (event.keyCode == 13) {
            onSizeSearch();
        }
    });
	
	function onUsrSelect() {
    	$("#btn_usr_modal").trigger('click');
    }
    
    function onUsrSelectEach(name_en, id) {
    	$("#pdt_usr_list").html(name_en);
    	$("#usr_uid").val(id);
    	$(".close").trigger('click');
    }
    
	function onUsrLengthChange() {
		ajax_usr_table.draw(false);
	}
	
	function onUsrSearch() {
		usr_keyword = $("#usr_keyword").val();
		ajax_usr_table.draw(false);
	}
	
	function onTagAdd() {
		if ($.trim($("#tag").val()) == "")
		{
			$toast = toastr["error"]('', '태그를 입력하세요.');
			return;
		}
		$("#pdt_tag_list").append('<div class="col-md-2" style="padding-top: 9px;">' + $("#tag").val() +
		 '<i class="fa fa-times-circle" style="font-size: 18px;" onclick="onTagDelete(this)"></i> ' +
		 '<input type="hidden" class="tags" value="' + $("#tag").val() + '"/></div>');
		$("#tag").val("");
	}
	
	function onTagDelete(obj) {
		$(obj).parent('div').remove();
	}
	
	function onSizeSelect() {
		$("#btn_size_modal").trigger('click');
	}
	
	function onSizeSelectEach(status, str, obj) {
		if (status == 1) {
			var size_width = $(obj).prev('td').find('.size_width').val();
			if ($.trim(size_width) == '') {
				$toast = toastr['error']('', '가로를 입력하세요.');
				$(obj).prev('td').find('.size_width').focus();
				return;
			}
			var size_height = $(obj).prev('td').find('.size_height').val();
			if ($.trim(size_height) == '') {
				$toast = toastr['error']('', '세로를 입력하세요.');
				$(obj).prev('td').find('.size_height').focus();
				return;
			}
			var size_wide = $(obj).prev('td').find('.size_wide').val();
			if ($.trim(size_wide) == '') {
				$toast = toastr['error']('', '폭을 입력하세요.');
				$(obj).prev('td').find('.size_wide').focus();
				return;
			}
			str = size_width + ' X ' + size_height + ' X ' + size_wide;
		}
		else if (status == 3) {
			str = $(obj).prev('td').find('.size_input').val();
			if ($.trim(str) == '') {
				$toast = toastr['error']('', '직접입력값을 입력하세요.');
				$(obj).prev('td').find('.size_input').focus();
				return;
			}
		}
		$("#size").html(str);
		$("#pdt_size").val(str);
		$(".close").trigger('click');
	}
	
	$(".sale").change(function () {
		$("#status").val($(this).val());
	});
	
	$(".condition").change(function () {
		$("#pdt_condition").val($(this).val());
	});
	
	$(".color").change(function () {
		$("#color_name").val($(this).val());
	});
	
	function onSizeSearch() {
		size_keyword = $("#size_keyword").val();
		size_pdt_group = $("#size_pdt_group").val();
		size_category1 = $("#size_category1").val();
		size_category2 = $("#size_category2").val();
		size_category3 = $("#size_category3").val();
		ajax_size_table.draw(false);
	}
	
	function onSave() {
	//check
		if ($("#status").val() == "") {
			$toast = toastr["error"]('', '판매상태를 선택하세요.');
			return;
		}
	
		if ($("#usr_uid").val() == "") {
			$toast = toastr["error"]('', '등록유저를 선택하세요.');
			return;
		}
		
		if ($("#pdt_group").val() == "" || $("#pdt_group").val() == 0) {
			$toast = toastr["error"]('', '상품군을 선택하세요.');
			return;
		}
		
		if ($("#category1").val() == "" || $("#category1").val() == 0)
		{
			$toast = toastr["error"]('', '카테고리1을 선택하세요.');
			return;
		}
		
		if ($("#category2 > option").length > 1 && ($("#category2").val() == "" || $("#category2").val() == 0))
		{
			$toast = toastr["error"]('', '카테고리2를 선택하세요.');
			return;
		}
		
		if ($("#category3 > option").length > 1 && ($("#category3").val() == "" || $("#category3").val() == 0))
		{
			$toast = toastr["error"]('', '카테고리3을 선택하세요.');
			return;
		}
		
		if ($("#brand_uid").val() == "") {
			$toast = toastr["error"]('', '브랜드를 선택하세요.');
			return;
		}
		
		if ($("#pdt_size").val() == "") {
			$toast = toastr["error"]('', '사이즈를 선택하세요.');
			return;
		}
		
		if ($("#profile_img").val() == "") {
			$toast = toastr["error"]('', '대표사진을 선택하세요.');
			return;
		}
		
		var photo_list = '';
		$(".photos").each(function () {
			photo_list += $(this).val() + ',';
		});
		
		// if (photo_list == '') {
		// 	$toast = toastr["error"]('', '사진리스트를 선택하세요.');
		// 	return;
		// }
		
		$("#photo_list").val(photo_list);
		
		if ($("#price").val() == "") {
			$toast = toastr["error"]('', '판매가격을 입력하세요.');
			$("#price").focus();
			return;
		}

		if ($("#send_price").val() == "") {
			$toast = toastr["error"]('', '배송비를 입력하세요.');
			$("#send_price").focus();
			return;
		}
		
		if ($("#pdt_condition").val() == "") {
			$toast = toastr["error"]('', '컨디션을 선택하세요.');
			return;
		}
		
		var component = '';
		$(".component").each(function () {
			if ($(this).is(':checked'))
				component += '1';
			else
				component += '0';
		});
		
		$("#component").val(component);
		
		if ($("#color_name").val() == "") {
			$toast = toastr["error"]('', '컬러를 선택하세요.');
			return;
		}
		
		if ($("#pdt_model").val() == "") {
			$toast = toastr["error"]('', '모델명을 입력하세요.');
			$("#pdt_model").focus();
			return;
		}
		
		// if ($("#pdt_content").val() == "") {
		// 	$toast = toastr["error"]('', '상세설명을 입력하세요.');
		// 	$("#pdt_content").focus();
		// 	return;
		// }
		
		var tag_list = '';
		$(".tags").each(function () {
			tag_list += '#' + $(this).val() + ' ';
		});
		$("#tag_list").val(tag_list);
		// if (tag_list == '') {
		// 	$toast = toastr["error"]('', '태그를 선택하세요.');
		// 	return;
		// }
		
		var style_photo_list = '';
		$(".style_photos").each(function () {
			style_photo_list += $(this).val() + ',';
		});
		$("#style_photo_list").val(style_photo_list);
		
		$.post('<c:url value="/pdt/pdt_mng/"/>' + $("#pdt_uid").val() + '/save', $("#pdt_form").serialize(), function (result) {
			if (result == "success") {
				$toast = toastr["success"]('', '성과적으로 저장되었습니다.');
				setTimeout('onBack()', 1000);
			} else {
				$toast = toastr["error"]('', '저장이 실패했습니다.');
			}
		});
	}
	
	function onPdtGroupChange(id) {
    	$('#category1').empty();
        $('#category1').append('<option value="0">카테고리1</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category1').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory1Change(json[0].categoryUid);
	            else
	            	onCategory1Change(0);
	        });
	    } else {
	    	onCategory1Change(0);
	    }
	}
	
	function onCategory1Change(id) {
		$('#category2').empty();
        $('#category2').append('<option value="0">카테고리2</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category2').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory2Change(json[0].categoryUid);
	            else
	            	onCategory2Change(0);
	        });
	   	} else {
	   		onCategory2Change(0);
	   	}
	}
	
	function onCategory2Change(id) {
		$('#category3').empty();
        $('#category3').append('<option value="0">카테고리3</option>');
		if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#category3').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	        });
	    }
	}
	
	function onPdtGroupChange1(id) {
    	$('#size_category1').empty();
        $('#size_category1').append('<option value="0">카테고리1</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_children"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#size_category1').append('<option value="' + json[i].categoryUid + '">' + json[i].categoryName + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory1Change1(json[0].categoryUid);
	            else
	            	onCategory1Change1(0);
	        });
	    } else {
	    	onCategory1Change1(0);
	    }
	}
	
	function onCategory1Change1(id) {
		$('#size_category2').empty();
        $('#size_category2').append('<option value="">카테고리2</option>');
        if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_size_category2"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#size_category2').append('<option value="' + json[i] + '">' + json[i] + '</option>');
	            }
	            if (json.length > 0)
	            	onCategory2Change1(json[0]);
	            else
	            	onCategory2Change1(0);
	        });
	   	} else {
	   		onCategory2Change1(0);
	   	}
	}
	
	function onCategory2Change1(id) {
		$('#size_category3').empty();
        $('#size_category3').append('<option value="">카테고리3</option>');
		if (id != 0) {
			$.post('<c:url value="/pdt/pdt_mng/get_size_category3"/>', {parent: id}, function (result) {
	            var json = result;
	            for (var i = 0; i < json.length; i++) {
	                $('#size_category3').append('<option value="' + json[i] + '">' + json[i] + '</option>');
	            }
	        });
	    }
	}
	
	function remove_pdt() {
		if(!confirm('해당 상품을 삭제하시겠습니까?'))
			return;
		location.href = "<c:url value="/pdt/pdt_mng/"/>${pdt.pdtUid }/remove";
	}
	
	$("#pdt_content").keydown(function(event){
        if (event.keyCode != 13) {
            var value = $("#pdt_content").val();
        	$("#content_length").html(value.length);
        }
    });
    
    $("#pdt_content").keyup(function(event){
        if (event.keyCode != 13) {
            var value = $("#pdt_content").val();
        	$("#content_length").html(value.length);
        }
    });
</script>
