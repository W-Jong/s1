<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="page-bar">
	<div class="header_title">
		<strong><span style="font-size: 20px; color: black;">상품</span></strong>
	</div>
	<ul class="nav nav-tabs">
		<li>
			<a href="<c:url value="/pdt/pdt_mng"/>" style="cursor: pointer;">
			리스트 </a>
		</li>
		<li>
			<a href="<c:url value="/pdt/valet_mng"/>"  style="cursor: pointer;">
			발렛관리 </a>
		</li>
		<li>
			<a href="<c:url value="/pdt/pdt_class"/>" style="cursor: pointer;">
			상품분류 </a>
		</li>
	</ul>
</div>

<script>
	$(document)
	  .ajaxStart(function() {
	    loadingOverlay();
	}).ajaxStop(function() {
	    loadingOverlayRemove();
	});
</script>
