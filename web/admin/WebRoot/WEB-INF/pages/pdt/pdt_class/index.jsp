<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script
	src="<c:url value="/resources/global/plugins/jstree/dist/jstree.js"/>"></script>

<style>
.page_nav {
	color: RGB(166, 166, 166);
}

.row {
	margin: 0 0;
}

.row>h4 {
	padding-left: 15px;
}

.td-label {
	background: #ebeef3 !important;
	width: 10%;
	padding-left: 20px !important;
}

.search-table>tbody>tr>td {
	vertical-align: middle;
	text-align: left !important;
	background: white;
}

.table>tbody>tr>td {
	vertical-align: top;
	text-align: left;
	background: white;
}

.table>thead>tr>th {
	vertical-align: middle;
	text-align: left;
	color: black;
	font-weight: normal;
}

.table>thead>tr {
	background: RGB(89, 89, 89);
}

.input-group-addon {
	border: none !important;
	background: none !important;
	padding-top: 2px;
}

.table .btn {
	margin-right: 0;
}

.input-group-btn {
	padding-left: 10px;
}

.btn-default:active, .btn-default.active, .btn-default.active:hover {
	background: RGB(89, 89, 89);
	border-color: RGB(89, 89, 89);
	color: white;
}

.select {
	border-radius: 8px !important;
}

.icon-calendar {
	color: RGB(166, 166, 166);
}

div.dataTables_paginate {
	float: none;
	text-align: center;
}

.pagination>li>a {
	border: none;
	color: RGB(89, 89, 89);
	background: transparent !important;
}

.pagination>.active>a {
	border: none;
	border-radius: 50% !important;
	color: white;
	background: RGB(89, 89, 89) !important;
}

.modal .modal-header {
	border-bottom: none;
}

.modal-body {
	padding: 0;
}

.modal-content {
	background: #f8f8fa;
}
</style>

<div class="row" style="margin-top: 40px;">
	<div class="col-md-6">
		<a class="page_nav" href="<c:url value="/pdt/pdt_mng"/>">상품</a>&nbsp;&nbsp;<i
			class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#"
			class="page_nav">상품분류</a>
	</div>
</div>

<div class="row" style="margin-top: 20px;">
	<h4>
		<strong>상품분류</strong>
	</h4>
</div>

<table class="table table-bordered">
	<thead>
		<tr style="background: #ebeef3;">
			<th>남성</th>
			<th>여성</th>
			<th>키즈</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="col-md-4">
				<div id="tree1" class="tree-demo"></div>
			</td>
			<td class="col-md-4">
				<div id="tree2" class="tree-demo"></div>
			</td>
			<td class="col-md-4">
				<div id="tree4" class="tree-demo"></div>
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-md-offset-5 col-md-1">
		<a class="btn btn-dark btn-block" onclick="onSave()">수정</a>
	</div>
	<div class="col-md-1">
		<a class="btn btn-dark-inline btn-block" onclick="onInitialize()">초기화</a>
	</div>
</div>

<script>
	$(document).ready(function() {
		$(".page-sidebar-menu li:eq(1)").addClass("active");
		$(".page-sidebar-menu li:eq(4)").addClass("active");
		$(".nav-tabs li:eq(2)").addClass("active");
		$("#tree1").jstree({
			"core" : {
				"themes" : {
					icons : false
				},
				// so that create works
				"check_callback" : true,
				'data' : ${data1}
			},
			"plugins" : [ "contextmenu", "dnd" ]
		});
		
		$("#tree2").jstree({
			"core" : {
				"themes" : {
					icons : false
				},
				// so that create works
				"check_callback" : true,
				'data' : ${data2}
			},
			"plugins" : [ "contextmenu", "dnd" ]
		});
		
		$("#tree4").jstree({
			"core" : {
				"themes" : {
					icons : false
				},
				// so that create works
				"check_callback" : true,
				'data' : ${data4}
			},
			"plugins" : [ "contextmenu", "dnd" ]
		});
	});
	
	$.jstree.defaults.contextmenu = {
		select_node : true,
		show_at_node : true,
		items : function (o, cb) { // Could be an object directly
			var menu = {};
			if (!(o.id >= 5 && o.id <= 19)) {
				menu.remove = {
					"separator_before"	: false,
					"icon"				: false,
					"separator_after"	: false,
					"_disabled"			: false, //(this.check("delete_node", data.reference, this.get_parent(data.reference), "")),
					"label"				: "삭제",
					"action"			: function (data) {
						var inst = $.jstree.reference(data.reference),
							obj = inst.get_node(data.reference);
						var category_uid = inst.get_selected();
						if (o.id[0] != 'j') {
							$.post('<c:url value="/pdt/pdt_class/"/>' + category_uid + '/confirm_remove', {}, function (result) {
								if (result == "yes") {
									if(inst.is_selected(obj)) {
										inst.delete_node(inst.get_selected());
									}
									else {
										inst.delete_node(obj);
									}
								} else {
									$toast = toastr['error']('해당 카테고리에 등록된 상품이 있으므로 삭제할수 없습니다.');
									return;
								}
							});
						} else {
							if(inst.is_selected(obj)) {
								inst.delete_node(inst.get_selected());
							}
							else {
								inst.delete_node(obj);
							}
						}
					}
				};
				
				menu.rename = {
					"separator_before"	: false,
					"separator_after"	: false,
					"_disabled"			: false, //(this.check("rename_node", data.reference, this.get_parent(data.reference), "")),
					"label"				: "이름변경",
					"action"			: function (data) {
						var inst = $.jstree.reference(data.reference),
							obj = inst.get_node(data.reference);
						inst.edit(obj);
					}
				};
				
				menu.add = {
					"separator_before"	: false,
					"separator_after"	: false,
					"_disabled"			: false, //(this.check("create_node", data.reference, {}, "last")),
					"label"				: "추가",
					"action"			: function (data) {
						var inst = $.jstree.reference(data.reference),
							obj = inst.get_node(data.reference);
						inst.create_node(obj, {}, "after", function (new_node) {
							setTimeout(function () { inst.edit(new_node); },0);
						});
					}
				};
			}
			
			if (o.parent >= 5 && o.parent <= 19 || o.id >= 5 && o.id <= 19) {
				menu.create = {
					"separator_before"	: false,
					"separator_after"	: false,
					"_disabled"			: false, //(this.check("create_node", data.reference, {}, "last")),
					"label"				: "하위생성",
					"action"			: function (data) {
						var inst = $.jstree.reference(data.reference),
							obj = inst.get_node(data.reference);
						inst.create_node(obj, {}, "last", function (new_node) {
							setTimeout(function () { inst.edit(new_node); },0);
						});
					}
				};
			}
			return menu;
		}
	};
	
	function onSave() {
		var data1 = JSON.stringify($("#tree1").jstree().get_json(null, { no_data : true, no_state : true }));
		var data2 = JSON.stringify($("#tree2").jstree().get_json(null, { no_data : true, no_state : true }));
		var data4 = JSON.stringify($("#tree4").jstree().get_json(null, { no_data : true, no_state : true }));
		$.post('<c:url value="/pdt/pdt_class/save"/>', {data1: data1, data2: data2, data4: data4}, function (result) {
            if (result == "success") {
            	$toast = toastr["success"]('', '성과적으로 저장되었습니다.');
            } else {
            	$toast = toastr["error"]('', '저장이 실패했습니다.');
            }
        });
	}
	
	function onInitialize() {
		location.reload();
	}
</script>
