<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script src="<c:url value="/resources/global/plugins/amcharts/amcharts/amcharts.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/global/plugins/amcharts/amcharts/serial.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/global/plugins/amcharts/amcharts/themes/custom.js"/>" type="text/javascript"></script>

<style>
	.page-content {
		padding-top: 10px !important;
	}
	
	.page_nav {
		color: RGB(166,166,166);
	}
	
	.row {
		margin: 0 0;
	}
	
    .td-label {
    	background: #ebeef3 !important;
    	padding-left: 20px !important;
    	text-align: left !important;
    	height: 51px;
    }
    
    .td-label-right {
    	background: #ebeef3 !important;
    	padding-right: 20px !important;
    	text-align: right !important;
    	height: 51px;
    }
    
    .td-right {
    	text-align: right !important;
    	padding-right: 20px !important;
    }
    
    .table > tbody > tr > td {
    	vertical-align: middle;
    	text-align: center;
    	background: white;
    }
    
	.btn-default:active, .btn-default.active, .btn-default.active:hover {
		background: RGB(89,89,89);
		border-color: RGB(89,89,89);
		color: white;
	}
	
	.percent-down {
		color: #5d9bfc;
	}
	
	.percent-up {
		color: #fc9b5d;
	}
	
	td > i {
		font-size: 10px !important;
	}
	
	td > strong {
		font-size: 15px;
	}
</style>

<div class="row" style="border-bottom: 1px solid black; margin-bottom: 20px;">
	<div class="col-md-4">
		<h4><strong>대시보드</strong></h4>
	</div>
	<div class="col-md-offset-4 col-md-4" style="text-align: right;">
		<h5><strong>${reg_time } 집계</strong></h5>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td class="td-label" style="border-right: none;">신규가입</td>
					<td class="td-label-right" style="border-left: none;"><strong style="font-size: 16px;">${newreg }</strong></td>
				</tr>
				<tr>
					<c:if test="${newreg_percent2 < 0 }">
						<td>지난주 대비 <strong class="percent-down">${newreg_percent2 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${newreg_percent2 >= 0 }">
						<td>지난주 대비 <strong class="percent-up">${newreg_percent2 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
					<c:if test="${newreg_percent1 < 0 }">
						<td>전날 대비 <strong class="percent-down">${newreg_percent1 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${newreg_percent1 >= 0 }">
						<td>전날 대비 <strong class="percent-up">${newreg_percent1 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-3">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td class="td-label" style="border-right: none;">방문자수</td>
					<td class="td-label-right" style="border-left: none;"><strong style="font-size: 16px;">${usr }</strong></td>
				</tr>
				<tr>
					<c:if test="${usr_percent2 < 0 }">
						<td>지난주 대비 <strong class="percent-down">${usr_percent2 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${usr_percent2 >= 0 }">
						<td>지난주 대비 <strong class="percent-up">${usr_percent2 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
					<c:if test="${usr_percent1 < 0 }">
						<td>전날 대비 <strong class="percent-down">${usr_percent1 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${usr_percent1 >= 0 }">
						<td>전날 대비 <strong class="percent-up">${usr_percent1 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-3">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td class="td-label" style="border-right: none;">신규거래</td>
					<td class="td-label-right" style="border-left: none;"><strong style="font-size: 16px;">${newdeal }</strong></td>
				</tr>
				<tr>
					<c:if test="${newdeal_percent2 < 0 }">
						<td>지난주 대비 <strong class="percent-down">${newdeal_percent2 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${newdeal_percent2 >= 0 }">
						<td>지난주 대비 <strong class="percent-up">${newdeal_percent2 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
					<c:if test="${newdeal_percent1 < 0 }">
						<td>전날 대비 <strong class="percent-down">${newdeal_percent1 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${newdeal_percent1 >= 0 }">
						<td>전날 대비 <strong class="percent-up">${newdeal_percent1 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-3">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td class="td-label" style="border-right: none;">거래대금</td>
					<td class="td-label-right" style="border-left: none;"><strong style="font-size: 16px;">${price }</strong></td>
				</tr>
				<tr>
					<c:if test="${price_percent2 < 0 }">
						<td>지난주 대비 <strong class="percent-down">${price_percent2 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${price_percent2 >= 0 }">
						<td>지난주 대비 <strong class="percent-up">${price_percent2 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
					<c:if test="${price_percent1 < 0 }">
						<td>전날 대비 <strong class="percent-down">${price_percent1 }</strong>%<i class="fa fa-long-arrow-down"></i></td>
					</c:if>
					<c:if test="${price_percent1 >= 0 }">
						<td>전날 대비 <strong class="percent-up">${price_percent1 }</strong>%<i class="fa fa-long-arrow-up"></i></td>
					</c:if>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row" style="border-bottom: 1px solid black; margin-bottom: 20px;">
	<div class="col-md-4">
		<h4><strong>주문관리</strong></h4>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td class="td-label col-md-2">배송준비</td>
					<td class="td-right col-md-2" style="cursor: pointer;" onclick="go_url('<c:url value="/deal/deal_mng/1/index"/>');"><strong class="td-number">${derivery_ready_count }</strong> 건</td>
					<td class="td-label col-md-2">배송진행</td>
					<td class="td-right col-md-2" style="cursor: pointer;" onclick="go_url('<c:url value="/deal/deal_mng/2/index"/>');"><strong class="td-number">${derivery_count }</strong> 건</td>
					<td class="td-label col-md-2">정품인증</td>
					<td class="td-right col-md-2" style="cursor: pointer;" onclick="go_url('<c:url value="/deal/deal_mng/3/index"/>');"><strong class="td-number">${verify_count }</strong> 건</td>
				</tr>
				<tr>
					<td class="td-label">배송완료</td>
					<td class="td-right" style="cursor: pointer;" onclick="go_url('<c:url value="/deal/deal_mng/4/index"/>');"><strong class="td-number">${derivery_done_count }</strong> 건</td>
					<td class="td-label">거래완료</td>
					<td class="td-right" style="cursor: pointer;" onclick="go_url('<c:url value="/deal/deal_mng/5/index"/>');"><strong class="td-number">${deal_done_count }</strong> 건</td>
					<td class="td-label">반품과정</td>
					<td class="td-right" style="cursor: pointer;" onclick="go_url('<c:url value="/deal/deal_mng/7/index"/>');"><strong class="td-number">${refund_count }</strong> 건</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row" style="border-bottom: 1px solid black; margin-bottom: 20px;">
	<div class="col-md-4">
		<h4><strong>운영관리</strong></h4>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td class="td-label col-md-2">포토샵전 신규상품</td>
					<td class="td-right col-md-2" style="cursor: pointer;" onclick="go_url('<c:url value="/pdt/pdt_mng?photoshop_yn=2"/>');"><strong class="td-number">${pdt_count }</strong> 건</td>
					<td class="td-label col-md-2">신규발렛접수</td>
					<td class="td-right col-md-2" style="cursor: pointer;" onclick="go_url('<c:url value="/pdt/valet_mng?status=2"/>');"><strong class="td-number">${valet_count }</strong> 건</td>
					<td class="td-label col-md-2">답변대기  문의</td>
					<td class="td-right col-md-2" style="cursor: pointer;" onclick="go_url('<c:url value="/system/qna_mng?status=2"/>');"><strong class="td-number">${qna_count }</strong> 건</td>
				</tr>
				<tr>
					<td class="td-label">미해결 신고</td>
					<td class="td-right" style="cursor: pointer;" onclick="go_url('<c:url value="/system/report_mng?status=2"/>');"><strong class="td-number">${report_count }</strong> 건</td>
					<td class="td-label">미처리 가품신고</td>
					<td class="td-right" style="cursor: pointer;" onclick="go_url('<c:url value="/reward/report_reward_mng?status=2"/>');"><strong class="td-number">${report_pdt_count }</strong> 건</td>
					<td class="td-label">미처리 공유리워드</td>
					<td class="td-right" style="cursor: pointer;" onclick="go_url('<c:url value="/reward/share_reward_mng?status=2"/>');"><strong class="td-number">${share_count }</strong> 건</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row" style="border-bottom: 1px solid black; margin-bottom: 20px;">
	<div class="col-md-4">
		<h4><strong>주요통계</strong></h4>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-offset-3 col-md-6">
				<div class="btn-group" data-toggle="buttons" style="width: 100%;">
					<label class="btn btn-default col-md-3">
					<input type="radio" class="toggle period1" name="period1" value="1"> 일 </label>
					<label class="btn btn-default active col-md-3">
					<input type="radio" class="toggle period1" name="period1" value="2" checked> 주 </label>
					<label class="btn btn-default col-md-3">
					<input type="radio" class="toggle period1" name="period1" value="3"> 월 </label>
					<label class="btn btn-default col-md-3">
					<input type="radio" class="toggle period1" name="period1" value="4"> 년 </label>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 15px; background: white; border: 1px solid RGB(217,217,217)">
			<div id="chart_1" class="chart" style="height: 500px;">
			</div>
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-md-offset-2 col-md-1" style="height: 20px; background: #8d5dfc;"></div>
				<div class="col-md-2">신규가입</div>
				<div class="col-md-1" style="height: 20px; background: #5d9bfc;"></div>
				<div class="col-md-2">방문자수</div>
				<div class="col-md-1" style="margin-top: 5px; height: 10px; background: #fc9d5d;"></div>
				<div class="col-md-2">방문횟수</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-offset-3 col-md-6">
				<div class="btn-group" data-toggle="buttons" style="width: 100%;">
					<label class="btn btn-default col-md-3">
					<input type="radio" class="toggle period2" name="period2" value="1"> 일 </label>
					<label class="btn btn-default active col-md-3">
					<input type="radio" class="toggle period2" name="period2" value="2" checked> 주 </label>
					<label class="btn btn-default col-md-3">
					<input type="radio" class="toggle period2" name="period2" value="3"> 월 </label>
					<label class="btn btn-default col-md-3">
					<input type="radio" class="toggle period2" name="period2" value="4"> 년 </label>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top: 15px; background: white; border: 1px solid RGB(217,217,217)">
			<div id="chart_2" class="chart" style="height: 500px;">
			</div>
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-md-offset-2 col-md-2" style="height: 20px; background: #5d9bfc;"></div>
				<div class="col-md-2">거래</div>
				<div class="col-md-2" style="margin-top: 5px; height: 10px; background: #fc9d5d;"></div>
				<div class="col-md-2">거래대금</div>
			</div>
		</div>
	</div>
</div>

<script>
	var chart1;
	var period1 = 2;
	var chart2;
	var period2 = 2;
	$(document).ready(function () {
		onDrawGraph1(period1);
		onDrawGraph2(period2);
	});
	
	$(".period1").change(function () {
		period1 = $(this).val();
		onDrawGraph1(period1);
	});
	
	$(".period2").change(function () {
		period2 = $(this).val();
		onDrawGraph2(period2);
	});
	
	function onDrawGraph2(period2) {
		$.post('<c:url value="/statistics/deal_statistics/update"/>', {period: period2}, function (result) {
			chart2 = AmCharts.makeChart("chart_2", {
	            "type": "serial",
	            "theme": "custom",
	            "autoMargins": false,
	            "marginLeft": 50,
	            "marginRight": 50,
	            "marginTop": 30,
	            "marginBottom": 50,
	            "fontFamily": 'Open Sans',            
	            "color":    '#888',
	            "dataProvider": result,
	            "valueAxes": [{
	            	"id": "countAxis",
	                "axisAlpha": 0,
	                "position": "left"
	            },
	            {
	            	"id": "priceAxis",
	                "axisAlpha": 0,
	                "position": "right",
	                "gridAlpha": 0
	            }],
	            "startDuration": 0,
	            "graphs": [{
	                "alphaField": "alpha",
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthColumn",
	                "fillAlphas": 1,
	                "title": "거래",
	                "type": "column",
	                "valueField": "count",
	                "valueAxis": "countAxis"
	            }, {
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthLine",
	                "lineThickness": 3,
	                "fillAlphas": 0,
	                "lineAlpha": 1,
	                "title": "거래대금",
	                "valueField": "price",
	                "valueAxis": "priceAxis"
	            }],
	            "categoryField": "time",
	            "categoryAxis": {
	                "gridPosition": "start",
	                "axisAlpha": 0,
	                "tickLength": 0,
	                "gridAlpha": 0
	            }
	        });
	        $("#chart_2").find('a').text("");
		});
	}
	
	function onDrawGraph1(period1) {
		$.post('<c:url value="/statistics/visit_statistics/update"/>', {period: period1}, function (result) {
			graph_data = result;
            chart1 = AmCharts.makeChart("chart_1", {
	            "type": "serial",
	            "theme": "custom2",
	            "autoMargins": false,
	            "marginLeft": 50,
	            "marginRight": 50,
	            "marginTop": 30,
	            "marginBottom": 50,
	            "fontFamily": 'Open Sans',            
	            "color":    '#888',
	            "dataProvider": graph_data,
	            "valueAxes": [{
	            	"id": "usrAxis",
	                "axisAlpha": 0,
	                "position": "left"
	            },
	            {
	            	"id": "countAxis",
	                "axisAlpha": 0,
	                "position": "right",
	                "gridAlpha": 0
	            }],
	            "startDuration": 0,
	            "graphs": [{
	                "alphaField": "alpha",
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthColumn",
	                "fillAlphas": 1,
	                "title": "신규가입",
	                "type": "column",
	                "valueField": "newreg",
	                "valueAxis": "usrAxis"
	            }, {
	                "alphaField": "alpha",
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthColumn",
	                "fillAlphas": 1,
	                "title": "방문자수",
	                "type": "column",
	                "valueField": "usr",
	                "valueAxis": "usrAxis"
	            }, {
	                "balloonText": "<span style='font-size:13px;'>[[title]]:<b>[[value]]</b> [[additional]]</span>",
	                "dashLengthField": "dashLengthLine",
	                "lineThickness": 3,
	                "fillAlphas": 0,
	                "lineAlpha": 1,
	                "title": "방문횟수",
	                "valueField": "count",
	                "valueAxis": "countAxis"
	            }],
	            "categoryField": "time",
	            "categoryAxis": {
	                "gridPosition": "start",
	                "axisAlpha": 0,
	                "tickLength": 0,
	                "gridAlpha": 0
	            }
	        });
	        $("#chart_1").find('a').text("");
		});
	}
</script>