$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 0;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
            $("#page-main").css("min-height", (height-110) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});

function MyAlert(obj, fa, btn_str) {
	var b = bootbox.dialog({
        message: function(){
        	return obj;
        },
        buttons: {
          ok: {
            label: btn_str
          }
        }
    });
    var $icon_div = '<div style="text-align: center; margin-bottom: -31px; z-index: 100000; position: relative;"><span style="font-size: 45px; border-radius: 50%; border: 4px solid white; color: white; padding: 8px 15px; background: rgb(0, 167, 213) none repeat scroll 0% 0%;"><i class="fa ' + fa + '"></i></span></div>';
    b.find('.modal-dialog').prepend($icon_div).css('max-width','400px');
    b.find('.bootbox-close-button').remove();
    b.find('.modal-content').css('padding-top', '30px');
    b.find('.modal-footer').css('padding', '10px').css('text-align','center');
    b.find('.bootbox-body').css('text-align','center').css('font-size','17px').css('padding','12px').css('color','#333');
    b.find('.modal-footer > button').css('background-color','white').css('color','rgb(0, 167, 213)').css('border','none').css('font-size','17px').addClass('btn-block');          
}
