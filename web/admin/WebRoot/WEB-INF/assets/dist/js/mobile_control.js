/**
 * Created by Star_Man on 11/3/2017.
 */



var android_marketUrl = "market://details?id=com.kyad.youyishou.store";
var android_appUrl = "youyishou://youyishou";
var android_intent = "intent://youyishou/#Intent;scheme=youyishou;package=com.kyad.youyishou.store;end";

function isIOS(){
    return false
    // var userAgent = navigator.userAgent.toLowerCase();
    // return ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1) || (userAgent.search("ipad") > -1));
}

function go_iOS_AppStore(){
    //TODO: AppStore URL
    app_store_url = "https://itunes.apple.com/kr/app/id1247128880";
    window.location = app_store_url;
    //openInNewTab(app_store_url);
}

function MobileSDK_showToast(str) {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://showToast?str=" + encodeURIComponent(str);
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        alert(str);
        return;
    }
    youyishou.showToast(str);
}

function MobileSDK_closeWebview() {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://closeWebview";
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        return;
    }
    youyishou.closeWebview();
}

function MobileSDK_downloadImage(url) {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://downloadImage?url=" + encodeURIComponent(url);
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        return;
    }
    youyishou.downloadImage(url);
}

function MobileSDK_uploadImage(index) {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://uploadImage?index=" + index;
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        return;
    }
    youyishou.uploadImage(index);
}

function MobileSDK_init() {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://init";
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        if(is_android_web()){
            var url = "Intent://youyishou";
            url += "#Intent;scheme=youyishou;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.kyad.offerwall.appall;end";
            // openInNewTab(url);
            // android_appUrl = url;
            android_intent = url;
            launch_app_or_alt_url();
        } else {
            alert("No Mobile Browser!!!");
        }
    }
}

function MobileSDK_inquiry(strMail, strOfferUsrUid) {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://inquiry?email=" + encodeURIComponent(strMail) + "&offer_usr_uid=" + encodeURIComponent(strOfferUsrUid);
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        if(is_android_web()) {
            // var str = "사용자UID: " + strOfferUsrUid + "\n 아래에 문의사항을 적어주세요! \n\n\n\   ";
            // openInNewTab('mailto:' + strMail + "?body=" + encodeURIComponent(str));
            var url = "Intent://youyishou?email=" + encodeURIComponent(strMail) ;
            url += "&offer_usr_uid=" + strOfferUsrUid;
            url += "#Intent;scheme=youyishou;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.kyad.offerwall.appall;end";
            // openInNewTab(url);
            // android_appUrl = url;
            android_intent = url;
            launch_app_or_alt_url();
        } else {
            alert("No Android Browser!!!");
        }
        return;
    }
    youyishou.inquiry(strMail, strOfferUsrUid);
}

function MobileSDK_joinAd(pay_tp, ad_key, pkg_nm, offer_usr_uid, ad_foreign) {
    if (isIOS()){
        setTimeout(function () {
            window.location = "youyishou://joinAd?pay_tp=" + encodeURIComponent(pay_tp) +
                "&ad_key=" + encodeURIComponent(ad_key) + "&pkg_nm=" + encodeURIComponent(pkg_nm) +
                "&offer_usr_uid=" + offer_usr_uid + "&ad_foreign=" + ad_foreign;
        }, 50);
        // setTimeout( function() {
        //     go_iOS_AppStore();
        // }, 1500);
        return;
    }
    if (typeof youyishou == 'undefined') {
        if(is_android_web()){
            var url = "Intent://youyishou?ad_key=" + ad_key ;
            url += "&pay_tp=" + pay_tp + "&pkg_nm=" + pkg_nm + "&offer_usr_uid=" + offer_usr_uid + "&ad_foreign" + ad_foreign;
            url += "#Intent;scheme=youyishou;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.kyad.offerwall.appall;end";
            // openInNewTab(url);
            // android_appUrl = url;
            android_intent = url;
            launch_app_or_alt_url();
        } else {
            alert("No Android Browser!!!");
        }
        return;
    }
    youyishou.joinAd(pay_tp, ad_key, pkg_nm, offer_usr_uid, ad_foreign);
}

function openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}

function is_android_web(){
    return (typeof youyishou == 'undefined');
}

var timer;
var heartbeat;
var iframe_timer;

function clearTimers() {
    clearTimeout(timer);
    clearTimeout(heartbeat);
    clearTimeout(iframe_timer);
}

function intervalHeartbeat() {
    if (document.webkitHidden || document.hidden) {
        clearTimers();
    }
}

function tryIframeApproach() {
    var iframe = document.createElement("iframe");
    iframe.style.border = "none";
    iframe.style.width = "1px";
    iframe.style.height = "1px";
    iframe.onload = function () {
        document.location = android_marketUrl;
    };
    iframe.src = android_appUrl;
    document.body.appendChild(iframe);
}

function tryWebkitApproach() {
    document.location = android_appUrl;
    timer = setTimeout(function () {
        document.location = android_marketUrl;
    }, 2500);
}

function useIntent() {
    document.location = android_intent;
}

function launch_app_or_alt_url(el) {
    heartbeat = setInterval(intervalHeartbeat, 200);
    if (navigator.userAgent.match(/Chrome/)) {
        useIntent();
    } else if (navigator.userAgent.match(/Firefox/)) {
        tryWebkitApproach();
        iframe_timer = setTimeout(function () {
            tryIframeApproach();
        }, 1500);
    } else {
        tryIframeApproach();
    }
}
