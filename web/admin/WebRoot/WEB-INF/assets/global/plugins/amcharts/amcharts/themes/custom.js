AmCharts.themes.custom = {

	themeName:"custom",

	AmChart: {
		color: "#000000", backgroundColor: "#FFFFFF"
	},
	
	AmCoordinateChart: {
		colors: ["#5d9bfc", "#fc9d5d", "#8d5dfc", "#cc4748", "#cd82ad", "#2f4074", "#448e4d", "#b7b83f", "#b9783f", "#b93e3d", "#913167"]
	}
};

AmCharts.themes.custom2 = {

		themeName:"custom2",

		AmChart: {
			color: "#000000", backgroundColor: "#FFFFFF"
		},
		
		AmCoordinateChart: {
			colors: ["#8d5dfc", "#5d9bfc", "#fc9d5d", "#cc4748", "#cd82ad", "#2f4074", "#448e4d", "#b7b83f", "#b9783f", "#b93e3d", "#913167"]
		}
	};