/*
* Author: Star_Man
* Date: 2018-03-20
* */

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}

function isIOS() {
    var userAgent = navigator.userAgent.toLowerCase();
    return ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1) || (userAgent.search("ipad") > -1));
}

function isSafari() {
    var userAgent = navigator.userAgent.toLowerCase();
    return (userAgent.search("safari") > -1);
}

function isIOSWebview() {
    var standalone = window.navigator.standalone,
        userAgent = window.navigator.userAgent.toLowerCase(),
        safari = /safari/.test(userAgent),
        ios = /iphone|ipod|ipad/.test(userAgent);

    if (ios) {
        if (!standalone && safari) {
            return false;//browser
        } else if (standalone && !safari) {
            return false;//standalone
        } else if (!standalone && !safari) {
            return true;//uiwebview
        }
    } else {
        return false;//not iOS
    }
}

function isAndroidWebview() {
    return typeof selluv !== 'undefined';
}

function iosWebviewCall(url) {
    window.top.location.href = "selluv://" + url;
}

function SELLUVJSSDK_closeWebview() {
    if (isAndroidWebview())
        selluv.closeWebview();
    else if (isIOSWebview())
        iosWebviewCall("closeWebview");
    else
        if (window.opener) {
            window.close();
        }
}

function SELLUVJSSDK_showToast(msg) {
    if (isAndroidWebview())
        selluv.showToast(JSON.stringify({msg: msg}));
    else if (isIOSWebview())
        iosWebviewCall("showToast?msg=" + encodeURIComponent(msg));
    else
        alert(msg);
}

function SELLUVJSSDK_paymentSuccess(dealUid) {
    if (isAndroidWebview())
        selluv.paymentSuccess(JSON.stringify({dealUid: dealUid}));
    else if (isIOSWebview())
        iosWebviewCall("paymentSuccess?dealUid=" + encodeURIComponent(dealUid));
    else
        alert('결제성공: ' + dealUid);
}

function SELLUVJSSDK_paymentFail(dealUid) {
    if (isAndroidWebview())
        selluv.paymentFail(JSON.stringify({dealUid: dealUid}));
    else if (isIOSWebview())
        iosWebviewCall("paymentFail?dealUid=" + encodeURIComponent(dealUid));
    else
        alert('결제실패: ' + dealUid);
}

function SELLUVJSSDK_danalSuccess(name, phone, birth, gender) {
    if (isAndroidWebview())
        selluv.danalSuccess(JSON.stringify({name: name, phone: phone, birth: birth, gender: gender}));
    else if (isIOSWebview())
        iosWebviewCall("danalSuccess?name=" + encodeURIComponent(name) + "&phone=" + encodeURIComponent(phone) +
            "&birth=" + encodeURIComponent(birth) + "&gender=" + encodeURIComponent(gender));
    else
        alert(name + ": " + phone + ": " + birth + ": " + gender);
}

function SELLUVJSSDK_danalFail() {
    if (isAndroidWebview())
        selluv.danalFail();
    else if (isIOSWebview())
        iosWebviewCall("danalFail");
    else
        alert('인증실패');
}

function SELLUVJSSDK_findAddress(address, postcode) {
    if (isAndroidWebview())
        selluv.findAddress(JSON.stringify({address: address, postcode: postcode}));
    else if (isIOSWebview())
        iosWebviewCall("findAddress?address=" + encodeURIComponent(address) + "&postcode=" + encodeURIComponent(postcode));
    else {
        if (window.opener) {
            try {
                window.opener.findAddress(address, postcode);
            } catch (e) {
                window.opener.postMessage(JSON.stringify({address: address, postcode: postcode}), '*');
            }
            window.close();
        } else {
            alert(address + "\n우편번호: " + postcode);
        }
    }
}
var android_marketUrl = "market://details?id=com.kyad.selluv";
var android_appUrl = "";
var android_intent = "";

function SELLUVJSSDK_openPdtDetail(pdtUid) {
    var mobile_type = getMobileOperatingSystem();
    if (mobile_type === "iOS"){
        setTimeout(function () {
            window.top.location.href = "selluv://pdtDetail?pdtUid=" + pdtUid;
        }, 50);
        setTimeout( function() {
            window.top.location.href = "https://itunes.apple.com/kr/app/id1367825994?mt=8";
        }, 1000);
    } else if (mobile_type === "Android"){
        android_appUrl = "selluv://pdtDetail?pdtUid=" + pdtUid;
        android_intent = "Intent://selluv/pdtDetail?pdtUid=" + pdtUid +  "#Intent;scheme=selluv;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.kyad.selluv;end";
        launch_app_or_alt_url();
    } else {
        alert('모바일 웹에서만 이용가능합니다.');
    }
}

var timer;
var heartbeat;
var iframe_timer;

function clearTimers() {
    clearTimeout(timer);
    clearTimeout(heartbeat);
    clearTimeout(iframe_timer);
}

function intervalHeartbeat() {
    if (document.webkitHidden || document.hidden) {
        clearTimers();
    }
}

function tryIframeApproach() {
    var iframe = document.createElement("iframe");
    iframe.style.border = "none";
    iframe.style.width = "1px";
    iframe.style.height = "1px";
    iframe.onload = function () {
        document.location = android_marketUrl;
    };
    iframe.src = android_appUrl;
    document.body.appendChild(iframe);
}

function tryWebkitApproach() {
    document.location = android_appUrl;
    timer = setTimeout(function () {
        document.location = android_marketUrl;
    }, 2500);
}

function launch_app_or_alt_url() {
    heartbeat = setInterval(intervalHeartbeat, 200);
    if (navigator.userAgent.match(/Chrome/)) {
        document.location = android_intent;
    } else if (navigator.userAgent.match(/Firefox/)) {
        tryWebkitApproach();
        iframe_timer = setTimeout(function () {
            tryIframeApproach();
        }, 1500);
    } else {
        tryIframeApproach();
    }
}