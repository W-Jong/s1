<%--
 Created by Star_Man
 2018-04-02 19:55:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>주소검색</title>
    <script type="text/javascript" src="<c:url value="/assets/app/app.js" />"></script>
    <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
    <style>
        * {
            margin:0;
            padding:0;
            -webkit-overflow-scrolling: touch;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }
        html, body{
            width: 100%;
            height: 100%;
            margin:0;
            padding:0;
            overflow: hidden;
        }
        iframe {
            position: absolute;
        }
        .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }
        .col-md-3 {
            width: 25%;
        }
        .col-md-4 {
            width: 33.33333333%;
        }
        .col-md-6 {
            width: 50%;
        }
        .col-md-12 {
            width: 100%;
        }
        .row {
            overflow: hidden;
        }
        .form-control {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555555;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            margin-top: 5px;
        }

        .form-control:focus {
            border-color: #33bbff;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(0, 136, 204, 0.3);
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(0, 136, 204, 0.3);
        }

        button, input, optgroup, select, textarea {
            color: inherit;
            font: inherit;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
            margin: 0;
        }

        input {
            color: black;
            font-weight: bold;
            outline: none;
            line-height: normal;
        }

        input, input:before, input:after {
            -webkit-touch-callout: initial;
            -webkit-user-select: initial;
            -khtml-user-select: initial;
            -moz-user-select: initial;
            -ms-user-select: initial;
            user-select: initial;
        }

        button {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #0088cc;
            white-space: normal;
            line-height: 1.334;
            padding: 5px 16px;
            border-radius: 6px;
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
        }
    </style>
</head>
<body>
<div id="input_area" style="width: 100%; height: 200px; margin:0;padding:0;position: fixed;overflow: hidden">
    <div class="row" style="line-height: 40px; margin: 10px 15px;">
        <button type="button" class="col-md-3" onclick="confirmAddress()">확인</button>
        <div class="col-md-6" style="text-align: center;font-size: larger"><strong>주소검색</strong></div>
        <button type="button" style="color: #333333;background-color: #e6e6e6;border-color: #adadad; " class="col-md-3" onclick="cancelAddress()">취소</button>
    </div>
    <hr>
    <div class="col-md-12">
        <input type="text" class="form-control" id="postcode" readonly placeholder="우편주소(직접입력 불가)"/>
        <input type="text" class="form-control" id="address_main" readonly placeholder="주소명(직접입력 불가)"/>
        <input type="text" class="form-control" id="address_detail" placeholder="상세주소(직접입력)"/>
        <span style="color: red; font-size: small"> ※ 상세주소는 직접 입력해주세요</span>
    </div>
</div>
<div style="width:100%;height:calc(100% - 210px);top: 200px;position:absolute;overflow-y: scroll;margin:0;padding:0;-webkit-overflow-scrolling: touch;">
    <div id="wrap"
         style="display:none;border:0;width: 100%;height: 100%; margin:0;padding:0;position:relative;"></div>
</div>
<script>
    var element_wrap = document.getElementById('wrap');
    var themeObj = {
        //bgColor: "", //바탕 배경색
        searchBgColor: "#1D86F9", //검색창 배경색
        //contentBgColor: "", //본문 배경색(검색결과,결과없음,첫화면,검색서제스트)
        //pageBgColor: "", //페이지 배경색
        //textColor: "", //기본 글자색
        queryTextColor: "#FFFFFF" //검색창 글자색
        //postcodeTextColor: "", //우편번호 글자색
        //emphTextColor: "", //강조 글자색
        //outlineColor: "", //테두리
    };

    execDaumPostcode();

    function foldDaumPostcode() {
        // element_wrap.style.display = 'none';
    }

    function execDaumPostcode() {
        var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
        new daum.Postcode({
            // theme: themeObj,
            hideMapBtn: true,
            hideEngBtn: true,
            oncomplete: function (data) {
                var fullAddr = data.address;
                var extraAddr = '';

                if (data.addressType === 'R') {
                    if (data.bname !== '') {
                        extraAddr += data.bname;
                    }
                    if (data.buildingName !== '') {
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    fullAddr += (extraAddr !== '' ? ' (' + extraAddr + ')' : '');
                }

                document.getElementById('postcode').value = data.zonecode;
                document.getElementById('address_main').value = fullAddr;
                // SELLUVJSSDK_findAddress(fullAddr, data.zonecode);

                element_wrap.style.display = 'block';

                document.body.scrollTop = currentScroll;
            },
            onresize: function (size) {
                element_wrap.style.height = size.height + 'px';
            },
            width: '100%',
            height: '100%'
        }).embed(element_wrap, {autoClose: false});
        element_wrap.style.display = 'block';
    }

    function confirmAddress() {
        var postcode = document.getElementById('postcode').value;
        var address_main = document.getElementById('address_main').value;
        var address_detail = document.getElementById('address_detail').value;
        if (postcode.trim().length === 0 || address_main.trim().length === 0) {
            SELLUVJSSDK_showToast("주소검색창에서 주소를 선택해주세요.");
            return;
        }
        if (address_detail.trim().length === 0) {
            SELLUVJSSDK_showToast("상세주소를 입력해주세요.");
            document.getElementById('address_detail').focus();
            return;
        }

        SELLUVJSSDK_findAddress(address_main + "\n" + address_detail, postcode);
    }

    function cancelAddress() {
        SELLUVJSSDK_closeWebview();
    }
</script>
</body>
</html>