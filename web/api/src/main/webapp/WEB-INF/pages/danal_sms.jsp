<%--
 Created by Star_Man
 2018-01-23 11:32:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>본인인증</title>
    <script type="text/javascript" src="<c:url value="/assets/app/app.js" />"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js" ></script>
    <script type="text/javascript" src="https://service.iamport.kr/js/iamport.payment-1.1.5.js" ></script>
</head>
<body>
<div style="width: 100%; height: 500px; text-align: center">
    <h2 style="margin-top:200px; text-align: center" id="description">본인인증 요청중...</h2>
</div>
<script>
    $(document).ready(function () {
        var IMP = window.IMP;
        IMP.init('imp13783002');
        IMP.certification({
            merchant_uid : 'merchant_' + new Date().getTime(),
            min_age: 18
        }, function(rsp) {
            if ( rsp.success ) {
                $("#description").text('인증성공');

                $.ajax({
                    type : 'POST',
                    url : '${selluvhost}payments/danal',
                    dataType : 'json',
                    data : {
                        imp_uid : rsp.imp_uid
                    }
                }).done(function(rsp) {
                    try {
                        if (rsp.meta.errCode === 0) {
                            var name = rsp.data.name;
                            var phone = rsp.data.phone;
                            var gender = (rsp.data.gender === "male") ? "1" : "2";
                            var birth = new Date(rsp.data.birth).toISOString().substring(0, 10);
                            SELLUVJSSDK_danalSuccess(name, phone, birth, gender);
                        } else {
                            $("#description").text('인증정보얻기 실패');
                            SELLUVJSSDK_danalFail();
                        }
                    } catch (e) {
                        $("#description").text('오류발생');
                        SELLUVJSSDK_danalFail();
                    }
                });

            } else {
                // 인증취소 또는 인증실패
                $("#description").text('인증실패');
                var msg = '인증에 실패하였습니다.';
                msg += '\n(' + rsp.error_msg + ')';

                SELLUVJSSDK_showToast(msg);
                SELLUVJSSDK_danalFail();
            }
        });
    });
</script>
</body>
</html>
