<%--
 Created by Star_Man
 2018-04-09 19:55:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <c:if test="${isValid}">
        <meta property="og:url" content="${selluvhost}web/share/${pdt.pdtUid}"/>
        <meta property="og:type" content="product"/>
        <meta property="og:title" content="${pdtTitle}"/>
        <meta property="og:description" content="${pdt.content}"/>
        <meta property="og:image" content="${pdt.profileImg}"/>
        <title>${pdtTitle}</title>
    </c:if>
    <title>상품없음</title>
    <style>
        button {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            white-space: normal;
            line-height: 1.6;
            padding: 10px 20px;
            border-radius: 6px;
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            font-size: 18px;
            text-align: center;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background: #0088cc none;
            border: 1px solid transparent;
        }
    </style>
    <script type="text/javascript" src="<c:url value="/assets/app/app.js" />"></script>
</head>
<body>
<div style="width: 100%; height: 500px; text-align: center">
    <button type="button" style="margin-top:200px; text-align: center" id="description" onclick="goToDetail()">앱에서 열기...</button>
</div>
<script>
    function goToDetail() {
        <c:if test="${isValid}">
        if (getMobileOperatingSystem() !== "unknown") {
            SELLUVJSSDK_openPdtDetail(${pdt.pdtUid});
        } else {
            alert('모바일웹에서만 이용가능합니다.');
            self.close();
            history.back();
        }
        </c:if>
        <c:if test="${!isValid}">
        alert('삭제되었거나 존재하지 않는 상품입니다.');
        self.close();
        history.back();
        </c:if>
    }

    document.getElementById("description").click();
</script>
</body>
</html>