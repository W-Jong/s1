<%--
 Created by Star_Man
 2018-01-23 11:32:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>결제진행</title>
    <script type="text/javascript" src="<c:url value="/assets/app/app.js" />"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://service.iamport.kr/js/iamport.payment-1.1.5.js"></script>
</head>
<body>
<c:if test="${isValid == false}">
    <div style="width: 100%; height: 500px; text-align: center">

        <h2 style="margin-top:100px; text-align: center">오류발생 알림화면</h2>
        <div style="width: 300px; margin: 0 auto">
            <h4>(허용되지 않는 요청을 하셨거나 결제요청에 실패했습니다)</h4>
            <h4>Error message (Access denied)</h4>
        </div>
    </div>
    <script type="text/javascript">
        SELLUVJSSDK_showToast('허용되지 않는 요청을 하셨거나 결제요청에 실패했습니다');
        SELLUVJSSDK_closeWebview();
    </script>
</c:if>
<c:if test="${isValid == true}">
    <div style="width: 100%; height: 500px; text-align: center">
        <h2 style="margin-top:200px; text-align: center" id="description">결제요청중...</h2>
    </div>
    <script type="text/javascript">
        var IMP = window.IMP;
        IMP.init('imp13783002');
        IMP.request_pay({
            pg: 'nice',
            pay_method: '${(deal.payImportMethod == 2) ? "card" : "trans"}',
            // escrow: true,
            merchant_uid: '${deal.dealUid}',
            name: '${deal.pdtTitle}',
            amount: ${deal.price},
            buyer_email: '${usr.usrMail}',
            buyer_name: '${usr.usrNm}',
            buyer_tel: '${usr.usrPhone}',
            buyer_addr: '${deal.recipientAddress.replaceAll("\\r", "").replaceAll("\\n", " ")}',
            niceMobileV2 : true,
            // buyer_postcode: '',
            m_redirect_url: '${selluvhost}web/payment/complete',
            app_scheme: 'selluv'
        }, function (rsp) {
            var msg = "";
            if (rsp.success) {
                $("#description").text('결제성공');
                msg = '결제가 완료되었습니다.';
                msg += '고유ID : ' + rsp.imp_uid;
                msg += '상점 거래ID : ' + rsp.merchant_uid;
                msg += '결제 금액 : ' + rsp.paid_amount;
                msg += '카드 승인번호 : ' + rsp.apply_num;
                SELLUVJSSDK_paymentSuccess('${deal.dealUid}');
            } else {
                $("#description").text('결제실패');
                msg = '결제에 실패하였습니다.';
                msg += '\n(' + rsp.error_msg + ')';
                SELLUVJSSDK_paymentFail('${deal.dealUid}');
            }

            SELLUVJSSDK_showToast(msg);
        });
    </script>
</c:if>
</body>
</html>
