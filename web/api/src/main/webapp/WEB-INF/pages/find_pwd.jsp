<%--
 Created by Star_Man
 2018-01-23 11:32:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>비밀번호변경</title>
    <style>
        body {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        .btn {
            white-space: normal;
            line-height: 1.334;
            font-size: 18px;
            border-radius: 6px;
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            padding: 6px 12px;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            outline: none;
            text-shadow: none;
            border: 1px solid transparent;
        }

        .btn_default {
            background-color: #01bad8;
            border-color: #01bad8;
            color: #ffffff;
        }

        .btn_default:active {
            background-color: #018297;
            border-color: #018297;
            color: #b2b2b2;
        }

        .screen_center {
            position: absolute;
            left: 0;
            width: 100%;
            height: 80%;
            text-align: center
        }

        .screen_center .content {
            display: inline-block;
            vertical-align: middle
        }

        .screen_center .blank {
            display: inline-block;
            width: 0;
            height: 100%;
            vertical-align: middle
        }

        .form-group {
            box-sizing: border-box;
            padding-bottom: 15px;
            margin-bottom: 15px;
            overflow: hidden;
        }

        .form-group label {
            float: left;
            text-align: right;
            margin-bottom: 0;
            padding-top: 7px;
            width: calc(40% - 10px);
            padding-right: 10px;
        }

        .form-group input {
            float: left;
            display: block;
            width: 59%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555555;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            outline: none;
            margin: 0;
        }

        .form-group input:focus {
            border-color: #33bbff;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(0, 136, 204, 0.3);
        }
    </style>
    <script type="text/javascript" src="<c:url value="/assets/jquery/jquery.min.js"/>"></script>
</head>
<body>
<div class="screen_center">
    <div class="content">
        <c:if test="${valid == true}">
            <div>
                <h2>셀럽 비밀번호 변경페이지</h2>
                <form id="password_reset_form">
                    <div class="form-group">
                        <label for="selluv_password">새&nbsp;&nbsp;비밀번호</label>
                        <input id="selluv_password" type="password" maxlength="40" autofocus required
                               placeholder="비밀번호 입력"/>
                    </div>
                    <div class="form-group">
                        <label for="selluv_password_reply">비밀번호확인</label>
                        <input id="selluv_password_reply" type="password" maxlength="40" required
                               placeholder="비밀번호 확인"/>
                    </div>
                    <div class="form-group">
                        <button type="button" id="password_reset" class="btn btn_default">비밀번호 변경</button>
                    </div>
                </form>
            </div>
            <script>
                $(document).ready(function () {
                    $(document).on('click', '#password_reset', function () {
                        var selluv_password = $('#selluv_password');
                        if (selluv_password.val() === '') {
                            alert('비밀번호를 입력해주세요.');
                            selluv_password.focus();
                            return;
                        }
                        if (selluv_password.val() !== $('#selluv_password_reply').val()) {
                            $('#selluv_password_reply').focus();
                            alert('두 비밀번호가 일치하지 않습니다.');
                            return;
                        }
                        $.ajax({
                            url: '/api/user/password',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
                                'accessToken': '${accessToken}',
                                'Content-Type': 'application/json'
                            },
                            processData: false,
                            data: JSON.stringify({
                                'oldPassword': '${tempPwd}',
                                'newPassword': selluv_password.val()
                            }),
                            success: function (response) {
                                try {
                                    if (response.meta.errCode === 0) {
                                        alert('비밀번호 변경에 성공했습니다.');
                                    } else {
                                        if (typeof response.meta.errCode !== 'undefined')
                                            alert('비밀번호 변경에 실패했습니다. \n\n 오류코드: ' + response.meta.errCode);
                                        else
                                            alert('비밀번호 변경에 실패했습니다.');
                                    }
                                } catch (e) {
                                    alert('비밀번호 변경에 실패했습니다.');
                                }
                                document.location.reload();
                            },
                            error: function (xhr, status, error) {
                                alert('비밀번호 변경중 오류가 발생했습니다. \n\n' + error);
                            }
                        });
                    });
                });
            </script>
        </c:if>
        <c:if test="${valid == false}">
            <h3>
                인증시간이 초과되었습니다.<br><br>
                비밀번호변경을 원하시는 경우<br>
                앱에서 비밀번호찾기를 다시 이용해주세요.
            </h3>
        </c:if>
    </div>
    <div class="blank"></div>
</div>
</body>
</html>
