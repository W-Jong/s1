<%--
 Created by Star_Man
 2018-01-23 11:32:21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>결제완료</title>
    <script type="text/javascript" src="<c:url value="/assets/app/app.js" />"></script>
</head>
<body>
<c:if test="${isValid == false}">
    <div style="width: 100%; height: 500px; text-align: center">
        <h2 style="margin-top:100px; text-align: center">결제취소</h2>
        <div style="width: 300px; margin: 0 auto">
            <h4>(결제중 직접 취소하셨거나 오류가 발생하여 결제가 취소되었습니다.)</h4>
            <h4>Payment is cancelled.</h4>
        </div>
    </div>
    <script>
        SELLUVJSSDK_paymentFail('${dealUid}');
    </script>
</c:if>
<c:if test="${isValid == true}">
    <div style="width: 100%; height: 500px; text-align: center">
        <h2 style="margin-top:200px; text-align: center" id="description">결제성공</h2>
        <div style="width: 200px; margin: 0 auto">
            <h4>결제에 성공했습니다. 잠시만 기다려주세요...</h4>
        </div>
    </div>
    <script>
        SELLUVJSSDK_paymentSuccess('${dealUid}');
    </script>
</c:if>
</body>
</html>