package com.kyad.selluvapi.dto.search;

import com.kyad.selluvapi.dao.BrandDao;
import com.kyad.selluvapi.dao.CategoryDao;
import com.kyad.selluvapi.dao.TagWordDao;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class RecommendSearchDto {
    @ApiModelProperty("추천브랜드")
    private List<BrandDao> brandList;
    @ApiModelProperty("추천카테고리")
    private List<List<CategoryDao>> categoryDaoList;
    @ApiModelProperty("추천모델명")
    private List<SearchModelDto> modelList;
    @ApiModelProperty("추천태그명")
    private List<String> tagWordList;
}
