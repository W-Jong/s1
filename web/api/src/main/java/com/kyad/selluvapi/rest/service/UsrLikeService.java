package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrLikeDao;
import com.kyad.selluvapi.jpa.UsrLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UsrLikeService {

    @Autowired
    private UsrLikeRepository usrLikeRepository;

    public Page<UsrLikeDao> findAll(Pageable pageable) {
        return usrLikeRepository.findAll(pageable);
    }

    public UsrLikeDao getByUsrUidAndPeerUsrUid(int usrUid, int peerUsrUid) {
        return usrLikeRepository.getByUsrUidAndPeerUsrUid(usrUid, peerUsrUid);
    }

    public UsrLikeDao save(UsrLikeDao dao) {
        return usrLikeRepository.save(dao);
    }

    public void delete(int usrLikeUid) {
        usrLikeRepository.delete(usrLikeUid);
    }

    public long countFollowerUserByUsrUid(int usrUid) {
        return usrLikeRepository.countByPeerUsrUid(usrUid);
    }

    public long countFollowingUserByUsrUid(int usrUid) {
        return usrLikeRepository.countByUsrUid(usrUid);
    }
}
