package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;

@Data
public class ValetCreateReq {

    @ApiModelProperty("브랜드UID")
    @Min(1)
    private int brandUid;

    @ApiModelProperty("카테고리UID")
    @Min(5)
    private int categoryUid;

    @Min(0)
    @ApiModelProperty("판매가, 추천가격의 경우 0, 직접설정의 경우 0이상값")
    private long reqPrice = 0;

    @NotEmpty
    @ApiModelProperty("성함")
    private String reqName;

    @NotEmpty
    @ApiModelProperty("연락처")
    private String reqPhone;

    @NotEmpty
    @ApiModelProperty("주소")
    private String reqAddress;

    @Range(min = 1, max = 2)
    @ApiModelProperty("발송방법, 1-배송키트 이용, 2-직접발송")
    private int sendType = 1;

    @NotEmpty
    @ApiModelProperty("사인이미지")
    private String signImg;
}
