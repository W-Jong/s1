package com.kyad.selluvapi.helper;

import javax.servlet.http.HttpServletRequest;

public class CommonHelper {

    public static String getRemoteAddr(HttpServletRequest request) {
        if (request == null)
            return "";
        else {
            String clientIp = request.getHeader("X-Forwarded-For");
            if (clientIp == null)
                return "";
            else
                return clientIp;
        }
    }

    public static int getAgeGroupFromAge(int age) {
        if (age < 20)
            return 1;
        else if (age < 30)
            return 2;
        else if (age < 40)
            return 3;
        else if (age < 50)
            return 4;
        else
            return 5;
    }
}
