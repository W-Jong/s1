package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.deal.DealDetailDto;
import com.kyad.selluvapi.dto.deal.DealPaymentDto;
import com.kyad.selluvapi.enumtype.*;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.StringHelper;
import com.kyad.selluvapi.helper.cvsnet.CvsnetClientHelper;
import com.kyad.selluvapi.helper.cvsnet.request.ReservationReq;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.IamportDeliveryCompanyHelper;
import com.kyad.selluvapi.helper.iamport.request.*;
import com.kyad.selluvapi.helper.iamport.response.EscrowLogisAnnotation;
import com.kyad.selluvapi.helper.iamport.response.PaymentAnnotation;
import com.kyad.selluvapi.helper.sweettracker.SweetTrackerClientHelper;
import com.kyad.selluvapi.helper.sweettracker.SweetTrackerDeliveryCompanyHelper;
import com.kyad.selluvapi.helper.sweettracker.request.InvoiceReq;
import com.kyad.selluvapi.helper.sweettracker.response.SweetTrackerResponse;
import com.kyad.selluvapi.helper.sweettracker.response.TrackingInfo;
import com.kyad.selluvapi.request.*;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@Api(value = "/deal", description = "거래관리")
@RequestMapping("/deal")
public class DealController {

    private final Logger LOG = LoggerFactory.getLogger(DealController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private DealService dealService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private OtherService otherService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private InviteRewardHisService inviteRewardHisService;

    @Autowired
    private MoneyChangeHisService moneyChangeHisService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @Autowired
    private PaymentResultService paymentResultService;

    @ApiOperation(value = "네고신청",
            notes = "상품에 대하여 네고를 신청한다.")
    @PutMapping(value = "/nego")
    public GenericResponse<DealDao> putNego(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid DealNegoReq negoReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(negoReq.getPdtUid());

        if (pdt.getStatus() != 1 || pdt.getUsrUid() == usr.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (pdt.getPrice() / 2 > negoReq.getReqPrice() || pdt.getPrice() < negoReq.getReqPrice()) {
            return new GenericResponse<>(ResponseMeta.PAY_PRICE_RANGE_ERROR, null);
        }

        UsrCardDao usrCard = usrService.findOneUsrCard(negoReq.getUsrCardUid());
        if (usrCard == null || usr.getStatus() == 0 || usrCard.getUsrUid() != usr.getUsrUid())
            return new GenericResponse<>(ResponseMeta.CARD_NOT_EXIST, null);

        UsrDao sellerUsr = usrService.getByUsrUid(pdt.getUsrUid());

        if (sellerUsr == null || sellerUsr.getStatus() != 1 || sellerUsr.getUsrUid() < 1) {
            return new GenericResponse<>(ResponseMeta.SELLER_USER_NOT_EXIST, null);
        }

        BrandDao brand = brandService.getByBrandUid(pdt.getBrandUid());
        CategoryDao category = categoryService.findOne(pdt.getCategoryUid());

        DealDao deal = new DealDao();
        deal.setUsrUid(usr.getUsrUid());
        deal.setPdtUid(pdt.getPdtUid());
        deal.setSellerUsrUid(pdt.getUsrUid());
        deal.setValetUid(pdt.getValetUid());
        deal.setRecipientNm(negoReq.getRecipientNm());
        deal.setRecipientPhone(negoReq.getRecipientPhone());
        deal.setRecipientAddress(negoReq.getRecipientAddress());
        deal.setBrandEn((brand == null) ? "" : brand.getNameEn());
        deal.setPdtTitle(StringHelper.join(Arrays.asList(((brand == null) ? "" : brand.getNameKo()), pdt.getColorName(),
                pdt.getPdtModel(), ((category == null) ? "" : category.getCategoryName())), " "));
        deal.setPdtSize(pdt.getPdtSize());
        deal.setPdtImg(pdt.getProfileImg());
        deal.setPdtPrice(pdt.getPrice());
        deal.setSendPrice(((sellerUsr.getFreeSendPrice() > 0 && sellerUsr.getFreeSendPrice() <= pdt.getPrice()) ? 0 : pdt.getSendPrice()) + negoReq.getIslandSendPrice());
        deal.setPrice(deal.getPdtPrice() + deal.getSendPrice());
        deal.setReqPrice(negoReq.getReqPrice());
        deal.setPayImportMethod(PAY_TYPE.SIMPLECARD.getCode());//간편결제만 사용가능...
        deal.setUsrCardUid(negoReq.getUsrCardUid());
        deal.setNegoTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.NEGO_REQUEST.getCode());

        dealService.save(deal);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH07_S_NEGO_SUGGEST, sellerUsr, usr, deal, pdt);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "상품구매",
            notes = "상품을 구매한다. 거래상태는 배송준비상태로 변경된다.")
    @PutMapping(value = "/buy")
    public GenericResponse<DealPaymentDto> putBuy(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid DealBuyReq buyReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(buyReq.getPdtUid());

        if (pdt.getStatus() != PDT_STATUS_TYPE.NORMAL.getCode()) {
            return new GenericResponse<>(ResponseMeta.PAY_PDT_NOT_NORMAL, null);
        }

        if (pdt.getUsrUid() == usr.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        UsrDao sellerUsr = usrService.getByUsrUid(pdt.getUsrUid());

        if (sellerUsr == null || sellerUsr.getStatus() != 1 || sellerUsr.getUsrUid() < 1) {
            return new GenericResponse<>(ResponseMeta.SELLER_USER_NOT_EXIST, null);
        }

        long payPromotionPrice = 0;
        if (!"".equals(buyReq.getPayPromotion())) {
            PromotionReq promotionReq = new PromotionReq();
            promotionReq.setPdtUid(pdt.getPdtUid());
            promotionReq.setBrandUid(pdt.getBrandUid());
            promotionReq.setDealType(DEAL_TYPE.BUYER);
            promotionReq.setCategoryUid(pdt.getCategoryUid());
            promotionReq.setPromotionCode(buyReq.getPayPromotion());

            PromotionCodeDao codeDao = otherService.getPromotionInfo(promotionReq, usr.getUsrUid());

            if (codeDao == null)
                return new GenericResponse<>(ResponseMeta.PROMOTION_NOT_EXIST, null);

            payPromotionPrice = (codeDao.getPriceUnit() == 2) ? codeDao.getPrice() : pdt.getPrice() * codeDao.getPrice() / 100;
        }

        long verifyPrice = (buyReq.getPdtVerifyEnabled() == 1) ? CommonConstant.PRICE_PDT_VERIFICATION : 0;

        UsrCardDao usrCard = null;
        if (buyReq.getPayType().getCode() == PAY_TYPE.SIMPLECARD.getCode()) {
            usrCard = usrService.findOneUsrCard(buyReq.getUsrCardUid());
            if (usrCard == null || usr.getStatus() == 0 || usrCard.getUsrUid() != usr.getUsrUid())
                return new GenericResponse<>(ResponseMeta.CARD_NOT_EXIST, null);
        }

        BrandDao brand = brandService.getByBrandUid(pdt.getBrandUid());
        CategoryDao category = categoryService.findOne(pdt.getCategoryUid());

        DealDao deal = new DealDao();
        deal.setUsrUid(usr.getUsrUid());
        deal.setPdtUid(pdt.getPdtUid());
        deal.setSellerUsrUid(pdt.getUsrUid());
        deal.setValetUid(pdt.getValetUid());
        deal.setRecipientNm(buyReq.getRecipientNm());
        deal.setRecipientPhone(buyReq.getRecipientPhone());
        deal.setRecipientAddress(buyReq.getRecipientAddress());
        deal.setBrandEn((brand == null) ? "" : brand.getNameEn());
        deal.setPdtTitle(StringHelper.join(Arrays.asList(((brand == null) ? "" : brand.getNameKo()), pdt.getColorName(),
                pdt.getPdtModel(), ((category == null) ? "" : category.getCategoryName())), " "));
        deal.setPdtSize(pdt.getPdtSize());
        deal.setPdtImg(pdt.getProfileImg());
        deal.setPdtPrice(pdt.getPrice());
        deal.setSendPrice(((sellerUsr.getFreeSendPrice() > 0 && sellerUsr.getFreeSendPrice() <= pdt.getPrice()) ? 0 : pdt.getSendPrice()) + buyReq.getIslandSendPrice());
        deal.setVerifyPrice(verifyPrice);
        deal.setRewardPrice(buyReq.getRewardPrice());
        deal.setPayPromotionPrice(payPromotionPrice);
        deal.setPayPromotion(buyReq.getPayPromotion());

        long price = deal.getPdtPrice() + deal.getSendPrice() + deal.getVerifyPrice() - deal.getRewardPrice() - deal.getPayPromotionPrice();
        if (price < 0) {
            return new GenericResponse<>(ResponseMeta.PAY_PRICE_OVER_ZERO, null);
        }
        deal.setPrice(price);
        deal.setPayImportMethod(buyReq.getPayType().getCode());
        deal.setUsrCardUid(buyReq.getUsrCardUid());
        deal.setSelluvPrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_FEE_RATE));
        deal.setPayFeePrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_PAY_FEE_RATE));

        //결제전 거래상태를 임시로 삭제상태로 전환
        deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
        dealService.save(deal);

        if (deal.getPayImportMethod() == PAY_TYPE.NORMALCARD.getCode() ||
                deal.getPayImportMethod() == PAY_TYPE.REALTIMEPAY.getCode()) {

            String paymentUrl = CommonConstant.SELLUV_HOST + "web/payment/" + accessToken + "/" + deal.getDealUid();

            deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

            DealPaymentDto dealPaymentDto = new DealPaymentDto();
            dealPaymentDto.setDeal(deal);
            dealPaymentDto.setPaymentUrl(paymentUrl);

            return new GenericResponse<>(ResponseMeta.OK, dealPaymentDto);
        }

        //카드 간편결제처리
        CardPayment cardPayment = new CardPayment();
        cardPayment.setCustomer_uid("" + deal.getUsrCardUid());
        cardPayment.setMerchant_uid("" + deal.getDealUid());
        cardPayment.setAmount(BigDecimal.valueOf(deal.getPrice()));
        cardPayment.setName(deal.getPdtTitle());
        cardPayment.setBuyer_name(deal.getRecipientNm());
        cardPayment.setBuyer_email(usr.getUsrMail());
        cardPayment.setBuyer_tel(deal.getRecipientPhone());
        cardPayment.setBuyer_addr(deal.getRecipientAddress());

        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        PaymentAnnotation paymentResult;
        try {
            paymentResult = iamportClientHelper.cardPaymentProcess(cardPayment).getResponse();
        } catch (Exception e) {
            e.printStackTrace();
            paymentResult = null;
        }

        if (paymentResult == null || !paymentResultService.paymentResultProcess(paymentResult, true)) { //결제실패시

            return new GenericResponse<>(new ResponseMeta(ResponseMeta.PAY_FAILED.getErrCode(), "카드결제에 실패했습니다. 카드상태를 확인해주세요", null), null);
        }

        deal.setPayTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode());
        dealService.save(deal);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        if (buyReq.getRewardPrice() > 0) { //셀럽머니를 사용하면
            moneyChangeHisService.insertPointHistory(usr, -deal.getRewardPrice(), MONEY_HIS_TYPE.MONEYUSE, deal);
        }

        inviteRewardHisService.addFirstBoughtReward(usr, deal);
        pdt.setStatus(PDT_STATUS_TYPE.SOLD.getCode());//상품판매완료로 전환
        pdtService.save(pdt);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH01_S_PAY_COMPLETED, sellerUsr, usr, deal, pdt);

        DealPaymentDto dealPaymentDto = new DealPaymentDto();
        dealPaymentDto.setDeal(deal);
        dealPaymentDto.setPaymentUrl("");

        return new GenericResponse<>(ResponseMeta.OK, dealPaymentDto);
    }

    @ApiOperation(value = "판매자 카운터 네고 요청",
            notes = "네고제안에 대한 카운터네고를 제안한다.")
    @PutMapping(value = "/{dealUid}/nego/counter")
    public GenericResponse<Void> postNegoRefuse(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestParam(value = "reqPrice") long reqPrice) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.NEGO_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (deal.getPdtPrice() < reqPrice || reqPrice < deal.getReqPrice()) {
            return new GenericResponse<>(ResponseMeta.PAY_PRICE_RANGE_ERROR, null);
        }

        deal.setReqPrice(reqPrice);
        deal.setNegoTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.COUNTER_NEGO_REQUEST.getCode());
        dealService.save(deal);

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH08_B_NEGO_UPDATED, buyerUsr, usr, deal, null, "1", 0);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "판매자네고거절",
            notes = "상품에 대한 네고를 거절한다.")
    @PostMapping(value = "/{dealUid}/nego/refuse")
    public GenericResponse<Void> postNegoRefuse(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.NEGO_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }


        deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
        deal.setCancelReason("네고거절");
        deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
        dealService.save(deal);

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH08_B_NEGO_UPDATED, buyerUsr, usr, deal, null, "2", 0);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "판매자네고승인",
            notes = "상품에 대한 네고를 승인한다.")
    @PostMapping(value = "/{dealUid}/nego/allow")
    public GenericResponse<DealDao> postNegoAllow(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.NEGO_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        PdtDao pdt = pdtService.getPdtByPdtUid(deal.getPdtUid());
        if (pdt.getStatus() == PDT_STATUS_TYPE.DELETED.getCode()) {
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("상품삭제됨");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return new GenericResponse<>(ResponseMeta.PDT_NOT_EXIST, null);
        } else if (pdt.getStatus() == PDT_STATUS_TYPE.SOLD.getCode()) {
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("상품없음");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return new GenericResponse<>(ResponseMeta.PAY_PDT_ALREADY_SOLD, null);
        } else if (pdt.getStatus() != PDT_STATUS_TYPE.NORMAL.getCode()) {
            return new GenericResponse<>(ResponseMeta.PAY_PDT_NOT_NORMAL, null);
        }

        UsrCardDao usrCard = usrService.findOneUsrCard(deal.getUsrCardUid());
        if (usrCard == null || usr.getStatus() == 0 || usrCard.getUsrUid() != deal.getUsrUid()) {
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("유저정보 오류");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return new GenericResponse<>(ResponseMeta.CARD_NOT_EXIST, null);
        }

        deal.setPdtPrice(deal.getReqPrice());
        deal.setPrice(deal.getPdtPrice() + deal.getSendPrice());
        deal.setPayTime(new Timestamp(System.currentTimeMillis()));

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());

        //카드 간편결제처리
        CardPayment cardPayment = new CardPayment();
        cardPayment.setCustomer_uid("" + deal.getUsrCardUid());
        cardPayment.setMerchant_uid("" + deal.getDealUid());
        cardPayment.setAmount(BigDecimal.valueOf(deal.getPrice()));
        cardPayment.setName(deal.getPdtTitle());
        cardPayment.setBuyer_name(deal.getRecipientNm());
        cardPayment.setBuyer_email(buyerUsr.getUsrMail());
        cardPayment.setBuyer_tel(deal.getRecipientPhone());
        cardPayment.setBuyer_addr(deal.getRecipientAddress());

        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        PaymentAnnotation paymentResult;
        try {
            paymentResult = iamportClientHelper.cardPaymentProcess(cardPayment).getResponse();
        } catch (Exception e) {
            e.printStackTrace();
            paymentResult = null;
        }

        if (paymentResult == null || !paymentResultService.paymentResultProcess(paymentResult, true)) { //결제실패시
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH08_B_NEGO_UPDATED, buyerUsr, usr, deal, null, "4", 0);

            return new GenericResponse<>(ResponseMeta.PAY_AUTOPAYED_FAILED, null);
        }

        deal.setPayImportCode(paymentResult.getImp_uid());
        deal.setPayTime(new Timestamp(System.currentTimeMillis()));
        deal.setCancelTime(null);
        deal.setCancelReason("");
        deal.setSelluvPrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_FEE_RATE));
        deal.setPayFeePrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_PAY_FEE_RATE));
        deal.setStatus(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode());
        dealService.save(deal);

        pdt.setStatus(PDT_STATUS_TYPE.SOLD.getCode());
        pdtService.save(pdt);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH08_B_NEGO_UPDATED, buyerUsr, usr, deal, null, "3", 0);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "구매자카운터네고거절",
            notes = "상품에 대한 카운터네고를 거절한다.")
    @PostMapping(value = "/{dealUid}/nego/refuse/counter")
    public GenericResponse<Void> postNegoRefuseCounter(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.COUNTER_NEGO_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
        deal.setCancelReason("카운터네고거절");
        deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
        dealService.save(deal);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "구매자카운터네고승인",
            notes = "상품에 대한 카운터네고를 승인한다.")
    @PostMapping(value = "/{dealUid}/nego/allow/counter")
    public GenericResponse<DealDao> postNegoAllowCounter(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.COUNTER_NEGO_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        PdtDao pdt = pdtService.getPdtByPdtUid(deal.getPdtUid());
        if (pdt.getStatus() == PDT_STATUS_TYPE.DELETED.getCode()) {
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("상품삭제됨");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return new GenericResponse<>(ResponseMeta.PDT_NOT_EXIST, null);
        } else if (pdt.getStatus() == PDT_STATUS_TYPE.SOLD.getCode()) {
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("상품없음");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return new GenericResponse<>(ResponseMeta.PAY_PDT_ALREADY_SOLD, null);
        } else if (pdt.getStatus() != PDT_STATUS_TYPE.NORMAL.getCode()) {
            return new GenericResponse<>(ResponseMeta.PAY_PDT_NOT_NORMAL, null);
        }

        UsrCardDao usrCard = usrService.findOneUsrCard(deal.getUsrCardUid());
        if (usrCard == null || usr.getStatus() == 0 || usrCard.getUsrUid() != deal.getUsrUid()) {
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("유저정보 오류");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return new GenericResponse<>(ResponseMeta.CARD_NOT_EXIST, null);
        }

        deal.setPdtPrice(deal.getReqPrice());
        deal.setPrice(deal.getPdtPrice() + deal.getSendPrice());
        deal.setPayTime(new Timestamp(System.currentTimeMillis()));

        UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());

        //카드 간편결제처리
        CardPayment cardPayment = new CardPayment();
        cardPayment.setCustomer_uid("" + deal.getUsrCardUid());
        cardPayment.setMerchant_uid("" + deal.getDealUid());
        cardPayment.setAmount(BigDecimal.valueOf(deal.getPrice()));
        cardPayment.setName(deal.getPdtTitle());
        cardPayment.setBuyer_name(deal.getRecipientNm());
        cardPayment.setBuyer_email(usr.getUsrMail());
        cardPayment.setBuyer_tel(deal.getRecipientPhone());
        cardPayment.setBuyer_addr(deal.getRecipientAddress());

        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        PaymentAnnotation paymentResult;
        try {
            paymentResult = iamportClientHelper.cardPaymentProcess(cardPayment).getResponse();
        } catch (Exception e) {
            e.printStackTrace();
            paymentResult = null;
        }

        if (paymentResult == null || !paymentResultService.paymentResultProcess(paymentResult, true)) { //결제실패시
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH08_B_NEGO_UPDATED, usr, sellerUsr, deal, null, "5", 0);

            return new GenericResponse<>(ResponseMeta.PAY_AUTOPAYED_FAILED, null);
        }

        deal.setPayImportCode(paymentResult.getImp_uid());
        deal.setPayTime(new Timestamp(System.currentTimeMillis()));
        deal.setCancelTime(null);
        deal.setCancelReason("");
        deal.setSelluvPrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_FEE_RATE));
        deal.setPayFeePrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_PAY_FEE_RATE));
        deal.setStatus(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode());
        dealService.save(deal);

        pdt.setStatus(PDT_STATUS_TYPE.SOLD.getCode());
        pdtService.save(pdt);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH01_S_PAY_COMPLETED, sellerUsr, usr, deal, pdt);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "구매자주문취소",
            notes = "구매자가 거래를 취소한다.(결제후 3시간만 유효)")
    @PostMapping(value = "/{dealUid}/cancel")
    public GenericResponse<DealDao> postCancel(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getUsrUid() != usr.getUsrUid() || deal.getPayTime() == null ||
                deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (System.currentTimeMillis() - deal.getPayTime().getTime() > 3 * 3600 * 1000) {
            return new GenericResponse<>(ResponseMeta.PAY_TIME_RANGE_ERROR, null);
        }

        PdtDao pdt = pdtService.getPdtInfo(deal.getPdtUid());

        //결제취소처리
        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        CancelData cancelData = new CancelData(deal.getPayImportCode(), true);
        boolean isCancelSuccess = false;
        try {
            isCancelSuccess = (iamportClientHelper.cancelPayment(cancelData).getCode() == 0);
        } catch (Exception e) {
            isCancelSuccess = false;
            e.printStackTrace();
        }

        if (!isCancelSuccess) {
            return new GenericResponse<>(ResponseMeta.PAY_CANCEL_FAILED, null);
        }

        deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
        deal.setCancelReason("구매자 주문취소");
        deal.setStatus(DEAL_STATUS_TYPE.DEAL_CANCEL.getCode());
        dealService.save(deal);

        pdt.setStatus(PDT_STATUS_TYPE.NORMAL.getCode());
        pdtService.save(pdt);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH05_S_DEAL_CANCELED, sellerUsr, usr, deal, null);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "판매자 판매거절",
            notes = "판매자가 거래를 취소한다.(결제후 3시간만 유효)")
    @PostMapping(value = "/{dealUid}/cancel/seller")
    public GenericResponse<DealDao> postCancelSeller(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() || deal.getPayTime() == null ||
                deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (System.currentTimeMillis() - deal.getPayTime().getTime() > 3 * 3600 * 1000) {
            return new GenericResponse<>(ResponseMeta.PAY_TIME_RANGE_ERROR, null);
        }

        PdtDao pdt = pdtService.getPdtInfo(deal.getPdtUid());

        //결제취소처리
        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        CancelData cancelData = new CancelData(deal.getPayImportCode(), true);
        boolean isCancelSuccess = false;
        try {
            isCancelSuccess = (iamportClientHelper.cancelPayment(cancelData).getCode() == 0);
        } catch (Exception e) {
            isCancelSuccess = false;
            e.printStackTrace();
        }

        if (!isCancelSuccess) {
            return new GenericResponse<>(ResponseMeta.PAY_CANCEL_FAILED, null);
        }

        deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
        deal.setCancelReason("판매자 판매거절");
        deal.setStatus(DEAL_STATUS_TYPE.DEAL_CANCEL.getCode());
        dealService.save(deal);

        pdt.setStatus(PDT_STATUS_TYPE.STOP.getCode());
        pdtService.save(pdt);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH04_B_DEAL_CANCELED, buyerUsr, usr, deal, null);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "구매자 거래확정",
            notes = "구매자가 거래를 확정하여 상태를 거래완료로 전환한다.")
    @PostMapping(value = "/{dealUid}/complete")
    @Transactional
    public GenericResponse<DealDao> postComplete(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());

        if (deal.getUsrUid() != usr.getUsrUid() || deal.getPayTime() == null ||
                deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_COMPLETE.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        //TODO: 판매자 정산처리??? 에스크로 자동처리....

        long cashPromotionPrice = 0;
        PdtDao pdt = pdtService.getPdtByPdtUid(deal.getPdtUid());
        if (pdt != null && !"".equals(pdt.getPromotionCode())) {
            PromotionReq promotionReq = new PromotionReq();
            promotionReq.setPdtUid(pdt.getPdtUid());
            promotionReq.setBrandUid(pdt.getBrandUid());
            promotionReq.setDealType(DEAL_TYPE.SELLER);
            promotionReq.setCategoryUid(pdt.getCategoryUid());
            promotionReq.setPromotionCode(pdt.getPromotionCode());

            PromotionCodeDao codeDao = otherService.getPromotionInfo(promotionReq, usr.getUsrUid());

            if (codeDao == null) {
                cashPromotionPrice = 0;
            } else {
                deal.setCashPromotion(pdt.getPromotionCode());
                cashPromotionPrice = (codeDao.getPriceUnit() == 2) ? codeDao.getPrice() : deal.getPrice() * codeDao.getPrice() / 100;
            }
        }

        deal.setCashPromotionPrice(cashPromotionPrice);
        deal.setCashPrice(deal.getPdtPrice()+ deal.getSendPrice() - deal.getSelluvPrice() - deal.getPayFeePrice() + deal.getCashPromotionPrice());
        deal.setCashAccount((sellerUsr != null) ? sellerUsr.getBankNm() + " " + sellerUsr.getAccountNum() : "");
        deal.setCompTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode());
        dealService.save(deal);

        //구매자 구매적립리워드
        int buyRewardPercent = Integer.parseInt(otherService.getSystemSetting(SYSTEM_SETTING_TYPE.DEAL_REWARD.getCode()).getSetting());
        long rewardPrice = deal.getPrice() * buyRewardPercent / 100;
        moneyChangeHisService.insertPointHistory(usr, rewardPrice, MONEY_HIS_TYPE.BUYREWARD, deal);

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH03_D_DEAL_COMPLETED, sellerUsr, usr, deal, null);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "주문상세",
            notes = "주문상세정보를 얻는다.")
    @GetMapping(value = "/{dealUid}")
    public GenericResponse<DealDetailDto> getDetail(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getUsrUid() != usr.getUsrUid() && deal.getSellerUsrUid() != usr.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        DealDetailDto detailDto = new DealDetailDto();
        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));
        detailDto.setDeal(deal);

        int targetUsrUid = (deal.getUsrUid() == usr.getUsrUid()) ? deal.getSellerUsrUid() : deal.getUsrUid();
        detailDto.setUsr(usrService.convertUsrDaoToUsrMiniDto(usrService.getByUsrUid(targetUsrUid)));

        List<String> refundPhotos = new ArrayList<>();
        if (deal.getRefundPhotos() != null && !deal.getRefundPhotos().isEmpty()) {
            String[] spiltStrings = StringHelper.splitString(deal.getRefundPhotos(), ",");
            for (String item : spiltStrings) {
                if (!"".equals(StringHelper.trimWhitespace(item))) {
                    refundPhotos.add(StarmanHelper.getTargetImageUrl("deal", deal.getDealUid(), StringHelper.trimWhitespace(item)));
                }
            }
        }
        detailDto.setRefundPhotos(refundPhotos);

        return new GenericResponse<>(ResponseMeta.OK, detailDto);
    }

    @ApiOperation(value = "구매자 반품신청",
            notes = "배송완료시 반품을 신청한다. 반품신청상태로 변경된다.")
    @PutMapping(value = "/{dealUid}/refund")
    public GenericResponse<DealDao> putRefund(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestBody @Valid DealRefundReq refundReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getUsrUid() != usr.getUsrUid() || deal.getPayTime() == null ||
                deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_COMPLETE.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        deal.setRefundReason(refundReq.getRefundReason());

        boolean isPhotos = false;
        String photos = StringHelper.join(refundReq.getPhotos(), ",");
        if (!"".equals(photos)) {
            deal.setRefundPhotos(photos);
            isPhotos = true;
        }

        deal.setRefundReqTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.REFUND_REQUEST.getCode());
        dealService.save(deal);

        if (isPhotos) {
            for (String photo : refundReq.getPhotos()) {
                StarmanHelper.moveTargetFolder("deal", deal.getDealUid(), StringHelper.trimWhitespace(photo));
            }
        }

        deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

        UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH09_S_REFUND_REQUEST, sellerUsr, usr, deal, null);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "후기작성",
            notes = "완료된 거래에 대한 후기를 작성한다.")
    @PutMapping(value = "/{dealUid}/review")
    public GenericResponse<Void> putReview(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestBody @Valid DealReviewReq reviewReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if ((deal.getUsrUid() != usr.getUsrUid() && deal.getSellerUsrUid() != usr.getUsrUid()) ||
                (deal.getStatus() != DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode() &&
                        deal.getStatus() != DEAL_STATUS_TYPE.CASH_COMPLETE.getCode()) ||
                deal.getCompTime().getTime() < System.currentTimeMillis() - 3 * 24 * 3600 * 1000) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        ReviewDao reviewDao = reviewService.getByDealUidAndUsrUid(dealUid, usr.getUsrUid());
        if (reviewDao != null) {
            return new GenericResponse<>(ResponseMeta.DEAL_REVIEW_ALREADY_ADDED, null);
        }

        int peerUsrUid;
        int reviewType;
        long reviewReward;
        if (deal.getUsrUid() == usr.getUsrUid()) {
            peerUsrUid = deal.getSellerUsrUid();
            reviewType = 1;
            reviewReward = Long.parseLong(
                    otherService.getSystemSetting(SYSTEM_SETTING_TYPE.BUY_REVIEW_REWARD.getCode()).getSetting());
        } else {
            peerUsrUid = deal.getUsrUid();
            reviewType = 2;
            reviewReward = Long.parseLong(
                    otherService.getSystemSetting(SYSTEM_SETTING_TYPE.SELL_REVIEW_REWARD.getCode()).getSetting());
        }

        ReviewDao review = new ReviewDao();
        review.setUsrUid(usr.getUsrUid());
        review.setDealUid(dealUid);
        review.setPeerUsrUid(peerUsrUid);
        review.setType(reviewType);
        review.setContent(reviewReq.getContent());
        review.setPoint(reviewReq.getPoint());
        review.setRewardPrice(reviewReward);

        reviewService.save(review);

        moneyChangeHisService.insertPointHistory(usr, reviewReward, MONEY_HIS_TYPE.REVIEWREWARD, deal);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "판매자 공유리워드 신청",
            notes = "판매자가 완료된 거래에 대한 공유리워드를 신청한다.")
    @PutMapping(value = "/{dealUid}/share")
    public GenericResponse<Void> putShare(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestBody @Valid DealShareReq shareReq) {

        if (shareReq.getInstagramPhotos().size() + shareReq.getTwitterPhotos().size() +
                shareReq.getNaverblogPhotos().size() + shareReq.getKakaostoryPhotos().size() +
                shareReq.getFacebookPhotos().size() == 0) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                (deal.getStatus() != DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode() &&
                        deal.getStatus() != DEAL_STATUS_TYPE.CASH_COMPLETE.getCode())) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (System.currentTimeMillis() - deal.getCompTime().getTime() > 7 * 24 * 3600 * 1000) {
            return new GenericResponse<>(ResponseMeta.PAY_TIME_RANGE_ERROR, null);
        }

        ShareDao share = new ShareDao();
        share.setUsrUid(usr.getUsrUid());
        share.setDealUid(dealUid);
        share.setInstagramYn((shareReq.getInstagramPhotos().size() > 0) ? 1 : 0);
        share.setTwitterYn((shareReq.getTwitterPhotos().size() > 0) ? 1 : 0);
        share.setNaverblogYn((shareReq.getNaverblogPhotos().size() > 0) ? 1 : 0);
        share.setKakaostoryYn((shareReq.getKakaostoryPhotos().size() > 0) ? 1 : 0);
        share.setFacebookYn((shareReq.getFacebookPhotos().size() > 0) ? 1 : 0);
        share.setInstagramPhotos(StringHelper.join(shareReq.getInstagramPhotos(), ","));
        share.setTwitterPhotos(StringHelper.join(shareReq.getTwitterPhotos(), ","));
        share.setNaverblogPhotos(StringHelper.join(shareReq.getNaverblogPhotos(), ","));
        share.setKakaostoryPhotos(StringHelper.join(shareReq.getKakaostoryPhotos(), ","));
        share.setFacebookPhotos(StringHelper.join(shareReq.getFacebookPhotos(), ","));
        share.setRewardPrice(0);
        share.setStatus(2);

        otherService.saveShare(share);

        for (String photo : shareReq.getInstagramPhotos()) {
            StarmanHelper.moveTargetFolder("share", share.getShareUid(), StringHelper.trimWhitespace(photo));
        }
        for (String photo : shareReq.getTwitterPhotos()) {
            StarmanHelper.moveTargetFolder("share", share.getShareUid(), StringHelper.trimWhitespace(photo));
        }
        for (String photo : shareReq.getNaverblogPhotos()) {
            StarmanHelper.moveTargetFolder("share", share.getShareUid(), StringHelper.trimWhitespace(photo));
        }
        for (String photo : shareReq.getKakaostoryPhotos()) {
            StarmanHelper.moveTargetFolder("share", share.getShareUid(), StringHelper.trimWhitespace(photo));
        }
        for (String photo : shareReq.getFacebookPhotos()) {
            StarmanHelper.moveTargetFolder("share", share.getShareUid(), StringHelper.trimWhitespace(photo));
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "판매자 반품승인",
            notes = "구매자의 반품요청을 판매자가 승인한다. 해당 선택된 주소정보 업데이트 및 거래상태를 반품승인상태로 변경한다.")
    @PostMapping(value = "/{dealUid}/refund/allow")
    public GenericResponse<Void> postRefundAllow(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestBody @Valid DealRefundAddressReq refundAddressReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.REFUND_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        usrService.saveUsrAddress(usr.getUsrUid(), refundAddressReq.getAddressSelectionIndex(), refundAddressReq.getAddressName(),
                refundAddressReq.getAddressPhone(), refundAddressReq.getAddressDetail(), refundAddressReq.getAddressDetailSub(), refundAddressReq.getAddressSelectionIndex());

        deal.setRefundCheckTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.REFUND_ALLOW.getCode());
        dealService.save(deal);

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH10_B_REFUND_UPDATED, buyerUsr, usr, deal, null, "1", 0);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "판매자 반품거절",
            notes = "구매자의 반품요청을 판매자가 거절한다. 반품거절확인?상태로 변경된다.")
    @PostMapping(value = "/{dealUid}/refund/disallow")
    public GenericResponse<DealDao> postRefundDisallow(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.REFUND_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        //TODO 반품거절 프로세스 미결 - 현재는 거래완료 상태로 처리함
        deal.setCompTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode());
        dealService.save(deal);

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH10_B_REFUND_UPDATED, buyerUsr, usr, deal, null, "2", 0);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "판매자 반품확인",
            notes = "반품승인상태에서 반품을 확인한다. 반품완료상태로 변경된다.")
    @PostMapping(value = "/{dealUid}/refund/complete")
    public GenericResponse<DealDao> postRefundComplete(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.REFUND_REQUEST.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        // 정산취소처리는 구현안함?..., 구매취소처리
        //결제취소처리
        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        CancelData cancelData = new CancelData(deal.getPayImportCode(), true);
        boolean isCancelSuccess = false;
        try {
            isCancelSuccess = (iamportClientHelper.cancelPayment(cancelData).getCode() == 0);
        } catch (Exception e) {
            isCancelSuccess = false;
            e.printStackTrace();
        }

        if (!isCancelSuccess) {
            return new GenericResponse<>(ResponseMeta.PAY_CANCEL_FAILED, null);
        }

        deal.setRefundCheckTime(new Timestamp(System.currentTimeMillis()));
        deal.setStatus(DEAL_STATUS_TYPE.REFUND_COMPLETE.getCode());
        dealService.save(deal);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "판매자 운송장 번호등록",
            notes = "배송준비상태에서 운송장번호 업데이트 및 배송진행상태로 변경한다. 인증전송요청한 경우 정품인증상태로 변경됨")
    @PutMapping(value = "/{dealUid}/deliveryNumber")
    public GenericResponse<DealDao> putDeliveryNumber(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestBody @Valid DealDeliveryNumReq deliveryNumReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if (deal.getSellerUsrUid() != usr.getUsrUid() ||
                deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        //운송장 번호 유효성 검사필요
        SweetTrackerClientHelper sweetTrackerClient = new SweetTrackerClientHelper();
        InvoiceReq invoiceReq = new InvoiceReq();
        invoiceReq.setTCode(SweetTrackerDeliveryCompanyHelper.getCompanyCode(deliveryNumReq.getDeliveryCompany()));
        invoiceReq.setTInvoice(deliveryNumReq.getDeliveryNumber());
        SweetTrackerResponse<TrackingInfo> sweetTrackerResponse = sweetTrackerClient.getTrackingInfo(invoiceReq);
        if (sweetTrackerResponse.getCode() != SweetTrackerResponse.SWEET_TRACKER_API_200) {
            return new GenericResponse<>(new ResponseMeta(ResponseMeta.DELIVERY_NUMBER_FAILED.getErrCode(),
                    ResponseMeta.DELIVERY_NUMBER_FAILED.getErrMsg() + "\n(" + sweetTrackerResponse.getMessage() + ")"), null);
        }

        if (deal.getVerifyPrice() == 0) {

            //에스크로 등록
            IamportClientHelper iamportClientHelper = new IamportClientHelper();
            EscrowData escrowData = new EscrowData();

            EscrowLogisPeopleAnnotation sender = new EscrowLogisPeopleAnnotation();
            sender.setName(usr.getUsrNm());
            sender.setTel(usr.getUsrPhone());
            UsrAddressDao sellerAddress = usrService.getAddressActiveByUsrUid(usr.getUsrUid());
            sender.setAddr((sellerAddress == null) ? "" : sellerAddress.getAddressDetail());
            sender.setPostcode((sellerAddress == null) ? "" : sellerAddress.getAddressDetailSub());
            escrowData.setSender(sender);

            EscrowLogisPeopleAnnotation receiver = new EscrowLogisPeopleAnnotation();
            receiver.setName(deal.getRecipientNm());
            receiver.setTel(deal.getRecipientPhone());
            receiver.setAddr(deal.getRecipientAddress());
            receiver.setPostcode("");
            escrowData.setReceiver(receiver);

            EscrowLogisInfoAnnotation logis = new EscrowLogisInfoAnnotation();
            String[] deliveryInfos = deal.getDeliveryNumber().split(" ");
            if (deliveryInfos.length < 1) {
                logis.setCompany("");
                logis.setInvoice("");
            } else if (deliveryInfos.length < 2) {
                logis.setCompany(IamportDeliveryCompanyHelper.getCompanyCode(deliveryInfos[0]));
                logis.setInvoice("");
            } else {
                logis.setCompany(IamportDeliveryCompanyHelper.getCompanyCode(deliveryInfos[0]));
                logis.setInvoice(deliveryInfos[1]);
            }
            logis.setSent_at(new Timestamp(System.currentTimeMillis()));

            escrowData.setLogis(logis);

            EscrowLogisAnnotation escrowLogisAnnotation = null;
            try {
                escrowLogisAnnotation = iamportClientHelper.registerEscrowProcess(deal.getPayImportCode(), escrowData).getResponse();
            } catch (Exception e) {
                escrowLogisAnnotation = null;
                e.printStackTrace();
            }

            deal.setDeliveryNumber(deliveryNumReq.getDeliveryCompany() + " " + deliveryNumReq.getDeliveryNumber());
            deal.setDeliveryTime(new Timestamp(System.currentTimeMillis()));
            deal.setStatus(DEAL_STATUS_TYPE.DELIVERY_PROGRESS.getCode());
        } else {
            deal.setDeliveryTime(new Timestamp(System.currentTimeMillis()));
            deal.setStatus(DEAL_STATUS_TYPE.PDT_VERIFY.getCode());
            deal.setVerifyDeliveryNumber(deliveryNumReq.getDeliveryCompany() + " " + deliveryNumReq.getDeliveryNumber());
        }

        dealService.save(deal);

        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        if (deal.getVerifyPrice() > 0)
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH06_B_PDT_VERIFICATION, buyerUsr, usr, deal, null);
        else
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH02_B_DELIVERY_SENT, buyerUsr, usr, deal, null, "1", 0);

        return new GenericResponse<>(ResponseMeta.OK, deal);
    }

    @ApiOperation(value = "택배예약하기",
            notes = "택배를 예약한다.")
    @PutMapping(value = "/{dealUid}/delivery")
    public GenericResponse<DeliveryHistoryDao> putDeliveryHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @RequestBody @Valid DealDeliveryReq deliveryReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if ((deal.getUsrUid() != usr.getUsrUid() && deal.getSellerUsrUid() != usr.getUsrUid()) ||
                (deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode() &&
                        deal.getStatus() != DEAL_STATUS_TYPE.REFUND_ALLOW.getCode())) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        DeliveryHistoryDao deliveryHistory = dealService.getDeliveryHistoryByUsrUidAndDealUid(usr.getUsrUid(), dealUid);
        if (deliveryHistory != null) {
            return new GenericResponse<>(ResponseMeta.DELIVERY_HISTORY_ALREADY_EXIST, null);
        }

        deliveryHistory = new DeliveryHistoryDao();
        deliveryHistory.setUsrUid(usr.getUsrUid());
        deliveryHistory.setDealUid(dealUid);
        deliveryHistory.setKind(1); //편의점 택배
        deliveryHistory.setType((usr.getUsrUid() == deal.getUsrUid()) ? 2 : 1); //반품인가, 배송인가 판정
        deliveryHistory.setRecipientNm(deliveryReq.getRecipientNm());
        deliveryHistory.setRecipientPhone(deliveryReq.getRecipientPhone());
        deliveryHistory.setRecipientAddress(deliveryReq.getRecipientAddress());
        deliveryHistory.setRecipientAddressDetail(deliveryReq.getRecipientAddressDetail());

        //TODO: 택배사 예약 API 구현 시작
        /*
        ReservationReq reservationReq = new ReservationReq();
        reservationReq.setCust_ord_no("" + CvsnetClientHelper.SELLUV_CVSNET_CODE + String.format("%09d", dealUid) + deliveryReq.getDeliveryType());
        reservationReq.setAcpt_dt(new java.text.SimpleDateFormat("yyyyMMdd").format(System.currentTimeMillis()));
        reservationReq.setSend_tel(deliveryReq.getRecipientPhone());
        reservationReq.setSend_name(deliveryReq.getRecipientNm());
        reservationReq.setSend_zip(deliveryReq.getRecipientAddressDetail());
        String[] sendAddressArray = deliveryReq.getRecipientAddress().split("\n");
        reservationReq.setSend_addr1(sendAddressArray.length > 0 ? sendAddressArray[0] : "");
        reservationReq.setSend_addr2(sendAddressArray.length > 1 ? sendAddressArray[1] : "");
        reservationReq.setGoods(deal.getPdtTitle());
        reservationReq.setGoods_cnt(1);
        reservationReq.setGoods_price(deal.getPrice() > 500000 ? 500000 : deal.getPrice());
        if (deliveryReq.getDeliveryType() == 2) {
            reservationReq.setReceive_tel(CommonConstant.SELLUV_DELEVERY_TEL);
            reservationReq.setReceive_name(CommonConstant.SELLUV_DELEVERY_NAME);
            reservationReq.setReceive_addr1(CommonConstant.SELLUV_DELEVERY_ADDRESS);
            reservationReq.setReceive_addr2(CommonConstant.SELLUV_DELEVERY_ADDRESS_DETAIL);
            reservationReq.setReceive_zip(CommonConstant.SELLUV_DELEVERY_POSTCODE);
        } else {
            reservationReq.setReceive_tel(deal.getRecipientPhone());
            reservationReq.setReceive_name(deal.getRecipientNm());
            String[] receiveAddressArray = deal.getRecipientAddress().split("\n");
            reservationReq.setReceive_addr1(receiveAddressArray.length > 0 ? receiveAddressArray[0] : "");
            reservationReq.setReceive_addr2(receiveAddressArray.length > 1 ? receiveAddressArray[1] : "");
            reservationReq.setReceive_zip(receiveAddressArray.length > 2 ? receiveAddressArray[2] : "");
        }
        reservationReq.setTls_gb(deliveryReq.getDeliveryType() == 3 ? 2 : 1);
        reservationReq.setGoods_info(deal.getPdtTitle());
        reservationReq.setOrder_num(dealUid);

        CvsnetClientHelper cvsnetClientHelper = new CvsnetClientHelper();
        cvsnetClientHelper.requestReservation(reservationReq);
        */

        deliveryHistory.setConvenienceNumber(""); //callback

        dealService.saveDeliveryHistory(deliveryHistory);

        return new GenericResponse<>(ResponseMeta.OK, deliveryHistory);
    }

    @ApiOperation(value = "택배예약정보얻기",
            notes = "해당 거래에 대하여 예약한 택배정보를 얻는다.")
    @GetMapping(value = "/{dealUid}/delivery")
    public GenericResponse<DeliveryHistoryDao> getDeliveryHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {
        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if ((deal.getUsrUid() != usr.getUsrUid() && deal.getSellerUsrUid() != usr.getUsrUid())) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        DeliveryHistoryDao deliveryHistory = dealService.getDeliveryHistoryByUsrUidAndDealUid(usr.getUsrUid(), dealUid);
        if (deliveryHistory == null) {
            return new GenericResponse<>(ResponseMeta.DELIVERY_HISTORY_NOT_EXIST, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, deliveryHistory);
    }

    @ApiOperation(value = "택배예약 취소하기",
            notes = "해당 거래에 대하여 예약한 택배를 취소한다.")
    @DeleteMapping(value = "/{dealUid}/delivery")
    public GenericResponse<Void> deleteDeliveryHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {
        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if ((deal.getUsrUid() != usr.getUsrUid() && deal.getSellerUsrUid() != usr.getUsrUid())) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        DeliveryHistoryDao deliveryHistory = dealService.getDeliveryHistoryByUsrUidAndDealUid(usr.getUsrUid(), dealUid);
        if (deliveryHistory == null) {
            return new GenericResponse<>(ResponseMeta.DELIVERY_HISTORY_NOT_EXIST, null);
        }

        dealService.deleteDeliveryHistory(deliveryHistory.getDeliveryHistoryUid());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "대상자 주소얻기",
            notes = "해당 거래에 대하여 대상자의 주소정보를 얻는다.")
    @GetMapping(value = "/{dealUid}/targetAddress")
    public GenericResponse<List<UsrAddressDao>> getTargetAddressInfo(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid) {
        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        DealDao deal = dealService.getDealInfo(dealUid);

        if ((deal.getUsrUid() != usr.getUsrUid() && deal.getSellerUsrUid() != usr.getUsrUid())) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        List<UsrAddressDao> addressDaoList = usrService.getAddressListByUsrUid(
                (deal.getUsrUid() == usr.getUsrUid()) ? deal.getSellerUsrUid() : deal.getUsrUid());

        return new GenericResponse<>(ResponseMeta.OK, addressDaoList);
    }
}
