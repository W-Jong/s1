package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.HomeService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "/home", description = "홈관리")
@RequestMapping("/home")
public class HomeController {

    private final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private HomeService homeService;

    @ApiOperation(value = "인기목록얻기",
            notes = "인기목록을 얻는다.")
    @GetMapping(value = "/fame")
    public GenericResponse<Page<PdtListDto>> getFamePdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page,
            @ApiParam(value = "랜덤정렬을 의한 seed값, pagination할때는 동일한 값으로 유지해야 함, page가 0일때 램덤값으로 다시 초기화")
            @RequestParam(value = "seed", defaultValue = "0") long seed) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, homeService.getFamePdtList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), seed, usr.getLikeGroup()));
    }

    @ApiOperation(value = "신상품목록얻기",
            notes = "신상품 목록을 얻는다.")
    @GetMapping(value = "/new")
    public GenericResponse<Page<PdtListDto>> getNewPdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, homeService.getNewPdtList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), usr.getLikeGroup()));
    }

    @ApiOperation(value = "피드목록얻기",
            notes = "피드 목록을 얻는다.")
    @GetMapping(value = "/feed")
    public GenericResponse<Page<UsrFeedPdtListDto>> getUsrFeedPdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, homeService.getUsrFeedPdtList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));
    }

    @ApiOperation(value = "인기스타일 목록얻기",
            notes = "인기스타일 목록을 얻는다.")
    @GetMapping(value = "/fame/style")
    public GenericResponse<Page<PdtListDto>> getFamePdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page,
            @ApiParam(value = "랜덤정렬을 의한 seed값, pagination할때는 동일한 값으로 유지해야 함, page가 0일때 램덤값으로 다시 초기화")
            @RequestParam(value = "seed", defaultValue = "0") long seed) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, homeService.getFameStylePdtList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), seed, usr.getLikeGroup()));
    }

    @ApiOperation(value = "신상품스타일 목록얻기",
            notes = "신상품스타일 목록을 얻는다.")
    @GetMapping(value = "/new/style")
    public GenericResponse<Page<PdtListDto>> getNewPdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, homeService.getNewPdtStyleList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), usr.getLikeGroup()));
    }

    @ApiOperation(value = "피드스타일 목록얻기",
            notes = "피드스타일 목록을 얻는다.")
    @GetMapping(value = "/feed/style")
    public GenericResponse<Page<UsrFeedPdtListDto>> getUsrFeedPdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, homeService.getUsrFeedPdtStyleList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));
    }
}
