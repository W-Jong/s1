package com.kyad.selluvapi.dto.usr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UsrSnsStatusDto {
    @ApiModelProperty("페이스북 연동상태")
    private boolean isFacekbook;
    @ApiModelProperty("네이버 연동상태")
    private boolean isNaver;
    @ApiModelProperty("카카오 연동상태")
    private boolean isKakaoTalk;
    @ApiModelProperty("인스타그램 연동상태")
    private boolean isInstagram;
    @ApiModelProperty("트위터 연동상태")
    private boolean isTwitter;
}
