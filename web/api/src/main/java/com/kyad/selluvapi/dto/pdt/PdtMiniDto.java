package com.kyad.selluvapi.dto.pdt;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class PdtMiniDto {
    @ApiModelProperty("상품UID")
    private int pdtUid;
//    @ApiModelProperty("추가된 시간")
//    private Timestamp regTime;
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    @ApiModelProperty("카테고리 UID")
    private int categoryUid;
    @ApiModelProperty("사이즈")
    private String pdtSize;
    @ApiModelProperty("대표사진")
    private String profileImg;
    @ApiModelProperty("변경전 가격")
    private long originPrice;
    @ApiModelProperty("판매가격")
    private long price;
    @ApiModelProperty("컬러명")
    private String colorName;
    @ApiModelProperty("모델명")
    private String pdtModel;
    @ApiModelProperty("업데이트 시간")
    private Timestamp edtTime;
    @ApiModelProperty("상태  0-삭제, 1-판매중 2-판매완료, 3-판매중지, 4-휴가모드")
    private int status;
    @ApiModelProperty("수정상태 true: 수정됨, false: 수정안됨")
    private boolean isEdited;
}
