package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Data
public class UsrAlarmDto {
    private int usrAlarmUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime;
    @ApiModelProperty("회원 UID")
    private int usrUid;
    @ApiModelProperty("알람분류 1-거래, 2-소식 3-댓글")
    private int kind;
    @ApiModelProperty("부분분류, 앱기획서 208~210페이지 알림리스트유형에 해당")
    private int subKind;
    @ApiModelProperty("타겟 UID, subKind에 따라 결정됨")
    private int targetUid;
    @ApiModelProperty("알림내용")
    private String content;
    @ApiModelProperty("연관된 상품UID")
    private int pdtUid;
    @ApiModelProperty("연관된 상품이미지")
    private String profileImg;
    @ApiModelProperty("연관된 유저 프로필이미지")
    private String usrProfileImg;
}
