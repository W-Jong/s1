package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.DeliveryHistoryDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryHistoryRepository extends CrudRepository<DeliveryHistoryDao, Integer> {

    @Query("select a from DeliveryHistoryDao a")
    Page<DeliveryHistoryDao> findAll(Pageable pageable);

    DeliveryHistoryDao findTopByUsrUidAndDealUidOrderByRegTimeDesc(int usrUid, int dealUid);
}
