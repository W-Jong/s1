package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.ReviewDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.other.ReviewListDto;
import com.kyad.selluvapi.jpa.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private UsrService usrService;

    public ReviewDao save(ReviewDao dao) {
        return reviewRepository.save(dao);
    }

    public ReviewDao getByDealUidAndUsrUid(int dealUid, int usrUid) {
        return reviewRepository.getByDealUidAndUsrUid(dealUid, usrUid);
    }

    public Page<ReviewListDto> findAllByUsrUid(Pageable pageable, int peerUsrUid, int point) {
        if (point == -1)
            return reviewRepository.findAllByPeerUsrUid(pageable, peerUsrUid)
                    .map(o -> convertObjectsToReviewListDto((Object[]) o));
        else
            return reviewRepository.findAllByPeerUsrUidAndPoint(pageable, peerUsrUid, point)
                    .map(o -> convertObjectsToReviewListDto((Object[]) o));
    }

    public long countByUsrUidAndPoint(int peerUsrUid, int point) {
        return reviewRepository.countAllByPeerUsrUidAndPointAndStatus(peerUsrUid, point, 1);
    }

    public ReviewListDto convertObjectsToReviewListDto(Object[] objects) {
        if (objects == null)
            return null;

        ReviewListDto dto = new ReviewListDto();
        dto.setReview((ReviewDao) objects[0]);
        dto.setPeerUsr(usrService.convertUsrDaoToUsrMiniDto((UsrDao) objects[1]));
        return dto;
    }
}
