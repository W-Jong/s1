package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "banner", schema = "selluv")
public class BannerDao {
    private int bannerUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("분류  1-공지사항 2-테마 3-이벤트")
    private int type;
    @ApiModelProperty("분류에 따르는 타겟 UID")
    private int targetUid;
    @ApiModelProperty("배너타이틀")
    private String title;
    @ApiModelProperty("배너이미지 시간")
    private String profileImg;
    @ApiModelProperty("시작 시간")
    private Timestamp startTime;
    @ApiModelProperty("종료 시간")
    private Timestamp endTime;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "banner_uid", nullable = false)
    public int getBannerUid() {
        return bannerUid;
    }

    public void setBannerUid(int bannerUid) {
        this.bannerUid = bannerUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "target_uid", nullable = false)
    public int getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(int targetUid) {
        this.targetUid = targetUid;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = false)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BannerDao bannerDao = (BannerDao) o;
        return bannerUid == bannerDao.bannerUid &&
                type == bannerDao.type &&
                targetUid == bannerDao.targetUid &&
                status == bannerDao.status &&
                Objects.equals(regTime, bannerDao.regTime) &&
                Objects.equals(title, bannerDao.title) &&
                Objects.equals(profileImg, bannerDao.profileImg) &&
                Objects.equals(startTime, bannerDao.startTime) &&
                Objects.equals(endTime, bannerDao.endTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bannerUid, regTime, type, targetUid, title, profileImg, startTime, endTime, status);
    }
}
