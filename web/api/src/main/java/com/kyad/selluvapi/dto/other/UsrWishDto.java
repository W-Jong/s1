package com.kyad.selluvapi.dto.other;

import com.kyad.selluvapi.dao.UsrWishDao;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.category.CategoryMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UsrWishDto {
    @ApiModelProperty("찾는 제품정보")
    private UsrWishDao usrWish;
    @ApiModelProperty("브랜드")
    private BrandMiniDto brand;
    @ApiModelProperty("카테고리")
    private CategoryMiniDto category;
}
