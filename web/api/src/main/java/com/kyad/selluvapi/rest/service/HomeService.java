package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluvapi.jpa.PdtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Service
public class HomeService {

    @Autowired
    private PdtService pdtService;

    @Autowired
    private PdtRepository pdtRepository;

    @Autowired
    private UsrFeedService usrFeedService;

    public Page<PdtListDto> getFamePdtList(Pageable pageable, int usrUid, long seed, int likeGroup) {
        return pdtRepository.findAllFameList(pageable, usrUid, seed, likeGroup)
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> getNewPdtList(Pageable pageable, int usrUid, int likeGroup) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.DATE, -7);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Timestamp aWeekAgo = new Timestamp(cal.getTime().getTime());

        return pdtRepository.findAllNewList(pageable, usrUid, aWeekAgo, likeGroup)
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<UsrFeedPdtListDto> getUsrFeedPdtList(Pageable pageable, int usrUid) {
        return usrFeedService.findAllFeedList(pageable, usrUid);
    }

    public Page<PdtListDto> getFameStylePdtList(Pageable pageable, int usrUid, long seed, int likeGroup) {
        return pdtRepository.findAllFameStyleList(pageable, usrUid, seed, likeGroup)
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> getNewPdtStyleList(Pageable pageable, int usrUid, int likeGroup) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.DATE, -7);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Timestamp aWeekAgo = new Timestamp(cal.getTime().getTime());

        return pdtRepository.findAllNewStyleList(pageable, usrUid, aWeekAgo, likeGroup)
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<UsrFeedPdtListDto> getUsrFeedPdtStyleList(Pageable pageable, int usrUid) {
        return usrFeedService.findAllFeedStyleList(pageable, usrUid);
    }
}
