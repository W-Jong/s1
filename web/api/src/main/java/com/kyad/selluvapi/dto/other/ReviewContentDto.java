package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class ReviewContentDto {
    @ApiModelProperty("후기내용")
    private Page<ReviewListDto> reviews;
    @ApiModelProperty("만족갯수")
    private long countTwoPoint;
    @ApiModelProperty("보통갯수")
    private long countOnePoint;
    @ApiModelProperty("불만족갯수")
    private long countZeroPoint;
}
