package com.kyad.selluvapi.dto.usr;

import com.kyad.selluvapi.dto.pdt.PdtMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UsrRecommendDto {
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("아이디")
    private String usrId;
    @ApiModelProperty("닉네임")
    private String usrNckNm;
    @ApiModelProperty("프로필이미지")
    private String profileImg;
    @ApiModelProperty("아이템수")
    private long pdtCount;
    @ApiModelProperty("팔로워수")
    private long usrFollowerCount;
    @ApiModelProperty("회원 팔로우 여부")
    private boolean usrLikeStatus;
    @ApiModelProperty("인기상품목록")
    private List<PdtMiniDto> pdtList;
}
