package com.kyad.selluvapi.dto.usr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UsrFollowListDto {
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("아이디")
    private String usrId;
    @ApiModelProperty("닉네임")
    private String usrNckNm;
    @ApiModelProperty("프로필이미지")
    private String profileImg;
    @ApiModelProperty("회원 팔로우 여부")
    private boolean usrLikeStatus;
}
