package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "usr_card", schema = "selluv")
public class UsrCardDao {
    private int usrCardUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("카드종류 1-개인 2-법인")
    private int kind;
    @ApiModelProperty("카드번호")
    private String cardNumber;
    @ApiModelProperty("유효기간")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate validDate;
    @ApiModelProperty("유효번호 개인인 경우 생년월일(YYMMDD), 법인일 경우 사업자번호")
    private String validNum;
    @ApiModelProperty("비밀번호")
    private String password;
    @ApiModelProperty("카드이름")
    private String cardName = "";
    @JsonIgnore
    private int iamportValidYn = 1;
    @JsonIgnore
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_card_uid", nullable = false)
    public int getUsrCardUid() {
        return usrCardUid;
    }

    public void setUsrCardUid(int usrCardUid) {
        this.usrCardUid = usrCardUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "card_number", nullable = false, length = 20)
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "valid_date", nullable = false)
    public LocalDate getValidDate() {
        return validDate;
    }

    public void setValidDate(LocalDate validDate) {
        this.validDate = validDate;
    }

    @Basic
    @Column(name = "valid_num", nullable = false, length = 20)
    public String getValidNum() {
        return validNum;
    }

    public void setValidNum(String validNum) {
        this.validNum = validNum;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 20)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "card_name", nullable = false, length = 30)
    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    @Basic
    @Column(name = "iamport_valid_yn", nullable = false)
    public int getIamportValidYn() {
        return iamportValidYn;
    }

    public void setIamportValidYn(int iamportValidYn) {
        this.iamportValidYn = iamportValidYn;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrCardDao that = (UsrCardDao) o;
        return usrCardUid == that.usrCardUid &&
                usrUid == that.usrUid &&
                kind == that.kind &&
                iamportValidYn == that.iamportValidYn &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(cardNumber, that.cardNumber) &&
                Objects.equals(validDate, that.validDate) &&
                Objects.equals(validNum, that.validNum) &&
                Objects.equals(password, that.password) &&
                Objects.equals(cardName, that.cardName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrCardUid, regTime, usrUid, kind, cardNumber, validDate, validNum, password, cardName, iamportValidYn, status);
    }
}
