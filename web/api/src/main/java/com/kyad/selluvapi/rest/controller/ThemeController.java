package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.ThemeDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.theme.ThemeListDto;
import com.kyad.selluvapi.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.ThemeService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "/theme", description = "테마관리")
@RequestMapping("/theme")
public class ThemeController {

    private final Logger LOG = LoggerFactory.getLogger(ThemeController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private ThemeService themeService;

    @ApiOperation(value = "테마 목록얻기",
            notes = "남성, 여성, 키즈 상품군에 따르는 테마 목록을 얻는다.")
    @GetMapping(value = "/recommended")
    public GenericResponse<List<ThemeListDto>> getRecommendedTheme(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("상품군 MALE-남성, FEMALE-여성, KIDS-키즈")
            @RequestParam(value = "pdtGroup") PDT_GROUP_TYPE pdtGroup) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        List<ThemeDao> themeDaos = themeService.getActiveList(pdtGroup.getCode());
        if (themeDaos == null)
            return new GenericResponse<>(ResponseMeta.OK, null);

        List<ThemeListDto> themeListDtos = new ArrayList<>();
        for (ThemeDao theme : themeDaos) {
            ThemeListDto dto = new ThemeListDto();
            theme.setProfileImg(StarmanHelper.getTargetImageUrl("theme", theme.getThemeUid(), theme.getProfileImg()));
            dto.setTheme(theme);
            dto.setPdtCount(themeService.countPdtByThemeUid(theme.getThemeUid()));
            dto.setPdtList(themeService.getRecommendedPdtListByThemeUid(theme.getThemeUid(), usr.getUsrUid()));

            themeListDtos.add(dto);
        }

        return new GenericResponse<>(ResponseMeta.OK, themeListDtos);
    }

    @ApiOperation(value = "테마 상세",
            notes = "테마상세정보 및 테마에 포함된 모든 상품리스트를 얻는다.")
    @GetMapping(value = "/{themeUid}")
    public GenericResponse<ThemeListDto> getThemeDetail(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "themeUid") int themeUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        ThemeDao theme = themeService.getThemeInfo(themeUid);

        ThemeListDto dto = new ThemeListDto();
        theme.setProfileImg(StarmanHelper.getTargetImageUrl("theme", theme.getThemeUid(), theme.getProfileImg()));
        dto.setTheme(theme);

        List<PdtListDto> pdtList =  themeService.getAllPdtListByThemeUid(theme.getThemeUid(), usr.getUsrUid());
        dto.setPdtList(pdtList);
        dto.setPdtCount((pdtList == null) ? 0 : pdtList.size());

        return new GenericResponse<>(ResponseMeta.OK, dto);
    }
}
