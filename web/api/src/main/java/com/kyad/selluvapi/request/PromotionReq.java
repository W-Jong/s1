package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.DEAL_TYPE;
import com.kyad.selluvapi.enumtype.PDT_GROUP_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class PromotionReq {

    @NotEmpty
    @ApiModelProperty("프로모션코드")
    private String promotionCode;

    @NotNull
    @ApiModelProperty("코드분류(구매용, 판매용)")
    private DEAL_TYPE dealType;

    @ApiModelProperty("브랜드UID, 정해지지 않은 경우 0")
    @Min(0)
    private int brandUid;

    @ApiModelProperty("카테고리UID, 정해지지 않은 경우 0")
    @Min(0)
    private int categoryUid;

    @ApiModelProperty("상품UID, 정해지지 않은 경우 0")
    @Min(0)
    private int pdtUid;
}
