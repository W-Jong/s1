package com.kyad.selluvapi.helper.iamport.response;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.rest.common.serializer.GsonUnixTimestampDeserializer;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Data
public class PaymentAnnotation {
    @SerializedName("imp_uid")
    String imp_uid;

    @SerializedName("merchant_uid")
    String merchant_uid;

    @SerializedName("pay_method")
    String pay_method;

    @SerializedName("channel")
    String channel;

    @SerializedName("pg_provider")
    String pg_provider;

    @SerializedName("pg_tid")
    String pg_tid;

    @SerializedName("escrow")
    boolean escrow;

    @SerializedName("apply_num")
    String apply_num;

    @SerializedName("card_name")
    String card_name;

    @SerializedName("card_quota")
    int card_quota;

    @SerializedName("vbank_code")
    String vbank_code;

    @SerializedName("vbank_name")
    String vbank_name;

    @SerializedName("vbank_num")
    String vbank_num;

    @SerializedName("vbank_holder")
    String vbank_holder;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("vbank_date")
    Timestamp vbank_date;

    @SerializedName("name")
    String name;

    @SerializedName("amount")
    BigDecimal amount;

    @SerializedName("cancel_amount")
    BigDecimal cancel_amount;

    @SerializedName("currency")
    String currency;

    @SerializedName("buyer_name")
    String buyer_name;

    @SerializedName("buyer_email")
    String buyer_email;

    @SerializedName("buyer_tel")
    String buyer_tel;

    @SerializedName("buyer_addr")
    String buyer_addr;

    @SerializedName("buyer_postcode")
    String buyer_postcode;

    @SerializedName("custom_data")
    String custom_data;

    @SerializedName("user_agent")
    String user_agent;

    @SerializedName("status")
    String status;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("paid_at")
    Timestamp paid_at;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("failed_at")
    Timestamp failed_at;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("cancelled_at")
    Timestamp cancelled_at;

    @SerializedName("fail_reason")
    String fail_reason;

    @SerializedName("cancel_reason")
    String cancel_reason;

    @SerializedName("receipt_url")
    String receipt_url;

    @SerializedName("cancel_history")
    List<PaymentCancelAnnotation> cancel_history;

    @SerializedName("cash_receipt_issued")
    boolean cash_receipt_issued;
}
