package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
public class DealDeliveryReq {
    @NotNull
    @ApiModelProperty("배송유형 1-일반주문, 2-정품인증, 3-반품배송")
    private int deliveryType = 1;
    @NotEmpty
    @ApiModelProperty("성함")
    private String recipientNm = "";
    @NotEmpty
    @ApiModelProperty("연락처")
    private String recipientPhone = "";
    @NotEmpty
    @ApiModelProperty("주소")
    private String recipientAddress = "";
    @NotEmpty
    @ApiModelProperty("상세주소 (우편번호)")
    private String recipientAddressDetail = "";
}
