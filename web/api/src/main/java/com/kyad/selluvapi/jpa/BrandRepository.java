package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.BrandDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends CrudRepository<BrandDao, Integer> {

    @Query("select a from BrandDao a")
    Page<BrandDao> findAll(Pageable pageable);

    @Query(value = "SELECT b.* FROM recom_brand rb JOIN brand b ON rb.brand_uid = b.brand_uid " +
            " WHERE rb.pdt_group = ?1 AND rb.status = 1 ORDER BY rb.brand_order ASC LIMIT 10", nativeQuery = true)
    List<BrandDao> findRecommendBrandByPdtGroup(int pdtGroup);

    List<BrandDao> findAllByStatus(int status);

    @Query(value = "SELECT b.* FROM usr_brand_like ubl JOIN brand b ON ubl.brand_uid = b.brand_uid AND b.status = 1 " +
            " WHERE ubl.usr_uid = ?1 ORDER BY  ubl.reg_time ASC", nativeQuery = true)
    List<BrandDao> findAllFollowListByUsrUid(int usrUid);

    @Query(value = "SELECT b.brand_uid, b.first_en, b.first_ko, b.name_en, b.name_ko, b.logo_img, b.profile_img, b.back_img, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = b.brand_uid AND ubl.status = 1) AS like_count, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = b.brand_uid AND ubl.usr_uid = ?1 AND ubl.status = 1) AS like_status, " +
            " (SELECT count(*) FROM pdt p WHERE p.brand_uid = b.brand_uid AND p.status = 1 AND p.pdt_group = ?2 AND p.photoshop_yn = 1) AS pdt_group_count " +
            " FROM brand b WHERE b.status = 1 HAVING pdt_group_count > 0 " +
            " ORDER BY pdt_group_count DESC LIMIT 10", nativeQuery = true)
    List<Object> findFameBrandByPdtGroup(int usrUid, int pdtGroup);

    @Query(value = "SELECT b.brand_uid, b.first_en, b.first_ko, b.name_en, b.name_ko, b.logo_img, b.profile_img, b.back_img, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = b.brand_uid AND ubl.status = 1) AS like_count, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = b.brand_uid AND ubl.usr_uid = ?1 AND ubl.status = 1) AS like_status, " +
            " (SELECT count(*) FROM pdt p WHERE p.brand_uid = b.brand_uid AND p.status = 1 AND p.pdt_group = ?2 AND p.photoshop_yn = 1) AS pdt_group_count " +
            " FROM brand b WHERE b.status = 1 " +
            " ORDER BY pdt_group_count DESC", nativeQuery = true)
    List<Object> findFameAllBrandByPdtGroup(int usrUid, int pdtGroup);

    @Query(value = "SELECT b.brand_uid, b.first_en, b.first_ko, b.name_en, b.name_ko, b.logo_img, b.profile_img, b.back_img, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = b.brand_uid AND ubl.status = 1) AS like_count, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = b.brand_uid AND ubl.usr_uid = ?1 AND ubl.status = 1) AS like_status " +
            " FROM brand b WHERE b.status = 1 ORDER BY if(?2 = 1, name_en, name_ko) ", nativeQuery = true)
    List<Object> findAllDetailList(int usrUid, int orderColumn);

    @Query(value = "SELECT b.brand_uid, b.first_en, b.first_ko, b.name_en, b.name_ko, b.license_url, b.logo_img, b.profile_img, b.back_img, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = ?1 AND ubl.status = 1) AS like_count, " +
            " (SELECT count(*) FROM usr_brand_like ubl WHERE ubl.brand_uid = ?1 AND ubl.usr_uid = ?2 AND ubl.status = 1) AS like_status, " +
            " (SELECT count(*) FROM pdt p1 WHERE p1.brand_uid = ?1 AND p1.pdt_group = 1 AND p1.photoshop_yn = 1 AND p1.status = 1) AS male_pdt_count, " +
            " (SELECT count(*) FROM pdt p2 WHERE p2.brand_uid = ?1 AND p2.pdt_group = 2 AND p2.photoshop_yn = 1 AND p2.status = 1) AS female_pdt_count, " +
            " (SELECT count(*) FROM pdt p3 WHERE p3.brand_uid = ?1 AND p3.pdt_group = 4 AND p3.photoshop_yn = 1 AND p3.status = 1) AS kids_pdt_count, " +
            " (SELECT count(*) FROM pdt_style ps JOIN pdt p4 ON ps.pdt_uid = p4.pdt_uid WHERE ps.status = 1 AND p4.status = 1 AND p4.photoshop_yn = 1 AND p4.brand_uid = ?1) AS style_pdt_count " +
            " FROM brand b WHERE b.brand_uid = ?1 AND b.status = 1", nativeQuery = true)
    Object findByBrandUid(int brandUid, int usrUid);

    @Query(value = "SELECT count(*) FROM usr_brand_like WHERE brand_uid = ?1 AND status = 1", nativeQuery = true)
    long countBrandLikeByBrandUid(int brandUid);

    @Query(value = "SELECT * FROM brand b WHERE (b.name_ko LIKE %?1% OR b.name_en LIKE %?1%) AND b.status = 1 ORDER BY name_ko DESC", nativeQuery = true)
    List<BrandDao> getByKeyword(String keyword);
}
