package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_login_his", schema = "selluv")
public class UsrLoginHisDao {
    private long usrLoginHisUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int osTp;
    private int loginTp;
    private String connAddr;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_login_his_uid", nullable = false)
    public long getUsrLoginHisUid() {
        return usrLoginHisUid;
    }

    public void setUsrLoginHisUid(long usrLoginHisUid) {
        this.usrLoginHisUid = usrLoginHisUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "os_tp", nullable = false)
    public int getOsTp() {
        return osTp;
    }

    public void setOsTp(int osTp) {
        this.osTp = osTp;
    }

    @Basic
    @Column(name = "login_tp", nullable = false)
    public int getLoginTp() {
        return loginTp;
    }

    public void setLoginTp(int loginTp) {
        this.loginTp = loginTp;
    }

    @Basic
    @Column(name = "conn_addr", nullable = false, length = 60)
    public String getConnAddr() {
        return connAddr;
    }

    public void setConnAddr(String connAddr) {
        this.connAddr = connAddr;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrLoginHisDao that = (UsrLoginHisDao) o;
        return usrLoginHisUid == that.usrLoginHisUid &&
                usrUid == that.usrUid &&
                osTp == that.osTp &&
                loginTp == that.loginTp &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(connAddr, that.connAddr);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrLoginHisUid, regTime, usrUid, osTp, loginTp, connAddr, status);
    }
}
