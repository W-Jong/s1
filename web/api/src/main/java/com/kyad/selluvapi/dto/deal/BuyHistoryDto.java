package com.kyad.selluvapi.dto.deal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class BuyHistoryDto {
    @ApiModelProperty("네고제안수")
    private long negoCount;
    @ApiModelProperty("거래완료수")
    private long completedCount;
    @ApiModelProperty("배송준비수")
    private long preparedCount;
    @ApiModelProperty("정품인증수")
    private long verifiedCount;
    @ApiModelProperty("배송진행수")
    private long progressCount;
    @ApiModelProperty("배송완료수")
    private long sentCount;
    @ApiModelProperty("구매내역")
    private Page<DealListDto> history;
}
