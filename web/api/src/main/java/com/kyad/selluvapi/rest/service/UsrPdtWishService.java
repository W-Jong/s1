package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrPdtWishDao;
import com.kyad.selluvapi.jpa.UsrPdtWishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UsrPdtWishService {

    @Autowired
    private UsrPdtWishRepository usrPdtWishRepository;

    public Page<UsrPdtWishDao> findAll(Pageable pageable) {
        return usrPdtWishRepository.findAll(pageable);
    }

    public UsrPdtWishDao getByUsrUidAndPdtUid(int usrUid, int pdtUid) {
        return usrPdtWishRepository.getByUsrUidAndPdtUid(usrUid, pdtUid);
    }

    public long countByPdtUid(int pdtUid) {
        return usrPdtWishRepository.countByPdtUid(pdtUid);
    }

    public UsrPdtWishDao save(UsrPdtWishDao dao) {
        return usrPdtWishRepository.save(dao);
    }

    public void delete(int usrPdtWishUid) {
        usrPdtWishRepository.delete(usrPdtWishUid);
    }
}
