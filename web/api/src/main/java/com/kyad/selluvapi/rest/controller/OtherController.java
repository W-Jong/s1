package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.other.*;
import com.kyad.selluvapi.enumtype.FAQ_TYPE;
import com.kyad.selluvapi.enumtype.LICENSE_TYPE;
import com.kyad.selluvapi.helper.DateCalculatorHelper;
import com.kyad.selluvapi.helper.LocalDateHelper;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.request.*;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@Api(value = "/other", description = "기타 관리")
@RequestMapping("/other")
public class OtherController {

    private final Logger LOG = LoggerFactory.getLogger(OtherController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private OtherService otherService;

    @Autowired
    private PdtService pdtService;

    @ApiOperation(value = "배너목록 얻기",
            notes = "활성중인 배너목록을 얻는다.")
    @GetMapping(value = "/banner")
    public GenericResponse<List<BannerDto>> getBannerList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        List<BannerDto> bannerDtoList = new ArrayList<>();
        List<BannerDao> bannerList = otherService.getBannerActiveList();
        for (BannerDao banner : bannerList) {
            BannerDto bannerDto = new BannerDto();
            banner.setProfileImg(StarmanHelper.getTargetImageUrl("banner", banner.getBannerUid(), banner.getProfileImg()));
            bannerDto.setBanner(banner);
            if (banner.getType() == 1) { //공지사항
                bannerDto.setTarget(otherService.getNoticeByNoticeUid(banner.getTargetUid()));
            } else if (banner.getType() == 3) { // 이벤트
                bannerDto.setTarget(otherService.getEventByEventUid(banner.getTargetUid()));
            } else { //테마
                bannerDto.setTarget(null);
            }

            bannerDtoList.add(bannerDto);
        }
        return new GenericResponse<>(ResponseMeta.OK, bannerDtoList);
    }

    @ApiOperation(value = "약관얻기",
            notes = "약관타입에 따르는 약관상세정보를 얻는다.")
    @GetMapping(value = "/license")
    public GenericResponse<LicenseDao> getLicense(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestParam(value = "licenseType") LICENSE_TYPE licenseType) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        LicenseDao license = otherService.getLicense(licenseType.getCode());
        return new GenericResponse<>(ResponseMeta.OK, license);
    }

    @ApiOperation(value = "FAQ목록 얻기",
            notes = "FAQ타입에 따르는 FAQ목록을 얻는다.")
    @GetMapping(value = "/faq")
    public GenericResponse<List<FaqDao>> getFaqList(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestParam(value = "faqType") FAQ_TYPE faqType) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, otherService.getFaqListByType(faqType.getCode()));
    }

    @ApiOperation(value = "공지사항 목록 얻기",
            notes = "공지사항 목록을 얻는다.")
    @GetMapping(value = "/notice")
    public GenericResponse<List<NoticeDao>> getNoticeList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        usr.setNoticeTime(new Timestamp(System.currentTimeMillis()));
        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, otherService.getNoticeActiveList());
    }

    @ApiOperation(value = "공지상세 얻기",
            notes = "공지사항 상세정보를 얻는다.")
    @GetMapping(value = "/notice/{noticeUid}")
    public GenericResponse<NoticeDao> getNoticeList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "noticeUid") int noticeUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        NoticeDao noticeDao = otherService.getNoticeByNoticeUid(noticeUid);

        if (noticeDao == null || noticeDao.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, noticeDao);
    }

    @ApiOperation(value = "이벤트 목록 얻기",
            notes = "이벤트 목록을 얻는다.")
    @GetMapping(value = "/event")
    public GenericResponse<List<EventDao>> getEventList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        List<EventDao> eventDaoList = otherService.getEventActiveList();
        for (EventDao event : eventDaoList) {
            event.setProfileImg(StarmanHelper.getTargetImageUrl("event", event.getEventUid(), event.getProfileImg()));
            event.setDetailImg(StarmanHelper.getTargetImageUrl("event", event.getEventUid(), event.getDetailImg()));
        }

        return new GenericResponse<>(ResponseMeta.OK, eventDaoList);
    }

    @ApiOperation(value = "이벤트 상세 얻기",
            notes = "이벤트 상세정보를 얻는다.")
    @GetMapping(value = "/event/{eventUid}")
    public GenericResponse<EventDao> getEventList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "eventUid") int eventUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        EventDao eventDao = otherService.getEventByEventUid(eventUid);
        if (eventDao == null || eventDao.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);
        }

        eventDao.setProfileImg(StarmanHelper.getTargetImageUrl("event", eventDao.getEventUid(), eventDao.getProfileImg()));
        eventDao.setDetailImg(StarmanHelper.getTargetImageUrl("event", eventDao.getEventUid(), eventDao.getDetailImg()));

        return new GenericResponse<>(ResponseMeta.OK, eventDao);
    }

    @ApiOperation(value = "1:1문의목록 얻기",
            notes = "해당 유저가 작성한 1:1문의 목록을 얻는다")
    @GetMapping(value = "/qna")
    public GenericResponse<List<QnaDao>> getQnaList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, otherService.getQnaListByUsrUid(usr.getUsrUid()));
    }

    @ApiOperation(value = "1:1문의 작성",
            notes = "1:1 문의를 작성한다.")
    @PutMapping(value = "/qna")
    public GenericResponse<QnaDao> putQna(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid QnaReq qnaReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        QnaDao qna = new QnaDao();
        qna.setUsrUid(usr.getUsrUid());
        qna.setTitle(qnaReq.getTitle());
        qna.setContent(qnaReq.getContent());

        otherService.saveQna(qna);

        return new GenericResponse<>(ResponseMeta.OK, qna);
    }

    @ApiOperation(value = "프로모션정보 얻기",
            notes = "프로모션코드로부터 프로모션관련 정보를 얻는다.")
    @PostMapping(value = "/promotion")
    public GenericResponse<PromotionCodeDao> getPromotion(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("프로모션코드 및 상품정보")
            @RequestBody @Valid PromotionReq promotionReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PromotionCodeDao codeDao = otherService.getPromotionInfo(promotionReq, usr.getUsrUid());

        if (codeDao == null)
            return new GenericResponse<>(ResponseMeta.PROMOTION_NOT_EXIST, null);

        return new GenericResponse<>(ResponseMeta.OK, codeDao);
    }

    @ApiOperation(value = "신고하기",
            notes = "상품 또는 유저를 신고한다.")
    @PutMapping(value = "/report")
    public GenericResponse<Void> putReport(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid ReportReq reportReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        ReportDao report = new ReportDao();
        report.setUsrUid(usr.getUsrUid());
        report.setType(reportReq.getReportType().getCode());
        report.setContent(reportReq.getContent());

        if (reportReq.getKind() == 1) { //회원신고

            UsrDao peerUsr = usrService.getByUsrUid(reportReq.getTargetUid());
            if (peerUsr == null || peerUsr.getStatus() != 1) {
                return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
            }
            report.setPeerUsrUid(peerUsr.getUsrUid());
            report.setPdtUid(0);
        } else { //상품신고
            PdtDao pdt = pdtService.getPdtInfo(reportReq.getTargetUid());

            report.setPeerUsrUid(pdt.getUsrUid());
            report.setPdtUid(pdt.getPdtUid());
        }

        if (report.getUsrUid() == report.getPeerUsrUid()) {
            return new GenericResponse<>(ResponseMeta.REPORT_NOT_ALLOW_ITSELF, null);
        }

        otherService.saveReport(report);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "찾는 상품 알람설정",
            notes = "찾는 상품을 새로 등록한다.")
    @PutMapping(value = "/usrWish")
    public GenericResponse<UsrWishDao> insertUsrWish(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserWishReq userWishReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrWishDao usrWish = new UsrWishDao();
        usrWish.setUsrUid(usr.getUsrUid());
        usrWish.setBrandUid(userWishReq.getBrandUid());
        usrWish.setPdtGroup(userWishReq.getPdtGroupType().getCode());
        usrWish.setCategoryUid(userWishReq.getCategoryUid());
        usrWish.setPdtModel(userWishReq.getPdtModel());
        usrWish.setPdtSize(userWishReq.getPdtSize());
        usrWish.setColorName(userWishReq.getColorName());

        otherService.saveUsrWish(usrWish);

        return new GenericResponse<>(ResponseMeta.OK, usrWish);
    }

    @ApiOperation(value = "찾는 아이템 목록얻기",
            notes = "찾는 아이템 목록을 얻는다.")
    @GetMapping(value = "/usrWish")
    public GenericResponse<Page<UsrWishDto>> getUsrWishList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, otherService.getUsrWishList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));
    }

    @ApiOperation(value = "찾고 있는 아이템 멀티삭제",
            notes = "찾고있는 아이템 목록에서 아이템을 멀티로 삭제한다.")
    @DeleteMapping(value = "/usrWishs")
    public GenericResponse<Void> deleteUsrWishs(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid MultiUidsReq multiUidsRequest) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        for (int usrWishUid : multiUidsRequest.getUids()) {
            try {
                otherService.deleteUsrWish(usrWishUid);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "시스템설정정보 얻기",
            notes = "시스템 설정정보를 얻는다.")
    @GetMapping(value = "/setting")
    public GenericResponse<SystemSettingDto> getSystemSettingDto(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        SystemSettingDto systemSetting = otherService.getSystemSettingDto();

        return new GenericResponse<>(ResponseMeta.OK, systemSetting);
    }

    @ApiOperation(value = "발렛신청",
            notes = "셀럽에 발렛을 신청한다.")
    @PutMapping(value = "/valet")
    public GenericResponse<Void> putValet(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid ValetCreateReq createReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        BrandDao brandDao = brandService.getBrandInfo(createReq.getBrandUid());

        List<CategoryDao> categoryDaoList = categoryService.getAllHierarchyCategories(createReq.getCategoryUid());

        if (categoryDaoList == null || categoryDaoList.size() < 2) {
            return new GenericResponse<>(ResponseMeta.BRAND_NOT_EXIST, null);
        }

        ValetDao valet = new ValetDao();
        valet.setReqPrice(createReq.getReqPrice());
        valet.setReqName(createReq.getReqName());
        valet.setReqPhone(createReq.getReqPhone());
        valet.setReqAddress(createReq.getReqAddress());
        valet.setSendType(createReq.getSendType());
        valet.setSignImg(createReq.getSignImg());
        valet.setMemo("<<<<<<<<신청정보>>>>>>>>\n신청브랜드: " + brandDao.getNameKo() + "(" + brandDao.getNameEn() + ") \n" +
                "신청카테고리: " + categoryDaoList.get(0).getCategoryName() + " > " +
                categoryDaoList.get(1).getCategoryName() +
                ((categoryDaoList.size() > 2) ? " > " + categoryDaoList.get(2).getCategoryName() : "") +
                ((categoryDaoList.size() > 3) ? " > " + categoryDaoList.get(3).getCategoryName() : "") + "\n");

        otherService.saveValet(valet);

        if (!"".equals(valet.getSignImg())) {
            StarmanHelper.moveTargetFolder("valet", valet.getValetUid(), valet.getSignImg());
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "영업일기준 날짜얻기",
            notes = "기준날짜로부터 영업일기준 타겟 날짜를 얻는다.")
    @PostMapping(value = "/delayWorkingDay")
    public GenericResponse<WorkingDayDto> postDelayWorkingDay(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid WorkingDayReq workingDayReq) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        DateCalculatorHelper calculatorHelper = new DateCalculatorHelper();
        Date targetDate = calculatorHelper.getDelayedWorkingDate(workingDayReq.getDeltaDays(),
                LocalDateHelper.asDate(workingDayReq.getBasisDate()));

        WorkingDayDto workingDayDto = new WorkingDayDto();
        workingDayDto.setTargetDate(LocalDateHelper.asLocalDate(targetDate));

        return new GenericResponse<>(ResponseMeta.OK, workingDayDto);
    }

    @ApiOperation(value = "택배사목록얻기",
            notes = "택배사 목록을 얻는다.")
    @GetMapping(value = "/deliveryCompanies")
    public GenericResponse<DeliveryCompaniesDto> getDeliveryCompanies(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        String[] deliveryCompanies = {
                "우체국택배",
                "CJ대한통운",
                "한진택배",
                "롯데택배",
                "로젠택배",
                "KGB택배",
                "KG로지스",
                "GTX로지스",
                "포스트박스",
                "SC로지스",
                "경동합동택배",
                "아주택배",
                "고려택배",
                "하나로택배",
                "대신택배",
                "천일택배",
                "건영택배",
                "한의사랑택배",
                "용마로지스",
                "일양로지스",
        };

        DeliveryCompaniesDto deliveryCompaniesDto = new DeliveryCompaniesDto();
        deliveryCompaniesDto.setCompanies(Arrays.asList(deliveryCompanies));

        return new GenericResponse<>(ResponseMeta.OK, deliveryCompaniesDto);
    }
}
