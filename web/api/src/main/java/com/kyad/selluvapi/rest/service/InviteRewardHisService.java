package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.enumtype.MONEY_HIS_TYPE;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.enumtype.SYSTEM_SETTING_TYPE;
import com.kyad.selluvapi.jpa.InviteRewardHisRepository;
import com.kyad.selluvapi.jpa.MoneyChangeHisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Service
public class InviteRewardHisService {

    @Autowired
    private InviteRewardHisRepository inviteRewardHisRepository;

    @Autowired
    private MoneyChangeHisRepository moneyChangeHisRepository;

    @Autowired
    private OtherService otherService;

    @Autowired
    private UsrService usrService;

    @Autowired
    private DealService dealService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @Autowired
    private EntityManager entityManager;

    public long countInvitedUserByUsrUid(int usrUid) {
        return inviteRewardHisRepository.countAllByUsrUidAndKind(usrUid, 1);
    }

    public long countBoughtInvitedUserByUsrUid(int usrUid) {
        return inviteRewardHisRepository.countAllByUsrUidAndKind(usrUid, 3);
    }

    public long countInvitedUserByUsrUidThisMonth(int usrUid) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.set(Calendar.DATE, 1);
        return inviteRewardHisRepository.countAllByUsrUidAndKindAndRegTimeGreaterThanEqual(usrUid, 1, new Timestamp(cal.getTime().getTime()));
    }

    @Transactional
    public void addJoinedReward(UsrDao usr, UsrDao inviteUsr) {
        //초대자
        SystemSettingDao systemSetting1 = otherService.getSystemSetting(SYSTEM_SETTING_TYPE.INVITER_REWRAD.getCode());
        long rewardInviter = (systemSetting1 == null) ? 0 : Long.parseLong(systemSetting1.getSetting());
        InviteRewardHisDao inviteRewardHis1 = new InviteRewardHisDao();
        inviteRewardHis1.setUsrUid(inviteUsr.getUsrUid());
        inviteRewardHis1.setInviteUsrUid(inviteUsr.getUsrUid());
        inviteRewardHis1.setJoinUsrUid(usr.getUsrUid());
        inviteRewardHis1.setKind(1);//초대자에게 초대리워드
        inviteRewardHis1.setDealUid(0);
        inviteRewardHis1.setRewardPrice(rewardInviter);
        inviteRewardHisRepository.save(inviteRewardHis1);

        MoneyChangeHisDao moneyChangeHisDao1 = new MoneyChangeHisDao();
        moneyChangeHisDao1.setUsrUid(inviteUsr.getUsrUid());
        moneyChangeHisDao1.setKind(MONEY_HIS_TYPE.INVITEREWARD.getCode());
        moneyChangeHisDao1.setTargetUid(usr.getUsrUid());
        moneyChangeHisDao1.setPdtUid("0");
        moneyChangeHisDao1.setContent("초대 리워드 지급");
        moneyChangeHisDao1.setAmount(rewardInviter);
        moneyChangeHisRepository.save(moneyChangeHisDao1);

        String sql = "update usr set money = money + " + rewardInviter + " where usr_uid = " + inviteUsr.getUsrUid();
        entityManager.createNativeQuery(sql).executeUpdate();

        //가입자
        SystemSettingDao systemSetting2 = otherService.getSystemSetting(SYSTEM_SETTING_TYPE.INVITED_USER_REWARD.getCode());
        long rewardJoiner = (systemSetting2 == null) ? 0 : Long.parseLong(systemSetting2.getSetting());
        InviteRewardHisDao inviteRewardHis2 = new InviteRewardHisDao();
        inviteRewardHis2.setUsrUid(usr.getUsrUid());
        inviteRewardHis2.setInviteUsrUid(inviteUsr.getUsrUid());
        inviteRewardHis2.setJoinUsrUid(usr.getUsrUid());
        inviteRewardHis2.setKind(2);//가입자에게 가입리워드
        inviteRewardHis2.setDealUid(0);
        inviteRewardHis2.setRewardPrice(rewardJoiner);
        inviteRewardHisRepository.save(inviteRewardHis2);

        MoneyChangeHisDao moneyChangeHisDao2 = new MoneyChangeHisDao();
        moneyChangeHisDao2.setUsrUid(usr.getUsrUid());
        moneyChangeHisDao2.setKind(MONEY_HIS_TYPE.JOINREWARD.getCode());
        moneyChangeHisDao2.setTargetUid(inviteUsr.getUsrUid());
        moneyChangeHisDao2.setPdtUid("0");
        moneyChangeHisDao2.setContent("가입 리워드 지급");
        moneyChangeHisDao2.setAmount(rewardJoiner);
        moneyChangeHisRepository.save(moneyChangeHisDao2);

        sql = "update usr set money = money + " + rewardJoiner + " where usr_uid = " + usr.getUsrUid();
        entityManager.createNativeQuery(sql).executeUpdate();

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH11_REWARD_INVITE, inviteUsr, usr, null, null, "1", 0);
    }

    @Transactional
    public void addFirstBoughtReward(UsrDao usr, DealDao deal) {
        //첫 구매 리워드
        if (usr.getInviteUsrUid() == 0)
            return;
        if (dealService.countPaymentByUsrUid(usr.getUsrUid()) != 1) {
            return;
        }
        UsrDao inviteUsr = usrService.getByUsrUid(usr.getInviteUsrUid());
        if (inviteUsr == null || inviteUsr.getStatus() != 1) {
            return;
        }

        //초대자
        SystemSettingDao systemSetting = otherService.getSystemSetting(SYSTEM_SETTING_TYPE.INVITED_USER_FIRST_DEAL_REWARD.getCode());
        long rewardInviter = (systemSetting == null) ? 0 : Long.parseLong(systemSetting.getSetting());
        InviteRewardHisDao inviteRewardHis = new InviteRewardHisDao();
        inviteRewardHis.setUsrUid(inviteUsr.getUsrUid());
        inviteRewardHis.setInviteUsrUid(inviteUsr.getUsrUid());
        inviteRewardHis.setJoinUsrUid(usr.getUsrUid());
        inviteRewardHis.setKind(3);//초대자에게 가입자의 첫 구매리워드
        inviteRewardHis.setDealUid(deal.getDealUid());
        inviteRewardHis.setRewardPrice(rewardInviter);
        inviteRewardHisRepository.save(inviteRewardHis);

        MoneyChangeHisDao moneyChangeHisDao = new MoneyChangeHisDao();
        moneyChangeHisDao.setUsrUid(inviteUsr.getUsrUid());
        moneyChangeHisDao.setKind(MONEY_HIS_TYPE.INVITEFIRSTSELL.getCode());
        moneyChangeHisDao.setTargetUid(usr.getUsrUid());
        moneyChangeHisDao.setPdtUid("" + deal.getPdtUid());
        moneyChangeHisDao.setContent("초대한 유저 첫 구매리워드 지급");
        moneyChangeHisDao.setAmount(rewardInviter);
        moneyChangeHisRepository.save(moneyChangeHisDao);

        String sql = "update usr set money = money + " + rewardInviter + " where usr_uid = " + inviteUsr.getUsrUid();
        entityManager.createNativeQuery(sql).executeUpdate();

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH11_REWARD_INVITE, inviteUsr, usr, null, null, "2", 0);
    }
}
