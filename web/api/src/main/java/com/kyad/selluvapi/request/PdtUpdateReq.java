package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluvapi.enumtype.PDT_GROUP_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class PdtUpdateReq {

    @NotNull
    @Size(min = 1)
    @ApiModelProperty("상품이미지 리스트")
    private List<String> photos;

    @Min(50000)
    @ApiModelProperty("가격")
    private long price;

    @Min(0)
    @ApiModelProperty("배송비")
    private long sendPrice;

    @NotEmpty
    @ApiModelProperty("상품사이즈")
    private String pdtSize;

    @NotNull
    @ApiModelProperty("컨디션")
    private PDT_CONDITION_TYPE pdtCondition;

    @NotNull
    @ApiModelProperty("태그")
    private String tag;

    @NotEmpty
    @Pattern(regexp = "^[01]{6}$")
    @ApiModelProperty("부속품 예:110101 6자리문자열(0-없음, 1-있음)  첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백")
    private String component;

    @NotNull
    @ApiModelProperty("기타")
    private String etc;

    @NotNull
    @ApiModelProperty("컬러")
    private String pdtColor;

    @NotNull
    @ApiModelProperty("모델명")
    private String pdtModel;

    @NotNull
    @ApiModelProperty("상세설명")
    private String content;

    @Range(min = 1, max = 2)
    @ApiModelProperty("네고허용여부 1-네고허용 2-네고 받지 않음")
    private int negoYn;

    @NotNull
    @ApiModelProperty("프로모션코드")
    private String promotionCode;

    @NotNull
    @ApiModelProperty("판매여부")
    private boolean isSell;

    public boolean getIsSell() {
        return this.isSell;
    }

}
