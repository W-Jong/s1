package com.kyad.selluvapi.dto.usr;

import com.kyad.selluvapi.dao.UsrFeedDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UsrFeedPdtListDto extends PdtListDto {
    @ApiModelProperty("피드데이터")
    UsrFeedDao usrFeed;
    @ApiModelProperty("팔로우유저")
    UsrMiniDto peerUsr;
}
