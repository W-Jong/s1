package com.kyad.selluvapi.helper.iamport.request;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class EscrowLogisPeopleAnnotation {
    @SerializedName("name")
    private String name;

    @SerializedName("tel")
    private String tel;

    @SerializedName("addr")
    private String addr;

    @SerializedName("postcode")
    private String postcode;
}