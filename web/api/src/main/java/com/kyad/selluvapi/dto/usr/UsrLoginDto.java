package com.kyad.selluvapi.dto.usr;

import com.kyad.selluvapi.dao.UsrAddressDao;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UsrLoginDto {
    @ApiModelProperty("액세스 토큰")
    String accessToken;
    @ApiModelProperty("회원정보")
    UsrDetailDto usr;
    @ApiModelProperty("주소정보")
    private List<UsrAddressDao> addressList;
}
