package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.other.SystemSettingDto;
import com.kyad.selluvapi.dto.other.UsrWishDto;
import com.kyad.selluvapi.helper.StringHelper;
import com.kyad.selluvapi.jpa.*;
import com.kyad.selluvapi.request.PromotionReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class OtherService {

    @Autowired
    private BannerRepository bannerRepository;

    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private PromotionCodeRepository promotionCodeRepository;

    @Autowired
    private FaqRepository faqRepository;

    @Autowired
    private QnaRepository qnaRepository;

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SystemSettingRepository systemSettingRepository;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ShareRepository shareRepository;

    @Autowired
    private UsrWishRepository usrWishRepository;

    @Autowired
    private ValetRepository valetRepository;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    public List<BannerDao> getBannerActiveList() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        return bannerRepository.findAllByStartTimeBeforeAndEndTimeAfterAndStatusOrderByRegTimeDesc(now, now, 1);
    }

    public LicenseDao getLicense(int licenseUid) {
        return licenseRepository.findOne(licenseUid);
    }

    public List<FaqDao> getFaqListByType(int faqType) {
        return faqRepository.findAllByKindAndStatusOrderByRegTimeDesc(faqType, 1);
    }

    public PromotionCodeDao getPromotionInfo(PromotionReq promotionReq, int usrUid) {
        List<PromotionCodeDao> codeDaoList = promotionCodeRepository.findAllByTitle(promotionReq.getPromotionCode());

        if (codeDaoList == null || codeDaoList.size() < 1) {
            return null;
        }

        Timestamp now = new Timestamp(System.currentTimeMillis());
        for (PromotionCodeDao codeDao : codeDaoList) {
            if (codeDao.getKind() != promotionReq.getDealType().getCode() ||
                    codeDao.getEndTime().before(now)) //코드분류나 종료시간이 되었으면 무시함
                continue;

            if (codeDao.getLimitType() == 0 || //제한없음
                    (codeDao.getLimitType() == 4 && //상품제한
                            StringHelper.containSubStringSpiltByComma(codeDao.getLimitUids(), "" + promotionReq.getPdtUid())) ||
                    (codeDao.getLimitType() == 4 && // 회원제한
                            StringHelper.containSubStringSpiltByComma(codeDao.getLimitUids(), "" + usrUid)) ||
                    (codeDao.getLimitType() == 4 && //브랜드제한
                            StringHelper.containSubStringSpiltByComma(codeDao.getLimitUids(), "" + promotionReq.getBrandUid())) ||
                    (codeDao.getLimitType() == 4 && //카테고리제한
                            StringHelper.containSubStringSpiltByComma(codeDao.getLimitUids(), "" + promotionReq.getCategoryUid()))) {
                return codeDao;
            }
        }
        return null;
    }

    public List<QnaDao> getQnaListByUsrUid(int usrUid) {
        return qnaRepository.findAllByUsrUidAndStatusNotOrderByRegTimeDesc(usrUid, 0);
    }

    public QnaDao saveQna(QnaDao dao) {
        return qnaRepository.save(dao);
    }

    public List<NoticeDao> getNoticeActiveList() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        return noticeRepository.findAllByStartTimeBeforeAndEndTimeAfterAndStatusOrderByRegTimeDesc(now, now, 1);
    }

    public NoticeDao getNoticeByNoticeUid(int noticeUid) {
        return noticeRepository.findOne(noticeUid);
    }

    public long countUnReadNotice(Timestamp usrReadTime) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        return noticeRepository.countAllByStartTimeBeforeAndEndTimeAfterAndStatusAndRegTimeAfter(now, now, 1, usrReadTime);
    }

    public List<EventDao> getEventActiveList() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        return eventRepository.findAllByStartTimeBeforeAndEndTimeAfterAndStatusOrderByRegTimeDesc(now, now, 1);
    }

    public EventDao getEventByEventUid(int eventUid) {
        return eventRepository.findOne(eventUid);
    }

    public List<EventDao> getMyPageEventActiveList() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        return eventRepository.findAllByStartTimeBeforeAndEndTimeAfterAndStatusAndMypageYnOrderByRegTimeDesc(now, now, 1, 1);
    }

    public SystemSettingDao getSystemSetting(int systemSettingUid) {
        return systemSettingRepository.findOne(systemSettingUid);
    }

    public SystemSettingDto getSystemSettingDto(){
        SystemSettingDto systemSetting = new SystemSettingDto();
        List<SystemSettingDao> daoList = systemSettingRepository.findAllBasicSetting();

        try {
            systemSetting.setDealReward(Long.parseLong(daoList.get(0).getSetting()));
            systemSetting.setBuyReviewReward(Long.parseLong(daoList.get(1).getSetting()));
            systemSetting.setSellReviewReward(Long.parseLong(daoList.get(2).getSetting()));
            systemSetting.setFakeReward(Long.parseLong(daoList.get(3).getSetting()));
            systemSetting.setShareRewrad(Long.parseLong(daoList.get(4).getSetting()));
            systemSetting.setInviterRewrad(Long.parseLong(daoList.get(5).getSetting()));
            systemSetting.setInvitedUserReward(Long.parseLong(daoList.get(6).getSetting()));
            systemSetting.setInvitedUserFirstDealReward(Long.parseLong(daoList.get(7).getSetting()));
        } catch (Exception e) {
            e.printStackTrace();
            systemSetting.setDealReward(0);
            systemSetting.setBuyReviewReward(0);
            systemSetting.setSellReviewReward(0);
            systemSetting.setFakeReward(0);
            systemSetting.setShareRewrad(0);
            systemSetting.setInviterRewrad(0);
            systemSetting.setInvitedUserReward(0);
            systemSetting.setInvitedUserFirstDealReward(0);
        }
        return systemSetting;
    }

    public EventDao findOneEvent(int eventUid) {
        return eventRepository.findOne(eventUid);
    }

    public NoticeDao findOneNotice(int noticeUid) {
        return noticeRepository.findOne(noticeUid);
    }

    public ReportDao saveReport(ReportDao dao) {
        return reportRepository.save(dao);
    }

    public ShareDao saveShare(ShareDao dao) {
        return shareRepository.save(dao);
    }

    public UsrWishDao saveUsrWish(UsrWishDao dao) {
        return usrWishRepository.save(dao);
    }

    public Page<UsrWishDto> getUsrWishList(Pageable pageable, int usrUid) {
        return usrWishRepository.findAllByUsrUid(pageable, usrUid).map(result -> {
            if (result == null)
                return null;

            Object[] objects = (Object[]) result;
            UsrWishDto dto = new UsrWishDto();
            dto.setUsrWish((UsrWishDao) objects[0]);
            dto.setBrand(brandService.convertBrandDaoToBrandMiniDto((BrandDao) objects[1]));
            dto.setCategory(categoryService.convertCategoryDaoToCategoryMiniDto((CategoryDao) objects[2]));

            return dto;
        });
    }

    public void deleteUsrWish(int usrWishUid) {
        usrWishRepository.delete(usrWishUid);
    }

    public ValetDao saveValet(ValetDao dao) {
        return valetRepository.save(dao);
    }
}
