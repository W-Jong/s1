package com.kyad.selluvapi.rest.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;
import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .directModelSubstitute(Timestamp.class, Long.class);
//                .globalResponseMessage(
//                        RequestMethod.GET,
//                        newArrayList(new ResponseMessageBuilder()
//                                        .code(500).message("500 message")
//                                        .responseModel(new ModelRef("Error")).build(),
//                                new ResponseMessageBuilder()
//                                        .code(403).message("Forbidden!!!!!").build())
//                );
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo("SELLUV REST API",
                "<a target=\"_blank\" href=\"https://docs.google.com/spreadsheets/d/15ZwSv8DJgU2EfHwQZZjH0li2oUMjMP8O4kXzdJAkRDc/edit?usp=sharing\">셀럽 프로젝트 RESTful API 문서</a>  ",
                "Version 0.0.2",
                "",
                null,
                "",
                "",
                new ArrayList<VendorExtension>());
        return apiInfo;
    }
}
