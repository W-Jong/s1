package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PdtStyleReq {

    @NotNull
    @ApiModelProperty("상품UID")
    private int pdtUid;

    @NotNull
    @ApiModelProperty("스타일이미지")
    private String styleImg;
}
