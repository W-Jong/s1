package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class UsrMoneyHisDto {
    @ApiModelProperty("변경이력UID")
    private int moneyChangeHisUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime;
    @ApiModelProperty("분류, 1-머니사용 2-판매대금 3-공유리워드 4-가품신고리워드 5-초대리워드 6-가입리워드 7-초대유저첫구매리워드 8-거래후기리워드 9-구매적립금")
    private int kind;
    @ApiModelProperty("타겟 UID, kind가 1,2,3,8,9일 경우 dealUid, 4인 경우 pdtUid, 5,6,7인 경우 usrUid")
    private int targetUid;
    @ApiModelProperty("내역")
    private String content;
    @ApiModelProperty("변동금액")
    private long amount;
    @ApiModelProperty("힌트내용")
    private String hintContent;
}
