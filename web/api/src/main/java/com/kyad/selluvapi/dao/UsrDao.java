package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "usr", schema = "selluv")
public class UsrDao {
    private int usrUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String usrMail;
    private String usrPwd;
    private String usrPhone;
    private String usrId = "";
    private String usrNm = "";
    private String usrNckNm = "";
    private String profileImg = "";
    private String profileBackImg = "";
    private LocalDate birthday = LocalDate.of(1990, 1, 1);
    private int gender = 1;
    private String bankNm = "";
    private String accountNm = "";
    private String accountNum = "";
    private String description = "";
    private int inviteUsrUid = 0;
    private long point = 0;
    private long money = 0;
    private int usrLoginType = 0;
    private String snsId = "";
    private int likeGroup = 1;
    private String accessToken = "";
    private String deviceToken = "";
    private Timestamp loginTime = new Timestamp(System.currentTimeMillis());
    private int sleepYn = 0;
    private long freeSendPrice = -1;
    private Timestamp sleepTime = new Timestamp(System.currentTimeMillis());
    private Timestamp edtTime = new Timestamp(System.currentTimeMillis());
    private Timestamp noticeTime = new Timestamp(0);
    private String memo = "";
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_mail", nullable = false, length = 50)
    public String getUsrMail() {
        return usrMail;
    }

    public void setUsrMail(String usrMail) {
        this.usrMail = usrMail;
    }

    @Basic
    @Column(name = "usr_pwd", nullable = false, length = 30)
    public String getUsrPwd() {
        return usrPwd;
    }

    public void setUsrPwd(String usrPwd) {
        this.usrPwd = usrPwd;
    }

    @Basic
    @Column(name = "usr_phone", nullable = false, length = 50)
    public String getUsrPhone() {
        return usrPhone;
    }

    public void setUsrPhone(String usrPhone) {
        this.usrPhone = usrPhone;
    }

    @Basic
    @Column(name = "usr_id", nullable = false, length = 50)
    public String getUsrId() {
        return usrId;
    }

    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    @Basic
    @Column(name = "usr_nm", nullable = false, length = 20)
    public String getUsrNm() {
        return usrNm;
    }

    public void setUsrNm(String usrNm) {
        this.usrNm = usrNm;
    }

    @Basic
    @Column(name = "usr_nck_nm", nullable = false, length = 50)
    public String getUsrNckNm() {
        return usrNckNm;
    }

    public void setUsrNckNm(String usrNckNm) {
        this.usrNckNm = usrNckNm;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 500)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "profile_back_img", nullable = false, length = 255)
    public String getProfileBackImg() {
        return profileBackImg;
    }

    public void setProfileBackImg(String profileBackImg) {
        this.profileBackImg = profileBackImg;
    }

    @Basic
    @JsonSerialize(using = LocalDateSerializer.class)
    @Column(name = "birthday", nullable = false)
    public LocalDate getBirthday() {
        return birthday;
    }

    @JsonDeserialize(using = LocalDateDeserializer.class)
    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "gender", nullable = false)
    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "bank_nm", nullable = false, length = 30)
    public String getBankNm() {
        return bankNm;
    }

    public void setBankNm(String bankNm) {
        this.bankNm = bankNm;
    }

    @Basic
    @Column(name = "account_nm", nullable = false, length = 20)
    public String getAccountNm() {
        return accountNm;
    }

    public void setAccountNm(String accountNm) {
        this.accountNm = accountNm;
    }

    @Basic
    @Column(name = "account_num", nullable = false, length = 63)
    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "invite_usr_uid", nullable = false)
    public int getInviteUsrUid() {
        return inviteUsrUid;
    }

    public void setInviteUsrUid(int inviteUsrUid) {
        this.inviteUsrUid = inviteUsrUid;
    }

    @Basic
    @Column(name = "point", nullable = false)
    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    @Basic
    @Column(name = "money", nullable = false)
    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    @Basic
    @Column(name = "usr_login_type", nullable = false)
    public int getUsrLoginType() {
        return usrLoginType;
    }

    public void setUsrLoginType(int usrLoginType) {
        this.usrLoginType = usrLoginType;
    }

    @Basic
    @Column(name = "sns_id", nullable = false, length = 50)
    public String getSnsId() {
        return snsId;
    }

    public void setSnsId(String snsId) {
        this.snsId = snsId;
    }

    @Basic
    @Column(name = "like_group", nullable = false)
    public int getLikeGroup() {
        return likeGroup;
    }

    public void setLikeGroup(int likeGroup) {
        this.likeGroup = likeGroup;
    }

    @Basic
    @Column(name = "access_token", nullable = false, length = 50)
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Basic
    @Column(name = "device_token", nullable = false, length = 255)
    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @Basic
    @Column(name = "login_time", nullable = false)
    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    @Basic
    @Column(name = "sleep_yn", nullable = false)
    public int getSleepYn() {
        return sleepYn;
    }

    public void setSleepYn(int sleepYn) {
        this.sleepYn = sleepYn;
    }

    @Basic
    @Column(name = "free_send_price", nullable = false)
    public long getFreeSendPrice() {
        return freeSendPrice;
    }

    public void setFreeSendPrice(long freeSendPrice) {
        this.freeSendPrice = freeSendPrice;
    }

    @Basic
    @Column(name = "sleep_time", nullable = false)
    public Timestamp getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(Timestamp sleepTime) {
        this.sleepTime = sleepTime;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "notice_time", nullable = false)
    public Timestamp getNoticeTime() {
        return noticeTime;
    }

    public void setNoticeTime(Timestamp noticeTime) {
        this.noticeTime = noticeTime;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrDao usrDao = (UsrDao) o;
        return usrUid == usrDao.usrUid &&
                gender == usrDao.gender &&
                inviteUsrUid == usrDao.inviteUsrUid &&
                point == usrDao.point &&
                money == usrDao.money &&
                usrLoginType == usrDao.usrLoginType &&
                likeGroup == usrDao.likeGroup &&
                sleepYn == usrDao.sleepYn &&
                freeSendPrice == usrDao.freeSendPrice &&
                status == usrDao.status &&
                Objects.equals(regTime, usrDao.regTime) &&
                Objects.equals(usrMail, usrDao.usrMail) &&
                Objects.equals(usrPwd, usrDao.usrPwd) &&
                Objects.equals(usrPhone, usrDao.usrPhone) &&
                Objects.equals(usrId, usrDao.usrId) &&
                Objects.equals(usrNm, usrDao.usrNm) &&
                Objects.equals(usrNckNm, usrDao.usrNckNm) &&
                Objects.equals(profileImg, usrDao.profileImg) &&
                Objects.equals(profileBackImg, usrDao.profileBackImg) &&
                Objects.equals(birthday, usrDao.birthday) &&
                Objects.equals(bankNm, usrDao.bankNm) &&
                Objects.equals(accountNm, usrDao.accountNm) &&
                Objects.equals(accountNum, usrDao.accountNum) &&
                Objects.equals(description, usrDao.description) &&
                Objects.equals(snsId, usrDao.snsId) &&
                Objects.equals(accessToken, usrDao.accessToken) &&
                Objects.equals(deviceToken, usrDao.deviceToken) &&
                Objects.equals(loginTime, usrDao.loginTime) &&
                Objects.equals(sleepTime, usrDao.sleepTime) &&
                Objects.equals(edtTime, usrDao.edtTime) &&
                Objects.equals(noticeTime, usrDao.noticeTime) &&
                Objects.equals(memo, usrDao.memo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrUid, regTime, usrMail, usrPwd, usrPhone, usrId, usrNm, usrNckNm, profileImg, profileBackImg, birthday, gender, bankNm, accountNm, accountNum, description, inviteUsrUid, point, money, usrLoginType, snsId, likeGroup, accessToken, deviceToken, loginTime, sleepYn, freeSendPrice, sleepTime, edtTime, noticeTime, memo, status);
    }
}
