package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.PdtDao;
import com.kyad.selluvapi.dao.PdtStyleDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluvapi.enumtype.FEED_TYPE;
import com.kyad.selluvapi.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.request.PdtStyleMultiReq;
import com.kyad.selluvapi.request.PdtStyleReq;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.PdtService;
import com.kyad.selluvapi.rest.service.PdtStyleService;
import com.kyad.selluvapi.rest.service.UsrFeedService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "/pdtStyle", description = "상품스타일관리")
@RequestMapping("/pdtStyle")
public class PdtStyleController {

    private final Logger LOG = LoggerFactory.getLogger(PdtStyleController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private PdtStyleService pdtStyleService;

    @Autowired
    private UsrFeedService usrFeedService;

    @ApiOperation(value = "스타일 팝업상세",
            notes = "스타일 팝업상세 내용을 얻는다.")
    @GetMapping(value = "/{pdtStyleUid}")
    public GenericResponse<List<PdtStyleDao>> getPdtStyleDetail(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtStyleUid") int pdtStyleUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtStyleDao pdtStyle = pdtStyleService.getPdtStyleInfo(pdtStyleUid);

        List<PdtStyleDao> pdtStyleList = pdtStyleService.getPdtStyleListByPdtUidNotOne(pdtStyle.getPdtUid(), pdtStyleUid);

        List<PdtStyleDao> responseList = new ArrayList<>();

        pdtStyle.setStyleImg(StarmanHelper.getTargetImageUrl("pdt_style", pdtStyleUid, pdtStyle.getStyleImg()));
        responseList.add(pdtStyle);

        for (PdtStyleDao pdtStyleDao : pdtStyleList) {
            pdtStyleDao.setStyleImg(StarmanHelper.getTargetImageUrl("pdt_style", pdtStyleDao.getPdtStyleUid(), pdtStyleDao.getStyleImg()));
            responseList.add(pdtStyleDao);
        }

        return new GenericResponse<>(ResponseMeta.OK, responseList);
    }

    @ApiOperation(value = "상품스타일 목록얻기",
            notes = "상품군에 따르는 상품스타일 목록을 얻는다.")
    @GetMapping(value = "")
    public GenericResponse<Page<PdtListDto>> getUsrFeedPdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("상품군 MALE-남성, FEMALE-여성, KIDS-키즈")
            @RequestParam(value = "pdtGroup") PDT_GROUP_TYPE pdtGroup,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.getNewPdtStyleList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), pdtGroup.getCode()));
    }

    @ApiOperation(value = "전체 상품스타일 목록얻기",
            notes = "전체 상품스타일 목록을 얻는다.")
    @GetMapping(value = "/all")
    public GenericResponse<Page<PdtListDto>> getAllPdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.getAllNewPdtStyleList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));
    }

    @ApiOperation(value = "상품스타일 등록",
            notes = "상품스타일을 새로 등록한다.")
    @PutMapping(value = "")
    public GenericResponse<PdtStyleDao> putPdtStyle(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid PdtStyleReq pdtStyleReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtStyleReq.getPdtUid());

        PdtStyleDao pdtStyle = new PdtStyleDao();
        pdtStyle.setUsrUid(usr.getUsrUid());
        pdtStyle.setPdtUid(pdt.getPdtUid());
        pdtStyle.setStyleImg(pdtStyleReq.getStyleImg());

        pdtStyleService.save(pdtStyle);

        if (!"".equals(pdtStyle.getStyleImg())) {
            StarmanHelper.moveTargetFolder("pdt_style", pdtStyle.getPdtStyleUid(), pdtStyle.getStyleImg());
        }

        pdtStyle.setStyleImg(StarmanHelper.getTargetImageUrl("pdt_style", pdtStyle.getPdtStyleUid(), pdtStyle.getStyleImg()));

        usrFeedService.insertFeed(FEED_TYPE.FEED05_FOLLOWING_USER_STYLE, usr, pdt);

        return new GenericResponse<>(ResponseMeta.OK, pdtStyle);
    }

    @ApiOperation(value = "상품스타일 다수등록",
            notes = "상품스타일을 다수 등록한다.")
    @PutMapping(value = "/{pdtUid}")
    public GenericResponse<Void> putPdtStyle(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @RequestBody @Valid PdtStyleMultiReq pdtStyleMultiReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        List<String> pdtStyleImgList = pdtStyleMultiReq.getStyleImgList();

        if (pdtStyleImgList == null || pdtStyleImgList.size() == 0) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        for (String pdtStyleImg : pdtStyleImgList) {
            PdtStyleDao pdtStyle = new PdtStyleDao();
            pdtStyle.setUsrUid(usr.getUsrUid());
            pdtStyle.setPdtUid(pdt.getPdtUid());
            pdtStyle.setStyleImg(pdtStyleImg);

            pdtStyleService.save(pdtStyle);

            if (!"".equals(pdtStyle.getStyleImg())) {
                StarmanHelper.moveTargetFolder("pdt_style", pdtStyle.getPdtStyleUid(), pdtStyle.getStyleImg());
            }

            usrFeedService.insertFeed(FEED_TYPE.FEED05_FOLLOWING_USER_STYLE, usr, pdt);
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "상품스타일 삭제",
            notes = "상품스타일을 삭제한다.")
    @DeleteMapping(value = "/{pdtStyleUid}")
    public GenericResponse<Void> deletePdtStyle(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtStyleUid") int pdtStyleUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtStyleDao pdtStyle = pdtStyleService.getPdtStyleInfo(pdtStyleUid);

        if (usr.getUsrUid() != pdtStyle.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        pdtStyleService.delete(pdtStyleUid);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }
}
