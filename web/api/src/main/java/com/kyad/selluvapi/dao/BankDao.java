package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bank", schema = "selluv")
public class BankDao {
    private int bankUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String bankNm;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bank_uid", nullable = false)
    public int getBankUid() {
        return bankUid;
    }

    public void setBankUid(int bankUid) {
        this.bankUid = bankUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "bank_nm", nullable = false, length = 255)
    public String getBankNm() {
        return bankNm;
    }

    public void setBankNm(String bankNm) {
        this.bankNm = bankNm;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankDao bankDao = (BankDao) o;
        return bankUid == bankDao.bankUid &&
                status == bankDao.status &&
                Objects.equals(regTime, bankDao.regTime) &&
                Objects.equals(bankNm, bankDao.bankNm);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bankUid, regTime, bankNm, status);
    }
}
