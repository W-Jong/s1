package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.CategoryDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.category.CategoryDetailDto;
import com.kyad.selluvapi.dto.category.SizeRefDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.jpa.SizeRefRepository;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.CategoryService;
import com.kyad.selluvapi.rest.service.PdtService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@Api(value = "/category", description = "카테고리관리")
@RequestMapping("/category")
public class CategoryController {

    private final Logger LOG = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private SizeRefRepository sizeRefRepository;

    @ApiOperation(value = "카테고리 상세",
            notes = "카테고리 UID에 따르는 카테고리 상세 정보를 얻는다.")
    @GetMapping(value = "/{categoryUid}")
    public GenericResponse<CategoryDao> getCategory(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "categoryUid") int categoryUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        CategoryDao category = categoryService.findOne(categoryUid);

        if (category == null || category.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);

        return new GenericResponse<>(ResponseMeta.OK, category);
    }

    @ApiOperation(value = "카테고리 전체 계층별 상세",
            notes = "카테고리 UID에 따르는 상위/.../카테고리 상세 정보를 얻는다.")
    @GetMapping(value = "/{categoryUid}/hierarchy")
    public GenericResponse<List<CategoryDao>> getCategoryHierarchy(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "categoryUid") int categoryUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        CategoryDao category = categoryService.findOne(categoryUid);

        if (category == null || category.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);

        return new GenericResponse<>(ResponseMeta.OK, categoryService.getAllHierarchyCategories(categoryUid));
    }

    @ApiOperation(value = "하위 카테고리 목록",
            notes = "해당 카테고리 UID에 따르는 하위 카테고리 목록정보를 얻는다.")
    @GetMapping(value = "/{categoryUid}/sub")
    public GenericResponse<List<CategoryDao>> getSubCategories(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "categoryUid") int categoryUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        CategoryDao category = categoryService.findOne(categoryUid);

        if (category == null || category.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);

        return new GenericResponse<>(ResponseMeta.OK, categoryService.getSubCategories(categoryUid));
    }

    @ApiOperation(value = "카테고리별 인기브랜드 및 하위카테고리 얻기",
            notes = "카테고리 UID에 따르는 카테고리 상세페이지에 표시 할 인기브랜드 및 하위카테고리 정보를 얻는다.")
    @GetMapping(value = "/{categoryUid}/detail")
    public GenericResponse<CategoryDetailDto> getCategoryDetail(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "categoryUid") int categoryUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        CategoryDao category = categoryService.findOne(categoryUid);

        if (category == null || category.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);

        CategoryDetailDto dto = new CategoryDetailDto();
        dto.setCategory(category);
        dto.setRecommendedBrand(categoryService.getRecommendedBrandByCategoryUid(categoryUid));
        List<CategoryDao> categoryDaoList = categoryService.getSubCategories(categoryUid);
        List<CategoryDao> childrenCategories = new ArrayList<>();
        for (CategoryDao categoryDao : categoryDaoList) {
            categoryDao.setPdtCount(categoryService.getPdtCountByCategoryUids(
                    categoryService.getSubAllCategoryUidsWithIt(categoryDao.getCategoryUid())));
            if (categoryDao.getPdtCount() > 0) {
                childrenCategories.add(categoryDao);
            }
        }
        childrenCategories.sort((o1, o2) -> {
            if (o1.getPdtCount() == o2.getPdtCount())
                return 0;
            else
                return (o1.getPdtCount() < o2.getPdtCount()) ? 1 : -1;
        });
        dto.setChildrenCategories(childrenCategories);

        return new GenericResponse<>(ResponseMeta.OK, dto);
    }

    @ApiOperation(value = "해당 카테고리에 속하는 상품목록얻기(브랜드기준)",
            notes = "해당 카테고리에 속한 상품목록을 얻는다.")
    @GetMapping(value = "/{categoryUid}/pdt")
    public GenericResponse<Page<PdtListDto>> getCategoryPdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "categoryUid") int categoryUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page,
            @ApiParam(value = "브랜드UID, 전체인 경우 0")
            @RequestParam(value = "brandUid", defaultValue = "0") int brandUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        CategoryDao category = categoryService.findOne(categoryUid);

        if (category == null || category.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);

        Page<PdtListDto> pdtListDtos = (brandUid == 0) ?
                pdtService.findAllByCategoryUid(
                        new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), categoryUid) :
                pdtService.findAllByCategoryUidAndBrandUid(
                        new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), categoryUid, brandUid);

        return new GenericResponse<>(ResponseMeta.OK, pdtListDtos);
    }

    @ApiOperation(value = "사이즈목록얻기",
            notes = "상품카테고리에 따르는 사이즈목록을 얻는다.")
    @GetMapping(value = "/{categoryUid}/size")
    public GenericResponse<SizeRefDto> getSizeList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "categoryUid") int categoryUid) {

        if (categoryUid < 5)
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

        UsrDao usr = usrService.getUserInfo(accessToken);

        CategoryDao category = categoryService.findOne(categoryUid);
        if (category == null || category.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);

        List<CategoryDao> categoryDaoList = categoryService.getAllHierarchyCategories(categoryUid);
        try {
            CategoryDao categoryStep2 = categoryDaoList.get(1);
            int category2Uid = categoryStep2.getCategoryUid();

            List<Integer> type1List = sizeRefRepository.findCategory2UidsByStatus(Arrays.asList(1));//Arrays.asList(6, 11);
            List<Integer> type23Array = sizeRefRepository.findCategory2UidsByStatus(Arrays.asList(2, 3));//Arrays.asList(8, 9, 13, 14);

            SizeRefDto sizeRef = new SizeRefDto();

            if (type1List.contains(category2Uid)) {
                sizeRef.setSizeType(1);
                sizeRef.setSizeFirst(null);
                sizeRef.setSizeSecond(null);
            } else if (type23Array.contains(category2Uid)) {
                sizeRef.setSizeType(2);
                sizeRef.setSizeFirst(null);
                sizeRef.setSizeSecond(null);
            } else {
                if (categoryDaoList.size() == 2) { // 2단계카테고리이면
                    sizeRef.setSizeType(2);
                    sizeRef.setSizeFirst(null);
                    sizeRef.setSizeSecond(null);
                } else {
                    sizeRef.setSizeType(3);

                    String strCategory2 = "";
                    String strCategory3 = "";
                    CategoryDao categoryStep3 = categoryDaoList.get(2);
                    String category2Name = categoryStep3.getCategoryName();
                    if (category2Uid == 5 || category2Uid == 10) {
                        if (category2Name.contains("셔츠")) {
                            strCategory2 = "상의";
                            strCategory3 = (category2Uid == 5) ? "셔츠" : "";
                        } else if (category2Name.contains("팬츠")) {
                            strCategory2 = "하의";
                        } else {
                            strCategory2 = "상의";
                        }
                    } else if (category2Uid >= 15) {
                        strCategory2 = (category2Name.contains("슈즈")) ? "슈즈" : "의류";
                    }

                    List<String> sizeFirst = sizeRefRepository.findSizeFirstByCategory1AndCategory2AndCategory3(category2Uid, strCategory2, strCategory3);
                    sizeRef.setSizeFirst((sizeFirst == null || sizeFirst.isEmpty()) ? null : sizeFirst);

                    if (sizeRef.getSizeFirst() == null) {
                        sizeRef.setSizeSecond(null);
                    } else {
                        List<List<String>> sizeSecond = new ArrayList<>();

                        for (String typeName : sizeRef.getSizeFirst()) {
                            List<String> sizeSubSecond = sizeRefRepository.findSizeSecondByCategory1AndCategory2AndCategory3AndTypeName(category2Uid, strCategory2, strCategory3, typeName);
                            sizeSecond.add((sizeSubSecond == null || sizeSubSecond.isEmpty()) ? null : sizeSubSecond);
                        }

                        sizeRef.setSizeSecond((sizeSecond.isEmpty()) ? null : sizeSecond);
                    }
                }
            }
            return new GenericResponse<>(ResponseMeta.OK, sizeRef);
        } catch (Exception e) {
            return new GenericResponse<>(ResponseMeta.CATEGORY_NOT_EXIST, null);
        }
    }
}
