package com.kyad.selluvapi.helper.iamport.request;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class CardData {
    @SerializedName("card_number")
    private String card_number;

    @SerializedName("expiry")
    private String expiry;

    @SerializedName("birth")
    private String birth;

    @SerializedName("pwd_2digit")
    private String pwd_2digit;

    @SerializedName("customer_name")
    private String customer_name;

    @SerializedName("customer_tel")
    private String customer_tel;

    @SerializedName("customer_email")
    private String customer_email;

    @SerializedName("customer_addr")
    private String customer_addr;

    @SerializedName("customer_postcode")
    private String customer_postcode;
}