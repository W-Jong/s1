package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.PdtStyleDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.pdt.PdtStyleDto;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.jpa.PdtStyleRepository;
import com.kyad.selluvapi.rest.common.exception.PdtStyleNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PdtStyleService {

    @Autowired
    private PdtStyleRepository pdtStyleRepository;

    @Autowired
    private UsrService usrService;

    public Page<PdtStyleDao> findAll(Pageable pageable) {
        return pdtStyleRepository.findAll(pageable);
    }

    public PdtStyleDao getPdtStyle(int pdtStyleUid) {
        return pdtStyleRepository.findOne(pdtStyleUid);
    }

    public PdtStyleDao getPdtStyleInfo(int pdtStyleUid) {
        PdtStyleDao pdtStyle = pdtStyleRepository.findOne(pdtStyleUid);
        if (pdtStyle == null/* || pdtStyle.getStatus() != 1*/) {
            throw new PdtStyleNotFoundException();
        }

        return pdtStyle;
    }

    public List<PdtStyleDao> getPdtStyleListByPdtUidNotOne(int pdtUid, int pdtStyleUid) {
        return pdtStyleRepository.findAllByPdtUidAndPdtStyleUidNotOrderByRegTimeDesc(pdtUid, pdtStyleUid);
    }

    public List<PdtStyleDto> getPdtStyleDtoListByPdtUid(int pdtUid) {
        List<Object> objectList = pdtStyleRepository.findAllByPdtUidOrderByRegTimeDesc(pdtUid);
        if (objectList == null)
            return null;
        List<PdtStyleDto> dtoList = new ArrayList<>();
        for (Object object : objectList) {
            Object[] objects = (Object[]) object;
            PdtStyleDto styleDto = new PdtStyleDto();
            PdtStyleDao styleDao = (PdtStyleDao) objects[0];

            String[] styleProfileImageInfo = StarmanHelper.getTargetImageUrlWithSize("pdt_style", styleDao.getPdtStyleUid(), styleDao.getStyleImg());
            styleDao.setStyleImg(styleProfileImageInfo[0]);
            styleDto.setStyleProfileImgWidth(Integer.parseInt(styleProfileImageInfo[1]));
            styleDto.setStyleProfileImgHeight(Integer.parseInt(styleProfileImageInfo[2]));

            styleDto.setPdtStyle(styleDao);
            styleDto.setUsr(usrService.convertUsrDaoToUsrMiniDto((UsrDao) objects[1]));

            dtoList.add(styleDto);
        }

        return dtoList;
    }

    public PdtStyleDao save(PdtStyleDao pdtStyle) {
        return pdtStyleRepository.save(pdtStyle);
    }

    @Transactional
    public void delete(int pdtStyleUid) {
        pdtStyleRepository.delete(pdtStyleUid);
    }
}
