package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrHideDao;
import com.kyad.selluvapi.jpa.UsrHideRepository;
import com.kyad.selluvapi.jpa.UsrLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UsrHideService {

    @Autowired
    private UsrHideRepository usrHideRepository;

    public Page<UsrHideDao> findAll(Pageable pageable) {
        return usrHideRepository.findAll(pageable);
    }

    public UsrHideDao getByUsrUidAndPeerUsrUid(int usrUid, int peerUsrUid) {
        return usrHideRepository.getByUsrUidAndPeerUsrUid(usrUid, peerUsrUid);
    }

    public UsrHideDao save(UsrHideDao dao) {
        return usrHideRepository.save(dao);
    }

    public void delete(int usrLikeUid) {
        usrHideRepository.delete(usrLikeUid);
    }
}
