package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface UsrRepository extends CrudRepository<UsrDao, Integer> {

    @Query("select a from UsrDao a")
    Page<UsrDao> findAll(Pageable pageable);

    @Query("select u from UsrDao u where u.status = 1")
    List<UsrDao> findAllActiveUser();

    UsrDao getByAccessToken(String accessToken);

    @Query(value = "SELECT * FROM usr u WHERE u.usr_id = ?1 AND u.usr_pwd = ?2 AND u.status = 1 ORDER BY reg_time DESC LIMIT 1", nativeQuery = true)
    UsrDao getByUsrIdAndUsrPwd(String usrId, String usrPwd);

    @Query(value = "SELECT * FROM usr u WHERE u.usr_mail = ?1 AND u.status = 1 ORDER BY u.usr_login_type LIMIT 1", nativeQuery = true)
    UsrDao getByUsrMail(String email);

    @Query("select u from UsrDao u join UsrSnsDao us on u.usrUid = us.usrUid where us.snsId = ?1 and us.snsType = ?2")
    UsrDao getTopBySnsIdAndUsrLoginType(String snsId, int usrLoginType);

    @Query("select count(a) as cnt from UsrDao a where a.usrMail = ?1 and a.usrUid <> ?2 and a.status = 1")
    long getCountByEmail(String email, int usrUid);

    @Query("select count(a) as cnt from UsrDao a where a.usrId = ?1")
    long getCountByUsrId(String usrId);

    @Query("select count(a) as cnt from UsrDao a where a.usrNm = ?1 and a.gender = ?2 and a.birthday = ?3 and a.status = 1 and a.usrUid <> ?4")
    long getCountByUsrNmAndGenderAndBirthdayAndNotUsrUid(String usrNm, int gender, LocalDate birthday, int usrUid);

    @Query(value = "SELECT * FROM usr u WHERE u.usr_id = ?1 AND u.status = 1 LIMIT 1", nativeQuery = true)
    UsrDao getByUsrId(String usrId);

    @Query(value = "SELECT * FROM usr u WHERE u.usr_phone = ?1 AND u.status = 1 LIMIT 1", nativeQuery = true)
    UsrDao getByUsrPhone(String usrPhone);

    @Query("select u from UsrBlockDao ub join UsrDao u on ub.peerUsrUid = u.usrUid and u.status = 1 " +
            " where ub.usrUid = ?1 order by ub.regTime desc")
    Page<UsrDao> findAllBlocked(Pageable pageable, int usrUid);

    @Query(value = "SELECT u.usr_uid, u.usr_id, u.usr_nck_nm, u.profile_img, " +
            " (SELECT count(*) FROM usr_like ul2 WHERE ul2.usr_uid = ?1 AND ul2.peer_usr_uid = u.usr_uid) AS usr_like_status " +
            " FROM usr_like ul JOIN usr u ON ul.peer_usr_uid = u.usr_uid AND u.status = 1 " +
            " WHERE ul.usr_uid = ?2 ORDER BY ul.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_like ul JOIN usr u ON ul.peer_usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE ul.usr_uid = ?2 AND ?1 = ?1 ",
            nativeQuery = true)
    Page<Object> findAllFollowing(Pageable pageable, int usrUid, int peerUsrUid);

    @Query(value = "SELECT u.usr_uid, u.usr_id, u.usr_nck_nm, u.profile_img, " +
            " (SELECT count(*) FROM usr_like ul2 WHERE ul2.usr_uid = ?1 AND ul2.peer_usr_uid = u.usr_uid) AS usr_like_status " +
            " FROM usr_like ul JOIN usr u ON ul.usr_uid = u.usr_uid AND u.status = 1 " +
            " WHERE ul.peer_usr_uid = ?2 ORDER BY ul.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_like ul JOIN usr u ON ul.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE ul.peer_usr_uid = ?2 AND ?1 = ?1 ",
            nativeQuery = true)
    Page<Object> findAllFollower(Pageable pageable, int usrUid, int peerUsrUid);

    @Query(value = "SELECT u.usr_uid, u.usr_id, u.usr_nck_nm, u.profile_img, " +
            " (SELECT count(*) FROM usr_like ul2 WHERE ul2.usr_uid = ?1 AND ul2.peer_usr_uid = upl.usr_uid) AS usr_like_status " +
            " FROM usr_pdt_like upl JOIN usr u ON upl.usr_uid = u.usr_uid AND u.status = 1 " +
            " WHERE upl.pdt_uid = ?2 ORDER BY upl.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_pdt_like upl JOIN usr u ON upl.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE upl.pdt_uid = ?2 AND ?1 = ?1 ",
            nativeQuery = true)
    Page<Object> findAllPdtLiked(Pageable pageable, int usrUid, int pdtUid);

    @Query(value = "SELECT u.usr_uid, u.usr_id, u.usr_nck_nm, u.profile_img, " +
            " (SELECT count(*) FROM usr_like ul2 WHERE ul2.usr_uid = ?1 AND ul2.peer_usr_uid = upw.usr_uid) AS usr_like_status " +
            " FROM usr_pdt_wish upw JOIN usr u ON upw.usr_uid = u.usr_uid AND u.status = 1 " +
            " WHERE upw.pdt_uid = ?2 ORDER BY upw.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_pdt_wish upw JOIN usr u ON upw.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE upw.pdt_uid = ?2 AND ?1 = ?1 ",
            nativeQuery = true)
    Page<Object> findAllPdtWished(Pageable pageable, int usrUid, int pdtUid);

    @Query(value = "SELECT u.* FROM usr u WHERE u.status = 1 AND u.usr_uid <> ?1 AND (u.usr_id LIKE %?3% OR u.usr_nck_nm LIKE %?3%) " +
            " AND (exists(SELECT r.reply_uid FROM reply r WHERE r.usr_uid = u.usr_uid AND r.pdt_uid = ?2 AND r.status = 1) " +
            " OR exists(SELECT ul.usr_like_uid FROM usr_like ul WHERE ul.usr_uid = u.usr_uid AND ul.peer_usr_uid = ?1))",
            nativeQuery = true)
    List<UsrDao> getMentionedUserListByUsrUidAndPdtUid(int usrUid, int pdtUid, String keyword);

    @Query(value = "SELECT u.* FROM usr u WHERE u.status = 1 AND u.usr_uid <> ?1 AND u.usr_nck_nm = ?3 " +
            " AND (exists(SELECT r.reply_uid FROM reply r WHERE r.usr_uid = u.usr_uid AND r.pdt_uid = ?2 AND r.status = 1) " +
            " OR exists(SELECT ul.usr_like_uid FROM usr_like ul WHERE ul.usr_uid = u.usr_uid AND ul.peer_usr_uid = ?1)) LIMIT 1",
            nativeQuery = true)
    UsrDao getMentionedUserByUsrUidAndPdtUid(int usrUid, int pdtUid, String usrNckNm);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.status IN ?3 AND p.usr_uid = ?2 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.status IN ?3 AND p.usr_uid = ?2 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ",
            nativeQuery = true)
    Page<Object> findAllPdtByUsrUid(Pageable pageable, int usrUid, int peerUsrUid, List<Integer> statusList);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM usr_pdt_wish upw JOIN pdt p ON p.pdt_uid = upw.pdt_uid JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.status IN ?3 AND upw.usr_uid = ?2 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_pdt_wish upw JOIN pdt p ON p.pdt_uid = upw.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.status IN ?3 AND upw.usr_uid = ?2 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ",
            nativeQuery = true)
    Page<Object> findAllPdtWishByUsrUid(Pageable pageable, int usrUid, int peerUsrUid, List<Integer> statusList);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE p.status IN ?3 AND p.usr_uid = ?2 " +
            " ORDER BY ps.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.status IN ?3 AND p.usr_uid = ?2 AND ?1 = ?1 ",
            nativeQuery = true)
    Page<Object> findAllPdtStyleByUsrUid(Pageable pageable, int usrUid, int peerUsrUid, List<Integer> statusList);

    @Query("select count(p) from PdtDao p where p.usrUid = ?1 and p.status <> 0")
    long countPdtByUsrUid(int usrUid);

    @Query(value = "select u.usr_uid, u.usr_id, u.usr_nck_nm, u.profile_img, " +
            " (select count(*) from usr_like ul where ul.peer_usr_uid = u.usr_uid) as follower_count, " +
            " (select count(*) from usr_like ul2 where ul2.peer_usr_uid = u.usr_uid and ul2.usr_uid = ?1) as like_status, " +
            " (select count(*) from pdt p where p.usr_uid = u.usr_uid and p.status <> 0) as pdt_count, " +
            " (select count(*) from pdt p where p.usr_uid = u.usr_uid and p.pdt_group = ?2 and p.status <> 0) as pdt_group_count " +
            " from usr u where u.status = 1 and u.usr_uid <> ?1 and " +
            " (select count(*) from pdt p where p.usr_uid = u.usr_uid and p.photoshop_yn= 1 and p.fame_score > 0 and p.status = 1) > 2 and " +
            " not exists (select uh.usr_hide_uid from usr_hide uh where uh.usr_uid = ?1 and uh.peer_usr_uid = u.usr_uid) " +
            " order by (follower_count + pdt_group_count) desc \n#pageable\n ",
            countQuery = "select count(*) from usr u where u.status = 1 and u.usr_uid <> ?1 and " +
                    " (select count(*) from pdt p where p.usr_uid = u.usr_uid and p.photoshop_yn= 1 and p.fame_score > 0 and p.status = 1) > 2 and " +
                    " not exists (select uh.usr_hide_uid from usr_hide uh where uh.usr_uid = ?1 and uh.peer_usr_uid = u.usr_uid) AND ?2 = ?2 ",
            nativeQuery = true)
    Page<Object> findAllRecommendListByUsrUid(Pageable pageable, int usrUid, int pdtGroup);
}
