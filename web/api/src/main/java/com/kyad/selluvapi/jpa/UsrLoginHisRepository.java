package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.AppVersionDao;
import com.kyad.selluvapi.dao.UsrLoginHisDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrLoginHisRepository extends CrudRepository<UsrLoginHisDao, Integer> {

    @Query("select a from UsrLoginHisDao a")
    Page<AppVersionDao> findAll(Pageable pageable);
}
