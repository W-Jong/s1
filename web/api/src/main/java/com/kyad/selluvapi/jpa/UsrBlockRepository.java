package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrBlockDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrBlockRepository extends CrudRepository<UsrBlockDao, Integer> {

    @Query("select a from UsrBlockDao a")
    Page<UsrBlockDao> findAll(Pageable pageable);

    UsrBlockDao getByUsrUidAndPeerUsrUid(int usrUid, int peerUsrUid);
}
