package com.kyad.selluvapi.enumtype;

public enum PDT_GROUP_TYPE {
    MALE(1), FEMALE(2), KIDS(4);

    private int code;

    private PDT_GROUP_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
