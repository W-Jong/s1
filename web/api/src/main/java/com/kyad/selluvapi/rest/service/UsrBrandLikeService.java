package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrBrandLikeDao;
import com.kyad.selluvapi.jpa.UsrBrandLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsrBrandLikeService {

    @Autowired
    private UsrBrandLikeRepository usrBrandLikeRepository;

    public UsrBrandLikeDao save(UsrBrandLikeDao dao) {
        return usrBrandLikeRepository.save(dao);
    }

    public Iterable<UsrBrandLikeDao> save(Iterable<UsrBrandLikeDao> daoIterable) {
        return usrBrandLikeRepository.save(daoIterable);
    }

    public UsrBrandLikeDao getByUsrUidAndBrandUid(int usrUid, int brandUid) {
        return usrBrandLikeRepository.getByUsrUidAndBrandUid(usrUid, brandUid);
    }

    public void delete(int usrBrandLikeUid) {
        usrBrandLikeRepository.delete(usrBrandLikeUid);
    }
}
