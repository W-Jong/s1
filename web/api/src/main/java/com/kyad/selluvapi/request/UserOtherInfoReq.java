package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;


@Data
public class UserOtherInfoReq {
    @NotEmpty
    @ApiModelProperty("이메일")
    private String usrMail;
    @NotNull
    @ApiModelProperty("성함1")
    private String addressName1;
    @NotNull
    @ApiModelProperty("연락처1")
    private String addressPhone1;
    @NotNull
    @ApiModelProperty("주소1")
    private String addressDetail1;
    @NotNull
    @ApiModelProperty("상세주소1(우편번호)")
    private String addressDetailSub1;
    @NotNull
    @ApiModelProperty("성함2")
    private String addressName2;
    @NotNull
    @ApiModelProperty("연락처2")
    private String addressPhone2;
    @NotNull
    @ApiModelProperty("주소2")
    private String addressDetail2;
    @NotNull
    @ApiModelProperty("상세주소2(우편번호)")
    private String addressDetailSub2;
    @NotNull
    @ApiModelProperty("성함3")
    private String addressName3;
    @NotNull
    @ApiModelProperty("연락처3")
    private String addressPhone3;
    @NotNull
    @ApiModelProperty("주소3")
    private String addressDetail3;
    @NotNull
    @ApiModelProperty("상세주소3(우편번호)")
    private String addressDetailSub3;
    @Range(min = 1, max = 3)
    @ApiModelProperty("선택된 주소번호 1~3만 가능")
    private int addressSelectionIndex;
}
