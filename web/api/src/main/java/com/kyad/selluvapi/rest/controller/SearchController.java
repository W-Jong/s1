package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.BrandDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.search.MinMaxDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.search.RecommendSearchDto;
import com.kyad.selluvapi.dto.search.SearchWordDto;
import com.kyad.selluvapi.request.SearchReq;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "/search", description = "검색관리")
@RequestMapping("/search")
public class SearchController {

    private final Logger LOG = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private SearchWordService searchWordService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private TagWordService tagWordService;

    @ApiOperation(value = "검색",
            notes = "키워드에 의한 상품검색 리스트를 얻는다.")
    @GetMapping(value = "")
    public GenericResponse<Page<PdtListDto>> getSearch(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "키워드", required = true) @RequestParam(value = "keyword") String keyword,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        searchWordService.saveKeywordHistory(keyword);

        SearchReq searchReq = new SearchReq();
        searchReq.setKeyword(keyword);

        return new GenericResponse<>(ResponseMeta.OK, searchService.searchByCondition(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), searchReq));
    }

    @ApiOperation(value = "상세검색",
            notes = "필터조건에 맞는 상품리스트를 표시한다.")
    @PostMapping(value = "/detail")
    public GenericResponse<Page<PdtListDto>> getSearchDetail(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "검색조건") @RequestBody @Valid SearchReq searchReq,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        searchWordService.saveKeywordHistory(searchReq.getKeyword());

        return new GenericResponse<>(ResponseMeta.OK, searchService.searchByCondition(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), searchReq));
    }

    @ApiOperation(value = "검색결과 갯수",
            notes = "검색조건에 맞는 상품갯수를 얻는다")
    @PostMapping(value = "/count")
    public GenericResponse<Long> getSearchCount(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "검색조건") @RequestBody @Valid SearchReq searchReq) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, searchService.countByCondition(usr.getUsrUid(), searchReq));
    }

    @ApiOperation(value = "상품 최대, 최소가격",
            notes = "검색조건에 맞는 상품가격 최대, 최소값을 얻는다.")
    @PostMapping(value = "/priceRange")
    public GenericResponse<MinMaxDto> getMinMaxPrice(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "검색조건") @RequestBody @Valid SearchReq searchReq) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, searchService.getPriceRangeByCondition(usr.getUsrUid(), searchReq));
    }

    @ApiOperation(value = "인기검색 키워드 리스트 얻기",
            notes = "인기검색리스트를 얻는다.")
    @GetMapping(value = "/fame")
    public GenericResponse<List<SearchWordDto>> getFameSearchWordList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, searchWordService.findAllFameWords());
    }

    @ApiOperation(value = "추천검색 키워드 리스트 얻기",
            notes = "키워드에 따르는 추천검색 리스트를 얻는다")
    @GetMapping(value = "/recommend")
    public GenericResponse<RecommendSearchDto> getRecommendSearchWords(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "키워드", required = true) @RequestParam(value = "keyword") String keyword) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        RecommendSearchDto recommendSearch = new RecommendSearchDto();
        recommendSearch.setBrandList(brandService.getListByKeyword(keyword));
        recommendSearch.setCategoryDaoList(categoryService.getListByKeyword(keyword));
        recommendSearch.setModelList(pdtService.getRecommendModelByKeyword(keyword));
        recommendSearch.setTagWordList(tagWordService.getListByKeyword(keyword));

        return new GenericResponse<>(ResponseMeta.OK, recommendSearch);
    }

    @ApiOperation(value = "태그검색",
            notes = "키워드에 따르는 태그 목록을 얻는다")
    @GetMapping(value = "/tag")
    public GenericResponse<List<String>> getSearchTag(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "키워드(#제거한 문자열)", required = true) @RequestParam(value = "keyword") String keyword) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, tagWordService.getListByPrefixKeyword(keyword));
    }
}
