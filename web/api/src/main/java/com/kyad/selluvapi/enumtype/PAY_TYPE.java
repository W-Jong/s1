package com.kyad.selluvapi.enumtype;

public enum PAY_TYPE {
    SIMPLECARD(1), NORMALCARD(2), REALTIMEPAY(3); //, NAVERPAY(4)

    private int code;

    private PAY_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
