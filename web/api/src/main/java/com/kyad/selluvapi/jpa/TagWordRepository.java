package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.TagWordDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagWordRepository extends CrudRepository<TagWordDao, Integer> {

    @Query("select a from TagWordDao a")
    Page<TagWordDao> findAll(Pageable pageable);

    TagWordDao findFirstByContent(String content);

    @Query(value = "select content from tag_word where content like %?1% order by pdt_count desc", nativeQuery = true)
    List<String> getByKeyword(String keyword);

    @Query(value = "select content from tag_word where content like ?1% order by pdt_count desc", nativeQuery = true)
    List<String> getByPrefixKeyword(String keyword);
}
