package com.kyad.selluvapi.rest.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class EmailSender {
    /*
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE html
            PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>[셀럽] 비밀번호 재설정</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
    </head>
    <body>
    <table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #999999">
        <thead>
        <tr>
            <th height="40" style="border:1px solid #999999; background-color: #eeeeee; text-align: left">
                <strong style="font-size:large">&nbsp;&nbsp;&nbsp;[셀럽] 비밀번호 재설정</strong>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td height="300" style="padding: 20px; border:1px solid #999999; vertical-align: top">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                    <tr>
                        <td colspan="2"><strong>본 메일은 해당 회원님의 이메일로 등록된 셀럽계정에 대하여 비밀번호찾기를 요청하셔서 전송되었습니다.</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2" height="30">&nbsp;</td>
                    </tr>
                    <tr>
                        <th height="50" width="150" style="background-color: #dddddd; border: 1px solid #cccccc">
                            재설정 URL
                        </th>
                        <td style="padding: 0 20px; border: 1px solid #cccccc; font-weight: bold"><a href="url" target="_blank">url</a></td>
                    </tr>
                    <tr>
                        <td colspan="2" height="30">위의 URL로 접속하신 후 셀럽 비밀번호를 재설정해주세요.</td>
                    </tr>
                    <tr>
                        <td colspan="2" height="50">본인이 요청한것이 아니라면 위의 링크를 무시해주세요.</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="50" style="text-align: justify; padding: 10px 20px; border:1px solid #999999">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td style="vertical-align: top"><span style="font-weight: bold; font-size: 35px;">&#x26a0;</span>
                        </td>
                        <td style="padding-left: 10px">
                            <strong>본 메일은 발신 전용이며 회신되지 않습니다. &nbsp;&nbsp;본 메일과 관련한 문의는 고객센터로 문의하여 주시기 바랍니다.</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    </body>
    </html>
     */
    @Autowired
    private JavaMailSender mailSender;

    public boolean sendTemporaryPasswordResetUrl(String email, String url) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            message.setSubject("[셀럽] 비밀번호재설정", "UTF-8");
            String htmlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<!DOCTYPE html\n" +
                    "        PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n" +
                    "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                    "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                    "<head>\n" +
                    "    <title>[셀럽] 비밀번호 재설정</title>\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:1px solid #999999\">\n" +
                    "    <thead>\n" +
                    "    <tr>\n" +
                    "        <th height=\"40\" style=\"border:1px solid #999999; background-color: #eeeeee; text-align: left\">\n" +
                    "            <strong style=\"font-size:large\">&nbsp;&nbsp;&nbsp;[셀럽] 비밀번호 재설정</strong>\n" +
                    "        </th>\n" +
                    "    </tr>\n" +
                    "    </thead>\n" +
                    "    <tbody>\n" +
                    "    <tr>\n" +
                    "        <td height=\"300\" style=\"padding: 20px; border:1px solid #999999; vertical-align: top\">\n" +
                    "            <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n" +
                    "                <tbody>\n" +
                    "                <tr>\n" +
                    "                    <td colspan=\"2\"><strong>본 메일은 해당 회원님의 이메일로 등록된 셀럽계정에 대하여 비밀번호찾기를 요청하셔서 전송되었습니다.</strong></td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td colspan=\"2\" height=\"30\">&nbsp;</td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <th height=\"50\" width=\"150\" style=\"background-color: #dddddd; border: 1px solid #cccccc\">\n" +
                    "                        재설정 URL\n" +
                    "                    </th>\n" +
                    "                    <td style=\"padding: 0 20px; border: 1px solid #cccccc; font-weight: bold\"><a href=\"" + url + "\" target=\"_blank\">" + url + "</a></td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td colspan=\"2\" height=\"30\">위의 URL로 접속하신 후 셀럽 비밀번호를 재설정해주세요.</td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td colspan=\"2\" height=\"50\">본인이 요청한것이 아니라면 위의 링크를 무시해주세요.</td>\n" +
                    "                </tr>\n" +
                    "                </tbody>\n" +
                    "            </table>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "    <tr>\n" +
                    "        <td height=\"50\" style=\"text-align: justify; padding: 10px 20px; border:1px solid #999999\">\n" +
                    "            <table width=\"100%\">\n" +
                    "                <tbody>\n" +
                    "                <tr>\n" +
                    "                    <td style=\"vertical-align: top\"><span style=\"font-weight: bold; font-size: 35px;\">&#x26a0;</span>\n" +
                    "                    </td>\n" +
                    "                    <td style=\"padding-left: 10px\">\n" +
                    "                        <strong>본 메일은 발신 전용이며 회신되지 않습니다. &nbsp;&nbsp;본 메일과 관련한 문의는 고객센터로 문의하여 주시기 바랍니다.</strong>\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                </tbody>\n" +
                    "            </table>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "    </tbody>\n" +
                    "</table>\n" +
                    "</body>\n" +
                    "</html>";
            message.setContent(htmlContent, "text/html; charset=utf-8");
            message.setFrom(new InternetAddress("no-reply@selluv.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mailSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
