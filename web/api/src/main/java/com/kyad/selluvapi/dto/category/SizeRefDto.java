package com.kyad.selluvapi.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SizeRefDto {
    @ApiModelProperty("사이즈타입, 1 - 가로x세로x폭, 2 - FREE SIZE & 직접입력, 3-일반")
    private int sizeType;
    @ApiModelProperty("사이즈타입이 4인 경우에만 유효, 첫번째 피커목록")
    private List<String> sizeFirst;
    @ApiModelProperty("사이즈타입이 4인 경우에만 유효, 두번째피커목록(null인 경우 두번째피커 없음)")
    private List<List<String>> sizeSecond;
}
