package com.kyad.selluvapi.enumtype;

public enum DEAL_TYPE {
    BUYER(1), SELLER(2);

    private int code;

    private DEAL_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
