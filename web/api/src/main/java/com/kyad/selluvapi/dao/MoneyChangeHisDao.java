package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "money_change_his", schema = "selluv")
public class MoneyChangeHisDao {
    private int moneyChangeHisUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int kind;
    private int targetUid;
    private String pdtUid = "0";
    private String content = "";
    private long amount = 0;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "money_change_his_uid", nullable = false)
    public int getMoneyChangeHisUid() {
        return moneyChangeHisUid;
    }

    public void setMoneyChangeHisUid(int moneyChangeHisUid) {
        this.moneyChangeHisUid = moneyChangeHisUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "target_uid", nullable = false)
    public int getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(int targetUid) {
        this.targetUid = targetUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false, length = 63)
    public String getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(String pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "amount", nullable = false)
    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoneyChangeHisDao that = (MoneyChangeHisDao) o;
        return moneyChangeHisUid == that.moneyChangeHisUid &&
                usrUid == that.usrUid &&
                kind == that.kind &&
                targetUid == that.targetUid &&
                amount == that.amount &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(pdtUid, that.pdtUid) &&
                Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {

        return Objects.hash(moneyChangeHisUid, regTime, usrUid, kind, targetUid, pdtUid, content, amount, status);
    }
}
