package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.other.UsrAlarmDto;
import com.kyad.selluvapi.enumtype.ALARM_TYPE;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.helper.KakaoAlarmTalkHelper;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.push.FCMPushHelper;
import com.kyad.selluvapi.jpa.UsrAlarmRepository;
import com.kyad.selluvapi.rest.common.CommonConstant;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsrAlarmService {

    private final Logger LOG = LoggerFactory.getLogger(UsrAlarmService.class);

    @Autowired
    private UsrAlarmRepository usrAlarmRepository;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private UsrService usrService;

    public long countUnreadByUsrUidAndKind(int usrUid, int kind) {
        return usrAlarmRepository.countAllByUsrUidAndKindAndStatus(usrUid, kind, 2);
    }

    public long countUnreadByUsrUid(int usrUid) {
        return usrAlarmRepository.countAllByUsrUidAndStatus(usrUid, 2);
    }

    @Transactional
    public Page<UsrAlarmDto> findAllByUsrUidAndKind(Pageable pageable, int usrUid, int kind) {
        usrAlarmRepository.makeReadMarkByUsrUidAndKind(usrUid, kind);
        return usrAlarmRepository.findAllByUsrUidAndKindOrderByRegTimeDesc(pageable, usrUid, kind)
                .map(this::convertUsrAlarmDaoToUsrAlarmDto);
    }

    public UsrAlarmDto convertUsrAlarmDaoToUsrAlarmDto(UsrAlarmDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        UsrAlarmDto dto = modelMapper.map(dao, UsrAlarmDto.class);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("pdt", dao.getPdtUid(), dao.getProfileImg()));

        return dto;
    }

    @Async
    public void insertUsrAlarm(PUSH_TYPE pushType, UsrDao targetUsr, UsrDao peerUsr, DealDao deal, PdtDao pdt) {
        insertUsrAlarm(pushType, targetUsr, peerUsr, deal, pdt, "", 0);
    }

    @Async
    @Transactional
    public void insertUsrAlarm(PUSH_TYPE pushType, UsrDao targetUsr, UsrDao peerUsr, DealDao deal, PdtDao pdt, String extra, int extraUid) {
        if (targetUsr == null)
            return;

        if (peerUsr != null && targetUsr.getUsrUid() == peerUsr.getUsrUid()) {
            return;
        }

        try {
            //알림처리
            String alarmPermissionString;
            UsrPushSettingDao usrPushSetting = usrService.getPushSetting(targetUsr.getUsrUid());
            if (usrPushSetting == null)
                return;
            if (pushType.getCode() <= PUSH_TYPE.PUSH10_B_REFUND_UPDATED.getCode()) {
//                if (usrPushSetting.getDealYn() != 1)
//                    return;
                alarmPermissionString = usrPushSetting.getDealSetting();
                if (alarmPermissionString.charAt(pushType.getCode() - 1) != '1')
                    return;
            } else if (pushType.getCode() <= PUSH_TYPE.PUSH24_PDT_REPLY.getCode()) {
//                if (usrPushSetting.getNewsYn() != 1)
//                    return;
                alarmPermissionString = usrPushSetting.getNewsSetting();
            } else {
//                if (usrPushSetting.getReplyYn() != 1)
//                    return;
                alarmPermissionString = usrPushSetting.getReplySetting();
            }

            String pushContent;
            int targetUid;
            switch (pushType) {
                case PUSH01_S_PAY_COMPLETED:
                    if (deal == null)
                        throw new Exception();
                    pushContent = "[" + deal.getPdtTitle() + "] 상품이 판매되었습니다. 상품을 발송하신 후, 운송장번호를 입력해주시기 바랍니다." +
                            " 3영업일 내로 상품 운송장번호가 업데이트 되지 않을 경우, 주문이 자동으로 취소됩니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH02_B_DELIVERY_SENT:
                    if (deal == null)
                        throw new Exception();
                    pushContent = "주문하신 상품이 발송되었습니다. 하단 배송조회 버튼으로 배송 상황을 확인하실 수 있습니다.\n" +
                            "(판매자가 운송장번호를 잘못 기입한 경우 조회가 어려울 수 있습니다. 지속적으로 조회 되지 않을 경우, 셀럽으로 연락주시기 바랍니다. " +
                            CommonConstant.SELLUV_PHONE_NUMBER + ")";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH03_D_DEAL_COMPLETED:
                    if (deal == null)
                        throw new Exception();
                    pushContent = "구매자의 구매 확정으로 거래가 완료되어, 판매 대금이 익일 정산될 예정입니다. 계좌정보를 정확히 확인해주시기 바랍니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH04_B_DEAL_CANCELED:
                    if (deal == null || peerUsr == null)
                        throw new Exception();
                    pushContent = "주문하신 상품이 " + peerUsr.getUsrNm() + "님의 사정으로 거래가 취소되었습니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH05_S_DEAL_CANCELED:
                    if (deal == null || peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 [" + deal.getPdtTitle() + "] 상품 주문을 취소하였습니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH06_B_PDT_VERIFICATION:
                    if (deal == null)
                        throw new Exception();
                    pushContent = "주문하신 상품이 정품 감정을 위해 셀럽으로 배송 중입니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH07_S_NEGO_SUGGEST:
                    if (deal == null || peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 [" + deal.getPdtTitle() + "] 상품에 네고를 제안하였습니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH08_B_NEGO_UPDATED:
                    if (deal == null)
                        throw new Exception();
                    pushContent = deal.getPdtTitle() + " 상품 네고 제안에 대한 알림이 있습니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH09_S_REFUND_REQUEST:
                    if (deal == null || peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 [" + deal.getPdtTitle() + "] 상품에 대한 반품을 신청하였습니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH10_B_REFUND_UPDATED:
                    if (deal == null)
                        throw new Exception();
                    pushContent = "[" + deal.getPdtTitle() + "] 반품에 대한 알림이 있습니다.";
                    targetUid = deal.getDealUid();
                    break;
                case PUSH11_REWARD_INVITE:
                    if (alarmPermissionString.charAt(0) != '1')
                        return;
                    if (peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님의 가입으로 셀럽 머니가 적립되었습니다.";
                    targetUid = peerUsr.getUsrUid();
                    break;
                case PUSH12_REWARD_REPORT:
                    if (alarmPermissionString.charAt(1) != '1')
                        return;
                    if (pdt == null)
                        throw new Exception();
                    pushContent = pdtService.getPdtTitleByPdtUid(pdt.getPdtUid()) + "상품에 대한 가품 신고 리워드로 셀럽 머니가 적립되었습니다.";
                    targetUid = pdt.getPdtUid();
                    break;
                case PUSH14_FOLLOW_ME:
                    if (alarmPermissionString.charAt(2) != '1')
                        return;
                    if (peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 회원님을 팔로우하기 시작했습니다.";
                    targetUid = peerUsr.getUsrUid();
                    break;
                case PUSH15_LIKE_MY_PDT:
                    if (alarmPermissionString.charAt(3) != '1')
                        return;
                    if (peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 회원님의 상품을 좋아합니다.";
                    targetUid = peerUsr.getUsrUid();
                    break;
                case PUSH16_ITEM_MY_PDT:
                    if (alarmPermissionString.charAt(4) != '1')
                        return;
                    if (peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 회원님의 상품을 잇템하였습니다.";
                    targetUid = peerUsr.getUsrUid();
                    break;
                case PUSH17_LIKE_PDT_SALE:
                    if (alarmPermissionString.charAt(5) != '1')
                        return;
                    if (pdt == null)
                        throw new Exception();
                    pushContent = "회원님의 좋아요 상품이 가격 인하 되었습니다.";
                    targetUid = pdt.getPdtUid();
                    break;
                case PUSH18_FOLLOWING_USER_PDT:
                    if (alarmPermissionString.charAt(6) != '1')
                        return;
                    if (peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 새로운 상품을 등록하였습니다.";
                    targetUid = peerUsr.getUsrUid();
                    break;
                case PUSH19_FOLLOWING_USER_STYLE:
                    if (alarmPermissionString.charAt(7) != '1')
                        return;
                    if (peerUsr == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 새로운 스타일을 등록하였습니다.";
                    targetUid = peerUsr.getUsrUid();
                    break;
                case PUSH20_FOLLOWING_BRAND_PDT:
                    if (alarmPermissionString.charAt(8) != '1')
                        return;
                    if (extra == null || extra.isEmpty() || pdt == null)
                        throw new Exception();
                    pushContent = "[" + extra + "] 새로운 상품이 등록되었습니다.";
                    targetUid = pdt.getPdtUid();
                    break;
                case PUSH21_FINDING_PDT:
                    if (alarmPermissionString.charAt(9) != '1')
                        return;
                    if (extra == null || extra.isEmpty() || pdt == null)
                        throw new Exception();
                    pushContent = "회원님이 찾고 계신 [" + extra + "] 상품이 등록되었습니다";
                    targetUid = pdt.getPdtUid();
                    break;
                case PUSH22_EVENT:
                    if (alarmPermissionString.charAt(10) != '1')
                        return;
                    if (extra == null || extra.isEmpty() || extraUid < 1)
                        throw new Exception();
                    pushContent = extra;
                    targetUid = extraUid;
                    break;
                case PUSH23_NOTICE:
                    if (alarmPermissionString.charAt(11) != '1')
                        return;
                    if (extra == null || extra.isEmpty() || extraUid < 1)
                        throw new Exception();
                    pushContent = extra;
                    targetUid = extraUid;
                    break;
                case PUSH24_PDT_REPLY:
                    if (alarmPermissionString.charAt(0) != '1')
                        return;
                    if (peerUsr == null || pdt == null)
                        throw new Exception();
                    pushContent = pdtService.getPdtTitleByPdtUid(pdt.getPdtUid()) + " 상품에 " + peerUsr.getUsrNm() + "님이 댓글을 남겼습니다.";
                    targetUid = pdt.getPdtUid();
                    break;
                case PUSH26_MENTIONED_ME:
                    if (alarmPermissionString.charAt(1) != '1')
                        return;
                    if (peerUsr == null || pdt == null)
                        throw new Exception();
                    pushContent = peerUsr.getUsrNm() + "님이 회원님을 언급하였습니다.";
                    targetUid = pdt.getPdtUid();
                    break;
                case PUSH13:
                case PUSH25_REPLY_REPLY:
                default:
                    throw new Exception();
            }

            String alarmContent = pushContent;
            switch (pushType) {
                case PUSH02_B_DELIVERY_SENT:
                    if (extra != null && !extra.isEmpty() && "2".equals(extra)) {
                        alarmContent = "[" + deal.getPdtTitle() + "] 상품이 정품으로 판정되었습니다. 상품이 고객님께 발송되었으며, 배송을 조회하실 수 있습니다.";
                    } else {
                        alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") +
                                "주문하신 상품을 발송하였습니다. 상품 배송을 조회하실 수 있습니다.";
                    }
                    break;
                case PUSH08_B_NEGO_UPDATED:
                    switch (extra) {
                        default:
                        case "1":
                            alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") +
                                    "회원님의 네고 제안에 대한 카운터 네고를 제안하였습니다.";
                            break;

                        case "2":
                            alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") +
                                    "회원님의 네고 제안을 거절하였습니다.";
                            break;

                        case "3":
                            alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") + "[" + deal.getPdtTitle() +
                                    "] 상품에 대한 회원님의 네고 제안을 수락하였습니다. 네고 가격으로 주문이 완료되었습니다.";
                            break;

                        case "4":
                            alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") +
                                    "회원님의 네고 제안을 수락하였으나, 회원님의 결제정보가 정확하지 않아, 주문이 취소되었습니다.";
                            break;

                        case "5":
                            alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님의 " : "") +
                                    "카운터 네고를 수락하였으나, 회원님의 결제정보가 정확하지 않아 주문이 취소되었습니다.";
                            break;
                    }
                    break;
                case PUSH10_B_REFUND_UPDATED:
                    if (extra != null && !extra.isEmpty() && "2".equals(extra)) {
                        alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") +
                                "[" + deal.getPdtTitle() + "] 상품에 대한 반품을 거절하였습니다.";
                    } else {
                        alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님이 " : "") +
                                "[" + deal.getPdtTitle() + "] 상품에 대한 반품을 승인하였습니다.";
                    }
                    break;
                case PUSH11_REWARD_INVITE:
                    if (extra != null && !extra.isEmpty() && "2".equals(extra)) {
                        alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님의 " : "") +
                                "첫 구매로 초대 리워드가 적립되었습니다.";
                    } else {
                        alarmContent = ((peerUsr != null) ? peerUsr.getUsrNm() + "님의 " : "") +
                                "가입으로 초대 리워드가 지급되었습니다.";
                    }
                    break;
            }

            UsrAlarmDao usrAlarm = new UsrAlarmDao();
            usrAlarm.setUsrUid(targetUsr.getUsrUid());

            int kind = ALARM_TYPE.DEAL.getCode();
            if (pushType.getCode() >= PUSH_TYPE.PUSH24_PDT_REPLY.getCode())
                kind = ALARM_TYPE.REPLY.getCode();
            else if (pushType.getCode() >= PUSH_TYPE.PUSH11_REWARD_INVITE.getCode())
                kind = ALARM_TYPE.NEWS.getCode();

            usrAlarm.setKind(kind);
            usrAlarm.setSubKind(pushType.getCode());
            usrAlarm.setTargetUid(targetUid);
            usrAlarm.setContent(alarmContent);
            usrAlarm.setPdtUid((deal != null) ? deal.getPdtUid() : ((pdt != null ? pdt.getPdtUid() : 0)));
            usrAlarm.setProfileImg((deal != null) ? deal.getPdtImg() : ((pdt != null) ? pdt.getProfileImg() : ""));
            usrAlarm.setUsrProfileImg((peerUsr != null) ? StarmanHelper.getTargetImageUrl("usr", peerUsr.getUsrUid(), peerUsr.getProfileImg()) : "");

            usrAlarmRepository.save(usrAlarm);

            if (!"".equals(targetUsr.getDeviceToken()) && pushType.getCode() >= PUSH_TYPE.PUSH04_B_DEAL_CANCELED.getCode())
                FCMPushHelper.pushFCMNotification(targetUsr.getDeviceToken(), pushContent, pushType, targetUid);

            if (pushType.getCode() <= PUSH_TYPE.PUSH03_D_DEAL_COMPLETED.getCode()) {
                KakaoAlarmTalkHelper.sendKakaoAlarmTalk(pushType.getCode(), targetUsr.getUsrPhone(), pushContent, (deal == null) ? 0 : deal.getDealUid());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
