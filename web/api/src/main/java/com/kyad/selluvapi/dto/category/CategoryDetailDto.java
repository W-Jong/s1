package com.kyad.selluvapi.dto.category;

import com.kyad.selluvapi.dao.CategoryDao;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.brand.BrandRecommendedListDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CategoryDetailDto {
    private CategoryDao category;
    private List<BrandRecommendedListDto> recommendedBrand;
    private List<CategoryDao> childrenCategories;
}
