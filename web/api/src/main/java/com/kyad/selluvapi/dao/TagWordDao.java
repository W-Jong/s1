package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tag_word", schema = "selluv")
public class TagWordDao {
    private int tagWordUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String content = "";
    private int pdtCount = 0;
    private Timestamp edtTime = new Timestamp(System.currentTimeMillis());
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_word_uid", nullable = false)
    public int getTagWordUid() {
        return tagWordUid;
    }

    public void setTagWordUid(int tagWordUid) {
        this.tagWordUid = tagWordUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 63)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "pdt_count", nullable = false)
    public int getPdtCount() {
        return pdtCount;
    }

    public void setPdtCount(int pdtCount) {
        this.pdtCount = pdtCount;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagWordDao that = (TagWordDao) o;
        return tagWordUid == that.tagWordUid &&
                pdtCount == that.pdtCount &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(content, that.content) &&
                Objects.equals(edtTime, that.edtTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(tagWordUid, regTime, content, pdtCount, edtTime, status);
    }
}
