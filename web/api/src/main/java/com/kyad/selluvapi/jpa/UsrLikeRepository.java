package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrLikeDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrLikeRepository extends CrudRepository<UsrLikeDao, Integer> {

    @Query("select a from UsrLikeDao a")
    Page<UsrLikeDao> findAll(Pageable pageable);

    @Query("SELECT count(ul) from UsrLikeDao ul join UsrDao u on ul.usrUid = u.usrUid and u.status = 1 where ul.peerUsrUid = ?1")
    long countByPeerUsrUid(int peerUsrUid);

    @Query("SELECT count(ul) from UsrLikeDao ul join UsrDao u on ul.peerUsrUid = u.usrUid and u.status = 1 where ul.usrUid = ?1")
    long countByUsrUid(int usrUid);

    UsrLikeDao getByUsrUidAndPeerUsrUid(int usrUid, int peerUsrUid);
}
