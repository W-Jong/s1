package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrPdtLikeDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrPdtLikeRepository extends CrudRepository<UsrPdtLikeDao, Integer> {

    @Query("select a from UsrPdtLikeDao a")
    Page<UsrPdtLikeDao> findAll(Pageable pageable);

    UsrPdtLikeDao getByUsrUidAndPdtUid(int usrUid, int pdtUid);

    long countByPdtUid(int pdtUid);

    long countByUsrUidAndStatus(int usrUid, int status);
}
