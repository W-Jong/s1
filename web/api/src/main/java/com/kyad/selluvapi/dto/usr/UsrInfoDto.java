package com.kyad.selluvapi.dto.usr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UsrInfoDto {
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("아이디")
    private String usrId;
    @ApiModelProperty("닉네임")
    private String usrNckNm;
    @ApiModelProperty("프로필이미지")
    private String profileImg;
    @ApiModelProperty("프로필배경이미지")
    private String profileBackImg;
    @ApiModelProperty("자기소개")
    private String description;
    @ApiModelProperty("매너포인트")
    private long point;
    @ApiModelProperty("팔로워수")
    private long usrFollowerCount;
    @ApiModelProperty("팔로잉수")
    private long usrFollowingCount;
    @ApiModelProperty("유저 팔로우 상태")
    private Boolean usrLikeStatus;
    @ApiModelProperty("유저 차단 상태")
    private Boolean usrBlockStatus;
}
