package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.other.AlarmCountDto;
import com.kyad.selluvapi.dto.other.UsrAlarmDto;
import com.kyad.selluvapi.enumtype.ALARM_TYPE;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.UsrAlarmService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "/alarm", description = "알림관리")
@RequestMapping("/alarm")
public class AlarmController {

    private final Logger LOG = LoggerFactory.getLogger(AlarmController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @ApiOperation(value = "알림갯수 얻기",
            notes = "해당 회원의 알림갯수를 얻는다.")
    @GetMapping(value = "/count")
    public GenericResponse<AlarmCountDto> getAlarmCount(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        AlarmCountDto alarmCountDto = new AlarmCountDto();
        alarmCountDto.setTotalCount(usrAlarmService.countUnreadByUsrUid(usr.getUsrUid()));
        alarmCountDto.setDealCount(usrAlarmService.countUnreadByUsrUidAndKind(usr.getUsrUid(), ALARM_TYPE.DEAL.getCode()));
        alarmCountDto.setNewsCount(usrAlarmService.countUnreadByUsrUidAndKind(usr.getUsrUid(), ALARM_TYPE.NEWS.getCode()));
        alarmCountDto.setReplyCount(usrAlarmService.countUnreadByUsrUidAndKind(usr.getUsrUid(), ALARM_TYPE.REPLY.getCode()));

        return new GenericResponse<>(ResponseMeta.OK, alarmCountDto);
    }

    @ApiOperation(value = "알림목록 얻기",
            notes = "해당 회원의 유형별 알림목록을 얻는다.")
    @GetMapping(value = "")
    public GenericResponse<Page<UsrAlarmDto>> getAlarmList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "알림유형", required = true) @RequestParam(value = "kind") ALARM_TYPE kind,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, usrAlarmService.findAllByUsrUidAndKind(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), kind.getCode()));
    }
}
