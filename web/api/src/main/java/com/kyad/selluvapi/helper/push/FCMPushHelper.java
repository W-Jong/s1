package com.kyad.selluvapi.helper.push;

import com.google.gson.Gson;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;


public class FCMPushHelper {

    private final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    private final static String FCM_SERVER_KEY = "AAAA_GlH5Dg:APA91bEb3-Q1O68wbQwy7jfbzwQezt0lJeyGCt-wwtSO-flaETH74I5zACQsGWSrBjmp2JHJCuk_y2aMoKx_8e-ZDx-QeLqT_o76_pyyXuYZHnJ2RmRH_f8hvMjLiEO6k1OMyLuFWzxf";

    public static void pushFCMNotification(GenericFCMData fcmData) {

        if (fcmData == null || fcmData.getTo() == null || fcmData.getTo().isEmpty())
            return;

        //proxy설정
        //Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 10));

        HttpURLConnection conn = null;

        OutputStreamWriter wr = null;

        try {
            URL url = new URL(API_URL_FCM);
            //conn = (HttpURLConnection) url.openConnection(proxy);
            conn = (HttpURLConnection) url.openConnection();

            conn.setConnectTimeout(30000);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + FCM_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();

            wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

            wr.write(gson.toJson(fcmData));
            wr.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

//            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//
//            String output;
//            while ((output = br.readLine()) != null) {
//                System.out.println(output);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (wr != null) {
                try {
                    wr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null)
                conn.disconnect();
        }
    }

    public static void pushFCMNotification(String to, String body, PUSH_TYPE pushType, int targetUid) {
        GenericFCMData<HashMap<String, String>, HashMap<String, Object>> fcmData = new GenericFCMData<>();

        fcmData.setTo(to);

        HashMap<String, String> notification = new HashMap<>();
        notification.put("title", "셀럽");
        notification.put("body", body);
        fcmData.setNotification(notification);

        HashMap<String, Object> data = new HashMap<>();
        data.put("alarmType", pushType);
        data.put("targetUid", targetUid);
        fcmData.setData(data);

        pushFCMNotification(fcmData);
    }
}
