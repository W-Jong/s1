package com.kyad.selluvapi.helper.push;

public class GenericFCMData<S, T> {
    private String to;
//    private String[] registration_ids;
    private S notification;
    private T data;
    private String priority = "high";
    private boolean content_available = true;
    private boolean mutable_content = true;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public S getNotification() {
        return notification;
    }

    public void setNotification(S notification) {
        this.notification = notification;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public boolean isContent_available() {
        return content_available;
    }

    public void setContent_available(boolean content_available) {
        this.content_available = content_available;
    }

    public boolean isMutable_content() {
        return mutable_content;
    }

    public void setMutable_content(boolean mutable_content) {
        this.mutable_content = mutable_content;
    }
}