package com.kyad.selluvapi.dto.theme;

import com.kyad.selluvapi.dao.ThemeDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ThemeListDto {
    @ApiModelProperty("테마")
    private ThemeDao theme;
    @ApiModelProperty("테마상품갯수")
    private int pdtCount;
    @ApiModelProperty("테마상품목록")
    private List<PdtListDto> pdtList;
}
