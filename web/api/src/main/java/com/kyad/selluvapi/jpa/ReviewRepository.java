package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ReviewDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ReviewRepository extends CrudRepository<ReviewDao, Integer> {

    @Query("select a from ReviewDao a")
    Page<ReviewDao> findAll(Pageable pageable);

    @Query(value = "select r, u from ReviewDao r join UsrDao u on u.usrUid = r.usrUid where r.peerUsrUid = ?1 and r.status = 1 order by r.regTime desc")
    Page<Object> findAllByPeerUsrUid(Pageable pageable, int peerUsrUid);

    @Query(value = "select r, u from ReviewDao r join UsrDao u on u.usrUid = r.usrUid where r.peerUsrUid = ?1 and r.point = ?2 and r.status = 1 order by r.regTime desc")
    Page<Object> findAllByPeerUsrUidAndPoint(Pageable pageable, int peerUsrUid, int point);

    long countAllByPeerUsrUidAndPointAndStatus(int peerUsrUid, int point, int status);

    @Query(value = "select * from review where deal_uid = ?1 and usr_uid = ?2 and status = 1 limit 1", nativeQuery = true)
    ReviewDao getByDealUidAndUsrUid(int dealUid, int usrUid);
}
