package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.EventDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<EventDao, Integer> {

    @Query("select a from EventDao a")
    Page<EventDao> findAll(Pageable pageable);

    List<EventDao> findAllByStartTimeBeforeAndEndTimeAfterAndStatusOrderByRegTimeDesc(Timestamp startTime, Timestamp endTime, int status);

    List<EventDao> findAllByStartTimeBeforeAndEndTimeAfterAndStatusAndMypageYnOrderByRegTimeDesc(Timestamp startTime, Timestamp endTime, int status, int mypageYn);
}
