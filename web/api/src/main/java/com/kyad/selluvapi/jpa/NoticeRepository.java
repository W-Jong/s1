package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.NoticeDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface NoticeRepository extends CrudRepository<NoticeDao, Integer> {

    @Query("select a from NoticeDao a")
    Page<NoticeDao> findAll(Pageable pageable);

    List<NoticeDao> findAllByStartTimeBeforeAndEndTimeAfterAndStatusOrderByRegTimeDesc(Timestamp startTime, Timestamp endTime, int status);

    long countAllByStartTimeBeforeAndEndTimeAfterAndStatusAndRegTimeAfter(Timestamp startTime, Timestamp endTime, int status, Timestamp readTime);
}
