package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "category", schema = "selluv")
public class CategoryDao {
    @ApiModelProperty("카테고리UID")
    private int categoryUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("상위 카테고리UID")
    private int parentCategoryUid;
    @ApiModelProperty("카테고리 뎁스")
    private int depth;
    @ApiModelProperty("카테고리명")
    private String categoryName;
    @ApiModelProperty("카테고리 순서")
    private int categoryOrder;
    @ApiModelProperty("카테고리에 해당한 상품수(하위 카테고리 포함)")
    private int pdtCount;
    @ApiModelProperty("카테고리에 해당한 누적 거래건수(하위 카테고리 포함)")
    private long totalCount;
    @ApiModelProperty("카테고리에 해당한 누적 거래대금(하위 카테고리 포함)")
    private long totalPrice;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_uid", nullable = false)
    public int getCategoryUid() {
        return categoryUid;
    }

    public void setCategoryUid(int categoryUid) {
        this.categoryUid = categoryUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "parent_category_uid", nullable = false)
    public int getParentCategoryUid() {
        return parentCategoryUid;
    }

    public void setParentCategoryUid(int parentCategoryUid) {
        this.parentCategoryUid = parentCategoryUid;
    }

    @Basic
    @Column(name = "depth", nullable = false)
    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Basic
    @Column(name = "category_name", nullable = false, length = 30)
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Basic
    @Column(name = "category_order", nullable = false)
    public int getCategoryOrder() {
        return categoryOrder;
    }

    public void setCategoryOrder(int categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    @Basic
    @Column(name = "pdt_count", nullable = false)
    public int getPdtCount() {
        return pdtCount;
    }

    public void setPdtCount(int pdtCount) {
        this.pdtCount = pdtCount;
    }

    @Basic
    @Column(name = "total_count", nullable = false)
    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    @Basic
    @Column(name = "total_price", nullable = false)
    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDao that = (CategoryDao) o;
        return categoryUid == that.categoryUid &&
                parentCategoryUid == that.parentCategoryUid &&
                depth == that.depth &&
                categoryOrder == that.categoryOrder &&
                pdtCount == that.pdtCount &&
                totalCount == that.totalCount &&
                totalPrice == that.totalPrice &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(categoryName, that.categoryName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(categoryUid, regTime, parentCategoryUid, depth, categoryName, categoryOrder, pdtCount, totalCount, totalPrice, status);
    }
}
