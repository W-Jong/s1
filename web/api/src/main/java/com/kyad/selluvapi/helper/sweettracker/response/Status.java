package com.kyad.selluvapi.helper.sweettracker.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Status {
    @SerializedName("code")
    private String code;
    @SerializedName("msg")
    private String msg;
    @SerializedName("status")
    private boolean status;
}
