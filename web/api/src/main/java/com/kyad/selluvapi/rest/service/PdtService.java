package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.category.CategoryMiniDto;
import com.kyad.selluvapi.dto.pdt.*;
import com.kyad.selluvapi.dto.search.SearchModelDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.StringHelper;
import com.kyad.selluvapi.jpa.PdtRepository;
import com.kyad.selluvapi.jpa.ReportPdtRepository;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.exception.PdtNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;

@Service
public class PdtService {

    @Autowired
    private PdtRepository pdtRepository;

    @Autowired
    private ReportPdtRepository reportPdtRepository;

    @Autowired
    private ReplyService replyService;

    @Autowired
    private UsrService usrService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PdtStyleService pdtStyleService;

    @Autowired
    private UsrPdtLikeService usrPdtLikeService;

    @Autowired
    private UsrPdtWishService usrPdtWishService;

    @Autowired
    private UsrLikeService usrLikeService;

    @Autowired
    private EntityManager entityManager;

    public Page<PdtDao> findAll(Pageable pageable) {
        return pdtRepository.findAll(pageable);
    }

    public PdtDao getPdtInfo(int pdtUid) {
        PdtDao pdt = pdtRepository.findOne(pdtUid);
        if (pdt == null || pdt.getStatus() == 0) {
            throw new PdtNotFoundException();
        }

        return pdt;
    }

    public PdtDao getPdtByPdtUid(int pdtUid) {
        return pdtRepository.findOne(pdtUid);
    }

    public PdtDao save(PdtDao dao) {
        return pdtRepository.save(dao);
    }

    public Page<PdtListDto> getNewPdtStyleList(Pageable pageable, int usrUid, int likeGroup) {
        Timestamp zeroTimestamp = new Timestamp(0);

        return pdtRepository.findAllNewStyleList(pageable, usrUid, zeroTimestamp, likeGroup)
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> getAllNewPdtStyleList(Pageable pageable, int usrUid) {
        return pdtRepository.findAllStyleList(pageable, usrUid)
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public PdtListDto convertListObjectToPdtListDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        PdtListDto pdtDto = new PdtListDto();

        PdtMiniDto pdtMini = new PdtMiniDto();
        pdtMini.setPdtUid((int) objects[i++]);
        pdtMini.setUsrUid((int) objects[i++]);
        pdtMini.setBrandUid((int) objects[i++]);
        pdtMini.setCategoryUid((int) objects[i++]);
        pdtMini.setPdtSize((String) objects[i++]);

        String[] pdtProfileImageInfo = StarmanHelper.getTargetImageUrlWithSize("pdt", pdtMini.getPdtUid(), (String) objects[i++]);
        pdtMini.setProfileImg(pdtProfileImageInfo[0]);
        pdtDto.setPdtProfileImgWidth(Integer.parseInt(pdtProfileImageInfo[1]));
        pdtDto.setPdtProfileImgHeight(Integer.parseInt(pdtProfileImageInfo[2]));

        pdtMini.setOriginPrice(((BigInteger) objects[i++]).longValue());
        pdtMini.setPrice(((BigInteger) objects[i++]).longValue());
        pdtMini.setColorName((String) objects[i++]);
        pdtMini.setPdtModel((String) objects[i++]);
        Timestamp pdtMiniRegTime = (Timestamp) objects[i++];
        pdtMini.setEdtTime((Timestamp) objects[i++]);
        pdtMini.setStatus(((Integer) objects[i++]));
        pdtMini.setEdited(pdtMini.getEdtTime().getTime() > (pdtMiniRegTime.getTime() + 1000));
        pdtDto.setPdt(pdtMini);

        UsrMiniDto usrMini = new UsrMiniDto();
        usrMini.setUsrUid(pdtMini.getUsrUid());
        usrMini.setUsrId((String) objects[i++]);
        usrMini.setUsrNckNm((String) objects[i++]);
        usrMini.setProfileImg(StarmanHelper.getTargetImageUrl("usr", usrMini.getUsrUid(), (String) objects[i++]));
        pdtDto.setUsr(usrMini);

        BrandMiniDto brandMini = new BrandMiniDto();
        brandMini.setBrandUid(pdtMini.getBrandUid());
        brandMini.setFirstKo((String) objects[i++]);
        brandMini.setFirstEn((String) objects[i++]);
        brandMini.setNameKo((String) objects[i++]);
        brandMini.setNameEn((String) objects[i++]);
        brandMini.setProfileImg(StarmanHelper.getTargetImageUrl("brand", brandMini.getBrandUid(), (String) objects[i++]));
        pdtDto.setBrand(brandMini);

        CategoryMiniDto categoryMini = new CategoryMiniDto();
        categoryMini.setCategoryUid(pdtMini.getCategoryUid());
        categoryMini.setDepth((int) objects[i++]);
        categoryMini.setCategoryName((String) objects[i++]);
        pdtDto.setCategory(categoryMini);

        pdtDto.setPdtLikeCount(((BigInteger) objects[i++]).intValue());
        pdtDto.setPdtLikeStatus(((BigInteger) objects[i++]).intValue() > 0);

        if (objects[i] != null) {
            PdtStyleDao pdtStyle = new PdtStyleDao();
            pdtStyle.setPdtStyleUid((int) objects[i++]);
            pdtStyle.setRegTime((Timestamp) objects[i++]);
            pdtStyle.setPdtUid(pdtMini.getPdtUid());
            pdtStyle.setUsrUid((int) objects[i++]);

            String[] styleProfileImageInfo = StarmanHelper.getTargetImageUrlWithSize("pdt_style", pdtStyle.getPdtStyleUid(), (String) objects[i++]);
            pdtStyle.setStyleImg(styleProfileImageInfo[0]);
            pdtDto.setStyleProfileImgWidth(Integer.parseInt(styleProfileImageInfo[1]));
            pdtDto.setStyleProfileImgHeight(Integer.parseInt(styleProfileImageInfo[2]));

            pdtDto.setPdtStyle(pdtStyle);
        } else {
            i += 4;
            pdtDto.setPdtStyle(null);
            pdtDto.setStyleProfileImgWidth(1);
            pdtDto.setStyleProfileImgHeight(1);
        }

        return pdtDto;
    }

    public PdtMiniDto convertPdtDaoToPdtMiniDto(PdtDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        PdtMiniDto dto = modelMapper.map(dao, PdtMiniDto.class);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("pdt", dao.getPdtUid(), dao.getProfileImg()));
        dto.setEdited(dao.getEdtTime().getTime() > (dao.getRegTime().getTime() + 1000));
        return dto;
    }

    public PdtDto convertPdtDaoToPdtDto(PdtDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        PdtDto dto = modelMapper.map(dao, PdtDto.class);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("pdt", dao.getPdtUid(), dao.getProfileImg()));
        dto.setSignImg(StarmanHelper.getTargetImageUrl("pdt", dao.getPdtUid(), dao.getSignImg()));

        List<String> photoList = new ArrayList<>();
        photoList.add(StarmanHelper.getTargetImageUrl("pdt", dao.getPdtUid(), dao.getProfileImg()));
        if (dao.getPhotos() != null && !dao.getPhotos().isEmpty()) {
            String[] spiltStrings = StringHelper.splitString(dao.getPhotos(), ",");
            for (String item : spiltStrings) {
                if (!"".equals(StringHelper.trimWhitespace(item))) {
                    photoList.add(StarmanHelper.getTargetImageUrl("pdt", dao.getPdtUid(), StringHelper.trimWhitespace(item)));
                }
            }
        }
        dto.setPhotoList(photoList);

        return dto;
    }

    public List<PdtInfoDto> getNewPdtByBrandUidAndLikeGroup(int brandUid, int likeGroup) {

        List<Object> pdtObjectList = pdtRepository.getNewPdtByBrandUidAndLikeGroup(brandUid, likeGroup);

        List<PdtInfoDto> pdtInfoDtoList = new ArrayList<>();
        for (Object pdtObject : pdtObjectList) {
            pdtInfoDtoList.add(convertObjectToPdtInfoDto((Object[]) pdtObject));
        }

        return pdtInfoDtoList;
    }

    public List<PdtInfoDto> getRecommendedPdtByBrandUidAndLikeGroup(int brandUid, int likeGroup) {

        List<Object> pdtObjectList = pdtRepository.getRecommendedPdtByBrandUidAndLikeGroup(brandUid, likeGroup);

        List<PdtInfoDto> pdtInfoDtoList = new ArrayList<>();
        for (Object pdtObject : pdtObjectList) {
            pdtInfoDtoList.add(convertObjectToPdtInfoDto((Object[]) pdtObject));
        }

        return pdtInfoDtoList;
    }

    public List<PdtInfoDto> getRecommendedPdtByUsrUid(int usrUid, int pdtUid) {

        List<Object> pdtObjectList = pdtRepository.getRecommendedPdtByUsrUid(usrUid, pdtUid);

        List<PdtInfoDto> pdtInfoDtoList = new ArrayList<>();
        for (Object pdtObject : pdtObjectList) {
            pdtInfoDtoList.add(convertObjectToPdtInfoDto((Object[]) pdtObject));
        }

        return pdtInfoDtoList;
    }

    public PdtInfoDto convertObjectToPdtInfoDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        PdtInfoDto pdtDto = new PdtInfoDto();
        pdtDto.setPdtUid((int) objects[i++]);
        pdtDto.setBrandName((String) objects[i++]);
        pdtDto.setCategoryName((String) objects[i++]);
        pdtDto.setProfileImg(StarmanHelper.getTargetImageUrl("pdt", pdtDto.getPdtUid(), (String) objects[i++]));
        pdtDto.setPrice(((BigInteger) objects[i++]).longValue());

        return pdtDto;
    }

    public Page<PdtListDto> getPdtListByBrandUidAndPdtGroup(Pageable pageable, int usrUid, int brandUid, int pdtGroup) {

        return pdtRepository.findAllByBrandUidAndPdtGroup(pageable, usrUid, brandUid, pdtGroup)
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> getPdtStyleListByBrandUid(Pageable pageable, int usrUid, int brandUid) {

        return pdtRepository.findAllStyleListByBrandUid(pageable, usrUid, brandUid)
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> findAllByBrandUidOrCategory2(Pageable pageable, int usrUid, PdtDao pdtDao) {

        List<Integer> category2uids = categoryService.getStep2SubAllCategoryUids(pdtDao.getCategoryUid());
        return pdtRepository.findAllByBrandUidOrCategory2(pageable, usrUid, pdtDao.getBrandUid(),
                (category2uids == null || category2uids.size() < 1) ? Collections.singletonList(-1) : category2uids, pdtDao.getPdtUid())
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> findAllStyleListByBrandUidOrCateory2(Pageable pageable, int usrUid, PdtDao pdtDao) {

        List<Integer> category2uids = categoryService.getStep2SubAllCategoryUids(pdtDao.getCategoryUid());
        return pdtRepository.findAllStyleListByBrandUidOrCateory2(pageable, usrUid, pdtDao.getBrandUid(),
                (category2uids == null || category2uids.size() < 1) ? Collections.singletonList(-1) : category2uids, pdtDao.getPdtUid())
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> findAllByPdtModel(Pageable pageable, int usrUid, PdtDao pdtDao) {

        return pdtRepository.findAllByPdtModel(pageable, usrUid, pdtDao.getPdtModel(), pdtDao.getPdtUid())
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> findAllByTag(Pageable pageable, int usrUid, String tag, PdtDao pdtDao) {

        return pdtRepository.findAllByTag(pageable, usrUid, "#" + tag, pdtDao.getPdtUid())
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public PdtDetailDto getPdtDetail(int pdtUid, int usrUid) {
        PdtDetailDto detailDto = new PdtDetailDto();

        PdtDto pdtDto = convertPdtDaoToPdtDto(getPdtInfo(pdtUid));
        detailDto.setPdt(pdtDto);

        UsrDao usrDao = usrService.getByUsrUid(pdtDto.getUsrUid());
        if (usrDao == null || usrDao.getStatus() != 1)
            return null;
        detailDto.setSeller(usrService.convertUsrDaoToUsrDetailDto(usrDao));

        BrandDao brandDao = brandService.getByBrandUid(pdtDto.getBrandUid());
        if (brandDao == null || brandDao.getStatus() == 0)
            return null;
        detailDto.setBrand(brandService.convertBrandDaoToBrandMiniDto(brandDao));

        List<CategoryDao> categoryDaoList = categoryService.getAllHierarchyCategories(pdtDto.getCategoryUid());
        if (categoryDaoList == null || categoryDaoList.size() < 2)
            return null;
        detailDto.setCategoryList(categoryService.convertCategoryDaoListToCategoryMiniDtoList(categoryDaoList));

        detailDto.setPdtStyleList(pdtStyleService.getPdtStyleDtoListByPdtUid(pdtDto.getPdtUid()));

        detailDto.setPdtLikeCount((int) usrPdtLikeService.countByPdtUid(pdtDto.getPdtUid()));
        detailDto.setPdtLikeStatus(usrPdtLikeService.getByUsrUidAndPdtUid(usrUid, pdtDto.getPdtUid()) != null);

        detailDto.setPdtWishCount((int) usrPdtWishService.countByPdtUid(pdtDto.getPdtUid()));
        detailDto.setPdtWishStatus(usrPdtWishService.getByUsrUidAndPdtUid(usrUid, pdtDto.getPdtUid()) != null);

        detailDto.setPdtReplyCount((int) replyService.countByPdtUid(pdtDto.getPdtUid()));

        detailDto.setSellerItemCount((int) pdtRepository.countByUsrUid(pdtDto.getUsrUid()));
        detailDto.setSellerFollowerCount((int) usrLikeService.countFollowerUserByUsrUid(pdtDto.getUsrUid()));
        detailDto.setSellerPdtList(getRecommendedPdtByUsrUid(pdtDto.getUsrUid(), pdtDto.getPdtUid()));

        List<Integer> category2uids = categoryService.getStep2SubAllCategoryUids(pdtDto.getCategoryUid());
        detailDto.setRelationStyleCount((int) pdtRepository.countStyleListByBrandUidOrCategory2(
                pdtDto.getBrandUid(), (category2uids == null || category2uids.size() < 1) ? Collections.singletonList(-1) : category2uids, pdtDto.getPdtUid()));

        detailDto.setShareUrl(CommonConstant.SELLUV_HOST + "web/share/" + pdtDto.getPdtUid());

        detailDto.setEdited(pdtDto.getEdtTime().getTime() > (pdtDto.getRegTime().getTime() + 1000));

        return detailDto;
    }

    @Transactional
    public void removePdtAndReference(int pdtUid) {
        String sql = "delete from theme_pdt where pdt_uid = " + pdtUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_pdt_wish where pdt_uid = " + pdtUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_pdt_like where pdt_uid = " + pdtUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update reply set status = 0 where pdt_uid = " + pdtUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update pdt set status = 0 where pdt_uid = " + pdtUid;
        entityManager.createNativeQuery(sql).executeUpdate();
    }

    public int getFakeReportCountByPdtUid(int pdtUid) {
        return reportPdtRepository.countAllByPdtUid(pdtUid);
    }

    public ReportPdtDao saveReportPdt(ReportPdtDao dao) {
        return reportPdtRepository.save(dao);
    }

    public ReportPdtDao findOneReportPdt(int reportPdtUid) {
        return reportPdtRepository.findOne(reportPdtUid);
    }

    public List<String> getRecommendModelByBrandUid(int brandUid) {
        String sql = "SELECT pdt_model FROM pdt WHERE brand_uid = ?1 AND status <> 0 " +
                " GROUP BY pdt_model ORDER BY count(*) DESC LIMIT 10";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, brandUid);
        return (List<String>) query.getResultList();
    }

    public Page<PdtListDto> findAllByCategoryUid(Pageable pageable, int usrUid, int categoryUid) {
        List<Integer> categoryUidList = categoryService.getSubAllCategoryUidsWithIt(categoryUid);
        return pdtRepository.findAllByCategoryUidList(pageable, usrUid, categoryUidList)
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> findAllByCategoryUidAndBrandUid(Pageable pageable, int usrUid, int categoryUid, int brandUid) {
        List<Integer> categoryUidList = categoryService.getSubAllCategoryUidsWithIt(categoryUid);
        return pdtRepository.findAllByCategoryUidListAndBrandUid(pageable, usrUid, categoryUidList, brandUid)
                .map(result -> convertListObjectToPdtListDto((Object[]) result));
    }

    public String getPdtTitleByPdtUid(int pdtUid) {
        String sql = "SELECT CONCAT(b.name_ko, ' ', (CASE WHEN p.pdt_group = 1 THEN '남성' WHEN p.pdt_group = 2 THEN '여성' ELSE '키즈' END), " +
                " ' ', p.color_name, ' ', p.pdt_model, ' ', c.category_name) AS pdt_title " +
                " FROM pdt p JOIN brand b ON b.brand_uid = p.brand_uid JOIN category c ON c.category_uid = p.category_uid " +
                " WHERE p.pdt_uid = ?1";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, pdtUid);
        String title;
        try {
            title = (String) query.getSingleResult();
        } catch (NoResultException | NonUniqueResultException e) {
            title = "";
        }
        return title;
    }

    public List<SearchModelDto> getRecommendModelByKeyword(String keyword) {
        String sql = "SELECT p.pdt_model, b.name_ko FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid AND b.status = 1 " +
                " WHERE ((p.pdt_model LIKE ?1) OR (b.name_ko = ?2 OR b.name_en = ?2)) AND p.status <> 0 GROUP BY p.pdt_model";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, "%" + keyword + "%");
        query.setParameter(2, keyword);
        List<Object> objectList = query.getResultList();

        if (objectList == null)
            return null;
        List<SearchModelDto> searchModelList = new ArrayList<>();
        for (Object object : objectList) {
            Object[] objects = (Object[]) object;
            SearchModelDto searchModel = new SearchModelDto();
            searchModel.setModelName((String) objects[0]);
            searchModel.setBrandName((String) objects[1]);

            searchModelList.add(searchModel);
        }

        return searchModelList;
    }

    public List<PdtMiniDto> getThreeRecommendPdtByUsrUid(int usrUid) {
        List<PdtDao> pdtDaoList = pdtRepository.getThreeRecommendedPdtByUsrUid(usrUid);
        if (pdtDaoList == null)
            return null;
        List<PdtMiniDto> pdtMiniDtoList = new ArrayList<>();
        for (PdtDao pdtDao : pdtDaoList) {
            pdtMiniDtoList.add(convertPdtDaoToPdtMiniDto(pdtDao));
        }

        return pdtMiniDtoList;
    }
}
