package com.kyad.selluvapi.dto.usr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UsrInviteInfoDto {
    @ApiModelProperty("추천인코드")
    private String inviteCode;
    @ApiModelProperty("가입한 친구수")
    private long invitedCount;
    @ApiModelProperty("첫 구매한 친구수")
    private long boughtCount;
    @ApiModelProperty("이달 가입한 친구수")
    private long invitedMonthCount;
}
