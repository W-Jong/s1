package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrSnsDao;
import com.kyad.selluvapi.jpa.UsrSnsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsrSnsService {

    @Autowired
    private UsrSnsRepository usrSnsRepository;

    public boolean isSnsIdAndSnsTypeDuplicated(String snsId, int snsType) {
        return usrSnsRepository.countAllBySnsIdAndSnsType(snsId, snsType) > 0;
    }

    public UsrSnsDao save(UsrSnsDao dao) {
        return usrSnsRepository.save(dao);
    }

    @Transactional
    public void delete(int usrSnsUid) {
        usrSnsRepository.delete(usrSnsUid);
    }

    public List<UsrSnsDao> getListByUsrUid(int usrUid) {
        return usrSnsRepository.findAllByUsrUid(usrUid);
    }

    public UsrSnsDao getByUsrUidAndSnsType(int usrUid, int snsType) {
        return usrSnsRepository.findByUsrUidAndSnsType(usrUid, snsType);
    }
}
