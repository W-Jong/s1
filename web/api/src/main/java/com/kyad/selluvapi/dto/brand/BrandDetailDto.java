package com.kyad.selluvapi.dto.brand;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BrandDetailDto {
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    @ApiModelProperty("영문첫글자")
    private String firstEn;
    @ApiModelProperty("한글초성")
    private String firstKo;
    @ApiModelProperty("영문이름")
    private String nameEn;
    @ApiModelProperty("한글이름")
    private String nameKo;
    @ApiModelProperty("라이센스")
    private String licenseUrl;
    @ApiModelProperty("로고이미지")
    private String logoImg;
    @ApiModelProperty("썸네일이미지")
    private String profileImg;
    @ApiModelProperty("백그라운드 이미지")
    private String backImg;
    @ApiModelProperty("팔로우 유저수")
    private int brandLikeCount;
    @ApiModelProperty("유저 팔로우 상태")
    private Boolean brandLikeStatus;
    @ApiModelProperty("남성상품군 상품갯수")
    private int malePdtCount;
    @ApiModelProperty("여성상품군 상품갯수")
    private int femalePdtCount;
    @ApiModelProperty("키즈상품군 상품갯수")
    private int kidsPdtCount;
    @ApiModelProperty("스타일 상품갯수")
    private int stylePdtCount;
}
