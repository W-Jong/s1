package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.DealDao;
import com.kyad.selluvapi.dao.PdtDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.enumtype.DEAL_STATUS_TYPE;
import com.kyad.selluvapi.enumtype.PAY_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.response.IamportResponse;
import com.kyad.selluvapi.helper.iamport.response.PaymentAnnotation;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.exception.DealNotFoundException;
import com.kyad.selluvapi.rest.common.exception.UserNotFoundException;
import com.kyad.selluvapi.rest.common.exception.UserTempNotPermissionException;
import com.kyad.selluvapi.rest.service.DealService;
import com.kyad.selluvapi.rest.service.PaymentResultService;
import com.kyad.selluvapi.rest.service.PdtService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/web")
@Api(value = "/web", description = "웹브라우저 혹은 웹뷰연동", hidden = true)
public class WebviewController {

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private DealService dealService;

    @Autowired
    private PaymentResultService paymentResultService;

    @GetMapping(value = "/find/password/{accessToken}")
    @ApiOperation(value = "비밀번호변경페지 호출(이메일용)",
            notes = "비밀번호변경페지를 표시한다.")
    public String findPassword(@ApiIgnore ModelMap model,
                               @PathVariable("accessToken") String accessToken) {

        UsrDao usr = null;

        try {
            usr = usrService.getUserInfoNotTemp(accessToken);
//            String tempPwd = StarmanHelper.generateRandomString(20);
//            usr.setUsrPwd(tempPwd);
//            usrService.save(usr);
            model.addAttribute("valid", true);
            model.addAttribute("accessToken", accessToken);
            model.addAttribute("tempPwd", usr.getUsrPwd());
        } catch (UserNotFoundException | UserTempNotPermissionException e) {
            usr = null;
            model.addAttribute("valid", false);
            model.addAttribute("accessToken", "");
            model.addAttribute("tempPwd", "");
        }

        return "find_pwd";
    }

    @GetMapping(value = "/danalsms")
    @ApiOperation(value = "다날본인인증",
            notes = "다날본인인증 URL을 웹뷰에서 호출해야 함")
    public String danalAuth(@ApiIgnore ModelMap model) {

        model.addAttribute("selluvhost", CommonConstant.SELLUV_HOST);

        return "danal_sms";
    }

    @GetMapping(value = "/payment/{accessToken}/{dealUid}")
    @ApiOperation(value = "결제진행(결제API호출결과에 포함됨)",
            notes = "URL을 웹뷰에서 호출해서 결제진행함")
    public String pay(
            @PathVariable(value = "accessToken") String accessToken,
            @PathVariable(value = "dealUid") int dealUid,
            @ApiIgnore ModelMap model) {

        UsrDao usr = null;
        DealDao deal = null;

        try {
            usr = usrService.getUserInfoNotTemp(accessToken);
            deal = dealService.getDealByDealUid(dealUid);

            if (deal == null || usr.getUsrUid() != deal.getUsrUid())
                throw new Exception();

            if (deal.getPayImportMethod() != PAY_TYPE.NORMALCARD.getCode() &&
                    deal.getPayImportMethod() != PAY_TYPE.REALTIMEPAY.getCode()) {
                throw new Exception();
            }

            if (deal.getStatus() != DEAL_STATUS_TYPE.DELETED.getCode()) {
                throw new Exception();
            }

            model.addAttribute("isValid", true);

        } catch (Exception e) {
            usr = null;
            deal = null;
            model.addAttribute("isValid", false);
        }

        //결제진행웹뷰
        model.addAttribute("usr", usr);
        model.addAttribute("deal", deal);
        model.addAttribute("selluvhost", CommonConstant.SELLUV_HOST);

        return "payment";
    }



    @GetMapping(value = "/payment/complete")
    @ApiOperation(value = "아임포트결제 리다이렉트 URL(API용)",
            notes = "아임포트 인증결제 성공후 리다이렉트 페이지")
    public String paymentCompleted(@ApiIgnore ModelMap model, @ApiIgnore HttpServletRequest request) {
        // https://www.yourdomain.com/payments/complete?imp_uid=imp_12345678&merchant_uid=oid_987654321&imp_success=true

        String impUid = request.getParameter("imp_uid");
        String merchantUid = request.getParameter("merchant_uid");
        String impSuccess = request.getParameter("imp_success");

        if (!"true".equals(impSuccess)) {
            model.addAttribute("isValid", false);
        } else {
            IamportClientHelper iamportClient = new IamportClientHelper();
            try {
                IamportResponse<PaymentAnnotation> paymentIamportResponse = iamportClient.paymentByImpUid(impUid);
                PaymentAnnotation payment = paymentIamportResponse.getResponse();

                if ("paid".equals(payment.getStatus()) && paymentResultService.paymentResultProcess(payment)) { // 결제성공

                    model.addAttribute("isValid", true);
                } else {
                    model.addAttribute("isValid", false);
                }

            } catch (Exception e) {
                e.printStackTrace();
                model.addAttribute("isValid", false);
            }
        }

        model.addAttribute("dealUid", merchantUid);

        return "payment_completed";
    }

    @GetMapping(value = "/address")
    @ApiOperation(value = "주소검색",
            notes = "주소검색, 웹뷰에서 호출해야 함")
    public String findAddress(@ApiIgnore ModelMap model) {

        model.addAttribute("selluvhost", CommonConstant.SELLUV_HOST);

        return "find_address";
    }

    @GetMapping(value = "/share/{pdtUid}")
    @ApiOperation(value = "공유URL(API결과에 포함)",
            notes = "상품공유 페이지를 표시한다.")
    public String sharePdt(@ApiIgnore ModelMap model,
                           @PathVariable("pdtUid") int pdtUid) {

        model.addAttribute("selluvhost", CommonConstant.SELLUV_HOST);
        PdtDao pdt = pdtService.getPdtByPdtUid(pdtUid);
        if (pdt == null || pdt.getStatus() == 0) {
            model.addAttribute("isValid", false);
        } else {
            model.addAttribute("isValid", true);
            pdt.setProfileImg(StarmanHelper.getTargetImageUrl("pdt", pdtUid, pdt.getProfileImg()));
            model.addAttribute("pdt", pdt);
            model.addAttribute("pdtTitle", pdtService.getPdtTitleByPdtUid(pdtUid));
        }

        return "share_pdt";
    }
}
