package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.DealDao;
import com.kyad.selluvapi.dao.DeliveryHistoryDao;
import com.kyad.selluvapi.dto.deal.DealListDto;
import com.kyad.selluvapi.dto.deal.DealMiniDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import com.kyad.selluvapi.enumtype.DEAL_STATUS_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.jpa.DealRepository;
import com.kyad.selluvapi.jpa.DeliveryHistoryRepository;
import com.kyad.selluvapi.rest.common.exception.DealNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

@Service
public class DealService {

    @Autowired
    private DealRepository dealRepository;

    @Autowired
    private DeliveryHistoryRepository deliveryHistoryRepository;

    public DealDao save(DealDao dao) {
        return dealRepository.save(dao);
    }

    public DealDao getDealInfo(int dealUid) {
        DealDao deal = dealRepository.findOne(dealUid);
        if (deal == null || deal.getStatus() == 0) {
            throw new DealNotFoundException();
        }

        return deal;
    }

    public DeliveryHistoryDao saveDeliveryHistory(DeliveryHistoryDao dao) {
        return deliveryHistoryRepository.save(dao);
    }

    @Transactional
    public void deleteDeliveryHistory(int deliveryHistoryUid) {
        deliveryHistoryRepository.delete(deliveryHistoryUid);
    }

    public DeliveryHistoryDao getDeliveryHistoryByUsrUidAndDealUid(int usrUid, int dealUid) {
        return deliveryHistoryRepository.findTopByUsrUidAndDealUidOrderByRegTimeDesc(usrUid, dealUid);
    }

    public DealDao getDealByDealUid(int dealUid) {
        return dealRepository.findOne(dealUid);
    }

    public long countPaymentByUsrUid(int usrUid) {
        return dealRepository.countPaymentByUsrUid(usrUid);
    }

    public long countPaymentBySellerUsrUid(int sellerUsrUid) {
        return dealRepository.countPaymentBySellerUsrUid(sellerUsrUid);
    }

    public long countPaymentHistoryByUsrUid(int usrUid) {
        return dealRepository.countPaymentHistoryByUsrUid(usrUid);
    }

    public long countPaymentHistoryBySellerUsrUid(int sellerUsrUid) {
        return dealRepository.countPaymentHistoryBySellerUsrUid(sellerUsrUid);
    }

    public long countByUsrUidAndStatus(int usrUid, List<Integer> statusList) {
        return dealRepository.countByUsrUidAndStatus(usrUid, statusList);
    }

    public long countBySellerUsrUidAndStatus(int sellerUsrUid, List<Integer> statusList) {
        return dealRepository.countBySellerUsrUidAndStatus(sellerUsrUid, statusList);
    }

    public Page<DealListDto> findAllByUsrUid(Pageable pageable, int usrUid) {
        return dealRepository.findAllDealListByUsrUid(pageable, usrUid)
                .map(result -> convertListObjectToDealListDto((Object[]) result));
    }

    public Page<DealListDto> findAllBySellerUsrUid(Pageable pageable, int usrUid) {
        return dealRepository.findAllDealListBySellerUsrUid(pageable, usrUid)
                .map(result -> convertListObjectToDealListDto((Object[]) result));
    }

    public DealListDto convertListObjectToDealListDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        DealListDto dealListDto = new DealListDto();

        DealMiniDto dealMini = new DealMiniDto();
        dealMini.setDealUid((int) objects[i++]);
        dealMini.setRegTime((Timestamp) objects[i++]);
        dealMini.setPdtUid((int) objects[i++]);
        dealMini.setUsrUid((int) objects[i++]);
        dealMini.setSellerUsrUid((int) objects[i++]);
        dealMini.setBrandEn((String) objects[i++]);
        dealMini.setPdtTitle((String) objects[i++]);
        dealMini.setPdtSize((String) objects[i++]);
        dealMini.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", dealMini.getPdtUid(), (String) objects[i++]));
        dealMini.setPdtPrice(((BigInteger) objects[i++]).longValue());
        dealMini.setSendPrice(((BigInteger) objects[i++]).longValue());
        dealMini.setVerifyPrice(((BigInteger) objects[i++]).longValue());
        dealMini.setPrice(((BigInteger) objects[i++]).longValue());
        dealMini.setCashPrice(((BigInteger) objects[i++]).longValue());
        dealMini.setReqPrice(((BigInteger) objects[i++]).longValue());
        dealMini.setPayTime((Timestamp) objects[i++]);
        dealMini.setDeliveryTime((Timestamp) objects[i++]);
        dealMini.setCompTime((Timestamp) objects[i++]);
        dealMini.setCashCompTime((Timestamp) objects[i++]);
        dealMini.setStatus((int) objects[i++]);
        dealListDto.setDeal(dealMini);

        UsrMiniDto usrMini = new UsrMiniDto();
        usrMini.setUsrUid((int) objects[i++]);
        usrMini.setUsrId((String) objects[i++]);
        usrMini.setUsrNckNm((String) objects[i++]);
        usrMini.setProfileImg(StarmanHelper.getTargetImageUrl("usr", usrMini.getUsrUid(), (String) objects[i++]));
        dealListDto.setUsr(usrMini);

        return dealListDto;
    }

    public List<DealDao> findAllUnUpdatedDeliveryNumber(Timestamp targetDate) {
        return dealRepository.findAllUnUpdatedDeliveryNumber(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode(), targetDate);
    }

    public List<DealDao> findAllUnUpdatedDealCompleted(Timestamp targetDate) {
        return dealRepository.findAllUnUpdatedDealCompleted(DEAL_STATUS_TYPE.DELIVERY_COMPLETE.getCode(), targetDate);
    }

    public List<DealDao> findAllUnUpdatedNego(int status, Timestamp targetDate) {
        return dealRepository.findAllUnUpdatedNego(status, targetDate);
    }

    public List<DealDao> findAllDeliveryProcessing() {
        return dealRepository.findAllDeliveryProcessing();
    }

}
