package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DeliveryCompaniesDto {
    @ApiModelProperty("택배사 목록")
    private List<String> companies;
}
