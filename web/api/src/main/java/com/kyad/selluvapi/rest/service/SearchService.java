package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dto.search.MinMaxDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluvapi.enumtype.SEARCH_ORDER_TYPE;
import com.kyad.selluvapi.helper.StringHelper;
import com.kyad.selluvapi.request.SearchReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchService {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PdtService pdtService;

    @Transactional
    public Page<PdtListDto> searchByCondition(Pageable pageable, int usrUid, SearchReq searchReq) {

        String customWhere = getCustomWhereSql(searchReq);

        String customOrderBy;
        if (searchReq.getSearchOrderType().getCode() == SEARCH_ORDER_TYPE.RECOMMEND.getCode()) {
            customOrderBy = " ORDER BY p.edt_time DESC ";
        } else if (searchReq.getSearchOrderType().getCode() == SEARCH_ORDER_TYPE.NEW.getCode()) {
            customOrderBy = " ORDER BY p.reg_time DESC ";
        } else if (searchReq.getSearchOrderType().getCode() == SEARCH_ORDER_TYPE.FAME.getCode()) {
            customOrderBy = " ORDER BY p.fame_score DESC ";
        } else if (searchReq.getSearchOrderType().getCode() == SEARCH_ORDER_TYPE.PRICELOW.getCode()) {
            customOrderBy = " ORDER BY p.price ASC ";
        } else {
            customOrderBy = " ORDER BY p.price DESC ";
        }

        String sql = getBasicPdtSql(customWhere, customOrderBy);
        String countSql = getBasicCountSql(customWhere);

        Query query = entityManager.createNativeQuery(sql);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        Query countQuery = entityManager.createNativeQuery(countSql);

        query.setParameter("usrUid", usrUid);
        countQuery.setParameter("usrUid", usrUid);

        try {
            query.setParameter("keyword", "%" + searchReq.getKeyword() + "%");
            countQuery.setParameter("keyword", "%" + searchReq.getKeyword() + "%");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            List<Integer> categoryList = categoryService.getSubAllCategoryUidsWithIt(searchReq.getCategoryUid());
            query.setParameter("categoryList", categoryList);
            countQuery.setParameter("categoryList", categoryList);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            query.setParameter("brandList", searchReq.getBrandUidList());
            countQuery.setParameter("brandList", searchReq.getBrandUidList());
        } catch (IllegalArgumentException ignored) {
        }

        try {
            String pdtSizeRegexp = StringHelper.join(searchReq.getPdtSizeList(), "|");
            query.setParameter("pdtSizeRegexp", pdtSizeRegexp);
            countQuery.setParameter("pdtSizeRegexp", pdtSizeRegexp);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            List<Integer> conditionList = getConditionList(searchReq.getPdtConditionList());
            query.setParameter("conditionList", conditionList);
            countQuery.setParameter("conditionList", conditionList);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            query.setParameter("colorList", searchReq.getPdtColorList());
            countQuery.setParameter("colorList", searchReq.getPdtColorList());
        } catch (IllegalArgumentException ignored) {
        }

        Page<Object> searchResultList = new PageImpl<Object>(query.getResultList(), pageable,
                ((BigInteger) countQuery.getSingleResult()).longValue());

        return searchResultList.map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public long countByCondition(int usrUid, SearchReq searchReq) {
        String customWhere = getCustomWhereSql(searchReq);
        String countSql = getBasicCountSql(customWhere);

        Query countQuery = entityManager.createNativeQuery(countSql);

        countQuery.setParameter("usrUid", usrUid);

        try {
            countQuery.setParameter("keyword", "%" + searchReq.getKeyword() + "%");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            List<Integer> categoryList = categoryService.getSubAllCategoryUidsWithIt(searchReq.getCategoryUid());
            countQuery.setParameter("categoryList", categoryList);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            countQuery.setParameter("brandList", searchReq.getBrandUidList());
        } catch (IllegalArgumentException ignored) {
        }

        try {
            String pdtSizeRegexp = StringHelper.join(searchReq.getPdtSizeList(), "|");
            countQuery.setParameter("pdtSizeRegexp", pdtSizeRegexp);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            countQuery.setParameter("conditionList", getConditionList(searchReq.getPdtConditionList()));
        } catch (IllegalArgumentException ignored) {
        }

        try {
            countQuery.setParameter("colorList", searchReq.getPdtColorList());
        } catch (IllegalArgumentException ignored) {
        }

        return ((BigInteger) countQuery.getSingleResult()).longValue();
    }

    public MinMaxDto getPriceRangeByCondition(int usrUid, SearchReq searchReq) {
        searchReq.setPdtPriceMin(-1);
        searchReq.setPdtPriceMax(-1);

        String customWhere = getCustomWhereSql(searchReq);
        String minMaxSql = "SELECT IFNULL(MIN(p.price), 0) as min_price, IFNULL(MAX(p.price), 0) as max_price FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " JOIN category c ON p.category_uid = c.category_uid " +
                " WHERE p.photoshop_yn = 1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = :usrUid AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = :usrUid)) ";

        Query minMaxQuery = entityManager.createNativeQuery(minMaxSql);

        minMaxQuery.setParameter("usrUid", usrUid);

        try {
            minMaxQuery.setParameter("keyword", "%" + searchReq.getKeyword() + "%");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            List<Integer> categoryList = categoryService.getSubAllCategoryUidsWithIt(searchReq.getCategoryUid());
            minMaxQuery.setParameter("categoryList", categoryList);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            minMaxQuery.setParameter("brandList", searchReq.getBrandUidList());
        } catch (IllegalArgumentException ignored) {
        }

        try {
            String pdtSizeRegexp = StringHelper.join(searchReq.getPdtSizeList(), "|");
            minMaxQuery.setParameter("pdtSizeRegexp", pdtSizeRegexp);
        } catch (IllegalArgumentException ignored) {
        }

        try {
            minMaxQuery.setParameter("conditionList", getConditionList(searchReq.getPdtConditionList()));
        } catch (IllegalArgumentException ignored) {
        }

        try {
            minMaxQuery.setParameter("colorList", searchReq.getPdtColorList());
        } catch (IllegalArgumentException ignored) {
        }


        Object[] objects = (Object[])minMaxQuery.getSingleResult();
        MinMaxDto dto = new MinMaxDto();
        dto.setMin(((BigInteger) objects[0]).longValue());
        dto.setMax(((BigInteger) objects[1]).longValue());

        return dto;
    }

    private String getCustomWhereSql(SearchReq searchReq) {
        String customWhere = "";

        if (searchReq.getKeyword() != null && !searchReq.getKeyword().isEmpty()) {
            customWhere += " AND CONCAT(b.name_en, ' ', b.name_ko, ' ', (CASE WHEN p.pdt_group = 1 THEN '남성' WHEN p.pdt_group = 2 THEN '여성' ELSE '키즈' END), " +
                    " ' ', p.color_name, ' ', p.pdt_model, ' ', c.category_name, ' ', p.tag, ' ', p.pdt_size ) LIKE :keyword ";
        }

        if (searchReq.getCategoryUid() != 0) {
            customWhere += " AND p.category_uid in :categoryList ";
        }

        if (searchReq.getBrandUidList() != null && searchReq.getBrandUidList().size() > 0) {
            customWhere += " AND p.brand_uid in :brandList ";
        }

        if (searchReq.getPdtSizeList() != null && searchReq.getPdtSizeList().size() > 0) {
            customWhere += " AND p.pdt_size REGEXP :pdtSizeRegexp ";
        }

        if (searchReq.getPdtConditionList() != null && searchReq.getPdtConditionList().size() >0) {
            customWhere += " AND p.pdt_condition in :conditionList ";
        }

        if (searchReq.getPdtColorList() != null && searchReq.getPdtColorList().size() > 0) {
            customWhere += " AND p.color_name in :colorList ";
        }

        if (searchReq.getPdtPriceMin() != -1) {
            customWhere += " AND p.price >= " + searchReq.getPdtPriceMin() + " ";
        }

        if (searchReq.getPdtPriceMax() != -1) {
            customWhere += " AND p.price <= " + searchReq.getPdtPriceMax() + " ";
        }

        if (searchReq.isSoldOutExpect()) {
            customWhere += " AND p.status = 1 ";
        } else {
            customWhere += " AND p.status <> 0 ";
        }

        return customWhere;
    }

    private String getBasicPdtSql(String customWhere, String customOrderBy) {
        String sql = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
                " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
                " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
                " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
                " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = :usrUid) AS pdt_like_status, " +
                " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
                " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
                " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " JOIN category c ON p.category_uid = c.category_uid " +
                " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
                " WHERE p.photoshop_yn = 1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = :usrUid AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = :usrUid)) " +
                customOrderBy;

        return sql;
    }

    private String getBasicCountSql(String customWhere) {
        String countSql = "SELECT count(*) FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " JOIN category c ON p.category_uid = c.category_uid " +
                " WHERE p.photoshop_yn = 1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = :usrUid AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = :usrUid)) ";

        return countSql;
    }

    private List<Integer> getConditionList(List<PDT_CONDITION_TYPE> pdtConditionTypeList) {
        List<Integer> conditionList = new ArrayList<>();
        for(PDT_CONDITION_TYPE pdtConditionType : pdtConditionTypeList) {
            conditionList.add(pdtConditionType.getCode());
        }
        return conditionList;
    }
}
