package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "event", schema = "selluv")
public class EventDao {
    private int eventUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("마이페이지 노출여부 1-노출, 0-노출안함")
    private int mypageYn;
    @ApiModelProperty("타이틀")
    private String title;
    @ApiModelProperty("썸네일이미지")
    private String profileImg;
    @JsonIgnore
    private String content;
    @ApiModelProperty("상세이미지")
    private String detailImg;
    @JsonIgnore
    private Timestamp startTime;
    @JsonIgnore
    private Timestamp endTime;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_uid", nullable = false)
    public int getEventUid() {
        return eventUid;
    }

    public void setEventUid(int eventUid) {
        this.eventUid = eventUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "mypage_yn", nullable = false)
    public int getMypageYn() {
        return mypageYn;
    }

    public void setMypageYn(int mypageYn) {
        this.mypageYn = mypageYn;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "detail_img", nullable = false, length = 255)
    public String getDetailImg() {
        return detailImg;
    }

    public void setDetailImg(String detailImg) {
        this.detailImg = detailImg;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = false)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDao eventDao = (EventDao) o;
        return eventUid == eventDao.eventUid &&
                mypageYn == eventDao.mypageYn &&
                status == eventDao.status &&
                Objects.equals(regTime, eventDao.regTime) &&
                Objects.equals(title, eventDao.title) &&
                Objects.equals(profileImg, eventDao.profileImg) &&
                Objects.equals(content, eventDao.content) &&
                Objects.equals(detailImg, eventDao.detailImg) &&
                Objects.equals(startTime, eventDao.startTime) &&
                Objects.equals(endTime, eventDao.endTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(eventUid, regTime, mypageYn, title, profileImg, content, detailImg, startTime, endTime, status);
    }
}
