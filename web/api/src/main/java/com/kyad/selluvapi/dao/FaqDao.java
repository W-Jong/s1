package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "faq", schema = "selluv")
public class FaqDao {
    private int faqUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("분류 1-판매 2-구매 3-활동")
    private int kind;
    @ApiModelProperty("타이틀")
    private String title;
    @ApiModelProperty("내용")
    private String content;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "faq_uid", nullable = false)
    public int getFaqUid() {
        return faqUid;
    }

    public void setFaqUid(int faqUid) {
        this.faqUid = faqUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FaqDao faqDao = (FaqDao) o;
        return faqUid == faqDao.faqUid &&
                kind == faqDao.kind &&
                status == faqDao.status &&
                Objects.equals(regTime, faqDao.regTime) &&
                Objects.equals(title, faqDao.title) &&
                Objects.equals(content, faqDao.content);
    }

    @Override
    public int hashCode() {

        return Objects.hash(faqUid, regTime, kind, title, content, status);
    }
}
