package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrPdtWishDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrPdtWishRepository extends CrudRepository<UsrPdtWishDao, Integer> {

    @Query("select a from UsrPdtWishDao a")
    Page<UsrPdtWishDao> findAll(Pageable pageable);

    UsrPdtWishDao getByUsrUidAndPdtUid(int usrUid, int pdtUid);

    long countByPdtUid(int pdtUid);
}
