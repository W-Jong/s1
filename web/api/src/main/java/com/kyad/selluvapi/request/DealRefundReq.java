package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class DealRefundReq {
    @ApiModelProperty("반품사유")
    @NotEmpty
    private String refundReason;
    @NotNull
    @Size(min = 1)
    @ApiModelProperty("반품이미지 리스트")
    private List<String> photos;
}
