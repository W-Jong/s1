package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_wish", schema = "selluv")
public class UsrWishDao {
    private int usrWishUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("유저UID")
    private int usrUid;
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    @ApiModelProperty("상품군 1-남성, 2-여성, 4-키즈")
    private int pdtGroup;
    @ApiModelProperty("카테고리UID")
    private int categoryUid;
    @ApiModelProperty("모델명")
    private String pdtModel = "";
    @ApiModelProperty("사이즈")
    private String pdtSize = "";
    @ApiModelProperty("컬러")
    private String colorName = "";
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_wish_uid", nullable = false)
    public int getUsrWishUid() {
        return usrWishUid;
    }

    public void setUsrWishUid(int usrWishUid) {
        this.usrWishUid = usrWishUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "brand_uid", nullable = false)
    public int getBrandUid() {
        return brandUid;
    }

    public void setBrandUid(int brandUid) {
        this.brandUid = brandUid;
    }

    @Basic
    @Column(name = "pdt_group", nullable = false)
    public int getPdtGroup() {
        return pdtGroup;
    }

    public void setPdtGroup(int pdtGroup) {
        this.pdtGroup = pdtGroup;
    }

    @Basic
    @Column(name = "category_uid", nullable = false)
    public int getCategoryUid() {
        return categoryUid;
    }

    public void setCategoryUid(int categoryUid) {
        this.categoryUid = categoryUid;
    }

    @Basic
    @Column(name = "pdt_model", nullable = false, length = 63)
    public String getPdtModel() {
        return pdtModel;
    }

    public void setPdtModel(String pdtModel) {
        this.pdtModel = pdtModel;
    }

    @Basic
    @Column(name = "pdt_size", nullable = false, length = 63)
    public String getPdtSize() {
        return pdtSize;
    }

    public void setPdtSize(String pdtSize) {
        this.pdtSize = pdtSize;
    }

    @Basic
    @Column(name = "color_name", nullable = false, length = 30)
    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrWishDao that = (UsrWishDao) o;
        return usrWishUid == that.usrWishUid &&
                usrUid == that.usrUid &&
                brandUid == that.brandUid &&
                pdtGroup == that.pdtGroup &&
                categoryUid == that.categoryUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(pdtModel, that.pdtModel) &&
                Objects.equals(pdtSize, that.pdtSize) &&
                Objects.equals(colorName, that.colorName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrWishUid, regTime, usrUid, brandUid, pdtGroup, categoryUid, pdtModel, pdtSize, colorName, status);
    }
}
