package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.ReplyDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.pdt.ReplyDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import com.kyad.selluvapi.jpa.ReplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReplyService {

    @Autowired
    private ReplyRepository replyRepository;

    @Autowired
    private UsrService usrService;

    public Page<ReplyDao> findAll(Pageable pageable) {
        return replyRepository.findAll(pageable);
    }

    public ReplyDao save(ReplyDao dao) {
        return replyRepository.save(dao);
    }

    public void delete(int ReplyUid) {
        replyRepository.delete(ReplyUid);
    }

    public long countByPdtUid(int pdtUid) {
        return replyRepository.countByPdtUid(pdtUid);
    }

    public Page<ReplyDto> findAllByPdtUid(Pageable pageable, int pdtUid) {
        return replyRepository.findAllByPdtUid(pageable, pdtUid).map(result -> {
            if (result == null)
                return null;

            Object[] objects = (Object[]) result;
            ReplyDto dto = new ReplyDto();
            dto.setReply((ReplyDao) objects[0]);
            dto.setUsr(usrService.convertUsrDaoToUsrMiniDto((UsrDao) objects[1]));
            return dto;
        });
    }

    public ReplyDto convertReplyDaoAndUsrDaoToReplyDto(ReplyDao reply, UsrDao usr) {
        if (reply == null)
            return null;
        ReplyDto dto = new ReplyDto();
        dto.setReply(reply);
        dto.setUsr(usrService.convertUsrDaoToUsrMiniDto(usr));

        return dto;
    }

    public ReplyDao findOne(int replyUid) {
        return replyRepository.findOne(replyUid);
    }
}
