package com.kyad.selluvapi.enumtype;

public enum DEAL_STATUS_TYPE {
    DELETED(0), DELIVERY_PREPARE(1), DELIVERY_PROGRESS(2), PDT_VERIFY(3),
    DELIVERY_COMPLETE(4), DEAL_COMPLETE(5), CASH_COMPLETE(6),
    REFUND_REQUEST(7), REFUND_ALLOW(8), REFUND_COMPLETE(9),
    DEAL_CANCEL(10), NEGO_REQUEST(11), COUNTER_NEGO_REQUEST(12);

    private int code;

    private DEAL_STATUS_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
