package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "statistics_cache", schema = "selluv")
public class StatisticsCacheDao {
    private int statisticsCacheUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private Date targetDate;
    private long dealCount;
    private long dealAmount;
    private long joinCount;
    private long visitUserCount;
    private long visitCount;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "statistics_cache_uid", nullable = false)
    public int getStatisticsCacheUid() {
        return statisticsCacheUid;
    }

    public void setStatisticsCacheUid(int statisticsCacheUid) {
        this.statisticsCacheUid = statisticsCacheUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "target_date", nullable = false)
    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    @Basic
    @Column(name = "deal_count", nullable = false)
    public long getDealCount() {
        return dealCount;
    }

    public void setDealCount(long dealCount) {
        this.dealCount = dealCount;
    }

    @Basic
    @Column(name = "deal_amount", nullable = false)
    public long getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(long dealAmount) {
        this.dealAmount = dealAmount;
    }

    @Basic
    @Column(name = "join_count", nullable = false)
    public long getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(long joinCount) {
        this.joinCount = joinCount;
    }

    @Basic
    @Column(name = "visit_user_count", nullable = false)
    public long getVisitUserCount() {
        return visitUserCount;
    }

    public void setVisitUserCount(long visitUserCount) {
        this.visitUserCount = visitUserCount;
    }

    @Basic
    @Column(name = "visit_count", nullable = false)
    public long getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(long visitCount) {
        this.visitCount = visitCount;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticsCacheDao that = (StatisticsCacheDao) o;
        return statisticsCacheUid == that.statisticsCacheUid &&
                dealCount == that.dealCount &&
                dealAmount == that.dealAmount &&
                joinCount == that.joinCount &&
                visitUserCount == that.visitUserCount &&
                visitCount == that.visitCount &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(targetDate, that.targetDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(statisticsCacheUid, regTime, targetDate, dealCount, dealAmount, joinCount, visitUserCount, visitCount, status);
    }
}
