package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.DealDao;
import com.kyad.selluvapi.dao.MoneyChangeHisDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.other.UsrMoneyHisDto;
import com.kyad.selluvapi.enumtype.MONEY_HIS_TYPE;
import com.kyad.selluvapi.jpa.MoneyChangeHisRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigDecimal;

@Service
public class MoneyChangeHisService {

    @Autowired
    private MoneyChangeHisRepository moneyChangeHisRepository;

    @Autowired
    private UsrService usrService;

    @Autowired
    private EntityManager entityManager;

    @Transactional
    public void insertPointHistory(UsrDao usr, long deltaPoint, MONEY_HIS_TYPE moneyHisType, DealDao deal, String content) {
        if (deltaPoint == 0)
            return;

        if ("".equals(content)) {
            if (moneyHisType.getCode() == MONEY_HIS_TYPE.MONEYUSE.getCode())
                content = "셀럽머니 사용";
            else if (moneyHisType.getCode() == MONEY_HIS_TYPE.SELLMONEY.getCode())
                content = "판매대금 정산";
            else if (moneyHisType.getCode() == MONEY_HIS_TYPE.REVIEWREWARD.getCode())
                content = "거래후기 리워드 지급";
            else if (moneyHisType.getCode() == MONEY_HIS_TYPE.BUYREWARD.getCode())
                content = "구매적립금 지급";
        }
        MoneyChangeHisDao moneyChangeHisDao = new MoneyChangeHisDao();
        moneyChangeHisDao.setUsrUid(usr.getUsrUid());
        moneyChangeHisDao.setKind(moneyHisType.getCode());
        moneyChangeHisDao.setTargetUid(deal.getDealUid());
        moneyChangeHisDao.setPdtUid("" + deal.getPdtUid());
        moneyChangeHisDao.setContent(content);
        moneyChangeHisDao.setAmount(deltaPoint);
        moneyChangeHisRepository.save(moneyChangeHisDao);

        String sql = "update usr set money = money + (" + deltaPoint + ") where usr_uid = " + usr.getUsrUid();
        entityManager.createNativeQuery(sql).executeUpdate();
    }

    public void insertPointHistory(UsrDao usr, long deltaPoint, MONEY_HIS_TYPE moneyHisType, DealDao deal) {
        insertPointHistory(usr, deltaPoint, moneyHisType, deal, "");
    }

    public long getTotalCashbackMoneyByUsrUid(int usrUid) {
        String sql = "SELECT ifnull(-sum(amount), 0) AS total_amount FROM money_change_his WHERE usr_uid = ?1 AND kind = 2";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, usrUid);
        try {
            return ((BigDecimal) query.getSingleResult()).longValue();
        } catch (NoResultException e) {
            return 0;
        }
    }

    public Page<UsrMoneyHisDto> getHistoryList(Pageable pageable, int usrUid) {
        return moneyChangeHisRepository.findAllByUsrUidOrderByRegTimeDesc(pageable, usrUid)
                .map(this::convertMoneyChangeDaoToUsrMoneyHisDto);
    }

    public UsrMoneyHisDto convertMoneyChangeDaoToUsrMoneyHisDto(MoneyChangeHisDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        UsrMoneyHisDto dto = modelMapper.map(dao, UsrMoneyHisDto.class);
        if (dto.getKind() == MONEY_HIS_TYPE.INVITEREWARD.getCode() ||
                dto.getKind() == MONEY_HIS_TYPE.JOINREWARD.getCode() ||
                dto.getKind() == MONEY_HIS_TYPE.INVITEFIRSTSELL.getCode()) {
            UsrDao targetUsr = usrService.getByUsrUid(dto.getTargetUid());
            if (targetUsr == null)
                dto.setHintContent("(탈퇴한 유저)");
            else {
                if (dto.getKind() == MONEY_HIS_TYPE.JOINREWARD.getCode())
                    dto.setHintContent(String.format("(추천인코드:%s)", targetUsr.getUsrId()));
                else
                    dto.setHintContent(String.format("(가입유저:%s)", targetUsr.getUsrId()));
            }
        } else {
            dto.setHintContent(String.format("(#%010d)", dto.getTargetUid()));
        }

        return dto;
    }
}
