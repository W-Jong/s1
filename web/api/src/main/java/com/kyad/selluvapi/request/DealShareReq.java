package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class DealShareReq {
    @NotNull
    @ApiModelProperty("인스타그램 공유이미지 리스트")
    private List<String> instagramPhotos;
    @NotNull
    @ApiModelProperty("트위터 공유이미지 리스트")
    private List<String> twitterPhotos;
    @NotNull
    @ApiModelProperty("네이버블로그 공유이미지 리스트")
    private List<String> naverblogPhotos;
    @NotNull
    @ApiModelProperty("카카오스토리 공유이미지 리스트")
    private List<String> kakaostoryPhotos;
    @NotNull
    @ApiModelProperty("페이스북 공유이미지 리스트")
    private List<String> facebookPhotos;
}
