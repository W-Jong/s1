package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "color", schema = "selluv")
public class ColorDao {
    private int colorUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String colorName;
    private String colorCode;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "color_uid", nullable = false)
    public int getColorUid() {
        return colorUid;
    }

    public void setColorUid(int colorUid) {
        this.colorUid = colorUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "color_name", nullable = false, length = 30)
    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    @Basic
    @Column(name = "color_code", nullable = false, length = 20)
    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColorDao colorDao = (ColorDao) o;
        return colorUid == colorDao.colorUid &&
                status == colorDao.status &&
                Objects.equals(regTime, colorDao.regTime) &&
                Objects.equals(colorName, colorDao.colorName) &&
                Objects.equals(colorCode, colorDao.colorCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(colorUid, regTime, colorName, colorCode, status);
    }
}
