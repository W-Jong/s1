package com.kyad.selluvapi.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class WorkingDayReq {
    @ApiModelProperty("기준날짜 (형식:2018-01-01)")
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate basisDate;

    @ApiModelProperty("변경 날짜수 -30~30사이만 허용")
    @Range(min = -30, max = 30)
    private int deltaDays;
}
