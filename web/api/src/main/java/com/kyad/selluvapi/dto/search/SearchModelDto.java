package com.kyad.selluvapi.dto.search;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SearchModelDto {
    @ApiModelProperty("모델명")
    private String modelName;
    @ApiModelProperty("브랜드명")
    private String brandName;
}
