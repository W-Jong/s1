package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class MultiStringReq {
    @ApiModelProperty("string 배열")
    @NotNull
    @Size(min = 1)
    private List<String> list;
}
