package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class UsrMoneyHisContentDto {
    @ApiModelProperty("셀럽머니")
    private long money;
    @ApiModelProperty("판매정산")
    private long cashbackMoney;
    @ApiModelProperty("상세내역")
    private Page<UsrMoneyHisDto> history;
}
