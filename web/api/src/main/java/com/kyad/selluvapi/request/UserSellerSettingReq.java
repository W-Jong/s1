package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class UserSellerSettingReq {

    @NotNull
    @ApiModelProperty("무료배송여부")
    private Boolean isFreeSend;
    @NotNull
    @Min(0)
    @ApiModelProperty("무료배송가격, isFreeSend가 true일때만 유효")
    private long freeSendPrice;
    @NotNull
    @ApiModelProperty("휴가모두여부")
    private Boolean isSleep;
    @Range(min = 0, max = 30)
    @ApiModelProperty("타임아웃 날짜, isSleep이 true일때만 유효")
    private int timeOutDate;
}
