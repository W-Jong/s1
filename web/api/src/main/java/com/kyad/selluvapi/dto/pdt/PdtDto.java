package com.kyad.selluvapi.dto.pdt;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class PdtDto {
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime;
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    @ApiModelProperty("상품군 1-남성, 2-여성, 4-키즈")
    private int pdtGroup;
    @ApiModelProperty("카테고리UID")
    private int categoryUid;
    @ApiModelProperty("사이즈")
    private String pdtSize;
    @ApiModelProperty("대표사진")
    private String profileImg;
    @ApiModelProperty("사진리스트")
//    private String photos;
    private List<String> photoList;
    @ApiModelProperty("포토샵전후여부 1-포토샵후 2-포토샵전")
    private int photoshopYn;
    @ApiModelProperty("변경전 가격")
    private long originPrice;
    @ApiModelProperty("판매가격")
    private long price;
    @ApiModelProperty("배송비")
    private long sendPrice;
    @ApiModelProperty("컨디션 1-새상품, 2-최상, 3-상, 4-중상")
    private int pdtCondition;
    @ApiModelProperty("구성품 6자리문자열(0-없음, 1-있음)  첫-상품택, 둘-게런티카드, 셋-영수증, 넷-여분부속품, 다섯-브랜드박스, 여섯-더스트백")
    private String component;
    @ApiModelProperty("기타")
    private String etc;
    @ApiModelProperty("컬러명")
    private String colorName;
    @ApiModelProperty("모델명")
    private String pdtModel;
    //    private String promotionCode;
    @ApiModelProperty("상세설명")
    private String content;
    @ApiModelProperty("태그, #으로 시작 및 공백구분")
    private String tag;
    @ApiModelProperty("서명사진")
    private String signImg;
    @ApiModelProperty("발렛UID")
    private int valetUid;
    //    private String memo;
    @ApiModelProperty("업데이트 시간")
    private Timestamp edtTime;
    @ApiModelProperty("인기점수")
    private int fameScore;
    @ApiModelProperty("네고허용여부 1-허용 2-비허용")
    private int negoYn;
    @ApiModelProperty("상태  0-삭제, 1-판매중 2-판매완료, 3-판매중지, 4-휴가모드")
    private int status;
}
