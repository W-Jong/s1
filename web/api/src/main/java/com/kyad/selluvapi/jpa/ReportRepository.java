package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ReportDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends CrudRepository<ReportDao, Integer> {

    @Query("select a from ReportDao a")
    Page<ReportDao> findAll(Pageable pageable);
}
