package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AlarmCountDto {
    @ApiModelProperty("전체 알림갯수")
    private long totalCount;
    @ApiModelProperty("거래 알림갯수")
    private long dealCount;
    @ApiModelProperty("소식 알림갯수")
    private long newsCount;
    @ApiModelProperty("댓글 알림갯수")
    private long replyCount;
}
