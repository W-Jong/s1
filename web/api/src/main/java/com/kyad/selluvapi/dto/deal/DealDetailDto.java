package com.kyad.selluvapi.dto.deal;

import com.kyad.selluvapi.dao.DealDao;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DealDetailDto {
    @ApiModelProperty("거래정보")
    private DealDao deal;
    @ApiModelProperty("구매자 또는 판매자정보")
    private UsrMiniDto usr;
    @ApiModelProperty("반품첨부이미지")
    private List<String> refundPhotos;
}