package com.kyad.selluvapi.rest.common;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.enumtype.*;
import com.kyad.selluvapi.helper.DateCalculatorHelper;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.request.CancelData;
import com.kyad.selluvapi.helper.sweettracker.SweetTrackerClientHelper;
import com.kyad.selluvapi.helper.sweettracker.SweetTrackerDeliveryCompanyHelper;
import com.kyad.selluvapi.helper.sweettracker.request.InvoiceReq;
import com.kyad.selluvapi.helper.sweettracker.response.SweetTrackerResponse;
import com.kyad.selluvapi.helper.sweettracker.response.TrackingInfo;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 스프링 스케줄러 / 쿼츠 크론 표현식
 * 자세히 : 초[0-59] 분[0-59] 시[0-23] 일[day-of-month:1-31] 월[1-12] 요일[day-of-week:0-6] 연도[생략가능]
 * 간단히 : 초 분 시 일 월 요일 연도
 */
@Service
public class Scheduler {

    private final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

//    @Autowired
//    private EntityManager entityManager;

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private DealService dealService;

    @Autowired
    private OtherService otherService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @Autowired
    private MoneyChangeHisService moneyChangeHisService;

    /**
     * *** DB Event로 처리됨.(call updateFameScoreForPdt) ***
     *
     * #매일 12시 상품 인기카운트((좋아요카운트)+(잇템카운트)) 재설정
     * UPDATE pdt p
     * SET p.fame_score = (
     * (SELECT
     * 	count( * )
     * FROM
     * 	usr_pdt_like upl
     * WHERE
     * 	upl.pdt_uid = p.pdt_uid
     * 	AND upl.reg_time > ( NOW() - INTERVAL 1 WEEK )
     * 	) + (
     * SELECT
     * 	count( * )
     * FROM
     * 	usr_pdt_wish upw
     * WHERE
     * 	upw.pdt_uid = p.pdt_uid
     * 	AND upw.reg_time > ( NOW() - INTERVAL 1 WEEK )
     * 	)
     * );
     */

    /**
     *
     * *** DB Event로 처리됨.(call insertAutoReview) ***
     *
     * #자동거래후기 작성
     * #구매자거래후기
     * INSERT INTO review ( usr_uid, deal_uid, peer_usr_uid, type, point ) SELECT
     * d.usr_uid,
     * d.deal_uid,
     * d.seller_usr_uid,
     * 1,
     * 1
     * FROM
     * 	deal d
     * WHERE
     * 	d.STATUS IN ( 5, 6 )
     * 	AND d.comp_time < ( NOW( ) - INTERVAL 3 DAY )
     * 	AND NOT EXISTS (
     * SELECT
     * 	r.review_uid
     * FROM
     * 	review r
     * WHERE
     * 	r.usr_uid = d.usr_uid
     * 	AND r.deal_uid = d.deal_uid
     * 	AND r.STATUS = 1
     * 	);#판매자거래후기
     * INSERT INTO review ( usr_uid, deal_uid, peer_usr_uid, type, point ) SELECT
     * d.seller_usr_uid,
     * d.deal_uid,
     * d.usr_uid,
     * 2,
     * 1
     * FROM
     * 	deal d
     * WHERE
     * 	d.STATUS IN ( 5, 6 )
     * 	AND d.comp_time < ( NOW( ) - INTERVAL 3 DAY )
     * 	AND NOT EXISTS (
     * SELECT
     * 	r.review_uid
     * FROM
     * 	review r
     * WHERE
     * 	r.usr_uid = d.seller_usr_uid
     * 	AND r.deal_uid = d.deal_uid
     * 	AND r.STATUS = 1
     * 	);
     */

    /**
     * 매일 0시 2분에 호출되는 메서드
     * 이벤트, 공지가 active되는 경우 알림을 보낸다.
     */
    @Scheduled(cron = "0 2 0 * * *")
    @Transactional
    public void updateAlarmScheduleForNoticeAndEvent() {
        Calendar cal = GregorianCalendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long todayTwelve = cal.getTime().getTime();

        List<EventDao> eventDaoList = otherService.getEventActiveList();
        List<NoticeDao> noticeDaoList = otherService.getNoticeActiveList();

        if (noticeDaoList != null && eventDaoList != null &&
                (noticeDaoList.size() + eventDaoList.size()) > 0) {

            List<UsrDao> usrDaoList = usrService.findAllActiveUser();
            for (UsrDao usr : usrDaoList) {
                for (EventDao event : eventDaoList) {
                    if (event.getStartTime().getTime() >= todayTwelve &&
                            event.getStartTime().getTime() < (todayTwelve + 24 * 3600 * 1000))
                        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH22_EVENT, usr, null, null, null, event.getTitle(), event.getEventUid());
                }
                for (NoticeDao notice : noticeDaoList) {
                    if (notice.getStartTime().getTime() >= todayTwelve &&
                            notice.getStartTime().getTime() < (todayTwelve + 24 * 3600 * 1000))
                        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH22_EVENT, usr, null, null, null, notice.getTitle(), notice.getNoticeUid());
                }
            }
        }
    }

    /**
     * 자동 구매취소 스케쥴러
     * 결제일로부터 영업일 기준 3일째 되는날 22시까지 운송장업데이트가 없을때 구매를 자동적으로 취소한다.
     * 매일 22시에 호출
     */

    @Scheduled(cron = "0 0 22 * * *")
    @Transactional
    public void autoPaymentCancelScheduler() {
        Date threeWorkingDaysAgoDate = new DateCalculatorHelper().getDelayedWorkingDate(-2);
        Timestamp threeWorkingDaysAgo = new Timestamp(threeWorkingDaysAgoDate.getTime());

        List<DealDao> dealDaoList = dealService.findAllUnUpdatedDeliveryNumber(threeWorkingDaysAgo);

        for (DealDao deal : dealDaoList) {
            PdtDao pdt = pdtService.getPdtInfo(deal.getPdtUid());

            //결제취소처리
            IamportClientHelper iamportClientHelper = new IamportClientHelper();
            CancelData cancelData = new CancelData(deal.getPayImportCode(), true);
            boolean isCancelSuccess = false;
            try {
                isCancelSuccess = (iamportClientHelper.cancelPayment(cancelData).getCode() == 0);
            } catch (Exception e) {
                isCancelSuccess = false;
                e.printStackTrace();
            }

            if (!isCancelSuccess) {
                //결제취소에 실패하면
                continue;
            }

            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason("자동주문취소");
            deal.setStatus(DEAL_STATUS_TYPE.DEAL_CANCEL.getCode());
            dealService.save(deal);

            pdt.setStatus(PDT_STATUS_TYPE.STOP.getCode());
            pdtService.save(pdt);

            deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));

            UsrDao usr = usrService.getByUsrUid(deal.getUsrUid());
            UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH04_B_DEAL_CANCELED, usr, sellerUsr, deal, null);
        }
    }


    /**
     * 자동 구매확정 스케쥴러
     * 배송완료후 3일후까지 거래완료로 변경하지 않을 경우 자동 구매확정
     * 매일 0시 호출
     */
    @Scheduled(cron = "0 0 0 * * *")
    @Transactional
    public void autoPaymentConfirmScheduler() {
        Timestamp threeWorkingDaysAgo = new Timestamp(System.currentTimeMillis() - 3 * 24 * 3600 * 1000);
        List<DealDao> dealDaoList = dealService.findAllUnUpdatedDealCompleted(threeWorkingDaysAgo);

        for (DealDao deal : dealDaoList) {

            deal.setCompTime(new Timestamp(System.currentTimeMillis()));
            deal.setStatus(DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode());
            dealService.save(deal);

            UsrDao usr = usrService.getByUsrUid(deal.getUsrUid());
            UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());

            //구매자 구매적립리워드
            int buyRewardPercent = Integer.parseInt(otherService.getSystemSetting(SYSTEM_SETTING_TYPE.DEAL_REWARD.getCode()).getSetting());
            long rewardPrice = deal.getPrice() * buyRewardPercent / 100;
            moneyChangeHisService.insertPointHistory(usr, rewardPrice, MONEY_HIS_TYPE.BUYREWARD, deal);

            deal.setPdtImg(StarmanHelper.getTargetImageUrl("pdt", deal.getPdtUid(), deal.getPdtImg()));
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH03_D_DEAL_COMPLETED, sellerUsr, usr, deal, null);
        }
    }

    /**
     * 자동 네고, 카운터네고거절 스케쥴러
     * 네고요청후 3일후까지 액션이 없을 경우 자동네고거절, 카운터네고거절
     * 매일 0시 10분 호출
     */
    @Scheduled(cron = "0 10 0 * * *")
    @Transactional
    public void autoNegoCanceledScheduler() {
        Timestamp threeWorkingDaysAgo = new Timestamp(System.currentTimeMillis() - 3 * 24 * 3600 * 1000);

        //네고거절
        List<DealDao> dealDaoList = dealService.findAllUnUpdatedNego(DEAL_STATUS_TYPE.NEGO_REQUEST.getCode(), threeWorkingDaysAgo);

        for (DealDao deal : dealDaoList) {

            deal.setCancelReason("네고거절(자동거절)");
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);

            UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
            UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH08_B_NEGO_UPDATED, buyerUsr, sellerUsr, deal, null);
        }

        //카운터네고거절
        dealDaoList = dealService.findAllUnUpdatedNego(DEAL_STATUS_TYPE.COUNTER_NEGO_REQUEST.getCode(), threeWorkingDaysAgo);
        for (DealDao deal : dealDaoList) {

            deal.setCancelReason("카운터네고거절(자동거절)");
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
        }
    }

    /**
     * 배송완료 확인 스케쥴러
     * 배송중에 있는
     * 매일 30분 2시간 간격 호출
     */
    @Scheduled(cron = "0 30 0/2 * * *")
    @Transactional
    public void autoDeliveryCompleteCheckScheduler() {
        List<DealDao> dealDaoList = dealService.findAllDeliveryProcessing();

        for (DealDao deal : dealDaoList) {
            String[] deliveryInfo = deal.getDeliveryNumber().split(" ");
            if (deliveryInfo.length < 2)
                continue;
            String deliveryCompany = deliveryInfo[0];
            String deliveryNumber = deliveryInfo[1];

            SweetTrackerClientHelper sweetTrackerClient = new SweetTrackerClientHelper();
            InvoiceReq invoiceReq = new InvoiceReq();
            invoiceReq.setTCode(SweetTrackerDeliveryCompanyHelper.getCompanyCode(deliveryCompany));
            invoiceReq.setTInvoice(deliveryNumber);

            SweetTrackerResponse<TrackingInfo> sweetTrackerResponse = sweetTrackerClient.getTrackingInfo(invoiceReq);
            if (sweetTrackerResponse.getCode() != SweetTrackerResponse.SWEET_TRACKER_API_200) {
                continue;
            }

            TrackingInfo trackingInfo = sweetTrackerResponse.getData();

            if ("Y".equals(trackingInfo.getCompleteYN())) {
                deal.setDeliveryTime(new Timestamp(System.currentTimeMillis()));
                deal.setStatus(DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode());
                dealService.save(deal);
            }
        }

    }
}
