package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrAddressDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsrAddressRepository extends CrudRepository<UsrAddressDao, Integer> {

    @Query("select a from UsrAddressDao a")
    Page<UsrAddressDao> findAll(Pageable pageable);

    @Query("select ua from UsrAddressDao ua where ua.usrUid = ?1 order by ua.addressOrder asc")
    List<UsrAddressDao> getAddressListByUsrUid(int usrUid);

    UsrAddressDao findByUsrUidAndAddressOrder(int usrUid, int addressOrder);

    @Query(value = "SELECT * FROM usr_address ua WHERE ua.usr_uid = ?1 ORDER BY ua.status DESC, ua.address_order ASC LIMIT 1", nativeQuery = true)
    UsrAddressDao findActiveAddressByUsrUid(int usrUid);
}
