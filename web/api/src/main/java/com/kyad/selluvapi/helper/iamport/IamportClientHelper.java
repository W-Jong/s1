package com.kyad.selluvapi.helper.iamport;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kyad.selluvapi.helper.iamport.request.*;
import com.kyad.selluvapi.helper.iamport.response.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;

public class IamportClientHelper {
    private static final String API_URL = "https://api.iamport.kr";
    // private static final String API_URL = "http://localhost:8888";
    private String api_key = null;
    private String api_secret = null;
    private HttpClient client = null;
    private Gson gson = new Gson();

    public IamportClientHelper() {
        this.api_key = "3966461868589664";
        this.api_secret = "mZaECL4VHGAajM1WEBKydn2pKNi5TjmfjwTViYR0ErbYecUTOEs22urMiJSc3frmMli5j95YBgWPtRJi";
        this.client = HttpClientBuilder.create().build();
    }

    public IamportClientHelper(String api_key, String api_secret) {
        this.api_key = api_key;
        this.api_secret = api_secret;
        this.client = HttpClientBuilder.create().build();
    }

    private IamportResponse<AccessToken> getAuth() throws Exception {
        AuthData authData = new AuthData(api_key, api_secret);

        String authJsonData = gson.toJson(authData);

        try {
            StringEntity data = new StringEntity(authJsonData, "UTF-8");

            HttpPost postRequest = new HttpPost(API_URL + "/users/getToken");
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Connection", "keep-alive");
            postRequest.setHeader("Content-Type", "application/json");

            postRequest.setEntity(data);

            HttpResponse response = client.execute(postRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            ResponseHandler<String> handler = new BasicResponseHandler();
            String body = handler.handleResponse(response);

            Type listType = new TypeToken<IamportResponse<AccessToken>>() {
            }.getType();
            IamportResponse<AccessToken> auth = gson.fromJson(body, listType);

            return auth;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String postRequest(String path, String token, StringEntity postData) {

        try {

            HttpPost postRequest = new HttpPost(API_URL + path);
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Connection", "keep-alive");
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.addHeader("Authorization", token);

            postRequest.setEntity(postData);

            HttpResponse response = client.execute(postRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            ResponseHandler<String> handler = new BasicResponseHandler();
            String body = handler.handleResponse(response);

            return body;

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getRequest(String path, String token) {

        try {

            HttpGet getRequest = new HttpGet(API_URL + path);
            getRequest.addHeader("Accept", "application/json");
            getRequest.addHeader("Authorization", token);

            HttpResponse response = client.execute(getRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            ResponseHandler<String> handler = new BasicResponseHandler();
            String body = handler.handleResponse(response);

            return body;

        } catch (ClientProtocolException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return null;
    }

    private String deleteRequest(String path, String token) {

        try {

            HttpDelete deleteRequest = new HttpDelete(API_URL + path);
            deleteRequest.addHeader("Accept", "application/json");
            deleteRequest.addHeader("Authorization", token);

            HttpResponse response = client.execute(deleteRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            ResponseHandler<String> handler = new BasicResponseHandler();
            String body = handler.handleResponse(response);

            return body;

        } catch (ClientProtocolException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return null;
    }

    public String getToken() throws Exception {

        IamportResponse<AccessToken> auth = this.getAuth();

        if (auth != null) {
            String token = auth.getResponse().getToken();
            return token;
        }

        return null;
    }

    public IamportResponse<PaymentAnnotation> paymentByImpUid(String impUid) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String path = "/payments/" + impUid;
            String response = this.getRequest(path, token);

            Type listType = new TypeToken<IamportResponse<PaymentAnnotation>>() {
            }.getType();
            IamportResponse<PaymentAnnotation> payment = gson.fromJson(response, listType);

            return payment;
        }
        return null;
    }

    public IamportResponse<PaymentAnnotation> paymentByMerchantUid(String merchantUid) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String path = "/payments/find/" + merchantUid;
            String response = this.getRequest(path, token);

            Type listType = new TypeToken<IamportResponse<PaymentAnnotation>>() {
            }.getType();
            IamportResponse<PaymentAnnotation> payment = gson.fromJson(response, listType);

            return payment;
        }

        return null;
    }

    public IamportResponse<PaymentAnnotation> cancelPayment(CancelData cancelData) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String cancelJsonData = gson.toJson(cancelData);
            StringEntity data = new StringEntity(cancelJsonData, "UTF-8");

            String response = this.postRequest("/payments/cancel", token, data);

            Type listType = new TypeToken<IamportResponse<PaymentAnnotation>>() {
            }.getType();
            IamportResponse<PaymentAnnotation> payment = gson.fromJson(response, listType);

            return payment;
        }
        return null;
    }

    public IamportResponse<CustomerAnnotation> addCardInfo(String customer_uid, CardData cardData) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String cardJsonData = gson.toJson(cardData);
            StringEntity data = new StringEntity(cardJsonData, "UTF-8");

            String response = this.postRequest("/subscribe/customers/" + customer_uid, token, data);

            Type listType = new TypeToken<IamportResponse<CustomerAnnotation>>() {
            }.getType();
            IamportResponse<CustomerAnnotation> customer = gson.fromJson(response, listType);

            return customer;
        }
        return null;
    }

    public IamportResponse<CustomerAnnotation> deleteCardInfo(String customer_uid) throws Exception {

        String token = this.getToken();

        if (token != null) {

            String response = this.deleteRequest("/subscribe/customers/" + customer_uid, token);

            Type listType = new TypeToken<IamportResponse<CustomerAnnotation>>() {
            }.getType();
            IamportResponse<CustomerAnnotation> customer = gson.fromJson(response, listType);

            return customer;
        }
        return null;
    }

    public IamportResponse<PaymentAnnotation> cardPaymentProcess(CardPayment cardPayment) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String cardPaymentData = gson.toJson(cardPayment);
            StringEntity data = new StringEntity(cardPaymentData, "UTF-8");

            String response = this.postRequest("/subscribe/payments/again", token, data);

            Type listType = new TypeToken<IamportResponse<PaymentAnnotation>>() {
            }.getType();
            IamportResponse<PaymentAnnotation> payment = gson.fromJson(response, listType);

            return payment;
        }
        return null;
    }

    public IamportResponse<EscrowLogisAnnotation> registerEscrowProcess(String imp_uid, EscrowData escrow) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String escrowData = gson.toJson(escrow);
            StringEntity data = new StringEntity(escrowData, "UTF-8");

            String response = this.postRequest("/subscribe/payments/again", token, data);

            Type listType = new TypeToken<IamportResponse<EscrowLogisAnnotation>>() {
            }.getType();
            IamportResponse<EscrowLogisAnnotation> escrowResult = gson.fromJson(response, listType);

            return escrowResult;
        }
        return null;
    }

    public IamportResponse<CertificationAnnotation> certificationByImpUid(String impUid) throws Exception {

        String token = this.getToken();

        if (token != null) {
            String path = "/certifications/" + impUid;
            String response = this.getRequest(path, token);

            Type listType = new TypeToken<IamportResponse<CertificationAnnotation>>() {
            }.getType();
            IamportResponse<CertificationAnnotation> certification = gson.fromJson(response, listType);

            return certification;
        }
        return null;
    }
}
