package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "search_word", schema = "selluv")
public class SearchWordDao {
    private int searchWordUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String content = "";
    private int pdtCount = 0;
    private String pdtImage = "";
    private int status = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "search_word_uid", nullable = false)
    public int getSearchWordUid() {
        return searchWordUid;
    }

    public void setSearchWordUid(int searchWordUid) {
        this.searchWordUid = searchWordUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 63)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "pdt_count", nullable = false)
    public int getPdtCount() {
        return pdtCount;
    }

    public void setPdtCount(int pdtCount) {
        this.pdtCount = pdtCount;
    }

    @Basic
    @Column(name = "pdt_image", nullable = false, length = 255)
    public String getPdtImage() {
        return pdtImage;
    }

    public void setPdtImage(String pdtImage) {
        this.pdtImage = pdtImage;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchWordDao that = (SearchWordDao) o;
        return searchWordUid == that.searchWordUid &&
                pdtCount == that.pdtCount &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(content, that.content) &&
                Objects.equals(pdtImage, that.pdtImage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(searchWordUid, regTime, content, pdtCount, pdtImage, status);
    }
}
