package com.kyad.selluvapi.enumtype;

public enum PDT_CONDITION_TYPE {
    NEW(1), BEST(2), GOOD(3), MIDDLE(4);

    private int code;

    private PDT_CONDITION_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
