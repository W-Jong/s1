package com.kyad.selluvapi.dto.pdt;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PdtFakeInfoDto {
    @ApiModelProperty("가품신고 누적수")
    private int totalCount;
    @ApiModelProperty("가품신고 리워드 금액")
    private long reward;
}
