package com.kyad.selluvapi.helper.sweettracker.response;

public class SweetTrackerResponse<T> {

    public static final int SWEET_TRACKER_API_200 = 200;
    public static final int SWEET_TRACKER_API_101 = 101;
    public static final int SWEET_TRACKER_API_102 = 102;
    public static final int SWEET_TRACKER_API_103 = 103;
    public static final int SWEET_TRACKER_API_104 = 104;
    public static final int SWEET_TRACKER_API_105 = 105;
    public static final int SWEET_TRACKER_API_106 = 106;
    public static final int SWEET_TRACKER_API_404 = 404;
    public static final int SWEET_TRACKER_API_500 = 500;
    public static final int SWEET_TRACKER_API_ERROR = 999;

    private int code;
    private String message;
    private T data;

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public SweetTrackerResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public SweetTrackerResponse(int code, String message) {
        this(code, message, null);
    }

    public SweetTrackerResponse() {
    }
}
