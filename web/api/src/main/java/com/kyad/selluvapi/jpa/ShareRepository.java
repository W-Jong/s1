package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ShareDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends CrudRepository<ShareDao, Integer> {

    @Query("select a from ShareDao a")
    Page<ShareDao> findAll(Pageable pageable);
}
