package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.MoneyChangeHisDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface MoneyChangeHisRepository extends CrudRepository<MoneyChangeHisDao, Integer> {

    @Query("select a from MoneyChangeHisDao a")
    Page<MoneyChangeHisDao> findAll(Pageable pageable);

    Page<MoneyChangeHisDao> findAllByUsrUidOrderByRegTimeDesc(Pageable pageable, int usrUid);
}
