package com.kyad.selluvapi.dto.deal;

import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DealListDto {
    @ApiModelProperty("거래정보")
    private DealMiniDto deal;
    @ApiModelProperty("구매자 또는 판매자정보")
    private UsrMiniDto usr;
}