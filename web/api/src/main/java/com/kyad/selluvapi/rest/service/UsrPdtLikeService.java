package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrPdtLikeDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.jpa.UsrPdtLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;

@Service
public class UsrPdtLikeService {

    @Autowired
    private UsrPdtLikeRepository usrPdtLikeRepository;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private EntityManager entityManager;

    public Page<UsrPdtLikeDao> findAll(Pageable pageable) {
        return usrPdtLikeRepository.findAll(pageable);
    }

    public UsrPdtLikeDao getByUsrUidAndPdtUid(int usrUid, int pdtUid) {
        return usrPdtLikeRepository.getByUsrUidAndPdtUid(usrUid, pdtUid);
    }

    public long countByPdtUid(int pdtUid) {
        return usrPdtLikeRepository.countByPdtUid(pdtUid);
    }

    public UsrPdtLikeDao save(UsrPdtLikeDao dao) {
        return usrPdtLikeRepository.save(dao);
    }

    public void delete(int usrPdtLikeUid) {
        usrPdtLikeRepository.delete(usrPdtLikeUid);
    }

    public long countByUnConfirmLikePdt(int usrUid) {
        return usrPdtLikeRepository.countByUsrUidAndStatus(usrUid, 2);
    }

    @Transactional
    public Page<PdtListDto> findAllPdtListByUsrUid(Pageable pageable, int usrUid, int sale) {

        String readUpdateSql = "update usr_pdt_like upl set upl.status = 1 where upl.usr_uid = " + usrUid;
        entityManager.createNativeQuery(readUpdateSql).executeUpdate();

        String customWhere = "";
        if (sale != 0) {
            customWhere += " AND p.price < p.origin_price ";
        }

        String sql = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
                " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
                " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
                " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
                " 1 AS pdt_like_status, " +
                " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
                " FROM usr_pdt_like upl3 JOIN pdt p ON p.pdt_uid = upl3.pdt_uid JOIN brand b ON p.brand_uid = b.brand_uid " +
                " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " JOIN category c ON p.category_uid = c.category_uid " +
                " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
                " WHERE p.photoshop_yn = 1 AND p.status <> 0 AND upl3.usr_uid = ?1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
                " ORDER BY upl3.reg_time DESC ";

        String countSql = "SELECT count(*) FROM usr_pdt_like upl3 JOIN pdt p ON p.pdt_uid = upl3.pdt_uid " +
                " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " WHERE p.photoshop_yn = 1 AND p.status <> 0 AND upl3.usr_uid = ?1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ";


        Query query = entityManager.createNativeQuery(sql);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        Query countQuery = entityManager.createNativeQuery(countSql);

        query.setParameter(1, usrUid);
        countQuery.setParameter(1, usrUid);

        Page<Object> pdtLikeList = new PageImpl<Object>(query.getResultList(), pageable,
                ((BigInteger) countQuery.getSingleResult()).longValue());

        return pdtLikeList.map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> findAllPdtStyleListByUsrUid(Pageable pageable, int usrUid, int sale) {

        String customWhere = "";
        if (sale != 0) {
            customWhere += " AND p.price < p.origin_price ";
        }

        String sql = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
                " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
                " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
                " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
                " 1 AS pdt_like_status, " +
                " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
                " FROM pdt_style ps JOIN usr_pdt_like upl3 ON upl3.pdt_uid = ps.pdt_uid JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
                " JOIN brand b ON p.brand_uid = b.brand_uid " +
                " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " JOIN category c ON p.category_uid = c.category_uid " +
                " WHERE p.photoshop_yn = 1 AND p.status <> 0 AND upl3.usr_uid = ?1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
                " ORDER BY ps.reg_time DESC ";

        String countSql = "SELECT count(*) FROM pdt_style ps JOIN usr_pdt_like upl3 ON upl3.pdt_uid = ps.pdt_uid JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                " WHERE p.photoshop_yn = 1 AND p.status <> 0 AND upl3.usr_uid = ?1 " + customWhere +
                " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ";


        Query query = entityManager.createNativeQuery(sql);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        Query countQuery = entityManager.createNativeQuery(countSql);

        query.setParameter(1, usrUid);
        countQuery.setParameter(1, usrUid);

        Page<Object> pdtLikeList = new PageImpl<Object>(query.getResultList(), pageable,
                ((BigInteger) countQuery.getSingleResult()).longValue());

        return pdtLikeList.map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }
}
