package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrWishDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrWishRepository extends CrudRepository<UsrWishDao, Integer> {

    @Query("select a from UsrWishDao a")
    Page<UsrWishDao> findAll(Pageable pageable);

    @Query("select uw, b, c from UsrWishDao uw join BrandDao b on b.brandUid = uw.brandUid and b.status = 1 " +
            " join CategoryDao c on c.categoryUid = uw.categoryUid and c.status = 1 " +
            " where uw.usrUid = ?1 ")
    Page<Object> findAllByUsrUid(Pageable pageable, int usrUid);
}
