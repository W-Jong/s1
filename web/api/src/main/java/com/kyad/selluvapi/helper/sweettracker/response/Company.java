package com.kyad.selluvapi.helper.sweettracker.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class Company {
    @SerializedName("Company")
    private List<Companies> company;
}
