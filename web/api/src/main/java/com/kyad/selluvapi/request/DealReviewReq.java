package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class DealReviewReq {
    @ApiModelProperty("점수")
    @Range(min = 0, max = 2)
    private int point;
    @ApiModelProperty("후기내용")
    @NotEmpty
    private String content;
}
