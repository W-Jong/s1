package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "share", schema = "selluv")
public class ShareDao {
    private int shareUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int dealUid;
    private int instagramYn = 0;
    private int twitterYn = 0;
    private int naverblogYn = 0;
    private int kakaostoryYn = 0;
    private int facebookYn = 0;
    private String instagramPhotos = "";
    private String twitterPhotos = "";
    private String naverblogPhotos = "";
    private String kakaostoryPhotos = "";
    private String facebookPhotos = "";
    private long rewardPrice = 0;
    private Timestamp compTime = null;
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "share_uid", nullable = false)
    public int getShareUid() {
        return shareUid;
    }

    public void setShareUid(int shareUid) {
        this.shareUid = shareUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "deal_uid", nullable = false)
    public int getDealUid() {
        return dealUid;
    }

    public void setDealUid(int dealUid) {
        this.dealUid = dealUid;
    }

    @Basic
    @Column(name = "instagram_yn", nullable = false)
    public int getInstagramYn() {
        return instagramYn;
    }

    public void setInstagramYn(int instagramYn) {
        this.instagramYn = instagramYn;
    }

    @Basic
    @Column(name = "twitter_yn", nullable = false)
    public int getTwitterYn() {
        return twitterYn;
    }

    public void setTwitterYn(int twitterYn) {
        this.twitterYn = twitterYn;
    }

    @Basic
    @Column(name = "naverblog_yn", nullable = false)
    public int getNaverblogYn() {
        return naverblogYn;
    }

    public void setNaverblogYn(int naverblogYn) {
        this.naverblogYn = naverblogYn;
    }

    @Basic
    @Column(name = "kakaostory_yn", nullable = false)
    public int getKakaostoryYn() {
        return kakaostoryYn;
    }

    public void setKakaostoryYn(int kakaostoryYn) {
        this.kakaostoryYn = kakaostoryYn;
    }

    @Basic
    @Column(name = "facebook_yn", nullable = false)
    public int getFacebookYn() {
        return facebookYn;
    }

    public void setFacebookYn(int facebookYn) {
        this.facebookYn = facebookYn;
    }

    @Basic
    @Column(name = "instagram_photos", nullable = false, length = 4095)
    public String getInstagramPhotos() {
        return instagramPhotos;
    }

    public void setInstagramPhotos(String instagramPhotos) {
        this.instagramPhotos = instagramPhotos;
    }

    @Basic
    @Column(name = "twitter_photos", nullable = false, length = 4095)
    public String getTwitterPhotos() {
        return twitterPhotos;
    }

    public void setTwitterPhotos(String twitterPhotos) {
        this.twitterPhotos = twitterPhotos;
    }

    @Basic
    @Column(name = "naverblog_photos", nullable = false, length = 4095)
    public String getNaverblogPhotos() {
        return naverblogPhotos;
    }

    public void setNaverblogPhotos(String naverblogPhotos) {
        this.naverblogPhotos = naverblogPhotos;
    }

    @Basic
    @Column(name = "kakaostory_photos", nullable = false, length = 4095)
    public String getKakaostoryPhotos() {
        return kakaostoryPhotos;
    }

    public void setKakaostoryPhotos(String kakaostoryPhotos) {
        this.kakaostoryPhotos = kakaostoryPhotos;
    }

    @Basic
    @Column(name = "facebook_photos", nullable = false, length = 4095)
    public String getFacebookPhotos() {
        return facebookPhotos;
    }

    public void setFacebookPhotos(String facebookPhotos) {
        this.facebookPhotos = facebookPhotos;
    }

    @Basic
    @Column(name = "reward_price", nullable = false)
    public long getRewardPrice() {
        return rewardPrice;
    }

    public void setRewardPrice(long rewardPrice) {
        this.rewardPrice = rewardPrice;
    }

    @Basic
    @Column(name = "comp_time", nullable = true)
    public Timestamp getCompTime() {
        return compTime;
    }

    public void setCompTime(Timestamp compTime) {
        this.compTime = compTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShareDao shareDao = (ShareDao) o;
        return shareUid == shareDao.shareUid &&
                usrUid == shareDao.usrUid &&
                dealUid == shareDao.dealUid &&
                instagramYn == shareDao.instagramYn &&
                twitterYn == shareDao.twitterYn &&
                naverblogYn == shareDao.naverblogYn &&
                kakaostoryYn == shareDao.kakaostoryYn &&
                facebookYn == shareDao.facebookYn &&
                rewardPrice == shareDao.rewardPrice &&
                status == shareDao.status &&
                Objects.equals(regTime, shareDao.regTime) &&
                Objects.equals(instagramPhotos, shareDao.instagramPhotos) &&
                Objects.equals(twitterPhotos, shareDao.twitterPhotos) &&
                Objects.equals(naverblogPhotos, shareDao.naverblogPhotos) &&
                Objects.equals(kakaostoryPhotos, shareDao.kakaostoryPhotos) &&
                Objects.equals(facebookPhotos, shareDao.facebookPhotos) &&
                Objects.equals(compTime, shareDao.compTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(shareUid, regTime, usrUid, dealUid, instagramYn, twitterYn, naverblogYn, kakaostoryYn, facebookYn, instagramPhotos, twitterPhotos, naverblogPhotos, kakaostoryPhotos, facebookPhotos, rewardPrice, compTime, status);
    }
}
