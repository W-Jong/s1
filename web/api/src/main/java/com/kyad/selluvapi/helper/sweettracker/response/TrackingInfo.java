package com.kyad.selluvapi.helper.sweettracker.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class TrackingInfo {
    //택배사에서 광고용으로 사용하는 주소
    @SerializedName("adUrl")
    private String adUrl;

    //배송 완료 여부(true or false)
    @SerializedName("complete")
    private String complete;

    //배송 완료 여부 (Y,N)
    @SerializedName("completeYN")
    private String completeYN;

    //배송예정 시간
    @SerializedName("estimate")
    private String estimate;

    @SerializedName("firstDetail")
    private Object firstDetail;

    //운송장 번호
    @SerializedName("invoiceNo")
    private String invoiceNo;

    //상품이미지 url
    @SerializedName("itemImage")
    private String itemImage;

    //상품 이름
    @SerializedName("itemName")
    private String itemName;

    @SerializedName("lastDetail")
    private Object lastDetail;

    @SerializedName("lastStateDetail")
    private Object lastStateDetail;

    //진행단계 [level 1: 배송준비중, 2: 집화완료, 3: 배송중, 4: 지점 도착, 5: 배송출발, 6:배송 완료]
    @SerializedName("level")
    private String level;

    //주문 번호
    @SerializedName("orderNumber")
    private String orderNumber;

    //상품정보
    @SerializedName("productInfo")
    private String productInfo;

    //받는 사람 주소
    @SerializedName("receiverAddr")
    private String receiverAddr;

    //받는 사람
    @SerializedName("receiverName")
    private String receiverName;

    //수령인 정보
    @SerializedName("recipient")
    private String recipient;

    //조회 결과
    @SerializedName("result")
    private String result;

    //보내는 사람
    @SerializedName("senderName")
    private String senderName;

    @SerializedName("trackingDetails")
    private List<TrackingDetail> trackingDetails;

    //zipCode
    @SerializedName("zipCode")
    private String zipCode;
}
