package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
public class UserPasswordUpdateReq {

    @NotEmpty
    @ApiModelProperty("이전 비번")
    private String oldPassword;
    @NotEmpty
    @ApiModelProperty("새 비번")
    private String newPassword;
}
