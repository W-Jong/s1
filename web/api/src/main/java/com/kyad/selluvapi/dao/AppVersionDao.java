package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "app_version", schema = "selluv")
public class AppVersionDao {
    private int appVersionUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int appType;
    private String history;
    private String landingUrl;
    private int versionCode;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_version_uid", nullable = false)
    public int getAppVersionUid() {
        return appVersionUid;
    }

    public void setAppVersionUid(int appVersionUid) {
        this.appVersionUid = appVersionUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "app_type", nullable = false)
    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    @Basic
    @Column(name = "history", nullable = false, length = 500)
    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    @Basic
    @Column(name = "landing_url", nullable = false, length = 500)
    public String getLandingUrl() {
        return landingUrl;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    @Basic
    @Column(name = "version_code", nullable = false)
    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppVersionDao that = (AppVersionDao) o;
        return appVersionUid == that.appVersionUid &&
                appType == that.appType &&
                versionCode == that.versionCode &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(history, that.history) &&
                Objects.equals(landingUrl, that.landingUrl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(appVersionUid, regTime, appType, history, landingUrl, versionCode, status);
    }
}
