package com.kyad.selluvapi.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;


@Data
public class UserBasisInfoReq {
    @NotEmpty
    @ApiModelProperty("본인인증 실명")
    private String usrNm;
    @ApiModelProperty("본인인증 생년월일 (형식:1990-01-01)")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate birthday;
    @Min(1)
    @Max(2)
    @ApiModelProperty("본인인증 성별 1-남, 2-여")
    private int gender;
    @NotEmpty
    @ApiModelProperty("본인인증 폰번호")
    private String usrPhone;
}
