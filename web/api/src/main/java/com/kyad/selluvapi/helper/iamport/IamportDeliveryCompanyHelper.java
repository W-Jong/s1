package com.kyad.selluvapi.helper.iamport;

public class IamportDeliveryCompanyHelper {
    public static String getCompanyCode(String company) {
        /*String[] deliveryCompanies = {
                "우체국택배",
                "CJ대한통운",
                "한진택배",
                "롯데택배",
                "로젠택배",
                "KGB택배",
                "KG로지스",
                "GTX로지스",
                "포스트박스",
                "SC로지스",
                "경동합동택배",
                "아주택배",
                "고려택배",
                "하나로택배",
                "대신택배",
                "천일택배",
                "건영택배",
                "한의사랑택배",
                "용마로지스",
                "일양로지스",
        };*/

        String result = "ETC";
        if (company == null)
            return result;

        switch (company) {
            case "로젠택배":
                result = "LOGEN";
                break;

            case "대한통운":
            case "CJ대한통운":
                result = "KOREX";
                break;

            case "현대택배":
                result = "HYUNDAI";
                break;

            case "하나로택배":
                result = "HANARO";
                break;

            case "SC로지스":
                result = "SAGAWA";
                break;

            case "KGB택배":
                result = "KGB";
                break;

            case "옐로우캡":
                result = "YELLOWCAP";
                break;

            case "동부택배":
                result = "DONGBU";
                break;

            case "우체국택배":
                result = "EPOST";
                break;

            case "CJGLS":
                result = "CJGLS";
                break;

            case "한진택배":
                result = "HANJIN";
                break;

            default:
                break;
        }

        return result;
    }
}
