package com.kyad.selluvapi.dto.pdt;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class PdtInfoDto {
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("브랜드명")
    private String brandName;
    @ApiModelProperty("카테고리명")
    private String categoryName;
    @ApiModelProperty("이미지")
    private String profileImg;
    @ApiModelProperty("가격")
    private long price;
}
