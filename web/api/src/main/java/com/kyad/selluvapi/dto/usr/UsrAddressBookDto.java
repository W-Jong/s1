package com.kyad.selluvapi.dto.usr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UsrAddressBookDto {
    @ApiModelProperty("가입한 지인")
    private List<UsrFollowListDto> joinedList;
    @ApiModelProperty("가입하지 않은 전화번호 목록")
    private List<String> unJoinedPhoneList;
}
