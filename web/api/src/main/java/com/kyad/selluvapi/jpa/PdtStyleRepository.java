package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.PdtStyleDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PdtStyleRepository extends CrudRepository<PdtStyleDao, Integer> {

    @Query("select a from PdtStyleDao a")
    Page<PdtStyleDao> findAll(Pageable pageable);

    List<PdtStyleDao> findAllByPdtUidAndPdtStyleUidNotOrderByRegTimeDesc(int pdtUid, int pdtStyleUid);

    @Query("select ps, u from PdtStyleDao ps join UsrDao u on ps.usrUid = u.usrUid where ps.pdtUid = ?1 order by ps.regTime desc")
    List<Object> findAllByPdtUidOrderByRegTimeDesc(int pdtUid);
}
