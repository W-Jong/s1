package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_sns", schema = "selluv")
public class UsrSnsDao {
    private int usrSnsUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int snsType;
    private String snsId = "";
    private String snsEmail = "";
    private String snsNckNm = "";
    private String snsProfileImg = "";
    private String snsToken = "";
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_sns_uid", nullable = false)
    public int getUsrSnsUid() {
        return usrSnsUid;
    }

    public void setUsrSnsUid(int usrSnsUid) {
        this.usrSnsUid = usrSnsUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "sns_type", nullable = false)
    public int getSnsType() {
        return snsType;
    }

    public void setSnsType(int snsType) {
        this.snsType = snsType;
    }

    @Basic
    @Column(name = "sns_id", nullable = false, length = 63)
    public String getSnsId() {
        return snsId;
    }

    public void setSnsId(String snsId) {
        this.snsId = snsId;
    }

    @Basic
    @Column(name = "sns_email", nullable = false, length = 63)
    public String getSnsEmail() {
        return snsEmail;
    }

    public void setSnsEmail(String snsEmail) {
        this.snsEmail = snsEmail;
    }

    @Basic
    @Column(name = "sns_nck_nm", nullable = false, length = 63)
    public String getSnsNckNm() {
        return snsNckNm;
    }

    public void setSnsNckNm(String snsNckNm) {
        this.snsNckNm = snsNckNm;
    }

    @Basic
    @Column(name = "sns_profile_img", nullable = false, length = 255)
    public String getSnsProfileImg() {
        return snsProfileImg;
    }

    public void setSnsProfileImg(String snsProfileImg) {
        this.snsProfileImg = snsProfileImg;
    }

    @Basic
    @Column(name = "sns_token", nullable = false, length = 63)
    public String getSnsToken() {
        return snsToken;
    }

    public void setSnsToken(String snsToken) {
        this.snsToken = snsToken;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrSnsDao usrSnsDao = (UsrSnsDao) o;
        return usrSnsUid == usrSnsDao.usrSnsUid &&
                usrUid == usrSnsDao.usrUid &&
                snsType == usrSnsDao.snsType &&
                status == usrSnsDao.status &&
                Objects.equals(regTime, usrSnsDao.regTime) &&
                Objects.equals(snsId, usrSnsDao.snsId) &&
                Objects.equals(snsEmail, usrSnsDao.snsEmail) &&
                Objects.equals(snsNckNm, usrSnsDao.snsNckNm) &&
                Objects.equals(snsProfileImg, usrSnsDao.snsProfileImg) &&
                Objects.equals(snsToken, usrSnsDao.snsToken);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrSnsUid, regTime, usrUid, snsType, snsId, snsEmail, snsNckNm, snsProfileImg, snsToken, status);
    }
}
