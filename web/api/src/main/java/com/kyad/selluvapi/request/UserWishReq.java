package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.PDT_GROUP_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class UserWishReq {

    @ApiModelProperty("브랜드UID")
    @Min(1)
    private int brandUid;

    @ApiModelProperty("상품군")
    @NotNull
    private PDT_GROUP_TYPE pdtGroupType;

    @ApiModelProperty("카테고리UID")
    @Min(5)
    private int categoryUid;

    @ApiModelProperty("모델")
    @NotNull
    private String pdtModel;

    @ApiModelProperty("사이즈")
    @NotNull
    private String pdtSize;

    @ApiModelProperty("컬러")
    @NotNull
    private String colorName;
}
