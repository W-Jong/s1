package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrHideDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrHideRepository extends CrudRepository<UsrHideDao, Integer> {

    @Query("select a from UsrHideDao a")
    Page<UsrHideDao> findAll(Pageable pageable);

    @Query("SELECT count(ul) from UsrHideDao ul join UsrDao u on ul.usrUid = u.usrUid and u.status = 1 where ul.peerUsrUid = ?1")
    long countByPeerUsrUid(int peerUsrUid);

    @Query("SELECT count(ul) from UsrHideDao ul join UsrDao u on ul.peerUsrUid = u.usrUid and u.status = 1 where ul.usrUid = ?1")
    long countByUsrUid(int usrUid);

    UsrHideDao getByUsrUidAndPeerUsrUid(int usrUid, int peerUsrUid);
}
