package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "pdt", schema = "selluv")
public class PdtDao {
    private int pdtUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int brandUid;
    private int pdtGroup = 1;
    private int categoryUid;
    private String pdtSize = "";
    private String profileImg = "";
    private String photos = "";
    private int photoshopYn = 2;
    private long originPrice = 0;
    private long price = 0;
    private long sendPrice = 0;
    private int pdtCondition = 1;
    private String component = "000000";
    private String etc = "";
    private String colorName = "멀티";
    private String pdtModel = "";
    private String promotionCode = "";
    private String content = "";
    private String tag = "";
    private String signImg = "";
    private int valetUid = 0;
    private String memo = "";
    private Timestamp edtTime = new Timestamp(System.currentTimeMillis());
    private int fameScore = 0;
    private int negoYn = 1;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "brand_uid", nullable = false)
    public int getBrandUid() {
        return brandUid;
    }

    public void setBrandUid(int brandUid) {
        this.brandUid = brandUid;
    }

    @Basic
    @Column(name = "pdt_group", nullable = false)
    public int getPdtGroup() {
        return pdtGroup;
    }

    public void setPdtGroup(int pdtGroup) {
        this.pdtGroup = pdtGroup;
    }

    @Basic
    @Column(name = "category_uid", nullable = false)
    public int getCategoryUid() {
        return categoryUid;
    }

    public void setCategoryUid(int categoryUid) {
        this.categoryUid = categoryUid;
    }

    @Basic
    @Column(name = "pdt_size", nullable = false, length = 255)
    public String getPdtSize() {
        return pdtSize;
    }

    public void setPdtSize(String pdtSize) {
        this.pdtSize = pdtSize;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "photos", nullable = false, length = 4095)
    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    @Basic
    @Column(name = "photoshop_yn", nullable = false)
    public int getPhotoshopYn() {
        return photoshopYn;
    }

    public void setPhotoshopYn(int photoshopYn) {
        this.photoshopYn = photoshopYn;
    }

    @Basic
    @Column(name = "origin_price", nullable = false)
    public long getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(long originPrice) {
        this.originPrice = originPrice;
    }

    @Basic
    @Column(name = "price", nullable = false)
    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Basic
    @Column(name = "send_price", nullable = false)
    public long getSendPrice() {
        return sendPrice;
    }

    public void setSendPrice(long sendPrice) {
        this.sendPrice = sendPrice;
    }

    @Basic
    @Column(name = "pdt_condition", nullable = false)
    public int getPdtCondition() {
        return pdtCondition;
    }

    public void setPdtCondition(int pdtCondition) {
        this.pdtCondition = pdtCondition;
    }

    @Basic
    @Column(name = "component", nullable = false, length = 6)
    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    @Basic
    @Column(name = "etc", nullable = false, length = 100)
    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }

    @Basic
    @Column(name = "color_name", nullable = false, length = 30)
    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    @Basic
    @Column(name = "pdt_model", nullable = false, length = 50)
    public String getPdtModel() {
        return pdtModel;
    }

    public void setPdtModel(String pdtModel) {
        this.pdtModel = pdtModel;
    }

    @Basic
    @Column(name = "promotion_code", nullable = false, length = 63)
    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 511)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "tag", nullable = false, length = 100)
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Basic
    @Column(name = "sign_img", nullable = false, length = 255)
    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    @Basic
    @Column(name = "valet_uid", nullable = false)
    public int getValetUid() {
        return valetUid;
    }

    public void setValetUid(int valetUid) {
        this.valetUid = valetUid;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "fame_score", nullable = false)
    public int getFameScore() {
        return fameScore;
    }

    public void setFameScore(int fameScore) {
        this.fameScore = fameScore;
    }

    @Basic
    @Column(name = "nego_yn", nullable = false)
    public int getNegoYn() {
        return negoYn;
    }

    public void setNegoYn(int negoYn) {
        this.negoYn = negoYn;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PdtDao pdtDao = (PdtDao) o;
        return pdtUid == pdtDao.pdtUid &&
                usrUid == pdtDao.usrUid &&
                brandUid == pdtDao.brandUid &&
                pdtGroup == pdtDao.pdtGroup &&
                categoryUid == pdtDao.categoryUid &&
                photoshopYn == pdtDao.photoshopYn &&
                originPrice == pdtDao.originPrice &&
                price == pdtDao.price &&
                sendPrice == pdtDao.sendPrice &&
                pdtCondition == pdtDao.pdtCondition &&
                valetUid == pdtDao.valetUid &&
                fameScore == pdtDao.fameScore &&
                negoYn == pdtDao.negoYn &&
                status == pdtDao.status &&
                Objects.equals(regTime, pdtDao.regTime) &&
                Objects.equals(pdtSize, pdtDao.pdtSize) &&
                Objects.equals(profileImg, pdtDao.profileImg) &&
                Objects.equals(photos, pdtDao.photos) &&
                Objects.equals(component, pdtDao.component) &&
                Objects.equals(etc, pdtDao.etc) &&
                Objects.equals(colorName, pdtDao.colorName) &&
                Objects.equals(pdtModel, pdtDao.pdtModel) &&
                Objects.equals(promotionCode, pdtDao.promotionCode) &&
                Objects.equals(content, pdtDao.content) &&
                Objects.equals(tag, pdtDao.tag) &&
                Objects.equals(signImg, pdtDao.signImg) &&
                Objects.equals(memo, pdtDao.memo) &&
                Objects.equals(edtTime, pdtDao.edtTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(pdtUid, regTime, usrUid, brandUid, pdtGroup, categoryUid, pdtSize, profileImg, photos, photoshopYn, originPrice, price, sendPrice, pdtCondition, component, etc, colorName, pdtModel, promotionCode, content, tag, signImg, valetUid, memo, edtTime, fameScore, negoYn, status);
    }
}
