package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "privilege", schema = "selluv")
public class PrivilegeDao {
    private int privilegeUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int regAdmUid;
    private String menu;
    private String privilegeNm;
    private int edtAdmUid;
    private Timestamp edtTime;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "privilege_uid", nullable = false)
    public int getPrivilegeUid() {
        return privilegeUid;
    }

    public void setPrivilegeUid(int privilegeUid) {
        this.privilegeUid = privilegeUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "reg_adm_uid", nullable = false)
    public int getRegAdmUid() {
        return regAdmUid;
    }

    public void setRegAdmUid(int regAdmUid) {
        this.regAdmUid = regAdmUid;
    }

    @Basic
    @Column(name = "menu", nullable = false, length = 255)
    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    @Basic
    @Column(name = "privilege_nm", nullable = false, length = 50)
    public String getPrivilegeNm() {
        return privilegeNm;
    }

    public void setPrivilegeNm(String privilegeNm) {
        this.privilegeNm = privilegeNm;
    }

    @Basic
    @Column(name = "edt_adm_uid", nullable = false)
    public int getEdtAdmUid() {
        return edtAdmUid;
    }

    public void setEdtAdmUid(int edtAdmUid) {
        this.edtAdmUid = edtAdmUid;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrivilegeDao that = (PrivilegeDao) o;
        return privilegeUid == that.privilegeUid &&
                regAdmUid == that.regAdmUid &&
                edtAdmUid == that.edtAdmUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(menu, that.menu) &&
                Objects.equals(privilegeNm, that.privilegeNm) &&
                Objects.equals(edtTime, that.edtTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(privilegeUid, regTime, regAdmUid, menu, privilegeNm, edtAdmUid, edtTime, status);
    }
}
