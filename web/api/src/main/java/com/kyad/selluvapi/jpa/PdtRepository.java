package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.PdtDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PdtRepository extends CrudRepository<PdtDao, Integer> {

    @Query("select a from PdtDao a")
    Page<PdtDao> findAll(Pageable pageable);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status," +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.fame_score >= 5 AND p.pdt_group & ?3 <> 0" +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY rand(?2) \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.fame_score >= 5 AND p.pdt_group & ?3 <> 0 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
                    " AND ?2 = ?2 ",
            nativeQuery = true)
    Page<Object> findAllFameList(Pageable pageable, int usrUid, long seed, int likeGroup);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.reg_time >= ?2 AND p.pdt_group & ?3 <> 0 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.reg_time >= ?2 AND p.pdt_group & ?3 <> 0 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ",
            nativeQuery = true)
    Page<Object> findAllNewList(Pageable pageable, int usrUid, Timestamp targetDate, int likeGroup);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status," +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.fame_score >= 5 AND p.pdt_group & ?3 <> 0 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY rand(?2) \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.fame_score >= 5 AND p.pdt_group & ?3 <> 0 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
                    " AND ?2 = ?2 ",
            nativeQuery = true)
    Page<Object> findAllFameStyleList(Pageable pageable, int usrUid, long seed, int likeGroup);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.reg_time >= ?2 AND p.pdt_group & ?3 <> 0 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY ps.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.reg_time >= ?2 AND p.pdt_group & ?3 <> 0 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ",
            nativeQuery = true)
    Page<Object> findAllNewStyleList(Pageable pageable, int usrUid, Timestamp targetDate, int likeGroup);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY ps.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ",
            nativeQuery = true)
    Page<Object> findAllStyleList(Pageable pageable, int usrUid);

    @Query(value = "SELECT p.pdt_uid, b.name_ko, c.category_name, p.profile_img, p.price " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid AND b.brand_uid = ?1 " +
            " JOIN category c ON p.category_uid = c.category_uid AND c.status = 1 " +
            " WHERE p.photoshop_yn = 1 AND p.status <> 0 AND p.pdt_group & ?2 <> 0 " +
            " ORDER BY p.edt_time DESC LIMIT 20", nativeQuery = true)
    List<Object> getNewPdtByBrandUidAndLikeGroup(int brandUid, int likeGroup);

    @Query(value = "SELECT p.pdt_uid, b.name_ko, c.category_name, p.profile_img, p.price " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid AND b.brand_uid = ?1 " +
            " JOIN category c ON p.category_uid = c.category_uid AND c.status = 1 " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1, 3, 4) AND p.pdt_group & ?2 <> 0 " +
            " ORDER BY p.fame_score DESC, p.edt_time ASC LIMIT 3", nativeQuery = true)
    List<Object> getRecommendedPdtByBrandUidAndLikeGroup(int brandUid, int likeGroup);

    @Query(value = "SELECT p.pdt_uid, b.name_ko, c.category_name, p.profile_img, p.price " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid AND b.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid AND c.status = 1 " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1, 3, 4) AND p.usr_uid = ?1 AND p.pdt_uid <> ?2" +
            " ORDER BY p.edt_time DESC LIMIT 20", nativeQuery = true)
    List<Object> getRecommendedPdtByUsrUid(int usrUid, int pdtUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.brand_uid = ?2 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_group & ?3 <> 0" +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE ?1 = ?1 AND p.brand_uid = ?2 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_group & ?3 <> 0",
            nativeQuery = true)
    Page<Object> findAllByBrandUidAndPdtGroup(Pageable pageable, int usrUid, int brandUid, int pdtGroup);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE p.brand_uid = ?2 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) " +
            " ORDER BY ps.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE ?1 = ?1 AND p.brand_uid = ?2 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) ",
            nativeQuery = true)
    Page<Object> findAllStyleListByBrandUid(Pageable pageable, int usrUid, int brandUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE (p.brand_uid = ?2 OR p.category_uid IN ?3) AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?4" +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE (p.brand_uid = ?2 OR p.category_uid IN ?3) AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?4 AND ?1 = ?1",
            nativeQuery = true)
    Page<Object> findAllByBrandUidOrCategory2(Pageable pageable, int usrUid, int brandUid, List<Integer> categoryUids, int pdtUid);

    @Query(value = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " WHERE (p.brand_uid = ?1 OR p.category_uid IN ?2) AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.reg_time >= ?2 AND p.pdt_uid <> ?3", nativeQuery = true)
    long countByBrandUidOrCategory2(int brandUid, List<Integer> categoryUids, int pdtUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE (p.brand_uid = ?2 OR p.category_uid IN ?3) AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?4 " +
            " ORDER BY ps.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE (p.brand_uid = ?2 OR p.category_uid IN ?3) AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?4 AND ?1 = ?1",
            nativeQuery = true)
    Page<Object> findAllStyleListByBrandUidOrCateory2(Pageable pageable, int usrUid, int brandUid, List<Integer> categoryUids, int pdtUid);

    @Query(value = "SELECT count(*) FROM pdt_style ps JOIN pdt p ON ps.pdt_uid = p.pdt_uid JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " WHERE (p.brand_uid = ?1 OR p.category_uid IN ?2) AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?3 ", nativeQuery = true)
    long countStyleListByBrandUidOrCategory2(int brandUid, List<Integer> categoryUids, int pdtUid);

    @Query("select count(p) from PdtDao p where p.status <> 0 and p.usrUid = ?1 ")
    long countByUsrUid(int usrUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.pdt_model = ?2 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?3" +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.pdt_model = ?2 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?3 AND ?1 = ?1",
            nativeQuery = true)
    Page<Object> findAllByPdtModel(Pageable pageable, int usrUid, String pdtModel, int pdtUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.tag LIKE %?2% AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?3" +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.tag LIKE %?2% AND p.photoshop_yn = 1 AND p.status IN (1,2,3) AND p.pdt_uid <> ?3 AND ?1 = ?1",
            nativeQuery = true)
    Page<Object> findAllByTag(Pageable pageable, int usrUid, String tag, int pdtUid);


    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.category_uid IN ?2 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.category_uid IN ?2 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) ",
            nativeQuery = true)
    Page<Object> findAllByCategoryUidList(Pageable pageable, int usrUid, List<Integer> categoryUidList);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM pdt p JOIN brand b ON p.brand_uid = b.brand_uid AND b.brand_uid = ?3 " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.category_uid IN ?2 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY p.edt_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM pdt p JOIN brand b ON b.brand_uid = p.brand_uid AND b.brand_uid = ?3 JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND p.status IN (1,2,4) AND p.category_uid IN ?2 " +
                    " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) AND ?4 = ?4",
            nativeQuery = true)
    Page<Object> findAllByCategoryUidListAndBrandUid(Pageable pageable, int usrUid, List<Integer> categoryUidList, int brandUid);

    @Query(value = "select * from pdt p where p.usr_uid = ?1 and p.photoshop_yn= 1 and p.fame_score > 0 and p.status = 1 order by p.fame_score desc limit 3", nativeQuery = true)
    List<PdtDao> getThreeRecommendedPdtByUsrUid(int usrUid);
}
