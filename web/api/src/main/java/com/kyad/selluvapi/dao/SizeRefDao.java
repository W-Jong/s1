package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "size_ref", schema = "selluv")
public class SizeRefDao {
    private int sizeRefUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int pdtGroup;
    private int category1;
    private String category2;
    private String category3;
    private String basis;
    private String typeName;
    private String sizeName;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "size_ref_uid", nullable = false)
    public int getSizeRefUid() {
        return sizeRefUid;
    }

    public void setSizeRefUid(int sizeRefUid) {
        this.sizeRefUid = sizeRefUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "pdt_group", nullable = false)
    public int getPdtGroup() {
        return pdtGroup;
    }

    public void setPdtGroup(int pdtGroup) {
        this.pdtGroup = pdtGroup;
    }

    @Basic
    @Column(name = "category1", nullable = false)
    public int getCategory1() {
        return category1;
    }

    public void setCategory1(int category1) {
        this.category1 = category1;
    }

    @Basic
    @Column(name = "category2", nullable = false, length = 20)
    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        this.category2 = category2;
    }

    @Basic
    @Column(name = "category3", nullable = false, length = 20)
    public String getCategory3() {
        return category3;
    }

    public void setCategory3(String category3) {
        this.category3 = category3;
    }

    @Basic
    @Column(name = "basis", nullable = false, length = 10)
    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }

    @Basic
    @Column(name = "type_name", nullable = false, length = 10)
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Basic
    @Column(name = "size_name", nullable = false, length = 30)
    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SizeRefDao that = (SizeRefDao) o;
        return sizeRefUid == that.sizeRefUid &&
                pdtGroup == that.pdtGroup &&
                category1 == that.category1 &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(category2, that.category2) &&
                Objects.equals(category3, that.category3) &&
                Objects.equals(basis, that.basis) &&
                Objects.equals(typeName, that.typeName) &&
                Objects.equals(sizeName, that.sizeName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(sizeRefUid, regTime, pdtGroup, category1, category2, category3, basis, typeName, sizeName, status);
    }
}
