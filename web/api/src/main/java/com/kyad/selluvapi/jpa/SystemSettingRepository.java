package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.SystemSettingDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SystemSettingRepository extends CrudRepository<SystemSettingDao, Integer> {

    @Query("select a from SystemSettingDao a")
    Page<SystemSettingDao> findAll(Pageable pageable);

    @Query("select a from SystemSettingDao a where a.systemSettingUid <= 8")
    List<SystemSettingDao> findAllBasicSetting();
}
