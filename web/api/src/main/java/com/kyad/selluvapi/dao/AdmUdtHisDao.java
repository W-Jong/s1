package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "adm_udt_his", schema = "selluv")
public class AdmUdtHisDao {
    private int admUdtHisUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int admUid;
    private int kind;
    private String content;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adm_udt_his_uid", nullable = false)
    public int getAdmUdtHisUid() {
        return admUdtHisUid;
    }

    public void setAdmUdtHisUid(int admUdtHisUid) {
        this.admUdtHisUid = admUdtHisUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "adm_uid", nullable = false)
    public int getAdmUid() {
        return admUid;
    }

    public void setAdmUid(int admUid) {
        this.admUid = admUid;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmUdtHisDao that = (AdmUdtHisDao) o;
        return admUdtHisUid == that.admUdtHisUid &&
                admUid == that.admUid &&
                kind == that.kind &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {

        return Objects.hash(admUdtHisUid, regTime, admUid, kind, content, status);
    }
}
