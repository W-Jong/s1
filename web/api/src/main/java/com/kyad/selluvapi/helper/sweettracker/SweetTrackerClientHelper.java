package com.kyad.selluvapi.helper.sweettracker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kyad.selluvapi.helper.sweettracker.request.InvoiceReq;
import com.kyad.selluvapi.helper.sweettracker.response.Company;
import com.kyad.selluvapi.helper.sweettracker.response.Status;
import com.kyad.selluvapi.helper.sweettracker.response.SweetTrackerResponse;
import com.kyad.selluvapi.helper.sweettracker.response.TrackingInfo;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import java.lang.reflect.Type;

public class SweetTrackerClientHelper {
    private static final String API_URL = "http://info.sweettracker.co.kr";
    private String api_key = null;
    private HttpClient client = null;
    private Gson gson = new Gson();

    public SweetTrackerClientHelper() {
        this.api_key = "8DZiCFYODvgLadVe3vmz1Q";
        this.client = HttpClientBuilder.create().build();
    }

    private HttpResponse postRequest(String path, StringEntity postData) throws Exception {

        try {

            HttpPost postRequest = new HttpPost(API_URL + path);
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Connection", "keep-alive");
            postRequest.setHeader("Content-Type", "application/json");

            postRequest.setEntity(postData);

            HttpResponse response = client.execute(postRequest);

            return response;

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }

    private HttpResponse getRequest(String path) throws Exception {

        try {

            HttpGet getRequest = new HttpGet(API_URL + path + (path.contains("?") ? "&" : "?") + "t_key=" + api_key);
            getRequest.addHeader("Accept", "application/json");

            HttpResponse response = client.execute(getRequest);

            return response;

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }

    public SweetTrackerResponse<Company> getCompanyList() {
        SweetTrackerResponse<Company> sweetTrackerResponse = new SweetTrackerResponse<>();
        try {
            HttpResponse response = getRequest("/api/v1/companylist");
            ResponseHandler<String> handler = new BasicResponseHandler();
            String responseBody = ((BasicResponseHandler) handler).handleEntity(response.getEntity());

            Type responseType = new TypeToken<Status>() {
            }.getType();
            Status apiStatus = gson.fromJson(responseBody, responseType);
            if (apiStatus.getCode() == null) {
                sweetTrackerResponse.setCode(SweetTrackerResponse.SWEET_TRACKER_API_200);
                sweetTrackerResponse.setMessage("OK");
                Type trackingInfoType = new TypeToken<TrackingInfo>() {
                }.getType();
                sweetTrackerResponse.setData(gson.fromJson(responseBody, trackingInfoType));
            } else {
                sweetTrackerResponse.setCode(Integer.parseInt(apiStatus.getCode()));
                sweetTrackerResponse.setMessage(apiStatus.getMsg());
            }

        } catch (Exception e) {
            e.printStackTrace();
            sweetTrackerResponse.setCode(SweetTrackerResponse.SWEET_TRACKER_API_ERROR);
            sweetTrackerResponse.setMessage("스윗트레커 서버 오류입니다.");
            sweetTrackerResponse.setData(null);
        }

        return sweetTrackerResponse;
    }

    public SweetTrackerResponse<TrackingInfo> getTrackingInfo(InvoiceReq invoice) {
        SweetTrackerResponse<TrackingInfo> sweetTrackerResponse = new SweetTrackerResponse<>();
        try {
            HttpResponse response = getRequest("/api/v1/trackingInfo?" + invoice.getUriString());
            ResponseHandler<String> handler = new BasicResponseHandler();
            String responseBody = ((BasicResponseHandler) handler).handleEntity(response.getEntity());

            Type responseType = new TypeToken<Status>() {
            }.getType();
            Status apiStatus = gson.fromJson(responseBody, responseType);
            if (apiStatus.getCode() == null) {
                sweetTrackerResponse.setCode(SweetTrackerResponse.SWEET_TRACKER_API_200);
                sweetTrackerResponse.setMessage("OK");
                Type trackingInfoType = new TypeToken<TrackingInfo>() {
                }.getType();
                sweetTrackerResponse.setData(gson.fromJson(responseBody, trackingInfoType));
            } else {
                sweetTrackerResponse.setCode(Integer.parseInt(apiStatus.getCode()));
                sweetTrackerResponse.setMessage(apiStatus.getMsg());
            }

        } catch (Exception e) {
            e.printStackTrace();
            sweetTrackerResponse.setCode(SweetTrackerResponse.SWEET_TRACKER_API_ERROR);
            sweetTrackerResponse.setMessage("스윗트레커 서버 오류입니다.");
            sweetTrackerResponse.setData(null);
        }

        return sweetTrackerResponse;
    }
}
