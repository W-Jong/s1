package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.SearchWordDao;
import com.kyad.selluvapi.dto.search.SearchWordDto;
import com.kyad.selluvapi.jpa.SearchWordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchWordService {

    @Autowired
    private SearchWordRepository searchWordRepository;

    public Page<SearchWordDao> findAll(Pageable pageable) {
        return searchWordRepository.findAll(pageable);
    }

    public SearchWordDao save(SearchWordDao dao) {
        return searchWordRepository.save(dao);
    }

    public SearchWordDao saveKeywordHistory(String content) {
        if (content == null || content.isEmpty())
            return null;

        SearchWordDao searchWord = new SearchWordDao();
        searchWord.setContent(content);
        return searchWordRepository.save(searchWord);
    }

    public List<SearchWordDto> findAllFameWords() {
        List<Object> objectList = searchWordRepository.findAllFameWords();
        if (objectList == null)
            return null;

        List<SearchWordDto> searchWordDtoList = new ArrayList<>();
        for (Object object : objectList) {
            Object[] objects = (Object[]) object;

            SearchWordDto dto = new SearchWordDto();
            dto.setWord((String) objects[0]);
            dto.setCount(((BigInteger) objects[1]).longValue());

            searchWordDtoList.add(dto);
        }

        return searchWordDtoList;
    }
}
