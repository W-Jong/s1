package com.kyad.selluvapi.helper.iamport.request;

import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.helper.iamport.response.PaymentCancelAnnotation;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class CardPayment {
    @SerializedName("customer_uid")
    String customer_uid;

    @SerializedName("merchant_uid")
    String merchant_uid;

    @SerializedName("amount")
    BigDecimal amount;

//    @SerializedName("vat")
//    BigDecimal vat;

    @SerializedName("name")
    String name;

    @SerializedName("buyer_name")
    String buyer_name;

    @SerializedName("buyer_email")
    String buyer_email;

    @SerializedName("buyer_tel")
    String buyer_tel;

    @SerializedName("buyer_addr")
    String buyer_addr;

    @SerializedName("buyer_postcode")
    String buyer_postcode;

//    @SerializedName("card_quota")
//    int card_quota;

//    @SerializedName("custom_data")
//    String custom_data;
}
