package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_brand_like", schema = "selluv")
public class UsrBrandLikeDao {
    private int usrBrandLikeUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int brandUid;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_brand_like_uid", nullable = false)
    public int getUsrBrandLikeUid() {
        return usrBrandLikeUid;
    }

    public void setUsrBrandLikeUid(int usrBrandLikeUid) {
        this.usrBrandLikeUid = usrBrandLikeUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "brand_uid", nullable = false)
    public int getBrandUid() {
        return brandUid;
    }

    public void setBrandUid(int brandUid) {
        this.brandUid = brandUid;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrBrandLikeDao that = (UsrBrandLikeDao) o;
        return usrBrandLikeUid == that.usrBrandLikeUid &&
                usrUid == that.usrUid &&
                brandUid == that.brandUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrBrandLikeUid, regTime, usrUid, brandUid, status);
    }
}
