package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_feed", schema = "selluv")
public class UsrFeedDao {
    private int usrFeedUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("분류 1-찾았던 상품 2-좋아요 상품 가격인하 3-팔로우 브랜드 4-잇템 5-팔로우 유저 스타일 6-팔로우 유저 신상품")
    private int type;
    @ApiModelProperty("대상자 UID")
    private int peerUsrUid;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_feed_uid", nullable = false)
    public int getUsrFeedUid() {
        return usrFeedUid;
    }

    public void setUsrFeedUid(int usrFeedUid) {
        this.usrFeedUid = usrFeedUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "peer_usr_uid", nullable = false)
    public int getPeerUsrUid() {
        return peerUsrUid;
    }

    public void setPeerUsrUid(int peerUsrUid) {
        this.peerUsrUid = peerUsrUid;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrFeedDao that = (UsrFeedDao) o;
        return usrFeedUid == that.usrFeedUid &&
                usrUid == that.usrUid &&
                pdtUid == that.pdtUid &&
                type == that.type &&
                peerUsrUid == that.peerUsrUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrFeedUid, regTime, usrUid, pdtUid, type, peerUsrUid, status);
    }
}
