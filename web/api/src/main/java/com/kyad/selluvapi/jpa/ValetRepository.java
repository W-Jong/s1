package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ValetDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValetRepository extends CrudRepository<ValetDao, Integer> {

    @Query("select a from ValetDao a")
    Page<ValetDao> findAll(Pageable pageable);
}
