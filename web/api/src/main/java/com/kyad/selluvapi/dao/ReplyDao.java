package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "reply", schema = "selluv")
public class ReplyDao {
    private int replyUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("@붙은 회원UID, 콤마로 구분, 0인 경우 해당 유저상세페이지로 이동불가")
    private String targetUids = "";
    @ApiModelProperty("내용")
    private String content = "";
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reply_uid", nullable = false)
    public int getReplyUid() {
        return replyUid;
    }

    public void setReplyUid(int replyUid) {
        this.replyUid = replyUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "target_uids", nullable = false, length = 63)
    public String getTargetUids() {
        return targetUids;
    }

    public void setTargetUids(String targetUids) {
        this.targetUids = targetUids;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 511)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReplyDao replyDao = (ReplyDao) o;
        return replyUid == replyDao.replyUid &&
                usrUid == replyDao.usrUid &&
                pdtUid == replyDao.pdtUid &&
                status == replyDao.status &&
                Objects.equals(regTime, replyDao.regTime) &&
                Objects.equals(targetUids, replyDao.targetUids) &&
                Objects.equals(content, replyDao.content);
    }

    @Override
    public int hashCode() {

        return Objects.hash(replyUid, regTime, usrUid, pdtUid, targetUids, content, status);
    }
}
