package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
@Api(value = "/common", description = "공통API")
@RequestMapping("/common")
public class CommonController {

    private final Logger LOG = LoggerFactory.getLogger(CommonController.class);

    @ApiOperation(value = "한개 파일 업로드",
            notes = "하나의 파일을 업로드한후 temp URL을 돌려준다.")
    @PostMapping(value = "/upload")
    public GenericResponse<HashMap<String, String>> fileUploadToTemp(@RequestParam("uploadfile") MultipartFile uploadfile) {

        OutputStream out = null;
        boolean flagSuccess = true;
        String fileName = "";

        try {
            // 파일명 얻기
            String originFileName = uploadfile.getOriginalFilename();
            //랜덤 파일명생성(확장자 유지)
            fileName = StarmanHelper.getUniqueString(FilenameUtils.getExtension(originFileName));
            // 파일의 바이트 정보 얻기
            byte[] bytes = uploadfile.getBytes();
            // 파일의 저장 경로 얻기
            String uploadPath = StarmanHelper.makeDirectory("temp") + "/" + fileName;

            // 파일 객체 생성
            File file = new File(uploadPath);

            // 파일 아웃풋 스트림 생성
            out = new FileOutputStream(file);
            // 파일 아웃풋 스트림에 파일의 바이트 쓰기
            out.write(bytes);

        } catch (IOException e) {
            flagSuccess = false;
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!flagSuccess) {
            return new GenericResponse<>(ResponseMeta.FILE_UPLOAD_ERROR, null);
        }
        HashMap<String, String> fileData = new HashMap<>();
        fileData.put("fileName", fileName);
        fileData.put("fileURL", StarmanHelper.UPLOAD_URL_PATH + "/temp/" + fileName);

        return new GenericResponse<>(ResponseMeta.OK, fileData);
    }

    @ApiOperation(value = "여러개 파일 업로드",
            notes = "여러개의 파일을 업로드한후 temp URL들을 돌려준다.")
    @PostMapping(value = "/uploads")
    @RequestMapping(value = "/uploads", method = RequestMethod.POST)
    public GenericResponse<ArrayList<HashMap<String, String>>> filesUploadToTemp(@RequestParam("uploadfiles") MultipartFile[] uploadfiles) {

        if (uploadfiles.length == 0)
            return new GenericResponse<>(ResponseMeta.FILE_UPLOAD_ERROR, null);

        OutputStream out = null;
        boolean flagSuccess = true;
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        try {
            for (MultipartFile uploadfile : uploadfiles) {
                String fileName = "";
                // 파일명 얻기
                String originFileName = uploadfile.getOriginalFilename();
                //랜덤 파일명생성(확장자 유지)
                fileName = StarmanHelper.getUniqueString(FilenameUtils.getExtension(originFileName));
                // 파일의 바이트 정보 얻기
                byte[] bytes = uploadfile.getBytes();
                // 파일의 저장 경로 얻기
                String uploadPath = StarmanHelper.makeDirectory("temp") + "/" + fileName;

                // 파일 객체 생성
                File file = new File(uploadPath);

                // 파일 아웃풋 스트림 생성
                out = new FileOutputStream(file);
                // 파일 아웃풋 스트림에 파일의 바이트 쓰기
                out.write(bytes);

                HashMap<String, String> fileData = new HashMap<>();
                fileData.put("fileName", fileName);
                fileData.put("fileURL", StarmanHelper.UPLOAD_URL_PATH + "/temp/" + fileName);

                arrayList.add(fileData);

                out.close();
            }
        } catch (IOException e) {
            flagSuccess = false;
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!flagSuccess) {
            return new GenericResponse<>(ResponseMeta.FILE_UPLOAD_ERROR, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, arrayList);
    }
}
