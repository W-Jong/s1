package com.kyad.selluvapi.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CategoryMiniDto {
    @ApiModelProperty("카테고리UID")
    private int categoryUid;
    @ApiModelProperty("뎁스")
    private int depth;
    @ApiModelProperty("카테고리이름")
    private String categoryName;
}
