package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_hide", schema = "selluv")
public class UsrHideDao {
    private int usrHideUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int peerUsrUid;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_hide_uid", nullable = false)
    public int getUsrHideUid() {
        return usrHideUid;
    }

    public void setUsrHideUid(int usrHideUid) {
        this.usrHideUid = usrHideUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "peer_usr_uid", nullable = false)
    public int getPeerUsrUid() {
        return peerUsrUid;
    }

    public void setPeerUsrUid(int peerUsrUid) {
        this.peerUsrUid = peerUsrUid;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrHideDao that = (UsrHideDao) o;
        return usrHideUid == that.usrHideUid &&
                usrUid == that.usrUid &&
                peerUsrUid == that.peerUsrUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrHideUid, regTime, usrUid, peerUsrUid, status);
    }
}
