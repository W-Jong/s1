package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "brand", schema = "selluv")
public class BrandDao {
    private int brandUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String firstEn = "";
    private String firstKo = "";
    private String nameEn = "";
    private String nameKo = "";
    private String licenseUrl = "";
    private String logoImg = "";
    private String profileImg = "";
    private String backImg = "";
    private int usrUid = 0;
    private int pdtCount = 0;
    private long totalCount = 0;
    private long totalPrice = 0;
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brand_uid", nullable = false)
    public int getBrandUid() {
        return brandUid;
    }

    public void setBrandUid(int brandUid) {
        this.brandUid = brandUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "first_en", nullable = false, length = 2)
    public String getFirstEn() {
        return firstEn;
    }

    public void setFirstEn(String firstEn) {
        this.firstEn = firstEn;
    }

    @Basic
    @Column(name = "first_ko", nullable = false, length = 2)
    public String getFirstKo() {
        return firstKo;
    }

    public void setFirstKo(String firstKo) {
        this.firstKo = firstKo;
    }

    @Basic
    @Column(name = "name_en", nullable = false, length = 30)
    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    @Basic
    @Column(name = "name_ko", nullable = false, length = 30)
    public String getNameKo() {
        return nameKo;
    }

    public void setNameKo(String nameKo) {
        this.nameKo = nameKo;
    }

    @Basic
    @Column(name = "license_url", nullable = false, length = 255)
    public String getLicenseUrl() {
        return licenseUrl;
    }

    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }

    @Basic
    @Column(name = "logo_img", nullable = false, length = 255)
    public String getLogoImg() {
        return logoImg;
    }

    public void setLogoImg(String logoImg) {
        this.logoImg = logoImg;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "back_img", nullable = false, length = 255)
    public String getBackImg() {
        return backImg;
    }

    public void setBackImg(String backImg) {
        this.backImg = backImg;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "pdt_count", nullable = false)
    public int getPdtCount() {
        return pdtCount;
    }

    public void setPdtCount(int pdtCount) {
        this.pdtCount = pdtCount;
    }

    @Basic
    @Column(name = "total_count", nullable = false)
    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    @Basic
    @Column(name = "total_price", nullable = false)
    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrandDao brandDao = (BrandDao) o;
        return brandUid == brandDao.brandUid &&
                usrUid == brandDao.usrUid &&
                pdtCount == brandDao.pdtCount &&
                totalCount == brandDao.totalCount &&
                totalPrice == brandDao.totalPrice &&
                status == brandDao.status &&
                Objects.equals(regTime, brandDao.regTime) &&
                Objects.equals(firstEn, brandDao.firstEn) &&
                Objects.equals(firstKo, brandDao.firstKo) &&
                Objects.equals(nameEn, brandDao.nameEn) &&
                Objects.equals(nameKo, brandDao.nameKo) &&
                Objects.equals(licenseUrl, brandDao.licenseUrl) &&
                Objects.equals(logoImg, brandDao.logoImg) &&
                Objects.equals(profileImg, brandDao.profileImg) &&
                Objects.equals(backImg, brandDao.backImg);
    }

    @Override
    public int hashCode() {

        return Objects.hash(brandUid, regTime, firstEn, firstKo, nameEn, nameKo, licenseUrl, logoImg, profileImg, backImg, usrUid, pdtCount, totalCount, totalPrice, status);
    }
}
