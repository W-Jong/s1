package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_address", schema = "selluv")
public class UsrAddressDao {
    private int usrAddressUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("주소 순서 1,2,3만 가능")
    private int addressOrder = 1;
    @ApiModelProperty("성함")
    private String addressName = "";
    @ApiModelProperty("연락처")
    private String addressPhone = "";
    @ApiModelProperty("주소")
    private String addressDetail = "";
    @ApiModelProperty("상세주소(우편번호)")
    private String addressDetailSub = "";
    @ApiModelProperty("1이면 정상, 2이면 해당 유저가 기본으로 선택한 주소")
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_address_uid", nullable = false)
    public int getUsrAddressUid() {
        return usrAddressUid;
    }

    public void setUsrAddressUid(int usrAddressUid) {
        this.usrAddressUid = usrAddressUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "address_order", nullable = false)
    public int getAddressOrder() {
        return addressOrder;
    }

    public void setAddressOrder(int addressOrder) {
        this.addressOrder = addressOrder;
    }

    @Basic
    @Column(name = "address_name", nullable = false, length = 20)
    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Basic
    @Column(name = "address_phone", nullable = false, length = 20)
    public String getAddressPhone() {
        return addressPhone;
    }

    public void setAddressPhone(String addressPhone) {
        this.addressPhone = addressPhone;
    }

    @Basic
    @Column(name = "address_detail", nullable = false, length = 255)
    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    @Basic
    @Column(name = "address_detail_sub", nullable = false, length = 50)
    public String getAddressDetailSub() {
        return addressDetailSub;
    }

    public void setAddressDetailSub(String addressDetailSub) {
        this.addressDetailSub = addressDetailSub;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrAddressDao that = (UsrAddressDao) o;
        return usrAddressUid == that.usrAddressUid &&
                usrUid == that.usrUid &&
                addressOrder == that.addressOrder &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(addressName, that.addressName) &&
                Objects.equals(addressPhone, that.addressPhone) &&
                Objects.equals(addressDetail, that.addressDetail) &&
                Objects.equals(addressDetailSub, that.addressDetailSub);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrAddressUid, regTime, usrUid, addressOrder, addressName, addressPhone, addressDetail, addressDetailSub, status);
    }
}
