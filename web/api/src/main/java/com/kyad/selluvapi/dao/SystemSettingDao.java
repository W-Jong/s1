package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "system_setting", schema = "selluv")
public class SystemSettingDao {
    private int systemSettingUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String setting;
    private String settingExtra1;
    private String settingExtra2;
    private String memo;
    private int status;

    @Id
    @Column(name = "system_setting_uid", nullable = false)
    public int getSystemSettingUid() {
        return systemSettingUid;
    }

    public void setSystemSettingUid(int systemSettingUid) {
        this.systemSettingUid = systemSettingUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "setting", nullable = false, length = 511)
    public String getSetting() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting = setting;
    }

    @Basic
    @Column(name = "setting_extra1", nullable = false, length = 511)
    public String getSettingExtra1() {
        return settingExtra1;
    }

    public void setSettingExtra1(String settingExtra1) {
        this.settingExtra1 = settingExtra1;
    }

    @Basic
    @Column(name = "setting_extra2", nullable = false, length = 511)
    public String getSettingExtra2() {
        return settingExtra2;
    }

    public void setSettingExtra2(String settingExtra2) {
        this.settingExtra2 = settingExtra2;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemSettingDao that = (SystemSettingDao) o;
        return systemSettingUid == that.systemSettingUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(setting, that.setting) &&
                Objects.equals(settingExtra1, that.settingExtra1) &&
                Objects.equals(settingExtra2, that.settingExtra2) &&
                Objects.equals(memo, that.memo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(systemSettingUid, regTime, setting, settingExtra1, settingExtra2, memo, status);
    }
}
