package com.kyad.selluvapi.dto.pdt;

import com.kyad.selluvapi.dao.PdtStyleDao;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PdtStyleDto {
    @ApiModelProperty("스타일")
    private PdtStyleDao pdtStyle;
    @ApiModelProperty("등록유저")
    private UsrMiniDto usr;
    @ApiModelProperty("스타일프로필이미지 너비")
    private int styleProfileImgWidth = 1;
    @ApiModelProperty("스타일프로필이미지 높이")
    private int styleProfileImgHeight = 1;
}
