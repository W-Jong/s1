package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "deal", schema = "selluv")
public class DealDao {
    @ApiModelProperty("거래UID")
    private int dealUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("구매자 회원UID")
    private int usrUid;
    @ApiModelProperty("판매자 회원UID")
    private int sellerUsrUid;
    @ApiModelProperty("발렛UID  0-발렛상품 아님 0아니면 발렛상품")
    private int valetUid = 0;
    @ApiModelProperty("수령자 성함")
    private String recipientNm = "";
    @ApiModelProperty("수령자 연락처")
    private String recipientPhone = "";
    @ApiModelProperty("수령자 주소")
    private String recipientAddress = "";
    @ApiModelProperty("송장번호")
    private String deliveryNumber = "";
    @ApiModelProperty("인증전송장 번호")
    private String verifyDeliveryNumber = "";
    @ApiModelProperty("상품 영문브랜드")
    private String brandEn = "";
    @ApiModelProperty("상품 타이틀")
    private String pdtTitle = "";
    @ApiModelProperty("상품 사이즈")
    private String pdtSize;
    @ApiModelProperty("상품이미지")
    private String pdtImg = "";
    @ApiModelProperty("상품가격")
    private long pdtPrice = 0;
    @ApiModelProperty("배송비")
    private long sendPrice = 0;
    @ApiModelProperty("결제시간")
    private Timestamp payTime = null;
    @ApiModelProperty("정품인증가격")
    private long verifyPrice = 0;
    @ApiModelProperty("사용적립금")
    private long rewardPrice = 0;
    @ApiModelProperty("결제금액")
    private long price = 0;
    @ApiModelProperty("코드할인금액")
    private long payPromotionPrice = 0;
    @ApiModelProperty("구매프로모션")
    private String payPromotion = "";
    @JsonIgnore
    private String payImportCode = "";
    @ApiModelProperty("아임포트결제방법 1-간편결제신용카드 2-일반결제신용카드 3-실시간계좌이체 4-네이버페이")
    private int payImportMethod = 1;
    @JsonIgnore
    private int usrCardUid = 0;
    @ApiModelProperty("셀럽이용료")
    private long selluvPrice = 0;
    @ApiModelProperty("결제수수료")
    private long payFeePrice = 0;
    @ApiModelProperty("정산금액")
    private long cashPrice = 0;
    @ApiModelProperty("코드혜택금액")
    private long cashPromotionPrice = 0;
    @ApiModelProperty("판매프로모션")
    private String cashPromotion = "";
    @JsonIgnore
    private String nicepayCode = "";
    @ApiModelProperty("정산계좌정보")
    private String cashAccount = "";
    @ApiModelProperty("네고 및 카운터 요청시간(status에 따라 결정됨)")
    private Timestamp negoTime = null;
    @ApiModelProperty("네고 및 카운터네고 제안금액")
    private long reqPrice = 0;
    @ApiModelProperty("배송완료시간")
    private Timestamp deliveryTime = null;
    @ApiModelProperty("거래완료시간")
    private Timestamp compTime = null;
    @ApiModelProperty("정산완료시간")
    private Timestamp cashCompTime = null;
    @ApiModelProperty("취소사유")
    private String cancelReason = "";
    @ApiModelProperty("취소시간")
    private Timestamp cancelTime = null;
    @ApiModelProperty("환불사유")
    private String refundReason = "";
    @ApiModelProperty("반품요청시간")
    private Timestamp refundReqTime = null;
    @ApiModelProperty("반품승인시간")
    private Timestamp refundCheckTime = null;
    @ApiModelProperty("반품완료시간")
    private Timestamp refundCompTime = null;
    @ApiModelProperty("반품송장")
    private String refundDeliveryNumber = "";
    @ApiModelProperty("반품첨부사진")
    @JsonIgnore
    private String refundPhotos = "";
    @JsonIgnore
    private String memo = "";
    @ApiModelProperty("거래상태 0-삭제 1-배송준비 2-배송진행 3-정품인증 4-배송완료 5-거래완료 " +
            "6-정산완료 7-반품접수 8-반품승인 9-반품완료 10-주문취소 11-네고제안 12-카운터네고제안")
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deal_uid", nullable = false)
    public int getDealUid() {
        return dealUid;
    }

    public void setDealUid(int dealUid) {
        this.dealUid = dealUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "seller_usr_uid", nullable = false)
    public int getSellerUsrUid() {
        return sellerUsrUid;
    }

    public void setSellerUsrUid(int sellerUsrUid) {
        this.sellerUsrUid = sellerUsrUid;
    }

    @Basic
    @Column(name = "valet_uid", nullable = false)
    public int getValetUid() {
        return valetUid;
    }

    public void setValetUid(int valetUid) {
        this.valetUid = valetUid;
    }

    @Basic
    @Column(name = "recipient_nm", nullable = false, length = 20)
    public String getRecipientNm() {
        return recipientNm;
    }

    public void setRecipientNm(String recipientNm) {
        this.recipientNm = recipientNm;
    }

    @Basic
    @Column(name = "recipient_phone", nullable = false, length = 20)
    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    @Basic
    @Column(name = "recipient_address", nullable = false, length = 127)
    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    @Basic
    @Column(name = "delivery_number", nullable = false, length = 63)
    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public void setDeliveryNumber(String deliveryNumber) {
        this.deliveryNumber = deliveryNumber;
    }

    @Basic
    @Column(name = "verify_delivery_number", nullable = false, length = 63)
    public String getVerifyDeliveryNumber() {
        return verifyDeliveryNumber;
    }

    public void setVerifyDeliveryNumber(String verifyDeliveryNumber) {
        this.verifyDeliveryNumber = verifyDeliveryNumber;
    }

    @Basic
    @Column(name = "brand_en", nullable = false, length = 30)
    public String getBrandEn() {
        return brandEn;
    }

    public void setBrandEn(String brandEn) {
        this.brandEn = brandEn;
    }

    @Basic
    @Column(name = "pdt_title", nullable = false, length = 255)
    public String getPdtTitle() {
        return pdtTitle;
    }

    public void setPdtTitle(String pdtTitle) {
        this.pdtTitle = pdtTitle;
    }

    @Basic
    @Column(name = "pdt_size", nullable = false, length = 255)
    public String getPdtSize() {
        return pdtSize;
    }

    public void setPdtSize(String pdtSize) {
        this.pdtSize = pdtSize;
    }

    @Basic
    @Column(name = "pdt_img", nullable = false, length = 255)
    public String getPdtImg() {
        return pdtImg;
    }

    public void setPdtImg(String pdtImg) {
        this.pdtImg = pdtImg;
    }

    @Basic
    @Column(name = "pdt_price", nullable = false)
    public long getPdtPrice() {
        return pdtPrice;
    }

    public void setPdtPrice(long pdtPrice) {
        this.pdtPrice = pdtPrice;
    }

    @Basic
    @Column(name = "send_price", nullable = false)
    public long getSendPrice() {
        return sendPrice;
    }

    public void setSendPrice(long sendPrice) {
        this.sendPrice = sendPrice;
    }

    @Basic
    @Column(name = "pay_time", nullable = true)
    public Timestamp getPayTime() {
        return payTime;
    }

    public void setPayTime(Timestamp payTime) {
        this.payTime = payTime;
    }

    @Basic
    @Column(name = "verify_price", nullable = false)
    public long getVerifyPrice() {
        return verifyPrice;
    }

    public void setVerifyPrice(long verifyPrice) {
        this.verifyPrice = verifyPrice;
    }

    @Basic
    @Column(name = "reward_price", nullable = false)
    public long getRewardPrice() {
        return rewardPrice;
    }

    public void setRewardPrice(long rewardPrice) {
        this.rewardPrice = rewardPrice;
    }

    @Basic
    @Column(name = "price", nullable = false)
    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Basic
    @Column(name = "pay_promotion_price", nullable = false)
    public long getPayPromotionPrice() {
        return payPromotionPrice;
    }

    public void setPayPromotionPrice(long payPromotionPrice) {
        this.payPromotionPrice = payPromotionPrice;
    }

    @Basic
    @Column(name = "pay_promotion", nullable = false, length = 127)
    public String getPayPromotion() {
        return payPromotion;
    }

    public void setPayPromotion(String payPromotion) {
        this.payPromotion = payPromotion;
    }

    @Basic
    @Column(name = "pay_import_code", nullable = false, length = 63)
    public String getPayImportCode() {
        return payImportCode;
    }

    public void setPayImportCode(String payImportCode) {
        this.payImportCode = payImportCode;
    }

    @Basic
    @Column(name = "pay_import_method", nullable = false)
    public int getPayImportMethod() {
        return payImportMethod;
    }

    public void setPayImportMethod(int payImportMethod) {
        this.payImportMethod = payImportMethod;
    }

    @Basic
    @Column(name = "usr_card_uid", nullable = false)
    public int getUsrCardUid() {
        return usrCardUid;
    }

    public void setUsrCardUid(int usrCardUid) {
        this.usrCardUid = usrCardUid;
    }

    @Basic
    @Column(name = "selluv_price", nullable = false)
    public long getSelluvPrice() {
        return selluvPrice;
    }

    public void setSelluvPrice(long selluvPrice) {
        this.selluvPrice = selluvPrice;
    }

    @Basic
    @Column(name = "pay_fee_price", nullable = false)
    public long getPayFeePrice() {
        return payFeePrice;
    }

    public void setPayFeePrice(long payFeePrice) {
        this.payFeePrice = payFeePrice;
    }

    @Basic
    @Column(name = "cash_price", nullable = false)
    public long getCashPrice() {
        return cashPrice;
    }

    public void setCashPrice(long cashPrice) {
        this.cashPrice = cashPrice;
    }

    @Basic
    @Column(name = "cash_promotion_price", nullable = false)
    public long getCashPromotionPrice() {
        return cashPromotionPrice;
    }

    public void setCashPromotionPrice(long cashPromotionPrice) {
        this.cashPromotionPrice = cashPromotionPrice;
    }

    @Basic
    @Column(name = "cash_promotion", nullable = false, length = 127)
    public String getCashPromotion() {
        return cashPromotion;
    }

    public void setCashPromotion(String cashPromotion) {
        this.cashPromotion = cashPromotion;
    }

    @Basic
    @Column(name = "nicepay_code", nullable = false, length = 50)
    public String getNicepayCode() {
        return nicepayCode;
    }

    public void setNicepayCode(String nicepayCode) {
        this.nicepayCode = nicepayCode;
    }

    @Basic
    @Column(name = "cash_account", nullable = false, length = 255)
    public String getCashAccount() {
        return cashAccount;
    }

    public void setCashAccount(String cashAccount) {
        this.cashAccount = cashAccount;
    }

    @Basic
    @Column(name = "nego_time", nullable = true)
    public Timestamp getNegoTime() {
        return negoTime;
    }

    public void setNegoTime(Timestamp negoTime) {
        this.negoTime = negoTime;
    }

    @Basic
    @Column(name = "req_price", nullable = false)
    public long getReqPrice() {
        return reqPrice;
    }

    public void setReqPrice(long reqPrice) {
        this.reqPrice = reqPrice;
    }

    @Basic
    @Column(name = "delivery_time", nullable = true)
    public Timestamp getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Timestamp deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    @Basic
    @Column(name = "comp_time", nullable = true)
    public Timestamp getCompTime() {
        return compTime;
    }

    public void setCompTime(Timestamp compTime) {
        this.compTime = compTime;
    }

    @Basic
    @Column(name = "cash_comp_time", nullable = true)
    public Timestamp getCashCompTime() {
        return cashCompTime;
    }

    public void setCashCompTime(Timestamp cashCompTime) {
        this.cashCompTime = cashCompTime;
    }

    @Basic
    @Column(name = "cancel_reason", nullable = false, length = 63)
    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    @Basic
    @Column(name = "cancel_time", nullable = true)
    public Timestamp getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Timestamp cancelTime) {
        this.cancelTime = cancelTime;
    }

    @Basic
    @Column(name = "refund_reason", nullable = false, length = 511)
    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    @Basic
    @Column(name = "refund_req_time", nullable = true)
    public Timestamp getRefundReqTime() {
        return refundReqTime;
    }

    public void setRefundReqTime(Timestamp refundReqTime) {
        this.refundReqTime = refundReqTime;
    }

    @Basic
    @Column(name = "refund_check_time", nullable = true)
    public Timestamp getRefundCheckTime() {
        return refundCheckTime;
    }

    public void setRefundCheckTime(Timestamp refundCheckTime) {
        this.refundCheckTime = refundCheckTime;
    }

    @Basic
    @Column(name = "refund_comp_time", nullable = true)
    public Timestamp getRefundCompTime() {
        return refundCompTime;
    }

    public void setRefundCompTime(Timestamp refundCompTime) {
        this.refundCompTime = refundCompTime;
    }

    @Basic
    @Column(name = "refund_delivery_number", nullable = false, length = 63)
    public String getRefundDeliveryNumber() {
        return refundDeliveryNumber;
    }

    public void setRefundDeliveryNumber(String refundDeliveryNumber) {
        this.refundDeliveryNumber = refundDeliveryNumber;
    }

    @Basic
    @Column(name = "refund_photos", nullable = false, length = 4095)
    public String getRefundPhotos() {
        return refundPhotos;
    }

    public void setRefundPhotos(String refundPhotos) {
        this.refundPhotos = refundPhotos;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DealDao dealDao = (DealDao) o;
        return dealUid == dealDao.dealUid &&
                pdtUid == dealDao.pdtUid &&
                usrUid == dealDao.usrUid &&
                sellerUsrUid == dealDao.sellerUsrUid &&
                valetUid == dealDao.valetUid &&
                pdtPrice == dealDao.pdtPrice &&
                sendPrice == dealDao.sendPrice &&
                verifyPrice == dealDao.verifyPrice &&
                rewardPrice == dealDao.rewardPrice &&
                price == dealDao.price &&
                payPromotionPrice == dealDao.payPromotionPrice &&
                payImportMethod == dealDao.payImportMethod &&
                usrCardUid == dealDao.usrCardUid &&
                selluvPrice == dealDao.selluvPrice &&
                payFeePrice == dealDao.payFeePrice &&
                cashPrice == dealDao.cashPrice &&
                cashPromotionPrice == dealDao.cashPromotionPrice &&
                reqPrice == dealDao.reqPrice &&
                status == dealDao.status &&
                Objects.equals(regTime, dealDao.regTime) &&
                Objects.equals(recipientNm, dealDao.recipientNm) &&
                Objects.equals(recipientPhone, dealDao.recipientPhone) &&
                Objects.equals(recipientAddress, dealDao.recipientAddress) &&
                Objects.equals(deliveryNumber, dealDao.deliveryNumber) &&
                Objects.equals(verifyDeliveryNumber, dealDao.verifyDeliveryNumber) &&
                Objects.equals(brandEn, dealDao.brandEn) &&
                Objects.equals(pdtTitle, dealDao.pdtTitle) &&
                Objects.equals(pdtSize, dealDao.pdtSize) &&
                Objects.equals(pdtImg, dealDao.pdtImg) &&
                Objects.equals(payTime, dealDao.payTime) &&
                Objects.equals(payPromotion, dealDao.payPromotion) &&
                Objects.equals(payImportCode, dealDao.payImportCode) &&
                Objects.equals(cashPromotion, dealDao.cashPromotion) &&
                Objects.equals(nicepayCode, dealDao.nicepayCode) &&
                Objects.equals(cashAccount, dealDao.cashAccount) &&
                Objects.equals(negoTime, dealDao.negoTime) &&
                Objects.equals(deliveryTime, dealDao.deliveryTime) &&
                Objects.equals(compTime, dealDao.compTime) &&
                Objects.equals(cashCompTime, dealDao.cashCompTime) &&
                Objects.equals(cancelReason, dealDao.cancelReason) &&
                Objects.equals(cancelTime, dealDao.cancelTime) &&
                Objects.equals(refundReason, dealDao.refundReason) &&
                Objects.equals(refundReqTime, dealDao.refundReqTime) &&
                Objects.equals(refundCheckTime, dealDao.refundCheckTime) &&
                Objects.equals(refundCompTime, dealDao.refundCompTime) &&
                Objects.equals(refundDeliveryNumber, dealDao.refundDeliveryNumber) &&
                Objects.equals(refundPhotos, dealDao.refundPhotos) &&
                Objects.equals(memo, dealDao.memo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dealUid, regTime, pdtUid, usrUid, sellerUsrUid, valetUid, recipientNm, recipientPhone, recipientAddress, deliveryNumber, verifyDeliveryNumber, brandEn, pdtTitle, pdtSize, pdtImg, pdtPrice, sendPrice, payTime, verifyPrice, rewardPrice, price, payPromotionPrice, payPromotion, payImportCode, payImportMethod, usrCardUid, selluvPrice, payFeePrice, cashPrice, cashPromotionPrice, cashPromotion, nicepayCode, cashAccount, negoTime, reqPrice, deliveryTime, compTime, cashCompTime, cancelReason, cancelTime, refundReason, refundReqTime, refundCheckTime, refundCompTime, refundDeliveryNumber, refundPhotos, memo, status);
    }
}
