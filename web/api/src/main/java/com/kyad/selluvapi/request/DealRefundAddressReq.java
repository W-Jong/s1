package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class DealRefundAddressReq {
    @Range(min = 1, max = 3)
    @ApiModelProperty("선택된 주소번호 1~3만 가능")
    private int addressSelectionIndex;
    @ApiModelProperty("성함")
    private String addressName;
    @NotNull
    @ApiModelProperty("연락처")
    private String addressPhone;
    @NotNull
    @ApiModelProperty("주소")
    private String addressDetail;
    @NotNull
    @ApiModelProperty("상세주소(우편번호)")
    private String addressDetailSub;
}
