package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.BannerDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface BannerRepository extends CrudRepository<BannerDao, Integer> {

    @Query("select a from BannerDao a")
    Page<BannerDao> findAll(Pageable pageable);

    List<BannerDao> findAllByStartTimeBeforeAndEndTimeAfterAndStatusOrderByRegTimeDesc(Timestamp startTime, Timestamp endTime, int status);
}
