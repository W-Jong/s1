package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "theme", schema = "selluv")
public class ThemeDao {
    @ApiModelProperty("테마UID")
    private int themeUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("테마분류 1-남성 2-여성 4-키즈")
    private int kind;
    @ApiModelProperty("테마타이틀 - 영어")
    private String titleEn;
    @ApiModelProperty("테마타이틀 - 한국어")
    private String titleKo;
    @ApiModelProperty("테마이미지")
    private String profileImg;
    @ApiModelProperty("시작시간")
    private Timestamp startTime;
    @ApiModelProperty("종료시간")
    private Timestamp endTime;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "theme_uid", nullable = false)
    public int getThemeUid() {
        return themeUid;
    }

    public void setThemeUid(int themeUid) {
        this.themeUid = themeUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "title_en", nullable = false, length = 63)
    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    @Basic
    @Column(name = "title_ko", nullable = false, length = 63)
    public String getTitleKo() {
        return titleKo;
    }

    public void setTitleKo(String titleKo) {
        this.titleKo = titleKo;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = false)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThemeDao themeDao = (ThemeDao) o;
        return themeUid == themeDao.themeUid &&
                kind == themeDao.kind &&
                status == themeDao.status &&
                Objects.equals(regTime, themeDao.regTime) &&
                Objects.equals(titleEn, themeDao.titleEn) &&
                Objects.equals(titleKo, themeDao.titleKo) &&
                Objects.equals(profileImg, themeDao.profileImg) &&
                Objects.equals(startTime, themeDao.startTime) &&
                Objects.equals(endTime, themeDao.endTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(themeUid, regTime, kind, titleEn, titleKo, profileImg, startTime, endTime, status);
    }
}
