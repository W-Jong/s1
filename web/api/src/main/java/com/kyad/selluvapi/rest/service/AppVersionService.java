package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.AppVersionDao;
import com.kyad.selluvapi.jpa.AppVersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AppVersionService {

    @Autowired
    private AppVersionRepository appVersionRepository;

    public Page<AppVersionDao> findAll(Pageable pageable) {
        return appVersionRepository.findAll(pageable);
    }

    public AppVersionDao getLastedVersion(int type) {
        return appVersionRepository.getLastedVersion(type);
    }
}
