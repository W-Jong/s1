package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "report", schema = "selluv")
public class ReportDao {
    private int reportUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int peerUsrUid;
    private int pdtUid;
    private int type;
    private String content = "";
    private String memo = "";
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_uid", nullable = false)
    public int getReportUid() {
        return reportUid;
    }

    public void setReportUid(int reportUid) {
        this.reportUid = reportUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "peer_usr_uid", nullable = false)
    public int getPeerUsrUid() {
        return peerUsrUid;
    }

    public void setPeerUsrUid(int peerUsrUid) {
        this.peerUsrUid = peerUsrUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportDao reportDao = (ReportDao) o;
        return reportUid == reportDao.reportUid &&
                usrUid == reportDao.usrUid &&
                peerUsrUid == reportDao.peerUsrUid &&
                pdtUid == reportDao.pdtUid &&
                type == reportDao.type &&
                status == reportDao.status &&
                Objects.equals(regTime, reportDao.regTime) &&
                Objects.equals(content, reportDao.content) &&
                Objects.equals(memo, reportDao.memo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(reportUid, regTime, usrUid, peerUsrUid, pdtUid, type, content, memo, status);
    }
}
