package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "license", schema = "selluv")
public class LicenseDao {
    private int licenseUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String content;
    private Timestamp edtTime;
    private int status;

    @Id
    @Column(name = "license_uid", nullable = false)
    public int getLicenseUid() {
        return licenseUid;
    }

    public void setLicenseUid(int licenseUid) {
        this.licenseUid = licenseUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LicenseDao that = (LicenseDao) o;
        return licenseUid == that.licenseUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(content, that.content) &&
                Objects.equals(edtTime, that.edtTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(licenseUid, regTime, content, edtTime, status);
    }
}
