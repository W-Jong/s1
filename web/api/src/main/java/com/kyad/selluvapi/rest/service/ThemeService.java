package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.ThemeDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.jpa.ThemeRepository;
import com.kyad.selluvapi.rest.common.exception.ThemeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class ThemeService {

    @Autowired
    private ThemeRepository themeRepository;

    @Autowired
    private PdtService pdtService;

    public ThemeDao getThemeInfo(int themeUid) {
        ThemeDao theme = themeRepository.findOne(themeUid);
        if (theme == null || theme.getStatus() != 1) {
            throw new ThemeNotFoundException();
        }
        Timestamp now = new Timestamp(System.currentTimeMillis());
        if (theme.getStartTime().after(now) || theme.getEndTime().before(now)) {
            throw new ThemeNotFoundException();
        }

        return theme;
    }

    public List<ThemeDao> getActiveList(int pdtGroup) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        return themeRepository.findAllByKindAndStartTimeBeforeAndEndTimeAfterAndStatus(pdtGroup, now, now, 1);
    }

    public int countPdtByThemeUid(int themeUid) {
        return themeRepository.countPdtByThemeUid(themeUid);
    }

    public List<PdtListDto> getRecommendedPdtListByThemeUid(int themeUid, int usrUid) {
        List<Object> pdtObjectList = themeRepository.getRecommendedPdtListByThemeUid(themeUid, usrUid);
        if (pdtObjectList == null)
            return null;

        List<PdtListDto> pdtList = new ArrayList<>();
        for (Object pdtObject : pdtObjectList) {
            pdtList.add(pdtService.convertListObjectToPdtListDto((Object[]) pdtObject));
        }
        return pdtList;
    }

    public List<PdtListDto> getAllPdtListByThemeUid(int themeUid, int usrUid) {
        List<Object> pdtObjectList = themeRepository.getAllPdtListByThemeUid(themeUid, usrUid);
        if (pdtObjectList == null)
            return null;

        List<PdtListDto> pdtList = new ArrayList<>();
        for (Object pdtObject : pdtObjectList) {
            pdtList.add(pdtService.convertListObjectToPdtListDto((Object[]) pdtObject));
        }
        return pdtList;
    }
}
