package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.CategoryDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryDao, Integer> {

    @Query("select a from CategoryDao a")
    Page<CategoryDao> findAll(Pageable pageable);

    List<CategoryDao> findAllByParentCategoryUidAndStatusOrderByCategoryOrder(int parentCategoryUid, int status);

    @Query(value = "SELECT c1.category_uid FROM category c1 " +
            " LEFT JOIN category c2 ON c2.category_uid = c1.parent_category_uid " +
            " LEFT JOIN category c3 ON c3.category_uid = c2.parent_category_uid " +
            " LEFT JOIN category c4 ON c4.category_uid = c3.parent_category_uid " +
            " WHERE ?1 IN (c2.category_uid, c3.category_uid, c4.category_uid) AND c1.status = 1 " +
            " ORDER BY c1.depth, c1.category_order",
            nativeQuery = true)
    List<Integer> getSubAllCategoryUids(int categoryUid);

    @Query(value = "SELECT c1.* FROM category c1 " +
            " LEFT JOIN category c2 ON c2.category_uid = c1.parent_category_uid " +
            " LEFT JOIN category c3 ON c3.category_uid = c2.parent_category_uid " +
            " LEFT JOIN category c4 ON c4.category_uid = c3.parent_category_uid " +
            " WHERE ?1 IN (c2.category_uid, c3.category_uid, c4.category_uid) AND c1.status = 1 " +
            " ORDER BY c1.depth, c1.category_order",
            nativeQuery = true)
    List<CategoryDao> getSubAllCategories(int categoryUid);

    @Query(value = "SELECT c1, c2, c3, c4 FROM CategoryDao c1 " +
            " LEFT JOIN CategoryDao c2 ON c2.categoryUid = c1.parentCategoryUid " +
            " LEFT JOIN CategoryDao c3 ON c3.categoryUid = c2.parentCategoryUid " +
            " LEFT JOIN CategoryDao c4 ON c4.categoryUid = c3.parentCategoryUid " +
            " WHERE c1.categoryUid = ?1 and c1.status = 1 ")
    Object getAllHierarchyCategories(int categoryUid);

    @Query(value = "SELECT c1.category_uid FROM category c1 " +
            " LEFT JOIN category c2 ON c2.category_uid = c1.parent_category_uid " +
            " LEFT JOIN category c3 ON c3.category_uid = c2.parent_category_uid " +
            " LEFT JOIN category c4 ON c4.category_uid = c3.parent_category_uid " +
            " WHERE ?1 IN (c1.category_uid, c2.category_uid, c3.category_uid, c4.category_uid) AND c1.status = 1 " +
            " ORDER BY c1.depth, c1.category_order",
            nativeQuery = true)
    List<Integer> getSubAllCategoryUidsWithIt(int categoryUid);

    @Query(value = "SELECT b.brand_uid, b.name_en, b.name_ko, b.logo_img, b.back_img, count(p.pdt_uid) AS pdt_count " +
            " FROM brand b JOIN pdt p ON p.brand_uid = b.brand_uid AND b.status = 1 " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1, 2, 3) AND p.category_uid IN ?1" +
            " GROUP BY b.brand_uid HAVING pdt_count > 0 " +
            " ORDER BY pdt_count DESC LIMIT 10", nativeQuery = true)
    List<Object> getRecommendedBrandByCategoryUids(List<Integer> categoryUids);

    @Query(value = "SELECT count(p.pdt_uid) FROM pdt p " +
            " WHERE p.photoshop_yn = 1 AND p.status IN (1, 2, 3) AND p.category_uid IN ?1", nativeQuery = true)
    Integer getPdtCountByCategoryUids(List<Integer> categoryUids);

    @Query(value = "SELECT c.category_uid FROM category c WHERE c.category_name LIKE %?1% AND c.status = 1 ORDER BY c.depth DESC",
            nativeQuery = true)
    List<Integer> getByKeyword(String keyword);
}
