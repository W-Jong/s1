package com.kyad.selluvapi.enumtype;

public enum SEARCH_PDT_GROUP_TYPE {
    ALL(0), MALE(1), FEMALE(2), KIDS(4), STYLE(3);

    private int code;

    private SEARCH_PDT_GROUP_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
