package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrPushSettingDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrPushSettingRepository extends CrudRepository<UsrPushSettingDao, Integer> {

    @Query("select a from UsrPushSettingDao a")
    Page<UsrPushSettingDao> findAll(Pageable pageable);
}
