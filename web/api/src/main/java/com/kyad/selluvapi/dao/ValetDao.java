package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "valet", schema = "selluv")
public class ValetDao {
    private int valetUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid = 0;
    private int pdtUid = 0;
    private long reqPrice = 0;
    private long earnPrice = 0;
    private String reqName = "";
    private String reqPhone = "";
    private String reqAddress = "";
    private String reqAddressDetail = "";
    private int sendType = 1;
    private int receiveYn = 2;
    private String signImg = "";
    private String memo = "";
    private Timestamp edtTime = new Timestamp(System.currentTimeMillis());
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "valet_uid", nullable = false)
    public int getValetUid() {
        return valetUid;
    }

    public void setValetUid(int valetUid) {
        this.valetUid = valetUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "req_price", nullable = false)
    public long getReqPrice() {
        return reqPrice;
    }

    public void setReqPrice(long reqPrice) {
        this.reqPrice = reqPrice;
    }

    @Basic
    @Column(name = "earn_price", nullable = false)
    public long getEarnPrice() {
        return earnPrice;
    }

    public void setEarnPrice(long earnPrice) {
        this.earnPrice = earnPrice;
    }

    @Basic
    @Column(name = "req_name", nullable = false, length = 20)
    public String getReqName() {
        return reqName;
    }

    public void setReqName(String reqName) {
        this.reqName = reqName;
    }

    @Basic
    @Column(name = "req_phone", nullable = false, length = 30)
    public String getReqPhone() {
        return reqPhone;
    }

    public void setReqPhone(String reqPhone) {
        this.reqPhone = reqPhone;
    }

    @Basic
    @Column(name = "req_address", nullable = false, length = 63)
    public String getReqAddress() {
        return reqAddress;
    }

    public void setReqAddress(String reqAddress) {
        this.reqAddress = reqAddress;
    }

    @Basic
    @Column(name = "req_address_detail", nullable = false, length = 63)
    public String getReqAddressDetail() {
        return reqAddressDetail;
    }

    public void setReqAddressDetail(String reqAddressDetail) {
        this.reqAddressDetail = reqAddressDetail;
    }

    @Basic
    @Column(name = "send_type", nullable = false)
    public int getSendType() {
        return sendType;
    }

    public void setSendType(int sendType) {
        this.sendType = sendType;
    }

    @Basic
    @Column(name = "receive_yn", nullable = false)
    public int getReceiveYn() {
        return receiveYn;
    }

    public void setReceiveYn(int receiveYn) {
        this.receiveYn = receiveYn;
    }

    @Basic
    @Column(name = "sign_img", nullable = false, length = 255)
    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValetDao valetDao = (ValetDao) o;
        return valetUid == valetDao.valetUid &&
                usrUid == valetDao.usrUid &&
                pdtUid == valetDao.pdtUid &&
                reqPrice == valetDao.reqPrice &&
                earnPrice == valetDao.earnPrice &&
                sendType == valetDao.sendType &&
                receiveYn == valetDao.receiveYn &&
                status == valetDao.status &&
                Objects.equals(regTime, valetDao.regTime) &&
                Objects.equals(reqName, valetDao.reqName) &&
                Objects.equals(reqPhone, valetDao.reqPhone) &&
                Objects.equals(reqAddress, valetDao.reqAddress) &&
                Objects.equals(reqAddressDetail, valetDao.reqAddressDetail) &&
                Objects.equals(signImg, valetDao.signImg) &&
                Objects.equals(memo, valetDao.memo) &&
                Objects.equals(edtTime, valetDao.edtTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(valetUid, regTime, usrUid, pdtUid, reqPrice, earnPrice, reqName, reqPhone, reqAddress, reqAddressDetail, sendType, receiveYn, signImg, memo, edtTime, status);
    }
}
