package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserLikeGroupUpdateReq {

    @NotNull
    @ApiModelProperty("남성군 좋아요")
    private Boolean isMale;
    @NotNull
    @ApiModelProperty("여성군 좋아요")
    private Boolean isFemale;
    @NotNull
    @ApiModelProperty("키즈군 좋아요")
    private Boolean isChildren;
}
