package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrCardDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsrCardRepository extends CrudRepository<UsrCardDao, Integer> {

    @Query("select a from UsrCardDao a")
    Page<UsrCardDao> findAll(Pageable pageable);

    @Query("select uc from UsrCardDao uc where uc.usrUid = ?1 and uc.status <> 0 and uc.iamportValidYn = 1 order by uc.regTime desc")
    List<UsrCardDao> findAllByUsrUid(int usrUid);

    @Query(value = "SELECT * FROM usr_card uc WHERE uc.usr_uid = ?1 AND uc.status <> 0 AND uc.iamport_valid_yn = 1 " +
            " ORDER BY uc.reg_time DESC LIMIT 1", nativeQuery = true)
    UsrCardDao findActiveCardByUsrUid(int usrUid);
}
