package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.PAY_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class DealBuyReq {
    @ApiModelProperty("상품UID")
    @NotNull
    private int pdtUid;
    @ApiModelProperty("제안가격")
    @NotNull
    private long reqPrice;
    @ApiModelProperty("수령자주소 - 성함")
    @NotEmpty
    private String recipientNm;
    @ApiModelProperty("수령자주소 - 연락처")
    @NotEmpty
    private String recipientPhone;
    @ApiModelProperty("수령자주소 - 주소")
    @NotEmpty
    private String recipientAddress;
    @ApiModelProperty("결제유형")
    private PAY_TYPE payType;
    @ApiModelProperty("카드UID, 카드결제가 아닐때에는 0")
    @NotNull
    private int usrCardUid;
    @ApiModelProperty("정품감정서비스 사용여부, 0-사용안함, 1-사용")
    @NotNull
    private int pdtVerifyEnabled;
    @ApiModelProperty("구매프로모션코드")
    @NotNull
    private String payPromotion;
    @ApiModelProperty("사용할 셀럽머니")
    @Min(0)
    private long rewardPrice;
    @ApiModelProperty("도서지역 추가배송비")
    @NotNull
    @Min(value = 0)
    private long islandSendPrice;
}
