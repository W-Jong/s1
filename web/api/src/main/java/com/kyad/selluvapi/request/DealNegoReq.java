package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class DealNegoReq {
    @ApiModelProperty("상품UID")
    @NotNull
    private int pdtUid;
    @ApiModelProperty("제안가격")
    @NotNull
    private long reqPrice;
    @ApiModelProperty("수령자주소 - 성함")
    @NotEmpty
    private String recipientNm;
    @ApiModelProperty("수령자주소 - 연락처")
    @NotEmpty
    private String recipientPhone;
    @ApiModelProperty("수령자주소 - 주소")
    @NotEmpty
    private String recipientAddress;
    @ApiModelProperty("카드UID")
    @NotNull
    private int usrCardUid;
    @ApiModelProperty("도서지역 추가배송비")
    @NotNull
    @Min(value = 0)
    private long islandSendPrice;
}
