package com.kyad.selluvapi.dto.other;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
public class WorkingDayDto {
    @ApiModelProperty("타겟날짜")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate targetDate;
}
