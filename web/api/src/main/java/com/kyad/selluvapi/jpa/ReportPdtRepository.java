package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ReportPdtDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportPdtRepository extends CrudRepository<ReportPdtDao, Integer> {

    @Query("select a from ReportPdtDao a")
    Page<ReportPdtDao> findAll(Pageable pageable);

    int countAllByPdtUid(int pdtUid);
}
