package com.kyad.selluvapi.helper.iamport.request;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class EscrowData {

    @SerializedName("sender")
    private EscrowLogisPeopleAnnotation sender;

    @SerializedName("receiver")
    private EscrowLogisPeopleAnnotation receiver;

    @SerializedName("logis")
    private EscrowLogisInfoAnnotation logis;
}