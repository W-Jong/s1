package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserPasswordFindReq {

    @NotNull
    @ApiModelProperty("이메일")
    private String email;
    @NotNull
    @ApiModelProperty("닉네임")
    private String nickname;
}
