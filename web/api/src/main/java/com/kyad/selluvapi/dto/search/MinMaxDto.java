package com.kyad.selluvapi.dto.search;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MinMaxDto {
    @ApiModelProperty("최소값")
    private long min;
    @ApiModelProperty("최대값")
    private long max;
}
