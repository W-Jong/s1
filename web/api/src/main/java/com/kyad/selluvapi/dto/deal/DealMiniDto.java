package com.kyad.selluvapi.dto.deal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class DealMiniDto {
    @ApiModelProperty("거래UID")
    private int dealUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime;
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("구매자 회원UID")
    private int usrUid;
    @ApiModelProperty("판매자 회원UID")
    private int sellerUsrUid;
    @ApiModelProperty("상품 영문브랜드")
    private String brandEn;
    @ApiModelProperty("상품 타이틀")
    private String pdtTitle;
    @ApiModelProperty("상품 사이즈")
    private String pdtSize;
    @ApiModelProperty("상품이미지")
    private String pdtImg;
    @ApiModelProperty("상품가격")
    private long pdtPrice;
    @ApiModelProperty("배송비")
    private long sendPrice;
    @ApiModelProperty("정품인증가격, 0이면 정품인증요청안함")
    private long verifyPrice;
    @ApiModelProperty("결제금액")
    private long price;
    @ApiModelProperty("정산금액")
    private long cashPrice;
    @ApiModelProperty("결제시간")
    private Timestamp payTime;
    @ApiModelProperty("배송완료시간")
    private Timestamp deliveryTime;
    @ApiModelProperty("거래완료시간")
    private Timestamp compTime;
    @ApiModelProperty("정산완료시간")
    private Timestamp cashCompTime;
    @ApiModelProperty("네고 및 카운터네고 제안금액")
    private long reqPrice;
    @ApiModelProperty("거래상태 0-삭제 1-배송준비 2-배송진행 3-정품인증 4-배송완료 5-거래완료 " +
            "6-정산완료 7-반품접수 8-반품승인 9-반품완료 10-주문취소 11-네고제안 12-카운터네고제안")
    private int status;
}
