package com.kyad.selluvapi.rest.common;

public class CommonConstant {
    public static final int COUNT_PER_PAGE = 30; //리스트 페이지당 갯수
    //결제관련
    public static final long PRICE_PDT_VERIFICATION = 39000; // 본사 정품 감정 서비스 가격
    public static final float SELLUV_FEE_RATE = 0.089f;
    public static final float SELLUV_PAY_FEE_RATE = 0.035f;
    //서버호스트 URL
    public static final String SELLUV_HOST = "http://13.125.35.211/api/";

    //셀럽연락처번호
    public static final String SELLUV_PHONE_NUMBER = "02-3456-7890";

    //어드민용 토큰
    public static final String SELLUV_ADMIN_TOKEN = "ef8de1ae3192dfee311d3f9756c8cc24160647b";

    //셀럽주소
    public static final String SELLUV_DELEVERY_ADDRESS = "서울특별시 강남구";
    public static final String SELLUV_DELEVERY_ADDRESS_DETAIL = "테헤란로 105길";
    public static final String SELLUV_DELEVERY_POSTCODE = "123456";
    public static final String SELLUV_DELEVERY_TEL = "01012345678";
    public static final String SELLUV_DELEVERY_NAME = "셀럽";

}
