package com.kyad.selluvapi.helper.sweettracker.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class TrackingDetail {
    //배송상태 코드
    @SerializedName("code")
    private String code;

    //진행상태
    @SerializedName("kind")
    private String kind;

    //진행단계
    @SerializedName("level")
    private String level;

    //배송기사 이름
    @SerializedName("manName")
    private String manName;

    //배송기사 전화번호
    @SerializedName("manPic")
    private String manPic;

    //비고
    @SerializedName("remark")
    private String remark;

    //진행위치(지점)전화번호 (json 요청시)
    @SerializedName("telno")
    private String telno;

    //배송기사 전화번호 (json 요청시)
    @SerializedName("telno2")
    private String telno2;

    //진행시간 (json 요청시)
    @SerializedName("time")
    private String time;

    //진행시간
    @SerializedName("timeString")
    private String timeString;

    //진행위치지점 (json 요청시)
    @SerializedName("where")
    private String where;
}
