package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrBrandLikeDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrBrandLikeRepository extends CrudRepository<UsrBrandLikeDao, Integer> {

    @Query("select a from UsrBrandLikeDao a")
    Page<UsrBrandLikeDao> findAll(Pageable pageable);

    UsrBrandLikeDao getByUsrUidAndBrandUid(int usrUid, int brandUid);
}
