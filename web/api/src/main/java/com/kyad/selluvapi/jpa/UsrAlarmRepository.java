package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrAlarmDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrAlarmRepository extends CrudRepository<UsrAlarmDao, Integer> {

    @Query("select a from UsrAlarmDao a")
    Page<UsrAlarmDao> findAll(Pageable pageable);

    long countAllByUsrUidAndKindAndStatus(int usrUid, int kind, int status);

    long countAllByUsrUidAndStatus(int usrUid, int status);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE usr_alarm ua SET ua.status = 1 WHERE ua.usr_uid = ?1 AND ua.kind = ?2 AND ua.status = 2",
            nativeQuery = true)
    void makeReadMarkByUsrUidAndKind(int usrUid, int kind);

    Page<UsrAlarmDao> findAllByUsrUidAndKindOrderByRegTimeDesc(Pageable pageable, int usrUid, int kind);
}
