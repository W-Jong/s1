package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.BrandDao;
import com.kyad.selluvapi.dto.brand.BrandDetailDto;
import com.kyad.selluvapi.dto.brand.BrandListDto;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.enumtype.SEARCH_PDT_GROUP_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.jpa.BrandRepository;
import com.kyad.selluvapi.rest.common.exception.BrandNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class BrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private PdtService pdtService;

    public BrandDao getBrandInfo(int brandUid) {
        BrandDao brand = brandRepository.findOne(brandUid);
        if (brand == null || brand.getStatus() != 1) {
            throw new BrandNotFoundException();
        }

        return brand;
    }

    public BrandDao getByBrandUid(int brandUid) {
        return brandRepository.findOne(brandUid);
    }

    public BrandDao save(BrandDao dao) {
        return brandRepository.save(dao);
    }

    public List<BrandMiniDto> getAllBrandList() {
        List<BrandDao> daoList = brandRepository.findAllByStatus(1);
        if (daoList == null)
            return null;
        List<BrandMiniDto> dtoList = new ArrayList<>();
        for (BrandDao dao : daoList) {
            BrandMiniDto dto = convertBrandDaoToBrandMiniDto(dao);
            dtoList.add(dto);
        }

        return dtoList;
    }

    public List<BrandMiniDto> getRecommendedBrandListByPdtGroup(int pdtGroup) {
        List<BrandDao> daoList = brandRepository.findRecommendBrandByPdtGroup(pdtGroup);
        if (daoList == null)
            return null;
        List<BrandMiniDto> dtoList = new ArrayList<>();
        for (BrandDao dao : daoList) {
            BrandMiniDto dto = convertBrandDaoToBrandMiniDto(dao);
            dtoList.add(dto);
        }

        return dtoList;
    }

    public BrandMiniDto convertBrandDaoToBrandMiniDto(BrandDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        BrandMiniDto dto = modelMapper.map(dao, BrandMiniDto.class);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("brand", dao.getBrandUid(), dao.getProfileImg()));

        return dto;
    }

    public List<BrandListDto> getFollowBrandList(int usrUid, int likeGroup) {
        List<BrandDao> followBrandList = brandRepository.findAllFollowListByUsrUid(usrUid);
        if (followBrandList == null)
            return null;

        List<BrandListDto> brandListDtoList = new ArrayList<>();
        for (BrandDao dao : followBrandList) {
            BrandListDto dto = new BrandListDto();
            dto.setBrandUid(dao.getBrandUid());
            dto.setFirstEn(dao.getFirstEn());
            dto.setFirstKo(dao.getFirstKo());
            dto.setNameEn(dao.getNameEn());
            dto.setNameKo(dao.getNameKo());
            dto.setLogoImg(StarmanHelper.getTargetImageUrl("brand", dao.getBrandUid(), dao.getLogoImg()));
            dto.setProfileImg(StarmanHelper.getTargetImageUrl("brand", dao.getBrandUid(), dao.getProfileImg()));
            dto.setBackImg(StarmanHelper.getTargetImageUrl("brand", dao.getBrandUid(), dao.getBackImg()));
            dto.setBrandLikeCount(brandRepository.countBrandLikeByBrandUid(dao.getBrandUid()));
            dto.setBrandLikeStatus(true);
            dto.setPdtList(pdtService.getNewPdtByBrandUidAndLikeGroup(dao.getBrandUid(), likeGroup));

            brandListDtoList.add(dto);
        }

        return brandListDtoList;
    }

    public List<BrandListDto> getFameBrandListByPdtGroup(int usrUid, int pdtGroup) {
        List<Object> objectList = brandRepository.findFameBrandByPdtGroup(usrUid, pdtGroup);
        if (objectList == null)
            return null;
        List<BrandListDto> dtoList = new ArrayList<>();
        for (Object object : objectList) {
            BrandListDto dto = convertObjectToBrandListDto((Object[]) object);
            dto.setPdtList(pdtService.getRecommendedPdtByBrandUidAndLikeGroup(dto.getBrandUid(), pdtGroup));
            dtoList.add(dto);
        }

        return dtoList;
    }

    public List<BrandListDto> getFameAllBrandListByPdtGroup(int usrUid, int pdtGroup) {
        List<Object> objectList = brandRepository.findFameAllBrandByPdtGroup(usrUid, pdtGroup);
        if (objectList == null)
            return null;
        List<BrandListDto> dtoList = new ArrayList<>();
        for (Object object : objectList) {
            BrandListDto dto = convertObjectToBrandListDto((Object[]) object);
            dto.setPdtList(null);
            dtoList.add(dto);
        }

        return dtoList;
    }

    public BrandListDto convertObjectToBrandListDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        BrandListDto dto = new BrandListDto();
        dto.setBrandUid((int) objects[i++]);
        dto.setFirstEn((String) objects[i++]);
        dto.setFirstKo((String) objects[i++]);
        dto.setNameEn((String) objects[i++]);
        dto.setNameKo((String) objects[i++]);
        dto.setLogoImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setBackImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setBrandLikeCount(((BigInteger) objects[i++]).longValue());
        dto.setBrandLikeStatus(((BigInteger) objects[i++]).intValue() > 0);

        return dto;
    }

    public List<BrandListDto> getAllDetailBrandList(int usrUid, int orderColumn) {
        List<Object> objectList = brandRepository.findAllDetailList(usrUid, orderColumn);
        if (objectList == null)
            return null;
        List<BrandListDto> dtoList = new ArrayList<>();
        for (Object object : objectList) {
            BrandListDto dto = convertObjectToBrandListDto((Object[]) object);
            dtoList.add(dto);
        }

        return dtoList;
    }

    public BrandDetailDto getBrandDetail(int brandUid, int usrUid) {
        Object objectBrand = brandRepository.findByBrandUid(brandUid, usrUid);
        if (objectBrand == null)
            return null;

        Object[] objects = (Object[]) objectBrand;
        int i = 0;
        BrandDetailDto dto = new BrandDetailDto();
        dto.setBrandUid((int) objects[i++]);
        dto.setFirstEn((String) objects[i++]);
        dto.setFirstKo((String) objects[i++]);
        dto.setNameEn((String) objects[i++]);
        dto.setNameKo((String) objects[i++]);
        dto.setLicenseUrl((String) objects[i++]);
        dto.setLogoImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setBackImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setBrandLikeCount(((BigInteger) objects[i++]).intValue());
        dto.setBrandLikeStatus(((BigInteger) objects[i++]).intValue() > 0);
        dto.setMalePdtCount(((BigInteger) objects[i++]).intValue());
        dto.setFemalePdtCount(((BigInteger) objects[i++]).intValue());
        dto.setKidsPdtCount(((BigInteger) objects[i++]).intValue());
        dto.setStylePdtCount(((BigInteger) objects[i++]).intValue());

        return dto;
    }

    public Page<PdtListDto> getBrandPdtList(Pageable pageable, int usrUid, int brandUid, SEARCH_PDT_GROUP_TYPE searchPdtGroupType) {

        if (searchPdtGroupType == SEARCH_PDT_GROUP_TYPE.STYLE) {
            return pdtService.getPdtStyleListByBrandUid(pageable, usrUid, brandUid);
        } else if (searchPdtGroupType == SEARCH_PDT_GROUP_TYPE.ALL) {
            return pdtService.getPdtListByBrandUidAndPdtGroup(pageable, usrUid, brandUid, 7);
        } else {
            return pdtService.getPdtListByBrandUidAndPdtGroup(pageable, usrUid, brandUid, searchPdtGroupType.getCode());
        }
    }

    public List<String> getRecommendModelByBrandUid(int brandUid) {
        return pdtService.getRecommendModelByBrandUid(brandUid);
    }

    public List<BrandDao> getListByKeyword(String keyword) {
        return brandRepository.getByKeyword(keyword);
    }
}
