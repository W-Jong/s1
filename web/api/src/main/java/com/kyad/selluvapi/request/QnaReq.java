package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
public class QnaReq {
    @ApiModelProperty("타이틀")
    @NotEmpty
    private String title;
    @ApiModelProperty("내용")
    @NotEmpty
    private String content;
}
