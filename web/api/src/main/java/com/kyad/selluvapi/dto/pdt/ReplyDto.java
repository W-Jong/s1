package com.kyad.selluvapi.dto.pdt;

import com.kyad.selluvapi.dao.ReplyDao;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReplyDto {
    @ApiModelProperty("댓글내용")
    private ReplyDao reply;
    @ApiModelProperty("등록유저")
    private UsrMiniDto usr;
}
