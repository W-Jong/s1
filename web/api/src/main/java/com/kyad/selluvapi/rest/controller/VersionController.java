package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.helper.cvsnet.CvsnetClientHelper;
import com.kyad.selluvapi.helper.cvsnet.request.ReservationReq;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.IamportDeliveryCompanyHelper;
import com.kyad.selluvapi.helper.iamport.request.EscrowData;
import com.kyad.selluvapi.helper.iamport.request.EscrowLogisInfoAnnotation;
import com.kyad.selluvapi.helper.iamport.request.EscrowLogisPeopleAnnotation;
import com.kyad.selluvapi.helper.iamport.response.EscrowLogisAnnotation;
import com.kyad.selluvapi.request.DealDeliveryReq;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.Scheduler;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;

@RestController
@Api(value = "/version", description = "버전체크")
@RequestMapping("/version")
public class VersionController {

    private final Logger LOG = LoggerFactory.getLogger(VersionController.class);

    @Autowired
    private AppVersionService appVersionService;

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private UsrFeedService usrFeedService;

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private DealService dealService;

    @Autowired
    private OtherService otherService;

    @Autowired
    private UsrAlarmService usrAlarmService;


    @ApiOperation(value = "최신앱 버전코드 얻기",
            notes = "앱 유형별로 최신버전코드를 가져옵니다.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "app_type",
                    value = "1이면 iOS앱, 2이면 Android앱",
                    paramType = "path",
                    required = true,
                    allowableValues = "1,2")
    })
    @GetMapping(value = "/{app_type}")
    @Transactional(readOnly = true)
    public GenericResponse<AppVersionDao> appVersionList(@PathVariable("app_type") int app_type) {
        AppVersionDao appVersion = appVersionService.getLastedVersion(app_type);
        return new GenericResponse<AppVersionDao>(ResponseMeta.OK, appVersion);
    }

    /*
    public GenericResponse<Object> testAPI() {
        try {
            URL url = new URL("http://192.168.0.10:9983/api");//?api_type=check_app_version

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);

            String params = "";
            params += "api_type=" + URLEncoder.encode("check_app_version", "UTF-8");

            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(params.getBytes("UTF-8"));
            outputStream.flush();
            outputStream.close();

            int responseCode = conn.getResponseCode();

            StringBuilder response = new StringBuilder();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response.append(line).append("\n");
                }
                br.close();
            } else {
                response.append("");
            }

            String result = response.toString().trim();

            ObjectMapper objectMapper = new ObjectMapper();
            Object resultJson = objectMapper.readValue(result, Object.class);

            HashMap<String, Object> rootJson = (HashMap<String, Object>) resultJson;
            HashMap<String, Integer> nodeResult = (HashMap<String, Integer>) rootJson.get("result");

            return new GenericResponse<>(ResponseMeta.OK, nodeResult);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @GetMapping("/schedule")
    @Transactional
    public GenericResponse<Void> testSchedule() {

        scheduler.updatePopularStore();

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @GetMapping("/push")
    @Transactional
    public GenericResponse<Void> testPush(
            @RequestParam("deviceToken") String deviceToken) {

//        String to = "ejD6tzQX8C4:APA91bG_WlIjId5TpbYvCKx_p5GmrmdQpf4xusahLNpp2qXHg8U3Vn8yinNbP2rcAF4t8Dsiu8T9qeIr6OY1SC0Ie_1ZoK0RZokNdDeNEHeJKnRA0hVS_n7GO-cpFyhkhy4x5Ym3dFgH";
        String body = "내용입니다.";

        FCMPushHelper.pushFCMNotification(deviceToken, body, FCMPushHelper.ALARM_TYPE.POST, 1);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }
    */

//    @GetMapping("/asynctest")
//    public GenericResponse<String> testAsync() {
//
//        String a = "1";
//
//        UsrDao usrDao = usrService.getByUsrUid(1);
//        PdtDao pdtDao = pdtService.getPdtByPdtUid(1);
//
//        usrFeedService.insertFeed(FEED_TYPE.FEED06_FOLLOWING_USER_PDT, usrDao, pdtDao);
//
//        a = a + "3434";
//
//        return new GenericResponse<>(ResponseMeta.OK, a);
//    }

//    @GetMapping("/sweettracker")
//    public GenericResponse<Object> testSweetTracker() {
//
//        SweetTrackerClientHelper sweetTracker = new SweetTrackerClientHelper();
//
////        SweetTrackerResponse<Company> response = sweetTracker.getCompanyList();
//
//        InvoiceReq invoice = new InvoiceReq();
//        invoice.setTCode(SweetTrackerDeliveryCompanyHelper.getCompanyCode("CJ대한통운"));
//        invoice.setTInvoice("687099730363");
//        SweetTrackerResponse<TrackingInfo> response = sweetTracker.getTrackingInfo(invoice);
//
//        return new GenericResponse<>(ResponseMeta.OK, response);
//    }

//    @GetMapping("/dateutil")
//    public GenericResponse<Object> testDateCalculatorHelper() {
//
//        DateCalculatorHelper calculatorHelper = new DateCalculatorHelper();
//        String result = "";
//        result += " ** " + calculatorHelper.getDelayedWorkingDate(0);
//        result += " ** " + calculatorHelper.getDelayedWorkingDate(1);
//        result += " ** " + calculatorHelper.getDelayedWorkingDate(2);
//        result += " ** " + calculatorHelper.getDelayedWorkingDate(-3);
//        result += " ** " + calculatorHelper.getDelayedWorkingDate(-7);
//
//        return new GenericResponse<>(ResponseMeta.OK, result);
//    }

    @GetMapping("/ftptest")
    public GenericResponse<Object> testFTPClient() {

        CvsnetClientHelper cvsnetClientHelper = new CvsnetClientHelper();
        String result = cvsnetClientHelper.getFTPFileContent();

        return new GenericResponse<>(ResponseMeta.OK, result);
    }

    @PostMapping("/cvsnettest/{dealUid}")
    public GenericResponse<Object> testCvsnet(
            @PathVariable("dealUid") int dealUid,
            @RequestBody @Valid DealDeliveryReq deliveryReq) {

        DealDao deal = dealService.getDealInfo(dealUid);

        ReservationReq reservationReq = new ReservationReq();
        reservationReq.setCust_ord_no("" + CvsnetClientHelper.SELLUV_CVSNET_CODE + String.format("%09d", dealUid) + deliveryReq.getDeliveryType());
        reservationReq.setAcpt_dt(new java.text.SimpleDateFormat("yyyyMMdd").format(System.currentTimeMillis()));
        reservationReq.setSend_tel(deliveryReq.getRecipientPhone());
        reservationReq.setSend_name(deliveryReq.getRecipientNm());
        reservationReq.setSend_zip(deliveryReq.getRecipientAddressDetail());
        String[] sendAddressArray = deliveryReq.getRecipientAddress().split("\n");
        reservationReq.setSend_addr1(sendAddressArray.length > 0 ? sendAddressArray[0] : "");
        reservationReq.setSend_addr2(sendAddressArray.length > 1 ? sendAddressArray[1] : "");
        reservationReq.setGoods(deal.getPdtTitle());
        reservationReq.setGoods_cnt(1);
        reservationReq.setGoods_price(deal.getPrice() > 500000 ? 500000 : deal.getPrice());
        if (deliveryReq.getDeliveryType() == 2) {
            reservationReq.setReceive_tel(CommonConstant.SELLUV_DELEVERY_TEL);
            reservationReq.setReceive_name(CommonConstant.SELLUV_DELEVERY_NAME);
            reservationReq.setReceive_addr1(CommonConstant.SELLUV_DELEVERY_ADDRESS);
            reservationReq.setReceive_addr2(CommonConstant.SELLUV_DELEVERY_ADDRESS_DETAIL);
            reservationReq.setReceive_zip(CommonConstant.SELLUV_DELEVERY_POSTCODE);
        } else {
            reservationReq.setReceive_tel(deal.getRecipientPhone());
            reservationReq.setReceive_name(deal.getRecipientNm());
            String[] receiveAddressArray = deal.getRecipientAddress().split("\n");
            reservationReq.setReceive_addr1(receiveAddressArray.length > 0 ? receiveAddressArray[0] : "");
            reservationReq.setReceive_addr2(receiveAddressArray.length > 1 ? receiveAddressArray[1] : "");
            reservationReq.setReceive_zip(receiveAddressArray.length > 2 ? receiveAddressArray[2] : "");
        }
        reservationReq.setTls_gb(deliveryReq.getDeliveryType() == 3 ? 2 : 1);
        reservationReq.setGoods_info(deal.getPdtTitle());
        reservationReq.setOrder_num(dealUid);

        CvsnetClientHelper cvsnetClientHelper = new CvsnetClientHelper();
        cvsnetClientHelper.requestReservation(reservationReq);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "어드민용 알림API",
            notes = "어드민의 알림을 설정한다.")
    @PostMapping("/alarm")
    public GenericResponse<Void> adminAlarm(
            @RequestHeader(value = "adminToken") String adminToken,
            @RequestParam(value = "pushType") PUSH_TYPE pushType,
            @RequestParam(value = "targetUid") int targetUid) {

        if (!CommonConstant.SELLUV_ADMIN_TOKEN.equals(adminToken)) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (pushType.getCode() != PUSH_TYPE.PUSH02_B_DELIVERY_SENT.getCode() &&
                pushType.getCode() != PUSH_TYPE.PUSH12_REWARD_REPORT.getCode() &&
                pushType.getCode() != PUSH_TYPE.PUSH22_EVENT.getCode() &&
                pushType.getCode() != PUSH_TYPE.PUSH23_NOTICE.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        switch (pushType) {
            case PUSH02_B_DELIVERY_SENT: {
                DealDao deal = dealService.getDealByDealUid(targetUid);
                if (deal == null)
                    return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

                UsrDao usr = usrService.getByUsrUid(deal.getUsrUid());
                UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());
                if (usr == null || usr.getStatus() != 1 || sellerUsr == null || sellerUsr.getStatus() != 1)
                    return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

                usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH02_B_DELIVERY_SENT, usr, sellerUsr, deal, null, "2", 0);
            }
            break;
            case PUSH12_REWARD_REPORT: {
                ReportPdtDao reportPdt = pdtService.findOneReportPdt(targetUid);
                if (reportPdt == null)
                    return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

                UsrDao usr = usrService.getByUsrUid(reportPdt.getUsrUid());
                PdtDao pdt = pdtService.getPdtByPdtUid(reportPdt.getPdtUid());
                if (usr == null || usr.getStatus() != 1 || pdt == null)
                    return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

                usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH12_REWARD_REPORT, usr, null, null, pdt);
            }
            break;
            case PUSH22_EVENT: {
                EventDao event = otherService.findOneEvent(targetUid);
                if (event == null)
                    return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

                List<UsrDao> usrDaoList1 = usrService.findAllActiveUser();
                for (UsrDao usr1 : usrDaoList1) {
                    usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH22_EVENT, usr1, null, null, null, event.getTitle(), targetUid);
                }
            }
            break;
            case PUSH23_NOTICE: {
                NoticeDao notice = otherService.findOneNotice(targetUid);
                if (notice == null)
                    return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);

                List<UsrDao> usrDaoList2 = usrService.findAllActiveUser();
                for (UsrDao usr2 : usrDaoList2) {
                    usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH23_NOTICE, usr2, null, null, null, notice.getTitle(), targetUid);
                }
            }
            break;
            default:
                break;
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "어드민용 에스크로 등록API",
            notes = "거래시 정품인증서비스를 요청한 경우 구매자에게 발송시 에스크로 정보를 등록한다.")
    @PostMapping("/escrow/{dealUid}")
    public GenericResponse<Void> adminAlarm(
            @RequestHeader(value = "adminToken") String adminToken,
            @PathVariable(value = "dealUid") int dealUid) {
        if (!CommonConstant.SELLUV_ADMIN_TOKEN.equals(adminToken)) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        DealDao deal = null;
        try {
            deal = dealService.getDealInfo(dealUid);
        } catch (Exception e) {
            e.printStackTrace();
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        if (deal.getVerifyPrice() == 0) { //정품인증서비스요청이 아닌 경우
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        //에스크로 등록
        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        EscrowData escrowData = new EscrowData();
        UsrDao buyerUsr = usrService.getByUsrUid(deal.getUsrUid());
        if (buyerUsr == null || buyerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        EscrowLogisPeopleAnnotation sender = new EscrowLogisPeopleAnnotation();
        sender.setName(buyerUsr.getUsrNm());
        sender.setTel(buyerUsr.getUsrPhone());
        UsrAddressDao sellerAddress = usrService.getAddressActiveByUsrUid(buyerUsr.getUsrUid());
        sender.setAddr((sellerAddress == null) ? "" : sellerAddress.getAddressDetail());
        sender.setPostcode((sellerAddress == null) ? "" : sellerAddress.getAddressDetailSub());
        escrowData.setSender(sender);

        EscrowLogisPeopleAnnotation receiver = new EscrowLogisPeopleAnnotation();
        receiver.setName(deal.getRecipientNm());
        receiver.setTel(deal.getRecipientPhone());
        receiver.setAddr(deal.getRecipientAddress());
        receiver.setPostcode("");
        escrowData.setReceiver(receiver);

        EscrowLogisInfoAnnotation logis = new EscrowLogisInfoAnnotation();
        String[] deliveryInfos = deal.getDeliveryNumber().split(" ");
        if (deliveryInfos.length < 1) {
            logis.setCompany("");
            logis.setInvoice("");
        } else if (deliveryInfos.length < 2) {
            logis.setCompany(IamportDeliveryCompanyHelper.getCompanyCode(deliveryInfos[0]));
            logis.setInvoice("");
        } else {
            logis.setCompany(IamportDeliveryCompanyHelper.getCompanyCode(deliveryInfos[0]));
            logis.setInvoice(deliveryInfos[1]);
        }
        logis.setSent_at(new Timestamp(System.currentTimeMillis()));

        escrowData.setLogis(logis);

        EscrowLogisAnnotation escrowLogisAnnotation = null;
        try {
            escrowLogisAnnotation = iamportClientHelper.registerEscrowProcess(deal.getPayImportCode(), escrowData).getResponse();
        } catch (Exception e) {
            escrowLogisAnnotation = null;
            e.printStackTrace();
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

}
