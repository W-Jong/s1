package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
public class DealDeliveryNumReq {
    @NotNull
    @ApiModelProperty("택배사")
    private String deliveryCompany;

    @NotNull
    @ApiModelProperty("운송장번호")
    private String deliveryNumber;
}
