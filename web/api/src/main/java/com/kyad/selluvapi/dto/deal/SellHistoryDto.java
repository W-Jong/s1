package com.kyad.selluvapi.dto.deal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class SellHistoryDto {
    @ApiModelProperty("네고제안수")
    private long negoCount;
    @ApiModelProperty("정산완료수")
    private long cachedCount;
    @ApiModelProperty("발송대기수")
    private long preparedCount;
    @ApiModelProperty("배송진행수")
    private long progressCount;
    @ApiModelProperty("배송완료수")
    private long sentCount;
    @ApiModelProperty("거래완료수")
    private long completedCount;
    @ApiModelProperty("판매내역")
    private Page<DealListDto> history;
}
