package com.kyad.selluvapi.helper.sweettracker;

public class SweetTrackerDeliveryCompanyHelper {
    public static String getCompanyCode(String company) {
        /*String[] deliveryCompanies = {
                "우체국택배",
                "CJ대한통운",
                "한진택배",
                "롯데택배",
                "로젠택배",
                "KGB택배",
                "KG로지스",
                "GTX로지스",
                "포스트박스",
                "SC로지스",
                "경동합동택배",
                "아주택배",
                "고려택배",
                "하나로택배",
                "대신택배",
                "천일택배",
                "건영택배",
                "한의사랑택배",
                "용마로지스",
                "일양로지스",
        };*/

        String result = "00";
        if (company == null)
            return result;

        switch (company) {
            case "우체국택배":
                result = "01";
                break;

            case "대한통운":
            case "CJ대한통운":
            case "SC로지스":
                result = "04";
                break;

            case "한진택배":
                result = "05";
                break;

            case "롯데택배":
                result = "08";
                break;

            case "로젠택배":
                result = "06";
                break;

            case "KGB택배":
                result = "10";
                break;

            case "KG로지스":
                result = "07";
                break;

            case "GTX로지스":
                result = "15";
                break;

            case "포스트박스":
                result = "24";
                break;

            case "경동합동택배":
            case "경동택배":
                result = "23";
                break;

            case "고려택배":
                result = "19";
                break;

            case "대신택배":
                result = "22";
                break;

            case "천일택배":
                result = "17";
                break;

            case "건영택배":
                result = "18";
                break;

            case "한의사랑택배":
                result = "16";
                break;

            case "일양로지스":
                result = "11";
                break;

            default:
                break;
        }

        return result;
    }
}
