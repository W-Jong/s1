package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.UsrBlockDao;
import com.kyad.selluvapi.jpa.UsrBlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UsrBlockService {

    @Autowired
    private UsrBlockRepository usrBlockRepository;

    public Page<UsrBlockDao> findAll(Pageable pageable) {
        return usrBlockRepository.findAll(pageable);
    }

    public UsrBlockDao getByUsrUidAndPeerUsrUid(int usrUid, int peerUsrUid) {
        return usrBlockRepository.getByUsrUidAndPeerUsrUid(usrUid, peerUsrUid);
    }

    public UsrBlockDao save(UsrBlockDao dao) {
        return usrBlockRepository.save(dao);
    }

    public void delete(int usrBlockUid) {
        usrBlockRepository.delete(usrBlockUid);
    }
}
