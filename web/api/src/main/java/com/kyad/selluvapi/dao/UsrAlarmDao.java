package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_alarm", schema = "selluv")
public class UsrAlarmDao {
    private int usrAlarmUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int kind;
    private int subKind;
    private int targetUid;
    private String content = "";
    private int pdtUid = 0;
    private String profileImg = "";
    private String usrProfileImg = "";
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_alarm_uid", nullable = false)
    public int getUsrAlarmUid() {
        return usrAlarmUid;
    }

    public void setUsrAlarmUid(int usrAlarmUid) {
        this.usrAlarmUid = usrAlarmUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "sub_kind", nullable = false)
    public int getSubKind() {
        return subKind;
    }

    public void setSubKind(int subKind) {
        this.subKind = subKind;
    }

    @Basic
    @Column(name = "target_uid", nullable = false)
    public int getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(int targetUid) {
        this.targetUid = targetUid;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "profile_img", nullable = false, length = 255)
    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    @Basic
    @Column(name = "usr_profile_img", nullable = false, length = 255)
    public String getUsrProfileImg() {
        return usrProfileImg;
    }

    public void setUsrProfileImg(String usrProfileImg) {
        this.usrProfileImg = usrProfileImg;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrAlarmDao that = (UsrAlarmDao) o;
        return usrAlarmUid == that.usrAlarmUid &&
                usrUid == that.usrUid &&
                kind == that.kind &&
                subKind == that.subKind &&
                targetUid == that.targetUid &&
                pdtUid == that.pdtUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(content, that.content) &&
                Objects.equals(profileImg, that.profileImg);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrAlarmUid, regTime, usrUid, kind, subKind, targetUid, content, pdtUid, profileImg, status);
    }
}
