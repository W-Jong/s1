package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;


@Data
public class UserAccountReq {

    @NotEmpty
    @ApiModelProperty("은행명")
    private String bankNm;
    @NotEmpty
    @ApiModelProperty("계좌번호")
    private String accountNum;
}
