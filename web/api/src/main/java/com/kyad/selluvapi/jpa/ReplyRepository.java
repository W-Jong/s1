package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ReplyDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ReplyRepository extends CrudRepository<ReplyDao, Integer> {

    @Query("select a from ReplyDao a")
    Page<ReplyDao> findAll(Pageable pageable);

    @Query("SELECT count(r) from ReplyDao r where r.pdtUid = ?1 and r.status = 1")
    long countByPdtUid(int pdtUid);

    @Query("SELECT r, u from ReplyDao r join UsrDao u on u.usrUid = r.usrUid and u.status = 1 where r.pdtUid = ?1 and r.status = 1 ")
    Page<Object> findAllByPdtUid(Pageable pageable, int pdtUid);
}
