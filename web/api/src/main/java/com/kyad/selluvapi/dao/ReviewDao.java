package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "review", schema = "selluv")
public class ReviewDao {
    private int reviewUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("거래UID")
    private int dealUid;
    @ApiModelProperty("대상회원UID")
    private int peerUsrUid;
    @ApiModelProperty("후기분류 1-판매자후기 2-구매자후기")
    private int type;
    @ApiModelProperty("후기내용")
    private String content = "";
    @ApiModelProperty("점수 2-만족 1-보통 0-불만족")
    private int point = 0;
    @JsonIgnore
    private long rewardPrice = 0;
    @JsonIgnore
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_uid", nullable = false)
    public int getReviewUid() {
        return reviewUid;
    }

    public void setReviewUid(int reviewUid) {
        this.reviewUid = reviewUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "deal_uid", nullable = false)
    public int getDealUid() {
        return dealUid;
    }

    public void setDealUid(int dealUid) {
        this.dealUid = dealUid;
    }

    @Basic
    @Column(name = "peer_usr_uid", nullable = false)
    public int getPeerUsrUid() {
        return peerUsrUid;
    }

    public void setPeerUsrUid(int peerUsrUid) {
        this.peerUsrUid = peerUsrUid;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 511)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "point", nullable = false)
    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Basic
    @Column(name = "reward_price", nullable = false)
    public long getRewardPrice() {
        return rewardPrice;
    }

    public void setRewardPrice(long rewardPrice) {
        this.rewardPrice = rewardPrice;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewDao reviewDao = (ReviewDao) o;
        return reviewUid == reviewDao.reviewUid &&
                usrUid == reviewDao.usrUid &&
                dealUid == reviewDao.dealUid &&
                peerUsrUid == reviewDao.peerUsrUid &&
                type == reviewDao.type &&
                point == reviewDao.point &&
                rewardPrice == reviewDao.rewardPrice &&
                status == reviewDao.status &&
                Objects.equals(regTime, reviewDao.regTime) &&
                Objects.equals(content, reviewDao.content);
    }

    @Override
    public int hashCode() {

        return Objects.hash(reviewUid, regTime, usrUid, dealUid, peerUsrUid, type, content, point, rewardPrice, status);
    }
}
