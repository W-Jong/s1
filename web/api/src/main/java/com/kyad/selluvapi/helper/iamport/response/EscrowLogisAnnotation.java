package com.kyad.selluvapi.helper.iamport.response;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.rest.common.serializer.GsonUnixTimestampDeserializer;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class EscrowLogisAnnotation {
    @SerializedName("company")
    String company;

    @SerializedName("invoice")
    String invoice;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("sent_at")
    Timestamp sent_at;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("applied_at")
    Timestamp applied_at;
}
