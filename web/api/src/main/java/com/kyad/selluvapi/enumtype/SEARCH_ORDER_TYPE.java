package com.kyad.selluvapi.enumtype;

public enum SEARCH_ORDER_TYPE {
    RECOMMEND(1), NEW(2), FAME(3), PRICELOW(4), PRICEHIGH(5);

    private int code;

    private SEARCH_ORDER_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
