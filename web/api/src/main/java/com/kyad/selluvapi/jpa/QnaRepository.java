package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.QnaDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QnaRepository extends CrudRepository<QnaDao, Integer> {

    @Query("select a from QnaDao a")
    Page<QnaDao> findAll(Pageable pageable);

    List<QnaDao> findAllByUsrUidAndStatusNotOrderByRegTimeDesc(int usrUid, int status);
}
