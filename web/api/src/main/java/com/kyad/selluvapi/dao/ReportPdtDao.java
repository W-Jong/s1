package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "report_pdt", schema = "selluv")
public class ReportPdtDao {
    private int reportPdtUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int pdtUid;
    private int rank = 1;
    private String content = "";
    private int rewardYn = 0;
    private long rewardPrice = 0;
    private Timestamp compTime = null;
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_pdt_uid", nullable = false)
    public int getReportPdtUid() {
        return reportPdtUid;
    }

    public void setReportPdtUid(int reportPdtUid) {
        this.reportPdtUid = reportPdtUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "rank", nullable = false)
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "reward_yn", nullable = false)
    public int getRewardYn() {
        return rewardYn;
    }

    public void setRewardYn(int rewardYn) {
        this.rewardYn = rewardYn;
    }

    @Basic
    @Column(name = "reward_price", nullable = false)
    public long getRewardPrice() {
        return rewardPrice;
    }

    public void setRewardPrice(long rewardPrice) {
        this.rewardPrice = rewardPrice;
    }

    @Basic
    @Column(name = "comp_time", nullable = true)
    public Timestamp getCompTime() {
        return compTime;
    }

    public void setCompTime(Timestamp compTime) {
        this.compTime = compTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportPdtDao that = (ReportPdtDao) o;
        return reportPdtUid == that.reportPdtUid &&
                usrUid == that.usrUid &&
                pdtUid == that.pdtUid &&
                rank == that.rank &&
                rewardYn == that.rewardYn &&
                rewardPrice == that.rewardPrice &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(content, that.content) &&
                Objects.equals(compTime, that.compTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(reportPdtUid, regTime, usrUid, pdtUid, rank, content, rewardYn, rewardPrice, compTime, status);
    }
}
