package com.kyad.selluvapi.helper;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifImageDirectory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class StarmanHelper {

    /**
     * upload등록부는 chown tomcat7:tomcat7 -R upload/ 로 사용자권한을 주어야 함.
     */
    public static final String UPLOAD_PATH = /*System.getProperty("catalina.home") + */"/selluv_upload";
    /**
     * application context파일에 설정추가
     * <mvc:resources location="file:/selluv_upload/" mapping="/uploads/**"/>
     */
    public static final String UPLOAD_URL_PATH = "http://13.125.35.211/api/uploads";

    public static String getUniqueString() {
        return getUniqueString("");
    }

    public static String getUniqueString(String extension) {
        String strReturn = "" + System.currentTimeMillis() + generateRandomNumber(1000, 9999);
        if (extension != null && extension.length() != 0) {
            strReturn += "." + extension;
        }
        return strReturn;
    }

    public static int generateRandomNumber(int min, int max) {
        Double javaVersion = getJavaVersion();
        if (javaVersion < 1.7) {
            return min + (int) (Math.random() * ((max - min) + 1));
        } else {
            return ThreadLocalRandom.current().nextInt(min, max + 1);
        }
    }

    private static double getJavaVersion() {
        String version = System.getProperty("java.version");
        int pos = version.indexOf('.');
        pos = version.indexOf('.', pos + 1);
        return Double.parseDouble(version.substring(0, pos));
    }

    public static String makeDirectory(String path) {
        String[] dirs = path.split("/");

        String mkPath = UPLOAD_PATH;
        File file = new File(mkPath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        createDirectory(mkPath);

        for (String dir : dirs) {
            if (dir == null || dir.length() == 0)
                continue;

            mkPath += File.separator + dir;
            createDirectory(mkPath);
        }

        return mkPath;
    }

    private static void createDirectory(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
            file.setReadable(true, false);
            file.setWritable(true, false);
            file.setExecutable(true, false);
        }
    }

    public static void moveTargetFolder(String tableName, int uid, String fileName) {
        if (fileName == null || fileName.length() == 0)
            return;
        String filePath = makeDirectory(tableName + "/" + uid);
        String tempPath = makeDirectory("temp");
        File tempFile = new File(tempPath + "/" + fileName);
        File targetFile = new File(filePath + "/" + fileName);
        if (tempFile.exists()) {
            tempFile.renameTo(targetFile);
        }
    }

    public static void copyFromTargetFolder(String sourceTableName, int sourceUid, String sourceFileName, String destTableName, int destUid, String destFileName) {
        if (sourceFileName == null || sourceFileName.length() == 0 || destFileName == null || destFileName.length() == 0)
            return;
        String sourceFilePath = makeDirectory(sourceTableName + "/" + sourceUid);
        String destFilePath = makeDirectory(destTableName + "/" + destUid);
        File sourceFile = new File(sourceFilePath + "/" + sourceFileName);
        File destFile = new File(destFilePath + "/" + destFileName);
        if (sourceFile.exists()) {
            try {
                FileUtils.copyFile(sourceFile, destFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getTargetImageUrl(String tableName, int uid, String fileName, String defaultImagePath) //String defaultImagePath = "/assets/images/img_photo_default.png"
    {
        if (fileName == null || fileName.length() == 0)
            return defaultImagePath;
        if (fileName.startsWith("http://") || fileName.startsWith("https://"))
            return fileName;
        String returnStr = UPLOAD_URL_PATH + "/" + tableName + "/" + uid + "/" + fileName;
        File targetFile = new File(makeDirectory(tableName + "/" + uid) + "/" + fileName);
        return (targetFile.exists() && targetFile.isFile()) ? returnStr : defaultImagePath;
    }

    public static String[] getTargetImageUrlWithSize(String tableName, int uid, String fileName) //String defaultImagePath = "/assets/images/img_photo_default.png"
    {
        String[] result = {"", "1", "1"};
        if (fileName == null || fileName.length() == 0)
            return result;

        if (fileName.startsWith("http://") || fileName.startsWith("https://")) {
            result[0] = fileName;
            InputStream istream = null;
            try {
                URL url = new URL(fileName);
                /*BufferedImage image = ImageIO.read(url);
                result[1] = "" + image.getWidth();
                result[2] = "" + image.getHeight();*/

                istream = url.openStream();
                Metadata metaData = ImageMetadataReader.readMetadata(istream, istream.available());
                getImageWidthAndHeightFromMetaData(metaData, result);

            } catch (Exception e) {
                result[1] = "1";
                result[2] = "1";
            } finally {
                if (istream != null) {
                    try {
                        istream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }

        String returnStr = UPLOAD_URL_PATH + "/" + tableName + "/" + uid + "/" + fileName;
        File targetFile = new File(makeDirectory(tableName + "/" + uid) + "/" + fileName);

        if ((targetFile.exists() && targetFile.isFile())) {
            result[0] = returnStr;
            try {
                /*BufferedImage image = ImageIO.read(targetFile);
                result[1] = "" + image.getWidth();
                result[2] = "" + image.getHeight();*/

                Metadata metaData = ImageMetadataReader.readMetadata(targetFile);
                getImageWidthAndHeightFromMetaData(metaData, result);

            } catch (Exception e) {
                result[1] = "1";
                result[2] = "1";
            }
            return result;
        } else {
            return result;
        }
    }

    private static void getImageWidthAndHeightFromMetaData(Metadata metaData, String[] result) throws Exception {
        boolean isWidthGet = false;
        boolean isHeightGet = false;

        //회전정보얻기
        ExifIFD0Directory exifIFD0Directory = metaData.getFirstDirectoryOfType(ExifIFD0Directory.class);
        int orientation = 1;
        try {
            orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
        } catch (Exception ignored) {
        }

                /*
                    orientation 정보

                    1 => 'Horizontal (normal)',
                    2 => 'Mirror horizontal',
                    3 => 'Rotate 180',
                    4 => 'Mirror vertical',
                    5 => 'Mirror horizontal and rotate 270 CW',
                    6 => 'Rotate 90 CW',
                    7 => 'Mirror horizontal and rotate 90 CW',
                    8 => 'Rotate 270 CW',
                 */
        boolean isWidthHeightChanged = (orientation >= 5);

        //너비, 높이 얻기
        firstLoop:
        for (Directory directory : metaData.getDirectories()) {
            for (Tag tag : directory.getTags()) {
                String tagName = tag.getTagName();
                String desc = tag.getDescription();
                if (tagName.equals("Image Width")) {
                    result[1] = "" + Integer.parseInt(desc.replace(" pixels", ""));
                    isWidthGet = true;
                } else if (tagName.equals("Image Height")) {
                    result[2] = "" + Integer.parseInt(desc.replace(" pixels", ""));
                    isHeightGet = true;
                }

                if (isWidthGet && isHeightGet) {
                    break firstLoop;
                }
            }
        }

        if (isWidthHeightChanged) {
            String temp = result[2];
            result[2] = result[1];
            result[1] = temp;
        }
    }

    public static String getTargetImageUrl(String tableName, int uid, String fileName) {
        return getTargetImageUrl(tableName, uid, fileName, "");
    }

    public static boolean deleteTargetImage(String tableName, int uid, String fileName) {
        if (fileName == null || fileName.length() == 0)
            return true;

        File targetFile = new File(makeDirectory(tableName + "/" + uid) + "/" + fileName);
        return (targetFile.exists() && targetFile.isFile()) ? targetFile.delete() : true;
    }


    // 랜덤 알파벳문자열 생성
    public static String generateRandomString(int length) {
        String characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int charactersLength = characters.length();
        StringBuilder randomString = new StringBuilder();
        for (int i = 0; i < length; i++) {
            randomString.append(characters.charAt(generateRandomNumber(0, charactersLength - 1)));
        }
        return randomString.toString();
    }
}
