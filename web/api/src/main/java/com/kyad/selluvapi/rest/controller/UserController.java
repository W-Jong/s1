package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.deal.BuyHistoryDto;
import com.kyad.selluvapi.dto.deal.SellHistoryDto;
import com.kyad.selluvapi.dto.other.ReviewContentDto;
import com.kyad.selluvapi.dto.other.UsrMoneyHisContentDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.usr.*;
import com.kyad.selluvapi.enumtype.DEAL_STATUS_TYPE;
import com.kyad.selluvapi.enumtype.LOGIN_TYPE;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.enumtype.SNS_TYPE;
import com.kyad.selluvapi.helper.CommonHelper;
import com.kyad.selluvapi.helper.DataProcHelper;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.request.CardData;
import com.kyad.selluvapi.helper.iamport.response.CustomerAnnotation;
import com.kyad.selluvapi.helper.iamport.response.IamportResponse;
import com.kyad.selluvapi.request.*;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.EmailSender;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@RestController
@Api(value = "/user", description = "회원관리")
@RequestMapping("/user")
public class UserController {

    private final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private UsrSnsService usrSnsService;

    @Autowired
    private UsrBrandLikeService usrBrandLikeService;

    @Autowired
    private UsrBlockService usrBlockService;

    @Autowired
    private UsrLikeService usrLikeService;

    @Autowired
    private UsrHideService usrHideService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private OtherService otherService;

    @Autowired
    private InviteRewardHisService inviteRewardHisService;

    @Autowired
    private DealService dealService;

    @Autowired
    private MoneyChangeHisService moneyChangeHisService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @Autowired
    private EmailSender emailSender;

    @ApiOperation(value = "회원로그인",
            notes = "로그인후 access_token을 발급합니다.")
    @PostMapping(value = "/login")
    public GenericResponse<UsrLoginDto> login(
            HttpServletRequest request,
            @RequestBody @Valid UserLoginReq loginReq) {

        //임시회원 로그인 막기
        if ("temp".equals(loginReq.getUsrId())) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        UsrDao usr;
        if (loginReq.getLoginType() == LOGIN_TYPE.NORMAL) {
            usr = usrService.getByUsrIdAndPassword(loginReq.getUsrId(), loginReq.getPassword());
        } else {
            usr = usrService.getBySnsIdAndLoginType(loginReq.getPassword(), loginReq.getLoginType().getCode());
        }

        if (usr == null || usr.getStatus() != 1)
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);

        Timestamp timeLastLogin = new Timestamp(System.currentTimeMillis());

        String accessToken = DataProcHelper.getSHA1(timeLastLogin.toString());

        usr.setUsrLoginType(loginReq.getLoginType().getCode());
        usr.setLoginTime(timeLastLogin);
        usr.setAccessToken(accessToken);
        usrService.save(usr);

        UsrLoginDto usrLoginDto = new UsrLoginDto();
        usrLoginDto.setAccessToken(accessToken);
        usrLoginDto.setUsr(usrService.convertUsrDaoToUsrDetailDto(usr));
        usrLoginDto.setAddressList(usrService.getAddressListByUsrUid(usr.getUsrUid()));

        UsrLoginHisDao usrLoginHis = new UsrLoginHisDao();
        usrLoginHis.setUsrUid(usr.getUsrUid());
        usrLoginHis.setLoginTp(loginReq.getLoginType().getCode());
        usrLoginHis.setOsTp(loginReq.getOsTp().getCode());
        usrLoginHis.setConnAddr(CommonHelper.getRemoteAddr(request));
        usrService.saveUsrLoginHis(usrLoginHis);

        return new GenericResponse<>(ResponseMeta.OK, usrLoginDto);
    }

    @ApiOperation(value = "회원가입",
            notes = "회원가입")
    @PutMapping(value = "")
    @Transactional
    public GenericResponse<UsrLoginDto> signupUser(
            @RequestBody @Valid UserSignupReq signupReq) {

        if (signupReq.getUsrId().contains(" ") || signupReq.getUsrNckNm().contains(" ")) {
            return new GenericResponse<>(ResponseMeta.USER_ID_NICKNAME_SPACE_NOT_ALLOWED, null);
        }

        if (usrService.isSpecialCharContains(signupReq.getUsrId()) ||
                usrService.isSpecialCharContains(signupReq.getUsrNckNm())) {
            return new GenericResponse<>(ResponseMeta.USER_ID_NICKNAME_SPECIAL_CHAR_NOT_ALLOWED, null);
        }

        if (usrService.isUsrIdDuplicated(signupReq.getUsrId()))
            return new GenericResponse<>(ResponseMeta.USER_ID_DUPLICATED, null);

        if (signupReq.getLoginType() == LOGIN_TYPE.NORMAL &&
                usrService.isEmailDuplicated(signupReq.getUsrMail(), 0))
            return new GenericResponse<>(ResponseMeta.USER_EMAIL_DUPLICATED, null);

        if (signupReq.getLoginType() != LOGIN_TYPE.NORMAL &&
                usrSnsService.isSnsIdAndSnsTypeDuplicated(signupReq.getSnsId(), signupReq.getLoginType().getCode()))
            return new GenericResponse<>(ResponseMeta.USER_SNSID_DUPLICATED, null);

        if (usrService.isPersonDuplicated(signupReq.getUsrNm(), signupReq.getGender(), signupReq.getBirthday(), -1))
            return new GenericResponse<>(ResponseMeta.USER_PERSON_DUPLICATED, null);

        if (signupReq.getBirthday().isAfter(LocalDate.of(LocalDate.now().getYear() - 17, 1, 1)))
            return new GenericResponse<>(ResponseMeta.USER_AGE_NOT_ALLOWED, null);

        UsrDao inviteUsr = null;
        if (signupReq.getInviteCode() != null && !signupReq.getInviteCode().isEmpty()) {
            try {
                inviteUsr = usrService.getByUsrId(new String(Base64.getDecoder().decode(signupReq.getInviteCode().getBytes("UTF-8")), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (inviteUsr == null || inviteUsr.getUsrUid() == -1)
                return new GenericResponse<>(ResponseMeta.USER_INVITE_CODE_ERROR, null);
        }

        UsrDao usr = new UsrDao();
        usr.setUsrMail(signupReq.getUsrMail());
        usr.setUsrPwd(signupReq.getPassword());
        usr.setUsrPhone(signupReq.getUsrPhone());
        usr.setUsrId(signupReq.getUsrId());
        usr.setUsrNm(signupReq.getUsrNm());
        usr.setUsrNckNm(signupReq.getUsrNckNm());
        usr.setProfileImg(signupReq.getSnsProfileImg());
        usr.setBirthday(signupReq.getBirthday());
        usr.setGender(signupReq.getGender());
        usr.setAccountNm(signupReq.getUsrNm());
        usr.setInviteUsrUid((inviteUsr == null) ? 0 : inviteUsr.getUsrUid());
        usr.setUsrLoginType(signupReq.getLoginType().getCode());
        usr.setSnsId(signupReq.getSnsId());
        usr.setLikeGroup(usr.getGender());

        Timestamp timeLastLogin = new Timestamp(System.currentTimeMillis());
        String accessToken = DataProcHelper.getSHA1(timeLastLogin.toString());
        usr.setAccessToken(accessToken);

        usrService.save(usr);

        UsrPushSettingDao pushSetting = new UsrPushSettingDao();
        pushSetting.setUsrUid(usr.getUsrUid());
        usrService.savePushSetting(pushSetting);

        if (signupReq.getLoginType() != LOGIN_TYPE.NORMAL) {
            UsrSnsDao usrSns = new UsrSnsDao();
            usrSns.setUsrUid(usr.getUsrUid());
            usrSns.setSnsType(signupReq.getLoginType().getCode());
            usrSns.setSnsId(signupReq.getSnsId());
            usrSns.setSnsEmail(signupReq.getUsrMail());
            usrSns.setSnsNckNm(signupReq.getUsrNckNm());
            usrSns.setSnsProfileImg(signupReq.getSnsProfileImg());
            usrSns.setSnsToken(signupReq.getPassword());
            usrSnsService.save(usrSns);
        }

        if (signupReq.getLikeBrands() != null && signupReq.getLikeBrands().size() > 0) {
            List<UsrBrandLikeDao> brandLikeList = new ArrayList<>();
            for (int brandUid : signupReq.getLikeBrands()) {
                UsrBrandLikeDao brandLike = new UsrBrandLikeDao();
                brandLike.setUsrUid(usr.getUsrUid());
                brandLike.setBrandUid(brandUid);
                brandLikeList.add(brandLike);
            }
            usrBrandLikeService.save(brandLikeList);
        }

        if (inviteUsr != null) { // 추천인 코드 적립
            inviteRewardHisService.addJoinedReward(usr, inviteUsr);
        }

        UsrLoginDto usrLoginDto = new UsrLoginDto();
        usrLoginDto.setAccessToken(accessToken);
        usrLoginDto.setUsr(usrService.convertUsrDaoToUsrDetailDto(usr));
        usrLoginDto.setAddressList(usrService.getAddressListByUsrUid(usr.getUsrUid()));

        return new GenericResponse<>(ResponseMeta.OK, usrLoginDto);
    }

    @ApiOperation(value = "아이디 중복검사",
            notes = "아이디가 중복되었는가를 검사한다.")
    @GetMapping(value = "/check/usrId")
    public GenericResponse<Void> checkUsrId(
            @RequestParam String usrId) {
        if (usrId == null || usrId.contains(" ")) {
            return new GenericResponse<>(ResponseMeta.USER_ID_NICKNAME_SPACE_NOT_ALLOWED, null);
        }

        if (usrService.isSpecialCharContains(usrId)) {
            return new GenericResponse<>(ResponseMeta.USER_ID_NICKNAME_SPECIAL_CHAR_NOT_ALLOWED, null);
        }

        if (usrService.isUsrIdDuplicated(usrId)) {
            return new GenericResponse<>(ResponseMeta.USER_ID_DUPLICATED, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "이메일 중복검사",
            notes = "해당 이메일로 이미 연결되어 있는가를 검사한다. 이메일 회원가입시만 사용")
    @GetMapping(value = "/check/usrMail")
    public GenericResponse<Void> checkUsrMail(
            @RequestParam String usrMail) {

        if (usrService.isEmailDuplicated(usrMail, 0)) {
            return new GenericResponse<>(ResponseMeta.USER_EMAIL_DUPLICATED, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "SNS 아이디 중복검사",
            notes = "해당 SNS 아이디로 이미 연결되어 있는가를 검사한다.")
    @GetMapping(value = "/check/snsId")
    public GenericResponse<Void> checkSnsId(
            @RequestParam String snsId,
            @RequestParam SNS_TYPE snsType) {

        if (usrSnsService.isSnsIdAndSnsTypeDuplicated(snsId, snsType.getCode())) {
            return new GenericResponse<>(ResponseMeta.USER_SNSID_DUPLICATED, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "추천인 코드 유효성검사",
            notes = "추천인 코드가 유효한가를 검사한다.")
    @GetMapping(value = "/check/inviteCode")
    public GenericResponse<Void> checkInviteCode(@RequestParam String inviteCode) {
        if (inviteCode == null || inviteCode.isEmpty()) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }
        UsrDao inviteUsr = null;
        try {
            inviteUsr = usrService.getByUsrId(new String(Base64.getDecoder().decode(inviteCode.getBytes("UTF-8")), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (inviteUsr == null || inviteUsr.getUsrUid() == -1)
            return new GenericResponse<>(ResponseMeta.USER_INVITE_CODE_ERROR, null);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "회원탈퇴",
            notes = "해당 회원의 데이터를 완전히 삭제한다.")
    @DeleteMapping(value = "")
    public GenericResponse<Void> deleteUser(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        usrService.removeUsrAndReference(usr.getUsrUid());
        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "관심상품군 업데이트",
            notes = "관심상품군을 업데이트 한다.")
    @PostMapping(value = "/likeGroup")
    public GenericResponse<Void> updateLikeGroup(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserLikeGroupUpdateReq likeGroupUpdateReq) {

        if (!likeGroupUpdateReq.getIsMale() && !likeGroupUpdateReq.getIsFemale() && !likeGroupUpdateReq.getIsChildren())
            return new GenericResponse<>(ResponseMeta.ERROR_LIKE_GROUP_REQUIRED, null);

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        int likeGroupCode = 0;
        likeGroupCode += likeGroupUpdateReq.getIsMale() ? 1 : 0;
        likeGroupCode += likeGroupUpdateReq.getIsFemale() ? 2 : 0;
        likeGroupCode += likeGroupUpdateReq.getIsChildren() ? 4 : 0;
        usr.setLikeGroup(likeGroupCode);
        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "푸시토큰 업데이트",
            notes = "푸시발송을 위한 회원의 디바이스토큰 정보를 업데이트 한다.")
    @PostMapping(value = "/deviceToken")
    public GenericResponse<Void> updateDeviceToken(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestParam(value = "deviceToken", defaultValue = "") String deviceToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        if (deviceToken != null && !deviceToken.isEmpty()) {
            usr.setDeviceToken(deviceToken);
        }

        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "비밀번호 변경",
            notes = "회원의 비밀번호를 변경한다.")
    @PostMapping(value = "/password")
    public GenericResponse<Void> updatePassword(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserPasswordUpdateReq passwordUpdateReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        if (usr.getUsrLoginType() != LOGIN_TYPE.NORMAL.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (!usr.getUsrPwd().equals(passwordUpdateReq.getOldPassword())) {
            return new GenericResponse<>(ResponseMeta.USER_ERROR_OLD_PASSWORD, null);
        }

        usr.setUsrPwd(passwordUpdateReq.getNewPassword());
        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "비밀번호 찾기",
            notes = "이메일로 비밀번호 찾기를 위한 URL을 돌려준다.")
    @PostMapping(value = "/password/find")
    public GenericResponse<Void> findPassword(
            @ApiParam("이메일주소") @RequestBody @Valid ContentReq contentReq) {

        if (contentReq == null || "".equals(contentReq.getContent())) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        UsrDao usr = usrService.getByUsrMail(contentReq.getContent());
        if (usr == null)
            return new GenericResponse<>(ResponseMeta.USER_EMAIL_NOT_CONNECTED, null);

        if (usr.getUsrLoginType() == LOGIN_TYPE.FACEBOOK.getCode())
            return new GenericResponse<>(ResponseMeta.USER_SIGNUP_WITH_FACEBOOK, null);
        if (usr.getUsrLoginType() == LOGIN_TYPE.NAVER.getCode())
            return new GenericResponse<>(ResponseMeta.USER_SIGNUP_WITH_NAVER, null);
        if (usr.getUsrLoginType() == LOGIN_TYPE.KAKAOTALK.getCode())
            return new GenericResponse<>(ResponseMeta.USER_SIGNUP_WITH_KAKAOTALK, null);

        String accessToken = DataProcHelper.getSHA1(new Timestamp(System.currentTimeMillis()).toString());
        usr.setAccessToken(accessToken);
        usrService.save(usr);

        String findPwdUrl = CommonConstant.SELLUV_HOST + "web/find/password/" + accessToken;

        if (!emailSender.sendTemporaryPasswordResetUrl(usr.getUsrMail(), findPwdUrl)) {
            return new GenericResponse<>(ResponseMeta.USER_EMAIL_SENDING_ERROR, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "차단한 유저 목록얻기",
            notes = "차단한 유저 목록을 얻는다.")
    @GetMapping(value = "/block")
    public GenericResponse<Page<UsrMiniDto>> getBlockList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, usrService.getBlockList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));
    }

    @ApiOperation(value = "유저 차단하기",
            notes = "해당 유저를 차단한다.")
    @PutMapping(value = "/{usrUid}/block")
    public GenericResponse<Void> insertBlock(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        if (peerUsrUid < 1 || usr.getUsrUid() == peerUsrUid) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        UsrBlockDao usrBlock = usrBlockService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsrUid);
        if (usrBlock != null)
            return new GenericResponse<>(ResponseMeta.ALREADY_ADDED, null);

        usrBlock = new UsrBlockDao();
        usrBlock.setUsrUid(usr.getUsrUid());
        usrBlock.setPeerUsrUid(peerUsrUid);
        usrBlockService.save(usrBlock);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "유저 차단 해제하기",
            notes = "해당 유저에 대한 차단을 해제한다.")
    @DeleteMapping(value = "/{usrUid}/block")
    public GenericResponse<Void> deleteBlock(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        UsrBlockDao usrBlock = usrBlockService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsrUid);
        if (usrBlock == null)
            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);

        usrBlockService.delete(usrBlock.getUsrBlockUid());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "팔로잉 유저 목록얻기",
            notes = "해당 유저를 팔로우한 유저 목록을 얻는다.")
    @GetMapping(value = "/{usrUid}/following")
    public GenericResponse<Page<UsrFollowListDto>> getFollowingList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, usrService.getFollowingList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), peerUsrUid));
    }

    @ApiOperation(value = "팔로워 유저 목록얻기",
            notes = "해당 유저가 팔로우한 유저 목록을 얻는다.")
    @GetMapping(value = "/{usrUid}/follower")
    public GenericResponse<Page<UsrFollowListDto>> getFollowerList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, usrService.getFollowerList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), peerUsrUid));
    }

    @ApiOperation(value = "유저 팔로우하기",
            notes = "해당 유저를 팔로우한다.")
    @PutMapping(value = "/{usrUid}/follow")
    public GenericResponse<Void> insertFollow(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        if (peerUsrUid < 1 || usr.getUsrUid() == peerUsrUid) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        UsrLikeDao usrLike = usrLikeService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsrUid);
        if (usrLike != null)
            return new GenericResponse<>(ResponseMeta.ALREADY_ADDED, null);

        usrLike = new UsrLikeDao();
        usrLike.setUsrUid(usr.getUsrUid());
        usrLike.setPeerUsrUid(peerUsrUid);
        usrLikeService.save(usrLike);

        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH14_FOLLOW_ME, peerUsr, usr, null, null);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "유저 팔로우 해제하기",
            notes = "해당 유저에 대한 팔로우를 해제한다.")
    @DeleteMapping(value = "/{usrUid}/follow")
    public GenericResponse<Void> deleteFollow(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrLikeDao usrLike = usrLikeService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsrUid);
        if (usrLike == null)
            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);

        usrLikeService.delete(usrLike.getUsrLikeUid());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "회원상세정보얻기",
            notes = "유저 상세정보를 얻는다.")
    @GetMapping(value = "/{usrUid}")
    public GenericResponse<UsrInfoDto> getUserDetailInfo(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        UsrInfoDto usrInfo = new UsrInfoDto();
        usrInfo.setUsrUid(peerUsr.getUsrUid());
        usrInfo.setUsrId(peerUsr.getUsrId());
        usrInfo.setUsrNckNm(peerUsr.getUsrNckNm());
        usrInfo.setProfileImg(StarmanHelper.getTargetImageUrl("usr", peerUsr.getUsrUid(), peerUsr.getProfileImg()));
        usrInfo.setProfileBackImg(StarmanHelper.getTargetImageUrl("usr", peerUsr.getUsrUid(), peerUsr.getProfileBackImg()));
        usrInfo.setDescription(peerUsr.getDescription());
        usrInfo.setPoint(peerUsr.getPoint());
        usrInfo.setUsrFollowerCount(usrLikeService.countFollowerUserByUsrUid(peerUsr.getUsrUid()));
        usrInfo.setUsrFollowingCount(usrLikeService.countFollowingUserByUsrUid(peerUsr.getUsrUid()));
        usrInfo.setUsrLikeStatus(usrLikeService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsr.getUsrUid()) != null);
        usrInfo.setUsrBlockStatus(usrBlockService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsr.getUsrUid()) != null);

        return new GenericResponse<>(ResponseMeta.OK, usrInfo);
    }

    @ApiOperation(value = "회원의 아이템 목록 얻기",
            notes = "해당 회원의 아이템목록을 얻는다")
    @GetMapping(value = "/{usrUid}/pdt")
    public GenericResponse<Page<PdtListDto>> getPdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, usrService.getPdtListByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), peerUsrUid));
    }

    @ApiOperation(value = "회원의 잇템 목록 얻기",
            notes = "해당 회원의 잇템목록을 얻는다")
    @GetMapping(value = "/{usrUid}/wish")
    public GenericResponse<Page<PdtListDto>> getPdtWishList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, usrService.getPdtWishListByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), peerUsrUid));
    }

    @ApiOperation(value = "회원의 스타일 목록 얻기",
            notes = "해당 회원의 스타일목록을 얻는다")
    @GetMapping(value = "/{usrUid}/style")
    public GenericResponse<Page<PdtListDto>> getPdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        return new GenericResponse<>(ResponseMeta.OK, usrService.getPdtStyleListByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), peerUsrUid));
    }

    @ApiOperation(value = "매너포인트 변경이력(거래후기) 얻기",
            notes = "해당 회원의 매너포인트 변경이력(거래후기) 얻기")
    @GetMapping(value = "/{usrUid}/review")
    public GenericResponse<ReviewContentDto> getPointHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid,
            @ApiParam(value = "-1 : 전체, 2: 만족, 1:보통, 0:불만족", allowableValues = "-1, 0, 1, 2") @RequestParam(value = "point", defaultValue = "-1") int point,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        ReviewContentDto contentDto = new ReviewContentDto();
        contentDto.setReviews(reviewService.findAllByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), peerUsrUid, point));
        contentDto.setCountTwoPoint(reviewService.countByUsrUidAndPoint(peerUsrUid, 2));
        contentDto.setCountOnePoint(reviewService.countByUsrUidAndPoint(peerUsrUid, 1));
        contentDto.setCountZeroPoint(reviewService.countByUsrUidAndPoint(peerUsrUid, 0));

        return new GenericResponse<>(ResponseMeta.OK, contentDto);
    }

    @ApiOperation(value = "회원상세정보얻기",
            notes = "회원상세정보를 얻는다.")
    @GetMapping(value = "/myInfo")
    public GenericResponse<UsrMyInfoDto> getMyInfo(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrMyInfoDto usrInfo = new UsrMyInfoDto();

        usrInfo.setUsr(usrService.convertUsrDaoToUsrDetailDto(usr));
        usrInfo.setAddressList(usrService.getAddressListByUsrUid(usr.getUsrUid()));
        usrInfo.setUsrFollowerCount(usrLikeService.countFollowerUserByUsrUid(usr.getUsrUid()));
        usrInfo.setUsrFollowingCount(usrLikeService.countFollowingUserByUsrUid(usr.getUsrUid()));
        usrInfo.setCountPdt(usrService.countPdtByUsrUid(usr.getUsrUid()));
        usrInfo.setCountBuy(dealService.countPaymentHistoryByUsrUid(usr.getUsrUid()));
        usrInfo.setCountSell(dealService.countPaymentHistoryBySellerUsrUid(usr.getUsrUid()));
        usrInfo.setCountUnreadNotice(otherService.countUnReadNotice((usr.getNoticeTime() == null) ? new Timestamp(0) : usr.getNoticeTime()));
        List<EventDao> eventDaoList = otherService.getMyPageEventActiveList();
        for (EventDao event : eventDaoList) {
            event.setProfileImg(StarmanHelper.getTargetImageUrl("event", event.getEventUid(), event.getProfileImg()));
            event.setDetailImg(StarmanHelper.getTargetImageUrl("event", event.getEventUid(), event.getDetailImg()));
        }
        usrInfo.setMyPageEventList(eventDaoList);

        return new GenericResponse<>(ResponseMeta.OK, usrInfo);
    }

    @ApiOperation(value = "프로필이미지 업데이트",
            notes = "회원의 프로필이미지를 변경한다.")
    @PostMapping(value = "/profileImg")
    public GenericResponse<Void> updateProfileImage(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid ContentReq contentReq) {

        if (contentReq == null || contentReq.getContent() == null || "".equals(contentReq.getContent())) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        usr.setProfileImg(contentReq.getContent());
        StarmanHelper.moveTargetFolder("usr", usr.getUsrUid(), contentReq.getContent());
        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "프로필 업데이트",
            notes = "회원의 프로필을 변경한다.")
    @PostMapping(value = "/profile")
    public GenericResponse<Void> updateProfile(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserProfileReq userProfileReq) {

        if (userProfileReq.getUsrNckNm().contains(" ")) {
            return new GenericResponse<>(ResponseMeta.USER_ID_NICKNAME_SPACE_NOT_ALLOWED, null);
        }

        if (usrService.isSpecialCharContains(userProfileReq.getUsrNckNm())) {
            return new GenericResponse<>(ResponseMeta.USER_ID_NICKNAME_SPECIAL_CHAR_NOT_ALLOWED, null);
        }

        if (!userProfileReq.getIsMale() && !userProfileReq.getIsFemale() && !userProfileReq.getIsChildren())
            return new GenericResponse<>(ResponseMeta.ERROR_LIKE_GROUP_REQUIRED, null);

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        usr.setUsrNckNm(userProfileReq.getUsrNckNm());
        usr.setDescription(userProfileReq.getDescription());
        int likeGroupCode = 0;
        likeGroupCode += userProfileReq.getIsMale() ? 1 : 0;
        likeGroupCode += userProfileReq.getIsFemale() ? 2 : 0;
        likeGroupCode += userProfileReq.getIsChildren() ? 4 : 0;
        usr.setLikeGroup(likeGroupCode);
        if (!"".equals(userProfileReq.getProfileImg())) {
            StarmanHelper.moveTargetFolder("usr", usr.getUsrUid(), userProfileReq.getProfileImg());
            usr.setProfileImg(userProfileReq.getProfileImg());
        }
        if (!"".equals(userProfileReq.getProfileBackImg())) {
            StarmanHelper.moveTargetFolder("usr", usr.getUsrUid(), userProfileReq.getProfileBackImg());
            usr.setProfileBackImg(userProfileReq.getProfileBackImg());
        }

        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "계좌정보 업데이트",
            notes = "회원의 계좌정보를 변경한다.")
    @PostMapping(value = "/accountInfo")
    public GenericResponse<Void> updateAccountInfo(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserAccountReq userAccountReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        usr.setBankNm(userAccountReq.getBankNm());
        usr.setAccountNum(userAccountReq.getAccountNum());

        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "판매자설정 업데이트",
            notes = "회원의 판매자설정정보를 변경한다.")
    @PostMapping(value = "/sellerSetting")
    public GenericResponse<Void> updateSellerSetting(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserSellerSettingReq userSellerSettingReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        if (userSellerSettingReq.getIsFreeSend()) {
            usr.setFreeSendPrice(userSellerSettingReq.getFreeSendPrice());
        } else {
            usr.setFreeSendPrice(-1);
        }

        if (userSellerSettingReq.getIsSleep()) {
            usr.setSleepYn(1);
            usr.setSleepTime(new Timestamp(System.currentTimeMillis() + userSellerSettingReq.getTimeOutDate() * 24 * 3600 * 1000));
            usrService.updatePdtStatusByUsrUid(usr.getUsrUid(), true);
        } else {
            usr.setSleepYn(0);
            usr.setSleepTime(new Timestamp(System.currentTimeMillis()));
            usrService.updatePdtStatusByUsrUid(usr.getUsrUid(), false);
        }

        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "알림설정 얻기",
            notes = "회원의 알림설정정보를 얻는다.")
    @GetMapping(value = "/alarmSetting")
    public GenericResponse<UsrPushSettingDao> getAlarmSetting(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrPushSettingDao usrPushSetting = usrService.getPushSetting(usr.getUsrUid());
        if (usrPushSetting == null) {
            usrPushSetting = new UsrPushSettingDao();
            usrPushSetting.setUsrUid(usr.getUsrUid());
            usrService.savePushSetting(usrPushSetting);
        }

        return new GenericResponse<>(ResponseMeta.OK, usrPushSetting);
    }

    @ApiOperation(value = "알림설정 업데이트",
            notes = "회원의 알림설정정보를 업데이트한다.")
    @PostMapping(value = "/alarmSetting")
    public GenericResponse<Void> updateAlarmSetting(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserPushSettingReq userPushSettingReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrPushSettingDao usrPushSetting = usrService.getPushSetting(usr.getUsrUid());
        if (usrPushSetting == null) {
            usrPushSetting = new UsrPushSettingDao();
            usrPushSetting.setUsrUid(usr.getUsrUid());
        }

        usrPushSetting.setRegTime(new Timestamp(System.currentTimeMillis()));
        usrPushSetting.setDealYn(userPushSettingReq.getDealYn());
        usrPushSetting.setNewsYn(userPushSettingReq.getNewsYn());
        usrPushSetting.setReplyYn(userPushSettingReq.getReplyYn());
        usrPushSetting.setDealSetting(userPushSettingReq.getDealSetting());
        usrPushSetting.setNewsSetting(userPushSettingReq.getNewsSetting());
        usrPushSetting.setReplySetting(userPushSettingReq.getReplySetting());

        usrService.savePushSetting(usrPushSetting);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "SNS연동정보 얻기",
            notes = "회원의 SNS연동정보 상태를 얻는다.")
    @GetMapping(value = "/snsStatus")
    public GenericResponse<UsrSnsStatusDto> getSnsStatus(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrSnsStatusDto dto = new UsrSnsStatusDto();

        List<UsrSnsDao> snsDaos = usrSnsService.getListByUsrUid(usr.getUsrUid());

        for (UsrSnsDao snsDao : snsDaos) {
            if (snsDao.getSnsType() == SNS_TYPE.FACEBOOK.getCode()) {
                dto.setFacekbook(true);
            } else if (snsDao.getSnsType() == SNS_TYPE.NAVER.getCode()) {
                dto.setNaver(true);
            } else if (snsDao.getSnsType() == SNS_TYPE.KAKAOTALK.getCode()) {
                dto.setKakaoTalk(true);
            } else if (snsDao.getSnsType() == SNS_TYPE.INSTAGRAM.getCode()) {
                dto.setInstagram(true);
            } else if (snsDao.getSnsType() == SNS_TYPE.TWITTER.getCode()) {
                dto.setTwitter(true);
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, dto);
    }

    @ApiOperation(value = "SNS연동하기",
            notes = "SNS연동정보를 설정한다.")
    @PutMapping(value = "/sns")
    public GenericResponse<Void> putSns(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserSnsReq snsReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrDao targetUsr = usrService.getBySnsIdAndLoginType(snsReq.getSnsId(), snsReq.getSnsType().getCode());
        if (targetUsr != null) {
            return new GenericResponse<>(ResponseMeta.USER_SNSID_DUPLICATED, null);
        }

        UsrSnsDao usrSns = usrSnsService.getByUsrUidAndSnsType(usr.getUsrUid(), snsReq.getSnsType().getCode());
        if (usrSns == null) {
            usrSns = new UsrSnsDao();
            usrSns.setUsrUid(usr.getUsrUid());
            usrSns.setSnsType(snsReq.getSnsType().getCode());
        }
        usrSns.setSnsId(snsReq.getSnsId());
        usrSns.setSnsEmail(snsReq.getSnsEmail());
        usrSns.setSnsNckNm(snsReq.getSnsNckNm());
        usrSns.setSnsProfileImg(snsReq.getSnsProfileImg());
        usrSns.setSnsToken(snsReq.getSnsToken());

        usrSnsService.save(usrSns);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "SNS연동삭제",
            notes = "SNS연동정보를 삭제한다.")
    @DeleteMapping(value = "/sns")
    public GenericResponse<Void> deleteSns(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestParam(value = "snsType") SNS_TYPE snsType) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        if (usr.getUsrLoginType() == snsType.getCode()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        UsrSnsDao usrSns = usrSnsService.getByUsrUidAndSnsType(usr.getUsrUid(), snsType.getCode());
        if (usrSns == null) {
            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);
        }

        usrSnsService.delete(usrSns.getUsrSnsUid());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "회원기본정보 수정",
            notes = "회원의 기본정보(전화번호, 실명, 성별, 생년월일)를 수정한다.")
    @PostMapping(value = "/basis")
    public GenericResponse<Void> postBasisInfo(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserBasisInfoReq basisInfoReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);


        if (usrService.isPersonDuplicated(basisInfoReq.getUsrNm(), basisInfoReq.getGender(), basisInfoReq.getBirthday(), usr.getUsrUid()))
            return new GenericResponse<>(ResponseMeta.USER_PERSON_DUPLICATED, null);

        if (basisInfoReq.getBirthday().isAfter(LocalDate.of(LocalDate.now().getYear() - 17, 1, 1)))
            return new GenericResponse<>(ResponseMeta.USER_AGE_NOT_ALLOWED, null);

        usr.setUsrPhone(basisInfoReq.getUsrPhone());
        usr.setUsrNm(basisInfoReq.getUsrNm());
        usr.setBirthday(basisInfoReq.getBirthday());
        usr.setGender(basisInfoReq.getGender());
        usr.setAccountNm(basisInfoReq.getUsrNm());

        usrService.save(usr);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "회원기타정보 수정",
            notes = "회원의 기타정보(이메일, 주소1,2,3)를 수정한다.")
    @PostMapping(value = "/other")
    public GenericResponse<Void> postOtherInfo(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserOtherInfoReq otherInfoReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        if (usrService.isEmailDuplicated(otherInfoReq.getUsrMail(), usr.getUsrUid())) {
            return new GenericResponse<>(ResponseMeta.USER_EMAIL_DUPLICATED, null);
        }

        usr.setUsrMail(otherInfoReq.getUsrMail());
        usrService.save(usr);

        usrService.saveUsrAddress(usr.getUsrUid(), 1, otherInfoReq.getAddressName1(),
                otherInfoReq.getAddressPhone1(), otherInfoReq.getAddressDetail1(), otherInfoReq.getAddressDetailSub1(), otherInfoReq.getAddressSelectionIndex());
        usrService.saveUsrAddress(usr.getUsrUid(), 2, otherInfoReq.getAddressName2(),
                otherInfoReq.getAddressPhone2(), otherInfoReq.getAddressDetail2(), otherInfoReq.getAddressDetailSub2(), otherInfoReq.getAddressSelectionIndex());
        usrService.saveUsrAddress(usr.getUsrUid(), 3, otherInfoReq.getAddressName3(),
                otherInfoReq.getAddressPhone3(), otherInfoReq.getAddressDetail3(), otherInfoReq.getAddressDetailSub3(), otherInfoReq.getAddressSelectionIndex());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "카드 목록얻기",
            notes = "등록한 카드 목록을 얻는다.")
    @GetMapping(value = "/card")
    public GenericResponse<List<UsrCardDao>> getUsrCardList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, usrService.findAllCardListByUsrUid(usr.getUsrUid()));
    }

    @ApiOperation(value = "카드 등록",
            notes = "카드를 등록한다.")
    @PutMapping(value = "/card")
    public GenericResponse<UsrCardDao> putUsrCard(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid UserCardReq cardReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrCardDao usrCard = new UsrCardDao();
        usrCard.setUsrUid(usr.getUsrUid());
        usrCard.setKind(cardReq.getKind());
        usrCard.setCardNumber(cardReq.getCardNumber());
        usrCard.setValidDate(cardReq.getValidDate());
        usrCard.setValidNum(cardReq.getValidNum());
        usrCard.setPassword(cardReq.getPassword());
        usrCard.setIamportValidYn(0);
        usrService.saveUsrCard(usrCard);

        //아임포트 카드유효성 검사
        CardData cardData = new CardData();
        cardData.setCard_number(usrCard.getCardNumber());
        cardData.setExpiry(usrCard.getValidDate().toString().substring(0, 7));
        cardData.setBirth(usrCard.getValidNum());
        cardData.setPwd_2digit(usrCard.getPassword());
        cardData.setCustomer_name(usr.getAccountNm());
        cardData.setCustomer_tel(usr.getUsrPhone());
        cardData.setCustomer_email(usr.getUsrMail());
        cardData.setCustomer_addr("");
        cardData.setCustomer_postcode("");

        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        CustomerAnnotation customerAnnotation;
        IamportResponse<CustomerAnnotation> imaportResponse = null;
        try {
            imaportResponse = iamportClientHelper.addCardInfo("" + usrCard.getUsrCardUid(), cardData);
            customerAnnotation = imaportResponse.getResponse();
        } catch (Exception e) {
            customerAnnotation = null;
            e.printStackTrace();
        }
        if (customerAnnotation != null) {
            usrCard.setCardName(customerAnnotation.getCard_name());
            usrCard.setIamportValidYn(1);
            usrService.saveUsrCard(usrCard);
        } else {
            usrService.deleteUsrCard(usrCard.getUsrCardUid());
            if (imaportResponse != null && !imaportResponse.getMessage().isEmpty()) {
                return new GenericResponse<>(new ResponseMeta(ResponseMeta.CARD_NOT_VALID.getErrCode(), imaportResponse.getMessage(), null), null);
            } else {
                return new GenericResponse<>(ResponseMeta.CARD_NOT_VALID, null);
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, usrCard);
    }

    @ApiOperation(value = "카드를 선택한다.",
            notes = "선택된 카드를 제일 첫 카드로 만든다.")
    @PutMapping(value = "/card/{usrCardUid}")
    public GenericResponse<UsrCardDao> putUsrCardSelection(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrCardUid") int usrCardUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrCardDao usrCard = usrService.findOneUsrCard(usrCardUid);
        if (usrCard == null || usrCard.getStatus() == 0) {
            return new GenericResponse<>(ResponseMeta.CARD_NOT_EXIST, null);
        }

        if (usrCard.getUsrUid() != usr.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        usrCard.setRegTime(new Timestamp(System.currentTimeMillis()));
        usrService.saveUsrCard(usrCard);

        return new GenericResponse<>(ResponseMeta.OK, usrCard);
    }

    @ApiOperation(value = "카드 삭제",
            notes = "카드를 삭제한다.")
    @DeleteMapping(value = "/card/{usrCardUid}")
    public GenericResponse<Void> deleteUsrCard(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrCardUid") int usrCardUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrCardDao usrCard = usrService.findOneUsrCard(usrCardUid);
        if (usrCard == null || usrCard.getStatus() == 0) {
            return new GenericResponse<>(ResponseMeta.CARD_NOT_EXIST, null);
        }

        if (usrCard.getUsrUid() != usr.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        usrCard.setStatus(0);
        usrService.saveUsrCard(usrCard);

        //아임포트 카드정보 삭제
        IamportClientHelper iamportClientHelper = new IamportClientHelper();
        CustomerAnnotation customerAnnotation;
        try {
            customerAnnotation = iamportClientHelper.deleteCardInfo("" + usrCard.getUsrCardUid()).getResponse();
        } catch (Exception e) {
            customerAnnotation = null;
            e.printStackTrace();
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "페이스북 친구찾기",
            notes = "페이스북 id로 부터 가입한 친구목록을 얻는다.")
    @PostMapping(value = "/friend/facebook")
    public GenericResponse<List<UsrFollowListDto>> getFacebookFriendList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "페이스북id 배열") @RequestBody @Valid MultiStringReq multiStringReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        List<UsrFollowListDto> usrFollowList = new ArrayList<>();
        for (String facebookId : multiStringReq.getList()) {
            UsrDao friendDao = usrService.getBySnsIdAndLoginType(facebookId, SNS_TYPE.FACEBOOK.getCode());
            if (friendDao != null && friendDao.getUsrUid() != usr.getUsrUid()) {
                UsrFollowListDto usrFollow = new UsrFollowListDto();
                usrFollow.setUsrUid(friendDao.getUsrUid());
                usrFollow.setUsrId(friendDao.getUsrId());
                usrFollow.setUsrNckNm(friendDao.getUsrNckNm());
                usrFollow.setProfileImg(StarmanHelper.getTargetImageUrl("usr", friendDao.getUsrUid(), friendDao.getProfileImg()));
                usrFollow.setUsrLikeStatus(usrLikeService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), friendDao.getUsrUid()) != null);

                usrFollowList.add(usrFollow);
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, usrFollowList);
    }

    @ApiOperation(value = "연락처 친구찾기",
            notes = "연락처 전화번호로 부터 가입한 친구목록을 얻는다.")
    @PostMapping(value = "/friend/addressBook")
    public GenericResponse<UsrAddressBookDto> getAddressBookFriendList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "전화번호 배열") @RequestBody @Valid MultiStringReq multiStringReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        List<UsrFollowListDto> usrFollowList = new ArrayList<>();
        List<String> unJoinedPhoneList = new ArrayList<>();

        for (String usrPhone : multiStringReq.getList()) {
            UsrDao friendDao = usrService.getByUsrPhone(usrPhone);
            if (friendDao != null && friendDao.getUsrUid() != usr.getUsrUid()) {
                UsrFollowListDto usrFollow = new UsrFollowListDto();
                usrFollow.setUsrUid(friendDao.getUsrUid());
                usrFollow.setUsrId(friendDao.getUsrId());
                usrFollow.setUsrNckNm(friendDao.getUsrNckNm());
                usrFollow.setProfileImg(StarmanHelper.getTargetImageUrl("usr", friendDao.getUsrUid(), friendDao.getProfileImg()));
                usrFollow.setUsrLikeStatus(usrLikeService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), friendDao.getUsrUid()) != null);

                usrFollowList.add(usrFollow);
            } else if (friendDao == null) {
                unJoinedPhoneList.add(usrPhone);
            }
        }

        UsrAddressBookDto usrAddressBook = new UsrAddressBookDto();
        usrAddressBook.setJoinedList(usrFollowList);
        usrAddressBook.setUnJoinedPhoneList(unJoinedPhoneList);

        return new GenericResponse<>(ResponseMeta.OK, usrAddressBook);
    }

    @ApiOperation(value = "초대정보 얻기",
            notes = "초대코드, 초대현황정보를 얻는다.")
    @GetMapping(value = "/inviteInfo")
    public GenericResponse<UsrInviteInfoDto> getInviteInfo(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrInviteInfoDto inviteInfo = new UsrInviteInfoDto();
        try {
            inviteInfo.setInviteCode(Base64.getEncoder().encodeToString(usr.getUsrId().getBytes("UTF-8"))); //초청코드 usrId
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            inviteInfo.setInviteCode("");
        }
        inviteInfo.setInvitedCount(inviteRewardHisService.countInvitedUserByUsrUid(usr.getUsrUid()));
        inviteInfo.setBoughtCount(inviteRewardHisService.countBoughtInvitedUserByUsrUid(usr.getUsrUid()));
        inviteInfo.setInvitedMonthCount(inviteRewardHisService.countInvitedUserByUsrUidThisMonth(usr.getUsrUid()));

        return new GenericResponse<>(ResponseMeta.OK, inviteInfo);
    }

    @ApiOperation(value = "셀럽머니 변경이력 얻기",
            notes = "셀럽머니 변경이력을 얻는다.")
    @GetMapping(value = "/history/money")
    public GenericResponse<UsrMoneyHisContentDto> getMoneyHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrMoneyHisContentDto usrMoneyHisContent = new UsrMoneyHisContentDto();
        usrMoneyHisContent.setMoney(usr.getMoney());
        usrMoneyHisContent.setCashbackMoney(moneyChangeHisService.getTotalCashbackMoneyByUsrUid(usr.getUsrUid()));
        usrMoneyHisContent.setHistory(moneyChangeHisService.getHistoryList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));

        return new GenericResponse<>(ResponseMeta.OK, usrMoneyHisContent);
    }

    @ApiOperation(value = "회원구매내역",
            notes = "회원구매내역을 얻는다.")
    @GetMapping(value = "/history/buy")
    @Transactional(readOnly = true)
    public GenericResponse<BuyHistoryDto> getBuyHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        BuyHistoryDto buyHistory = new BuyHistoryDto();
        buyHistory.setNegoCount(dealService.countByUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.NEGO_REQUEST.getCode(),
                        DEAL_STATUS_TYPE.COUNTER_NEGO_REQUEST.getCode())));
        buyHistory.setCompletedCount(dealService.countByUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode(),
                        DEAL_STATUS_TYPE.CASH_COMPLETE.getCode())));
        buyHistory.setPreparedCount(dealService.countByUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode())));
        buyHistory.setVerifiedCount(dealService.countByUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.PDT_VERIFY.getCode())));
        buyHistory.setProgressCount(dealService.countByUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DELIVERY_PROGRESS.getCode())));
        buyHistory.setSentCount(dealService.countByUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DELIVERY_COMPLETE.getCode())));
        buyHistory.setHistory(dealService.findAllByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));

        return new GenericResponse<>(ResponseMeta.OK, buyHistory);
    }

    @ApiOperation(value = "회원판매내역",
            notes = "회원판매내역을 얻는다.")
    @GetMapping(value = "/history/sell")
    @Transactional(readOnly = true)
    public GenericResponse<SellHistoryDto> getSellHistory(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        SellHistoryDto sellHistory = new SellHistoryDto();
        sellHistory.setNegoCount(dealService.countBySellerUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.NEGO_REQUEST.getCode(),
                        DEAL_STATUS_TYPE.COUNTER_NEGO_REQUEST.getCode())));
        sellHistory.setCachedCount(dealService.countBySellerUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.CASH_COMPLETE.getCode())));
        sellHistory.setPreparedCount(dealService.countBySellerUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode())));
        sellHistory.setProgressCount(dealService.countBySellerUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DELIVERY_PROGRESS.getCode())));
        sellHistory.setSentCount(dealService.countBySellerUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DELIVERY_COMPLETE.getCode())));
        sellHistory.setCompletedCount(dealService.countBySellerUsrUidAndStatus(usr.getUsrUid(),
                Arrays.asList(DEAL_STATUS_TYPE.DEAL_COMPLETE.getCode())));
        sellHistory.setHistory(dealService.findAllBySellerUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid()));

        return new GenericResponse<>(ResponseMeta.OK, sellHistory);
    }

    @ApiOperation(value = "추천유저목록",
            notes = "셀럽 추천유저목록을 얻는다")
    @GetMapping(value = "/recommend")
    public GenericResponse<Page<UsrRecommendDto>> getSearchRecommendUser(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, usrService.findAllRecommendListByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), usr.getGender()));
    }

    @ApiOperation(value = "유저 숨기기",
            notes = "해당 유저를 추천목록에서 숨기기한다.")
    @PutMapping(value = "/{usrUid}/hide")
    public GenericResponse<Void> insertHide(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "usrUid") int peerUsrUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        UsrDao peerUsr = usrService.getByUsrUid(peerUsrUid);

        if (peerUsr == null || peerUsr.getStatus() != 1) {
            return new GenericResponse<>(ResponseMeta.USER_NOT_EXISTS, null);
        }

        if (peerUsrUid < 1 || usr.getUsrUid() == peerUsrUid) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        UsrHideDao usrHide = usrHideService.getByUsrUidAndPeerUsrUid(usr.getUsrUid(), peerUsrUid);
        if (usrHide != null)
            return new GenericResponse<>(ResponseMeta.ALREADY_ADDED, null);

        usrHide = new UsrHideDao();
        usrHide.setUsrUid(usr.getUsrUid());
        usrHide.setPeerUsrUid(peerUsrUid);
        usrHideService.save(usrHide);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }
}
