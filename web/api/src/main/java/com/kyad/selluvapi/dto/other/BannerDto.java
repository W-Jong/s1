package com.kyad.selluvapi.dto.other;

import com.kyad.selluvapi.dao.BannerDao;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BannerDto {
    @ApiModelProperty("배너정보")
    private BannerDao banner;
    @ApiModelProperty("타겟오브젝트(null검사필수), 공지사항인 경우 NoticeDao, 이벤트인 경우 EventDao, 테마인 경우 null")
    private Object target;
}
