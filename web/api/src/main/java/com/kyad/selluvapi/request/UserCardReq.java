package com.kyad.selluvapi.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class UserCardReq {

    @ApiModelProperty("카드종류 1-개인, 2-법인")
    @Range(min = 1, max = 2)
    private int kind;

    @ApiModelProperty("카드번호 (형식:dddd-dddd-dddd-dddd)")
    @NotEmpty
    @Length(min = 18, max = 19)
    private String cardNumber;

    @ApiModelProperty("유효기간 (형식:2018-01-01)")
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate validDate;

    @ApiModelProperty("유효번호 개인일경우 생년월일, 법인일 경우 사업자번호")
    @NotEmpty
    private String validNum;

    @ApiModelProperty("비밀번호")
    @NotEmpty
    private String password;
}
