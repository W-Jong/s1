package com.kyad.selluvapi.dto.brand;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BrandRecommendedListDto {
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    @ApiModelProperty("영문이름")
    private String nameEn;
    @ApiModelProperty("한글이름")
    private String nameKo;
    @ApiModelProperty("로고이미지")
    private String logoImg;
    @ApiModelProperty("백그라운드 이미지")
    private String backImg;
    @ApiModelProperty("상품갯수")
    private int pdtCount;
}
