package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;


@Data
public class UserProfileReq {

    @NotEmpty
    @ApiModelProperty("닉네임")
    private String usrNckNm;
    @NotNull
    @ApiModelProperty("남성군 좋아요")
    private Boolean isMale;
    @NotNull
    @ApiModelProperty("여성군 좋아요")
    private Boolean isFemale;
    @NotNull
    @ApiModelProperty("키즈군 좋아요")
    private Boolean isChildren;
    @NotNull
    @ApiModelProperty("자기소개")
    private String description;
    @NotNull
    @ApiModelProperty("프로필이미지, 변경하지 않는 경우 빈문자열")
    private String profileImg;
    @NotNull
    @ApiModelProperty("배경이미지, 변경하지 않는 경우 빈문자열")
    private String profileBackImg;
}
