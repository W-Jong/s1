package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.SNS_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
public class UserSnsReq {

    @ApiModelProperty("SNS유형")
    @NotNull
    private SNS_TYPE snsType;

    @ApiModelProperty("SNS아이디")
    @NotEmpty
    private String snsId;

    @ApiModelProperty("SNS이메일")
    @NotNull
    private String snsEmail;

    @ApiModelProperty("SNS닉네임")
    @NotNull
    private String snsNckNm;

    @ApiModelProperty("SNS프로필이미지")
    @NotNull
    private String snsProfileImg;

    @ApiModelProperty("SNS토큰")
    @NotNull
    private String snsToken;
}
