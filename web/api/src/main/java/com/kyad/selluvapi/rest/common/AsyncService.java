package com.kyad.selluvapi.rest.common;

import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.helper.push.FCMPushHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {
    @Async
    public void pushFCMNotification(String to, String body, PUSH_TYPE pushType, int targetUid) {
        FCMPushHelper.pushFCMNotification(to, body, pushType, targetUid);
    }
}
