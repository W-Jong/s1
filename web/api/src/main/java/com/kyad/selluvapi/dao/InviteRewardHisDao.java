package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "invite_reward_his", schema = "selluv")
public class InviteRewardHisDao {
    private int inviteRewardHisUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int inviteUsrUid;
    private int joinUsrUid;
    private int kind;
    private int dealUid = 0;
    private long rewardPrice = 0;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invite_reward_his_uid", nullable = false)
    public int getInviteRewardHisUid() {
        return inviteRewardHisUid;
    }

    public void setInviteRewardHisUid(int inviteRewardHisUid) {
        this.inviteRewardHisUid = inviteRewardHisUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "invite_usr_uid", nullable = false)
    public int getInviteUsrUid() {
        return inviteUsrUid;
    }

    public void setInviteUsrUid(int inviteUsrUid) {
        this.inviteUsrUid = inviteUsrUid;
    }

    @Basic
    @Column(name = "join_usr_uid", nullable = false)
    public int getJoinUsrUid() {
        return joinUsrUid;
    }

    public void setJoinUsrUid(int joinUsrUid) {
        this.joinUsrUid = joinUsrUid;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "deal_uid", nullable = false)
    public int getDealUid() {
        return dealUid;
    }

    public void setDealUid(int dealUid) {
        this.dealUid = dealUid;
    }

    @Basic
    @Column(name = "reward_price", nullable = false)
    public long getRewardPrice() {
        return rewardPrice;
    }

    public void setRewardPrice(long rewardPrice) {
        this.rewardPrice = rewardPrice;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InviteRewardHisDao that = (InviteRewardHisDao) o;
        return inviteRewardHisUid == that.inviteRewardHisUid &&
                usrUid == that.usrUid &&
                inviteUsrUid == that.inviteUsrUid &&
                joinUsrUid == that.joinUsrUid &&
                kind == that.kind &&
                dealUid == that.dealUid &&
                rewardPrice == that.rewardPrice &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(inviteRewardHisUid, regTime, usrUid, inviteUsrUid, joinUsrUid, kind, dealUid, rewardPrice, status);
    }
}
