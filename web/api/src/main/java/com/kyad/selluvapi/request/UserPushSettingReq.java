package com.kyad.selluvapi.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;


@Data
public class UserPushSettingReq {

    @ApiModelProperty("거래알림 사용여부 1-사용, 0-사용안함")
    private int dealYn = 1;
    @ApiModelProperty("소식알림 사용여부 1-사용, 0-사용안함")
    private int newsYn = 1;
    @ApiModelProperty("댓글알림 사용여부 1-사용, 0-사용안함")
    private int replyYn = 1;
    @NotEmpty
    @Pattern(regexp = "^[01]{10}$")
    @ApiModelProperty("거래알림 - 10자리 숫자배열(1-사용 0-사용안함)")
    private String dealSetting = "1111111111";
    @NotEmpty
    @Pattern(regexp = "^[01]{12}$")
    @ApiModelProperty("소식알림 - 12자리 숫자배열(1-사용 0-사용안함)")
    private String newsSetting = "111111111111";
    @NotEmpty
    @Pattern(regexp = "^[01]{2}$")
    @ApiModelProperty("댓글알림 - 2자리 숫자배열(1-사용 0-사용안함)")
    private String replySetting = "11";
}
