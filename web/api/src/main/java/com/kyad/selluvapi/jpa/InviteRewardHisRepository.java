package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.InviteRewardHisDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface InviteRewardHisRepository extends CrudRepository<InviteRewardHisDao, Integer> {

    @Query("select a from InviteRewardHisDao a")
    Page<InviteRewardHisDao> findAll(Pageable pageable);

    long countAllByUsrUidAndKind(int usrUid, int kind);

    long countAllByUsrUidAndKindAndRegTimeGreaterThanEqual(int usrUid, int kind, Timestamp regTime);
}
