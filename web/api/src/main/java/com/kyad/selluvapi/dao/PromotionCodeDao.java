package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "promotion_code", schema = "selluv")
public class PromotionCodeDao {
    @ApiModelProperty("프로모션코드UID")
    private int promotionCodeUid;
    @JsonIgnore
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("코드분류, 1-구매용 2-판매용")
    private int kind;
    @ApiModelProperty("프로모션 코드")
    private String title;
    @ApiModelProperty("혜택가격")
    private long price;
    @ApiModelProperty("혜택단위 1-퍼센트단위 2-원단위")
    private int priceUnit;
    @ApiModelProperty("혜택메모")
    private String memo;
    @JsonIgnore
    private int limitType;
    @JsonIgnore
    private String limitUids;
    @ApiModelProperty("프로모션적용 시작날짜")
    private Timestamp startTime;
    @ApiModelProperty("프로모션적용 끝날짜")
    private Timestamp endTime;
    @JsonIgnore
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "promotion_code_uid", nullable = false)
    public int getPromotionCodeUid() {
        return promotionCodeUid;
    }

    public void setPromotionCodeUid(int promotionCodeUid) {
        this.promotionCodeUid = promotionCodeUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 63)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "price", nullable = false)
    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Basic
    @Column(name = "price_unit", nullable = false)
    public int getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(int priceUnit) {
        this.priceUnit = priceUnit;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = 255)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "limit_type", nullable = false)
    public int getLimitType() {
        return limitType;
    }

    public void setLimitType(int limitType) {
        this.limitType = limitType;
    }

    @Basic
    @Column(name = "limit_uids", nullable = false, length = 1023)
    public String getLimitUids() {
        return limitUids;
    }

    public void setLimitUids(String limitUids) {
        this.limitUids = limitUids;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = false)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionCodeDao that = (PromotionCodeDao) o;
        return promotionCodeUid == that.promotionCodeUid &&
                kind == that.kind &&
                price == that.price &&
                priceUnit == that.priceUnit &&
                limitType == that.limitType &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(title, that.title) &&
                Objects.equals(memo, that.memo) &&
                Objects.equals(limitUids, that.limitUids) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(promotionCodeUid, regTime, kind, title, price, priceUnit, memo, limitType, limitUids, startTime, endTime, status);
    }
}
