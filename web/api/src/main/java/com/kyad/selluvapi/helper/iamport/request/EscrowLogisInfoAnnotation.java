package com.kyad.selluvapi.helper.iamport.request;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.rest.common.serializer.GsonUnixTimestampSerializer;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class EscrowLogisInfoAnnotation {
    @SerializedName("company")
    private String company;

    @SerializedName("invoice")
    private String invoice;

    @JsonAdapter(GsonUnixTimestampSerializer.class)
    @SerializedName("sent_at")
    private Timestamp sent_at;
}