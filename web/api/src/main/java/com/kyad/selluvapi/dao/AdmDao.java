package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "adm", schema = "selluv")
public class AdmDao {
    private int admUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private String admId;
    private String admPwd;
    private String admNck;
    private String admMail;
    private String admPhn;
    private int privilegeUid;
    private Timestamp edtTime;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adm_uid", nullable = false)
    public int getAdmUid() {
        return admUid;
    }

    public void setAdmUid(int admUid) {
        this.admUid = admUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "adm_id", nullable = false, length = 50)
    public String getAdmId() {
        return admId;
    }

    public void setAdmId(String admId) {
        this.admId = admId;
    }

    @Basic
    @Column(name = "adm_pwd", nullable = false, length = 100)
    public String getAdmPwd() {
        return admPwd;
    }

    public void setAdmPwd(String admPwd) {
        this.admPwd = admPwd;
    }

    @Basic
    @Column(name = "adm_nck", nullable = false, length = 100)
    public String getAdmNck() {
        return admNck;
    }

    public void setAdmNck(String admNck) {
        this.admNck = admNck;
    }

    @Basic
    @Column(name = "adm_mail", nullable = false, length = 100)
    public String getAdmMail() {
        return admMail;
    }

    public void setAdmMail(String admMail) {
        this.admMail = admMail;
    }

    @Basic
    @Column(name = "adm_phn", nullable = false, length = 50)
    public String getAdmPhn() {
        return admPhn;
    }

    public void setAdmPhn(String admPhn) {
        this.admPhn = admPhn;
    }

    @Basic
    @Column(name = "privilege_uid", nullable = false)
    public int getPrivilegeUid() {
        return privilegeUid;
    }

    public void setPrivilegeUid(int privilegeUid) {
        this.privilegeUid = privilegeUid;
    }

    @Basic
    @Column(name = "edt_time", nullable = false)
    public Timestamp getEdtTime() {
        return edtTime;
    }

    public void setEdtTime(Timestamp edtTime) {
        this.edtTime = edtTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmDao admDao = (AdmDao) o;
        return admUid == admDao.admUid &&
                privilegeUid == admDao.privilegeUid &&
                status == admDao.status &&
                Objects.equals(regTime, admDao.regTime) &&
                Objects.equals(admId, admDao.admId) &&
                Objects.equals(admPwd, admDao.admPwd) &&
                Objects.equals(admNck, admDao.admNck) &&
                Objects.equals(admMail, admDao.admMail) &&
                Objects.equals(admPhn, admDao.admPhn) &&
                Objects.equals(edtTime, admDao.edtTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(admUid, regTime, admId, admPwd, admNck, admMail, admPhn, privilegeUid, edtTime, status);
    }
}
