package com.kyad.selluvapi.helper.sweettracker.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Companies {
    //택배 회사 코드
    @SerializedName("Code")
    private String code;

    //택배 회사 이름
    @SerializedName("Name")
    private String name;
}
