package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.DealDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface DealRepository extends CrudRepository<DealDao, Integer> {

    @Query("select a from DealDao a")
    Page<DealDao> findAll(Pageable pageable);

    @Query("select count(d) from DealDao d where d.usrUid = ?1 and d.status between 1 and 10")
    long countPaymentByUsrUid(int usrUid);

    @Query("select count(d) from DealDao d where d.sellerUsrUid = ?1 and d.status between 1 and 10")
    long countPaymentBySellerUsrUid(int sellerUsrUid);

    @Query("select count(d) from DealDao d where d.usrUid = ?1 and d.status <> 0")
    long countPaymentHistoryByUsrUid(int usrUid);

    @Query("select count(d) from DealDao d where d.sellerUsrUid = ?1 and d.status <> 0")
    long countPaymentHistoryBySellerUsrUid(int sellerUsrUid);

    @Query("select count(d) from DealDao d where d.usrUid = ?1 and d.status in ?2")
    long countByUsrUidAndStatus(int usrUid, List<Integer> statusList);

    @Query("select count(d) from DealDao d where d.sellerUsrUid = ?1 and d.status in ?2")
    long countBySellerUsrUidAndStatus(int sellerUsrUid, List<Integer> statusList);

    @Query(value = "SELECT d.deal_uid, d.reg_time, d.pdt_uid, d.usr_uid, d.seller_usr_uid, d.brand_en, d.pdt_title, d.pdt_size, " +
            " d.pdt_img, d.pdt_price, d.send_price, d.verify_price, d.price, d.cash_price, d.req_price, " +
            " d.pay_time, d.delivery_time, d.comp_time, d.cash_comp_time, d.status, " +
            " u.usr_uid AS target_usr_uid, u.usr_id AS target_usr_id, u.usr_nck_nm AS target_usr_nck_nm, u.profile_img AS target_profile_img " +
            " FROM deal d JOIN usr u ON d.seller_usr_uid = u.usr_uid " +
            " WHERE d.status <> 0 AND d.usr_uid = ?1 " +
            " ORDER BY d.reg_time DESC  \n#pageable\n ",
            countQuery = "SELECT count(*) FROM deal d JOIN usr u ON d.seller_usr_uid = u.usr_uid " +
                    " WHERE d.status <> 0 AND d.usr_uid = ?1",
            nativeQuery = true)
    Page<Object> findAllDealListByUsrUid(Pageable pageable, int usrUid);

    @Query(value = "SELECT d.deal_uid, d.reg_time, d.pdt_uid, d.usr_uid, d.seller_usr_uid, d.brand_en, d.pdt_title, d.pdt_size, " +
            " d.pdt_img, d.pdt_price, d.send_price, d.verify_price, d.price, d.cash_price, d.req_price, " +
            " d.pay_time, d.delivery_time, d.comp_time, d.cash_comp_time, d.status, " +
            " u.usr_uid AS target_usr_uid, u.usr_id AS target_usr_id, u.usr_nck_nm AS target_usr_nck_nm, u.profile_img AS target_profile_img " +
            " FROM deal d JOIN usr u ON d.usr_uid = u.usr_uid " +
            " WHERE d.status <> 0 AND d.seller_usr_uid = ?1 " +
            " ORDER BY d.reg_time DESC  \n#pageable\n ",
            countQuery = "SELECT count(*) FROM deal d JOIN usr u ON d.usr_uid = u.usr_uid " +
                    " WHERE d.status <> 0 AND d.seller_usr_uid = ?1",
            nativeQuery = true)
    Page<Object> findAllDealListBySellerUsrUid(Pageable pageable, int sellerUsrUid);

    @Query("select d from DealDao d where d.status = ?1 and d.payTime < ?2 and (d.deliveryNumber = '' or d.verifyDeliveryNumber = '')")
    List<DealDao> findAllUnUpdatedDeliveryNumber(int status, Timestamp payTime);

    @Query("select d from DealDao d where d.status = ?1 and d.deliveryTime < ?2")
    List<DealDao> findAllUnUpdatedDealCompleted(int status, Timestamp deliveryTime);

    @Query("select d from DealDao d where d.status = ?1 and d.negoTime < ?2")
    List<DealDao> findAllUnUpdatedNego(int status, Timestamp negoTime);

    @Query("select d from DealDao d where d.status in (2, 3) and d.deliveryNumber <> ''")
    List<DealDao> findAllDeliveryProcessing();
}
