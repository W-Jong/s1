package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "usr_pdt_like", schema = "selluv")
public class UsrPdtLikeDao {
    private int usrPdtLikeUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int usrUid;
    private int pdtUid;
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_pdt_like_uid", nullable = false)
    public int getUsrPdtLikeUid() {
        return usrPdtLikeUid;
    }

    public void setUsrPdtLikeUid(int usrPdtLikeUid) {
        this.usrPdtLikeUid = usrPdtLikeUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrPdtLikeDao that = (UsrPdtLikeDao) o;
        return usrPdtLikeUid == that.usrPdtLikeUid &&
                usrUid == that.usrUid &&
                pdtUid == that.pdtUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(usrPdtLikeUid, regTime, usrUid, pdtUid, status);
    }
}
