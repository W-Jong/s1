package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrSnsDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsrSnsRepository extends CrudRepository<UsrSnsDao, Integer> {

    @Query("select a from UsrSnsDao a")
    Page<UsrSnsDao> findAll(Pageable pageable);

    long countAllBySnsIdAndSnsType(String snsId, int snsType);

    List<UsrSnsDao> findAllByUsrUid(int usrUid);

    UsrSnsDao findByUsrUidAndSnsType(int usrUid, int snsType);
}
