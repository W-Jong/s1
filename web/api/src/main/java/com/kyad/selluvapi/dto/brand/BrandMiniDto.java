package com.kyad.selluvapi.dto.brand;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BrandMiniDto {
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    //    private Timestamp regTime;
    @ApiModelProperty("영문첫글자")
    private String firstEn;
    @ApiModelProperty("한글초성")
    private String firstKo;
    @ApiModelProperty("영문이름")
    private String nameEn;
    @ApiModelProperty("한글이름")
    private String nameKo;
    //    private String licenseUrl;
//    private String logoImg;
    @ApiModelProperty("썸네일이미지")
    private String profileImg;
//    private String backImg;
//    private int usrUid;
//    private int pdtCount;
//    private long totalCount;
//    private long totalPrice;
//    private int status;
}
