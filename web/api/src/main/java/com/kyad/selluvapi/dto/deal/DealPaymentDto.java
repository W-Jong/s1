package com.kyad.selluvapi.dto.deal;

import com.kyad.selluvapi.dao.DealDao;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DealPaymentDto {
    @ApiModelProperty("거래정보")
    DealDao deal;
    @ApiModelProperty("카드간편결제인 경우 빈문자열, 기타 결제URL 웹뷰에서 호출필요함")
    String paymentUrl;
}
