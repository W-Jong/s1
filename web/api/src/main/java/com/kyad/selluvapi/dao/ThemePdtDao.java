package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "theme_pdt", schema = "selluv")
public class ThemePdtDao {
    private int themePdtUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int themeUid;
    private int pdtUid;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "theme_pdt_uid", nullable = false)
    public int getThemePdtUid() {
        return themePdtUid;
    }

    public void setThemePdtUid(int themePdtUid) {
        this.themePdtUid = themePdtUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "theme_uid", nullable = false)
    public int getThemeUid() {
        return themeUid;
    }

    public void setThemeUid(int themeUid) {
        this.themeUid = themeUid;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThemePdtDao that = (ThemePdtDao) o;
        return themePdtUid == that.themePdtUid &&
                themeUid == that.themeUid &&
                pdtUid == that.pdtUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(themePdtUid, regTime, themeUid, pdtUid, status);
    }
}
