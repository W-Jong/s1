package com.kyad.selluvapi.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "recom_brand", schema = "selluv")
public class RecomBrandDao {
    private int recomBrandUid;
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    private int pdtGroup;
    private int brandOrder;
    private int brandUid;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recom_brand_uid", nullable = false)
    public int getRecomBrandUid() {
        return recomBrandUid;
    }

    public void setRecomBrandUid(int recomBrandUid) {
        this.recomBrandUid = recomBrandUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "pdt_group", nullable = false)
    public int getPdtGroup() {
        return pdtGroup;
    }

    public void setPdtGroup(int pdtGroup) {
        this.pdtGroup = pdtGroup;
    }

    @Basic
    @Column(name = "brand_order", nullable = false)
    public int getBrandOrder() {
        return brandOrder;
    }

    public void setBrandOrder(int brandOrder) {
        this.brandOrder = brandOrder;
    }

    @Basic
    @Column(name = "brand_uid", nullable = false)
    public int getBrandUid() {
        return brandUid;
    }

    public void setBrandUid(int brandUid) {
        this.brandUid = brandUid;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecomBrandDao that = (RecomBrandDao) o;
        return recomBrandUid == that.recomBrandUid &&
                pdtGroup == that.pdtGroup &&
                brandOrder == that.brandOrder &&
                brandUid == that.brandUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(recomBrandUid, regTime, pdtGroup, brandOrder, brandUid, status);
    }
}
