package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.REPORT_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
public class ReportReq {
    @ApiModelProperty("대상회원 UID, 회원신고인 경우 usrUid, 상품신고인 경우 pdtUid")
    @NotNull
    private int targetUid;
    @ApiModelProperty("신고유형 1-회원신고, 2-상품신고")
    @Range(min = 1, max = 2)
    private int kind;
    @ApiModelProperty("신고사유")
    @NotNull
    private REPORT_TYPE reportType;
    @ApiModelProperty("신고내용")
    @NotEmpty
    private String content;
}
