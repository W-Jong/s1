package com.kyad.selluvapi.dto.other;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SystemSettingDto {
    @ApiModelProperty("구매적립(%)")
    private long dealReward;
    @ApiModelProperty("구매후기리워드(원)")
    private long buyReviewReward;
    @ApiModelProperty("판매후기리워드(원)")
    private long sellReviewReward;
    @ApiModelProperty("가품리워드(원)")
    private long fakeReward;
    @ApiModelProperty("공유리워드(원)")
    private long shareRewrad;
    @ApiModelProperty("초대자에게 지급되는 가입리워드(원)")
    private long inviterRewrad;
    @ApiModelProperty("가입자에게 지급되는 가입리워드(원)")
    private long invitedUserReward;
    @ApiModelProperty("초대자에게 가입자의 첫 구매 후 지급되는 리워드(원)")
    private long invitedUserFirstDealReward;
}
