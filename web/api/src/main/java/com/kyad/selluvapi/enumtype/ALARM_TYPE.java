package com.kyad.selluvapi.enumtype;

public enum ALARM_TYPE {
    DEAL(1), NEWS(2), REPLY(3);

    private int code;

    private ALARM_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
