package com.kyad.selluvapi.helper.iamport.response;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.rest.common.serializer.GsonUnixTimestampDeserializer;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class CertificationAnnotation {
    @SerializedName("imp_uid")
    String imp_uid;

    @SerializedName("merchant_uid")
    String merchant_uid;

    @SerializedName("pg_tid")
    String pg_tid;

    @SerializedName("pg_provider")
    String pg_provider;

    @SerializedName("name")
    String name;

    @SerializedName("gender")
    String gender;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("birth")
    Timestamp birth;

    @SerializedName("foreigner")
    boolean foreigner;

    @SerializedName("certified")
    boolean certified;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("certified_at")
    Timestamp certified_at;

    @SerializedName("unique_key")
    String unique_key;

    @SerializedName("unique_in_site")
    String unique_in_site;

    //아임포트에 폰번호 요청해야 만 받을수 있는 필드
    @SerializedName("phone")
    String phone;

    //아임포트에 폰번호 요청해야 만 받을수 있는 필드
    @SerializedName("carrier")
    String carrier;
}
