package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.category.CategoryMiniDto;
import com.kyad.selluvapi.dto.pdt.PdtMiniDto;
import com.kyad.selluvapi.dto.usr.UsrFeedPdtListDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import com.kyad.selluvapi.enumtype.FEED_TYPE;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.jpa.UsrFeedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

@Service
public class UsrFeedService {

    private final Logger LOG = LoggerFactory.getLogger(UsrFeedService.class);

    @Autowired
    private UsrFeedRepository usrFeedRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private EntityManager entityManager;

    public UsrFeedDao save(UsrFeedDao dao) {
        return usrFeedRepository.save(dao);
    }

    public Page<UsrFeedPdtListDto> findAllFeedList(Pageable pageable, int usrUid) {
        return usrFeedRepository.findAllFeedList(pageable, usrUid)
                .map(result -> convertListObjectToUsrFeedPdtListDto(((Object[]) result)));
    }

    public Page<UsrFeedPdtListDto> findAllFeedStyleList(Pageable pageable, int usrUid) {
        return usrFeedRepository.findAllFeedStyleList(pageable, usrUid)
                .map(result -> convertListObjectToUsrFeedPdtListDto(((Object[]) result)));
    }

    public UsrFeedPdtListDto convertListObjectToUsrFeedPdtListDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        UsrFeedPdtListDto usrFeedPdtDto = new UsrFeedPdtListDto();

        PdtMiniDto pdtMini = new PdtMiniDto();
        pdtMini.setPdtUid((int) objects[i++]);
        pdtMini.setUsrUid((int) objects[i++]);
        pdtMini.setBrandUid((int) objects[i++]);
        pdtMini.setCategoryUid((int) objects[i++]);
        pdtMini.setPdtSize((String) objects[i++]);

        String[] pdtProfileImageInfo = StarmanHelper.getTargetImageUrlWithSize("pdt", pdtMini.getPdtUid(), (String) objects[i++]);
        pdtMini.setProfileImg(pdtProfileImageInfo[0]);
        usrFeedPdtDto.setPdtProfileImgWidth(Integer.parseInt(pdtProfileImageInfo[1]));
        usrFeedPdtDto.setPdtProfileImgHeight(Integer.parseInt(pdtProfileImageInfo[2]));

        pdtMini.setOriginPrice(((BigInteger) objects[i++]).longValue());
        pdtMini.setPrice(((BigInteger) objects[i++]).longValue());
        pdtMini.setColorName((String) objects[i++]);
        pdtMini.setPdtModel((String) objects[i++]);
        Timestamp pdtMiniRegTime = (Timestamp) objects[i++];
        pdtMini.setEdtTime((Timestamp) objects[i++]);
        pdtMini.setStatus(((Integer) objects[i++]));
        pdtMini.setEdited(pdtMini.getEdtTime().getTime() > (pdtMiniRegTime.getTime() + 1000));
        usrFeedPdtDto.setPdt(pdtMini);

        UsrMiniDto usrMini = new UsrMiniDto();
        usrMini.setUsrUid(pdtMini.getUsrUid());
        usrMini.setUsrId((String) objects[i++]);
        usrMini.setUsrNckNm((String) objects[i++]);
        usrMini.setProfileImg(StarmanHelper.getTargetImageUrl("usr", usrMini.getUsrUid(), (String) objects[i++]));
        usrFeedPdtDto.setUsr(usrMini);

        BrandMiniDto brandMini = new BrandMiniDto();
        brandMini.setBrandUid(pdtMini.getBrandUid());
        brandMini.setFirstKo((String) objects[i++]);
        brandMini.setFirstEn((String) objects[i++]);
        brandMini.setNameKo((String) objects[i++]);
        brandMini.setNameEn((String) objects[i++]);
        brandMini.setProfileImg(StarmanHelper.getTargetImageUrl("brand", brandMini.getBrandUid(), (String) objects[i++]));
        usrFeedPdtDto.setBrand(brandMini);

        CategoryMiniDto categoryMini = new CategoryMiniDto();
        categoryMini.setCategoryUid(pdtMini.getCategoryUid());
        categoryMini.setDepth((int) objects[i++]);
        categoryMini.setCategoryName((String) objects[i++]);
        usrFeedPdtDto.setCategory(categoryMini);

        usrFeedPdtDto.setPdtLikeCount(((BigInteger) objects[i++]).intValue());
        usrFeedPdtDto.setPdtLikeStatus(((BigInteger) objects[i++]).intValue() > 0);

        if (objects[i] != null) {
            PdtStyleDao pdtStyle = new PdtStyleDao();
            pdtStyle.setPdtStyleUid((int) objects[i++]);
            pdtStyle.setRegTime((Timestamp) objects[i++]);
            pdtStyle.setPdtUid(pdtMini.getPdtUid());
            pdtStyle.setUsrUid((int) objects[i++]);

            String[] styleProfileImageInfo = StarmanHelper.getTargetImageUrlWithSize("pdt_style", pdtStyle.getPdtStyleUid(), (String) objects[i++]);
            pdtStyle.setStyleImg(styleProfileImageInfo[0]);
            usrFeedPdtDto.setStyleProfileImgWidth(Integer.parseInt(styleProfileImageInfo[1]));
            usrFeedPdtDto.setStyleProfileImgHeight(Integer.parseInt(styleProfileImageInfo[2]));

            usrFeedPdtDto.setPdtStyle(pdtStyle);
        } else {
            i += 4;
            usrFeedPdtDto.setPdtStyle(null);
            usrFeedPdtDto.setStyleProfileImgWidth(1);
            usrFeedPdtDto.setStyleProfileImgHeight(1);
        }

        UsrFeedDao usrFeed = new UsrFeedDao();
        usrFeed.setUsrFeedUid((int) objects[i++]);
        usrFeed.setRegTime((Timestamp) objects[i++]);
        usrFeed.setUsrUid(pdtMini.getUsrUid());
        usrFeed.setPdtUid(pdtMini.getPdtUid());
        usrFeed.setType((int) objects[i++]);
        usrFeed.setPeerUsrUid((int) objects[i++]);
        usrFeedPdtDto.setUsrFeed(usrFeed);

        if (usrFeed.getPeerUsrUid() != 0) {
            UsrMiniDto peerUsrMini = new UsrMiniDto();
            peerUsrMini.setUsrUid(usrFeed.getPeerUsrUid());
            peerUsrMini.setUsrId((String) objects[i++]);
            peerUsrMini.setUsrNckNm((String) objects[i++]);
            peerUsrMini.setProfileImg(StarmanHelper.getTargetImageUrl("usr", peerUsrMini.getUsrUid(), (String) objects[i++]));
            usrFeedPdtDto.setPeerUsr(peerUsrMini);
        } else {
            i += 3;
            usrFeedPdtDto.setPeerUsr(null);
        }

        return usrFeedPdtDto;
    }

    @Async
    @Transactional
    public void insertFeed(FEED_TYPE feedType, UsrDao usr, PdtDao pdt) {
        if (usr == null || pdt == null)
            return;

        String sql;
        Query query;
        String selectSql;
        Query selectQuery;
        List<UsrDao> usrDaoList = null;

        switch (feedType) {
            case FEED01_FINDING_PDT:
                //찾았던 상품
                sql = "INSERT INTO usr_feed(usr_uid, pdt_uid, type, peer_usr_uid) " +
                        " SELECT uw.usr_uid, :pdt_uid, :type, :peer_usr_uid FROM usr_wish uw " +
                        " WHERE uw.brand_uid = :brand_uid AND uw.pdt_group = :pdt_group AND uw.category_uid IN :category_uids " +
                        " AND IF(uw.pdt_model = '', 1, uw.pdt_model = :pdt_model) " +
                        " AND IF(uw.pdt_size = '', 1, uw.pdt_size = :pdt_size) " +
                        " AND IF(uw.color_name = '', 1, uw.color_name = :color_name) " +
                        " AND uw.usr_uid <> :peer_usr_uid";
                query = entityManager.createNativeQuery(sql);
                query.setParameter("pdt_uid", pdt.getPdtUid());
                query.setParameter("type", feedType.getCode());
                query.setParameter("peer_usr_uid", usr.getUsrUid());
                query.setParameter("brand_uid", pdt.getBrandUid());
                query.setParameter("pdt_group", pdt.getPdtGroup());
                query.setParameter("category_uids", categoryService.getSubAllCategoryUidsWithIt(pdt.getCategoryUid()));
                query.setParameter("pdt_model", pdt.getPdtModel());
                query.setParameter("pdt_size", pdt.getPdtSize());
                query.setParameter("color_name", pdt.getColorName());
                query.executeUpdate();

                //알람
                selectSql = " SELECT u.* FROM usr_wish uw " +
                        " JOIN usr u ON uw.usr_uid = u.usr_uid AND u.status = 1 " +
                        " WHERE uw.brand_uid = :brand_uid AND uw.pdt_group = :pdt_group AND uw.category_uid IN :category_uids " +
                        " AND IF(uw.pdt_model = '', 1, uw.pdt_model = :pdt_model) " +
                        " AND IF(uw.pdt_size = '', 1, uw.pdt_size = :pdt_size) " +
                        " AND IF(uw.color_name = '', 1, uw.color_name = :color_name) " +
                        " AND uw.usr_uid <> :peer_usr_uid";
                selectQuery = entityManager.createNativeQuery(selectSql, UsrDao.class);
                selectQuery.setParameter("peer_usr_uid", usr.getUsrUid());
                selectQuery.setParameter("brand_uid", pdt.getBrandUid());
                selectQuery.setParameter("pdt_group", pdt.getPdtGroup());
                selectQuery.setParameter("category_uids", categoryService.getSubAllCategoryUidsWithIt(pdt.getCategoryUid()));
                selectQuery.setParameter("pdt_model", pdt.getPdtModel());
                selectQuery.setParameter("pdt_size", pdt.getPdtSize());
                selectQuery.setParameter("color_name", pdt.getColorName());
                usrDaoList = selectQuery.getResultList();
                if (usrDaoList != null && usrDaoList.size() > 0) {
                    BrandDao brand = brandService.getByBrandUid(pdt.getBrandUid());
                    CategoryDao category = categoryService.findOne(pdt.getCategoryUid());
                    String extra = "" + ((brand != null) ? brand.getNameKo() : "") + " " + ((category != null) ? category.getCategoryName() : "");
                    for (UsrDao peerUsr : usrDaoList) {
                        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH21_FINDING_PDT, peerUsr, usr, null, pdt, extra, 0);
                    }
                }
                break;
            case FEED02_LIKE_PDT_SALE:
                //LOVEITEM 가격인하
                sql = "INSERT INTO usr_feed(usr_uid, pdt_uid, type, peer_usr_uid) " +
                        " SELECT upl.usr_uid, :pdt_uid, :type, :peer_usr_uid FROM usr_pdt_like upl " +
                        " WHERE upl.pdt_uid = :pdt_uid AND upl.usr_uid <> :peer_usr_uid";
                query = entityManager.createNativeQuery(sql);
                query.setParameter("pdt_uid", pdt.getPdtUid());
                query.setParameter("type", feedType.getCode());
                query.setParameter("peer_usr_uid", usr.getUsrUid());
                query.executeUpdate();

                selectSql = " SELECT u.* FROM usr_pdt_like upl " +
                        " JOIN usr u ON upl.usr_uid = u.usr_uid AND u.status = 1 " +
                        " WHERE upl.pdt_uid = :pdt_uid AND upl.usr_uid <> :peer_usr_uid";
                selectQuery = entityManager.createNativeQuery(selectSql, UsrDao.class);
                selectQuery.setParameter("pdt_uid", pdt.getPdtUid());
                selectQuery.setParameter("peer_usr_uid", usr.getUsrUid());
                usrDaoList = selectQuery.getResultList();
                if (usrDaoList != null && usrDaoList.size() > 0) {
                    for (UsrDao peerUsr : usrDaoList) {
                        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH17_LIKE_PDT_SALE, peerUsr, usr, null, pdt);
                    }
                }
                break;
            case FEED03_FOLLOWING_BRAND:
                //팔로우브랜드
                sql = "INSERT INTO usr_feed(usr_uid, pdt_uid, type, peer_usr_uid) " +
                        " SELECT ubl.usr_uid, :pdt_uid, :type, :peer_usr_uid FROM usr_brand_like ubl " +
                        " WHERE ubl.brand_uid = :brand_uid AND ubl.usr_uid <> :peer_usr_uid";
                query = entityManager.createNativeQuery(sql);
                query.setParameter("pdt_uid", pdt.getPdtUid());
                query.setParameter("type", feedType.getCode());
                query.setParameter("peer_usr_uid", usr.getUsrUid());
                query.setParameter("brand_uid", pdt.getBrandUid());
                query.executeUpdate();

                selectSql = " SELECT u.* FROM usr_brand_like ubl " +
                        " JOIN usr u ON ubl.usr_uid = u.usr_uid AND u.status = 1 " +
                        " WHERE ubl.brand_uid = :brand_uid AND ubl.usr_uid <> :peer_usr_uid";
                selectQuery = entityManager.createNativeQuery(selectSql, UsrDao.class);
                selectQuery.setParameter("peer_usr_uid", usr.getUsrUid());
                selectQuery.setParameter("brand_uid", pdt.getBrandUid());
                usrDaoList = selectQuery.getResultList();
                if (usrDaoList != null && usrDaoList.size() > 0) {
                    BrandDao brand = brandService.getByBrandUid(pdt.getBrandUid());
                    String extra = "" + ((brand != null) ? brand.getNameKo() : "");
                    for (UsrDao peerUsr : usrDaoList) {
                        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH20_FOLLOWING_BRAND_PDT, peerUsr, usr, null, pdt, extra, 0);
                    }
                }
                break;
            case FEED04_FOLLOWING_USER_WISH:
            case FEED05_FOLLOWING_USER_STYLE:
            case FEED06_FOLLOWING_USER_PDT:
                //팔로우유저 잇템
                //팔로우유저 스타일
                //팔로우 유저 신상품
                sql = "INSERT INTO usr_feed(usr_uid, pdt_uid, type, peer_usr_uid) " +
                        " SELECT ul.usr_uid, :pdt_uid, :type, :peer_usr_uid FROM usr_like ul " +
                        " WHERE ul.peer_usr_uid = :peer_usr_uid AND ul.usr_uid <> :peer_usr_uid";
                query = entityManager.createNativeQuery(sql);
                query.setParameter("pdt_uid", pdt.getPdtUid());
                query.setParameter("type", feedType.getCode());
                query.setParameter("peer_usr_uid", usr.getUsrUid());
                query.executeUpdate();

                if (feedType.getCode() == FEED_TYPE.FEED05_FOLLOWING_USER_STYLE.getCode() ||
                        feedType.getCode() == FEED_TYPE.FEED06_FOLLOWING_USER_PDT.getCode()) {
                    selectSql = " SELECT u.* FROM usr_like ul " +
                            " JOIN usr u ON ul.usr_uid = u.usr_uid AND u.status = 1 " +
                            " WHERE ul.peer_usr_uid = :peer_usr_uid AND ul.usr_uid <> :peer_usr_uid";
                    selectQuery = entityManager.createNativeQuery(selectSql, UsrDao.class);
                    selectQuery.setParameter("peer_usr_uid", usr.getUsrUid());
                    usrDaoList = selectQuery.getResultList();
                    if (usrDaoList != null && usrDaoList.size() > 0) {
                        PUSH_TYPE pushType = (feedType.getCode() == FEED_TYPE.FEED05_FOLLOWING_USER_STYLE.getCode()) ?
                                PUSH_TYPE.PUSH19_FOLLOWING_USER_STYLE : PUSH_TYPE.PUSH18_FOLLOWING_USER_PDT;
                        for (UsrDao peerUsr : usrDaoList) {
                            usrAlarmService.insertUsrAlarm(pushType, peerUsr, usr, null, pdt);
                        }
                    }
                }
                break;
        }
    }
}
