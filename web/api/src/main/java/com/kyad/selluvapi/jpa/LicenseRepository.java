package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.LicenseDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface LicenseRepository extends CrudRepository<LicenseDao, Integer> {

    @Query("select a from LicenseDao a")
    Page<LicenseDao> findAll(Pageable pageable);
}
