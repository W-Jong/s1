package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "qna", schema = "selluv")
public class QnaDao {
    private int qnaUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("타이틀")
    private String title;
    @ApiModelProperty("내용")
    private String content;
    @ApiModelProperty("답변")
    private String answer = "";
    @ApiModelProperty("답변시간")
    private Timestamp compTime = null;
    @JsonIgnore
    private String memo = "";
    @ApiModelProperty("1-답변완료, 2-답변대기(신청중)")
    private int status = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qna_uid", nullable = false)
    public int getQnaUid() {
        return qnaUid;
    }

    public void setQnaUid(int qnaUid) {
        this.qnaUid = qnaUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "answer", nullable = false, length = -1)
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Basic
    @Column(name = "comp_time", nullable = true)
    public Timestamp getCompTime() {
        return compTime;
    }

    public void setCompTime(Timestamp compTime) {
        this.compTime = compTime;
    }

    @Basic
    @Column(name = "memo", nullable = false, length = -1)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QnaDao qnaDao = (QnaDao) o;
        return qnaUid == qnaDao.qnaUid &&
                usrUid == qnaDao.usrUid &&
                status == qnaDao.status &&
                Objects.equals(regTime, qnaDao.regTime) &&
                Objects.equals(title, qnaDao.title) &&
                Objects.equals(content, qnaDao.content) &&
                Objects.equals(answer, qnaDao.answer) &&
                Objects.equals(compTime, qnaDao.compTime) &&
                Objects.equals(memo, qnaDao.memo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(qnaUid, regTime, usrUid, title, content, answer, compTime, memo, status);
    }
}
