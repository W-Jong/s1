package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.DealDao;
import com.kyad.selluvapi.dao.PdtDao;
import com.kyad.selluvapi.dao.UsrAddressDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.deal.DealPaymentDto;
import com.kyad.selluvapi.enumtype.DEAL_STATUS_TYPE;
import com.kyad.selluvapi.enumtype.MONEY_HIS_TYPE;
import com.kyad.selluvapi.enumtype.PDT_STATUS_TYPE;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.IamportDeliveryCompanyHelper;
import com.kyad.selluvapi.helper.iamport.request.EscrowData;
import com.kyad.selluvapi.helper.iamport.request.EscrowLogisInfoAnnotation;
import com.kyad.selluvapi.helper.iamport.request.EscrowLogisPeopleAnnotation;
import com.kyad.selluvapi.helper.iamport.response.EscrowLogisAnnotation;
import com.kyad.selluvapi.helper.iamport.response.PaymentAnnotation;
import com.kyad.selluvapi.rest.common.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class PaymentResultService {

    @Autowired
    private DealService dealService;

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @Autowired
    private MoneyChangeHisService moneyChangeHisService;

    @Autowired
    private InviteRewardHisService inviteRewardHisService;

    public boolean paymentResultProcess(PaymentAnnotation paymentAnnotation, boolean isCard) {

        if (paymentAnnotation == null)
            return false;

        DealDao deal = null;
        try {
            deal = dealService.getDealInfo(Integer.parseInt(paymentAnnotation.getMerchant_uid()));
        } catch (Exception e) {
            deal = null;
            e.printStackTrace();
        }
        if (deal == null)
            return false;

        if ("paid".equals(paymentAnnotation.getStatus())) {
            if (deal.getStatus() != DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode()) {

                deal.setPayImportCode(paymentAnnotation.getImp_uid());
                deal.setPayTime(new Timestamp(System.currentTimeMillis()));
                deal.setCancelTime(null);
                deal.setCancelReason("");
                deal.setSelluvPrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_FEE_RATE));
                deal.setPayFeePrice((long) Math.ceil((deal.getPdtPrice() + deal.getSendPrice()) * CommonConstant.SELLUV_PAY_FEE_RATE));
                deal.setStatus(DEAL_STATUS_TYPE.DELIVERY_PREPARE.getCode());
                dealService.save(deal);

                //에스크로 등록
                IamportClientHelper iamportClientHelper = new IamportClientHelper();
                EscrowData escrowData = new EscrowData();

                UsrDao usr = usrService.getByUsrUid(deal.getUsrUid());
                UsrDao sellerUsr = usrService.getByUsrUid(deal.getSellerUsrUid());
                PdtDao pdt = pdtService.getPdtByPdtUid(deal.getPdtUid());
                if (sellerUsr != null) {
                    EscrowLogisPeopleAnnotation sender = new EscrowLogisPeopleAnnotation();
                    sender.setName(sellerUsr.getUsrNm());
                    sender.setTel(sellerUsr.getUsrPhone());
                    UsrAddressDao sellerAddress = usrService.getAddressActiveByUsrUid(sellerUsr.getUsrUid());
                    sender.setAddr((sellerAddress == null) ? "" : sellerAddress.getAddressDetail());
                    sender.setPostcode((sellerAddress == null) ? "" : sellerAddress.getAddressDetailSub());
                    escrowData.setSender(sender);

                    EscrowLogisPeopleAnnotation receiver = new EscrowLogisPeopleAnnotation();
                    receiver.setName(deal.getRecipientNm());
                    receiver.setTel(deal.getRecipientPhone());
                    receiver.setAddr(deal.getRecipientAddress());
                    receiver.setPostcode("");
                    escrowData.setReceiver(receiver);

                    EscrowLogisInfoAnnotation logis = new EscrowLogisInfoAnnotation();
                    String[] deliveryInfos = deal.getDeliveryNumber().split(" ");
                    if (deliveryInfos.length < 1) {
                        logis.setCompany("");
                        logis.setInvoice("");
                    } else if (deliveryInfos.length < 2) {
                        logis.setCompany(IamportDeliveryCompanyHelper.getCompanyCode(deliveryInfos[0]));
                        logis.setInvoice("");
                    } else {
                        logis.setCompany(IamportDeliveryCompanyHelper.getCompanyCode(deliveryInfos[0]));
                        logis.setInvoice(deliveryInfos[1]);
                    }
                    logis.setSent_at(new Timestamp(System.currentTimeMillis()));

                    escrowData.setLogis(logis);

                    EscrowLogisAnnotation escrowLogisAnnotation = null;
                    try {
                        escrowLogisAnnotation = iamportClientHelper.registerEscrowProcess(deal.getPayImportCode(), escrowData).getResponse();
                    } catch (Exception e) {
                        escrowLogisAnnotation = null;
                        e.printStackTrace();
                    }
                }

                if (usr != null && pdt != null) {
                    if (deal.getRewardPrice() > 0) { //셀럽머니를 사용하면
                        moneyChangeHisService.insertPointHistory(usr, -deal.getRewardPrice(), MONEY_HIS_TYPE.MONEYUSE, deal);
                    }

                    inviteRewardHisService.addFirstBoughtReward(usr, deal);
                    pdt.setStatus(PDT_STATUS_TYPE.SOLD.getCode());//상품판매완료로 전환
                    pdtService.save(pdt);

                    if (sellerUsr != null)
                        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH01_S_PAY_COMPLETED, sellerUsr, usr, deal, pdt);
                }

                return true;
            } else {
                //이미 처리된 상태(중복호출로 봄)
                return true;
            }
        } else {
            //결제실패처리 (삭제로 처리함)
            deal.setCancelTime(new Timestamp(System.currentTimeMillis()));
            deal.setCancelReason(isCard ? "카드간편결제실패" : "결제실패");
            deal.setStatus(DEAL_STATUS_TYPE.DELETED.getCode());
            dealService.save(deal);
            return false;
        }
    }

    public boolean paymentResultProcess(PaymentAnnotation paymentAnnotation) {
        return paymentResultProcess(paymentAnnotation, false);
    }
}
