package com.kyad.selluvapi.dto.search;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SearchWordDto {
    @ApiModelProperty("검색어")
    private String word;
    @ApiModelProperty("조회수")
    private long count;
}
