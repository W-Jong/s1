package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.SizeRefDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SizeRefRepository extends CrudRepository<SizeRefDao, Integer> {

    @Query("select a from SizeRefDao a")
    Page<SizeRefDao> findAll(Pageable pageable);

    @Query(value = "SELECT DISTINCT category1 FROM size_ref WHERE status IN ?1", nativeQuery = true)
    List<Integer> findCategory2UidsByStatus(List<Integer> statusList);

    @Query(value = "SELECT DISTINCT type_name FROM size_ref WHERE category1 = ?1 " +
            " AND if(?2 <> '', category2 = ?2, 1) AND if(?3 <> '', category3 = ?3, 1)",
            nativeQuery = true)
    List<String> findSizeFirstByCategory1AndCategory2AndCategory3(int categoryUid, String category2, String cagegory3);

    @Query(value = "SELECT DISTINCT size_name FROM size_ref WHERE category1 = ?1 " +
            " AND if(?2 <> '', category2 = ?2, 1) AND if(?3 <> '', category3 = ?3, 1) AND type_name = ?4",
            nativeQuery = true)
    List<String> findSizeSecondByCategory1AndCategory2AndCategory3AndTypeName(int categoryUid, String category2, String cagegory3, String typeName);
}
