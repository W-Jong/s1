package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "delivery_history", schema = "selluv")
public class DeliveryHistoryDao {
    @ApiModelProperty("배송번호")
    private int deliveryHistoryUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("발송자회원UID")
    private int usrUid;
    @ApiModelProperty("연관 거래UID")
    private int dealUid;
    @ApiModelProperty("택배형태 1-구매자구매(판매자가 배송), 2-판매자반품(구매자가 배송)")
    private int type = 1;
    @ApiModelProperty("택배유형 1-편의점택배, 2-일반택배")
    private int kind = 1;
    @ApiModelProperty("택배사이름")
    private String deliveryCompany = "";
    @ApiModelProperty("운송장번호")
    private String deliveryNumber = "";
    @ApiModelProperty("수령자이름")
    private String recipientNm = "";
    @ApiModelProperty("수령자연락처")
    private String recipientPhone = "";
    @ApiModelProperty("수령자주소")
    private String recipientAddress = "";
    @ApiModelProperty("수령자상세주소(우편번호)")
    private String recipientAddressDetail = "";
    @ApiModelProperty("편의점택배번호, 일반택배인 경우 빈문자열")
    private String convenienceNumber = "";
    @JsonIgnore
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "delivery_history_uid", nullable = false)
    public int getDeliveryHistoryUid() {
        return deliveryHistoryUid;
    }

    public void setDeliveryHistoryUid(int deliveryHistoryUid) {
        this.deliveryHistoryUid = deliveryHistoryUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "deal_uid", nullable = false)
    public int getDealUid() {
        return dealUid;
    }

    public void setDealUid(int dealUid) {
        this.dealUid = dealUid;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "kind", nullable = false)
    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Basic
    @Column(name = "delivery_company", nullable = false, length = 60)
    public String getDeliveryCompany() {
        return deliveryCompany;
    }

    public void setDeliveryCompany(String deliveryCompany) {
        this.deliveryCompany = deliveryCompany;
    }

    @Basic
    @Column(name = "delivery_number", nullable = false, length = 60)
    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public void setDeliveryNumber(String deliveryNumber) {
        this.deliveryNumber = deliveryNumber;
    }

    @Basic
    @Column(name = "recipient_nm", nullable = false, length = 50)
    public String getRecipientNm() {
        return recipientNm;
    }

    public void setRecipientNm(String recipientNm) {
        this.recipientNm = recipientNm;
    }

    @Basic
    @Column(name = "recipient_phone", nullable = false, length = 30)
    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    @Basic
    @Column(name = "recipient_address", nullable = false, length = 255)
    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    @Basic
    @Column(name = "recipient_address_detail", nullable = false, length = 50)
    public String getRecipientAddressDetail() {
        return recipientAddressDetail;
    }

    public void setRecipientAddressDetail(String recipientAddressDetail) {
        this.recipientAddressDetail = recipientAddressDetail;
    }

    @Basic
    @Column(name = "convenience_number", nullable = false, length = 100)
    public String getConvenienceNumber() {
        return convenienceNumber;
    }

    public void setConvenienceNumber(String convenienceNumber) {
        this.convenienceNumber = convenienceNumber;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryHistoryDao that = (DeliveryHistoryDao) o;
        return deliveryHistoryUid == that.deliveryHistoryUid &&
                usrUid == that.usrUid &&
                dealUid == that.dealUid &&
                type == that.type &&
                kind == that.kind &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(deliveryCompany, that.deliveryCompany) &&
                Objects.equals(deliveryNumber, that.deliveryNumber) &&
                Objects.equals(recipientNm, that.recipientNm) &&
                Objects.equals(recipientPhone, that.recipientPhone) &&
                Objects.equals(recipientAddress, that.recipientAddress) &&
                Objects.equals(recipientAddressDetail, that.recipientAddressDetail) &&
                Objects.equals(convenienceNumber, that.convenienceNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(deliveryHistoryUid, regTime, usrUid, dealUid, type, kind, deliveryCompany, deliveryNumber, recipientNm, recipientPhone, recipientAddress, recipientAddressDetail, convenienceNumber, status);
    }
}
