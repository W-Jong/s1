package com.kyad.selluvapi.helper.cvsnet;

import com.google.gson.Gson;
import com.kyad.selluvapi.helper.cvsnet.request.ReservationReq;
import org.apache.commons.net.ftp.FTP;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.commons.net.ftp.FTPClient;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketException;

public class CvsnetClientHelper {

    //TODO: CVSNET 업체코드 요청중
    public static final int SELLUV_CVSNET_CODE = 35;

    //    private static final String API_URL = "https://www.cvsnet.co.kr/_ver2/shopping/shopping_insert.jsp";
    private static final String API_URL = "https://www.cvsnet.co.kr/_ver2/shopping/shopping_insert_test.jsp";
    private static final String CVSNET_FTP_HOST = "222.112.221.132";
    private static final int CVSNET_FTP_PORT = 21;
    private static final String CVSNET_FTP_USER = "selluv1";
    private static final String CVSNET_FTP_PASS = "selluv123";
//    private static final String CVSNET_TARGET_FILE_PATH = "/ftproot/selluv/cvs_data/";
    private static final String CVSNET_TARGET_FILE_PATH = "/ftproot/selluv/.profile";
    private HttpClient client = null;
    private Gson gson = new Gson();

    public CvsnetClientHelper() {
        this.client = HttpClientBuilder.create().build();
    }

    private HttpResponse postRequest(String path, StringEntity postData) throws Exception {

        try {

            HttpPost postRequest = new HttpPost(API_URL + path);
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Connection", "keep-alive");
            postRequest.setHeader("Content-Type", "application/x-www-form-urlencoded");

            postRequest.setEntity(postData);

            HttpResponse response = client.execute(postRequest);

            return response;

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }

    public boolean requestReservation(ReservationReq reservationReq) {

        try {
            String reservationReqData = gson.toJson(reservationReq);
            StringEntity data = new StringEntity(reservationReqData, "UTF-8");
            HttpResponse response = this.postRequest("", data);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getFTPFileContent() {
        return getFTPFileContent(false);
    }

    public String getFTPFileContent(boolean isDelete) {
        FTPClient ftpClient = new FTPClient();
        String result = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;

        try {

//            ftpClient.setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 10)));

            ftpClient.connect(CVSNET_FTP_HOST, CVSNET_FTP_PORT);
            ftpClient.login(CVSNET_FTP_USER, CVSNET_FTP_PASS);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            inputStream = ftpClient.retrieveFileStream(CVSNET_TARGET_FILE_PATH);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder resultBuilder = new StringBuilder();
            String lineString;
            while ((lineString = bufferedReader.readLine()) != null) {
                resultBuilder.append(lineString + "\n");
            }

            result = resultBuilder.toString();

            if (isDelete) {
                ftpClient.deleteFile(CVSNET_TARGET_FILE_PATH);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (inputStream != null)
                    inputStream.close();

                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}
