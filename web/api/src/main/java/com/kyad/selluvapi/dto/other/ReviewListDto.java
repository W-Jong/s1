package com.kyad.selluvapi.dto.other;

import com.kyad.selluvapi.dao.ReviewDao;
import com.kyad.selluvapi.dao.UsrFeedDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class ReviewListDto {
    @ApiModelProperty("후기")
    private ReviewDao review;
    @ApiModelProperty("유저")
    private UsrMiniDto peerUsr;
}
