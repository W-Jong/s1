package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.ThemeDao;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ThemeRepository extends CrudRepository<ThemeDao, Integer> {

    @Query("select a from ThemeDao a")
    Page<ThemeDao> findAll(Pageable pageable);

    List<ThemeDao> findAllByKindAndStartTimeBeforeAndEndTimeAfterAndStatus(int kind, Timestamp startTime, Timestamp endTime, int status);

    @Query(value = "SELECT count(*) FROM theme_pdt WHERE theme_uid = ?1", nativeQuery = true)
    int countPdtByThemeUid(int themeUid);


    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?2) AS pdt_like_status," +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM theme_pdt tp JOIN pdt p ON tp.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE tp.theme_uid = ?1 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) " +
            " ORDER BY p.fame_score DESC LIMIT 3",
            nativeQuery = true)
    List<Object> getRecommendedPdtListByThemeUid(int themeUid, int usrUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count," +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?2) AS pdt_like_status," +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img " +
            " FROM theme_pdt tp JOIN pdt p ON tp.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE tp.theme_uid = ?1 AND p.photoshop_yn = 1 AND p.status IN (1,2,3) " +
            " ORDER BY p.edt_time DESC ",
            nativeQuery = true)
    List<Object> getAllPdtListByThemeUid(int themeUid, int usrUid);
}
