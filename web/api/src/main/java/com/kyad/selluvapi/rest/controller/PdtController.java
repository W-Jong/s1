package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.pdt.*;
import com.kyad.selluvapi.dto.usr.UsrFollowListDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import com.kyad.selluvapi.enumtype.FEED_TYPE;
import com.kyad.selluvapi.enumtype.PUSH_TYPE;
import com.kyad.selluvapi.enumtype.SYSTEM_SETTING_TYPE;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.helper.StringHelper;
import com.kyad.selluvapi.request.ContentReq;
import com.kyad.selluvapi.request.PdtCreateReq;
import com.kyad.selluvapi.request.PdtUpdateReq;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.*;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@Api(value = "/pdt", description = "상품관리")
@RequestMapping("/pdt")
public class PdtController {

    private final Logger LOG = LoggerFactory.getLogger(PdtController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private PdtService pdtService;

    @Autowired
    private UsrPdtLikeService usrPdtLikeService;

    @Autowired
    private UsrPdtWishService usrPdtWishService;

    @Autowired
    private ReplyService replyService;

    @Autowired
    private OtherService otherService;

    @Autowired
    private TagWordService tagWordService;

    @Autowired
    private UsrFeedService usrFeedService;

    @Autowired
    private UsrAlarmService usrAlarmService;

    @ApiOperation(value = "좋아요 목록",
            notes = "좋아요 목록을 표시한다.")
    @GetMapping(value = "/like")
    public GenericResponse<Page<PdtListDto>> getLikePdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "할인여부 0-전체, 1-할인된 상품만", allowableValues = "0,1") @RequestParam(value = "sale", defaultValue = "0") int sale,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, usrPdtLikeService.findAllPdtListByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), sale));
    }

    @ApiOperation(value = "좋아요 스타일 목록",
            notes = "좋아요 스타일 목록을 표시한다.")
    @GetMapping(value = "/like/style")
    public GenericResponse<Page<PdtListDto>> getLikePdtStyleList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "할인여부 0-전체, 1-할인된 상품만", allowableValues = "0,1") @RequestParam(value = "sale", defaultValue = "0") int sale,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, usrPdtLikeService.findAllPdtStyleListByUsrUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), sale));
    }

    @ApiOperation(value = "좋아요 업데이트뱃지 표시여부",
            notes = "좋아요 상품이 새로 추가되었는가에 따라 업데이트 여부를 표시한다.")
    @GetMapping(value = "/like/updateStatus")
    public GenericResponse<Boolean> getLikePdtStatus(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        long noReadCount = usrPdtLikeService.countByUnConfirmLikePdt(usr.getUsrUid());

        return new GenericResponse<>(ResponseMeta.OK, (noReadCount > 0));
    }

    @ApiOperation(value = "좋아요 유저 목록얻기",
            notes = "해당 상품을 좋아요한 유저 목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/like")
    public GenericResponse<Page<UsrFollowListDto>> getPdtLikeList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, usrService.getPdtLikedList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), pdtUid));
    }

    @ApiOperation(value = "상품좋아요하기",
            notes = "상품에 대한 좋아요하기")
    @PutMapping(value = "/{pdtUid}/like")
    public GenericResponse<Void> insertLike(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        UsrPdtLikeDao usrPdtLike = usrPdtLikeService.getByUsrUidAndPdtUid(usr.getUsrUid(), pdtUid);
        if (usrPdtLike != null)
            return new GenericResponse<>(ResponseMeta.ALREADY_LIKE_ADDED, null);

        usrPdtLike = new UsrPdtLikeDao();
        usrPdtLike.setUsrUid(usr.getUsrUid());
        usrPdtLike.setPdtUid(pdtUid);
        usrPdtLike.setStatus(2);
        usrPdtLikeService.save(usrPdtLike);

        UsrDao peerUsr = usrService.getByUsrUid(pdt.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH15_LIKE_MY_PDT, peerUsr, usr, null, pdt);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "상품좋아요 취소하기",
            notes = "상품에 대한 좋아요를 취소한다.")
    @DeleteMapping(value = "/{pdtUid}/like")
    public GenericResponse<Void> deleteLike(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        UsrPdtLikeDao usrPdtLike = usrPdtLikeService.getByUsrUidAndPdtUid(usr.getUsrUid(), pdtUid);
        if (usrPdtLike == null)
            return new GenericResponse<>(ResponseMeta.ALREADY_LIKE_DELETED, null);

        usrPdtLikeService.delete(usrPdtLike.getUsrPdtLikeUid());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "잇템 유저 목록얻기",
            notes = "해당 상품을 잇템으로 등록한 유저 목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/wish")
    public GenericResponse<Page<UsrFollowListDto>> getPdtWishList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, usrService.getPdtWishedList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), pdtUid));
    }

    @ApiOperation(value = "상품잇템 추가하기",
            notes = "상품을 잇템으로 등록한다.")
    @PutMapping(value = "/{pdtUid}/wish")
    public GenericResponse<Void> insertWish(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        UsrPdtWishDao usrPdtWish = usrPdtWishService.getByUsrUidAndPdtUid(usr.getUsrUid(), pdtUid);
        if (usrPdtWish == null) {
            usrPdtWish = new UsrPdtWishDao();
            usrPdtWish.setUsrUid(usr.getUsrUid());
            usrPdtWish.setPdtUid(pdtUid);
            usrPdtWishService.save(usrPdtWish);
        }

        usrFeedService.insertFeed(FEED_TYPE.FEED04_FOLLOWING_USER_WISH, usr, pdt);

        UsrDao peerUsr = usrService.getByUsrUid(pdt.getUsrUid());
        usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH16_ITEM_MY_PDT, peerUsr, usr, null, pdt);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

//    @ApiOperation(value = "상품잇템 취소하기",
//            notes = "상품을 잇템에서 취소한다.")
//    @DeleteMapping(value = "/{pdtUid}/wish")
//    public GenericResponse<Void> deleteWish(
//            @RequestHeader(value = "accessToken") String accessToken,
//            @PathVariable(value = "pdtUid") int pdtUid) {
//
//        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);
//
//        PdtDao pdt = pdtService.getPdtInfo(pdtUid);
//
//        UsrPdtWishDao usrPdtWish = usrPdtWishService.getByUsrUidAndPdtUid(usr.getUsrUid(), pdtUid);
//        if (usrPdtWish == null)
//            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);
//
//        usrPdtWishService.delete(usrPdtWish.getUsrPdtWishUid());
//
//        return new GenericResponse<>(ResponseMeta.OK, null);
//    }

    @ApiOperation(value = "상품추가",
            notes = "새로운 상품을 등록한다.")
    @PutMapping(value = "")
    public GenericResponse<PdtDetailDto> putPdt(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid PdtCreateReq pdtCreateReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = new PdtDao();
        pdt.setUsrUid(usr.getUsrUid());
        pdt.setBrandUid(pdtCreateReq.getBrandUid());
        pdt.setPdtGroup(pdtCreateReq.getPdtGroup().getCode());
        pdt.setCategoryUid(pdtCreateReq.getCategoryUid());
        pdt.setPdtSize(pdtCreateReq.getPdtSize());
        pdt.setPhotoshopYn(2);
        pdt.setOriginPrice(pdtCreateReq.getPrice());
        pdt.setPrice(pdtCreateReq.getPrice());
        pdt.setSendPrice(pdtCreateReq.getSendPrice());
        pdt.setPdtCondition(pdtCreateReq.getPdtCondition().getCode());
        pdt.setComponent(pdtCreateReq.getComponent());
        pdt.setEtc(pdtCreateReq.getEtc());
        pdt.setColorName(pdtCreateReq.getPdtColor());
        pdt.setPdtModel(pdtCreateReq.getPdtModel());
        pdt.setPromotionCode(pdtCreateReq.getPromotionCode());
        pdt.setContent(pdtCreateReq.getContent());
        pdt.setTag(pdtCreateReq.getTag());
        pdt.setEdtTime(new Timestamp(System.currentTimeMillis()));
        pdt.setNegoYn(pdtCreateReq.getNegoYn());
        pdt.setStatus(1);

        boolean isProfileImage = false;
        boolean isPhotos = false;
        if (pdtCreateReq.getPhotos() != null && pdtCreateReq.getPhotos().size() > 0) {
            String profileImg = pdtCreateReq.getPhotos().get(0);
            if (profileImg != null && !profileImg.isEmpty()) {
                pdt.setProfileImg(profileImg);
                isProfileImage = true;
            }
            List<String> photoList = new ArrayList<>(pdtCreateReq.getPhotos());
            photoList.remove(0);
            String photos = StringHelper.join(photoList, ",");
            if (!"".equals(photos)) {
                pdt.setPhotos(photos);
                isPhotos = true;
            }
        }

        boolean isSignImage = false;
        if (pdtCreateReq.getSignImg() != null && !pdtCreateReq.getSignImg().isEmpty()) {
            pdt.setSignImg(pdtCreateReq.getSignImg());
            isSignImage = true;
        }
        pdtService.save(pdt);

        if (isProfileImage) {
            StarmanHelper.moveTargetFolder("pdt", pdt.getPdtUid(), pdt.getProfileImg());
        }
        if (isPhotos) {
            List<String> photoList = Arrays.asList(pdt.getPhotos().split(","));
            for (String photo : photoList) {
                StarmanHelper.moveTargetFolder("pdt", pdt.getPdtUid(), StringHelper.trimWhitespace(photo));
            }
        }
        if (isSignImage) {
            StarmanHelper.moveTargetFolder("pdt", pdt.getPdtUid(), pdt.getSignImg());
        }

        if (pdtCreateReq.getTag() != null && !pdtCreateReq.getTag().isEmpty()) {
            String[] tags = pdtCreateReq.getTag().split(" ");
            if (tags.length > 0) {
                for (String tag: tags) {
                    tagWordService.saveTagWord(tag);
                }
            }
        }

        usrFeedService.insertFeed(FEED_TYPE.FEED01_FINDING_PDT, usr, pdt);
        usrFeedService.insertFeed(FEED_TYPE.FEED03_FOLLOWING_BRAND, usr, pdt);
        usrFeedService.insertFeed(FEED_TYPE.FEED06_FOLLOWING_USER_PDT, usr, pdt);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.getPdtDetail(pdt.getPdtUid(), usr.getUsrUid()));
    }

    @ApiOperation(value = "상품정보 수정",
            notes = "상품정보를 수정한다.")
    @PostMapping(value = "/{pdtUid}")
    public GenericResponse<PdtMiniDto> postPdt(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @RequestBody @Valid PdtUpdateReq pdtUpdateReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        if (usr.getUsrUid() != pdt.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        if (pdt.getStatus() == 2) {
            return new GenericResponse<>(ResponseMeta.PDT_ALREADY_SOLD, null);
        }

        pdt.setPdtSize(pdtUpdateReq.getPdtSize());
        pdt.setOriginPrice(pdt.getPrice());
        pdt.setPrice(pdtUpdateReq.getPrice());
        pdt.setSendPrice(pdtUpdateReq.getSendPrice());
        pdt.setPdtCondition(pdtUpdateReq.getPdtCondition().getCode());
        pdt.setComponent(pdtUpdateReq.getComponent());
        pdt.setEtc(pdtUpdateReq.getEtc());
        pdt.setColorName(pdtUpdateReq.getPdtColor());
        pdt.setPdtModel(pdtUpdateReq.getPdtModel());
        pdt.setPromotionCode(pdtUpdateReq.getPromotionCode());
        pdt.setContent(pdtUpdateReq.getContent());
        pdt.setTag(pdtUpdateReq.getTag());
        pdt.setEdtTime(new Timestamp(System.currentTimeMillis()));
        pdt.setNegoYn(pdtUpdateReq.getNegoYn());
        pdt.setStatus(pdtUpdateReq.getIsSell() ? 1 : 3);

        boolean isProfileImage = false;
        boolean isPhotos = false;
        if (pdtUpdateReq.getPhotos() != null && pdtUpdateReq.getPhotos().size() > 0) {
            String profileImg = pdtUpdateReq.getPhotos().get(0);
            if (profileImg != null && !profileImg.isEmpty()) {
                pdt.setProfileImg(profileImg);
                isProfileImage = true;
            }
            List<String> photoList = new ArrayList<>(pdtUpdateReq.getPhotos());
            photoList.remove(0);
            String photos = StringHelper.join(photoList, ",");
            if (!"".equals(photos)) {
                pdt.setPhotos(photos);
                isPhotos = true;
            }

//            pdt.setPhotoshopYn(2);
        }

        pdtService.save(pdt);

        if (isProfileImage) {
            StarmanHelper.moveTargetFolder("pdt", pdt.getPdtUid(), pdt.getProfileImg());
        }
        if (isPhotos) {
            List<String> photoList = Arrays.asList(pdt.getPhotos().split(","));
            for (String photo : photoList) {
                StarmanHelper.moveTargetFolder("pdt", pdt.getPdtUid(), StringHelper.trimWhitespace(photo));
            }
        }

        if (pdt.getOriginPrice() > pdt.getPrice()) {
            usrFeedService.insertFeed(FEED_TYPE.FEED02_LIKE_PDT_SALE, usr, pdt);
        }

        if (pdtUpdateReq.getTag() != null && !pdtUpdateReq.getTag().isEmpty()) {
            String[] tags = pdtUpdateReq.getTag().split(" ");
            if (tags.length > 0) {
                for (String tag: tags) {
                    tagWordService.saveTagWord(tag);
                }
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, pdtService.convertPdtDaoToPdtMiniDto(pdt));
    }

    @ApiOperation(value = "상품상세",
            notes = "상품 상세정보를 얻는다.")
    @GetMapping(value = "/{pdtUid}")
    public GenericResponse<PdtDetailDto> getPdt(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.getPdtDetail(pdtUid, usr.getUsrUid()));
    }

    @ApiOperation(value = "상품삭제",
            notes = "상품을 삭제한다.")
    @DeleteMapping(value = "/{pdtUid}")
    public GenericResponse<Void> deletePdt(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        if (pdt.getUsrUid() != usr.getUsrUid()) {
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);
        }

        pdtService.removePdtAndReference(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "상품상세 - 추천상품목록얻기",
            notes = "상품상세페지에 표시 할 추천상품 목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/recommended")
    public GenericResponse<Page<PdtListDto>> getPdtRecommended(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.findAllByBrandUidOrCategory2(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), pdt));
    }

    @ApiOperation(value = "상품상세 - 연관스타일 목록얻기",
            notes = "상품과 연관되어 있는 스타일 목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/relationStyle")
    public GenericResponse<Page<PdtListDto>> getPdtStyleRelated(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.findAllStyleListByBrandUidOrCateory2(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), pdt));
    }

    @ApiOperation(value = "상품상세 - 동일한 모델명 상품목록얻기",
            notes = "해당상품 모델과 동일한 모델명을 가진 상품목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/sameModel")
    public GenericResponse<Page<PdtListDto>> getPdtSameModel(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.findAllByPdtModel(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), pdt));
    }

    @ApiOperation(value = "상품상세 - 동일한 태그 상품목록얻기",
            notes = "해당태그와 동일한 태그를 가진 상품목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/sameTag")
    public GenericResponse<Page<PdtListDto>> getPdtSameModel(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @RequestParam(value = "tag") String tag,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, pdtService.findAllByTag(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), tag, pdt));
    }

    @ApiOperation(value = "상품 댓글 목록얻기",
            notes = "해당 상품에 달린 댓글목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/reply")
    public GenericResponse<Page<ReplyDto>> getPdtReplyList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam("페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, replyService.findAllByPdtUid(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), pdtUid));
    }

    @ApiOperation(value = "상품 댓글 추가하기",
            notes = "해당 상품에 댓글을 추가한다.")
    @PutMapping(value = "/{pdtUid}/reply")
    public GenericResponse<ReplyDto> getPdtReplyList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @RequestBody @Valid ContentReq contentReq) {

        if (contentReq == null || contentReq.getContent() == null || contentReq.getContent().isEmpty()) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        String targetUids = "";
        String content = contentReq.getContent();
        Matcher matcher = Pattern.compile("@([ㄱ-ㅎㅏ-ㅣ가-힣a-zA-Z0-9]+)").matcher(content);
        while (matcher.find()) {
            String nickName = matcher.group().substring(1);
            if (nickName.equals(usr.getUsrNckNm())) {
                targetUids += usr.getUsrUid() + ",";
            } else {
                UsrDao mentionUsrDao = usrService.getMentionedUser(usr.getUsrUid(), pdtUid, nickName);

                if (mentionUsrDao == null) {
                    return new GenericResponse<>(ResponseMeta.INVALID_NICKNAME, null);
                }

                targetUids += (mentionUsrDao == null) ? "0," : mentionUsrDao.getUsrUid() + ",";
                if (mentionUsrDao != null) {
                    //댓글 알림
                    usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH26_MENTIONED_ME, mentionUsrDao, usr, null, pdt);
                }
            }
        }
        if (targetUids.length() > 0) {
            targetUids = targetUids.substring(0, targetUids.length() - 1);
        }

        if (usr.getUsrUid() != pdt.getUsrUid()) {
            //판매자 댓글 알림
            UsrDao sellerUsr = usrService.getByUsrUid(pdt.getUsrUid());
            usrAlarmService.insertUsrAlarm(PUSH_TYPE.PUSH24_PDT_REPLY, sellerUsr, usr, null, pdt);
        }

        ReplyDao reply = new ReplyDao();
        reply.setUsrUid(usr.getUsrUid());
        reply.setPdtUid(pdtUid);
        reply.setContent(content);
        reply.setTargetUids(targetUids);
        replyService.save(reply);

        return new GenericResponse<>(ResponseMeta.OK, replyService.convertReplyDaoAndUsrDaoToReplyDto(reply, usr));
    }

    @ApiOperation(value = "상품 댓글가능 회원목록얻기",
            notes = "해당 상품에 댓글을 작성할 때 언급가능한 회원목록을 얻는다.")
    @GetMapping(value = "/{pdtUid}/reply/search")
    public GenericResponse<List<UsrMiniDto>> getPdtReplyMentionUserList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @ApiParam("검색어") @RequestParam(value = "keyword", defaultValue = "", required = false) String keyword) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        return new GenericResponse<>(ResponseMeta.OK, usrService.getMentionedUserList(usr.getUsrUid(), pdtUid, keyword));
    }

    @ApiOperation(value = "댓글 삭제",
            notes = "댓글을 삭제한다.")
    @DeleteMapping(value = "/reply/{replyUid}")
    public GenericResponse<Void> deletePdtReply(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "replyUid") int replyUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        ReplyDao reply = replyService.findOne(replyUid);

        if (reply == null || reply.getStatus() == 0)
            return new GenericResponse<>(ResponseMeta.ALREADY_DELETED, null);

        if (usr.getUsrUid() != reply.getUsrUid())
            return new GenericResponse<>(ResponseMeta.PERMITTION_NOT_ALLOWED, null);

        replyService.delete(replyUid);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "상품 가품신고 정보얻기",
            notes = "해당 상품의 가품신고 관련 정보를 얻는다.")
    @GetMapping(value = "/{pdtUid}/fake")
    public GenericResponse<PdtFakeInfoDto> getPdtFakeInfo(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        PdtFakeInfoDto fakeInfo = new PdtFakeInfoDto();
        fakeInfo.setReward(Long.parseLong(
                otherService.getSystemSetting(SYSTEM_SETTING_TYPE.FAKE_REWARD.getCode()).getSetting()));
        fakeInfo.setTotalCount(pdtService.getFakeReportCountByPdtUid(pdtUid));

        return new GenericResponse<>(ResponseMeta.OK, fakeInfo);
    }

    @ApiOperation(value = "상품 가품신고 하기",
            notes = "해당 상품을 가품으로 신고한다.")
    @PutMapping(value = "/{pdtUid}/fake")
    public GenericResponse<Void> putPdtFake(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "pdtUid") int pdtUid,
            @RequestBody @Valid ContentReq contentReq) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        PdtDao pdt = pdtService.getPdtInfo(pdtUid);

        ReportPdtDao reportPdt = new ReportPdtDao();
        reportPdt.setUsrUid(usr.getUsrUid());
        reportPdt.setPdtUid(pdtUid);
        reportPdt.setRank(pdtService.getFakeReportCountByPdtUid(pdtUid) + 1);
        reportPdt.setContent(contentReq.getContent());

        pdtService.saveReportPdt(reportPdt);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }
}
