package com.kyad.selluvapi.dao;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "pdt_style", schema = "selluv")
public class PdtStyleDao {
    @ApiModelProperty("상품스타일UID")
    private int pdtStyleUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("상품UID")
    private int pdtUid;
    @ApiModelProperty("유저UID")
    private int usrUid;
    @ApiModelProperty("스타일사진")
    private String styleImg;
    private int status = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pdt_style_uid", nullable = false)
    public int getPdtStyleUid() {
        return pdtStyleUid;
    }

    public void setPdtStyleUid(int pdtStyleUid) {
        this.pdtStyleUid = pdtStyleUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "pdt_uid", nullable = false)
    public int getPdtUid() {
        return pdtUid;
    }

    public void setPdtUid(int pdtUid) {
        this.pdtUid = pdtUid;
    }

    @Basic
    @Column(name = "usr_uid", nullable = false)
    public int getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(int usrUid) {
        this.usrUid = usrUid;
    }

    @Basic
    @Column(name = "style_img", nullable = false, length = 255)
    public String getStyleImg() {
        return styleImg;
    }

    public void setStyleImg(String styleImg) {
        this.styleImg = styleImg;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PdtStyleDao that = (PdtStyleDao) o;
        return pdtStyleUid == that.pdtStyleUid &&
                pdtUid == that.pdtUid &&
                usrUid == that.usrUid &&
                status == that.status &&
                Objects.equals(regTime, that.regTime) &&
                Objects.equals(styleImg, that.styleImg);
    }

    @Override
    public int hashCode() {

        return Objects.hash(pdtStyleUid, regTime, pdtUid, usrUid, styleImg, status);
    }
}
