package com.kyad.selluvapi.helper;

import com.ibm.icu.util.ChineseCalendar;

import java.util.Calendar;
import java.util.Date;

public class DateCalculatorHelper {
    private Calendar cal;
    private ChineseCalendar chinaCal;
    private String[] solarArr;
    private String[] lunarArr;

    public DateCalculatorHelper() {
        cal = Calendar.getInstance();
        chinaCal = new ChineseCalendar();
        solarArr = new String[]{"01-01", "03-01", "05-05", "06-06", "08-15", "12-25"};
        lunarArr = new String[]{"01-01", "01-02", "04-08", "08-14", "08-15", "08-16"};
    }

    /**
     * 해당일자가 음력 법정공휴일에 해당하는 지 확인
     */
    private boolean isHolidayLunar(String date) {
        boolean result = false;
        cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(date.substring(5, 7)) - 1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(8)));
        chinaCal.setTimeInMillis(cal.getTimeInMillis());

        /** 음력으로 변환된 월과 일자 **/
        int mm = chinaCal.get(ChineseCalendar.MONTH) + 1;
        int dd = chinaCal.get(ChineseCalendar.DAY_OF_MONTH);

        StringBuilder sb = new StringBuilder();

        if (mm < 10) sb.append("0");
        sb.append(mm);

        sb.append("-");

        if (dd < 10) sb.append("0");
        sb.append(dd);

        /** 음력 12월의 마지막날 (설날 첫번째 휴일)인지 확인 **/
        if (mm == 12) {
            int lastDate = chinaCal.getActualMaximum(ChineseCalendar.DAY_OF_MONTH);
            if (dd == lastDate) {
                return true;
            }
        }

        for (String d : lunarArr) {
            if (sb.toString().equals(d)) {
                return true;
            }
        }

        return result;
    }

    /**
     * 해당일자가 양력 법정공휴일에 해당하는 지 확인
     */
    private boolean isHolidaySolar(String date) {
        boolean result = false;

        if (date != null && (date.equals("") || date.length() > 8)) {
            date = new java.text.SimpleDateFormat("MM-dd").format(cal.getTime());
        } else {
            date = date.substring(5);
        }

        for (String d : solarArr) {
            if (date.equals(d)) {
                return true;
            }
        }

        return result;
    }

    /**
     * 해당일자가 토, 일요일인지 확인
     */
    private boolean isSaturdayOrSunday(String date) {
        boolean result = false;
        cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(date.substring(5, 7)) - 1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(8)));
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ||
                cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            result = true;
        }

        return result;
    }

    public Date getDelayedWorkingDate(int delayDate, Date targetDate) {

        int addDay = (delayDate >= 0) ? 1 : -1;
        String date = "";
        if (targetDate == null) {
            cal.setTime(new java.util.Date(System.currentTimeMillis()));
        } else {
            cal.setTime(targetDate);
        }
        int dayCount = delayDate == 0 ? 1 : Math.abs(delayDate);

        for (int i = 0; i < dayCount; i++) {
            cal.add(Calendar.DATE, addDay);
            date = new java.text.SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            boolean result = true;
            /** 양력 법정공휴일부터 확인 **/
            result = isHolidaySolar(date);
            /** 양력 법정공휴일에 해당하지 않으면 **/
            if (!result) {
                /** 음력 법정공휴일에 해당하는 지 확인 **/
                result = isHolidayLunar(date);
                /** 음력 공휴일에 해당하면 **/
                if (result) {
                    /** 재조정된 값이 음력, 양력 공휴일 및 일요일에 해당하지 않을 때까지 루프 **/
                    while (result) {
                        cal.add(Calendar.DATE, addDay);
                        date = new java.text.SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
                        result = isHolidayLunar(date);

                        /** 음력 공휴일에도 해당하지 않을 경우
                         * 이 때의 date가 일요일 및 양력 공휴일에 해당하는 지 한번 더 확인하고 일요일인지도 확인
                         *  **/
                        if (!result) {
                            result = isHolidaySolar(date);
                        }

                        if (!result) {
                            result = isSaturdayOrSunday(date);
                        }
                    }
                    /** 양력 및 음력 공휴일에 해당하지 않으면 **/
                } else {
                    /** 양력 및 음력 공휴일에 해당하지 않으므로 일요일인지 확인 **/
                    result = isSaturdayOrSunday(date);
                    if (result) {
                        /** 재조정된 값이 음력, 양력 공휴일 및 일요일에 해당하지 않을 때까지 루프 **/
                        while (result) {
                            cal.add(Calendar.DATE, addDay);
                            date = new java.text.SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
                            result = isSaturdayOrSunday(date);
                            /** 재조정된 값이 양력 및 음력 공휴일에 해당하는 지 확인 **/
                            if (!result) {
                                result = isHolidaySolar(date);
                            }
                            if (!result) {
                                result = isHolidayLunar(date);
                            }
                        }
                    }
                }
            } else {    // 양력 공휴일에 해당하면
                /** 재조정된 값이 음력, 양력 공휴일 및 일요일에 해당하지 않을 때까지 루프 **/
                while (result) {
                    cal.add(Calendar.DATE, addDay);
                    date = new java.text.SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
                    result = isHolidaySolar(date);

                    if (!result) {
                        result = isHolidaySolar(date);
                    }

                    if (!result) {
                        result = isSaturdayOrSunday(date);
                    }
                }
            }
        }

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public Date getDelayedWorkingDate(int delayDate) {
        return getDelayedWorkingDate(delayDate, null);
    }
}
