package com.kyad.selluvapi.request;

import com.kyad.selluvapi.enumtype.PDT_CONDITION_TYPE;
import com.kyad.selluvapi.enumtype.SEARCH_ORDER_TYPE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
public class SearchReq {
    @ApiModelProperty("검색키워드")
    @NotNull
    private String keyword = "";

    @ApiModelProperty("정렬")
    @NotNull
    private SEARCH_ORDER_TYPE searchOrderType = SEARCH_ORDER_TYPE.RECOMMEND;

    @ApiModelProperty("카테고리UID, 전체인 경우 0")
    @NotNull
    private int categoryUid = 0;

    @ApiModelProperty("브랜드UID 리스트, 전체인 경우 빈리스트")
    @NotNull
    @Size(min = 0)
    private List<Integer> brandUidList = new ArrayList<>();

    @ApiModelProperty("사이즈 리스트, 전체인 경우 빈리스트")
    @NotNull
    @Size(min = 0)
    private List<String> pdtSizeList = new ArrayList<>();

    @ApiModelProperty("컨디션 리스트, 전체인 경우 빈리스트, NEW, BEST, GOOD, MIDDLE 만 해당")
    @NotNull
    @Size(min = 0)
    private List<PDT_CONDITION_TYPE> pdtConditionList = new ArrayList<>();

    @ApiModelProperty("컬러 리스트, 전체인 경우 빈리스트")
    @NotNull
    @Size(min = 0)
    private List<String> pdtColorList = new ArrayList<>();

    @ApiModelProperty("가격 낮은 값, -1이면 제한없음")
    @Range(min = -1)
    private long pdtPriceMin = -1;

    @ApiModelProperty("가격 높은 값, -1이면 제한없음")
    @Range(min = -1)
    private long pdtPriceMax = -1;

    @ApiModelProperty("품절 상품 제외여부")
    @NotNull
    private boolean isSoldOutExpect = false;
}
