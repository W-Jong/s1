package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.TagWordDao;
import com.kyad.selluvapi.dto.search.SearchWordDto;
import com.kyad.selluvapi.jpa.SearchWordRepository;
import com.kyad.selluvapi.jpa.TagWordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class TagWordService {

    @Autowired
    private TagWordRepository tagWordRepository;

    public Page<TagWordDao> findAll(Pageable pageable) {
        return tagWordRepository.findAll(pageable);
    }

    public TagWordDao save(TagWordDao dao) {
        return tagWordRepository.save(dao);
    }

    public TagWordDao saveTagWord(String content) {
        if (content == null || content.isEmpty() || content.trim().isEmpty())
            return null;

        String tagStr = content.trim().replaceAll("#", "");

        TagWordDao tagWord = tagWordRepository.findFirstByContent(tagStr);
        if (tagWord == null)
            tagWord = new TagWordDao();

        tagWord.setContent(tagStr);
        tagWord.setPdtCount(tagWord.getPdtCount() + 1);
        tagWord.setEdtTime(new Timestamp(System.currentTimeMillis()));

        return tagWordRepository.save(tagWord);
    }

    public List<String> getListByKeyword(String keyword) {
        return tagWordRepository.getByKeyword(keyword);
    }

    public List<String> getListByPrefixKeyword(String keyword) {
        return tagWordRepository.getByPrefixKeyword(keyword);
    }
}
