package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.rest.common.exception.*;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = UserNotFoundException.class)
    public GenericResponse<Void> handleUserNotFoundExceptionError(UserNotFoundException e) {
        return new GenericResponse<Void>(ResponseMeta.USER_ERROR_ACCESS_TOKEN, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = UserTempNotPermissionException.class)
    public GenericResponse<Void> handleUserTempNotPermissionExceptionError(UserTempNotPermissionException e) {
        return new GenericResponse<Void>(ResponseMeta.USER_TEMP_NOT_PERMISSION, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = PdtNotFoundException.class)
    public GenericResponse<Void> handlePdtNotFoundExceptionError(PdtNotFoundException e) {
        return new GenericResponse<Void>(ResponseMeta.PDT_NOT_EXIST, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = BrandNotFoundException.class)
    public GenericResponse<Void> handleBrandNotFoundExceptionError(BrandNotFoundException e) {
        return new GenericResponse<Void>(ResponseMeta.BRAND_NOT_EXIST, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = PdtStyleNotFoundException.class)
    public GenericResponse<Void> handlePdtStyleNotFoundExceptionError(PdtStyleNotFoundException e) {
        return new GenericResponse<Void>(ResponseMeta.PDT_STYLE_NOT_EXIST, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = ThemeNotFoundException.class)
    public GenericResponse<Void> handleThemeNotFoundExceptionError(ThemeNotFoundException e) {
        return new GenericResponse<Void>(ResponseMeta.THEME_NOT_EXIST, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = DealNotFoundException.class)
    public GenericResponse<Void> handleDealNotFoundExceptionError(DealNotFoundException e) {
        return new GenericResponse<Void>(ResponseMeta.DEAL_NOT_FOUND, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = BindException.class)
    public GenericResponse<Void> handleBindExceptionError(BindException e) {
        BindingResult bindingResult = e.getBindingResult();
        FieldError fieldError = bindingResult.getFieldError();
        return new GenericResponse<Void>(
                new ResponseMeta(900, "잘못된 요청입니다.", "[" + fieldError.getField() + "] : "
                        + fieldError.getDefaultMessage()), null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public GenericResponse<Void> handleMethodArgumentNotValidExceptionError(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        FieldError fieldError = bindingResult.getFieldError();
        return new GenericResponse<Void>(
                new ResponseMeta(900, "잘못된 요청입니다.", "[" + fieldError.getField() + "] : "
                        + fieldError.getDefaultMessage()), null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = {HttpMessageNotReadableException.class, ServletRequestBindingException.class,
            MethodArgumentTypeMismatchException.class, NoHandlerFoundException.class})
    public GenericResponse<Void> handleJsonMappingExceptionError(Exception e) {
        return new GenericResponse<Void>(
                new ResponseMeta(900, "잘못된 요청입니다.", e.getMessage()), null);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = Exception.class)
    public GenericResponse<Void> handleExceptionInternalError(Exception e) {
        return new GenericResponse<Void>(
                new ResponseMeta(910, "서버오류입니다", e.getMessage()), null);
    }

}
