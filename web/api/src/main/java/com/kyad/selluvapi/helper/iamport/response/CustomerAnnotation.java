package com.kyad.selluvapi.helper.iamport.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.rest.common.serializer.GsonUnixTimestampDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.UnixTimestampDeserializer;
import com.kyad.selluvapi.rest.common.serializer.UnixTimestampSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class CustomerAnnotation {
    @SerializedName("customer_uid")
    String customer_uid;

    @SerializedName("card_name")
    String card_name;

    @SerializedName("customer_name")
    String customer_name;

    @SerializedName("customer_email")
    String customer_email;

    @SerializedName("customer_addr")
    String customer_addr;

    @SerializedName("customer_postcode")
    String customer_postcode;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("inserted")
    Timestamp inserted;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("updated")
    Timestamp updated;
}
