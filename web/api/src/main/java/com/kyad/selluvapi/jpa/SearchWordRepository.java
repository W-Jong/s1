package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.SearchWordDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchWordRepository extends CrudRepository<SearchWordDao, Integer> {

    @Query("select a from SearchWordDao a")
    Page<SearchWordDao> findAll(Pageable pageable);

    @Query(value = "SELECT sw.content, (SELECT count(*) FROM search_word sw2 WHERE sw2.content = sw.content) AS total_cnt, count(*) AS week_cnt " +
            " FROM search_word sw WHERE reg_time > (NOW() - INTERVAL 1 WEEK) GROUP BY content ORDER BY reg_time DESC LIMIT 10",
            nativeQuery = true)
    List<Object> findAllFameWords();
}
