package com.kyad.selluvapi.dto.pdt;

import com.kyad.selluvapi.dao.PdtStyleDao;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.category.CategoryMiniDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PdtListDto {
    @ApiModelProperty("상품")
    private PdtMiniDto pdt;
    @ApiModelProperty("등록유저")
    private UsrMiniDto usr;
    @ApiModelProperty("브랜드")
    private BrandMiniDto brand;
    @ApiModelProperty("카테고리")
    private CategoryMiniDto category;
    @ApiModelProperty("스타일")
    private PdtStyleDao pdtStyle;
    @ApiModelProperty("상품좋아요 갯수")
    private int pdtLikeCount;
    @ApiModelProperty("유저 상품좋아요 상태")
    private Boolean pdtLikeStatus;
    @ApiModelProperty("상품프로필이미지 너비")
    private int pdtProfileImgWidth = 1;
    @ApiModelProperty("상품프로필이미지 높이")
    private int pdtProfileImgHeight = 1;
    @ApiModelProperty("스타일프로필이미지 너비")
    private int styleProfileImgWidth = 1;
    @ApiModelProperty("스타일프로필이미지 높이")
    private int styleProfileImgHeight = 1;
}
