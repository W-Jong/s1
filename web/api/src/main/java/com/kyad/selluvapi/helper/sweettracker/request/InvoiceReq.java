package com.kyad.selluvapi.helper.sweettracker.request;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class InvoiceReq {
    @SerializedName("t_code")
    private String tCode;
    @SerializedName("t_invoice")
    private String tInvoice;

    public String getUriString(){
        return "t_code=" + tCode + "&t_invoice=" + tInvoice;
    }
}