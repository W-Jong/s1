package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.*;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.usr.UsrDetailDto;
import com.kyad.selluvapi.dto.usr.UsrFollowListDto;
import com.kyad.selluvapi.dto.usr.UsrMiniDto;
import com.kyad.selluvapi.dto.usr.UsrRecommendDto;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.jpa.*;
import com.kyad.selluvapi.rest.common.exception.UserNotFoundException;
import com.kyad.selluvapi.rest.common.exception.UserTempNotPermissionException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UsrService {

    @Autowired
    private PdtService pdtService;

    @Autowired
    private UsrRepository usrRepository;

    @Autowired
    private UsrLoginHisRepository usrLoginHisRepository;

    @Autowired
    private UsrPushSettingRepository usrPushSettingRepository;

    @Autowired
    private UsrAddressRepository usrAddressRepository;

    @Autowired
    private UsrCardRepository usrCardRepository;

    @Autowired
    private EntityManager entityManager;

    public boolean isSpecialCharContains(String target) {
        return (target.contains("#") ||
                target.contains(",") ||
                target.contains("\"") ||
                target.contains("'") ||
                target.contains("`"));
    }

    public UsrDao getUserInfo(String accessToken) {
        UsrDao usr = usrRepository.getByAccessToken(accessToken);
        if (usr == null || (usr.getStatus() != 1 && usr.getUsrUid() > 0)) {
            throw new UserNotFoundException();
        }

        return usr;
    }

    public UsrDao getUserInfoNotTemp(String accessToken) {
        UsrDao usr = getUserInfo(accessToken);

        if (usr.getUsrUid() <= 0) {
            throw new UserTempNotPermissionException();
        }

        return usr;
    }

    public UsrDao getByUsrIdAndPassword(String usrId, String usrPwd) {
        return usrRepository.getByUsrIdAndUsrPwd(usrId, usrPwd);
    }

    public UsrDao getBySnsIdAndLoginType(String snsId, int loginType) {
        return usrRepository.getTopBySnsIdAndUsrLoginType(snsId, loginType);
    }

    public UsrDao getByUsrMail(String email) {
        return usrRepository.getByUsrMail(email);
    }

    public UsrDao getByUsrPhone(String usrPhone) {
        return usrRepository.getByUsrPhone(usrPhone);
    }

    @Transactional
    public List<UsrAddressDao> getAddressListByUsrUid(int usrUid) {
        List<UsrAddressDao> addressList = usrAddressRepository.getAddressListByUsrUid(usrUid);
        if (addressList == null || addressList.size() < 3) {
            for (int i = 0; i < 3; i++) {
                UsrAddressDao addressDao = getAddressByUsrUidAndOrder(usrUid, i + 1);
                if (addressDao == null) {
                    addressDao = new UsrAddressDao();
                    addressDao.setUsrUid(usrUid);
                    addressDao.setAddressOrder(i + 1);

                    usrAddressRepository.save(addressDao);
                }
            }
            addressList = usrAddressRepository.getAddressListByUsrUid(usrUid);
        }
        return addressList;
    }

    public UsrAddressDao getAddressByUsrUidAndOrder(int usrUid, int addressOrder) {
        return usrAddressRepository.findByUsrUidAndAddressOrder(usrUid, addressOrder);
    }

    public UsrAddressDao saveAddressDao(UsrAddressDao dao) {
        return usrAddressRepository.save(dao);
    }

    public void saveUsrAddress(int usrUid, int addressOrder, String addressName, String addressPhone, String addressDetail, String addressDetailSub, int selectionIndex) {
        UsrAddressDao usrAddress = getAddressByUsrUidAndOrder(usrUid, addressOrder);
        if (usrAddress == null) {
            usrAddress = new UsrAddressDao();
            usrAddress.setUsrUid(usrUid);
            usrAddress.setAddressOrder(addressOrder);
        }
        usrAddress.setAddressName(addressName);
        usrAddress.setAddressPhone(addressPhone);
        usrAddress.setAddressDetail(addressDetail);
        usrAddress.setAddressDetailSub(addressDetailSub);
        usrAddress.setStatus((addressOrder == selectionIndex) ? 2 : 1);

        saveAddressDao(usrAddress);
    }

    public UsrAddressDao getAddressActiveByUsrUid(int usrUid) {
        return usrAddressRepository.findActiveAddressByUsrUid(usrUid);
    }

    public UsrDao getByUsrId(String usrId) {
        return usrRepository.getByUsrId(usrId);
    }

    public UsrDao getByUsrUid(int usrUid) {
        return usrRepository.findOne(usrUid);
    }

    public UsrDao save(UsrDao dao) {
        return usrRepository.save(dao);
    }

    public long countPdtByUsrUid(int usrUid) {
        return usrRepository.countPdtByUsrUid(usrUid);
    }

    public UsrDetailDto convertUsrDaoToUsrDetailDto(UsrDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        UsrDetailDto dto = modelMapper.map(dao, UsrDetailDto.class);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("usr", dao.getUsrUid(), dao.getProfileImg()));
        dto.setProfileBackImg(StarmanHelper.getTargetImageUrl("usr", dao.getUsrUid(), dao.getProfileBackImg()));

        return dto;
    }

    public UsrMiniDto convertUsrDaoToUsrMiniDto(UsrDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        UsrMiniDto dto = modelMapper.map(dao, UsrMiniDto.class);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("usr", dao.getUsrUid(), dao.getProfileImg()));

        return dto;
    }

    public UsrLoginHisDao saveUsrLoginHis(UsrLoginHisDao dao) {
        return usrLoginHisRepository.save(dao);
    }

    public boolean isUsrIdDuplicated(String usrId) {
        return usrRepository.getCountByUsrId(usrId) > 0;
    }

    public boolean isEmailDuplicated(String email, int usrUid) {
        return usrRepository.getCountByEmail(email, usrUid) > 0;
    }

    public boolean isPersonDuplicated(String usrNm, int gender, LocalDate birthday, int usrUid) {
        return usrRepository.getCountByUsrNmAndGenderAndBirthdayAndNotUsrUid(usrNm, gender, birthday, usrUid) > 0;
    }

    public UsrPushSettingDao savePushSetting(UsrPushSettingDao dao) {
        return usrPushSettingRepository.save(dao);
    }

    public UsrPushSettingDao getPushSetting(int usrUid) {
        return usrPushSettingRepository.findOne(usrUid);
    }

    public Page<UsrMiniDto> getBlockList(Pageable pageable, int usrUid) {
        return usrRepository.findAllBlocked(pageable, usrUid).map(this::convertUsrDaoToUsrMiniDto);
    }

    public Page<UsrFollowListDto> getFollowingList(Pageable pageable, int usrUid, int peerUsrUid) {
        return usrRepository.findAllFollowing(pageable, usrUid, peerUsrUid)
                .map(result -> convertObjectToUsrFollowListDto((Object[]) result));
    }

    public Page<UsrFollowListDto> getFollowerList(Pageable pageable, int usrUid, int peerUsrUid) {
        return usrRepository.findAllFollower(pageable, usrUid, peerUsrUid)
                .map(result -> convertObjectToUsrFollowListDto((Object[]) result));
    }

    public Page<UsrFollowListDto> getPdtLikedList(Pageable pageable, int usrUid, int pdtUid) {
        return usrRepository.findAllPdtLiked(pageable, usrUid, pdtUid)
                .map(result -> convertObjectToUsrFollowListDto((Object[]) result));
    }

    public Page<UsrFollowListDto> getPdtWishedList(Pageable pageable, int usrUid, int pdtUid) {
        return usrRepository.findAllPdtWished(pageable, usrUid, pdtUid)
                .map(result -> convertObjectToUsrFollowListDto((Object[]) result));
    }

    public UsrFollowListDto convertObjectToUsrFollowListDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        UsrFollowListDto dto = new UsrFollowListDto();
        dto.setUsrUid((int) objects[i++]);
        dto.setUsrId((String) objects[i++]);
        dto.setUsrNckNm((String) objects[i++]);
        dto.setProfileImg(StarmanHelper.getTargetImageUrl("usr", dto.getUsrUid(), (String) objects[i++]));
        dto.setUsrLikeStatus(((BigInteger) objects[i++]).intValue() > 0);

        return dto;
    }

    public List<UsrMiniDto> getMentionedUserList(int usrUid, int pdtUid, String keyword) {
        List<UsrDao> usrDaoList = usrRepository.getMentionedUserListByUsrUidAndPdtUid(usrUid, pdtUid, keyword);

        List<UsrMiniDto> dtoList = new ArrayList<>();
        for (UsrDao dao : usrDaoList) {
            dtoList.add(convertUsrDaoToUsrMiniDto(dao));
        }

        return dtoList;
    }

    public UsrDao getMentionedUser(int usrUid, int pdtUid, String usrNckNm) {
        return usrRepository.getMentionedUserByUsrUidAndPdtUid(usrUid, pdtUid, usrNckNm);
    }

    public Page<PdtListDto> getPdtListByUsrUid(Pageable pageable, int usrUid, int peerUsrUid) {
        return usrRepository.findAllPdtByUsrUid(pageable, usrUid, peerUsrUid,
                (usrUid == peerUsrUid) ? Arrays.asList(1, 2, 3, 4) : Arrays.asList(1, 2, 4))
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> getPdtWishListByUsrUid(Pageable pageable, int usrUid, int peerUsrUid) {
        return usrRepository.findAllPdtWishByUsrUid(pageable, usrUid, peerUsrUid,
                (usrUid == peerUsrUid) ? Arrays.asList(1, 2, 3, 4) : Arrays.asList(1, 2, 4))
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public Page<PdtListDto> getPdtStyleListByUsrUid(Pageable pageable, int usrUid, int peerUsrUid) {
        return usrRepository.findAllPdtStyleByUsrUid(pageable, usrUid, peerUsrUid,
                (usrUid == peerUsrUid) ? Arrays.asList(1, 2, 3, 4) : Arrays.asList(1, 2, 4))
                .map(result -> pdtService.convertListObjectToPdtListDto((Object[]) result));
    }

    public List<UsrCardDao> findAllCardListByUsrUid(int usrUid) {
        return usrCardRepository.findAllByUsrUid(usrUid);
    }

    public UsrCardDao findActiveCardByUsrUid(int usrUid) {
        return usrCardRepository.findActiveCardByUsrUid(usrUid);
    }

    public UsrCardDao findOneUsrCard(int usrCardUid) {
        return usrCardRepository.findOne(usrCardUid);
    }

    public UsrCardDao saveUsrCard(UsrCardDao dao) {
        return usrCardRepository.save(dao);
    }

    @Transactional
    public void deleteUsrCard(int usrCardUid) {
        usrCardRepository.delete(usrCardUid);
    }

    public List<UsrDao> findAllActiveUser() {
        return usrRepository.findAllActiveUser();
    }

    public Page<UsrRecommendDto> findAllRecommendListByUsrUid(Pageable pageable, int usrUid, int pdtGroup) {
        return usrRepository.findAllRecommendListByUsrUid(pageable, usrUid, pdtGroup).map(result -> {
            if (result == null)
                return null;

            Object[] objects = (Object[]) result;
            int i = 0;

            UsrRecommendDto recommendDto= new UsrRecommendDto();
            recommendDto.setUsrUid((int) objects[i++]);
            recommendDto.setUsrId((String) objects[i++]);
            recommendDto.setUsrNckNm((String) objects[i++]);
            recommendDto.setProfileImg(StarmanHelper.getTargetImageUrl("usr", recommendDto.getUsrUid(), (String) objects[i++]));
            recommendDto.setUsrFollowerCount(((BigInteger) objects[i++]).longValue());
            recommendDto.setUsrLikeStatus(((BigInteger) objects[i++]).longValue() > 0);
            recommendDto.setPdtCount(((BigInteger) objects[i++]).longValue());
            recommendDto.setPdtList(pdtService.getThreeRecommendPdtByUsrUid(recommendDto.getUsrUid()));

            return recommendDto;
        });
    }

    @Transactional
    public void updatePdtStatusByUsrUid(int usrUid, boolean isSleep) {

        String sql;
        if (isSleep)
            sql = "update pdt set status = 4 where status = 1 and usr_uid = " + usrUid;
        else
            sql = "update reply set status = 1 where status = 4 and usr_uid = " + usrUid;

        entityManager.createNativeQuery(sql).executeUpdate();
    }

    @Transactional
    public void removeUsrAndReference(int usrUid) {

        String sql = "update reply set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update report_pdt set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_pdt_like where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_pdt_wish where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_brand_like where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_feed where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_wish where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_alarm where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update report set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update qna set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update usr_card set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_like where usr_uid = " + usrUid + " or peer_usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_hide where usr_uid = " + usrUid + " or peer_usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_block where usr_uid = " + usrUid + " or peer_usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete tp from theme_pdt tp join pdt p on tp.pdt_uid = p.pdt_uid where p.usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete upw from usr_pdt_wish upw join pdt p on upw.pdt_uid = p.pdt_uid where p.usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete upl from usr_pdt_like upl join pdt p on upl.pdt_uid = p.pdt_uid where p.usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update reply r join pdt p on r.pdt_uid = p.pdt_uid set r.status = 0 where p.usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update pdt set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_sns where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "delete from usr_push_setting where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();

        sql = "update usr set status = 0 where usr_uid = " + usrUid;
        entityManager.createNativeQuery(sql).executeUpdate();
    }
}
