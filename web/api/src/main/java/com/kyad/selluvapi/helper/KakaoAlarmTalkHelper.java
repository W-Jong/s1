package com.kyad.selluvapi.helper;

import com.google.gson.Gson;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KakaoAlarmTalkHelper {
    public static boolean sendKakaoAlarmTalk(int type, String telnumber, String msg, int dealUid) {
        //SMS 외부 API 연동
        boolean resultBool = false;
        HttpsURLConnection conn = null;
        OutputStream outputStream = null;
        try {
            URL url = new URL("https://dev-alimtalk-api.bizmsg.kr:1443/");

            conn = (HttpsURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
//            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);

            Map<String, Object> reqHashMap = new HashMap<>();
            reqHashMap.put("userId", "selluv");
            reqHashMap.put("message_type", "ft");
            if (telnumber.length() > 11)
                reqHashMap.put("phn", telnumber);
            else
                reqHashMap.put("phn", "082" + telnumber);
            reqHashMap.put("profile", "0d1b8a0a64bd22078ea266c5fa5447512791581a");
            reqHashMap.put("msg", msg);

            Map<String, String> button1 = new HashMap<>();
            switch (type) {
                default:
                case 1:
                    button1.put("name", "판매주문서");
                    button1.put("type", "AL");
                    button1.put("scheme_ios", "selluv://deal/" + dealUid);
                    button1.put("scheme_android", "selluv://deal/" + dealUid);
                    break;
                case 2:
                    button1.put("name", "배송조회");
                    button1.put("type", "DS");
                    break;
                case 3:
                    button1.put("name", "계좌번호관리");
                    button1.put("type", "AL");
                    button1.put("scheme_ios", "selluv://accountInfo");
                    button1.put("scheme_android", "selluv://accountInfo");
                    break;
            }
            reqHashMap.put("button1", button1);
            List<Object> sendList = new ArrayList<>();
            sendList.add(reqHashMap);

            Gson gson = new Gson();

            String params = gson.toJson(sendList);

            outputStream = conn.getOutputStream();
            outputStream.write(params.getBytes("UTF-8"));
            outputStream.flush();
            outputStream.close();

            int responseCode = conn.getResponseCode();

            StringBuilder response = new StringBuilder();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                while ((line = br.readLine()) != null) {
                    response.append(line).append("\n");
                }
                br.close();
            } else {
                response.append("");
            }

            String result = response.toString().trim();

            Object resultJson = gson.fromJson(result, Object.class);
            List<Object> resultList = (List<Object>) resultJson;


            if (resultList != null && resultList.size() > 0) {
                Map<String, Object> rootJson = (Map<String, Object>) (resultList.get(0));
                String code = (String) rootJson.get("code");
                String message = (String) rootJson.get("message");
                if ("success".equals(code)) {
                    resultBool =  true;
                } else {
                    resultBool =  false;
                }
            } else {
                resultBool =  false;
            }
        } catch (IOException e) {
            resultBool =  false;
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null)
                conn.disconnect();
        }
        return resultBool;
    }
}
