package com.kyad.selluvapi.rest.common.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.net.ntp.TimeStamp;

import java.io.IOException;

public class UnixTimestampSerializer extends JsonSerializer<TimeStamp> {
    @Override
    public void serialize(TimeStamp localTimestamp, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNumber(localTimestamp.getTime() / 1000L);
    }
}
