package com.kyad.selluvapi.helper.cvsnet.request;

import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.helper.cvsnet.CvsnetClientHelper;
import lombok.Data;

@Data
public class ReservationReq {
    //서비스구분
    @SerializedName("com_gb")
    private int com_gb = 1;

    //업체코드
    @SerializedName("cust_no")
    private int cust_no = CvsnetClientHelper.SELLUV_CVSNET_CODE;

    //승인번호
    @SerializedName("cust_ord_no")
    private String cust_ord_no;

    //접수 일(쇼핑몰사 처리일)
    @SerializedName("acpt_dt")
    private String acpt_dt;

    //업체_송하인 휴대전화번호
    @SerializedName("send_tel")
    private String send_tel;

    //업체_송하인 명
    @SerializedName("send_name")
    private String send_name;

    //업체_송하인 우편번호
    @SerializedName("send_zip")
    private String send_zip;

    //업체_송하인 우편주소
    @SerializedName("send_addr1")
    private String send_addr1;

    //업체_송하인 상세주소
    @SerializedName("send_addr2")
    private String send_addr2;

    //상품군 코드
    @SerializedName("goods_no")
    private String goods_no = "null";

    //대표 품목 명
    @SerializedName("goods")
    private String goods = "";

    //내용 총수량
    @SerializedName("goods_cnt")
    private long goods_cnt = 1;

    //사은품 포함여부
    @SerializedName("extra")
    private String extra = "";

    //물품가액
    @SerializedName("goods_price")
    private long goods_price = 0;

    //업체_수하인명
    @SerializedName("receive_name")
    private String receive_name;

    //업체_수하인 전화번호
    @SerializedName("receive_tel")
    private String receive_tel;

    //업체_수하인 우편번호
    @SerializedName("receive_zip")
    private String receive_zip;

    //업체_수하인 우편주소
    @SerializedName("receive_addr1")
    private String receive_addr1;

    //업체_수하인 상세주소
    @SerializedName("receive_addr2")
    private String receive_addr2;

    //결제 구분
    @SerializedName("tls_gb")
    private int tls_gb = 1;

    //상품구성정보
    @SerializedName("goods_info")
    private String goods_info = "";

    //Order_num
    @SerializedName("order_num")
    private int order_num;

    //운임
    @SerializedName("charge")
    private String charge = "null";

    //중량제한
    @SerializedName("limit_wgt")
    private int limit_wgt = 0;

    //취소구분
    @SerializedName("cancel_flag")
    private String cancel_flag = "null";
}
