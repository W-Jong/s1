package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.UsrFeedDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrFeedRepository extends CrudRepository<UsrFeedDao, Integer> {

    @Query("select a from UsrFeedDao a")
    Page<UsrFeedDao> findAll(Pageable pageable);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count, " +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img, " +
            " uf.usr_feed_uid, uf.reg_time AS uf_reg_time, uf.type, uf.peer_usr_uid," +
            " u2.usr_id AS u2_usr_id, u2.usr_nck_nm AS u2_usr_nck_nm, u2.profile_img AS u2_profile_img " +
            " FROM usr_feed uf JOIN pdt p ON uf.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " LEFT JOIN usr u2 ON uf.peer_usr_uid = u2.usr_uid AND u2.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " LEFT JOIN pdt_style ps ON ps.pdt_style_uid = (SELECT ps1.pdt_style_uid FROM pdt_style ps1 WHERE ps1.pdt_uid = p.pdt_uid ORDER BY ps1.reg_time DESC LIMIT 1) " +
            " WHERE p.photoshop_yn = 1 AND uf.usr_uid = ?1 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY uf.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_feed uf JOIN pdt p ON uf.pdt_uid = p.pdt_uid " +
                    " JOIN usr u ON p.usr_uid = u.usr_uid  AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND uf.usr_uid = ?1 ",
            nativeQuery = true)
    Page<Object> findAllFeedList(Pageable pageable, int usrUid);

    @Query(value = "SELECT p.pdt_uid, p.usr_uid, p.brand_uid, p.category_uid, p.pdt_size, p.profile_img, " +
            " p.origin_price, p.price, p.color_name, p.pdt_model, p.reg_time, p.edt_time, p.status, u.usr_id, u.usr_nck_nm, u.profile_img AS u_profile_img, " +
            " b.first_ko, b.first_en, b.name_ko, b.name_en, b.profile_img AS b_profile_img, " +
            " c.depth, c.category_name, (SELECT count(*) FROM usr_pdt_like upl WHERE upl.pdt_uid = p.pdt_uid) AS pdt_like_count, " +
            " (SELECT count(*) FROM usr_pdt_like upl2 WHERE upl2.pdt_uid = p.pdt_uid AND upl2.usr_uid = ?1) AS pdt_like_status, " +
            " ps.pdt_style_uid, ps.reg_time AS ps_reg_time, ps.usr_uid AS ps_usr_uid, ps.style_img AS ps_style_img, " +
            " uf.usr_feed_uid, uf.reg_time AS uf_reg_time, uf.type, uf.peer_usr_uid," +
            " u2.usr_id AS u2_usr_id, u2.usr_nck_nm AS u2_usr_nck_nm, u2.profile_img AS u2_profile_img " +
            " FROM usr_feed uf JOIN pdt_style ps ON uf.pdt_uid = ps.pdt_uid " +
            " JOIN pdt p ON uf.pdt_uid = p.pdt_uid " +
            " JOIN brand b ON p.brand_uid = b.brand_uid " +
            " JOIN usr u ON p.usr_uid = u.usr_uid AND u.status = 1 " +
            " LEFT JOIN usr u2 ON uf.peer_usr_uid = u2.usr_uid AND u2.status = 1 " +
            " JOIN category c ON p.category_uid = c.category_uid " +
            " WHERE p.photoshop_yn = 1 AND uf.usr_uid = ?1 " +
            " AND NOT exists(SELECT ub.usr_block_uid FROM usr_block ub WHERE (ub.usr_uid = ?1 AND ub.peer_usr_uid = u.usr_uid) OR (ub.usr_uid = u.usr_uid AND ub.peer_usr_uid = ?1)) " +
            " ORDER BY uf.reg_time DESC \n#pageable\n ",
            countQuery = "SELECT count(*) FROM usr_feed uf JOIN pdt_style ps ON uf.pdt_uid = ps.pdt_uid JOIN pdt p ON uf.pdt_uid = p.pdt_uid " +
                    " JOIN usr u ON p.usr_uid = u.usr_uid  AND u.status = 1 " +
                    " WHERE p.photoshop_yn = 1 AND uf.usr_uid = ?1 ",
            nativeQuery = true)
    Page<Object> findAllFeedStyleList(Pageable pageable, int usrUid);
}
