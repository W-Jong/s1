package com.kyad.selluvapi.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "notice", schema = "selluv")
public class NoticeDao {
    private int noticeUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty("타이틀")
    private String title;
    @ApiModelProperty("내용")
    private String content;
    @JsonIgnore
    private Timestamp startTime;
    @JsonIgnore
    private Timestamp endTime;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "notice_uid", nullable = false)
    public int getNoticeUid() {
        return noticeUid;
    }

    public void setNoticeUid(int noticeUid) {
        this.noticeUid = noticeUid;
    }

    @Basic
    @Column(name = "reg_time", nullable = false)
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = false)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoticeDao noticeDao = (NoticeDao) o;
        return noticeUid == noticeDao.noticeUid &&
                status == noticeDao.status &&
                Objects.equals(regTime, noticeDao.regTime) &&
                Objects.equals(title, noticeDao.title) &&
                Objects.equals(content, noticeDao.content) &&
                Objects.equals(startTime, noticeDao.startTime) &&
                Objects.equals(endTime, noticeDao.endTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noticeUid, regTime, title, content, startTime, endTime, status);
    }
}
