package com.kyad.selluvapi.helper.iamport.response;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.kyad.selluvapi.rest.common.serializer.GsonUnixTimestampDeserializer;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class PaymentCancelAnnotation {
    @SerializedName("pg_tid")
    String pg_tid;

    @SerializedName("amount")
    BigDecimal amount;

    @JsonAdapter(GsonUnixTimestampDeserializer.class)
    @SerializedName("cancelled_at")
    Timestamp cancelled_at;

    @SerializedName("reason")
    String reason;

    @SerializedName("receipt_url")
    String receipt_url;
}
