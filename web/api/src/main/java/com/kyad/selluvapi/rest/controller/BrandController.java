package com.kyad.selluvapi.rest.controller;

import com.kyad.selluvapi.dao.BrandDao;
import com.kyad.selluvapi.dao.UsrBrandLikeDao;
import com.kyad.selluvapi.dao.UsrDao;
import com.kyad.selluvapi.dto.brand.BrandDetailDto;
import com.kyad.selluvapi.dto.brand.BrandListDto;
import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.enumtype.PDT_GROUP_TYPE;
import com.kyad.selluvapi.enumtype.SEARCH_PDT_GROUP_TYPE;
import com.kyad.selluvapi.request.MultiUidsReq;
import com.kyad.selluvapi.rest.common.CommonConstant;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.BrandService;
import com.kyad.selluvapi.rest.service.UsrBrandLikeService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "/brand", description = "브랜드관리")
@RequestMapping("/brand")
public class BrandController {

    private final Logger LOG = LoggerFactory.getLogger(BrandController.class);

    @Autowired
    private UsrService usrService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private UsrBrandLikeService usrBrandLikeService;

    @ApiOperation(value = "브랜드 팔로우하기",
            notes = "브랜드를 팔로우한다.")
    @PutMapping(value = "/{brandUid}/like")
    public GenericResponse<Void> insertLike(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "brandUid") int brandUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        BrandDao brandDao = brandService.getBrandInfo(brandUid);

        UsrBrandLikeDao usrBrandLike = usrBrandLikeService.getByUsrUidAndBrandUid(usr.getUsrUid(), brandUid);
        if (usrBrandLike != null)
            return new GenericResponse<>(ResponseMeta.ALREADY_LIKE_ADDED, null);

        usrBrandLike = new UsrBrandLikeDao();
        usrBrandLike.setUsrUid(usr.getUsrUid());
        usrBrandLike.setBrandUid(brandUid);
        usrBrandLikeService.save(usrBrandLike);

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "멀티브랜드 팔로우하기",
            notes = "브랜드를 여러개 팔로우한다.")
    @PutMapping(value = "/likes")
    public GenericResponse<Void> insertLikes(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid MultiUidsReq multiUidsRequest) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        for (int brandUid : multiUidsRequest.getUids()) {
            if (usrBrandLikeService.getByUsrUidAndBrandUid(usr.getUsrUid(), brandUid) == null) {
                UsrBrandLikeDao usrBrandLike = new UsrBrandLikeDao();

                usrBrandLike.setUsrUid(usr.getUsrUid());
                usrBrandLike.setBrandUid(brandUid);
                usrBrandLikeService.save(usrBrandLike);
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "브랜드팔로우 취소하기",
            notes = "브랜드에 대한 팔로우를 취소한다.")
    @DeleteMapping(value = "/{brandUid}/like")
    public GenericResponse<Void> deleteLike(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "brandUid") int brandUid) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        BrandDao brandDao = brandService.getBrandInfo(brandUid);

        UsrBrandLikeDao usrBrandLike = usrBrandLikeService.getByUsrUidAndBrandUid(usr.getUsrUid(), brandUid);
        if (usrBrandLike == null)
            return new GenericResponse<>(ResponseMeta.ALREADY_LIKE_DELETED, null);

        usrBrandLikeService.delete(usrBrandLike.getUsrBrandLikeUid());

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "멀티브랜드 팔로우 취소하기",
            notes = "브랜드를 여러개 팔로우 취소한다.")
    @DeleteMapping(value = "/likes")
    public GenericResponse<Void> deleteLikes(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestBody @Valid MultiUidsReq multiUidsRequest) {

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        for (int brandUid : multiUidsRequest.getUids()) {
            UsrBrandLikeDao usrBrandLike = usrBrandLikeService.getByUsrUidAndBrandUid(usr.getUsrUid(), brandUid);
            if (usrBrandLike != null) {
                usrBrandLikeService.delete(usrBrandLike.getUsrBrandLikeUid());
            }
        }

        return new GenericResponse<>(ResponseMeta.OK, null);
    }

    @ApiOperation(value = "추천브랜드 목록얻기",
            notes = "남성, 여성, 키즈 상품군에 따르는 추천브랜드목록을 얻는다.")
    @GetMapping(value = "/recommended")
    public GenericResponse<List<BrandMiniDto>> getRecommended(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("상품군 MALE-남성, FEMALE-여성, KIDS-키즈")
            @RequestParam(value = "pdtGroup") PDT_GROUP_TYPE pdtGroup) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, brandService.getRecommendedBrandListByPdtGroup(pdtGroup.getCode()));
    }

    @ApiOperation(value = "모든 브랜드 목록얻기",
            notes = "모든 정규브랜드 목록을 얻는다.")
    @GetMapping(value = "")
    public GenericResponse<List<BrandMiniDto>> getAllList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, brandService.getAllBrandList());
    }

    @ApiOperation(value = "모든 브랜드 디테일 목록얻기",
            notes = "모든 정규브랜드 디테일목록을 얻는다.(배경이미지, 로고, 유저팔로우갯수 포함)")
    @GetMapping(value = "/list")
    public GenericResponse<List<BrandListDto>> getAllDetailList(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam(value = "정렬방식 1-영문정렬, 2-한글정렬", allowableValues = "1,2", required = true)
            @RequestParam(value = "column") int column) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK,
                brandService.getAllDetailBrandList(usr.getUsrUid(), column));
    }

    @ApiOperation(value = "신규 브랜드 등록",
            notes = "새로운 신규브랜드를 등록한다.")
    @PutMapping(value = "")
    public GenericResponse<BrandMiniDto> putBrand(
            @RequestHeader(value = "accessToken") String accessToken,
            @ApiParam("신규브랜드명") @RequestParam(value = "brandName") String brandName) {

        if (brandName == null || brandName.isEmpty()) {
            return new GenericResponse<>(ResponseMeta.PAGE_NOT_FOUND, null);
        }

        UsrDao usr = usrService.getUserInfoNotTemp(accessToken);

        BrandDao brand = new BrandDao();
        brand.setNameEn(brandName);
        brand.setNameKo(brandName);

        brandService.save(brand);

        return new GenericResponse<>(ResponseMeta.OK,
                brandService.convertBrandDaoToBrandMiniDto(brand));
    }

    @ApiOperation(value = "팔로우 브랜드목록 얻기",
            notes = "팔로우 브랜드 목록을 얻는다.")
    @GetMapping(value = "/like")
    public GenericResponse<List<BrandListDto>> getFollowBrandList(
            @RequestHeader(value = "accessToken") String accessToken) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK,
                brandService.getFollowBrandList(usr.getUsrUid(), usr.getLikeGroup()));
    }

    @ApiOperation(value = "인기 브랜드 목록 얻기(10개)",
            notes = "상품군에 따르는 인기 브랜드 10개 목록을 얻는다.")
    @GetMapping(value = "/fame")
    public GenericResponse<List<BrandListDto>> getFameBrandList(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestParam(value = "pdtGroupType") PDT_GROUP_TYPE pdtGroupType) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK,
                brandService.getFameBrandListByPdtGroup(usr.getUsrUid(), pdtGroupType.getCode()));
    }

    @ApiOperation(value = "인기 브랜드 목록 얻기(전체)",
            notes = "상품군에 따르는 인기 브랜드 전체목록을 얻는다.")
    @GetMapping(value = "/fame/all")
    public GenericResponse<List<BrandListDto>> getFameAllBrandList(
            @RequestHeader(value = "accessToken") String accessToken,
            @RequestParam(value = "pdtGroupType") PDT_GROUP_TYPE pdtGroupType) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK,
                brandService.getFameAllBrandListByPdtGroup(usr.getUsrUid(), pdtGroupType.getCode()));
    }

    @ApiOperation(value = "브랜드 상세얻기",
            notes = "브랜드 상세정보를 얻는다.")
    @GetMapping(value = "/{brandUid}")
    public GenericResponse<BrandDetailDto> getBrandDetail(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "brandUid") int brandUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        BrandDetailDto brandDetail = brandService.getBrandDetail(brandUid, usr.getUsrUid());

        if (brandDetail == null)
            return new GenericResponse<>(ResponseMeta.BRAND_NOT_EXIST, null);

        return new GenericResponse<>(ResponseMeta.OK, brandDetail);
    }

    @ApiOperation(value = "브랜드 상품군에 따르는 상품목록 얻기",
            notes = "해당 브랜드에 속하는 상품군에 따르는 상품목록을 얻는다.")
    @GetMapping(value = "/{brandUid}/list")
    public GenericResponse<Page<PdtListDto>> getBrandPdtList(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "brandUid") int brandUid,
            @RequestParam(value = "tab") SEARCH_PDT_GROUP_TYPE searchPdtGroupType,
            @ApiParam(value = "페이지번호, 0부터 시작") @RequestParam(value = "page", defaultValue = "0") int page) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        BrandDao brandDao = brandService.getBrandInfo(brandUid);

        return new GenericResponse<>(ResponseMeta.OK, brandService.getBrandPdtList(
                new PageRequest(page, CommonConstant.COUNT_PER_PAGE), usr.getUsrUid(), brandUid, searchPdtGroupType));
    }

    @ApiOperation(value = "브랜드에 따르는 추천모델명 리스트 얻기",
            notes = "해당 브랜드에 속하는 상품갯수가 많은 순서로 모델명 10개를 얻는다.")
    @GetMapping(value = "/{brandUid}/modelNames")
    public GenericResponse<List<String>> getRecommendModelByBrandUid(
            @RequestHeader(value = "accessToken") String accessToken,
            @PathVariable(value = "brandUid") int brandUid) {

        UsrDao usr = usrService.getUserInfo(accessToken);

        return new GenericResponse<>(ResponseMeta.OK, brandService.getRecommendModelByBrandUid(brandUid));
    }
}
