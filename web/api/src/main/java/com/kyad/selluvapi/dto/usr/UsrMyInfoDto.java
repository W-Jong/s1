package com.kyad.selluvapi.dto.usr;

import com.kyad.selluvapi.dao.EventDao;
import com.kyad.selluvapi.dao.UsrAddressDao;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
public class UsrMyInfoDto {
    @ApiModelProperty("회원정보 - 로그인시 회원정보와 동일")
    UsrDetailDto usr;
    @ApiModelProperty("주소정보")
    private List<UsrAddressDao> addressList;
    @ApiModelProperty("팔로워수")
    private long usrFollowerCount;
    @ApiModelProperty("팔로잉수")
    private long usrFollowingCount;
    @ApiModelProperty("아이템수")
    private long countPdt;
    @ApiModelProperty("구매수")
    private long countBuy;
    @ApiModelProperty("판매수")
    private long countSell;
    @ApiModelProperty("읽지 않는 공지수")
    private long countUnreadNotice;
    @ApiModelProperty("마이페이지 노출이벤트")
    private List<EventDao> myPageEventList;
}
