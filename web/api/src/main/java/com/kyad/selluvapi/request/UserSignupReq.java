package com.kyad.selluvapi.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kyad.selluvapi.enumtype.LOGIN_TYPE;
import com.kyad.selluvapi.rest.common.serializer.LocalDateDeserializer;
import com.kyad.selluvapi.rest.common.serializer.LocalDateSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;


@Data
public class UserSignupReq {

    @NotEmpty
    @ApiModelProperty("아이디")
    private String usrId;
    @NotEmpty
    @ApiModelProperty("닉네임")
    private String usrNckNm;
    @NotEmpty
    @ApiModelProperty("이메일")
    private String usrMail;
    @NotNull
    @ApiModelProperty("sns 아이디, 이메일회원가입 인 경우 빈문자열")
    private String snsId;
    @NotNull
    @ApiModelProperty("비번, sns로그인의 경우 sns토큰")
    private String password;
    @NotNull
    @ApiModelProperty("sns 프로필이미지 URL, 이메일회원가입 인 경우 빈문자열")
    private String snsProfileImg;
    @NotNull
    @ApiModelProperty("회원가입 유형")
    private LOGIN_TYPE loginType;
    @NotNull
    @ApiModelProperty("추천인 코드")
    private String inviteCode;
    @NotEmpty
    @ApiModelProperty("본인인증 실명")
    private String usrNm;
    @ApiModelProperty("본인인증 생년월일 (형식:1990-01-01)")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate birthday;
    @Min(1)
    @Max(2)
    @ApiModelProperty("본인인증 성별 1-남, 2-여")
    private int gender;
    @NotEmpty
    @ApiModelProperty("본인인증 폰번호")
    private String usrPhone;
    @NotNull
    @ApiModelProperty("관심브랜드 UID 리스트")
    private List<Integer> likeBrands;
}
