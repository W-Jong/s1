package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.AppVersionDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppVersionRepository extends CrudRepository<AppVersionDao, Integer> {

    @Query("select a from AppVersionDao a")
    Page<AppVersionDao> findAll(Pageable pageable);

    @Query(value = "SELECT * FROM app_version WHERE app_type = ?1 ORDER BY version_code DESC LIMIT 1", nativeQuery = true)
    AppVersionDao getLastedVersion(int type);
}
