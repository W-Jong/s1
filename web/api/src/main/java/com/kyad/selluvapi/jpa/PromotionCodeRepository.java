package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.PromotionCodeDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PromotionCodeRepository extends CrudRepository<PromotionCodeDao, Integer> {

    @Query("select a from PromotionCodeDao a")
    Page<PromotionCodeDao> findAll(Pageable pageable);

    @Query(value = "SELECT * from promotion_code where status = 1 and title = ?1 " +
            " order by limit_type desc, end_time asc, start_time asc", nativeQuery = true)
    List<PromotionCodeDao> findAllByTitle(String title);
}
