package com.kyad.selluvapi.dto.usr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class UsrDetailDto {
    @ApiModelProperty("회원UID")
    private int usrUid;
    @ApiModelProperty("추가된 시간")
    private Timestamp regTime;
    @ApiModelProperty("이메일")
    private String usrMail;
    //    private String usrPwd;
    @ApiModelProperty("휴대폰번호")
    private String usrPhone;
    @ApiModelProperty("아이디")
    private String usrId;
//    @ApiModelProperty("실명")
//    private String usrNm;
    @ApiModelProperty("닉네임")
    private String usrNckNm;
    @ApiModelProperty("프로필이미지")
    private String profileImg;
    @ApiModelProperty("프로필배경이미지")
    private String profileBackImg;
//    @ApiModelProperty("생년월일")
//    @JsonSerialize(using = LocalDateSerializer.class)
//    @JsonDeserialize(using = LocalDateDeserializer.class)
//    private LocalDate birthday;
    @ApiModelProperty("성별 1-남, 2-여")
    private int gender;
    @ApiModelProperty("은행명")
    private String bankNm;
    @ApiModelProperty("계좌주")
    private String accountNm;
    @ApiModelProperty("계좌번호")
    private String accountNum;
    @ApiModelProperty("자기소개")
    private String description;
    //    private int inviteUsrUid;
    @ApiModelProperty("매너포인트")
    private long point;
    @ApiModelProperty("셀럽머니")
    private long money;
    @ApiModelProperty("로그인타입 0 - 일반로그인 1-페이스북 2-네이버 3-카카오톡")
    private int usrLoginType;
    //    private String snsId;
    @ApiModelProperty("관심상품군 1-남성, 2-여성, 4-키즈, 다중선택일때 합산값")
    private int likeGroup;
    //    private String accessToken;
    //    private String deviceToken;
    //    private Timestamp loginTime;
    @ApiModelProperty("판매자휴가모드여부 1-휴가모드 0-휴가모드아님")
    private int sleepYn;
    @ApiModelProperty("판매자무료배송최저가 -1-무료배송안함, 그외 정수 무료배송최저가")
    private long freeSendPrice;
    @ApiModelProperty("판매자휴가모드완료날짜 sleepYn = 1일때만 유효")
    private Timestamp sleepTime;
    //    private Timestamp edtTime;
    //    private Timestamp noticeTime;
    @ApiModelProperty("상태 1- 정상, 2-탈퇴한 회원")
    private int status;
}
