package com.kyad.selluvapi.enumtype;

public enum OS_TYPE {
    IOS(1), ANDROID(2);

    private int code;

    private OS_TYPE(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
