package com.kyad.selluvapi.dto.pdt;

import com.kyad.selluvapi.dto.brand.BrandMiniDto;
import com.kyad.selluvapi.dto.category.CategoryMiniDto;
import com.kyad.selluvapi.dto.usr.UsrDetailDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PdtDetailDto {
    @ApiModelProperty("상품")
    private PdtDto pdt;
    @ApiModelProperty("판매유저")
    private UsrDetailDto seller;
    @ApiModelProperty("브랜드")
    private BrandMiniDto brand;
    @ApiModelProperty("상세카테고리목록(상품군/1단계/2단계/3단계 카테고리)")
    private List<CategoryMiniDto> categoryList;
    @ApiModelProperty("스타일리스트")
    private List<PdtStyleDto> pdtStyleList;
    @ApiModelProperty("상품좋아요 갯수")
    private int pdtLikeCount;
    @ApiModelProperty("유저 상품좋아요 상태")
    private Boolean pdtLikeStatus;
    @ApiModelProperty("상품잇템 유저수")
    private int pdtWishCount;
    @ApiModelProperty("유저 상품잇템 상태")
    private Boolean pdtWishStatus;
    @ApiModelProperty("상품 댓글수")
    private int pdtReplyCount;
    @ApiModelProperty("등록유저 아이템 갯수")
    private int sellerItemCount;
    @ApiModelProperty("등록유저 상품리스트")
    private List<PdtInfoDto> sellerPdtList;
    @ApiModelProperty("등록유저 팔로워 유저수")
    private int sellerFollowerCount;
    @ApiModelProperty("연관 스타일 갯수")
    private int relationStyleCount;
    @ApiModelProperty("공유URL")
    private String shareUrl;
    @ApiModelProperty("수정상태 true: 수정됨, false: 수정안됨")
    private boolean isEdited;
}
