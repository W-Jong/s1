package com.kyad.selluvapi.rest.controller;

import com.google.gson.Gson;
import com.kyad.selluvapi.dao.DealDao;
import com.kyad.selluvapi.helper.CommonHelper;
import com.kyad.selluvapi.helper.iamport.IamportClientHelper;
import com.kyad.selluvapi.helper.iamport.response.CertificationAnnotation;
import com.kyad.selluvapi.helper.iamport.response.IamportResponse;
import com.kyad.selluvapi.helper.iamport.response.PaymentAnnotation;
import com.kyad.selluvapi.rest.common.response.GenericResponse;
import com.kyad.selluvapi.rest.common.response.ResponseMeta;
import com.kyad.selluvapi.rest.service.DealService;
import com.kyad.selluvapi.rest.service.PaymentResultService;
import com.kyad.selluvapi.rest.service.UsrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/payments")
@Api(value = "/payments", description = "결제연동", hidden = true)
public class PaymentController {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PaymentResultService paymentResultService;

    @PostMapping(value = "/callback")
    @ApiOperation(value = "아임포트결제콜백(API용)",
            notes = "아임포트 인증결제 성공후 호출되는 콜백")
    @Transactional
    public GenericResponse<Void> paymentCompletedCallback(@ApiIgnore ModelMap model, @ApiIgnore HttpServletRequest request) {
        // https://www.yourdomain.com/payments/complete?imp_uid=imp_12345678&merchant_uid=oid_987654321&imp_success=true

        String impUid = request.getParameter("imp_uid");
        String merchantUid = request.getParameter("merchant_uid");
        String impSuccess = request.getParameter("imp_success");

        Gson gson = new Gson();
        String sql = "INSERT INTO iamport_callback(imp_uid, merchant_uid, request, header, ip_addr) VALUES (?1, ?2, ?3, ?4, ?5)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, (impUid != null) ? impUid : "");
        query.setParameter(2, (merchantUid != null) ? merchantUid : "");
        query.setParameter(3, gson.toJson(request.getParameterMap()));
        query.setParameter(4, request.getHeader("User-Agent"));
        query.setParameter(5, CommonHelper.getRemoteAddr(request));
        query.executeUpdate();

        if (!"true".equals(impSuccess)) {
            return new GenericResponse<>(ResponseMeta.PAY_FAILED, null);
        }

        IamportClientHelper iamportClient = new IamportClientHelper();
        boolean isSuccess = false;
        try {
            IamportResponse<PaymentAnnotation> paymentIamportResponse = iamportClient.paymentByImpUid(impUid);
            PaymentAnnotation payment = paymentIamportResponse.getResponse();

            if ("paid".equals(payment.getStatus()) && paymentResultService.paymentResultProcess(payment)) { // 결제성공
                isSuccess = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isSuccess)
            return new GenericResponse<>(ResponseMeta.OK, null);
        else
            return new GenericResponse<>(ResponseMeta.PAY_FAILED, null);
    }

    @PostMapping(value = "/danal")
    @ApiOperation(value = "다날본인인증완료(API용)",
            notes = "다날본인인증 완료후 ajax로 호출됨")
    public GenericResponse<CertificationAnnotation> danalCompleted(@RequestParam(value = "imp_uid") String impUid) {

        IamportClientHelper iamportClient = new IamportClientHelper();
        try {
            IamportResponse<CertificationAnnotation> certificationIamportResponse = iamportClient.certificationByImpUid(impUid);
            CertificationAnnotation certification = certificationIamportResponse.getResponse();

            return new GenericResponse<>(ResponseMeta.OK, certification);
        } catch (Exception e) {
            e.printStackTrace();
            return new GenericResponse<>(ResponseMeta.CERTIFICATION_FAILED, null);
        }
    }
}
