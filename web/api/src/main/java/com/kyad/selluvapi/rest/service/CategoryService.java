package com.kyad.selluvapi.rest.service;

import com.kyad.selluvapi.dao.CategoryDao;
import com.kyad.selluvapi.dto.brand.BrandRecommendedListDto;
import com.kyad.selluvapi.dto.category.CategoryMiniDto;
import com.kyad.selluvapi.helper.StarmanHelper;
import com.kyad.selluvapi.jpa.CategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public CategoryDao findOne(int categoryUid) {
        return categoryRepository.findOne(categoryUid);
    }

    public List<CategoryDao> getSubCategories(int categoryUid) {
        return categoryRepository.findAllByParentCategoryUidAndStatusOrderByCategoryOrder(categoryUid, 1);
    }

    public List<Integer> getSubAllCategoryUids(int categoryUid) {
        return categoryRepository.getSubAllCategoryUids(categoryUid);
    }

    public List<Integer> getSubAllCategoryUidsWithIt(int categoryUid) {
        return categoryRepository.getSubAllCategoryUidsWithIt(categoryUid);
    }

    public List<CategoryDao> getSubAllCategories(int categoryUid) {
        return categoryRepository.getSubAllCategories(categoryUid);
    }

    public List<CategoryDao> getAllHierarchyCategories(int categoryUid) {
        Object result = categoryRepository.getAllHierarchyCategories(categoryUid);
        if (result == null)
            return null;

        Object[] objects = (Object[]) result;
        List<CategoryDao> categoryDaoList = new ArrayList<>();
        if (objects[3] != null)
            categoryDaoList.add((CategoryDao) objects[3]);
        if (objects[2] != null)
            categoryDaoList.add((CategoryDao) objects[2]);
        if (objects[1] != null)
            categoryDaoList.add((CategoryDao) objects[1]);
        if (objects[0] != null)
            categoryDaoList.add((CategoryDao) objects[0]);

        return categoryDaoList;
    }

    public List<Integer> getStep2SubAllCategoryUids(int categoryUid) {
        List<CategoryDao> categoryDaoList = getAllHierarchyCategories(categoryUid);
        if (categoryDaoList == null || categoryDaoList.size() < 3)
            return null;
        int step2CategoryUid = categoryDaoList.get(2).getCategoryUid();
        return getSubAllCategoryUidsWithIt(step2CategoryUid);
    }

    public List<BrandRecommendedListDto> getRecommendedBrandByCategoryUid(int categoryUid) {
        List<Integer> categoryUids = getSubAllCategoryUidsWithIt(categoryUid);
        if (categoryUids == null || categoryUids.size() < 1) {
            return null;
        }

        List<Object> objectList = categoryRepository.getRecommendedBrandByCategoryUids(categoryUids);
        if (objectList == null)
            return null;

        List<BrandRecommendedListDto> dtoList = new ArrayList<>();
        for (Object result : objectList) {
            dtoList.add(convertObjectToBrandRecommendedListDto((Object[]) result));
        }

        return dtoList;
    }

    private BrandRecommendedListDto convertObjectToBrandRecommendedListDto(Object[] objects) {
        if (objects == null)
            return null;

        int i = 0;
        BrandRecommendedListDto dto = new BrandRecommendedListDto();
        dto.setBrandUid((int) objects[i++]);
        dto.setNameEn((String) objects[i++]);
        dto.setNameKo((String) objects[i++]);
        dto.setLogoImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setBackImg(StarmanHelper.getTargetImageUrl("brand", dto.getBrandUid(), (String) objects[i++]));
        dto.setPdtCount(((BigInteger) objects[i++]).intValue());

        return dto;
    }

    public CategoryMiniDto convertCategoryDaoToCategoryMiniDto(CategoryDao dao) {
        if (dao == null)
            return null;

        ModelMapper modelMapper = new ModelMapper();
        CategoryMiniDto dto = modelMapper.map(dao, CategoryMiniDto.class);
        return dto;
    }

    public List<CategoryMiniDto> convertCategoryDaoListToCategoryMiniDtoList(List<CategoryDao> daoList) {
        if (daoList == null)
            return null;

        List<CategoryMiniDto> dtoList = new ArrayList<>();
        for (CategoryDao dao : daoList) {
            dtoList.add(convertCategoryDaoToCategoryMiniDto(dao));
        }

        return dtoList;
    }

    public List<List<CategoryDao>> getListByKeyword(String keyword) {
        List<Integer> categoryUidList = categoryRepository.getByKeyword(keyword);
        if (categoryUidList == null)
            return null;

        List<List<CategoryDao>> categoryDaosList = new ArrayList<>();
        for (int categoryUid : categoryUidList) {
            categoryDaosList.add(getAllHierarchyCategories(categoryUid));
        }

        return categoryDaosList;
    }

    public Integer getPdtCountByCategoryUids(List<Integer> categoryUids) {
        return categoryRepository.getPdtCountByCategoryUids(categoryUids);
    }
}
