package com.kyad.selluvapi.jpa;

import com.kyad.selluvapi.dao.FaqDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FaqRepository extends CrudRepository<FaqDao, Integer> {

    @Query("select a from FaqDao a")
    Page<FaqDao> findAll(Pageable pageable);

    List<FaqDao> findAllByKindAndStatusOrderByRegTimeDesc(int kind, int status);
}
