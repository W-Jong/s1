package com.kyad.selluvapi.dto.brand;

import com.kyad.selluvapi.dto.pdt.PdtInfoDto;
import com.kyad.selluvapi.dto.pdt.PdtListDto;
import com.kyad.selluvapi.dto.pdt.PdtMiniDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class BrandListDto {
    @ApiModelProperty("브랜드UID")
    private int brandUid;
    @ApiModelProperty("영문첫글자")
    private String firstEn;
    @ApiModelProperty("한글초성")
    private String firstKo;
    @ApiModelProperty("영문이름")
    private String nameEn;
    @ApiModelProperty("한글이름")
    private String nameKo;
    @ApiModelProperty("로고이미지")
    private String logoImg;
    @ApiModelProperty("썸네일이미지")
    private String profileImg;
    @ApiModelProperty("백그라운드 이미지")
    private String backImg;
    @ApiModelProperty("팔로우 유저수")
    private long brandLikeCount;
    @ApiModelProperty("유저 팔로우 상태")
    private Boolean brandLikeStatus;
    @ApiModelProperty("상품목록")
    private List<PdtInfoDto> pdtList;
}
